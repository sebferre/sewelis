
open Jsutils
open XmlHttpRequest

(*let server_url = ref "http://localhost:9999/" (* for dev *)*)
(*let server_url = ref "http://lisfs2008.irisa.fr:9999/"*)
let server_url = ref "http://servolis.irisa.fr/sewelis/"

let user_login = ref "anonymous"
let user_key = ref "0"

let is_anonymous () = !user_login = "anonymous"

(* XML processing *)

module Xml = (* imitating Xml-light with Xmlm *)
struct
  type xml = (* copied from xml-light *)
  | Element of (string * (string * string) list * xml list)
  | PCData of string

  let to_string (xml : xml) : string =
    try
      let buf = Buffer.create 1000 in
      let output = Xmlm.make_output ~decl:false ~nl:true (`Buffer buf) in
      Xmlm.output_doc_tree
	(function
	| Element (local_tag, local_attrs, children) ->
	  let attrs = List.map (fun (name,v) -> (("",name),v)) local_attrs in
	  `El ((("",local_tag), attrs), children)
	| PCData s -> `Data s)
	output
	(None,xml);
      Buffer.contents buf
    with exn ->
      firebug ("Xml.to_string: " ^ Printexc.to_string exn);
      raise exn

  let parse_string (str : string) : xml =
    try
      let input = Xmlm.make_input (`String (0, str)) in
      snd (Xmlm.input_doc_tree
	     ~el:(fun ((_,tag), attrs) children ->
	       let local_attrs = List.map (fun ((_,name),v) -> (name,v)) attrs in
	       Element (tag, local_attrs, children))
	     ~data:(fun s -> PCData s)
	     input)
    with exn ->
      firebug ("Xml.parse_string: " ^ Printexc.to_string exn);
      raise exn
end

(*module Xml = Xml*)
      
type response_content = XML of (string * string) list * Xml.xml list | File of string * string (* mime, contents *)
type response = OK of response_content | Error of string

let response_of_xml = function
  | Xml.Element (_, attrs, children)::_ ->
    let status = try List.assoc "status" attrs with _ -> "error" in
    if status = "ok"
    then OK (XML (attrs, children))
    else
      let msg = match children with
	| [Xml.Element ("message", [], [Xml.PCData s])] -> s
	| [Xml.PCData s] -> s
	| _ -> "Undefined error" in
      Error msg
  | Xml.PCData s::_ -> Error s
  | [] -> Error "empty response"

let response_of_frame frame : response =
  match frame.headers "Content-Type" with
  | Some "text/xml" -> response_of_xml [Xml.parse_string frame.content]
  | Some mime -> OK (File (mime, frame.content))
  | None -> Error "Unexpected content-type in server response"
    
(* calling Sewelis services *)

exception HTTP_error of int
exception Sewelis_error
exception Invalid_key

type store = string

type call_mode = [`NoStore | `Store of store]

let url_service name args =
  !server_url ^ name ^ (if args=[] then  "" else "?" ^ Url.encode_arguments args)

let call_service_gen (mode : call_mode) (name : string)
    (args : (string * string) list) ?(file_args : (string * File.file Js.t) list = [])
    (k : response_content -> 'a) : 'a =
  let args =
    match mode with
      | `NoStore -> ("userKey",!user_key)::args
      | `Store store -> ("userKey",!user_key)::("storeName",store)::args in
  let get_args, post_args =
    match file_args with
    | [] -> Some args, None
    | l -> None, Some (List.map (fun (a,s) -> (a,`String (Js.string s))) args @ List.map (fun (a,f) -> (a,`File f)) file_args) in
  lwt frame = perform_raw_url ?get_args ?post_args (!server_url ^ name) in
  if frame.code / 100 = 2
  then
    match response_of_frame frame with
    | OK content -> k content
    | Error msg ->
      if msg = "Invalid key"
      then begin Dom_html.window##location##reload(); Lwt.fail (Invalid_key) end
      else begin Jsutils.alert ("Server error: " ^ msg); Lwt.fail Sewelis_error end
  else Lwt.fail (HTTP_error frame.code)

let call_service_xml mode name args ?file_args (k : (string * string) list -> Xml.xml list -> 'a) : 'a =
  call_service_gen mode name args ?file_args
    (function
    | XML (attrs, children) -> k attrs children
    | _ -> Lwt.fail (Failure "Unexpected type of server response"))
let call_service_file mode name args ?file_args (k : string -> string -> 'a) : 'a =
  call_service_gen mode name args ?file_args
    (function
    | File (mime, contents) -> k mime contents
    | _ -> Lwt.fail (Failure "Unexpected type of server response"))
    
(* we hash passwords with salt on client side in order to avoid
   that an unsecure Sewelis connection corrupts user passwords
   used on other websites. *)
let hash_passwd s =
(*Digest.to_hex (Digest.string s)*)
  pbkdf2_of_string s !server_url

    
(* Sewelis data structures coding/decoding *)

let string_of_list_user_role l =
  if l = []
  then "none"
  else String.concat " " (List.map (fun (u,r) -> u ^ ":" ^ r) l)

let list_user_role_of_string s =
  if s = "none"
  then []
  else
    let l = Regexp.split (Regexp.regexp "[,: ]+") s in
    let rec aux = function
      | u::r::l -> (u,r) :: aux l
      | _ -> [] in
    aux l

module Rdf =
struct
  type t =
  [ `URI of string
  | `XMLLiteral of Xml.xml
  | `Literal of string * [`Plain of string | `Typed of string]
  | `Blank of string ]

  let to_xml = function (* inverse of http-daemon/xmlutils.rdf_of_xmlstring *)
    | `URI uri -> Xml.Element ("URI", [], [Xml.PCData uri])
    | `XMLLiteral xml -> Xml.Element ("XMLLiteral", [], [Xml.PCData (Xml.to_string xml)])
    | `Literal (s, `Plain "") -> Xml.Element ("Literal", [], [Xml.PCData s])
    | `Literal (s, `Plain lang) -> Xml.Element ("Literal", ["lang", lang], [Xml.PCData s])
    | `Literal (s, `Typed uri) -> Xml.Element ("Literal", ["datatype", Url.urlencode uri], [Xml.PCData s])
    | `Blank id -> Xml.Element ("Blank", [], [Xml.PCData id])

  let to_xmlstring rdf = Xml.to_string (to_xml rdf)
end

      
(* defining Sewelis functions *)

let ping () : string Lwt.t =
  call_service_xml `NoStore "ping" []
    (fun _ _ -> Lwt.return "Hello world!")

let register ~(login : string) ~(passwd : string) ~(email : string) : unit Lwt.t =
  call_service_xml `NoStore "register"
    ["userLogin", login; "passwd", hash_passwd passwd; "email", email]
    (fun _ _ -> Lwt.return ())
    
let login ~(login : string) ~(passwd : string) : unit Lwt.t =
  call_service_xml `NoStore "login"
    ["userLogin", login; "passwd", hash_passwd passwd]
    (fun attrs _ ->
      let key = try List.assoc "userKey" attrs with _ -> assert false in
      user_login := login;
      user_key := key;
      Lwt.return ())

let logout () : unit Lwt.t =
  call_service_xml `NoStore "logout"
    ["userKey", !user_key]
    (fun _ _ ->
      user_login := "anonymous";
      user_key := "0";
      Lwt.return ())

let change_passwd ~passwd ~new_passwd : unit Lwt.t =
  call_service_xml `NoStore "changePasswd"
    ["userKey", !user_key; "passwd", hash_passwd passwd; "newPasswd", hash_passwd new_passwd]
    (fun _ _ -> Lwt.return ())

let change_email ~new_email : unit Lwt.t =
  call_service_xml `NoStore "changeEmail"
    ["userKey", !user_key; "newEmail", new_email]
    (fun _ _ -> Lwt.return ())

let unregister () : unit Lwt.t =
  call_service_xml `NoStore "unregister"
    ["userKey", !user_key]
    (fun _ _ -> Lwt.return ())

let visible_stores () : Xml.xml list Lwt.t =
  call_service_xml `NoStore "visibleStores"
    []
    (fun attrs children -> 
      Lwt.return children)
      
let removed_stores () : Xml.xml list Lwt.t =
  call_service_xml `NoStore "removedStores"
    []
    (fun attrs children ->
      Lwt.return children)

let create_store
    ~(default_role : string) ~(title : string) ~(list_user_role : (string * string) list) : string Lwt.t =
  call_service_xml `NoStore "createStore"
    ["defaultRole", default_role; "title", title; "listUserRole", string_of_list_user_role list_user_role]
    (fun attrs _ ->
      let store_name = try List.assoc "storeName" attrs with _ -> assert false in
      Lwt.return store_name)

let update_store
    ~(store_name : string) ~(default_role : string) ~(title : string)
    ~(list_user_role : (string * string) list) : unit Lwt.t =
  call_service_xml (`Store store_name) "updateStore"
    ["defaultRole", default_role; "title", title;
     "listUserRole", string_of_list_user_role list_user_role]
    (fun _ _ -> Lwt.return ())

let remove_store ~(store_name : string) : unit Lwt.t =
  call_service_xml (`Store store_name) "removeStore"
    []
    (fun _ _ -> Lwt.return ())

let store_namespace_of_prefix (store : string) ~(prefix : string) : string option Lwt.t =
  call_service_xml (`Store store) "storeNamespaceOfPrefix"
    ["prefix", prefix]
    (fun attrs _ ->
      let namespace = try List.assoc "namespace" attrs with _ -> assert false in
      Lwt.return (if namespace="" then None else Some namespace))

let define_namespace (store : string) ~(prefix : string) ~(uri : string) : unit Lwt.t =
  call_service_xml (`Store store) "defineNamespace"
    ["prefix",prefix; "uri",uri]
    (fun _ _ -> Lwt.return ())

let replace_object (store : string) ~(s : string) ~(p : string) ~(o : Rdf.t) : unit Lwt.t =
  call_service_xml (`Store store) "replaceObject"
    ["s",s; "p",p; "o", Rdf.to_xmlstring o]
    (fun _ _ -> Lwt.return ())

let import_rdf store (file : File.file Js.t) : unit Lwt.t =
  call_service_xml (`Store store) "importRdf"
    []
    ~file_args:["file", file]
    (fun _ _ -> Lwt.return ())

let export_rdf ~extension store : unit Lwt.t =
  call_service_file (`Store store) "exportRdf"
    ["extension", extension]
    (fun mime contents -> Jsutils.trigger_download ~mime contents; Lwt.return ())
    
(* producing places *)

type place = string

let get_new_place service store args : (place * Xml.xml) Lwt.t =
  call_service_xml (`Store store) service
    args
    (fun attrs children ->
      let place = try List.assoc "placeId" attrs with _ -> assert false in
      let xml_place = try List.hd children with _ -> assert false in
      Lwt.return (place,xml_place))

let get_place_root store =
  get_new_place "getPlaceRoot" store []
let get_place_home store =
  get_new_place "getPlaceHome" store []
let get_place_bookmarks store =
  get_new_place "getPlaceBookmarks" store []
let get_place_drafts store =
  get_new_place "getPlaceDrafts" store []
let get_place_uri store (uri : string) =
  get_new_place "getPlaceUri" store ["uri", uri]
let get_place_statement store (stat : string) =
  get_new_place "getPlaceStatement" store ["statement", stat]

let free_place place =
  call_service_xml `NoStore "freePlace"
    ["placeId", place]
    (fun _ _ -> Lwt.return ())

let change_focus store place focus : (place * Xml.xml) Lwt.t =
  get_new_place "changeFocus" store
    ["placeId", place; "focusId", focus]

let apply_transformation store place transf : (place * Xml.xml) Lwt.t =
  get_new_place "applyTransformation" store
    ["placeId", place; "transformation", transf]

let insert_increment store place increment : (place * Xml.xml) Lwt.t =
  get_new_place "insertIncrement" store
    ["placeId", place; "incrementId", increment]

let unquote_increment store place increment =
  get_new_place "unquoteIncrement" store
    ["placeId", place; "incrementId", increment]

let insert_plain_literal store place (text, lang) =
  get_new_place "insertPlainLiteral" store
    ["placeId", place; "text", text; "lang", lang]
let insert_typed_literal store place (text, datatype) =
  get_new_place "insertTypedLiteral" store
    ["placeId", place; "text", text; "datatype", datatype]
let insert_date store place (year, month, day) =
  get_new_place "insertDate" store
    ["placeId", place; "year", year; "month", month; "day", day]
let insert_dateTime store place (year, month, day, hours, minutes, seconds) =
  get_new_place "insertDateTime" store
    ["placeId", place; "year", year; "month", month; "day", day;
     "hours", hours; "minutes", minutes; "seconds", seconds]
let insert_var store place varname =
  get_new_place "insertVar" store
    ["placeId", place; "varname", varname]
let insert_uri store place uri =
  get_new_place "insertUri" store
    ["placeId", place; "uri", uri]
let insert_class store place uri =
  get_new_place "insertClass" store
    ["placeId", place; "uri", uri]
let insert_property store place uri =
  get_new_place "insertProperty" store
    ["placeId", place; "uri", uri]
let insert_inverse_property store place uri =
  get_new_place "insertInverseProperty" store
    ["placeId", place; "uri", uri]
let insert_structure store place (uri, arity, index) =
  get_new_place "insertStructure" store
    ["placeId", place; "uri", uri; "arity", string_of_int arity; "index", string_of_int index]

(* accessing the current place *)
    
let get_completions store place key : Xml.xml Lwt.t =
  call_service_xml (`Store store) "getCompletions"
    ["placeId", place; "matchingKey", key]
    (fun attrs children -> Lwt.return (try List.hd children with _ -> assert false))
    
(* modifying the current place *)

let modify_place service store place args : Xml.xml option Lwt.t =
  (* the returned boolean indicates whether the place has been modified *)
  call_service_xml (`Store store) service
    (("placeId", place) :: args)
    (fun attrs children ->
      let xml_opt =
	try Some (try List.hd children with _ -> assert false)
	with _ -> None in
      Lwt.return xml_opt)

let show_more store place =
  modify_place "showMore" store place []
let show_less store place =
  modify_place "showLess" store place []
let show_most store place =
  modify_place "showMost" store place []
let show_least store place =
  modify_place "showLeast" store place []

let page_down store place =
  modify_place "pageDown" store place []
let page_up store place =
  modify_place "pageUp" store place []
let page_top store place =
  modify_place "pageTop" store place []
let page_bottom store place =
  modify_place "pageBottom" store place []

let set_page_start store place pos =
  modify_place "setPageStart" store place ["pos", pos]
let set_page_end store place pos =
  modify_place "setPageEnd" store place ["pos", pos]

let move_column_left store place (column : string) =
  modify_place "moveColumnLeft" store place ["column", column]
let move_column_right store place (column: string) =
  modify_place "moveColumnRight" store place ["column", column]
let set_column_hidden store place (column : string) (hidden : string) =
  modify_place "setColumnHidden" store place
    ["column", column; "hidden", hidden]
let set_column_order store place (column : string) (order : string) =
  modify_place "setColumnOrder" store place
    ["column", column; "order", order]
let set_column_pattern store place (column : string) (pattern : string) =
  modify_place "setColumnPattern" store place
    ["column", column; "pattern", pattern]
let set_column_aggreg store place (column : string) (aggreg : string) =
  modify_place "setColumnAggreg" store place
    ["column", column; "aggreg", aggreg]

(* acting on the current place *)

let update_place service store place : (place * Xml.xml) Lwt.t =
  call_service_xml (`Store store) service
    ["placeId", place]
    (fun attrs children ->
      let place = try List.assoc "placeId" attrs with _ -> assert false in
      let xml_place = try List.hd children with _ -> assert false in
      Lwt.return (place,xml_place))

let do_run store place = update_place "doRun" store place


let record_place service store place : unit Lwt.t =
  call_service_xml (`Store store) service
    ["placeId", place]
    (fun attrs children -> Lwt.return ())

let set_as_home store place = record_place "setAsHome" store place
let add_as_bookmark store place = record_place "addAsBookmark" store place
let add_as_draft store place = record_place "addAsDraft" store place
let remove_as_draft store place = record_place "removeAsDraft" store place


let uri_of_label l = "#" ^ l (* TODO: take care of special characters, and declare RDFS labels *)

let entity_uri_of_label l = uri_of_label l
let class_uri_of_label l = uri_of_label l
let property_uri_of_label l = uri_of_label l
let functor_uri_of_label l = uri_of_label l
