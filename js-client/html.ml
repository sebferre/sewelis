
module Sewelis = Sewelis_api

exception Xml_error of string

open Sewelis_api.Xml

(* utils *)

let rec list_concat sep = function
  | [] -> []
  | [l] -> l
  | l::ll -> l @ sep @ list_concat sep ll

let class_mode mode = "mode-" ^ mode

let bs_context_of_mode = function
  | "root" -> "info"
  | "type" -> "warning"
  | "val" -> "success"
  | "relaxed" -> "danger"
  | _ -> "default"


let html_br = Element ("br", [], [])

let html_option ?title selected_value value label =
  Element ("option", (("value", value) ::
			 (if value=selected_value then ["selected", "selected"] else []) @
			 (match title with Some t -> ["title",t] | None -> [])),
	   [PCData label])

let html_label ~id label =
  Element ("label", ["for",id], [PCData label])
    
let html_input_text ?id ?classe ?(readonly = false) ?text ?size () =
  Element ("input",
	   ("type","text") ::
	     (match id with Some id -> ["id",id] | None -> []) @
	     (match classe with Some cl -> ["class",cl^" form-control"] | None -> ["class","form-control"]) @
	     (if readonly then ["readonly","true"] else []) @
	     (match text with Some t -> ["value",t] | None -> []) @
	     (match size with Some n -> ["size",string_of_int n] | None -> []),
	   [])

let html_glyphicon name = Element ("span", ["class","glyphicon glyphicon-"^name], [PCData ""]) (* void PCData necessary to avoid mysterious nesting changes by XML printer or browser *)
    
(* end utils *)


let html_select_role ?id ?role () =
  Element ("select", ("class","select-role form-control") :: (match id with None -> [] | Some id -> ["id",id]),
	   let sel = match role with Some r -> r | None -> "none" in
	   [html_option ~title:"cannot see the store" sel "none" "No right";
	    html_option ~title:"can explore all store contents" sel "reader" "Reader";
	    html_option ~title:"can add new contents" sel "publisher" "Publisher";
	    html_option ~title:"can add and remove any contents" sel "collaborator" "Collaborator";
	    html_option ~title:"can manage store configuration and access rights" sel "admin" "Admin"])
    
let html_user_role ?user ?role () =
  Element ("div", ["class","user-role row"],
	   [Element ("div", ["class","col-sm-2"], [html_input_text ~classe:"input-store-user" ?text:user (*~size:10*) ()]);
	    Element ("div", ["class","col-sm-2"], [html_select_role ?role ()])])

let html_add_user_role =
  Element ("button", ["class","add-user-role btn btn-default"],
	   [html_glyphicon "plus"])
						
let html_list_user_role ~id ?list_user_role () =
  let l = match list_user_role with Some l -> l | None -> [] in
  Element ("div", ["id",id],
	   List.map (fun (user,role) -> html_user_role ~user ~role ()) l @
	     [html_user_role (); html_add_user_role])
    
let html_store_config ~store_name ?title ?default_role ?list_user_role () =
  Element ("fieldset", ["id","form-config-"^store_name; "class","store-config"; "role","form"; "style","display:none"],
	   [Element ("div", ["class","form-group"],
		     [Element ("label", [], [PCData "Name:"]);
		      html_input_text ~classe:"input-store-name" ~readonly:true ~text:store_name (*~size:15*) ()]);
	    Element ("div", ["class","form-group"],
		     [Element ("label", [], [PCData "Title:"]);
		      html_input_text ~id:("title_" ^ store_name) ~classe:"input-store-title" ?text:title (*~size:50*) ()]);
	    Element ("div", ["class","form-group"],
		     [Element ("label", [], [PCData "Default role:"]);
		      html_select_role ~id:("default-role_" ^ store_name) ?role:default_role ()]);
	    Element ("div", ["class","form-group"],
		     [Element ("label", [], [PCData "Users (and their role):"]);
		      html_list_user_role ~id:("list-user-role_" ^ store_name) ?list_user_role ()]);
	    Element ("div", ["class","btn-group"],
		     [Element ("button", ["class","button-update-store-config btn btn-default"; "id", "update_" ^ store_name], [PCData "Save changes"]);
		      Element ("button", ["class","button-remove-store btn btn-danger"; "id", "remove_" ^ store_name], [PCData "Remove store"])])
	   ])
	     
let html_store_description = function
  | Element ("store", ("storeName", storeName) :: ("role", role) :: admin_attrs,
	     [PCData title])
    ->
    let config_opt =
      match admin_attrs with
      | ["defaultRole",default_role;
	 "listUserRole",s_list_user_role;
	 "removedStore",s_removed] ->
	Some
	  (html_store_config
	     ~store_name:storeName
	     ~title
	     ~default_role
	     ~list_user_role:(Sewelis_api.list_user_role_of_string s_list_user_role)
	     ())
      | [] -> None
      | _ -> raise (Xml_error "store/admin_attrs") in
    Element ("h4", [],
	     PCData title ::
	       PCData " " ::
	       Element ("button", ["id",storeName; "class","buttons-open-store btn btn-primary"; "title",title],
			[html_glyphicon "eye-open"; PCData " View"]) ::
	       ( match config_opt with
	       | Some _ -> PCData " " :: Element ("button", ["id","config-"^storeName; "class","buttons-config-store btn btn-default"],
						  [html_glyphicon "cog"; PCData " Configure"]) :: []
	       | None -> [] )) ::
      ( match config_opt with
      | Some config -> [config]
      | None -> [] )

  | _ -> raise (Xml_error "store")

let html_create_store =
  [Element ("fieldset", ["class","form-inline"; "role","form"],
	    [Element ("div", ["class","form-group"],
		      [Element ("label", ["for","input-store-creation"], [PCData "Title: "]);
		       html_input_text ~id:"input-store-creation" ()]);
	     PCData " ";
	     Element ("button", ["id","button-store-creation"; "class","btn btn-primary"],
		      [html_glyphicon "plus"; PCData " Create"])])]
    
let html_store_list l_store_description =
  Element ("ul", ["class", "list-group"],
	   List.map
	     (fun desc -> Element ("li", ["class","list-group-item"], html_store_description desc))
	     l_store_description
	   @ (if Sewelis.is_anonymous ()
	     then []
	     else [Element ("li", ["class","list-group-item"], html_create_store)]))

    
let html_id_of_focus foc = "focus" ^ foc
let focus_of_html_id id = String.sub id 5 (String.length id - 5)

let html_modifier ?(bs_context = "default") s =
  Element ("span", ["class", "modifier bg-" ^ bs_context], [PCData s])

let html_input ~id ~typ ~hint =
  Element ("input", ["id", id; "type", typ; "placeholder", hint], [])


let html_focus_control =
  Element ("fieldset", ["id", "focus-control"; "class","form-inline"],
	   [Element ("select", ["id", "focus-control-select"; "class","form-control"],
		     let sel = "entity" in
		     [html_option sel "entity" "entity";
		      html_option sel "class" "class";
		      html_option sel "property" "property";
		      html_option sel "inverseProperty" "inverse property";
		      html_option sel "plain" "text";
		      html_option sel "date" "date";
		      html_option sel "dateTime" "date/time";
		      html_option sel "time" "time";
		      html_option sel "integer" "integer";
		      html_option sel "decimal" "decimal";
		      html_option sel "double" "double";
		      html_option sel "boolean" "Boolean";
		      html_option sel "URI" "URI";
		      html_option sel "var" "variable";
		      html_option sel "structure" "structure"]);
	    Element ("span", [],
		     [Element ("input", ["id", "focus-control-input"; "type", "text"], []);
		      Element ("span", ["id", "completions"], [])]);
	    Element ("span", ["class","btn-group"],
		     [Element ("button", ["id", "focus-control-cut"; "class","btn btn-default"; "title","Erase focus"], [html_glyphicon "erase"]);
		      Element ("button", ["id", "focus-control-crop"; "class","btn btn-default"; "title","Crop focus"], [html_glyphicon "scissors"]);
		      Element ("button", ["id", "focus-control-up"; "class","btn btn-default"; "title","Widen focus"], [html_glyphicon "fullscreen"])]);
	    ])

let rec html_display ?(inline = false) ?(bs_context = "default") ?mode_id = function
  | Element ("display", [], l) ->
    List.fold_right (fun tok acc -> html_token ~inline ~bs_context ?mode_id  tok @ acc) l []
  | _ -> raise (Xml_error "display")
and html_token ~inline ~bs_context ?mode_id = function
  | Element ("Space", [], [])
    -> [PCData " "]
  | Element ("Kwd", [], cont)
    -> cont
  | Element ("Var", [], cont)
    -> [Element ("span", ["class", "var"], cont)]
  | Element ("URI", ["uri", uri; "kind", kind], cont)
    -> [Element ("span", ["class", kind ^ "URI"; "uri", uri], cont)]
  | Element ("URI", ["uri", uri; "kind", kind; "image", img_uri], cont)
    -> [Element ("span", ["class", kind ^ "URI"; "uri", uri],
		 Element("img", ["src",img_uri; "class","image-of-URI"], []) :: cont)]
  | Element ("Prim", [], cont)
    -> [Element ("span", ["class", "primitive"], cont)]
  | Element ("Plain", [], cont)
    -> [Element ("span", ["class", "plainLiteral"], cont)]
  | Element ("Plain", ["lang", lang], cont)
    -> [Element ("span", ["class", "plainLiteral"; "lang", lang], cont)]
  | Element ("Typed", ["uri", uri], cont)
    -> [Element ("span", ["class", "typedLiteral"; "datatype", uri], cont)]
  | Element ("Typed", ["uri", uri; "datatype", s_dt], cont)
    -> [Element ("span", ["class", "typedLiteral"; "datatype", uri], cont); PCData " ("; Element ("span", ["class", "datatype"], [PCData s_dt]); PCData ")"]
  | Element ("Xml", [], cont)
    -> [Element ("span", ["class", "XMLLiteral"], cont)]
  | Element ("List", [], l)
    -> [Element ("ol", ["class", "list"], List.map (fun x -> Element ("li", [], html_display ~inline ~bs_context ?mode_id x)) l)]
  | Element ("Tuple", [], l)
    -> [Element ("span", ["class", "tuple"], PCData "(" :: list_concat [PCData ", "] (List.map (html_display ~inline ~bs_context ?mode_id) l) @ [PCData ")"])]
  | Element ("Seq", [], l)
    -> if inline
      then List.concat (List.map (fun x -> html_display ~inline ~bs_context ?mode_id x @ [PCData ";"]) l)
      else [Element ("ul", ["class", "seq"], List.map (fun x -> Element ("li", [], html_display ~inline ~bs_context ?mode_id x @ [PCData ";"])) l)]
  | Element ("And", [], x::l)
    -> if inline
      then
	html_display ~inline ~bs_context ?mode_id x
	@ List.concat (List.map
			 (fun y -> PCData " and " :: html_display ~inline ~bs_context ?mode_id y)
			 l)
      else [Element ("ul", ["class", "coordAnd"],
		     Element ("li", [], html_display ~inline ~bs_context ?mode_id x) ::
		       List.map (fun y ->
			 Element ("li", [],
				  Element ("span", ["class", "bg-" ^ bs_context], [PCData "and "])
				  :: html_display ~inline ~bs_context ?mode_id y)
		   ) l)]
  | Element ("Or", [], x::l)
    -> if inline
      then
	html_display ~inline ~bs_context ?mode_id x
	@ List.concat (List.map
			 (fun y -> html_modifier " or " :: html_display ~inline ~bs_context ?mode_id y)
			 l)
      else [Element ("ul", ["class", "coordOr"],
		     Element ("li", [], html_display ~inline ~bs_context ?mode_id x) ::
		       List.map (fun y ->
			 Element ("li", [],
			      html_modifier ~bs_context "or " :: html_display ~inline ~bs_context ?mode_id y)
		   ) l)]
  | Element ("Not", [], [xml])
    -> html_modifier "not " :: html_display ~inline ~bs_context ?mode_id xml
  | Element ("Maybe", [], [xml])
    -> html_modifier "maybe " :: html_display ~inline ~bs_context ?mode_id xml
  | Element ("Brackets", [], [xml])
    -> if inline
      then PCData " " :: html_display ~inline ~bs_context ?mode_id xml
      else [Element ("div", [], html_display ~inline ~bs_context ?mode_id xml)]
  | Element ("Quote", [], [xml])
    -> html_modifier "`` " :: html_display ~inline ~bs_context ?mode_id xml @ [html_modifier " ''"]
  | Element ("Pair", ["forceIndent", force_indent], [xml1; xml2])
    -> if not inline && force_indent = "true"
      then
	html_display ~inline ~bs_context ?mode_id xml1 @
	  Element ("ul", ["class", "indent"], 
		   [Element ("li", [],
			     html_display ~inline ~bs_context ?mode_id xml2)]) ::
	  []
      else 
	html_display ~inline ~bs_context ?mode_id xml1 @
	  PCData " " ::
	  html_display ~inline ~bs_context ?mode_id xml2
  | Element ("Focus", ["id", foc], [xml]) ->
    ( match mode_id with
      | None -> html_display ~inline ~bs_context ?mode_id xml
      | Some (m,i) ->
	let focused_bs_context = bs_context_of_mode m in
	[Element ("span", ["class", (if i=foc then "focused bg-" ^ focused_bs_context else "focus bg-" ^ bs_context);
			   "id", html_id_of_focus foc],
		  if i=foc
		  then
		    html_display ~inline ~bs_context:focused_bs_context ?mode_id xml @
		      [PCData "  "; html_focus_control]
		  else html_display ~inline ~bs_context ?mode_id xml)] )
  | _ -> raise (Xml_error "token")

let html_focused_display ~mode = function
  | Element
      ("focusedDisplay",
       ["focusId", id],
       [display])
    -> html_display ~mode_id:(mode,id) display
  | _ -> raise (Xml_error "focusedDisplay")


let html_relaxation ~mode = function
  | Element
      ("relaxation",
       ["rank", rank;
	"hasMore", hasMore;
	"hasLess", hasLess],
       [])
    -> let control_less =
	 let disabled_attr = if hasLess = "true" then [] else ["disabled", "disabled"] in
	 [Element ("button", ("class", "showLeast btn btn-default btn-xs")::disabled_attr, [html_glyphicon "fast-backward"]);
	  Element ("button", ("class", "showLess btn btn-default btn-xs")::disabled_attr, [html_glyphicon "step-backward"])] in
       let control_more =
	 let disabled_attr = if hasMore = "true" then [] else ["disabled", "disabled"] in
	 [Element ("button", ("class", "showMore btn btn-default btn-xs")::disabled_attr, [html_glyphicon "step-forward"]);
	  Element ("button", ("class", "showMost btn btn-default btn-xs")::disabled_attr, [html_glyphicon "fast-forward"])] in
       Element ("span", ["class", "relaxation"; "title","relaxation rank"],
		  (*PCData "Relaxation rank: " ::*)
		  control_less @
		  Element ("span", ["class","bg-" ^ bs_context_of_mode mode],
			   [PCData (" " ^ rank ^ " ")]) ::
		  control_more)
  | _ -> raise (Xml_error "relaxation")

let html_statement ~mode = function
  | Element
      ("statement",
       [],
       [Element ("string", [], [PCData string]);
	focused_display])
    -> Element ("div", ["id", "statement panel panel-default"],
		[Element ("div", ["class","panel-body"],
			  html_focused_display ~mode focused_display)])
  | _ -> raise (Xml_error "statement")

let html_input_number ~id ?classe ~min ~max ~value () =
  Element ("input", ["id", id;
		     "class", (match classe with None -> "" | Some c -> c);
		     "type", "number";
		     "min", min;
		     "max", max;
		     "value", value],
	   [])

let html_paging = function
  | Element
      ("paging",
       ["count", count;
	"start", start;
	"end", ende;
	"size", size],
       [])
    -> let control_top_up =
	 [Element ("button", ["id", "pageTop"; "class","btn btn-default btn-sm"], [html_glyphicon "fast-backward"]);
	  Element ("button", ["id", "pageUp"; "class","btn btn-default btn-sm"], [html_glyphicon "step-backward"])] in
       let control_down_bottom =
	 [Element ("button", ["id", "pageDown"; "class","btn btn-default btn-sm"], [html_glyphicon "step-forward"]);
	  Element ("button", ["id", "pageBottom"; "class","btn btn-default btn-sm"], [html_glyphicon "fast-forward"])] in
       let input_start = html_input_number ~id:"input-page-start" ~classe:"input-count"
	 ~min:"1" ~max:count ~value:start () in
       let input_end = html_input_number ~id:"input-page-end" ~classe:"input-count"
	 ~min:"1" ~max:count ~value:ende () in
       Element ("div", ["id", "paging"],
		PCData ("Paging (per " ^ size ^ ") : ") ::
		  control_top_up @
		  input_start ::
		  PCData " - " ::
		  input_end ::
		  PCData (" out of " ^ count ^ " ") ::
		  control_down_bottom)
  | _ -> raise (Xml_error "paging")

let html_column (kind,pos,n) = function
  | Element
      ("column",
       ["name", name;
	"hidden", hidden;
	"order", order;
	"pattern", pattern;
	"aggreg", aggreg],
       [])
    -> let control_left =
	 Element ("button", ["column", name;
			     "class", "btn btn-default moveColumnLeft"] @ (if pos=1 then ["disabled","disabled"] else []),
		  [html_glyphicon "chevron-left"]) in
       let control_right =
	 Element ("button", ["column", name;
			     "class", "btn btn-default moveColumnRight"] @ (if pos=n then ["disabled","disabled"] else []),
		  [html_glyphicon "chevron-right"]) in
       let control_hidden =
	 Element ("select", ["column", name;
			     "class", "select-hidden form-control"],
		  [html_option hidden "false" "shown";
		   html_option hidden "true" "hidden"]) in
       let control_order =
	 Element ("select", ["column", name;
			     "class", "select-order form-control"],
		  [html_option order "default" "(order)";
		   html_option order "asc" "ascending";
		   html_option order "desc" "descending"]) in
       let control_aggreg =
	 Element ("select", ["column", name;
			     "class", "select-aggreg form-control"],
		  [html_option aggreg "index" "(aggreg)";
		   html_option aggreg "distinct_count" "count";
		   html_option aggreg "sum" "sum";
		   html_option aggreg "avg" "average"]) in
       let control_pattern =
	 Element ("input", ["column", name;
			    "class", "input-pattern form-control";
			    "type", "text";
			    "value", pattern],
		  []) in
       name,
       (if name="#"
	then Element ("th", [], [PCData "#"])
	else
	   Element ("th", [],
		    [Element ("div", ["class","row"],
			      [Element ("div", ["class","pull-left clearfix";
						"style","padding-left:15px"],
					[control_left]);
			       Element ("div", ["class","pull-right clearfix";
						"style","padding-right:15px"],
					[control_right]);
			       Element ("div", ["class","text-center"], [PCData name])]);
		     Element ("div", ["class","form-inline"],
			      [Element ("div", ["style","display:inline"],
					[control_hidden]);
			       ( match kind with
			       | `Dimension ->
				 Element ("div", ["style","display:inline"],
					  [control_order])
			       | `Measure ->
				 Element ("div", ["style","display:inline"],
					  [control_aggreg]) )]);
		     control_pattern]))
  (* TODO: include column attributes *)
  | _ -> raise (Xml_error "column")

let html_columns = function
  | Element ("columns", [], l)
    -> let n = List.length l in
       let children, _, _ =
	 List.fold_right
	   (fun col (children,pos,col_kind) ->
	     let col_name, html = html_column (col_kind,pos,n) col in
	     html :: children,
	     pos-1,
	     if col_name = "#" then `Dimension else col_kind)
	   l ([],n,`Measure) in
       Element ("tr", [], children)
  | _ -> raise (Xml_error "columns")

let html_cell = function
  | Element
      ("cell",
       ["cellId", id],
       [display])
    -> Element ("td", [], html_display display)
  | _ -> raise (Xml_error "cell")

let html_row = function
  | Element ("row", [], l)
    -> Element ("tr", [], List.map html_cell l)
  | _ -> raise (Xml_error "row")

let html_rows = function
  | Element ("rows", [], l)
    -> List.map html_row l
  | _ -> raise (Xml_error "rows")

let html_table columns rows =
  Element ("div", ["class","table-responsive"],
	   [Element ("table", ["class","table"],
		     html_columns columns ::
		       html_rows rows)])

let html_answers = function
  | Element
      ("answers",
       [],
       [paging;
	columns;
	rows]) ->
    Element ("div", ["id", "answers"],
	     [html_paging paging;
	      html_table columns rows])
  | _ -> raise (Xml_error "answers")

let html_id_of_increment incr = "increment" ^ incr
let increment_of_html_id id = String.sub id 9 (String.length id - 9)

let html_increment = function
  | Element
      ("increment",
       ["incrementId", id;
	"kind", kind;
	"ratioLeft", left;
	"ratioRight", right;
	"isNew", isNew;
	"uri", uri_opt;
	"isLisql", isLisql],
       [display;
	term_opt])
    -> 	let elt_increment =
	  Element ("span", ["class","increment-span"],
		   Element ("span", ["id", html_id_of_increment id;
				     "class", "increment" ^ if isNew="true" then " is-new" else ""],
			    html_display ~inline:true display)
		   :: (if left <> "1"
		     then [PCData "  "; Element ("span", ["class","badge"], [PCData left])]
		     else []) @
		     [Element ("span", ["class","increment-actions"],
			       (if isLisql = "true" then
				   [Element ("span", ["increment",id; "class","increment-action unquote-increment"], [html_glyphicon "new-window"])]
				else if uri_opt <> "none" then
				  [Element ("span", ["uri",uri_opt; "class","increment-action edit-uri-increment"], [html_glyphicon "edit"]);
				   Element ("span", ["uri",uri_opt; "class","increment-action open-uri-increment"], [html_glyphicon "new-window"])]
				else []))]) in
	id, elt_increment
  | _ -> raise (Xml_error "increment")

(* deprecated *)
(*
let rec html_increment_tree_aux = function
  | Element ("node", [], x::l) ->
    html_increment x ::
      (if l = []
       then []
       else
	  [Element ("ul", ["class", "incrementList"],
		    List.map
		      (fun subtree ->
			Element ("li", [],
				 html_increment_tree_aux subtree))
		      l)])
  | _ -> raise (Xml_error "increment_tree")
*)

let rec html_increment_tree_list l =
  if l = []
  then []
  else
    [Element ("ul", ["class", "incrementList"],
	      List.map
		(function
		  | Element ("node", [], x::l) ->
		    Element ("li", [],
			     let incr_id, incr_elt = html_increment x in
			     let check_id = "collapse" ^ incr_id in
			     if l=[]
			     then
			       Element ("label", ["style","visibility:hidden;"], [PCData "► "]) (* creating space *)
			       :: incr_elt
			       :: []
			     else
			       Element ("input", ["type","checkbox"; "id",check_id], [])
			       :: Element ("label", ["for",check_id; "class","label-checked"], [PCData "▼" (*html_glyphicon "triangle-bottom"*)])
			       :: Element ("label", ["for",check_id; "class","label-unchecked"], [PCData "► " (*html_glyphicon "triangle-right"*)])
			       :: incr_elt
			       :: html_increment_tree_list l)
		  | _ -> raise (Xml_error "increment_tree"))
		l)]

let html_increment_tree ~mode ~html_relaxation ~id ~title tree =
  match tree with
    | Element ("node", [], _root::subtrees)
      ->
      Element ("div", ["id", id;
		       "class", "panel panel-" ^ bs_context_of_mode mode],
	       [Element ("div", ["class","panel-heading"], [PCData title; html_relaxation]);
		Element ("div", ["class", "panel-body incrementTree css-treeview"],
			 if subtrees = []
			 then [PCData "<none>"]
			 else html_increment_tree_list subtrees)])
    | _ -> raise (Xml_error "increment_tree")

let html_id_of_transformation transf = "transf" ^ transf
let transformation_of_html_id id = String.sub id 6 (String.length id - 6)

let html_transformation = function
  | Element (transf, [], []) ->
    let label =
      match transf with
	| "FocusUp" -> "<move focus up>"
	| "FocusDown" -> "<move focus down>"
	| "FocusLeft" -> "<move focus left>"
	| "FocusRight" -> "<move focus right>"
	| "FocusTab" -> "<next focus>"
	| "InsertForEach" -> "for each _, _"
	| "InsertCond" -> "if _ then _ else _"
	| "InsertSeq" -> "_ ; _"
	| "InsertAnd" -> "_ and _"
	| "InsertOr" -> "_ or _"
	| "InsertAndNot" -> "_ and not _"
	| "InsertAndMaybe" -> "_ and maybe _"
	| "InsertIsThere" -> "_ is there"
	| "ToggleAn" -> "an _"
	| "ToggleThe" -> "the _"
	| "ToggleEvery" -> "every _"
	| "ToggleOnly" -> "only _"
	| "ToggleNo" -> "no _"
	| "ToggleEach" -> "each _"
	| "ToggleNot" -> "not _"
	| "ToggleMaybe" -> "maybe _"
	| "ToggleOpt" -> "opt _"
	| "ToggleTrans" -> "trans _"
	| "ToggleSym" -> "sym _"
	| "Describe" -> "<describe entity>"
	| "Select" -> "<select focus>"
	| "Delete" -> "<delete focus>"
	| _ -> raise (Xml_error "transformation") in
    Element ("span", ["id", html_id_of_transformation transf;
		      "class", "transformation"],
	     [PCData label])
  | _ -> raise (Xml_error "transformation")

let html_transformations ~mode ~html_relaxation = function
  | Element ("transformations", [], l) ->
    Element ("div", ["id", "transformations";
		     "class", "panel panel-" ^ bs_context_of_mode mode],
	     [Element ("div", ["class","panel-heading"], [PCData "Suggested transformations"; html_relaxation]);
	      Element ("div", ["class","panel-body transformationTree"],
		       [Element ("ul", ["class","transformationList"],
				 List.map
				   (fun t -> Element ("li", [],
						      [html_transformation t]))
				   l)])])
  | _ -> raise (Xml_error "transformations")

let html_suggestions ~mode relaxation = function
  | Element
      ("suggestions",
       ["canInsertEntity", canInsertEntity;
	"canInsertRelation", canInsertRelation],
       [increment_tree_entity;
	increment_tree_relation;
	transformations])
    ->
    let html_relaxation = html_relaxation ~mode relaxation in
    Element ("div", ["id", "suggestions"; "class","row"],
		[Element ("div", ["class","col-md-4"],
			  [html_increment_tree
			      ~mode
			      ~html_relaxation
			      ~id:"incrementTreeRelation"
			      ~title:"Suggested concepts and relations"
			      increment_tree_relation]);
		 Element ("div", ["class","col-md-4"],
			  [html_increment_tree
			      ~mode
			      ~html_relaxation
			      ~id:"incrementTreeEntity"
			      ~title:"Suggested entities and values"
			      increment_tree_entity]);
		 Element ("div", ["class","col-md-4"],
			  [html_transformations
			      ~mode
			      ~html_relaxation
			      transformations])])
  | _ -> raise (Xml_error "suggestions")

let html_place = function
  | Element
      ("place",
       ["placeId", id;
	"mode", mode],
       [relaxation;
	statement;
	answers;
	suggestions])
    -> Element ("div", ["id", "place"],
		[html_statement ~mode statement;
		 (*html_relaxation ~mode relaxation;*)
		 html_suggestions ~mode relaxation suggestions;
		 html_answers answers])
  | _ -> raise (Xml_error "place")


let html_completions = function
  | Element ("completions",
	     ["partial", partial;
	      "relaxed", relaxed],
	     incrs)
    ->
    let mode = if relaxed="true" then "relaxed" else "val" in
    Element ("div", ["class", "panel panel-" ^ bs_context_of_mode mode],
	     [Element ("div", ["class", "panel-body bg-" ^ bs_context_of_mode mode],
		       [Element ("ul", ["class", "incrementList"],
				 List.map (fun incr -> Element ("li", [], [snd (html_increment incr)])) incrs
				 @ (if partial="true" then [Element ("li", [], [PCData "..."])] else []))])])
  | _ -> raise (Xml_error "completions")
      
