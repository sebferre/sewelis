
module Sewelis = Sewelis_api
  
let rec matched_groups res n acc =
  if n = 0
  then
    match acc with
      | Some l -> Some (Regexp.matched_string res, l)
      | None -> None
  else
    match Regexp.matched_group res n, acc with
      | Some x, Some l -> matched_groups res (n-1) (Some (x::l))
      | _ -> None

let parse re n str =
  match Regexp.string_match re str 0 with
    | Some res -> matched_groups res n (Some [])
    | None -> None

exception Parse_error of string

(* URIs and labels *)

type uri_kind =
[ `Absolute of string
| `QName of string * string
| `Label of string ]

let uri_re_qname = Regexp.regexp "^([A-Z_a-z][-0-9A-Z_a-z]*:)([A-Z_a-z][-0-9A-Z_a-z]*)$"
let uri_re_absolute = Regexp.regexp "^[a-zA-Z][-+.a-zA-Z0-9]*:[^ ]+$" (* to check after qname *)

let uri_kind_of_string (str : string) : uri_kind =
  match parse uri_re_qname 2 str with
  | Some (s, [prefix;name]) -> `QName (prefix,name)
  | _ ->
    match parse uri_re_absolute 0 str with
    | Some (_,[]) -> `Absolute str
    | _ -> `Label str

let uri_re_forbidden_characters = Regexp.regexp "[ <>#%\"{}|\\^';/?:@&=+$,]+"
let relative_uri_of_label (label : string) : string =
  Regexp.global_replace uri_re_forbidden_characters label "_"

exception Cancel
    
let uri_edition store (str : string) (k : string -> 'a Lwt.t) : 'a Lwt.t =
  match uri_kind_of_string str with
  | `Absolute uri -> k uri
  | `QName (prefix,name) ->
    lwt ns_opt = Sewelis.store_namespace_of_prefix store ~prefix in
    ( match ns_opt with
    | Some ns -> k (ns ^ name)
    | None ->
      ( match Jsutils.prompt ("Please define the namespace of prefix " ^ prefix) "" with
      | Some ns ->
	lwt () = Sewelis.define_namespace store ~prefix ~uri:ns in
        k (ns ^ name)
      | None (* cancel *) -> Lwt.fail Cancel ) )
  | `Label label ->
    let rel_uri = relative_uri_of_label label in
    ( match Jsutils.prompt "The following string will be used as a relative URI (internal identifier).\nYou can change it if you like.\nYou will not see it in the interface." rel_uri with
    | Some rel_uri ->
      lwt () = Sewelis.replace_object store ~s:rel_uri ~p:"rdfs:label" ~o:(`Literal (label, `Plain "")) in
      k rel_uri
    | None (* cancel *) -> Lwt.fail Cancel )

(* xsd literal regexp *)

let xsd_re_boolean = "(true|false)"
let xsd_re_integer = "[+-]?[0-9]+"
let xsd_re_decimal = "[+-]?[0-9]+([.][0-9]*)?"
let xsd_re_double = "[+-]?[0-9]+([.][0-9]*)?([Ee][+-]?[0-9]+)?"
let xsd_re_duration = "-?P([0-9]+Y)?([0-9]+M)?([0-9]+D)?(T([0-9]+H)?([0-9]+M)?([0-9]+([.][0-9]+)?)?)?"
let xsd_re_timezone = "(Z|[+-][0-1][0-9]:[0-5][0-9])"
let xsd_re_timezone_opt = xsd_re_timezone ^ "?"
let xsd_re_time = "([0-1][0-9]|2[0-3]):([0-5][0-9]):([0-5][0-9]([.][0-9]+)?)"
let xsd_re_year = "(-?[0-9][0-9][0-9][0-9][0-9]*)"
let xsd_re_month = "(0[1-9]|1[0-2])"
let xsd_re_day = "(0[1-9]|[1-2][0-9]|3[0-1])"
let xsd_re_date = xsd_re_year ^ "-" ^ xsd_re_month ^ "-" ^ xsd_re_day
let xsd_re_dateTime = xsd_re_date ^ "T" ^ xsd_re_time
let xsd_re_gYearMonth = xsd_re_year ^ "-" ^ xsd_re_month
let xsd_re_gYear = xsd_re_year
let xsd_re_gMonthDay = "--" ^ xsd_re_month ^ "-" ^ xsd_re_day
let xsd_re_gDay = "---" ^ xsd_re_day
let xsd_re_gMonth = "--" ^ xsd_re_month

let parse_dt re_dt n extract error_msg =
  let re = Regexp.regexp ("^" ^ re_dt ^ "$") in
  fun value ->
    match parse re n value with
      | Some (s,l) -> extract s l
      | _ -> raise (Parse_error error_msg)

let xsd = "http://www.w3.org/2001/XMLSchema#"

let xsd_boolean = xsd ^ "boolean"
let pattern_boolean = "true/false"
let parse_boolean = parse_dt xsd_re_boolean 0 (fun s _ -> s) "Invalid boolean"

let xsd_integer = xsd ^ "integer"
let pattern_integer = "42"
let parse_integer = parse_dt xsd_re_integer 0 (fun s _ -> s) "Invalid integer"

let xsd_decimal = xsd ^ "decimal"
let pattern_decimal = "3.14"
let parse_decimal = parse_dt xsd_re_decimal 0 (fun s _ -> s) "Invalid decimal"

let xsd_double = xsd ^ "double"
let pattern_double = "-2.3e+4"
let parse_double = parse_dt xsd_re_double 0 (fun s _ -> s) "Invalid double"

let xsd_duration = xsd ^ "duration"
let pattern_duration = "P1Y2M3DT4H5M6.7"
let default_duration = "P0Y0M0DT0H0M0.0"
let parse_duration = parse_dt xsd_re_duration 0 (fun s _ -> s) "Invalid duration"

let xsd_date = xsd ^ "date"
let pattern_date = "yyyy-mm-dd"
let default_date () =
  let d = jsnew Js.date_now() in
  Printf.sprintf "%d-%02d-%02d"
    d##getFullYear() (d##getMonth()+1) (d##getDate())
let parse_date = parse_dt xsd_re_date 0 (fun s _ -> s) "Invalid date"

let xsd_time = xsd ^ "time"
let pattern_time = "hh:mm:ss"
let default_time () =
  let d = jsnew Js.date_now() in
  Printf.sprintf "%02d:%02d:%02d"
    d##getHours() d##getMinutes() d##getSeconds()
let parse_time = parse_dt xsd_re_time 0 (fun s _ -> s) "Invalid time"

let xsd_dateTime = xsd ^ "dateTime"
let pattern_dateTime = "yyyy-mm-ddThh:mm:ss+01:00"
let default_dateTime () =
  let d = jsnew Js.date_now() in
  let tz = - d##getTimezoneOffset() in
  let tz_abs, tz_sign = abs tz, if tz < 0 then "-" else "+" in
  let tz_hours, tz_minutes = tz_abs / 60, tz_abs mod 60 in
  Printf.sprintf "%d-%02d-%02dT%02d:%02d:%02d%s%02d:%02d"
    d##getFullYear() (d##getMonth()+1) d##getDate()
    d##getHours() d##getMinutes() d##getSeconds()
    tz_sign tz_hours tz_minutes
let parse_dateTime = parse_dt (xsd_re_dateTime ^ xsd_re_timezone) 0 (fun s _ -> s) "Invalid date/time"

