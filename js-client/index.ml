
open Js
open Lwt_js_events
open Jsutils

module Sewelis = Sewelis_api
module Xml = Sewelis.Xml
  
type navigation_state =
    { store : string;
      place : string;
      content : Xml.xml }

class history store =
object (self)
  val mutable bwd_states : navigation_state list = []
  val mutable fwd_states : navigation_state list = []

  method private current =
    try List.hd bwd_states
    with _ -> failwith "No current navigation state"

  method place = self#current.place
  method content = self#current.content

  method push place content =
    lwt _ = Lwt_list.iter_p (fun state -> Sewelis.free_place state.place) fwd_states in
    fwd_states <- [];
    bwd_states <- {store; place; content} :: bwd_states;
    Lwt.return ()
  method back =
    match bwd_states with
      | s1::s2::l -> bwd_states <- s2::l; fwd_states <- s1::fwd_states
      | _ -> ()
  method forward =
    match fwd_states with
      | s1::l -> bwd_states <- s1::bwd_states; fwd_states <- l
      | _ -> ()

  method reset =
    lwt _ = Lwt_list.iter_p (fun state -> Sewelis.free_place state.place) fwd_states in
    fwd_states <- [];
    lwt _ = Lwt_list.iter_p (fun state -> Sewelis.free_place state.place) bwd_states in
    bwd_states <- [];
    Lwt.return ()
end

exception No_open_store
  
let histories =
object (self)
  val mutable store_hist : (Sewelis.store * history) list = []
  val mutable current_store : Sewelis.store = ""

  method store =
    if current_store = "" then raise No_open_store
    else current_store

  method private current =
    if current_store = "" then raise No_open_store
    else
      try List.assoc current_store store_hist
      with _ -> assert false

  method goto_store store : bool =
    let is_open = List.mem_assoc store store_hist in
    if not is_open then
      store_hist <- (store, new history store) :: store_hist;
    current_store <- store;
    is_open

  method place = self#current#place
  method content = self#current#content

  method push place content = self#current#push place content
  method back = self#current#back
  method forward = self#current#forward

  method reset =
    lwt _ = Lwt_list.iter_p (fun (store,history) -> history#reset) store_hist in
    store_hist <- [];
    current_store <- "";
    Lwt.return ()
end
  
open Html

let string_html (html : Xml.xml) =
  string (Xml.to_string html)

let lwt_opt opt f =
  match opt with
    | Some x -> f x
    | None -> Lwt.return ()

let rec refresh_answers_handlers store place : unit =
  let pageX selector func =
    jquery selector (onclick (fun elt ev ->
      firebug ("clicked " ^ selector);
      lwt xml_ans_opt = func store place in
      lwt_opt xml_ans_opt (fun xml ->
	refresh_answers store place xml))) in
  pageX "#pageTop" Sewelis.page_top;
  pageX "#pageUp" Sewelis.page_up;
  pageX "#pageDown" Sewelis.page_down;
  pageX "#pageBottom" Sewelis.page_bottom;

  let set_pageX selector func =
    jquery_input selector (onchange (fun input ev ->
      firebug ("clicked " ^ selector);
      lwt xml_ans_opt = func store place (to_string input##value) in
      lwt_opt xml_ans_opt (fun xml ->
	refresh_answers store place xml))) in
  set_pageX "#input-page-start" Sewelis.set_page_start;
  set_pageX "#input-page-end" Sewelis.set_page_end;

  let move_columnX selector func =
    jquery_all selector Dom_html.CoerceTo.element (onclick (fun elt ev ->
      case_attribute "column" elt
	(fun () -> Lwt.return ())
	(fun column ->
	  firebug ("clicked " ^ column ^ selector);
	  lwt xml_ans_opt = func store place column in
	  lwt_opt xml_ans_opt (fun xml ->
            refresh_answers store place xml)))) in
  move_columnX ".moveColumnLeft" Sewelis.move_column_left;
  move_columnX ".moveColumnRight" Sewelis.move_column_right;

  let set_columnX selector func =
    jquery_all selector Dom_html.CoerceTo.select
      (onchange (fun input ev ->
	case_attribute "column" input
	  (fun () -> Lwt.return ())
	  (fun column ->
	    let option = to_string input##value in
	    lwt xml_ans_opt = func store place column option in
	    lwt_opt xml_ans_opt (fun xml ->
	      refresh_answers store place xml)))) in
  set_columnX ".select-hidden" Sewelis.set_column_hidden;
  set_columnX ".select-order" Sewelis.set_column_order;
  set_columnX ".select-aggreg" Sewelis.set_column_aggreg;

  jquery_all ".input-pattern" Dom_html.CoerceTo.input
    (onchange (fun input ev ->
      case_attribute "column" input
	(fun () -> Lwt.return ())
	(fun column ->
	  let option = to_string input##value in
	  lwt xml_ans_opt = Sewelis.set_column_pattern store place column option in
	  lwt_opt xml_ans_opt (fun xml ->
	    refresh_answers store place xml))));
      
  ()

and refresh_answers store place xml : unit Lwt.t =
  firebug "refresh_answers";
  let html =
    try html_answers xml
    with exn -> firebug (Printexc.to_string exn); raise exn in
  match html with
    | Xml.Element (_, _, l) ->
      jquery "#answers" (fun elt ->
	let s_html = String.concat "" (List.map Xml.to_string l) in
	elt##innerHTML <- string s_html);
      refresh_answers_handlers store place;
      Lwt.return ()
    | _ -> Lwt.return () (* TODO: handle error? *)
(* TODO: put new answers in navigation state *)

let clear_completions () : unit Lwt.t =
  jquery "#completions" (fun elt ->
    elt##innerHTML <- string "";
    elt##style##display <- string "none");
  Lwt.return ()

let rec refresh_completions store place xml : unit Lwt.t =
  jquery "#completions" (fun elt ->
    elt##innerHTML <- string (Xml.to_string (html_completions xml));
    elt##style##display <- string "block";
    jquery_all_from elt ".increment" Dom_html.CoerceTo.element (onclick (fun elt ev ->
      firebug ("clicked " ^ to_string elt##id);
      let increment = increment_of_html_id (to_string elt##id) in
      lwt new_place = Sewelis.insert_increment store place increment in
      refresh_place ~push:true store new_place)));
  Lwt.return ()

and refresh_place ?(push = false) store (place, xml) : unit Lwt.t =
  firebug "refresh_place";
  (* defining HTML content *)
  let html =
    try html_place xml
    with exn -> firebug (Printexc.to_string exn); raise exn in
  jquery "#content" (fun elt ->
    let s_html = Xml.to_string html in
    elt##innerHTML <- string s_html (*string_html (html_place xml)*) );

  (* modifying the current place*)
  let showX selector func =
    jquery_all selector Dom_html.CoerceTo.element (onclick (fun elt ev ->
      firebug ("clicked " ^ selector);
      lwt xml_place_opt = func store place in
      lwt_opt xml_place_opt (fun xml ->
	refresh_place store (place, xml)))) in
  showX ".showLeast" Sewelis.show_least;
  showX ".showLess" Sewelis.show_less;
  showX ".showMore" Sewelis.show_more;
  showX ".showMost" Sewelis.show_most;

  refresh_answers_handlers store place;

  (* focus controls *)
  jquery "#focus-control-cut" (onclick (fun elt ev ->
    firebug "clicked Cut";
    Dom_html.stopPropagation ev;
    lwt new_place = Sewelis.apply_transformation store place "Delete" in
    refresh_place ~push:true store new_place));
  jquery "#focus-control-crop" (onclick (fun elt ev ->
    Dom_html.stopPropagation ev;
    lwt new_place = Sewelis.apply_transformation store place "Select" in
    refresh_place ~push:true store new_place));
  jquery "#focus-control-up" (onclick (fun elt ev ->
    Dom_html.stopPropagation ev;
    lwt new_place = Sewelis.apply_transformation store place "FocusUp" in
    refresh_place ~push:true store new_place));
  jquery_select "#focus-control-select" (onchange (fun select ev ->
    Dom_html.stopPropagation ev;
    let placeholder, default =
      match to_string select##value with
	| "date" -> Input.pattern_date, Input.default_date ()
	| "time" -> Input.pattern_time, Input.default_time ()
	| "dateTime" -> Input.pattern_dateTime, Input.default_dateTime ()
	| "integer" -> Input.pattern_integer, ""
	| "decimal" -> Input.pattern_decimal, ""
	| "double" -> Input.pattern_double, ""
	| "boolean" -> Input.pattern_boolean, ""
	| "var" -> "X", ""
	| "URI" -> "http://", "http://"
	| "entity" -> "", ""
	| "class" -> "", ""
	| "property" -> "", ""
	| "inverseProperty" -> "", ""
	| "structure" -> "f(,)", ""
	| _ -> "", "" in
    jquery_input "#focus-control-input" (fun elt ->
      elt##setAttribute(string "placeholder", string placeholder);
      elt##value <- string default
    )));
  jquery_input "#focus-control-input" (onkeypress (fun input ev ->
    if ev##keyCode = 13 then begin
      Dom_html.stopPropagation ev;
      let value = to_string input##value in
      let kind = ref "" in
      jquery_select "#focus-control-select" (fun select ->
	kind := to_string select##value);
      try_lwt
	lwt new_place =
	  match !kind with
	    | "plain" -> Sewelis.insert_plain_literal store place
	      (value, "en")
	    | "date" -> Sewelis.insert_typed_literal store place
	      (Input.parse_date value, Input.xsd_date)
	    | "time" -> Sewelis.insert_typed_literal store place
	      (Input.parse_time value, Input.xsd_time)
	    | "dateTime" -> Sewelis.insert_typed_literal store place
	      (Input.parse_dateTime value, Input.xsd_dateTime)
	    | "integer" -> Sewelis.insert_typed_literal store place
	      (Input.parse_integer value, Input.xsd_integer)
	    | "decimal" -> Sewelis.insert_typed_literal store place
	      (Input.parse_decimal value, Input.xsd_decimal)
	    | "double" -> Sewelis.insert_typed_literal store place
	      (Input.parse_double value, Input.xsd_double)
	    | "boolean" -> Sewelis.insert_typed_literal store place
	      (Input.parse_boolean value, Input.xsd_boolean)
	    | "var" -> Sewelis.insert_var store place value
	    | "URI" -> Sewelis.insert_uri store place value
	    | "entity" -> Input.uri_edition store value (fun uri -> Sewelis.insert_uri store place uri)
	    | "class" -> Input.uri_edition store value (fun uri -> Sewelis.insert_class store place uri)
	    | "property" -> Input.uri_edition store value (fun uri -> Sewelis.insert_property store place uri)
	    | "inverseProperty" -> Input.uri_edition store value (fun uri -> Sewelis.insert_inverse_property store place uri)
	    | "structure" ->
	      ( match Regexp.string_match (Regexp.regexp "([^(]+)[(]([,]*)[)]") value 0 with
	      | None -> Lwt.fail (Failure "Wrong format: ex. cos(), add(,), cond(,,)")
	      | Some result ->
		( match Regexp.matched_group result 1, Regexp.matched_group result 2 with
		| Some label, Some commas ->
		  Input.uri_edition store label (fun uri ->
		    let arity = 1 + String.length commas in
		    let index = 0 in
		    Sewelis.insert_structure store place (uri, arity, index))
		| _ -> Lwt.fail (Failure "Unexpected failure at structure input") ) )
	    | _ -> Lwt.fail (Failure "Unknown kind of input") in
	 refresh_place ~push:true store new_place
       with
       | Input.Cancel -> Lwt.return ()
       | Input.Parse_error msg -> alert msg; Lwt.return ()
       | Sewelis.Sewelis_error -> Lwt.return ()
       | exn -> alert (Printexc.to_string exn); Lwt.return ()
    end
    else begin
      Dom_html.stopPropagation ev;
      let value = to_string input##value in
      if value = ""
      then clear_completions ()
      else
	lwt xml = Sewelis.get_completions store place value in
	refresh_completions store place xml
    end));

  (* navigation links to other places *)
  jquery ".focused" (onclick (fun elt ev ->
    Dom_html.stopPropagation ev));
  jquery_all ".focus" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    let id = to_string elt##id in
    firebug ("clicked " ^ id);
    Dom_html.stopPropagation ev;
    let focus = focus_of_html_id id in
    lwt new_place = Sewelis.change_focus store place focus in
    refresh_place store new_place));
  jquery_all ".transformation" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    firebug ("clicked " ^ to_string elt##id);
    let transf = transformation_of_html_id (to_string elt##id) in
    lwt new_place = Sewelis.apply_transformation store place transf in
    refresh_place ~push:true store new_place));
  jquery_all ".increment" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    firebug ("clicked " ^ to_string elt##id);
    let increment = increment_of_html_id (to_string elt##id) in
    lwt new_place = Sewelis.insert_increment store place increment in
    refresh_place ~push:true store new_place));
  jquery_all ".open-uri-increment" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    case_attribute "uri" elt
      (fun () -> Lwt.return ())
      (fun uri ->
	let _window = Dom_html.window##open_(string uri, string "_blank", null) in
	Lwt.return ())));
  jquery_all ".edit-uri-increment" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    case_attribute "uri" elt
      (fun () -> Lwt.return ())
      (fun uri ->
	lwt new_place = Sewelis.get_place_uri store uri in
        refresh_place ~push:true store new_place)));
  jquery_all ".unquote-increment" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    case_attribute "increment" elt
      (fun () -> Lwt.return ())
      (fun increment ->
	lwt new_place = Sewelis.unquote_increment store place increment in
        refresh_place ~push:true store new_place)));

  (* the end *)
  if push
  then histories#push place xml
  else Lwt.return ()

let ui_mode : [`List | `View] ref = ref `List
    
let show_store_list () =
  ui_mode := `List;
  jquery_show "#store-list";
  jquery_hide "#store-view";
  jquery "#goto-store-list" (fun elt -> elt##className <- string "active");
  jquery "#current-store" (fun elt -> elt##className <- string "")
    
let show_store_view () =
  ui_mode := `View;
  jquery_hide "#store-list";
  jquery_show "#store-view";
  jquery "#goto-store-list" (fun elt -> elt##className <- string "");
  jquery "#current-store" (fun elt -> elt##className <- string "active")

let modal_in () =
  jquery_hide (match !ui_mode with `List -> "#store-list" | `View -> "#store-view")
let modal_out () =
  jquery_show (match !ui_mode with `List -> "#store-list" | `View -> "#store-view")


let refresh_store_view store : unit Lwt.t =
  firebug "refresh_store_view";
  jquery "#store-view" (fun elt -> elt##style##display <- string "block");
  let is_open = histories#goto_store store in
  if is_open then
    refresh_place ~push:false store (histories#place, histories#content)
  else
    lwt place, xml = Sewelis.get_place_home store in
    refresh_place ~push:true store (place,xml)
  
let rec refresh_store_list () : unit Lwt.t =
  firebug "refresh_store_list";
  lwt xml_visible = Sewelis.visible_stores () in

  let html =
    try html_store_list xml_visible
    with exn -> firebug (Printexc.to_string exn); raise exn in
  jquery "#store-list" (fun elt ->
    let s_html = Xml.to_string html in
    elt##innerHTML <- string s_html);

  (* navigation links to stores *)
  jquery_all ".buttons-open-store" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    let store = to_string elt##id in
    let title = to_string elt##title in
    firebug ("opening " ^ store);
    jquery_set_innerHTML "#current-store" ("<a>" ^ title ^ "</a>");
    show_store_view ();
    refresh_store_view store));
  jquery_all ".buttons-config-store" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    let config_store = to_string elt##id in
    jquery_toggle ("#form-" ^ config_store)));
  
  jquery_all ".add-user-role" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    Opt.iter ((elt :> Dom.node t)##previousSibling) (fun node_prev -> (* getting the void span before *)
      Opt.iter (Dom_html.CoerceTo.element node_prev) (fun elt_prev ->
	elt_prev##outerHTML <- string (Xml.to_string (html_user_role ()) ^ to_string elt_prev##outerHTML)))));

  jquery_all ".button-update-store-config" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    let store_name = remove_prefix "update_" (to_string elt##id) in
    firebug ("updating store config: " ^ store_name);
    jquery_input ("#title_" ^ store_name) (fun input_title ->
      jquery_select ("#default-role_" ^ store_name) (fun select_default_role ->
	jquery ("#list-user-role_" ^ store_name) (fun elt_list_user_role ->
	  ignore (
	    let title = to_string input_title##value in
	    let default_role = to_string select_default_role##value in
	    let ref_list_user_role = ref [] in (* TODO *)
	    jquery_all_from elt_list_user_role ".user-role" Dom_html.CoerceTo.element (fun elt_li ->
	      jquery_input_from elt_li ".input-store-user" (fun input_user ->
		jquery_select_from elt_li ".select-role" (fun select_role ->
		  let user = to_string input_user##value in
		  let role = to_string select_role##value in
		  if user <> "" && role <> "none" then
		    ref_list_user_role := (user,role) :: !ref_list_user_role)));
	    let list_user_role = List.rev !ref_list_user_role in
	    lwt () = Sewelis.update_store ~store_name ~title ~default_role ~list_user_role in
	    refresh_store_list ()))))));

  jquery_all ".button-remove-store" Dom_html.CoerceTo.element (onclick (fun elt ev ->
    if confirm "Are you sure that you want to remove this store?"
    then ignore (
      let store_name = remove_prefix "remove_" (to_string elt##id) in
      lwt () = Sewelis.remove_store ~store_name in
      refresh_store_list ())));

  if not (Sewelis.is_anonymous ()) then
    jquery "#button-store-creation" (onclick (fun elt ev ->
      jquery_input "#input-store-creation" (fun input ->
	let title = to_string input##value in
	ignore
	  (try_lwt
	     lwt store_name = Sewelis.create_store
	      ~default_role:"none" ~title ~list_user_role:[] in
	     input##value <- string "";
	     refresh_store_list ()
	   with
	   | Sewelis.Sewelis_error -> Lwt.return ()
	   | exn -> alert (Printexc.to_string exn); Lwt.return ()))));

  Lwt.return ()

let reset () : unit Lwt.t =
  lwt _ = histories#reset in
  jquery_set_innerHTML "#current-store" "";
  jquery_set_innerHTML "#content" "";
  show_store_list ();
  refresh_store_list ()

let rec reset_login () =
  jquery_show "#link-register";
  jquery_show "#link-login";
  jquery_hide "#dropdown-account";
  jquery "#link-login" (onclick (fun elt ev -> modal_in (); jquery_show "#form-login"));
  jquery "#button-cancel-login" (onclick (fun elt ev -> jquery_hide "#form-login"; modal_out ()));
  jquery "#button-login" (onclick (fun elt ev ->
    let login = jquery_input_value "#user-login" in
    let passwd = jquery_input_value "#user-passwd" in
    if login = "" then alert "Please, enter a login and password"
    else if passwd = "" then alert "Please, enter a password"
    else ignore (
      firebug "before login...";
      lwt () = Sewelis.login ~login ~passwd in
      firebug "after login";
      jquery_hide "#form-login";
      modal_out ();
      firebug "before reset_logout";
      reset_logout ())));
  jquery_input "#user-passwd" (onenter (fun elt ev -> jquery_click "#button-login"));

  jquery "#link-register" (onclick (fun elt ev -> modal_in (); jquery_show "#form-register"));
  jquery "#button-cancel-register" (onclick (fun elt ev -> jquery_hide "#form-register"; modal_out ()));
  jquery "#button-register" (onclick (fun _ _ ->
    let login = jquery_input_value "#register-login" in
    let passwd = jquery_input_value "#register-passwd" in
    let passwd_bis = jquery_input_value "#register-passwd-bis" in
    let email = jquery_input_value "#register-email" in
    let error_msg =
      if login = "" then "Please, enter a username"
      else if passwd = "" then "Please, enter a password"
      else if passwd_bis <> passwd then "Error: the two passwords are different!"
      else if email = "" then "Please, enter an email address"
      else "" in
    if error_msg <> ""
    then alert error_msg
    else ignore (
      lwt () = Sewelis.register ~login ~passwd ~email in
      lwt () = Sewelis.login ~login ~passwd in
      jquery_hide "#form-register";
      modal_out ();
      reset_logout ())));

  reset ()

and reset_logout () =
  jquery_hide "#link-register";
  jquery_hide "#link-login";
  jquery_show "#dropdown-account";
  jquery "#link-logout" (onclick (fun elt ev ->
    lwt () = Sewelis.logout () in
    reset_login ()));

  jquery "#link-change-passwd" (onclick (fun elt ev -> modal_in (); jquery_show "#form-change-passwd"));
  jquery "#button-cancel-change-passwd" (onclick (fun elt ev -> jquery_hide "#form-change-passwd"; modal_out ()));
  jquery "#button-change-passwd" (onclick (fun elt ev ->
    let passwd = jquery_input_value "#change-passwd-passwd" in
    let new_passwd = jquery_input_value "#change-passwd-new-passwd" in
    let new_passwd_bis = jquery_input_value "#change-passwd-new-passwd-bis" in
    if passwd = "" then alert "Please, enter the current password"
    else if new_passwd = "" then alert "Please, enter the new password"
    else if new_passwd_bis <> new_passwd then alert "Error: the two new passwords are different !"
    else ignore (
      lwt () = Sewelis.change_passwd ~passwd ~new_passwd in
      jquery_hide "#form-change-passwd";
      modal_out ();
      reset_logout ())));

  jquery "#link-change-email" (onclick (fun elt ev -> modal_in (); jquery_show "#form-change-email"));
  jquery "#button-cancel-change-email" (onclick (fun elt ev -> jquery_hide "#form-change-email"; modal_out ()));
  jquery "#button-change-email" (onclick (fun elt ev ->
    let new_email = jquery_input_value "#change-email-new-email" in
    if new_email = "" then alert "Please, enter the new email"
    else ignore (
      lwt () = Sewelis.change_email ~new_email in
      jquery_hide "#form-change-email";
      modal_out ();
      reset_logout ())));

  reset ()

    
let main () =
  jquery "#goto-store-list" (onclick (fun elt ev -> show_store_list ()));
  jquery "#current-store" (onclick (fun elt ev -> show_store_view ()));

  jquery "#button-back" (onclick (fun elt ev ->
    histories#back;
    refresh_place ~push:false histories#store (histories#place, histories#content)));
  jquery "#button-forward" (onclick (fun elt ev ->
    histories#forward;
    refresh_place ~push:false histories#store (histories#place, histories#content)));
(*
  onkeypress
    (fun elt ev ->
      let transf =
	match to_bool (ev##ctrlKey), (ev##keyCode) with
	  | true, 9 (* Tab *) -> "FocusTab"
	  | true, 37 (* Left *) -> "FocusLeft"
	  | true, 39 (* Right *) -> "FocusRight"
	  | true, 38 (* Up *) -> "FocusUp"
	  | true, 40 (* Down *) -> "FocusDown"
	  | _ -> "" in
      if transf <> ""
      then
	  lwt new_place = Sewelis.apply_transformation histories#store histories#place transf in
	  refresh_place histories#store new_place
      else Lwt.return ())
    Dom_html.document;
*)
  
  let goto_place sel func =
    jquery sel (onclick (fun elt ev ->
      lwt new_place = func histories#store in
      refresh_place ~push:true histories#store new_place)) in
  goto_place "#button-root" Sewelis.get_place_root;
  goto_place "#button-home" Sewelis.get_place_home;
  goto_place "#button-bookmarks" Sewelis.get_place_bookmarks;
  goto_place "#button-drafts" Sewelis.get_place_drafts;

  let update_place sel func =
    jquery sel (onclick (fun elt ev ->
      let store = histories#store in
      let place = histories#place in
      lwt new_place = func store place in
      refresh_place ~push:true store new_place)) in
  update_place "#button-run" Sewelis.do_run;

  jquery "#button-export-rdf-nt" (onclick (fun elt ev -> Sewelis.export_rdf ~extension:"nt" histories#store));
  jquery "#button-export-rdf-ttl" (onclick (fun elt ev -> Sewelis.export_rdf ~extension:"ttl" histories#store));
  jquery "#button-export-rdf-log" (onclick (fun elt ev -> Sewelis.export_rdf ~extension:"log" histories#store));

  jquery_input "#input-upload-rdf" (onchange (fun input ev ->
      match Optdef.to_option input##files with
      | Some files -> ignore
	(try_lwt
	 let l_file = Dom.list_of_nodeList files in
	 lwt _ =
	     Lwt_list.iter_p (fun file ->
	       let filename = to_string file##name in
	       if List.exists (Filename.check_suffix filename) [".xml"; ".rdf"; ".owl"; ".nt"; ".ttl"]
	       then Sewelis.import_rdf histories#store file
	       else Lwt.fail (Failure (filename ^ " has not the expected extension"))
	     ) l_file in
	 lwt _ = refresh_place ~push:false histories#store (histories#place, histories#content) in
	 alert "Upload is complete!";
	 Lwt.return ()
					with
					| Failure msg -> alert msg; Lwt.return ()
					| Sewelis.Sewelis_error -> Lwt.return ()
					| exn -> alert (Printexc.to_string exn); Lwt.return ())
      | None -> alert "Please, select the files to be uploaded"));
  
  let record_place sel func =
    jquery sel (onclick (fun elt ev ->
      func histories#store histories#place)) in
  record_place "#button-set-home" Sewelis.set_as_home;
  record_place "#button-add-bookmark" Sewelis.add_as_bookmark;
  record_place "#button-add-draft" Sewelis.add_as_draft;
  record_place "#button-remove-draft" Sewelis.remove_as_draft;

  lwt msg = Sewelis.ping () in
  reset_login ()

let _ = (* main *)
  firebug "Starting Sewelis";
  Dom_html.window##onload <- Dom.handler (fun ev ->
    ignore (main ());
    bool true)
