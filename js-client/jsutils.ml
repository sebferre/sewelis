
open Js
open XmlHttpRequest

let alert msg = Dom_html.window##alert(string msg)

let confirm msg = to_bool (Dom_html.window##confirm(string msg))
  
let prompt msg text : string option =
  Opt.case (Dom_html.window##prompt(string msg, string text))
    (fun () -> None)
    (fun jstring -> Some (to_string jstring))

let trigger_download ~mime contents : unit =
  let data_url = "data:" ^ mime ^ "," ^ Url.urlencode contents in
  Dom_html.window##location##assign(string data_url)
    
let firebug msg = Firebug.console##log(string msg)

let case_attribute (attr : string) elt (none : unit -> 'a) (some : string -> 'a) : 'a =
  Opt.case (elt##getAttribute(string attr))
    none
    (fun v -> some (to_string v))

let jquery_from (root : #Dom_html.nodeSelector Js.t) s k =
  Opt.iter (root##querySelector(string s)) (fun elt ->
    k elt)
let jquery s k = jquery_from Dom_html.document s k

let jquery_input_from (root : #Dom_html.nodeSelector Js.t) s k =
  Opt.iter (root##querySelector(string s)) (fun elt ->
    Opt.iter (Dom_html.CoerceTo.input elt) (fun input ->
      k input))
let jquery_input s k = jquery_input_from Dom_html.document s k

let jquery_select_from (root : #Dom_html.nodeSelector Js.t) s k =
  Opt.iter (root##querySelector(string s)) (fun elt ->
    Opt.iter (Dom_html.CoerceTo.select elt) (fun select ->
      k select))
let jquery_select s k = jquery_select_from Dom_html.document s k

let jquery_all_from (root : #Dom_html.nodeSelector Js.t) s coerce k =
  let nodelist = root##querySelectorAll(string s) in
  let n = nodelist##length in
  for i=0 to n-1 do
    Opt.iter nodelist##item(i) (fun elt ->
      Opt.iter (coerce elt) (fun coerced_elt ->
	k coerced_elt))
  done
let jquery_all s coerce k = jquery_all_from Dom_html.document s coerce k

let jquery_input_value sel : string =
  let res = ref "" in
  jquery_input sel (fun input -> res := to_string input##value);
  !res
  
let jquery_set_innerHTML sel html =
  jquery sel (fun elt -> elt##innerHTML <- string html)

let jquery_show sel = jquery sel (fun elt -> elt##style##display <- string "block")
let jquery_hide sel = jquery sel (fun elt -> elt##style##display <- string "none")
let jquery_toggle sel = jquery sel (fun elt ->
  if to_string elt##style##display = "block"
  then elt##style##display <- string "none"
  else elt##style##display <- string "block")
    
let jquery_click sel = jquery sel (fun elt -> firebug (sel ^ " is clicked!"); Unsafe.meth_call elt "click" [||])

let onclick k elt =
  elt##onclick <- Dom.handler (fun ev -> k elt ev; bool true)

let ondblclick k elt =
  elt##ondblclick <- Dom.handler (fun ev -> k elt ev; bool true)

let onhover k elt =
  elt##onmouseover <- Dom.handler (fun ev -> k elt ev; bool true)

let oninput k elt =
  elt##oninput <- Dom.handler (fun ev -> k elt ev; bool true)

let onchange k elt =
  elt##onchange <- Dom.handler (fun ev -> k elt ev; bool true)

let onkeypress k elt =
  elt##onkeypress <- Dom.handler (fun ev -> k elt ev; bool true)

let onenter k elt =
  onkeypress
    (fun elt ev -> if ev##keyCode = 13 then begin firebug "enter!"; k elt ev end)
    elt

let stop_links_propagation_from elt =
  jquery_all_from elt "a" Dom_html.CoerceTo.element
    (onclick (fun elt ev -> Dom_html.stopPropagation ev))
    
(* prepare a string for safe insertion in HTML code *)
let escapeHTML (str : string) : string =
  let div = Dom_html.createDiv Dom_html.document in
  ignore (div##appendChild((Dom_html.document##createTextNode(string str) :> Dom.node t)));
  to_string (div##innerHTML)

let integer_of_input ?(min = min_int) ?(max = max_int) input : int option =
  try
    let n = int_of_string (to_string input##value) in
    if n < min then None
    else if n > max then None
    else Some n
  with _ -> None

let remove_prefix prefix s =
  let k = String.length prefix in
  let l = String.length s in
  if String.sub s 0 k = prefix
  then String.sub s k (l-k)
  else invalid_arg "Jsutils.remove_prefix: invalid prefix"

(* binding to Stanford Javascript Crypto Library *)

let pbkdf2_of_string : string -> string -> string =
  let sjcl = Unsafe.variable "sjcl" in
  fun passwd salt ->
    let bits = Unsafe.fun_call sjcl##misc##pbkdf2 [|Unsafe.inject (string passwd); Unsafe.inject (string salt)|] in
    (* relying on default parameters for iteration count (1000) and key length (256) *)
    let hex = Unsafe.fun_call sjcl##codec##hex##fromBits [|bits|] in
    to_string (Unsafe.coerce hex)

