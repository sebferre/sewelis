(* -------------------------------------------------------------- *)
(* Access rights, users/key management *)

open Utils

(* -------------------------------------- *)
(* User list management *)
  
let users_csv = "users.csv"
  
type user_login = string (* login name of users *)

type hash_mode =
| Sha256 (* 2016-02-18 *)
(* to be improved by key stretching *)
let string_of_hash_mode = function
  | Sha256 -> "sha256"
let hash_mode_of_string = function
  | "sha256" -> Sha256
  | _ -> Sha256 (* oops, should not happen *)

type user_info = { mutable email : string;  (* email *)
		   mutable removed : bool;  (* when removed, not destroyed to avoid reuse of identity *)
		   mutable hash_mode : hash_mode; (* to know which passwd hashing mode was used *)
		   mutable salt : string; (* user&passwd-specific salt *)
		   mutable hashed_passwd : string; (* hashed  salt+passwd *)
		 }

let users : (user_login * user_info) list ref = ref []

exception No_such_user of user_login
exception Already_used_login of user_login
exception Invalid_passwd of user_login

let hexa_encoding bytes =
  let transform = Cryptokit.Hexa.encode () in
  Cryptokit.transform_string transform bytes
    
let random_salt () =
  hexa_encoding (Cryptokit.Random.string Cryptokit.Random.secure_rng 32 (* bytes *))
    
let hash_passwd salt passwd =
  let hash = Cryptokit.Hash.sha256 () in
  hexa_encoding (Cryptokit.hash_string hash (salt ^ passwd))

(* persistency *)

let load () =
  try
    let csv = try Csv.load (!server_dir // users_csv) with _ -> [] in
    let l_users =
      List.map
	(function
	| [ login; email; s_removed; s_hash_mode; salt; hashed_passwd] -> (login, { email; removed=bool_of_string s_removed; hash_mode = hash_mode_of_string s_hash_mode; salt; hashed_passwd })
	| l -> failwith ("in users.csv: misformed line: " ^ String.concat "," l))
	csv in
    users := l_users
  with exn ->
    prerr_endline (Printexc.to_string exn);
    exit 1

let save () =
  let csv =
    List.map
      (fun (login, info) -> [login; info.email; string_of_bool info.removed; string_of_hash_mode info.hash_mode; info.salt; info.hashed_passwd])
      !users in
  Csv.save (!server_dir // users_csv) csv

(* access and update functions *)

let user_exists login =
  List.mem_assoc login !users
    
let get_user_info login =
  try List.assoc login !users
  with _ -> raise $ No_such_user login

let get_user_hashed_passwd login =
  (get_user_info login).hashed_passwd

let create_user ~login ~passwd ~email =
  if List.mem_assoc login !users
  then raise $ Already_used_login login
  else
    let salt = random_salt () in
    let hashed_passwd = hash_passwd salt passwd in
    users := (login, { email; removed=false; hash_mode=Sha256; salt; hashed_passwd }) :: !users

let update_user_passwd ~login ~new_passwd =
  let info = get_user_info login in
  let new_salt = random_salt () in
  info.salt <- new_salt;
  info.hashed_passwd <- hash_passwd new_salt new_passwd

let update_user_email ~login ~new_email =
  let info = get_user_info login in
  info.email <- new_email

let remove_user ~login =
  let info = get_user_info login in
  info.removed <- true

let restore_user ~login =
  let info = get_user_info login in
  info.removed <- false


(* ------------------------------------------------ *)
(* User authentication and access rights management *)

type user_role = | No_right             (* cannot do anything *)
                 | Reader               (* can read context *)
                 | Collaborator         (* can add but not delete *)
                 | Publisher            (* can edit his context(s) *)
                 | Admin                (* can edit context and users *)
                 | General_admin        (* can do everything *)

type userkey = string                   (* hexadecimal number as a string *)

type date = float

type logged_user = userkey * (user_login * date ref)

exception No_such_role of string
exception Not_enough_rights
exception No_such_key of string
exception Expired_key of string
exception Registered_key of string

let default_login = "anonymous"
let default_key = "0"
let default_key_timeout = 3. *. 60. *. 60. (* 3 hours, in seconds *)

let logged_users : logged_user list ref = ref []


let rec new_random_key () : string =
  let key = Printf.sprintf "%X" (Random.int 0x10000000) in
  if List.mem_assoc key !users
  then new_random_key ()
  else key

let string_of_role r = match r with
  | General_admin -> "super_admin"
  | Admin -> "admin"
  | Publisher -> "publisher"
  | Collaborator -> "collaborator"
  | Reader -> "reader"
  | No_right -> "none"


let role_of_string s = match (String.lowercase s) with
  | "super_admin" -> General_admin
  | "admin" -> Admin
  | "publisher" -> Publisher
  | "collaborator" -> Collaborator
  | "reader" -> Reader
  | "none"
  | "" -> No_right
  | _ -> raise (No_such_role s)


(* -------------------------------------------------------------- *)
(* Core accessors *)

let get_logged_user = fun key ->
  if key = default_key
  then default_login, ref 0.
  else
    try
      List.assoc key !logged_users
    with
      Not_found -> raise $ No_such_key key

let get_login = fun key ->
  fst (get_logged_user key)


let update_user_expiration = fun key ->
  if key = default_key
  then ()
  else
    try
      let login, ref_expire_date = get_logged_user key in
      ref_expire_date := now () +. default_key_timeout
    with
    (* when key is not found, just do nothing *)
    (* this happens with eg. the ping command *)
      Not_found -> ()


let add_logged_user = fun key login ->
  let open Netstring_str in
  if (string_match (regexp "[a-fA-F0-9]+$") key 0) == None then
    failwith "Malformed key"
  else if List.mem_assoc key !logged_users then
    ()
  else
    let expire_date = now () +. default_key_timeout in
    logged_users := (key, (login, ref expire_date)) :: !logged_users


let remove_logged_user = fun key ->
  logged_users := List.remove_assoc key !logged_users


(* -------------------------------------------------------------- *)
(* Other functions, should not touch !users *)

let assert_valid_passwd ~login ~passwd : unit =
  let info = get_user_info login in
  if hash_passwd info.salt passwd = info.hashed_passwd
  then ()
  else raise $ Invalid_passwd login

let assert_new_key = fun key ->
  if List.mem_assoc key !logged_users then raise (Registered_key key)

let assert_not_expired = fun key ->
  if key = default_key
  then ()
  else
    try
      let _, ref_expire_date = get_logged_user key in
      if now() > !ref_expire_date then raise (Expired_key key)
    with
    | Not_found -> raise $ No_such_key key
    | Expired_key _ as e -> remove_logged_user key; raise e
