open Nethttpd_types
open Nethttpd_services
open Nethttpd_engine
open Utils
open Xmlutils
open StdLabels

module Sewelis = Func_api

(* -------------------------------------------------------------- *)
(* Globals *)
let port = ref 0

let activation_date = ref (rfc3339_now ())
let last_update = ref "none"

let logfile = ref "/tmp/server.log" (* default log file of sewelis server *)
let ignore_rights = ref false

(* -------------------------------------------------------------- *)
(* exception handling *)

type param_error =
  | Missing_parameter of string
  | Empty_parameter of string
exception Parameter_error of param_error list

let xml_of_exn = fun e ->
  let xml_msg msg = [Xml.Element ("message", [], [pcdata msg])] in
  match e with
  | Parameter_error errors ->
    let f = fun err -> match err with
      | Missing_parameter str -> xml_msg ("parameter " ^ str ^ " is missing")
      | Empty_parameter str -> xml_msg ("parameter " ^ str ^ " is empty")
    in
    List.flatten (List.map f errors)
  (* Sewelis exceptions *)
  | No_such_file str -> xml_msg ("File " ^ str ^ " was not found on the server.")
  | Not_a_valid_order str -> xml_msg (str ^ "does not represent a column order. Use one of \"default\", \"asc\" or \"desc\".")
  | Not_a_valid_aggreg str -> xml_msg (str ^ "does not represent an aggregation. Use one of \"index\", \"distinct_count\", \"sum\" or \"avg\".")
  | Not_a_valid_transformation str -> xml_msg (str ^ "does not represent a transformation.")
  | Todo str -> xml_msg ("TODO: implement " ^ str ^ " command.")
  | Not_a_valid_rdf_term str -> xml_msg (str ^ " is not a valid XML representation of an RDF term.")
  (* Users exceptions *)
  | Users.No_such_user login -> xml_msg "Invalid login or password" (*"User login '" ^ login ^ "' does not exist"*)
  | Users.Already_used_login login -> xml_msg ("User login '" ^ login ^ "' already exists")
  | Users.Invalid_passwd _login -> xml_msg "Invalid login or password"
  | Users.No_such_role role -> xml_msg ("Invalid role: " ^ role)
  | Users.Not_enough_rights -> xml_msg ("You do not have enough rights for this action")
  | Users.No_such_key key -> xml_msg "Invalid key" (*"Invalid key: " ^ key*)
  | Users.Expired_key key -> xml_msg "Invalid key" (*"Expired key: " ^ key*)
  | Users.Registered_key key -> xml_msg "Invalid key" (*"Already used key: " ^ key*)
  (* Stores exceptions *)
  | Stores.No_such_store name -> xml_msg ("Store '" ^ name ^ "' does not exist")
  | Stores.Already_used_store name -> xml_msg ("Store '" ^ name ^ "' already exists")
  (* Any other exception *)
  | _ -> xml_msg (Printexc.to_string e ^ "\n" ^ Printexc.get_backtrace ())


(* ------------------------------------------------ *)
(* different kinds of responses *)

type response = XML of Xml.xml | File of string * string (* mime, filepath *)
    
let xml_response ?attrs ?children tag = XML (xml_ok ?attrs ?children tag)
let file_response ~mime ~filepath = File (mime, filepath)

let send_xml = fun env cgi xml ->
  let xml_declaration = "<?xml version=\"1.0\" encoding=\"UTF-8\" ?>\n" in
  let response = xml_declaration ^ (Xml.to_string xml) in
  env # set_output_header_field "Access-Control-Allow-Origin" "*";
  env # set_output_header_field "Content-Type" "text/xml";
  cgi # output # output_string response;
  cgi # output # commit_work()

let send_file = fun env (cgi : Netcgi.cgi) mime filepath ->
  let input = open_in filepath in
  let ocamlnet_input = (new Netchannels.input_channel input :> Netchannels.in_obj_channel) in
  env # set_output_header_field "Access-Control-Allow-Origin" "*";
  env # set_output_header_field "Content-Type" mime;
  cgi # output # output_channel ocamlnet_input;
  cgi # output # commit_work()

let send_response env cgi = function
  | XML xml -> send_xml env cgi xml
  | File (mime,filepath) -> send_file env cgi mime filepath
    
(* -------------------------------------------------------------- *)
(* Common sanity checks *)

let check_param = fun param (cgi: Netcgi.cgi) ->
  if not (cgi # argument_exists param) then
    Some (Missing_parameter param)
  (*else if (cgi # argument_value param) = "" then
    Some (Empty_parameter param)*) (* ex: 'patterns' can be empty strings *)
  else
    None

let assert_params = fun params (cgi: Netcgi.cgi) ->
  let checks = List.map (fun p -> check_param p cgi) params in
  let errors = List.filter (fun x -> is_some x) checks in
  let errors' = List.map val_of_option errors in
  if errors' <> [] then raise (Parameter_error errors')


(* Login *)

let register_service = fun tag (cgi: Netcgi.cgi) ->
  let login = string_argument cgi "userLogin" in
  let passwd = string_argument cgi "passwd" in
  let email = string_argument cgi "email" in
  Users.create_user ~login ~passwd ~email;
  xml_response tag

let login_service = fun tag (cgi: Netcgi.cgi) ->
  let login = string_argument cgi "userLogin" in
  let passwd = string_argument cgi "passwd" in
  if login = Users.default_login then
    xml_response tag ~attrs:["userKey", Users.default_key]
  else begin
    Users.assert_valid_passwd ~login ~passwd;
    let key = Users.new_random_key () in
    Users.add_logged_user key login;
    xml_response tag ~attrs:["userKey", key]
  end

let active_session_service = fun tag (cgi: Netcgi.cgi) ->
  let key = string_argument cgi "userKey" in
  let active = try Users.assert_not_expired key; true with _ -> false in
  xml_response tag ~attrs:["active", string_of_bool active]

let logout_service = fun tag (cgi: Netcgi.cgi) ->
  let key = string_argument cgi "userKey" in
  Users.remove_logged_user key;
  xml_response tag

(* -------------------------------------------------------------- *)
(* Users > Info update *)

let user_argument cgi = Users.get_login (string_argument cgi "userKey") 

let change_passwd_service = fun tag (cgi: Netcgi.cgi) ->
  let login = user_argument cgi in
  let passwd = string_argument cgi "passwd" in
  let new_passwd = string_argument cgi "newPasswd" in
  Users.assert_valid_passwd ~login ~passwd;
  Users.update_user_passwd ~login ~new_passwd;
  xml_response tag

let change_email_service = fun tag (cgi: Netcgi.cgi) ->
  let login = user_argument cgi in
  let new_email = string_argument cgi "newEmail" in
  Users.update_user_email ~login ~new_email;
  xml_response tag

let unregister_service = fun tag (cgi: Netcgi.cgi) ->
  let key = string_argument cgi "userKey" in
  let login = Users.get_login key in
  Users.remove_logged_user key;
  Users.remove_user ~login;
  xml_response tag
  
(* -------------------------------------------------------------- *)
(* Stores > Store list *)

let visible_stores_service = fun tag (cgi: Netcgi.cgi) ->
  let user = user_argument cgi in
  let l_stores = Stores.visible_stores user in
  let children = List.map xml_of_store_description l_stores in
  xml_response tag ~children

let removed_stores_service = fun tag (cgi: Netcgi.cgi) ->
  let user = user_argument cgi in
  let l_stores = Stores.removed_stores user in
  let children = List.map xml_of_store_description l_stores in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Stores > Store creators *)

(* No service associated with save_store, since it is automatically
   called after an operation that modifies the store *)
let save_store = fun storeName ->
  Sewelis.save_store ~store:(Stores.get_open_store_id storeName)


let create_store_service = fun tag (cgi: Netcgi.cgi) ->
  let admin = user_argument cgi in
  let default_role = Users.role_of_string (string_argument cgi "defaultRole") in
  let title = string_argument cgi "title" in
  let list_user_role = list_user_role_of_string (string_argument cgi "listUserRole") in
  let store_name =
    let path_log = Filename.temp_file ~temp_dir:!server_dir "store" ".log" in
    Filename.chop_suffix (Filename.basename path_log) ".log" in
  Stores.create_store ~admin ~name:store_name ~default_role ~title ~list_user_role;
  xml_response tag ~attrs:["storeName",store_name]

let update_store_service = fun tag (cgi: Netcgi.cgi) ->
  let admin = user_argument cgi in
  let store_name = string_argument cgi "storeName" in
  let default_role = Users.role_of_string (string_argument cgi "defaultRole") in
  let title = string_argument cgi "title" in
  let list_user_role = list_user_role_of_string (string_argument cgi "listUserRole") in
  Stores.update_store ~admin ~name:store_name ~default_role ~title ~list_user_role;
  xml_response tag

let remove_store_service = fun tag (cgi: Netcgi.cgi) ->
  let store_name = string_argument cgi "storeName" in
  Stores.remove_store ~name:store_name;
  xml_response tag

let restore_store_service = fun tag (cgi: Netcgi.cgi) ->
  let store_name = string_argument cgi "storeName" in
  Stores.restore_store ~name:store_name;
  xml_response tag

    
let store_argument cgi = Stores.get_open_store_id (string_argument cgi "storeName")    

let export_rdf_service = fun tag (cgi: Netcgi.cgi) ->
  let store_name = string_argument cgi "storeName" in
  let store = store_argument cgi in
  let suffix, mime =
    match string_argument cgi "extension" with
    | ".log" | "log" -> ".log", "text/sewelis-log; charset=\"UTF-8\""
    | ".rdf" | "rdf" | "xml" -> ".rdf", "application/rdf+xml"
    | ".nt" | "nt" -> ".nt", "application/n-triples"
    | _ -> ".ttl", "text/turtle; charset=\"UTF-8\"" in
  if suffix = ".log"
  then
    let filepath = !server_dir // (store_name ^ suffix) in
    file_response ~mime ~filepath
  else
    let filepath = Filename.temp_file "sewelis_export" suffix in
    let _ = Sewelis.export_rdf ~store ~filepath () in
    file_response ~mime ~filepath

(* -------------------------------------------------------------- *)
(* Stores > Store accessors *)

let store_base_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let uri = Sewelis.store_base ~store in
  let attrs = ["uri", uri] in
  xml_response tag ~attrs

let store_xmlns_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let namespaces = Sewelis.store_xmlns ~store in
  let children = List.map xml_of_ns namespaces in
  xml_response tag ~children

let store_namespace_of_prefix_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let prefix = string_argument cgi "prefix" in
  let namespace =
    match Sewelis.store_namespace_of_prefix ~store ~prefix with
    | Some ns -> ns
    | None -> "" in
  let attrs = ["namespace", namespace] in
  xml_response tag ~attrs

let uri_description_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let uri = string_argument cgi "uri" in
  let display = Sewelis.uri_description ~store ~uri in
  let children = [xml_of_display display] in
  xml_response tag ~children

let results_of_statement_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let statement = string_argument cgi "statement" in
  let results = Sewelis.results_of_statement ~store ~statement in
  let children = [xml_of_results results] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Stores > Store modifiers *)

let define_base_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let base = string_argument cgi "base" in
  let _ = Sewelis.define_base ~store ~base in
  xml_response tag

let define_namespace_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let prefix = string_argument cgi "prefix" in
  let uri = string_argument cgi "uri" in
  let _ = Sewelis.define_namespace ~store ~prefix ~uri in
  xml_response tag

let add_triple_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.add_triple ~store ~s ~p ~o in
  xml_response tag

let remove_triple_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.remove_triple ~store ~s ~p ~o in
  xml_response tag

let replace_object_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let s = string_argument cgi "s" in
  let p = string_argument cgi "p" in
  let o = rdf_of_xmlstring (string_argument cgi "o") in
  let _ = Sewelis.replace_object ~store ~s ~p ~o in
  xml_response tag

let import_rdf_service = fun tag (cgi: Netcgi.cgi) ->
  let store_name = string_argument cgi "storeName" in
  let store = Stores.get_open_store_id store_name in
  let filepath = file_argument (!server_dir // (store_name ^ "-files")) cgi "file" in
  let base = Sewelis.store_base store in
  let _ = Sewelis.import_rdf ~store ~base ~filepath in
  xml_response tag
    
let import_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let uri = string_argument cgi "uri" in
  let _ = Sewelis.import_uri ~store ~uri in
  xml_response tag

let run_statement_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let statement = string_argument cgi "statement" in
  let _ = Sewelis.run_statement ~store ~statement in
  xml_response tag

(* -------------------------------------------------------------- *)
(* Navigation places > Place creators *)

type kind_of_place = Root | Home | Bookmarks | Drafts | Uri of string | Statement of string

let get_place = fun place_kind tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let get_place_function = match place_kind with
    | Root -> Sewelis.get_place_root
    | Home -> Sewelis.get_place_home
    | Bookmarks -> Sewelis.get_place_bookmarks
    | Drafts -> Sewelis.get_place_drafts
    | Uri uri -> Sewelis.get_place_uri ~uri
    | Statement statement -> Sewelis.get_place_statement ~statement in
  let place = get_place_function ~store in
  let content = Sewelis.place_content ~store ~place in
  xml_response tag
    ~attrs:["placeId", string_of_int place]
    ~children:[xml_of_place content]

let get_place_root_service = get_place Root
let get_place_home_service = get_place Home
let get_place_bookmarks_service = get_place Bookmarks
let get_place_drafts_service = get_place Drafts

let get_place_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let uri = string_argument cgi "uri" in
  get_place (Uri uri) tag cgi

let get_place_statement_service = fun tag (cgi: Netcgi.cgi) ->
  let statement = string_argument cgi "statement" in
  get_place (Statement statement) tag cgi

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Mode *)

let place_mode_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let attrs = ["mode", string_of_place_mode (Sewelis.place_mode ~place)] in
  xml_response tag ~attrs

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Relaxation *)

let place_relaxation_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_relaxation (Sewelis.place_relaxation ~place)] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Place statement *)

let statement_string_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let children = [pcdata (Sewelis.statement_string ~store ~place)] in
  xml_response tag ~children

let statement_focused_display_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let children = [xml_of_focused_display (Sewelis.statement_focused_display ~store ~place)] in
  xml_response tag ~children

let place_statement_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let children = [xml_of_statement (Sewelis.place_statement ~store ~place)] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Place answers *)

let answer_count_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let count = Sewelis.answers_count place in
  xml_response tag ~attrs:["count", string_of_int count]

let answers_paging_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_paging (Sewelis.answers_paging ~place)] in
  xml_response tag ~children

let answers_columns_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_columns (Sewelis.place_columns ~place)] in
  xml_response tag ~attrs:[] ~children

let cell_display_service = fun tag (cgi:Netcgi.cgi) ->
  let cell = int_argument cgi "cellId" in
  let display = Sewelis.cell_display ~cell in
  let children = [xml_of_display display] in
  xml_response tag ~children

let place_rows_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_rows (Sewelis.place_rows ~place)] in
  xml_response tag ~children

let place_answers_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Increments *)

let increment_kind_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let kind = Sewelis.increment_kind ~increment in
  xml_response tag ~attrs:["kind", string_of_increment_kind kind]

let increment_display_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let display = Sewelis.increment_display ~increment in
  xml_response tag ~children:[xml_of_display display]

let increment_ratio_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let left, right = Sewelis.increment_ratio ~increment in
  xml_response tag ~attrs:["left", string_of_int left;
                     "right", string_of_int right]

let increment_new_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let is_new = Sewelis.increment_new ~increment in
  xml_response tag ~attrs:["isNew", string_of_bool is_new]

let increment_uri_opt_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let uri_opt = Sewelis.increment_uri_opt ~increment in
  xml_response tag ~attrs:["uri", string_of_option xml_string uri_opt]

let increment_term_opt_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let children = [xml_of_option xml_of_term (Sewelis.increment_term_opt ~increment)] in
  xml_response tag ~children

let place_increment_service = fun tag (cgi:Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let children = [xml_of_increment (Sewelis.place_increment ~increment)] in
  xml_response tag ~children

let get_increment_entity_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let increment = Sewelis.get_increment_entity ~place in
  let children = [xml_of_increment (Sewelis.place_increment ~increment)] in
  xml_response tag ~attrs:["incrementId", string_of_int increment] ~children

let get_increment_relation_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let increment = Sewelis.get_increment_relation ~place in
  let children = [xml_of_increment (Sewelis.place_increment ~increment)] in
  xml_response tag ~attrs:["incrementId", string_of_int increment] ~children

let get_children_increments_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let parent = int_argument cgi "parentId" in
  let increments = Sewelis.get_children_increments ~place ~parent in
  let children =
    List.map
      (fun increment -> 
	xml_of_increment (Sewelis.place_increment ~increment))
      increments in
  xml_response tag ~children

let get_increment_tree_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let parent = int_argument cgi "parentId" in
  let increments = Sewelis.get_increment_tree ~place ~parent in
  let children = [xml_of_increment_tree increments] in
  xml_response tag ~children

(*
let get_increment_trees_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let root_entity = Sewelis.get_increment_entity ~place in
  let tree_entity = Sewelis.get_increment_tree ~place ~parent:root_entity in
  let root_relation = Sewelis.get_increment_relation ~place in
  let tree_relation = Sewelis.get_increment_relation ~place ~parent:root_relation in
  let children = [xml_of_increment_tree tree_entity; xml_of_increment_tree tree_relation] in
  xml_response tag ~children
*)

let get_transformations_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let transf_list = Sewelis.get_transformations ~place in
  let children = [xml_of_transformations transf_list] in
  xml_response tag ~children

let can_insert_entity_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let can_insert = string_of_bool (Sewelis.can_insert_entity ~place) in
  xml_response tag ~attrs:["canInsertEntity", can_insert]

let can_insert_relation_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let can_insert = string_of_bool (Sewelis.can_insert_relation ~place) in
  xml_response tag ~attrs:["canInsertRelation", can_insert]

let place_suggestions_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let children = [xml_of_suggestions (Sewelis.place_suggestions ~place)] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Aggregated structure *)

let place_content_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let children = [xml_of_place (Sewelis.place_content ~store ~place)] in
  xml_response tag ~children

(* -------------------------------------------------------------- *)
(* Navigation places > Place accessors > Completions *)

let get_completions_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let key = string_argument cgi "matchingKey" in
  let max_compl =
    if (cgi # argument_exists "maxCompl") then
      Some (int_argument cgi "maxCompl")
    else None in
  let nb_more_relax =
    if (cgi # argument_exists "nbMoreRelax") then
      Some (int_argument cgi "nbMoreRelax")
    else None in
  let completions = Sewelis.get_completions ~place ~key ?max_compl ?nb_more_relax in
  xml_response tag ~children:[xml_of_completions completions]


(* -------------------------------------------------------------- *)
(* Navigation places > Place modifiers > Statement relaxation *)

let show_more_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_more ~place in
  xml_response tag
    ~children:[xml_of_place (Sewelis.place_content ~store ~place)]

let show_less_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_less ~place in
  xml_response tag
    ~children:[xml_of_place (Sewelis.place_content ~store ~place)]

let show_most_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_most ~place in
  xml_response tag
    ~children:[xml_of_place (Sewelis.place_content ~store ~place)]

let show_least_service = fun tag (cgi:Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.show_least ~place in
  xml_response tag
    ~children:[xml_of_place (Sewelis.place_content ~store ~place)]

(* -------------------------------------------------------------- *)
(* Navigation places > Place modifiers > Place answers *)

let page_down_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_down ~place in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let page_up_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_up ~place in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let page_top_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_top ~place in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let page_bottom_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let result = Sewelis.page_bottom ~place in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let set_page_start_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let pos = int_argument cgi "pos" in
  let result = Sewelis.set_page_start ~place ~pos in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let set_page_end_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let pos = int_argument cgi "pos" in
  let result = Sewelis.set_page_end ~place ~pos in
  let attrs = ["value", string_of_bool result] in
  let children =
    if result
    then [xml_of_answers (Sewelis.place_answers ~place)]
    else [] in
  xml_response tag ~attrs ~children

let move_column_left_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let () = Sewelis.move_column_left ~place ~column in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

let move_column_right_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let () = Sewelis.move_column_right ~place ~column in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

let set_column_hidden_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let hidden = bool_argument cgi "hidden" in
  let () = Sewelis.set_column_hidden ~place ~column ~hidden in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

let set_column_order_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let order = column_order_of_xmlstring (string_argument cgi "order") in
  let () = Sewelis.set_column_order ~place ~column ~order in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

let set_column_pattern_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let pattern = string_argument cgi "pattern" in
  let () = Sewelis.set_column_pattern ~place ~column ~pattern in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children

let set_column_aggreg_service = fun tag (cgi:Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let column = string_argument cgi "column" in
  let aggreg = column_aggreg_of_xmlstring (string_argument cgi "aggreg") in
  let () = Sewelis.set_column_aggreg ~place ~column ~aggreg in
  let children = [xml_of_answers (Sewelis.place_answers ~place)] in
  xml_response tag ~children


(* -------------------------------------------------------------- *)
(* Navigation places > Place-based store modifiers *)

let do_run_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let new_place = Sewelis.do_run ~store ~place in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let set_as_home_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.set_as_home ~place in
  xml_response tag

let add_as_bookmark_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.add_as_bookmark ~place in
  xml_response tag

let add_as_draft_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.add_as_draft ~place in
  xml_response tag

let remove_as_draft_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.remove_as_draft ~place in
  xml_response tag


(* -------------------------------------------------------------- *)
(* Statement transformation / Navigation links *)

let change_focus_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let focus = int_argument cgi "focusId" in
  let new_place = Sewelis.change_focus ~store ~place ~focus in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let apply_transformation_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let transformation = transformation_of_string (string_argument cgi "transformation") in
  let new_place = Sewelis.apply_transformation ~store ~place ~transformation in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let increment = int_argument cgi "incrementId" in
  let new_place = Sewelis.insert_increment ~store ~place ~increment in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_increment_list = fun combinator tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let increment_list =
    List.map (fun x -> int_of_string x#value)
      (cgi # multiple_argument "incrementId") in
  let insert_func = match combinator with
    | `And -> Sewelis.insert_increment_list_and
    | `Or -> Sewelis.insert_increment_list_or
    | `Not -> Sewelis.insert_increment_list_not in
  let new_place = insert_func ~store ~place ~increment_list in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_increment_list_and_service = insert_increment_list `And
let insert_increment_list_or_service = insert_increment_list `Or
let insert_increment_list_not_service = insert_increment_list `Not


let unquote_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let increment = int_argument cgi "incrementId" in
  let new_place = Sewelis.unquote_increment ~store ~place ~increment in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_plain_literal_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let text = string_argument cgi "text" in
  let lang = string_argument cgi "lang" in
  let new_place = Sewelis.insert_plain_literal ~store ~place ~text ~lang in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_typed_literal_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let text = string_argument cgi "text" in
  let datatype = string_argument cgi "datatype" in
  let new_place = Sewelis.insert_typed_literal ~store ~place ~text ~datatype in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_date_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let year = int_argument cgi "year" in
  let month = int_argument cgi "month" in
  let day = int_argument cgi "day" in
  let new_place = Sewelis.insert_date ~store ~place ~year ~month ~day in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_dateTime_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let year = int_argument cgi "year" in
  let month = int_argument cgi "month" in
  let day = int_argument cgi "day" in
  let hours = int_argument cgi "hours" in
  let minutes = int_argument cgi "minutes" in
  let seconds = int_argument cgi "seconds" in
  let new_place = Sewelis.insert_dateTime ~store ~place
                                      ~year ~month ~day
                                      ~hours ~minutes ~seconds in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_filename_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let filename = string_argument cgi "filename" in
  let new_place = Sewelis.insert_filename ~store ~place ~filename in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_var_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let varname' = string_argument cgi "varname" in
  let varname = if varname' = "" then None else Some varname' in
  let new_place = Sewelis.insert_var ~store ~place ~varname in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_uri_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_uri ~store ~place ~uri in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_class_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_class ~store ~place ~uri in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_property_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_property ~store ~place ~uri in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_inverse_property_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let new_place = Sewelis.insert_inverse_property ~store ~place ~uri in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]

let insert_structure_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let place = int_argument cgi "placeId" in
  let uri = string_argument cgi "uri" in
  let arity = int_argument cgi "arity" in
  let index = int_argument cgi "index" in
  let new_place = Sewelis.insert_structure ~store ~place ~uri ~arity ~index in
  xml_response tag
    ~attrs:["placeId", string_of_int new_place]
    ~children:[xml_of_place (Sewelis.place_content ~store ~place:new_place)]


(* -------------------------------------------------------------- *)
(* Freeing memory *)

let free_place_service = fun tag (cgi: Netcgi.cgi) ->
  let place = int_argument cgi "placeId" in
  let _ = Sewelis.free_place ~place in
  xml_response tag

(* should not be the responsability of clients *)
(*
let free_focus_service = fun tag (cgi: Netcgi.cgi) ->
  let focus = int_argument cgi "focusId" in
  let _ = Sewelis.free_focus ~focus in
  xml_response tag

let free_cell_service = fun tag (cgi: Netcgi.cgi) ->
  let cell = int_argument cgi "cellId" in
  let _ = Sewelis.free_cell ~cell in
  xml_response tag

let free_increment_service = fun tag (cgi: Netcgi.cgi) ->
  let increment = int_argument cgi "incrementId" in
  let _ = Sewelis.free_increment ~increment in
  xml_response tag

let free_store_service = fun tag (cgi: Netcgi.cgi) ->
  let store = store_argument cgi in
  let _ = Sewelis.free_store ~store in
  xml_response tag

let free_root_service = fun tag (cgi: Netcgi.cgi) ->
  let _ = Sewelis.free_root () in
  xml_response tag
    *)


(* -------------------------------------------------------------- *)
(* Compute and send result to HTTP queries *)

let ping_service = fun tag cgi ->
  let attrs = [ "activationDate", !activation_date;
                "pid", string_of_int $ Unix.getpid ();
              ] in
  xml_response tag ~attrs

(* -------------------------------------------------------------- *)
(* Dispatch services to URLs *)

(* list of services; each is a 5-uplet:
 *   URL,
 *   handler function,
 *   minimum role,
 *   list of required params,
 *   UpdateStore | UpdateStoreList | UpdateUsers | NoChange
 *)
(* Note that presence of a "userKey" parameter is automatically checked
   if role <> No_right *)

type effect = UpdateStore | UpdateStoreList | UpdateUsers | NoChange

let services =
  [ "/ping", ping_service, Users.No_right, [], NoChange;
    "/register", register_service, Users.No_right, ["userLogin"; "passwd"; "email"], UpdateUsers;
    "/login", login_service, Users.No_right, ["userLogin"; "passwd"], NoChange;
    "/activeSession", active_session_service, Users.No_right, ["userKey"], NoChange;
    "/logout", logout_service, Users.No_right, ["userKey"], NoChange;

    (* Users *)
    "/changePasswd", change_passwd_service, Users.No_right, ["userKey"; "passwd"; "newPasswd"], UpdateUsers;
    "/changeEmail", change_email_service, Users.No_right, ["userKey"; "newEmail"], UpdateUsers;
    "/unregister", unregister_service, Users.No_right, ["userKey"], UpdateUsers;

    (* Stores > Store lists *)
    "/visibleStores", visible_stores_service, Users.No_right, ["userKey"], NoChange;
    "/removedStores", removed_stores_service, Users.No_right, ["userKey"], NoChange;
    (* Stores > Store creators *)
    "/createStore", create_store_service, Users.No_right, ["userKey"; "defaultRole"; "title"; "listUserRole"], UpdateStoreList;
    "/updateStore", update_store_service, Users.Admin, ["defaultRole"; "title"; "listUserRole"], UpdateStoreList;
    "/removeStore", remove_store_service, Users.Admin, [], UpdateStoreList;
    "/restoreStore", restore_store_service, Users.Admin, [], UpdateStoreList;
    (* Stores > Store accessors *)
    "/exportRdf", export_rdf_service, Users.Reader, ["extension"], NoChange;
    "/storeBase", store_base_service, Users.Reader, [], NoChange;
    "/storeXmlns", store_xmlns_service, Users.Reader, [], NoChange;
    "/storeNamespaceOfPrefix", store_namespace_of_prefix_service, Users.Reader, ["prefix"], NoChange;
    "/uriDescription", uri_description_service, Users.Reader, ["uri"], NoChange;
    "/resultsOfStatement", results_of_statement_service, Users.Reader, ["statement"], NoChange;
    (* Stores > Store modifiers *)
    "/defineBase", define_base_service, Users.Publisher, ["base"], UpdateStore;
    "/defineNamespace", define_namespace_service, Users.Publisher, ["prefix"; "uri"], UpdateStore;
    "/addTriple", add_triple_service, Users.Publisher, ["s"; "p"; "o"], UpdateStore;
    "/removeTriple", remove_triple_service, Users.Publisher, ["s"; "p"; "o"], UpdateStore;
    "/replaceObject", replace_object_service, Users.Publisher, ["s"; "p"; "o"], UpdateStore;
    "/importRdf", import_rdf_service, Users.Publisher, ["file"], UpdateStore;
    "/importUri", import_uri_service, Users.Publisher, ["uri"], UpdateStore;
    "/runStatement", run_statement_service, Users.Publisher, ["statement"], UpdateStore;

    (* Navigation places > Place creators *)
    "/getPlaceRoot", get_place_root_service, Users.Reader, [], NoChange;
    "/getPlaceHome", get_place_home_service, Users.Reader, [], NoChange;
    "/getPlaceBookmarks", get_place_bookmarks_service, Users.Reader, [], NoChange;
    "/getPlaceDrafts", get_place_drafts_service, Users.Reader, [], NoChange;
    "/getPlaceUri", get_place_uri_service, Users.Reader, ["uri"], NoChange;
    "/getPlaceStatement", get_place_statement_service, Users.Reader, ["statement"], NoChange;

    (* Navigation places > Deallocation *)
    "/freePlace", free_place_service, Users.No_right, ["placeId"], NoChange;

    (* Navigation places > Place accessors > Mode and Relaxation *)
    "/placeMode", place_mode_service, Users.Reader, ["placeId"], NoChange;
    "/placeRelaxation", place_relaxation_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Place accessors > Place statement *)
    "/statementString", statement_string_service, Users.Reader, ["placeId"], NoChange;
    "/statementFocusedDisplay", statement_focused_display_service, Users.Reader, ["placeId"], NoChange;
    "/placeStatement", place_statement_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Place accessors > Place answers*)
    "/answersCount", answer_count_service, Users.Reader, ["placeId"], NoChange;
    "/answersPaging", answers_paging_service, Users.Reader, ["placeId"], NoChange;
    "/answersColumns", answers_columns_service, Users.Reader, ["placeId"], NoChange;
    "/cellDisplay", cell_display_service, Users.Reader, ["cellId"], NoChange;
    "/placeRows", place_rows_service, Users.Reader, ["placeId"], NoChange;
    "/placeAnswers", place_answers_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Place accessors > Increments *)
    "/incrementKind", increment_kind_service, Users.Reader, ["incrementId"], NoChange;
    "/incrementDisplay", increment_display_service, Users.Reader, ["incrementId"], NoChange;
    "/incrementRatio", increment_ratio_service, Users.Reader, ["incrementId"], NoChange;
    "/incrementNew", increment_new_service, Users.Reader, ["incrementId"], NoChange;
    "/incrementUriOpt", increment_uri_opt_service, Users.Reader, ["incrementId"], NoChange;
    "/incrementTermOpt", increment_term_opt_service, Users.Reader, ["incrementId"], NoChange;
    "/placeIncrement", place_increment_service, Users.Reader, ["incrementId"], NoChange;
    "/getIncrementEntity", get_increment_entity_service, Users.Reader, ["placeId"], NoChange;
    "/getIncrementRelation", get_increment_relation_service, Users.Reader, ["placeId"], NoChange;
    "/getChildrenIncrements", get_children_increments_service, Users.Reader, ["placeId"; "parentId"], NoChange;
    "/getIncrementTree", get_increment_tree_service, Users.Reader, ["placeId"; "parentId"], NoChange;
    "/getTransformations", get_transformations_service, Users.Reader, ["placeId"], NoChange;
    "/canInsertEntity", can_insert_entity_service, Users.Reader, ["placeId"], NoChange;
    "/canInsertRelation", can_insert_relation_service, Users.Reader, ["placeId"], NoChange;
    "/placeSuggestions", place_suggestions_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Place accessors > Aggregated structure *)
    "/placeContent", place_content_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Places accessors > Completions *)
    "/getCompletions", get_completions_service, Users.Reader, ["placeId"; "matchingKey"], NoChange;

    (* Navigation places > Place modifiers > Statement relaxation *)
    "/showMore", show_more_service, Users.Reader, ["placeId"], NoChange;
    "/showLess", show_less_service, Users.Reader, ["placeId"], NoChange;
    "/showMost", show_most_service, Users.Reader, ["placeId"], NoChange;
    "/showLeast", show_least_service, Users.Reader, ["placeId"], NoChange;
    (* Navigation places > Place modifiers > Place answers *)
    "/pageDown", page_down_service, Users.Reader, ["placeId"], NoChange;
    "/pageTop", page_top_service, Users.Reader, ["placeId"], NoChange;
    "/pageUp", page_up_service, Users.Reader, ["placeId"], NoChange;
    "/pageBottom", page_bottom_service, Users.Reader, ["placeId"], NoChange;
    "/setPageStart", set_page_start_service, Users.Reader, ["placeId"; "pos"], NoChange;
    "/setPageEnd", set_page_end_service, Users.Reader, ["placeId"; "pos"], NoChange;
    "/moveColumnLeft", move_column_left_service, Users.Reader, ["placeId"; "column"], NoChange;
    "/moveColumnRight", move_column_right_service, Users.Reader, ["placeId"; "column"], NoChange;
    "/setColumnHidden", set_column_hidden_service, Users.Reader, ["placeId"; "column"; "hidden"], NoChange;
    "/setColumnOrder", set_column_order_service, Users.Reader, ["placeId"; "column"; "order"], NoChange;
    "/setColumnPattern", set_column_pattern_service, Users.Reader, ["placeId"; "column"; "pattern"], NoChange;
    "/setColumnAggreg", set_column_aggreg_service, Users.Reader, ["placeId"; "column"; "aggreg"], NoChange;

    (* Navigation places > Place-based store modifiers *)
    "/doRun", do_run_service, Users.Publisher, ["placeId"], UpdateStore;
    "/setAsHome", set_as_home_service, Users.Publisher, ["placeId"], UpdateStore;
    "/addAsBookmark", add_as_bookmark_service, Users.Publisher, ["placeId"], UpdateStore;
    "/addAsDraft", add_as_draft_service, Users.Publisher, ["placeId"], UpdateStore;
    "/removeAsDraft", remove_as_draft_service, Users.Publisher, ["placeId"], UpdateStore;

    (* Statement transformation / Navigation links *)
    "/changeFocus", change_focus_service, Users.Reader, ["placeId"; "focusId"], NoChange;
    "/applyTransformation", apply_transformation_service, Users.Reader, ["placeId"; "transformation"], NoChange;
    "/insertIncrement", insert_increment_service, Users.Reader, ["placeId"; "incrementId"], NoChange;
    "/insertIncrementListAnd", insert_increment_list_and_service, Users.Reader, ["placeId"; "incrementId"], NoChange; (* multiple incrementId *)
    "/insertIncrementListOr", insert_increment_list_or_service, Users.Reader, ["placeId"; "incrementId"], NoChange; (* multiple incrementId *)
    "/insertIncrementListNot", insert_increment_list_not_service, Users.Reader, ["placeId"; "incrementId"], NoChange; (* multiple incrementId *)
    "/unquoteIncrement", unquote_increment_service, Users.Reader, ["placeId"; "incrementId"], NoChange;
    "/insertPlainLiteral", insert_plain_literal_service, Users.Reader, ["placeId"; "text"; "lang"], NoChange;
    "/insertTypedLiteral", insert_typed_literal_service, Users.Reader, ["placeId"; "text"; "datatype"], NoChange;
    "/insertDate", insert_date_service, Users.Reader, ["placeId"; "year"; "month"; "day"], NoChange;
    "/insertDateTime", insert_dateTime_service, Users.Reader, ["placeId"; "year"; "month"; "day"; "hours"; "minutes"; "seconds"], NoChange;
    "/insertFilename", insert_filename_service, Users.Reader, ["placeId"; "filename"], NoChange;
    "/insertVar", insert_var_service, Users.Reader, ["placeId"], NoChange; (* varname is optional *)
    "/insertUri", insert_uri_service, Users.Reader, ["placeId"; "uri"], NoChange;
    "/insertClass", insert_class_service, Users.Reader, ["placeId"; "uri"], NoChange;
    "/insertProperty", insert_property_service, Users.Reader, ["placeId"; "uri"], NoChange;
    "/insertInverseProperty", insert_inverse_property_service, Users.Reader, ["placeId"; "uri"], NoChange;
    "/insertStructure", insert_structure_service, Users.Reader, ["placeId"; "uri"; "arity"; "index"], NoChange;
  ]

(* checking acces right per user and store *)

let assert_rights = fun userKey storeName role ->
  Users.assert_not_expired userKey;
    (*try*)
    let user_login = Users.get_login userKey in
    let store_name = storeName in
    let user_role = Stores.get_role store_name user_login in
    match Pervasives.compare user_role role with
    | -1 -> raise Users.Not_enough_rights
    |_ -> ()
(*with Not_found -> failwith "Undefined store"*)

let save_changes cgi = function
  | UpdateUsers -> Users.save ()
  | UpdateStoreList -> Stores.save ()
  | UpdateStore ->
    let storeName = string_argument cgi "storeName" in
    let _ = last_update := rfc3339_now () in
    save_store storeName
  | NoChange -> ()

    
(* Wrapper around service handlers *)
(* It checks for required params. It checks for access rights. It also
   catches exception fired by the handler, and send an xml
   representation of the error. *)
(* WARNING: the parameter for checking access rights is hardcoded to "userKey" *)
(* Also checks if the store has to be saved on disk *)
let wrap_handler = fun url handler role params update_kind env (cgi: Netcgi.cgi) ->
  (* remove leading "/" from url to get tagname *)
  let tag = (Str.string_after url 1) ^ "Response" in
  try
    if role <> Users.No_right
    then begin
      assert_params ("userKey"::"storeName"::params) cgi;
      let userKey = string_argument cgi "userKey" in
      let storeName = string_argument cgi "storeName" in
      assert_rights userKey storeName role;
      Users.update_user_expiration userKey end
    else assert_params params cgi;
    let _ = logline !logfile (cgi # url ~with_query_string:`Env ()) in
    let response = handler tag cgi in
    logline !logfile (url ^ " compute OK.");
    save_changes cgi update_kind;
    send_response env cgi response;
    logline !logfile (url ^ " all done.")
  with
  | e ->
    let xml = xml_error tag (xml_of_exn e) in
    send_xml env cgi xml


let srv =
  let make_service = fun (url, f, role, params, update_kind) ->
    url,
    dynamic_service { dyn_handler = wrap_handler url f role params update_kind;
	                  dyn_activation = std_activation `Std_activation_buffered;
	                  dyn_uri = Some "/";
	                  dyn_translator = (fun _ -> "");
	                  dyn_accept_all_conditionals = false }
  in
  uri_distributor $ List.map make_service services



(* -------------------------------------------------------------- *)
(* Boilerplate code to create and launch web server *)

open Nethttpd_reactor

let start port =
  let config = Nethttpd_reactor.default_http_reactor_config in
  let master_sock = Unix.socket Unix.PF_INET Unix.SOCK_STREAM 0 in
  Unix.setsockopt master_sock Unix.SO_REUSEADDR true;
  Unix.bind master_sock (Unix.ADDR_INET(Unix.inet_addr_any, port));
  Unix.listen master_sock 100;
  Printf.printf "Listening on port %d\n" port;
  flush stdout;

  while true do
    try
      let conn_sock, _ = Unix.accept master_sock in
      Unix.set_nonblock conn_sock;
      let _ = Thread.create (process_connection config conn_sock) srv in
      ()
    with
      Unix.Unix_error(Unix.EINTR,_,_) -> ()  (* ignore *)
  done



(* -------------------------------------------------------------- *)
(* Parse command-line arguments and launches server *)

let usage = String.concat " "
  [ "Usage:"; Sys.argv.(0);
    "-port portnum";
    "-storedir string";
    "-base URI";
    "[-despot]";
    "\n"
  ]

let speclist =
  let set_storedir = fun path -> server_dir := path; logfile := path // "server.log" in
  let set_base = fun base -> server_base := base in
  Arg.align
    [ ("-port",      Arg.Set_int port,           " port number the server will listen");
      ("-storedir",  Arg.String set_storedir,    " path to the directory holding store data on server");
      ("-base",      Arg.String set_base,        " base URI for the store");
      ("-despot",    Arg.Set ignore_rights,      " ignore users right (use for debugging purposes)");
    ]

let anon_fun = fun x -> raise $ Arg.Bad ("Bad argument : " ^ x)

let check_args = fun () ->
  (*let warn cond msg = if cond then Printf.printf "[Warning] %s\n" msg else () in*)
  let check cond msg = if cond then raise $ Arg.Bad msg else () in
  let in_argv str = array_mem str Sys.argv in
  let checks = [ not (in_argv "-port"), "Unspecified port number";
                 not (in_argv "-storedir"), "Unspecified storedir";
		 not (in_argv "-base"), "Unspecified base URI";
               ] in
  List.iter (uncurry check) checks

let main = fun () ->
  Printexc.record_backtrace true;
  let _ = Arg.parse speclist anon_fun usage in
  try
    check_args ();
    Users.load ();
    Stores.load ();
    Netsys_signal.init ();
    (*
    Nettls_gnutls.init ();
    let tls_provider = Netsys_crypto.current_tls () in
    let tls_config = 
      Netsys_tls.create_x509_config
	~system_trust:true
	~peer_auth:`None
	tls_provider in
    let _ = new Nethttpd_kernel.modify_http_protocol_config ~config_tls:(Some tls_config) Nethttpd_kernel.default_http_protocol_config in
    *)
    Sys.set_signal Sys.sigint
      (Sys.Signal_handle (fun signal ->
	prerr_endline "Saving all stores before shuting down.";
	Sewelis.free_root ();
	Users.save ();
	Stores.save ();
	exit 0));
    start !port
  with Arg.Bad msg ->
    print_endline $ "Error: " ^ msg;
    print_newline ();
    Arg.usage speclist usage

let _ = if not !Sys.interactive then main ()
