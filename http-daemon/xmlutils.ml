open Utils

module Sewelis = Func_api

(* -------------------------------------------------------------- *)
(* Safety wrt. special charaters in strings *)

let xml_string = Netencoding.Html.encode
  ~in_enc:`Enc_utf8
  ~out_enc:`Enc_utf8
  ~unsafe_chars:"<>&\"'" ()

let url_decode = Netencoding.Url.decode

let pcdata s = Xml.PCData s


(* -------------------------------------------------------------- *)
(* reading parameters from http command *)

let int_argument = fun (cgi: Netcgi.cgi) name ->
  int_of_string (cgi # argument_value name)

let string_argument = fun (cgi: Netcgi.cgi) name ->
  url_decode (cgi # argument_value name)

let bool_argument = fun (cgi: Netcgi.cgi) name ->
  bool_of_string (cgi # argument_value name)

let file_argument (dirpath : string) = fun (cgi: Netcgi.cgi) name ->
  let arg = cgi # argument name in
  match arg#store, arg#filename with
  | `File filepath, Some filename ->
    prerr_endline ("Received file: " ^ filepath);
    let timestamp = string_of_int (int_of_float (Unix.time ())) in
    let new_filepath = dirpath // (timestamp ^ "_" ^ filename) in
    prerr_endline ("Copied to: " ^ new_filepath);
    ignore (Sys.command ("cp " ^ filepath ^ " " ^ new_filepath));
    new_filepath
  | _ -> failwith ("Argument '" ^ name ^ "' was expected as an uploaded file, not a string")

(* -------------------------------------------------------------- *)
(* Helper functions for building xml answers *)

let xml_ok = fun ?(attrs = []) ?(children = []) tag ->
  let safe_attrs = List.map (fun (x, y) -> x, xml_string y) attrs in
  let attrs' = ("status", "ok") :: safe_attrs in
  Xml.Element (tag, attrs', children)


let xml_error = fun tag xml_msg ->
  Xml.Element (tag,
               ["status", "error"],
               xml_msg)


(* -------------------------------------------------------------- *)
(* Convert users and stores structures from/to string *)

let string_of_list_user_role l =
  String.concat " " (List.map (fun (u,r) -> u ^ ":" ^ Users.string_of_role r) l)

let list_user_role_of_string s =
  if s = "none"
  then []
  else
    let l = Str.split (Str.regexp "[,: ]+") s in
    let rec aux = function
      | u::r::l -> (u, Users.role_of_string r) :: aux l
      | _ -> [] in
    aux l

let xml_of_store_description (filename, role, config) =
  let admin_attrs =
    if role = Users.Admin
    then ["defaultRole",Users.string_of_role config.Stores.default_role;
	  "listUserRole",string_of_list_user_role config.Stores.list_user_role;
	  "removedStore",string_of_bool config.Stores.removed]
    else [] in
  Xml.Element ("store", ("storeName", filename) :: ("role", Users.string_of_role role) :: admin_attrs,
	       [pcdata config.Stores.title])

(* -------------------------------------------------------------- *)
(* Convert Sewelis structures to xml *)

let string_of_option func = function
  | None -> "none"
  | Some x -> func x

let xml_of_option xml_func = function
  | None -> Xml.Element ("none",[],[])
  | Some x -> xml_func x


let xml_of_term = fun t ->
  match t with
  | Rdf.URI uri -> Xml.Element ("URI", [], [pcdata uri])
  | Rdf.XMLLiteral xml -> Xml.Element ("XMLLiteral", [], [xml])
  | Rdf.Literal (s, Rdf.Plain "") -> Xml.Element ("Literal", [], [pcdata s])
  | Rdf.Literal (s, Rdf.Plain lang) -> Xml.Element ("Literal", ["lang", lang], [pcdata s])
  | Rdf.Literal (s, Rdf.Typed dt) -> Xml.Element ("Literal", ["datatype", xml_string dt], [pcdata s])
  | Rdf.Blank id -> Xml.Element ("Blank", [], [pcdata id])


let xml_of_results results =
  Xml.Element ("results", [],
	       [Xml.Element ("columns", [],
			     List.map
			       (fun col -> Xml.Element ("column", [], [pcdata col]))
			       results.Sewelis.results_columns);
		Xml.Element ("rows", [],
			     List.map
			       (fun row -> Xml.Element ("row", [], List.map xml_of_term row))
			       results.Sewelis.results_rows)])
    
let string_of_uri_kind = fun k ->
  match k with
  | `Entity -> "entity"
  | `Class -> "class"
  | `Property -> "property"
  | `Functor -> "functor"
  | `Datatype -> "datatype"

let rec xml_of_display = fun l ->
  Xml.Element ("display", [], (List.map xml_of_token l))
and xml_of_token = fun token ->
  match token with
  | `Space -> Xml.Element ("Space", [], [])
  | `Kwd s -> Xml.Element ("Kwd", [], [pcdata s])
  | `Var v -> Xml.Element ("Var", [], [pcdata v])
  | `URI (uri, kind, s, None) ->
     Xml.Element ("URI", ["uri", xml_string uri; "kind", string_of_uri_kind kind], [pcdata s])
  | `URI (uri, kind, s, Some img_uri) ->
     Xml.Element ("URI",
                  ["uri", xml_string uri; "kind", string_of_uri_kind kind; "image", xml_string img_uri],
                  [pcdata s])
  | `Prim s -> Xml.Element ("Prim", [], [pcdata s])
  | `Plain (s, "") -> Xml.Element ("Plain", [], [pcdata s])
  | `Plain (s, lang) -> Xml.Element ("Plain", ["lang", lang], [pcdata s])
  | `Typed (s, uri, "") -> Xml.Element ("Typed", ["uri", xml_string uri], [pcdata s])
  | `Typed (s, uri, s_dt) ->
     Xml.Element ("Typed", ["uri", xml_string uri; "datatype", xml_string s_dt], [pcdata s])
  | `Xml xml -> Xml.Element ("Xml", [], [xml])
  | `List ltoks -> Xml.Element ("List", [], List.map xml_of_display ltoks)
  | `Tuple ltoks -> Xml.Element ("Tuple", [], List.map xml_of_display ltoks)
  | `Seq ltoks -> Xml.Element ("Seq", [], List.map xml_of_display ltoks)
  | `And ltoks -> Xml.Element ("And", [], List.map xml_of_display ltoks)
  | `Or ltoks -> Xml.Element ("Or", [], List.map xml_of_display ltoks)
  | `Not toks -> Xml.Element ("Not", [], [xml_of_display toks])
  | `Maybe toks -> Xml.Element ("Maybe", [], [xml_of_display toks])
  | `Brackets toks -> Xml.Element ("Brackets", [], [xml_of_display toks])
  | `Quote toks -> Xml.Element ("Quote", [], [xml_of_display toks])
  | `Pair (force_indent, toks1, toks2) ->
     Xml.Element ("Pair",
                  ["forceIndent", string_of_bool force_indent],
                  [xml_of_display toks1; xml_of_display toks2])
  | `Focus (foc, toks) -> Xml.Element ("Focus",
                                       ["id", string_of_int foc],
                                       [xml_of_display toks])

let xml_of_focused_display = fun (stat : Sewelis.focused_display) ->
  Xml.Element
    ("focusedDisplay",
     ["focusId", string_of_int stat.Sewelis.focus],
     [xml_of_display stat.Sewelis.display])


let xml_of_ns = fun (prefix, ns) ->
    Xml.Element ("namespaceDefinition", ["prefix", prefix; "namespace", xml_string ns], [])

let string_of_place_mode = function
  | `Root -> "root"
  | `Type -> "type"
  | `Val -> "val"
  | `Relaxed -> "relaxed"

let xml_of_relaxation = fun (relax : Sewelis.place_relaxation) ->
  Xml.Element
    ("relaxation",
     ["rank", string_of_int relax.Sewelis.rank;
      "hasMore", string_of_bool relax.Sewelis.has_more;
      "hasLess", string_of_bool relax.Sewelis.has_less],
     [])

let xml_of_statement = fun (stat : Sewelis.place_statement) ->
  Xml.Element
    ("statement",
     [],
     [Xml.Element ("string", [], [pcdata stat.Sewelis.string]);
      xml_of_focused_display stat.Sewelis.focused_display])

let xml_of_paging = fun (paging : Sewelis.answers_paging) ->
  Xml.Element
    ("paging",
     ["count", string_of_int paging.Sewelis.count;
      "start", string_of_int paging.Sewelis.start;
      "end", string_of_int paging.Sewelis.ende;
      "size", string_of_int paging.Sewelis.size],
     [])

let string_of_column_order = function
  | `DEFAULT -> "default"
  | `DESC -> "desc"
  | `ASC -> "asc"

let string_of_column_aggreg = function
    | `INDEX -> "index"
    | `DISTINCT_COUNT -> "distinct_count"
    | `SUM -> "sum"
    | `AVG -> "avg"

let xml_of_column = fun (column : Sewelis.place_column) ->
  Xml.Element
    ("column",
     ["name", xml_string (column.Sewelis.name);
      "hidden", string_of_bool column.Sewelis.hidden;
      "order", string_of_column_order column.Sewelis.order;
      "pattern", xml_string (column.Sewelis.pattern);
      "aggreg", string_of_column_aggreg column.Sewelis.aggreg],
     [])

let xml_of_columns = fun (cols : Sewelis.place_column list) ->
  Xml.Element
    ("columns", [], List.map xml_of_column cols)

(*
let xml_of_column = fun place column ->
  let hidden = string_of_bool (Sewelis.column_hidden ~place ~column) in
  (* TODO: add other properties: order, pattern, aggreg *)
  let order = match Sewelis.column_order ~place ~column with
    | `DEFAULT -> "default"
    | `DESC -> "desc"
    | `ASC -> "asc" in
  let pattern = Sewelis.column_pattern ~place ~column in
  let aggreg = match Sewelis.column_aggreg ~place ~column with
    | `INDEX -> "index"
    | `DISTINCT_COUNT -> "distinct_count"
    | `SUM -> "sum"
    | `AVG -> "avg" in
  let attrs = ["hidden", hidden;
               "order", order;
               "pattern", pattern;
               "aggreg", aggreg] in
  Xml.Element ("column", attrs, [pcdata column])
*)

let xml_of_cell = fun (cell : Sewelis.place_cell) ->
  Xml.Element
    ("cell",
     ["cellId", string_of_int cell.Sewelis.id],
     [xml_of_display cell.Sewelis.cell_display])

(*
let xml_of_cell = fun cell_id ->
  let attrs = ["cellId", string_of_int cell_id] in
  Xml.Element ("cell", attrs, [])
*)

let xml_of_row = fun (row : Sewelis.place_row) ->
  Xml.Element ("row", [], List.map xml_of_cell row)

(*
let xml_of_cell_row = fun row ->
  let rows = List.map xml_of_cell row in
  Xml.Element ("row", [], rows)
*)

let xml_of_rows = fun (rows : Sewelis.place_row list) ->
  Xml.Element ("rows", [], List.map xml_of_row rows)

let xml_of_answers = fun (ans : Sewelis.place_answers) ->
  Xml.Element
    ("answers",
     [],
     [xml_of_paging ans.Sewelis.paging;
      xml_of_columns ans.Sewelis.columns;
      xml_of_rows ans.Sewelis.rows])

let string_of_increment_kind =
  let open Lisql_feature in
  function
  | Kind_Something -> "something"
  | Kind_Variable -> "variable"
  | Kind_Entity -> "entity"
  | Kind_Literal -> "literal"
  | Kind_Thing -> "thing"
  | Kind_Class -> "class"
  | Kind_Operator -> "operator"
  | Kind_Property -> "property"
  | Kind_InverseProperty -> "inverseProperty"
  | Kind_Structure -> "structure"
  | Kind_Argument -> "argument"

let xml_of_increment = fun (incr : Sewelis.place_increment) ->
  Xml.Element
    ("increment",
     ["incrementId", string_of_int incr.Sewelis.increment_id;
      "kind", string_of_increment_kind incr.Sewelis.kind;
      "ratioLeft", string_of_int (fst incr.Sewelis.ratio);
      "ratioRight", string_of_int (snd incr.Sewelis.ratio);
      "isNew", string_of_bool incr.Sewelis.is_new;
      "uri", string_of_option xml_string incr.Sewelis.uri_opt;
      "isLisql", string_of_bool incr.Sewelis.is_lisql],
     [xml_of_display incr.Sewelis.increment_display;
      xml_of_option xml_of_term incr.Sewelis.term_opt])

let rec xml_of_increment_tree = function
  | Sewelis.Node (incr, subtrees) ->
    Xml.Element
      ("node",
       [],
       xml_of_increment incr ::
	 List.map xml_of_increment_tree subtrees)

(*
let xml_of_increment = fun id ->
  Xml.Element ("increment", ["id", string_of_int id], [])


let rec xml_of_increment_tree = fun incr_tree ->
  match incr_tree with
  | Sewelis.Node (id, []) -> xml_of_increment id
  | Sewelis.Node (id, subtrees) ->
     Xml.Element ("increment", ["id", string_of_int id],
                  List.map xml_of_increment_tree subtrees)
*)

let xml_of_transformation = fun t ->
  let open Lisql.Transf in
  let tag =
    match t with
      | FocusUp -> "FocusUp"
      | FocusDown -> "FocusDown"
      | FocusLeft -> "FocusLeft"
      | FocusRight -> "FocusRight"
      | FocusTab -> "FocusTab"
      | InsertForEach -> "InsertForEach"
      | InsertCond -> "InsertCond"
      | InsertSeq -> "InsertSeq"
      | InsertAnd -> "InsertAnd"
      | InsertOr -> "InsertOr"
      | InsertAndNot -> "InsertAndNot"
      | InsertAndMaybe -> "InsertAndMaybe"
      | InsertIsThere -> "InsertIsThere"
      | ToggleQu Lisql.AST.An -> "ToggleAn"
      | ToggleQu Lisql.AST.The -> "ToggleThe"
      | ToggleQu Lisql.AST.Every -> "ToggleEvery"
      | ToggleQu Lisql.AST.Only -> "ToggleOnly"
      | ToggleQu Lisql.AST.No -> "ToggleNo"
      | ToggleQu Lisql.AST.Each -> "ToggleEach"
      | ToggleNot -> "ToggleNot"
      | ToggleMaybe -> "ToggleMaybe"
      | ToggleOpt -> "ToggleOpt"
      | ToggleTrans -> "ToggleTrans"
      | ToggleSym -> "ToggleSym"
      | Describe -> "Describe"
      | Select -> "Select"
      | Delete -> "Delete" in
  Xml.Element (tag, [], [])

let xml_of_transformations = fun (transfs : Sewelis.transf list) ->
  Xml.Element ("transformations", [], List.map xml_of_transformation transfs)

let xml_of_suggestions = fun (sugg : Sewelis.place_suggestions) ->
  Xml.Element
    ("suggestions",
     ["canInsertEntity", string_of_bool sugg.Sewelis.can_insert_entity;
      "canInsertRelation", string_of_bool sugg.Sewelis.can_insert_relation],
     [xml_of_increment_tree sugg.Sewelis.increment_tree_entity;
      xml_of_increment_tree sugg.Sewelis.increment_tree_relation;
      xml_of_transformations sugg.Sewelis.transformations])

let xml_of_place = fun (place : Sewelis.place_content) ->
  Xml.Element
    ("place",
     ["placeId", string_of_int place.Sewelis.place_id;
      "mode", string_of_place_mode place.Sewelis.mode],
     [xml_of_relaxation place.Sewelis.relaxation;
      xml_of_statement place.Sewelis.statement;
      xml_of_answers place.Sewelis.answers;
      xml_of_suggestions place.Sewelis.suggestions])


let xml_of_completions = fun (completions : Sewelis.completions) ->
  Xml.Element
    ("completions",
     ["partial", string_of_bool completions.Sewelis.partial;
      "relaxed", string_of_bool completions.Sewelis.relaxed],
     List.map xml_of_increment completions.Sewelis.completion_increments)

(* Convert XML to Sewelis structures *)

exception Not_a_valid_rdf_term of string

let rdf_of_xmlstring = fun s ->
  let xml = Xml.parse_string s in
  match xml with
  | Xml.Element ("URI", [], [Xml.PCData uri]) -> Rdf.URI uri
  | Xml.Element ("XMLLiteral", [], [Xml.PCData x]) ->
     Rdf.XMLLiteral (Xml.parse_string x)
  | Xml.Element ("Literal", [], [Xml.PCData s]) ->
     Rdf.Literal (s, Rdf.Typed Xsd.uri_string)
  | Xml.Element ("Literal", ["lang", lang], [Xml.PCData s]) ->
     Rdf.Literal (s, Rdf.Plain lang)
  | Xml.Element ("Literal", ["datatype", dt], []) ->
     Rdf.Literal (s, Rdf.Typed (url_decode dt))
  | Xml.Element ("Blank", [], [Xml.PCData id]) -> Rdf.Blank id
  | _ -> raise (Not_a_valid_rdf_term s)

exception Not_a_valid_order of string

let column_order_of_xmlstring = function
  | "default" -> `DEFAULT
  | "desc" -> `DESC
  | "asc" -> `ASC
  | x -> raise (Not_a_valid_order x)

exception Not_a_valid_aggreg of string

let column_aggreg_of_xmlstring = function
  | "index" -> `INDEX
  | "distinct_count" -> `DISTINCT_COUNT
  | "sum" -> `SUM
  | "avg" -> `AVG
  | x -> raise (Not_a_valid_aggreg x)

exception Not_a_valid_transformation of string

let transformation_of_string = fun s ->
  let open Lisql.Transf in
  match s with
  | "FocusUp" -> FocusUp
  | "FocusDown" -> FocusDown
  | "FocusLeft" -> FocusLeft
  | "FocusRight" -> FocusRight
  | "FocusTab" -> FocusTab
  | "InsertForEach" -> InsertForEach
  | "InsertCond" -> InsertCond
  | "InsertSeq" -> InsertSeq
  | "InsertAnd" -> InsertAnd
  | "InsertOr" -> InsertOr
  | "InsertAndNot" -> InsertAndNot
  | "InsertAndMaybe" -> InsertAndMaybe
  | "InsertIsThere" -> InsertIsThere
  | "ToggleAn" -> ToggleQu Lisql.AST.An
  | "ToggleThe" -> ToggleQu Lisql.AST.The
  | "ToggleEvery" -> ToggleQu Lisql.AST.Every
  | "ToggleOnly" -> ToggleQu Lisql.AST.Only
  | "ToggleNo" -> ToggleQu Lisql.AST.No
  | "ToggleEach" -> ToggleQu Lisql.AST.Each
  | "ToggleNot" -> ToggleNot
  | "ToggleMaybe" -> ToggleMaybe
  | "ToggleOpt" -> ToggleOpt
  | "ToggleTrans" -> ToggleTrans
  | "ToggleSym" -> ToggleSym
  | "Describe" -> Describe
  | "Select" -> Select
  | "Delete" -> Delete
  | _ -> raise (Not_a_valid_transformation s)
