OCAMLC   = ocamlfind ocamlc
OCAMLOPT = ocamlfind ocamlopt -w -40

# Path to sewelis source tree. It is assumed to be in the parent
# directory, override using 'make SEWELIS_SRC=/path/to/sewelis' if this
# is not the case
SEWELIS_SRC = ..

#INCLUDES = -package threads,num,str,nethttpd,xml-light,sewelis
# add nettls-gnutls for TLS-based http
INCLUDES = -package unix,threads,num,str,nethttpd,csv,xml-light,xmlm,cryptokit -I $(SEWELIS_SRC)/src -I $(SEWELIS_SRC)/ocaml-lib -I $(SEWELIS_SRC)/ocaml-lib/dcg -I $(SEWELIS_SRC)/ocaml-lib/ipp
PP =
FLAGS = -g -thread $(PP) $(INCLUDES)    # add other options for ocamlc here

SRC = utils.ml users.ml stores.ml xmlutils.ml main_sewelis.ml

OBJECTS  = $(SRC:%.ml=%.cmo)
XOBJECTS = $(OBJECTS:%.cmo=%.cmx)

LISQL_COMMON = common.ml unicode.ml bintree.ml lSet.ml cis.ml iterator.ml intmap.ml intset.ml intrel2.ml intreln.ml text_index.ml
LISQL_IPP = ipp.ml printer.ml
LISQL_DCG = msg.ml dcg.ml matcher.ml
LISQL_SRC = option.ml tarpit.ml uri.ml xsd.ml rdf.ml rdfs.ml owl.ml foaf.ml strdf.ml term.ml turtle.ml ntriples.ml name.ml builtins.ml builtins_basic.ml builtins_temporal.ml extension.ml code.ml store_order.ml store_match.ml store.ml fol.ml log.ml lisql_namespace.ml lisql_ast.ml lisql_syntax.ml lisql_display.ml lisql_semantics.ml lisql_transf.ml lisql_feature.ml lisql_index.ml lisql_concept.ml lisql.ml func_api.ml
LISQL_XOBJECTS = $(LISQL_COMMON:.ml=.cmx) $(LISQL_IPP:.ml=.cmx) $(LISQL_DCG:.ml=.cmx) $(LISQL_SRC:.ml=.cmx)

server: $(XOBJECTS)
	$(OCAMLOPT) $(FLAGS) -o sewelis-xml-server.exe -linkpkg $(LISQL_XOBJECTS) $^

server-java: $(XOBJECTS)
	$(OCAMLOPT) $(FLAGS) -I $(SEWELIS_SRC)/ocaml-lib/camljava/lib -o sewelis-java-xml-server.exe -linkpkg jni.cmxa $(LISQL_XOBJECTS) jni_common.cmx jts.cmx builtins_spatial.cmx $^

# ----------------------------------------------------------------------
# Common rules

%.cmi: %.mli
	$(OCAMLC) $(FLAGS) -c $<

%.cmo: %.ml
	$(OCAMLC) $(FLAGS) -c $<

%.cmx: %.ml
	$(OCAMLOPT) $(FLAGS) -c $<

%.cmxs: %.cmxa
	$(OCAMLOPT) -shared -linkall -o $@ $<

#users.cmo: users.cmi
#users.cmx: users.cmi

# ----------------------------------------------------------------------
# Clean up

.PHONY: clean

clean:
	rm -f *.cm[ioax]
	rm -f *.o
	rm -f *.a
	rm -f *.cmx[as]
	rm -f *.exe
	rm -f *.zip


# ----------------------------------------------------------------------
#  Run server

.PHONY: run run-test

run: sewelis-xml-server.exe
	./sewelis-xml-server.exe -port 9999 -storedir /home/ferre/data/sewelis -base "http://www.irisa.fr/LIS/sewelis/"

run-java: sewelis-java-xml-server.exe
	./sewelis-java-xml-server.exe -port 9999 -storedir /home/ferre/data/sewelis -base "http://www.irisa.fr/LIS/sewelis/"

TESTDIR = /tmp/movies

runtest: sewelis-xml-server
	rm -rf $(TESTDIR)
	mkdir -p $(TESTDIR)
	install movies.rdf $(TESTDIR)
	xdg-open test.html
	./sewelis-xml-server.exe -port 9999 -key 123 -storedir $(TESTDIR)
