
module Sewelis = Func_api

open Utils

(* ---------------------------------------------------------------- *)
(* Store list management *)

let stores_csv = "stores.csv"
let _users_csv = "_users.csv"

type store_name = string (* basename of Sewelis files *)
  
type store_config = { mutable title : string; (* store title for display *)
		      mutable default_role : Users.user_role; (* default role for anonymous users and logged in users *)
		      mutable list_user_role : (Users.user_login * Users.user_role) list; (* registered user role *)
		      mutable removed : bool; (* when removed, not destroyed but no more visible *)
		    }

let stores : (store_name * store_config) list ref = ref []

exception No_such_store of store_name
exception Already_used_store of store_name

(* persistency *)

let load () =
  try
    let csv = try Csv.load (!server_dir // stores_csv) with _ -> [] in
    let l_stores =
      List.map
	(function
	| [ name; s_removed; s_default_role; title] ->
	  let csv_users = try Csv.load (!server_dir // (name ^ _users_csv)) with _ -> [] in
	  let list_user_role =
	    List.map
	      (function
	      | [user; role] -> (user, Users.role_of_string role)
	      | l -> failwith ("in " ^ name ^ _users_csv ^ ": misformed line: " ^ String.concat "," l))
	      csv_users in
	  (name, { removed = bool_of_string s_removed; title; default_role = Users.role_of_string s_default_role; list_user_role })
	| l -> failwith ("in stores.csv: misformed line: " ^ String.concat "," l))
	csv in
    stores := l_stores
  with exn ->
    prerr_endline (Printexc.to_string exn);
    exit 1

let save () =
  let csv =
    List.map
      (fun (name, config) ->
	let csv_users =
	  List.map
	    (fun (user,role) -> [user; Users.string_of_role role])
	    config.list_user_role in
	Csv.save (!server_dir // (name ^ _users_csv)) csv_users;
	[name; string_of_bool config.removed; Users.string_of_role config.default_role; config.title])
      !stores in
  Csv.save (!server_dir // stores_csv) csv

(* access and update functions *)

(* returns the configuration of a store *)
let get_store_config name =
  try List.assoc name !stores
  with _ -> raise $ No_such_store name
    
let get_role_from_config user config =
  try List.assoc user config.list_user_role
  with _ -> config.default_role

let get_role name user =
  try
    let config = get_store_config name in
    get_role_from_config user config
  with _ -> raise $ No_such_store name
    

type store_description = store_name * Users.user_role * store_config (* name, role, config *)
    
(* returns the list of visible stores for [user], with the associated role and title *)
let visible_stores (user : Users.user_login) : store_description list =
  List.fold_left
    (fun acc (name,config) ->
      let role = get_role_from_config user config in
      if config.removed || role = Users.No_right
      then acc
      else (name, role, config) :: acc)
    [] !stores

(* returns the list of stores removed by [admin], along with title *)
let removed_stores (admin: Users.user_login) : store_description list =
  List.fold_left
    (fun acc (name,config) ->
      let role = get_role_from_config admin config in
      if config.removed && role = Users.Admin
      then (name, role, config) :: acc
      else acc)
    [] !stores

let normalize_list_user_role admin l =    
  try
    match List.assoc admin l with
    | Users.Admin | Users.General_admin -> l
    | _ -> (admin, Users.Admin) :: List.remove_assoc admin l
  with _ -> (admin, Users.Admin) :: l

(* creation of a new store by [admin] with [name] and configuration info *)
let create_store ~(admin : Users.user_login)
    ~(name : store_name) ~(default_role : Users.user_role) ~(title : string)
    ~(list_user_role : (Users.user_login * Users.user_role) list) : unit =
  if List.mem_assoc name !stores
  then raise $ Already_used_store name
  else
    let list_user_role = normalize_list_user_role admin list_user_role in
    stores := (name, {title; removed=false; default_role; list_user_role}) :: !stores

(* update of an existing store by [admin] *)
let update_store ~(admin : Users.user_login)
    ~(name : store_name) ~(default_role : Users.user_role) ~(title : string)
    ~(list_user_role : (Users.user_login * Users.user_role) list) : unit =
  try
    let config = List.assoc name !stores in
    let list_user_role = normalize_list_user_role admin list_user_role in
    config.title <- title;
    config.default_role <- default_role;
    config.list_user_role <- list_user_role
  with _ -> raise $ No_such_store name
  
(* removal of a store (by admin) *)
let remove_store ~(name : string) : unit =
  try
    let config = List.assoc name !stores in
    config.removed <- true
  with _ -> raise $ No_such_store name

(* restoration of a store (by admin) *)
let restore_store ~(name : string) : unit =
  try
    let config = List.assoc name !stores in
    config.removed <- false
  with _ -> raise $ No_such_store name


(* open stores management, resource management *)

type date = float

let default_store_timeout = 3. *. 60. *. 60. (* 3 hours, in seconds *)

type open_store = store_name * (Sewelis.store_id * date ref)
  
let open_stores : open_store list ref = ref []

let close_expired_stores () =
  let now = now () in
  open_stores :=
    List.filter (fun (name, (id, ref_expire_date)) ->
      if now > !ref_expire_date
      then begin
	prerr_endline ("Closing " ^ name);
	Sewelis.free_store ~store:id;
	false end
      else true
    ) !open_stores

let get_open_store_id name =
  try
    let id, ref_expire_date = List.assoc name !open_stores in
    ref_expire_date := now () +. default_store_timeout;
    close_expired_stores ();
    id
  with _ ->
    close_expired_stores ();
    prerr_endline ("Opening " ^ name);
    let id = Sewelis.open_store ~base:(!server_base ^ name ^ "/") ~filepath:(!server_dir // name) in
    let expire_date = now () +. default_store_timeout in
    open_stores := (name, (id, ref expire_date)) :: !open_stores;
    id
    
