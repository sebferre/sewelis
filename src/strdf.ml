(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* STRDF vocabulary *)

(* namespace *)
let prefix = "strdf:"
let namespace = "http://strdf.di.uoa.gr/ontology#"
(* datatypes *)
let uri_period = namespace ^ "period"
let uri_interval = namespace ^ "interval"
let uri_WKT = namespace ^ "WKT"
let uri_GML = namespace ^ "GML"
let uri_geometry = namespace ^ "geometry"
