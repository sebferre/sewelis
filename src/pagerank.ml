(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

class page (name : string) =
  object
    val mutable prob = 1. (* probability out of the number of pages *)
(*    val mutable next_prob = 0.*)
    val mutable succs : page list = []
    val mutable preds : page list = []

    method name = name
    method prob = prob
    method nb_succs = List.length succs

    method set_next_prob p = prob <- p (* next_prob <- p *)
    method tick = () (* prob <- next_prob *)

    method add_pred ~unique (pred : page) =
      if not unique || not (List.mem pred preds)
      then preds <- pred::preds

    method add_succ ~unique (succ : page) =
      if not unique || not (List.mem succ succs)
      then succs <- succ::succs

    method fold_pred : 'a.('a -> page -> 'a) -> 'a -> 'a =
      fun f init ->
	List.fold_left f init preds
  end

class store =
  object (self)
    val pages : (string,page) Hashtbl.t = Hashtbl.create 1013

    method private get_page (name : string) =
      try Hashtbl.find pages name
      with _ ->
	let page = new page name in
	Hashtbl.add pages name page;
	page

    method nb_pages = Hashtbl.length pages

    method add_link ~unique x y =
      let px = self#get_page x in
      let py = self#get_page y in
      px#add_succ ~unique py;
      py#add_pred ~unique px

    method fold : 'a.('a -> page -> 'a) -> 'a -> 'a =
      fun f init ->
	Hashtbl.fold (fun name page res -> f res page) pages init

    method iter : (page -> unit) -> unit = 
      fun f ->
	Hashtbl.iter (fun name page -> f page) pages
  end

let triples_of_xml filename =
  let base = "" in
  let xml = Xml.parse_file filename in
  let rdf = Rdf.from_xml base xml in
  Rdf.triples_of_rdf rdf

let triples_of_ttl filename =
  let turtle = Turtle.parse_file filename in
  let base = "" in
  Turtle.triples_of_turtle base turtle

let triples_of_rdf filename =
  if Filename.check_suffix filename ".rdf" then
    triples_of_xml filename
  else if Filename.check_suffix filename ".ttl" then
    triples_of_ttl filename
  else failwith "unknown extension"

let name = function
  | Rdf.URI uri -> "<" ^ uri ^ ">"
  | Rdf.Literal (s,_) -> "\"" ^ s ^ "\""
  | Rdf.Blank id -> "_:" ^ id
  | Rdf.XMLLiteral _ -> "XML"

let preprocess triples =
  let store = new store in
  List.iter
    (fun triple ->
      let s, p, o = triple.Rdf.s, triple.Rdf.p, triple.Rdf.o in
      if p = Rdf.uri_type
      then begin
	let ns, nc = name s, "C " ^ name o in
	store#add_link ~unique:true ns nc;
	store#add_link ~unique:true nc ns;
	() end
      else begin
	let ns, np, no = name s, "P " ^ name (Rdf.URI p), name o in
	store#add_link ~unique:true no ns; (* we like resources with many descriptors *)
	store#add_link ~unique:true np ns; (* we like resources with many properties *)
	store#add_link ~unique:true ns no; (* we like resources with many antecedents *)
	store#add_link ~unique:true np no; (* we like resources with many antecedents *)
	store#add_link ~unique:true ns np; (* we like properties with many subjects *)
(*	store#add_link no np; *) (* we prefer properties with fewer values *)
	() end)
    triples;
(*
  let n = store#nb_pages in
  store#iter
    (fun (page : page) -> page#set_next_prob (1. /. float n); page#tick);
*)
  store

let iteration store =
  let d = 0.85 in
(*  let n = store#nb_pages in *)
  store#iter
    (fun (page : page) ->
      let sum =
	page#fold_pred
	  (fun res pred -> res +. pred#prob /. float pred#nb_succs)
	  0. in
      page#set_next_prob ((1. -. d) +. (d *. sum)))
(*
  store#iter
    (fun (page : page) -> page#tick)
*)

let display (store : store) =
  let n = store#nb_pages in
  let l =
    store#fold
      (fun res (page : page) -> (page#prob, page#name)::res)
      [] in
  let l = List.sort Pervasives.compare l in
  List.iter
    (fun (prob,name) -> Printf.printf "%.3f\t%s\n" prob name)
    l;
  Printf.printf "#pages: %d\n" n

let main filename n =
  let triples = triples_of_rdf filename in
  let store = preprocess triples in
  for i = 1 to n do
    iteration store
  done;
  display store

let _ =
  let filename = Sys.argv.(1) in
  let n = int_of_string (Sys.argv.(2)) in
  main filename n

