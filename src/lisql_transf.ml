(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Lisql_ast

(* transformations *)

let focus_default_filter : focus -> bool = function
  | AtC (Nope,_)
  | AtC (Seq _,_)
  | AtS (True,_)
  | AtS (SAnd _,_)
  | AtS1 (Det (Qu _,_),_)
  | AtS1 (NAnd _,_)
  | AtP1 (Thing,_)
  | AtP1 (And _,_)
    -> true
  | _ -> false

let rec merge_s2 d1 d2 =
  match d1, d2 with
  | Name n1, Name n2 ->
      if n1 = n2
      then top_s2
      else d2
  | Ref v1, Ref v2 ->
      if v1 = v2
      then top_s2
      else d2
  | Qu (qu1,vopt1), Qu (qu2,vopt2) ->
      Qu (merge_qu qu1 qu2, merge_vopt vopt1 vopt2)
  | _ -> d2
and merge_qu qu1 qu2 =
  match qu1, qu2 with
  | _, An -> qu1
  | _ -> qu2
and merge_vopt vopt1 vopt2 =
  match vopt1, vopt2 with
  | Some _, None -> vopt1
  | _ -> vopt2


let focus_up_and foc =
  match foc with
    | AtC (_, SeqN _)
    | AtS (_, SAndN _)
    | AtS1 (_, NAndN _)
    | AtP1 (_, AndN _) -> (match focus_up foc with None -> foc | Some foc -> foc)
    | _ -> foc

let focus_up_or foc =
  match foc with
  | AtS (_, SOrN _)
  | AtS1 (_, NOrN _)
  | AtP1 (_, OrN _) -> (match focus_up foc with None -> foc | Some foc -> foc)
  | _ -> foc

	      (* adds a fresh variable to the concept *)
let rec focus_name store v : transf = function
  | AtS1 (Det (Qu (qu, None), f), k) ->
      Some (AtS1 (Det (Qu (qu, Some v), f), k))
  | AtP1 (f, Det1 (Qu (qu, None), _, k)) ->
      Some (AtP1 (f, Det1 (Qu (qu, Some v), f, k)))
  | _ -> None

let focus_subst (subst : (var * Name.t) list) : transf =
  fun foc ->
    let c = command_of_focus foc in
    let env = List.map (fun (v,n) -> (v, Name n)) subst in
    let c' = subst_c env c in
    Some (focus_of_command c')

let focus_describe store : transf = function
  | AtS1 (Det (Name n, _), k) ->
      let np = store#description ~obs:Tarpit.blind_observer n in
      Some (AtS1 (np, k))
  | _ -> None

	      (* intelligent conjunctions, removing redundant, trivial
		 and contradictory elements from the query. *)

(* TODO: combine with focus_and ? *)
(*
let focus_inter_smart store (x : p1) : transf =
  let rec zoom (add,rev_l) = function
    | And lf ->
	List.fold_left
	  (fun (add,rev_l) fi -> zoom (add,rev_l) fi)
	  (add,rev_l)
	  lf
    | f ->
	let f_x = entails_p1 ~obs:Tarpit.blind_observer store f x in
	let x_f = entails_p1 ~obs:Tarpit.blind_observer store x f in
	add && not (f_x && x_f),
	if f_x || x_f || contradicts_p1 ~obs:Tarpit.blind_observer store f x
	then rev_l
	else f::rev_l
  in
  fun foc ->
    match focus_up_and foc with
    | AtP1 (f,k) ->
	let add, rev_l = zoom (true,[]) f in
	let rev_l = if add then x::rev_l else rev_l in
	let f', k' =
	  match rev_l with
	  | [] -> Thing, k
	  | [f] -> f, k
	  | _ -> List.hd rev_l, AndN (List.length rev_l - 1, List.rev rev_l, k) in
	Some (AtP1 (f',k'))
    | AtS1 (np,k) ->
	let npx = an x in
	( match np with
	| NAnd l -> Some (AtS1 (npx, NAndN (List.length l, l @ [npx], k)))
	| Det (Qu (An,_), Thing) -> Some (AtS1 (npx, k))
	| _ -> Some (AtS1 (npx, NAndN (1, [np; npx], k))))
    | _ -> assert false
*)

let rec focus_select : transf = function
  | AtC (c,k) ->
      Some (AtC (c, focus_select_c c k))
  | AtS (s,k) ->
      Some (AtS (s, focus_select_s s k))
  | AtS1 (np,k) ->
      Some (AtS1 (np, focus_select_s1 np k))
  | AtS2 (det,k) ->
      Some (AtS2 (det, focus_select_s2 det k))
  | AtP1 (f,k) ->
      Some (AtP1 (f, focus_select_p1 f k))
and focus_select_c c = function
  | SeqN (_,_,k') -> k'
  | Cond1 (_,_,k') -> k'
  | Cond2 (_,_,k') -> k'
  | For2 (_,k') -> k'
  | _ -> default_context_of_c c
and focus_select_s s = function
  | SAndN (_,_,k') -> k'
  | SOrN (_,_,k') -> k'
  | SNot0 (_,k') -> k'
  | SMaybe0 (_,k') -> k'
  | _ -> default_context_of_s s
and focus_select_s1 np = function
  | NAndN (_,_,k') -> k'
  | NOrN (_,_,k') -> k'
  | NNot0 (_,k') -> k'
  | NMaybe0 (_,k') -> k'
  | _ -> default_context_of_s1 np
and focus_select_s2 det = function
  | _ -> default_context_of_s2 det
and focus_select_p1 f = function
  | AndN (_,_,k') -> k'
  | OrN (_,_,k') -> k'
  | Not0 (_,k') -> k'
  | Maybe0 (_,k') -> k'
  | _ -> default_context_of_p1 f

let rec focus_delete : transf =
  fun foc ->
    match foc with
      | AtC (c,k) -> focus_delete_c k
      | AtS (s,k) -> focus_delete_s k
      | AtS1 (Det ((Name _ | Quote _ | Ref _ as det), f), k) when f<>Thing -> Some (AtS1 (Det (det, Thing), k))
      | AtS1 (np,k) -> focus_delete_s1 k
      | AtS2 (det,k) -> focus_delete_s2 k
      | AtP1 (f,k) -> focus_delete_p1 k
and focus_delete_c k =
  match k with
    | For2 (np,k') -> Some (AtS1 (np, Get0 k'))
    | SeqN (n,l,k') ->
      let c' =
	match Common.list_remove_nth l n with
	  | [] -> top_c
	  | [c] -> c
	  | l' -> Seq l' in
      Some (AtC (c',k'))
    | Quote0 k' -> focus_delete_s2 k'
    | _ -> Some (AtC (top_c,k))
and focus_delete_s k =
  match k with
    | Assert0 k' -> focus_delete_c k'
    | Cond0 (c1,c2,k') when c2 = Nope || c2 = top_c -> Some (AtC (c1,k'))
    | SuchThat0 (v,s,k') -> focus_delete_p1 k'
    | SAndN (n,l,k') ->
      let s' =
	match Common.list_remove_nth l n with
	| [] -> top_s
	| [s] -> s
	| l' -> SAnd l' in
      Some (AtS (s',k'))
    | SOrN (n,l,k') ->
      let s' =
	match Common.list_remove_nth l n with
	  | [] -> top_s
	  | [s] -> s
	  | l' -> SOr l' in
      Some (AtS (s',k'))
    | SNot0 (_,k') -> focus_delete_s k'
    | SMaybe0 (_,k') -> focus_delete_s k'
    | _ -> Some (AtS (top_s,k))
and focus_delete_s1 k =
  match k with
    | Get0 k' -> focus_delete_c k'
    | For1 (Nope,k') -> focus_delete_c k'
    | For1 (c,k') -> Some (AtC (c,k'))
    | Is0 (_,Thing,k') -> focus_delete_s k'
    | NAndN (n,l,k') ->
      let np' =
	match Common.list_remove_nth l n with
	  | [] -> top_s1
	  | [np] -> np
	  | l' -> NAnd l' in
      Some (AtS1 (np',k'))
    | NOrN (n,l,k') ->
      let np' =
	match Common.list_remove_nth l n with
	  | [] -> top_s1
	  | [np] -> np
	  | l' -> NOr l' in
      Some (AtS1 (np',k'))
    | NNot0 (_,k') -> focus_delete_s1 k'
    | NMaybe0 (_,k') -> focus_delete_s1 k'
    | _ -> Some (AtS1 (top_s1,k))
and focus_delete_s2 k =
  match k with
  | Det0 (_,Thing,k') -> focus_delete_s1 k'
  | _ -> Some (AtS2 (top_s2,k))
and focus_delete_p1 k =
  match k with
  | Is1 (Det (Qu (An,_),Thing),_,k') -> focus_delete_s k'
  | Is1 (np,_, Assert0 k') -> Some (AtS1 (np, Get0 k'))
  | Det1 (det,_,k') -> Some (AtS1 (Det (det,top_p1),k'))
  | AndN (n,l,k') ->
      let f' =
	match Common.list_remove_nth l n with
	| [] -> top_p1
	| [f] -> f
	| l' -> And l' in
      ( match k' with
      | Det1 (det,_,k'') -> Some (AtS1 (Det (det,f'),k''))
      | _ -> Some (AtP1 (f',k')) )
  | OrN (n,l,k') ->
      let f' =
	match Common.list_remove_nth l n with
	| [] -> top_p1
	| [f] -> f
	| l' -> Or l' in
      ( match k' with
      | Det1 (det,_,k'') -> Some (AtS1 (Det (det,f'),k''))
      | _ -> Some (AtP1 (f',k')) )
  | Not0 (_,k') -> focus_delete_p1 k'
  | Maybe0 (_,k') -> focus_delete_p1 k'
  | _ -> Some (AtP1 (top_p1,k))


let rec focus_and : transf = 
  fun foc ->
    match foc with
    | AtC _ -> focus_and_c top_c foc
    | AtS _ -> focus_and_s top_s foc
    | AtS1 _ -> focus_and_s1 top_s1 foc
    | AtS2 _ -> None
    | AtP1 _ -> focus_and_p1 top_p1 foc
and focus_and_c x foc =
  match x with
  | Seq lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_and_c_atom x foc))
	(Some foc) lx
  | _ -> focus_and_c_atom x foc
and focus_and_c_atom (x : c) : transf =
  fun foc ->
    match focus_up_and foc with
      | AtC (c,k) ->
	( match c with
	  | Nope -> Some (AtC (x,k))
	  | Seq l ->
	    if List.mem x l
	    then
	      let c' =
		match List.filter ((<>) x) l with
		  | [] -> top_c
		  | [c] -> c
		  | l' -> Seq l' in
	      Some (AtC (c', k))
	    else Some (AtC (x, SeqN (List.length l, l@[x], k)))
	  | _ ->
	    if c = top_c then Some (AtC (x,k))
	    else if x = c then Some (AtC (top_c,k))
	    else Some (AtC (x, SeqN (1, [c; x], k))))
      | _ -> None
and focus_and_s x foc =
  match x with
  | SAnd lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_and_s_atom x foc))
	(Some foc) lx
  | _ -> focus_and_s_atom x foc
and focus_and_s_atom (x : s) : transf =
  fun foc ->
    match focus_up_and foc with
    | AtS (s,k) ->
	( match s with
	| True -> Some (AtS (x,k))
	| SAnd l ->
	    if List.mem x l
	    then
	      let s' =
		match List.filter ((<>) x) l with
		| [] -> top_s
		| [s] -> s
		| l' -> SAnd l' in
	      Some (AtS (s', k))
	    else Some (AtS (x, SAndN (List.length l, l@[x], k)))
	| _ ->
	  if s = top_s then Some (AtS (x,k))
	  else if x = s then Some (AtS (top_s,k))
	  else Some (AtS (x, SAndN (1, [s; x], k))))
    | _ -> None
and focus_and_s1 x foc =
  match x with
  | NAnd lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_and_s1_atom x foc))
	(Some foc) lx
  | _ -> focus_and_s1_atom x foc
and focus_and_s1_atom (x : s1) : transf =
  fun foc ->
    match focus_up_and foc with
    | AtS1 (np,k) ->
	( match np with
	| NAnd l ->
	  if List.mem x l
	  then
	    let np' =
	      match List.filter ((<>) x) l with
	      | [] -> top_s1
	      | [np] -> np
	      | l' -> NAnd l' in
	    Some (AtS1 (np', k))
	  else Some (AtS1 (x, NAndN (List.length l, l@[x], k)))
	| _ ->
	  if np = top_s1 then Some (AtS1 (x,k))
	  else if x = np then Some (AtS1 (top_s1,k))
	  else Some (AtS1 (x, NAndN (1, [np; x], k))))
    | _ -> None
and focus_and_p1 x foc =
  match x with
  | And lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_and_p1_atom x foc))
	(Some foc) lx
  | _ -> focus_and_p1_atom x foc
and focus_and_p1_atom (x : p1) : transf =
  fun foc ->
    match focus_up_and foc with
    | AtP1 (f,k) ->
	( match f with
	| Thing -> Some (AtP1 (x,k))
	| And l ->
	    if List.mem x l
	    then
	      let f' =
		match List.filter ((<>) x) l with
		| [] -> top_p1
		| [f] -> f
		| l' -> And l' in
	      Some (AtP1 (f', k))
	    else Some (AtP1 (x, AndN (List.length l, l@[x], k)))
	| _ ->
	  if f = top_p1 then Some (AtP1 (x,k))
	  else if x = f then Some (AtP1 (top_p1,k))
	  else Some (AtP1 (x, AndN (1, [f; x], k))))
    | _ -> None

let rec focus_or : transf =
  fun foc ->
    match foc with
    | AtC _ -> None
    | AtS _ -> focus_or_s top_s foc
    | AtS1 _ -> focus_or_s1 top_s1 foc
    | AtS2 _ -> None
    | AtP1 _ -> focus_or_p1 top_p1 foc
and focus_or_s x foc =
  match x with
  | SOr lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_or_s_atom x foc))
	(Some foc) lx
  | _ -> focus_or_s_atom x foc
and focus_or_s_atom (x : s) : transf =
  fun foc ->
    match focus_up_or foc with
    | AtS (s,k) ->
	( match s with
	| True -> Some (AtS (x, k))
	| SOr l -> Some (AtS (x, SOrN (List.length l, l@[x], k)))
	| _ -> Some (AtS (x, SOrN (1, [s; x], k))) )
    | _ -> None
and focus_or_s1 x foc =
  match x with
  | NOr lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_or_s1_atom x foc))
	(Some foc) lx
  | _ -> focus_or_s1_atom x foc
and focus_or_s1_atom (x : s1) : transf =
  fun foc ->
    match focus_up_or foc with
    | AtS1 (np,k) ->
	( match np with
	| Det (Qu (An, None), Thing) -> Some (AtS1 (x,k))
	| NOr l -> Some (AtS1 (x, NOrN (List.length l, l@[x], k)))
	| _ -> Some (AtS1 (x, NOrN (1, [np;x], k))) )
    | _ -> None
and focus_or_p1 x foc =
  match x with
  | Or lx ->
      List.fold_left
	(fun res x -> Option.bind res (fun foc -> focus_or_p1_atom x foc))
	(Some foc) lx
  | _ -> focus_or_p1_atom x foc
and focus_or_p1_atom (x : p1) : transf =
  fun foc ->
    match focus_up_or foc with
    | AtP1 (f,k) ->
	( match f with
	| Thing -> Some (AtP1 (x,k))
	| Or l -> Some (AtP1 (x, OrN (List.length l, l@[x], k)))
	| _ -> Some (AtP1 (x, OrN (1, [f;x], k))) )
    | _ -> None

let rec focus_not : transf = function
  | AtC _ -> None
  | AtS (s,k) -> focus_not_s (s,k)
  | AtS1 (np,k) -> focus_not_s1 (np,k)
  | AtS2 _ -> None
  | AtP1 (f,k) -> focus_not_p1 (f,k)
and focus_not_s = function
  | SNot s1, k -> Some (AtS (s1,k))
  | s, SNot0 (_,k') -> Some (AtS (s,k'))
  | s, k ->
    if s = top_s
    then Some (AtS (True, SNot0 (s,k)))
    else Some (AtS (SNot s, k))
and focus_not_s1 = function
  | NNot np1, k -> Some (AtS1 (np1,k))
  | np, NNot0 (_,k') -> Some (AtS1 (np,k'))
  | np, k ->
    if np = top_s1
    then Some (AtS1 (np, NNot0 (np,k)))
    else Some (AtS1 (NNot np, k))
and focus_not_p1 = function
  | Not f1, k -> Some (AtP1 (f1,k))
  | f, Not0 (_,k') -> Some (AtP1 (f,k'))
  | f, k ->
    if f = top_p1
    then Some (AtP1 (f, Not0 (f,k)))
    else Some (AtP1 (Not f, k))

let focus_and_not : transf = focus_and >> focus_not

let rec focus_maybe : transf = function
  | AtC _ -> None
  | AtS (s,k) -> focus_maybe_s (s,k)
  | AtS1 (np,k) -> focus_maybe_s1 (np,k)
  | AtS2 _ -> None
  | AtP1 (f,k) -> focus_maybe_p1 (f,k)
and focus_maybe_s = function
  | SMaybe s1, k -> Some (AtS (s1,k))
  | s, SMaybe0 (_,k') -> Some (AtS (s,k'))
  | s, k ->
    if s = top_s
    then Some (AtS (s, SMaybe0 (s,k)))
    else Some (AtS (SMaybe s, k))
and focus_maybe_s1 = function
  | NMaybe np1, k -> Some (AtS1 (np1,k))
  | np, NMaybe0 (_,k') -> Some (AtS1 (np,k'))
  | np, k ->
    if np = top_s1
    then Some (AtS1 (np, NMaybe0 (np,k)))
    else Some (AtS1 (NMaybe np, k))
and focus_maybe_p1 = function
  | Maybe f1, k -> Some (AtP1 (f1,k))
  | f, Maybe0 (_,k') -> Some (AtP1 (f,k'))
  | f, k ->
    if f = top_p1
    then Some (AtP1 (f, Maybe0 (f,k)))
    else Some (AtP1 (Maybe f, k))

let focus_and_maybe : transf = focus_and >> focus_maybe

let focus_cond : transf = function
  | AtC (c,k) -> Some (AtS (top_s, Cond0 (c, top_c, k)))
  | AtS (s, Assert0 k) -> Some (AtC (top_c, Cond1 (s, top_c, k)))
  | _ -> None

let rec s1_each = function
  | Det (Qu (_, v_opt), f) -> Det (Qu (Each, v_opt), f)
  | Det (det,f) -> Det (det,f) (* names, refs, and quotes *)
  | NAnd l -> NAnd (List.map s1_each l)
  | NOr l -> NAnd (List.map s1_each l)
  | NNot np -> s1_each np
  | NMaybe np -> s1_each np

let focus_for_each : transf = function
  | AtC (c,k) -> Some (AtS1 (each Thing, For1 (c, k)))
  | AtS1 (np, Get0 k) -> Some (AtC (top_c, For2 (s1_each np, k)))
  | _ -> None

let focus_quantifier (qu : qu) : transf = function
  | AtS1 (Det (Qu (_,v_opt),f), k) ->
    let np = Det (Qu (qu,v_opt),f) in
    ( match k with
      | Get0 k' -> Some (AtP1 (top_p1, Is1 (np, top_p1, Assert0 k')))
      | _ -> Some (AtS1 (np,k)) )
(*
  | AtP1 (f, Det1 (Qu (_,v_opt), _, k)) ->  
      Some (AtP1 (f, Det1 (Qu (qu,v_opt), f, k)))
*)
  | _ -> None

let focus_is_there : transf = function
  | AtS1 (np, Get0 k) -> Some (AtP1 (Thing, Is1 (np, Thing, Assert0 k)))
  | _ -> None

(* TODO *)
(*
let rec focus_subject v : transf =
  let rec aux_s1 np_k =
    decontext_s1_gen
      ~decontext_s:(fun s_k' -> s_k')
      ~decontext_p1:(fun c_k' -> aux_p1 c_k')
      np_k
  and aux_p1 c_k =
    decontext_p1_gen
      ~decontext_s:(fun s_k' -> s_k')
      ~decontext_s1:(fun np_k' -> aux_s1 np_k')
      c_k
  in
  function
    | AtP1 (f, k) ->
	let f, k = focus_up_and_p1 (f,k) in
	( match k with
	| Det1 (det,_,k') -> focus_subject v (AtS1 (Det (det,f), k'))
	| _ -> None)
    | AtS1 (np, Is0 (_,c,NilS)) -> None
    | AtS1 (np,k) ->
	let np, c, k' =
	  match abstract_focus_s1 np v k with
	  | Some (np,c,k') -> np, c, k'
	  | None ->
	      let s, k' =
		match k with
		| Is0 (_, c, SuchThat0 (x,_,k1)) ->
		    aux_p1 (SuchThat (x, Is (Qu (Exists, Var v), c)), k1)
		| _ ->
		    aux_s1 (Qu (Exists, Var v), k) in
	      np, SuchThat (v, s), k' in
	match np, c, k' with
	| Qu (Exists, Var x1), _, SuchThat0 (x2, _, k'') when x1 = x2 ->
	    Some (AtP1 (c, k''))
	| _ ->
	    Some (AtS1 (np, Is0 (np, c, k')))
*)

(* TODO *)
(*
let focus_toggle_quote store ~src : transf = function
  | AtP1 (f,k) ->
      ( match f with
      | Quote f1 ->
	  Some (AtP1 (f1,k))
      | Name (Rdf.Literal (s, Rdf.Typed t)) when t = Lisql.uri_Class ->
	  let f1 = Syntax.class_of_string s in
	  Some (AtP1 (f1, k))
      | _ ->
	  Some (AtP1 (f, Quote0 (f, k))))
  | _ -> None
*)

let rec focus_toggle_opt store : transf = function
  | AtP1 (Arg (Pred.Role (p,modifs),i,args), k) ->
      Some (AtP1 (Arg (Pred.Role (p, {modifs with Pred.reflexive = not modifs.Pred.reflexive}),i,args), k))
  | _ -> None

let rec focus_toggle_trans store : transf = function
  | AtP1 (Arg (Pred.Role (p,modifs),i,args), k) ->
      let modifs' =
	if modifs.Pred.transitive then {modifs with Pred.transitive=false}
	else if modifs.Pred.direct then {modifs with Pred.direct=false}
	else
	  if store#owl_TransitiveProperty#relation#has_instance ~obs:Tarpit.blind_observer (Rdf.URI p)
	  then {modifs with Pred.direct=true}
	  else {modifs with Pred.transitive=true} in
      Some (AtP1 (Arg (Pred.Role (p,modifs'),i,args), k))
  | _ -> None

let rec focus_toggle_sym store : transf = function
  | AtP1 (Arg (Pred.Role (p,modifs),i,args), k) ->
      if store#owl_SymmetricProperty#relation#has_instance ~obs:Tarpit.blind_observer (Rdf.URI p)
      then None
      else Some (AtP1 (Arg (Pred.Role (p, {modifs with Pred.symmetric = not modifs.Pred.symmetric}),i,args), k))
  | _ -> None


let focus_define_struct define_functor ~(callback: focus option -> unit) : focus -> unit =
  let split_and l =
    let lt, l' = List.partition (function Arg (Pred.Type _, 1, _) -> true | _ -> false) l in
    let lr, l'' = List.partition (function Arg (Pred.Role _, 1, _) -> true | _ -> false) l' in
    let types = List.map (function Arg (Pred.Type c, 1, _) -> c | _ -> assert false) lt in
    let l_args = List.map (function Arg (Pred.Role (p,_), 1, args) -> (p, args.(2)) | _ -> assert false) lr in
    let f_rest = match l'' with [] -> Thing | [x] -> x | _ -> And l'' in
    types, l_args, f_rest
  in
  function
    | AtS1 (Det (det,f),k) ->
	let is_arg, (types, l_args, f_rest) =
	  match f with
	  | Arg (Pred.Role (p,_), 2, [|_; Det (Qu (An,None), And l); _|]) ->
	      let types, l_args, f_rest = split_and l in
	      true, (types, (p, top_s1)::l_args, f_rest)
	  | And l -> false, split_and l
	  | _ -> false, ([], [], f) in
	if l_args = []
	then failwith "No structure can be defined from that query"
	else
	  let arg_props = Array.of_list (List.map fst l_args) in
	  let arity = Array.length arg_props in
	  define_functor ~arity ~implicit:(Some (types,arg_props))
	    (fun funct ->
	      let f' =
		if is_arg
		then
		  let args = Array.of_list (an f_rest :: List.map snd l_args) in
		  Arg (Pred.Funct funct, 1, args)
		else
		  let args = Array.of_list (top_s1 :: List.map snd l_args) in
		  simpl_and (Arg (Pred.Funct funct, 0, args)) f_rest in
	      callback (Some (AtS1 (Det (det,f'),k))))
    | _ -> ()


let rec command_after_run : c -> c = function
  | Nope -> Nope
  | Assert s ->
    ( match s_after_run s with
    | True -> Nope
    | s' -> Assert s' )
  | Get np -> Get (s1_after_run np)
  | Call (proc,args) -> Nope
  | Seq lc ->
    ( match List.filter ((<>) Nope) (List.map command_after_run lc) with
    | [] -> Nope
    | [c'] -> c'
    | lc' -> Seq lc' )
  | Cond (s,c1,c2) ->
    ( match command_after_run c1, command_after_run c2 with
    | Nope, Nope -> Nope
    | c1', c2' -> Cond (s,c1',c2') )
  | For (np,c) ->
    ( match s1_after_run np, command_after_run c with
    | np', _ when np' = top_s1 -> Nope
    | _, Nope -> Nope
    | np', c' -> For (np',c') )
and s_after_run : s -> s = function
  | Is (np,f) ->
    ( match s1_after_run np, p1_after_run f with
    | np', _ when np' = top_s1 -> True
    | _, Thing -> True
    | np', f' -> Is (np',f') )
  | True -> True
  | SAnd l ->
    ( match List.filter ((<>) True) (List.map s_after_run l) with
    | [] -> True
    | [s'] -> s'
    | l' -> SAnd l' )
  | SOr l -> True
  | SNot s -> True
  | SMaybe s -> True
and s1_after_run : s1 -> s1 = function
  | (Det (Qu ((The | Every | Each), _), _) as np) -> np
  | Det (Qu ((No | Only), _), _) -> top_s1
  | Det (det,f) -> Det (det, p1_after_run f)
  | NAnd l ->
    ( match List.filter ((<>) top_s1) (List.map s1_after_run l) with
    | [] -> top_s1
    | [np'] -> np'
    | l' -> NAnd l' )
  | NOr l -> top_s1
  | NNot np -> top_s1
  | NMaybe np -> top_s1
and p1_after_run : p1 -> p1 = function
  | Arg (pred,pos,args) -> Arg (pred, pos, Array.map s1_after_run args)
  | SuchThat (x,s) ->
    ( match s_after_run s with
    | True -> Thing
    | s' -> SuchThat (x,s') )
  | Thing -> Thing
  | And l ->
    ( match List.filter ((<>) Thing) (List.map p1_after_run l) with
    | [] -> Thing
    | [f'] -> f'
    | l' -> And l' )
  | Or l -> Thing
  | Not f -> Thing
  | Maybe f -> Thing

let focus_after_run (focus : focus) : focus option =
  let c = command_of_focus focus in
  let c' = command_after_run c in
  if c' = Nope
  then None
  else Some (focus_of_command c')
     
(* transformation kinds and their application *)

type kind =
  | FocusUp
  | FocusDown
  | FocusLeft
  | FocusRight
  | FocusTab
  | InsertForEach
  | InsertCond
  | InsertSeq
  | InsertAnd
  | InsertOr
  | InsertAndNot
  | InsertAndMaybe
  | InsertIsThere
  | ToggleQu of qu
  | ToggleNot
  | ToggleMaybe
  | ToggleOpt
  | ToggleTrans
  | ToggleSym
  | Describe
  | Select
  | Delete

let focus_tab_after_insert : transf =
  fun foc' ->
    let foc'' = focus_next_postfix_down foc' in
    match focus_next_postfix_aux ~filter:focus_default_filter foc'' with
      | Some foc''' -> Some foc'''
      | None -> Some foc'

let focus_insert_tab (tr : transf) : transf =
  fun foc -> Option.bind (tr foc) focus_tab_after_insert

let focus_apply store (transf : kind) : transf =
  match transf with
  | FocusUp -> no_fail_transf focus_up
  | FocusDown -> no_fail_transf focus_down
  | FocusLeft -> no_fail_transf focus_left
  | FocusRight -> no_fail_transf focus_right
  | FocusTab -> focus_next_postfix ~filter:focus_default_filter
  | InsertForEach -> focus_insert_tab focus_for_each
  | InsertCond -> focus_insert_tab focus_cond
  | InsertSeq -> focus_insert_tab focus_and
  | InsertAnd -> focus_insert_tab focus_and
  | InsertOr -> focus_insert_tab focus_or
  | InsertAndNot -> focus_insert_tab focus_and_not
  | InsertAndMaybe -> focus_insert_tab focus_and_maybe
  | InsertIsThere -> focus_insert_tab focus_is_there
  | ToggleQu qu -> focus_quantifier qu
  | ToggleNot -> focus_not
  | ToggleMaybe -> focus_maybe
  | ToggleOpt -> focus_toggle_opt store
  | ToggleTrans -> focus_toggle_trans store
  | ToggleSym -> focus_toggle_sym store
  | Describe -> focus_describe store
  | Select -> focus_select
  | Delete -> focus_delete
