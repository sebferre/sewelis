(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

class log (base : string) =
  let filename = base ^ ".log" in
  let dirname = base ^ "-files" in
  object (self)
(*    val ch_log = open_out_gen [Open_creat; Open_wronly; Open_append; Open_text] 0o660 filename *) (* cannot be marshaled *)
    val mutable silent = false

    method filename = filename
    method dirname = dirname

    method append (line : string) =
      if not silent then begin
	let ch_log = open_out_gen [Open_creat; Open_wronly; Open_append; Open_text] 0o660 filename in
	output_string ch_log line;
	output_string ch_log "\n";
	flush ch_log;
	close_out ch_log
      end

    method fold : 'a. ('a -> string -> 'a) -> 'a -> 'a =
      fun f init ->
	try
	  let ch_in = open_in filename in
	  let line = ref "" in
	  let res = ref init in
	  silent <- true; (* to avoid writing the log during fold *)
	  begin try while true do
	    line := input_line ch_in;
	    res := f !res !line
	  done with
	  | End_of_file ->
	      silent <- false;
	      close_in ch_in
	  | exn ->
	      silent <- false;
	      close_in ch_in;
	      print_endline (Printexc.to_string exn);
	      print_string "in line: "; print_endline !line;
	      raise exn end;
	  !res
	with _ -> init

    method iter : (string -> unit) -> unit =
      fun f -> self#fold (fun () x -> f x) ()

    initializer
      if not (Sys.file_exists dirname) then
	Unix.mkdir dirname 0o751
(*      at_exit (fun () -> flush ch_log; close_out ch_log) *)
  end

