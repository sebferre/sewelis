(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Ext = Intset.Intmap
module Name = Name.Make
module Namespace = Lisql_namespace

(* utilities *)

let array_set_nth args i x =
  if i < 0 || i > Array.length args then failwith "Lisql.array_set_nth: invalid index";
  if args.(i) = x
  then args
  else
    let args' = Array.copy args in
    args'.(i) <- x;
    args'

let rec same_list_but n args1 args2 =
  match args1, args2 with
  | [], [] -> true
  | a1::args1', a2::args2' ->
      (n = 0 || same_list_but (n-1) args1' args2')
  | _, _ -> false

(* AST definition *)

module Proc =
  struct
    type t =
      | RemoveName
      | CopyName
      | MoveName
      | Prim of string
  end

module Pred =
  struct
    type t =
      | Equal
      | Type of Uri.t
      | Role of role
      | Funct of Uri.t
      | Prim of string
    and role = Uri.t * role_modifs
    and role_modifs = { direct : bool;
			symmetric : bool;
			reflexive : bool;
			transitive : bool }

    let default_role_modifs = { direct=false; symmetric=false; transitive=false; reflexive=false }

    let make_role ?(modifs = default_role_modifs) (p : Uri.t)  = (p,modifs)

    let reifiable = function (* whether a predicate can get focus at 0 *)
      | Equal -> false
      | Type _ -> false
      | Role _ -> false
      | Funct _ -> true
      | Prim _ -> false

    let primitive = function
      | Equal -> true
      | Prim _ -> true
      | _ -> false
  end

module Argindex =
  struct
    type t = int

    let stat = 0
    let subj = 1
    let obj = 2

    let inbounds i arity = 0 <= i && i <= arity

    let opposite = function
      | 1 -> 2 (* subj -> obj *)
      | 2 -> 1 (* obj -> subj *)
      | _ -> invalid_arg "Lisql_ast.Argindex.inverse_of: only applies to indices 1/2"

    type kind = [`Statement | `Subject | `Object | `Other of int]

    let to_kind = function
      | 0 -> `Statement
      | 1 -> `Subject
      | 2 -> `Object
      | i -> `Other i

    let to_kind_role = function
      | 1 -> `Subject
      | 2 -> `Object
      | _ -> invalid_arg "Lisql_ast.Argindex.to_kind_role: only subject/object" 

    let of_kind = function
      | `Statement -> 0
      | `Subject -> 1
      | `Object -> 2
      | `Other i -> i
  end

type command =
  | Base of Uri.t
  | Prefix of string * Uri.t
  | Import of Uri.t * string (* src, base, filename *)
  | LinkedData of Uri.t
(*
  | CopyName of Name.t * Name.t
  | MoveName of Name.t * Name.t
  | RemoveName of Name.t
  | Assert of Uri.t * s
*)
  | Comment of string
  | Command of c
and c =
  | Nope
  | Assert of s
  | Get of s1
  | Call of Proc.t * s1 array
  | Seq of c list
  | Cond of s * c * c
  | For of s1 * c
and s =
  | Is of s1 * p1
  | True
  | SAnd of s list
  | SOr of s list
  | SNot of s
  | SMaybe of s
and s1 =
  | Det of s2 * p1
  | NAnd of s1 list
  | NOr of s1 list
  | NNot of s1
  | NMaybe of s1
and s2 =
  | Name of Name.t
  | Quote of c
  | Ref of var
  | Qu of qu * var option
  | Which
and qu =
  | An
  | The
  | Every
  | Only
  | No
  | Each
(*
  | Atleast of int
*)
and p1 =
  | Arg of Pred.t * Argindex.t * s1 array (* predicate, rank [0,arity(predicate)], args *)
      (* position 0 is for reified relation or term, other positions are for arguments *)
      (* the arity of the predicate is the array size - 1 *)
  | SuchThat of var * s (* sentence abstraction *)
  | Thing
  | And of p1 list
  | Or of p1 list
  | Not of p1
  | Maybe of p1
and var = string

(* AST context definitions *)

type context_c =
  | Nil
  | SeqN of int * c list * context_c
  | Cond1 of s * c * context_c
  | Cond2 of s * c * context_c
  | For2 of s1 * context_c
  | Quote0 of context_s2
and context_s =
  | Assert0 of context_c
  | Cond0 of c * c * context_c
  | SuchThat0 of var * s * context_p1
  | SAndN of int * s list * context_s
  | SOrN of int * s list * context_s
  | SNot0 of s * context_s
  | SMaybe0 of s * context_s
and context_s1 =
  | Get0 of context_c
  | CallN of Argindex.t * Proc.t * s1 array * context_c
  | For1 of c * context_c
  | Is0 of s1 * p1 * context_s
  | ArgN of Argindex.t * Pred.t * Argindex.t * s1 array * context_p1
  | NAndN of int * s1 list * context_s1
  | NOrN of int * s1 list * context_s1
  | NNot0 of s1 * context_s1
  | NMaybe0 of s1 * context_s1
and context_s2 =
  | Det0 of s2 * p1 * context_s1
and context_p1 =
  | Is1 of s1 * p1 * context_s
  | Det1 of s2 * p1 * context_s1
  | AndN of int * p1 list * context_p1
  | OrN of int * p1 list * context_p1
  | Not0 of p1 * context_p1
  | Maybe0 of p1 * context_p1

(* focus and transformation definition *)

type focus =
  | AtC of c * context_c
  | AtS of s * context_s
  | AtS1 of s1 * context_s1
  | AtS2 of s2 * context_s2
  | AtP1 of p1 * context_p1

type transf = focus -> focus option

let no_fail_transf t = (fun foc -> match t foc with Some foc' -> Some foc' | None -> Some foc)

let (>>) t1 t2 = fun foc -> Option.bind (t1 foc) t2

(* constructor and test functions *)

let rec top_c = Get top_s1
and top_s = Is (top_s1, top_p1)
and top_s1 = Det (top_s2, top_p1)
and top_s2 = Qu (An, None)
and top_p1 = Thing

let nil = Arg (Pred.Funct Term.uri_nil, Argindex.stat, [| top_s1 |])
let cons x l = Arg (Pred.Funct Term.uri_cons, Argindex.stat, [| top_s1; x ; l |])
let first l st = Arg (Pred.Funct Term.uri_cons, Argindex.subj, [| st; top_s1 ; l |])
let rest x st = Arg (Pred.Funct Term.uri_cons, Argindex.obj, [| st; x ; top_s1 |])
let is_nil pred i args = pred = Pred.Funct Term.uri_nil && i = Argindex.stat && Array.length args = 1
let is_cons pred i args = pred = Pred.Funct Term.uri_cons && i = Argindex.stat && Array.length args = 3

let var_s2 v = Qu (An, Some v)
let var_s1 v = Det (var_s2 v, Thing)

let name_s2 n = Name n
let name_s1 n = Det (name_s2 n, Thing)

let uri_s2 u = Name (Rdf.URI u)
let uri_s1 u = Det (uri_s2 u, Thing)
let uri_s u = Is (uri_s1 u, top_p1)
let uri_c u = Get (uri_s1 u)

let ref_s2 v = Ref v
let ref_s1 v = Det (ref_s2 v, Thing)

let which_s2 = Which
let which_s1 = Det (which_s2, Thing)

let has_equal np =
  Arg (Pred.Equal, 1, [|top_s1;top_s1;np|])
let has_type (c : Uri.t) =
  Arg (Pred.Type c, Argindex.subj, [|top_s1; top_s1|])
let has_role (r : Pred.role) i np =
  Arg (Pred.Role r, i, if i=Argindex.subj then [|top_s1; top_s1; np|] else [|top_s1; np; top_s1|])
let has_prim2 (op : string) i np =
  Arg (Pred.Prim op, i, if i=Argindex.subj then [|top_s1; top_s1; np|] else [|top_s1; np; top_s1|])

let prop_name (p : Uri.t) (n : Rdf.thing) =
  Arg (Pred.Role (Pred.make_role p), Argindex.subj, [|top_s1; top_s1; name_s1 n|])
let inv_prop_name (p : Uri.t) (n : Rdf.thing) =
  Arg (Pred.Role (Pred.make_role p), Argindex.obj, [|top_s1; name_s1 n; top_s1|])

let make_pred (pred : Pred.t) (arity : int) (i : int) =
  assert (Argindex.inbounds i arity);
  Arg (pred,i,Array.make (1+arity) top_s1)

let make_role p ikind =
  make_pred (Pred.Role (Pred.make_role p)) 2 (Argindex.of_kind ikind)

let make_role1 p ikind k =
  let i = Argindex.of_kind ikind in
  ArgN (Argindex.opposite i, Pred.Role (Pred.make_role p), i, [|top_s1; top_s1; top_s1|], k)

let make_funct funct arity i = make_pred (Pred.Funct funct) arity i

let make_prim op arity i = make_pred (Pred.Prim op) arity i

let make_call_of_np proc arity i npi =
  let args = Array.make arity top_s1 in
  args.(i) <- npi;
  Call (proc, args)

let quantif qu f = Det (Qu (qu, None), f)

let an (f : p1) = quantif An f
let the (f : p1) = quantif The f
let each (f : p1) = quantif Each f
let every (f : p1) = quantif Every f
let only (f : p1) = quantif Only f
let no (f : p1) = quantif No f

let and_p1 : p1 list -> p1 = function
  | [] -> Thing
  | [f] -> f
  | l -> And l

let name_is (n : Name.t) (f : p1) = Is (name_s1 n, f)

let eq_which : p1 = has_equal which_s1

let is_role = function
  | Arg (Pred.Role r, i, args) -> Some (r,i,args.(Argindex.opposite i))
  | _ -> None

let is_subterm_role f =
  match is_role f with
    | Some ((a,modifs),i,np) when a = Term.uri_subterm -> Some (i, modifs.Pred.reflexive)
    | _ -> None

let is_sublist_role f =
  match is_role f with
    | Some ((a,modifs),i, Det (Qu (An, _), f)) when a = Term.uri_sublist -> Some (i, modifs.Pred.reflexive, f)
    | _ -> None

let is_sublist_np = function
  | Det (Qu (An, _), f) ->
    ( match is_role f with
      | Some ((a,modifs),i,np) when a = Term.uri_sublist -> Some (i, modifs.Pred.reflexive, np)
      | _ -> None )
  | _ -> None

(* variables utilities *)

let var_root = "root"
let var_this = "this"
let var_What = "What"
let var_focus = "_focus"
let var_name = "_name"
let var_quote = "_quote"

let var_has_prefix prefix v =
  Str.string_match (Str.regexp_string prefix) v 0

class gen_var =
  object (self)
    val h : (string, int ref) Hashtbl.t = Hashtbl.create 11
    val mutable generated : var list = []

    method get_prefix prefix =
      let cpt =
	try Hashtbl.find h prefix
	with _ ->
	  let cpt = ref 0 in
	  Hashtbl.add h prefix cpt;
	  cpt in
      incr cpt;
      let v =
	let name = if prefix = "" then "_" else prefix in
	name ^ String.make (!cpt-1) '\'' in
(*
	if prefix <> "" && !cpt = 1
	then prefix
	else prefix ^ String.make (!cpt-1) '\'' (*string_of_int !cpt*) in
*)
      generated <- v::generated;
      v

    method get = self#get_prefix ""

    method generated v = List.mem v generated
  end

let var_list = 
  [ "X"; "Y"; "Z"; "A"; "B"; "C"; "D"; "E"; "F"; "G"; "H"; "I"; "J";
    "K"; "L"; "M"; "N"; "O"; "P"; "Q"; "R"; "S"; "T"; "U"; "V"; "W"]
    
let get_new_var vs = List.find (fun v -> not (List.mem v vs)) var_list

let rec get_list_new_vars vs = function
  | 0 -> []
  | n ->
      let v = get_new_var vs in
      v :: get_list_new_vars (v::vs) (n-1)

let rec vars_c = function
  | Nope -> LSet.empty ()
  | Assert s -> vars_s s
  | Get np -> vars_s1 np
  | Call (proc, args) -> Array.fold_left (fun res np -> LSet.union res (vars_s1 np)) (LSet.empty ()) args
  | Seq lc -> List.fold_left (fun res c -> LSet.union res (vars_c c)) (LSet.empty ()) lc
  | Cond (s,c1,c2) -> LSet.union (vars_s s) (LSet.union (vars_c c1) (vars_c c2))
  | For (np,c) -> LSet.union (vars_s1 np) (vars_c c)
and vars_s = function
  | Is (np,c) -> LSet.union (vars_s1 np) (vars_p1 c)
  | True -> LSet.empty ()
  | SAnd l -> List.fold_left (fun res s -> LSet.union res (vars_s s)) (LSet.empty ()) l
  | SOr l -> List.fold_left (fun res s -> LSet.union res (vars_s s)) (LSet.empty ()) l
  | SNot s -> vars_s s
  | SMaybe s -> vars_s s
and vars_s1 = function
  | Det (det, c) -> LSet.union (vars_s2 det) (vars_p1 c)
  | NAnd l -> List.fold_left (fun res np -> LSet.union res (vars_s1 np)) (LSet.empty ()) l
  | NOr l -> List.fold_left (fun res np -> LSet.union res (vars_s1 np)) (LSet.empty ()) l
  | NNot np -> vars_s1 np
  | NMaybe np -> vars_s1 np
and vars_s2 = function
  | Name _ -> LSet.empty ()
  | Quote _ -> LSet.empty ()
  | Ref v -> LSet.empty ()
  | Qu (_, v_opt) -> vars_var_opt v_opt
  | Which -> LSet.empty ()
and vars_p1 q =
  match q with
  | Arg (_,_,args) -> Array.fold_left (fun res np -> LSet.union res (vars_s1 np)) (LSet.empty ()) args
  | SuchThat (x,s) -> vars_s s
  | Thing -> LSet.empty ()
  | And l -> List.fold_left (fun res f -> LSet.union res (vars_p1 f)) (LSet.empty ()) l
  | Or l -> List.fold_left (fun res f -> LSet.union res (vars_p1 f)) (LSet.empty ()) l
  | Not f -> vars_p1 f
  | Maybe f -> vars_p1 f
and vars_var_opt = function
  | None -> LSet.empty ()
  | Some v -> LSet.singleton v

let new_var_c c = get_new_var (vars_c c)
let new_var_s a = get_new_var (vars_s a)
let new_var_s1 np = get_new_var (vars_s1 np)
let new_var_p1 q = get_new_var (vars_p1 q)


(* TODO: should be derived from FOL *)
let rec accessible_vars_s ?(lv = []) = function
  | Is (np,f) ->
      let acc, lv = accessible_vars_s1 ~lv np in
      if acc
      then accessible_vars_p1 ~lv f
      else lv
  | True -> lv
  | SAnd ls -> List.fold_left (fun lv s -> accessible_vars_s ~lv s) lv ls
  | SOr ls -> List.fold_left (fun lv s -> accessible_vars_s ~lv s) lv ls
  | SNot s -> lv
  | SMaybe s -> accessible_vars_s ~lv s
and accessible_vars_s1 ?(lv = []) = function
  | Det (det, c) ->
      let acc, lv = accessible_vars_s2 ~lv det in
      if acc
      then true, accessible_vars_p1 ~lv c
      else false, lv
  | NAnd l ->
      List.fold_left
	(fun (acc,lv) np ->
	  let acc1, lv = accessible_vars_s1 ~lv np in
	  acc || acc1, lv)
	(false, lv) l
  | NOr l ->
      List.fold_left
	(fun (acc,lv) np ->
	  let acc1, lv1 = accessible_vars_s1 ~lv np in
	  acc || acc1, lv)
	(false, lv) l
  | NNot np -> false, lv
  | NMaybe np -> accessible_vars_s1 ~lv np
and accessible_vars_s2 ?(lv = []) = function
  | Name _ -> true, lv
  | Quote _ -> true, lv
  | Ref v -> true, lv
  | Qu (An, v_opt) -> true, Option.fold (fun v -> v::lv) lv v_opt
  | Qu ((The | Every | Only | No | Each), _) -> false, lv
  | Which -> true, lv
and accessible_vars_p1 ?(lv = []) = function
  | Arg (_,_,args) ->
      Array.fold_right
	(fun np lv' ->
	  let acc, lv' = accessible_vars_s1 ~lv:lv' np in
	  if acc then lv' else lv)
	args lv
  | SuchThat (x,s) -> accessible_vars_s ~lv s
  | Thing -> lv
  | And l -> List.fold_left (fun lv f -> accessible_vars_p1 ~lv f) lv l
  | Or l -> List.fold_left (fun lv f -> accessible_vars_p1 ~lv f) lv l
  | Not f -> lv
  | Maybe f -> accessible_vars_p1 ~lv f


let rec subst_c env = function
  | Nope -> Nope
  | Assert s -> Assert (subst_s env s)
  | Get np -> Get (subst_s1 env np)
  | Call (proc,args) -> Call (proc, Array.map (subst_s1 env) args)
  | Seq lc -> Seq (List.map (subst_c env) lc)
  | Cond (s,c1,c2) -> Cond (subst_s env s, subst_c env c1, subst_c env c2)
  | For (np,c) -> For (subst_s1 env np, subst_c env c)
and subst_s env = function
  | Is (np,c) -> Is (subst_s1 env np, subst_p1 env c)
  | True -> True
  | SAnd l -> SAnd (List.map (subst_s env) l)
  | SOr l -> SOr (List.map (subst_s env) l)
  | SNot s -> SNot (subst_s env s)
  | SMaybe s -> SMaybe (subst_s env s)
and subst_s1 env = function
  | Det (det,c) -> Det (subst_s2 env det, subst_p1 env c)
  | NAnd l -> NAnd (List.map (subst_s1 env) l)
  | NOr l -> NOr (List.map (subst_s1 env) l)
  | NNot np -> NNot (subst_s1 env np)
  | NMaybe np -> NMaybe (subst_s1 env np)
and subst_s2 env det =
  match det with
  | Name _
  | Quote _ -> det
  | Ref v -> (try List.assoc v env with Not_found -> det)
  | Qu (_, None) -> det
  | Qu (qu, Some v) -> (try List.assoc v env with Not_found -> det)
  | Which -> det
and subst_p1 env = function
  | Arg (pred,i,args) -> Arg (pred, i, subst_args env args)
  | SuchThat (x,s) -> SuchThat (x, subst_s (List.remove_assoc x env) s)
  | Thing -> Thing
  | And l -> And (List.map (subst_p1 env) l)
  | Or l -> Or (List.map (subst_p1 env) l)
  | Not c' -> Not (subst_p1 env c')
  | Maybe c' -> Maybe (subst_p1 env c')
and subst_args env args =
  Array.map (subst_s1 env) args


(* handling of contexts and foci *)

let default_context_of_c c = Nil
let default_context_of_s s = Assert0 Nil
let default_context_of_s1 np = Get0 Nil
let default_context_of_s2 det = Det0 (det, top_p1, default_context_of_s1 (Det (det, top_p1)))
let default_context_of_p1 f = Is1 (top_s1, f, default_context_of_s (Is (top_s1, f)))

let rec same_context_c k1 k2 =
  match k1, k2 with
    | Nil, Nil -> true
    | SeqN (n1,l1,k1'), SeqN (n2,l2,k2') -> n1 = n2 && same_list_but n1 l1 l2 && same_context_c k1' k2'
    | Cond1 (s1,c1,k1'), Cond1 (s2,c2,k2') -> s1 = s2 && c1 = c2 && same_context_c k1' k2'
    | Cond2 (s1,c1,k1'), Cond2 (s2,c2,k2') -> s1 = s2 && c1 = c2 && same_context_c k1' k2'
    | For2 (np1,k1'), For2 (np2,k2') -> np1 = np2 && same_context_c k1' k2'
    | Quote0 k1', Quote0 k2' -> same_context_s2 k1' k2'
    | _, _ -> false
and same_context_s k1 k2 =
  match k1, k2 with
  | Assert0 k1', Assert0 k2' -> same_context_c k1' k2'
  | Cond0 (c11,c12,k1'), Cond0 (c21,c22,k2') -> c11 = c21 && c12 = c22 && same_context_c k1' k2'
  | SuchThat0 (x1,_,k1'), SuchThat0 (x2,_,k2') ->
      x1 = x2 && same_context_p1 k1' k2'
  | SAndN (n1,l1,k1'), SAndN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_s k1' k2'
  | SOrN (n1,l1,k1'), SOrN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_s k1' k2'
  | SNot0 (_,k1'), SNot0 (_,k2') ->
      same_context_s k1' k2'
  | SMaybe0 (_,k1'), SMaybe0 (_,k2') ->
      same_context_s k1' k2'
  | _, _ -> false
and same_context_s1 k1 k2 =
  match k1, k2 with
  | For1 (c1,k1'), For1 (c2,k2') -> c1 = c2 && same_context_c k1' k2'
  | Get0 k1', Get0 k2' -> same_context_c k1' k2'
  | CallN (n1,proc1,args1,k1'), CallN (n2,proc2,args2,k2') -> n1 = n2 && proc1 = proc2 && same_args_but [n1] args1 args2 && same_context_c k1' k2'
  | Is0 (_,c1,k1'), Is0 (_,c2,k2') ->
      c1 = c2 && same_context_s k1' k2'
  | ArgN (j1,pred1,i1,args1,k1'), ArgN (j2,pred2,i2,args2,k2') ->
      j1 = j2 && i1 = i2 && pred1 = pred2 && same_args_but [j1;i1] args1 args2 && same_context_p1 k1' k2'
  | NAndN (n1,l1,k1'), NAndN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_s1 k1' k2'
  | NOrN (n1,l1,k1'), NOrN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_s1 k1' k2'
  | NNot0 (_,k1'), NNot0 (_,k2') ->
      same_context_s1 k1' k2'
  | NMaybe0 (_,k1'), NMaybe0 (_,k2') ->
      same_context_s1 k1' k2'
  | _, _ -> false
and same_context_s2 k1 k2 =
  match k1, k2 with
  | Det0 (_,f1,k1'), Det0 (_,f2,k2') ->
      f1 = f2 && same_context_s1 k1' k2'
  | _ -> false
and same_context_p1 k1 k2 =
  match k1, k2 with
  | Is1 (np1,_,k1'), Is1 (np2,_,k2') ->
      np1 = np2 && same_context_s k1' k2'
  | Det1 (d1,_,k1'), Det1 (d2,_,k2') ->
      d1 = d2 && same_context_s1 k1' k2'
  | AndN (n1,l1,k1'), AndN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_p1 k1' k2'
  | OrN (n1,l1,k1'), OrN (n2,l2,k2') ->
      n1 = n2 && same_list_but n1 l1 l2 && same_context_p1 k1' k2'
  | Not0 (_,k1'), Not0 (_,k2') ->
      same_context_p1 k1' k2'
  | Maybe0 (_,k1'), Maybe0 (_,k2') ->
      same_context_p1 k1' k2'
  | _, _ -> false
and same_args_but is args1 args2 =
  let n1 = Array.length args1 - 1 in
  let n2 = Array.length args2 - 1 in
  n1 = n2 && Common.fold_for (fun j res -> res && (List.mem j is || args1.(j) = args2.(j))) 0 n1 true

let same_focus foc1 foc2 =
  match foc1, foc2 with
  | AtC (c1,k1), AtC (c2,k2) ->
      c1 = c2 && same_context_c k1 k2
  | AtS (s1,k1), AtS (s2,k2) ->
      s1 = s2 && same_context_s k1 k2
  | AtS1 (np1,k1), AtS1 (np2,k2) ->
      np1 = np2 && same_context_s1 k1 k2
  | AtS2 (det1,k1), AtS2 (det2,k2) ->
      det1 = det2 && same_context_s2 k1 k2
  | AtP1 (f1,k1), AtP1 (f2,k2) ->
      f1 = f2 && same_context_p1 k1 k2
  | _, _ -> false


let rec command_of_c_gen ~command_of_s2 (c, k : c * context_c) =
  match k with
    | Nil -> c
    | SeqN (n,l,k') ->
      let l' = Common.list_set_nth l n c in
      command_of_c_gen ~command_of_s2 (Seq l', k')
    | Cond1 (s,c2,k') -> command_of_c_gen ~command_of_s2 (Cond (s,c,c2), k')
    | Cond2 (s,c1,k') -> command_of_c_gen  ~command_of_s2 (Cond (s,c1,c), k')
    | For2 (np,k') -> command_of_c_gen ~command_of_s2 (For (np,c), k')
    | Quote0 k' -> command_of_s2 (Quote c, k')

let rec command_of_s_gen ~command_of_c ~command_of_p1 (s, k : s * context_s) =
  match k with
  | Assert0 k' -> command_of_c (Assert s, k')
  | Cond0 (c1,c2,k') -> command_of_c (Cond (s,c1,c2), k')
  | SuchThat0 (x,_,k') -> command_of_p1 (SuchThat (x,s), k')
  | SAndN (n,l,k') ->
      let l' = Common.list_set_nth l n s in
      command_of_s_gen ~command_of_c ~command_of_p1 (SAnd l', k')
  | SOrN (n,l,k') ->
      let l' = Common.list_set_nth l n s in
      command_of_s_gen ~command_of_c ~command_of_p1 (SOr l', k')
  | SNot0 (_,k') -> command_of_s_gen ~command_of_c ~command_of_p1 (SNot s, k')
  | SMaybe0 (_,k') -> command_of_s_gen ~command_of_c ~command_of_p1 (SMaybe s, k')

let rec command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 (np, k : s1 * context_s1) =
  match k with
  | Get0 k' -> command_of_c (Get np, k')
  | CallN (n,proc,args,k') ->
    let args' = array_set_nth args n np in
    command_of_c (Call (proc, args'), k')
  | For1 (c,k') -> command_of_c (For (np,c), k')
  | Is0 (_,c,k') -> command_of_s (Is (np,c), k')
  | ArgN (j,pred,i,args,k') ->
      let args' = array_set_nth args j np in
      command_of_p1 (Arg (pred,i,args'), k')
  | NAndN (n,l,k') ->
      let l' = Common.list_set_nth l n np in
      command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 (NAnd l', k')
  | NOrN (n,l,k') ->
      let l' = Common.list_set_nth l n np in
      command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 (NOr l', k')
  | NNot0 (_,k') -> command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 (NNot np, k')
  | NMaybe0 (_,k') -> command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 (NMaybe np, k')

let rec command_of_s2_gen ~command_of_s1 (det, k : s2 * context_s2) =
  match k with
  | Det0 (_,c,k') -> command_of_s1 (Det (det,c), k')

let rec command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 (f, k : p1 * context_p1) =
  match k with
  | Is1 (np,_,k') -> command_of_s (Is (np,f), k')
  | Det1 (q,_,k') -> command_of_s1 (Det (q,f), k')
  | AndN (n,l,k') ->
      let l' = Common.list_set_nth l n f in
      command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 (And l', k')
  | OrN (n,l,k') ->
      let l' = Common.list_set_nth l n f in
      command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 (Or l', k')
  | Not0 (_,k') -> command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 (Not f, k')
  | Maybe0 (_,k') -> command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 (Maybe f, k')


let rec command_of_c c_k = command_of_c_gen ~command_of_s2 c_k
and command_of_s s_k = command_of_s_gen ~command_of_c ~command_of_p1 s_k
and command_of_s1 np_k = command_of_s1_gen ~command_of_c ~command_of_s ~command_of_p1 np_k
and command_of_s2 det_k = command_of_s2_gen ~command_of_s1 det_k
and command_of_p1 c_k = command_of_p1_gen ~command_of_c ~command_of_s ~command_of_s1 c_k

let command_of_focus : focus -> c = function
  | AtC (c,k) -> command_of_c (c,k)
  | AtS (s,k) -> command_of_s (s,k)
  | AtS1 (np,k) -> command_of_s1 (np,k)
  | AtS2 (det,k) -> command_of_s2 (det,k)
  | AtP1 (f,k) -> command_of_p1 (f,k)

let focus_of_command : c -> focus = function
  | Get np -> AtS1 (np, Get0 Nil)
  | c -> AtC (c, Nil)

(* deprecated *)
(*
let rec abstract_s c0 = function
  | NilS -> Some (c0, [])
  | SuchThat0 (v,_,k') ->
      match abstract_p1 (fun c -> c) k' with
      | Some (c,l) -> Some (c0, (v,c)::l)
      | None -> None
and abstract_s1 f = function
  | Is0 (_,c,k') -> abstract_s (f c) k'
  | Role1 (r,_,Is1 (np,_,k')) -> abstract_s (f (Role (reverse_p2 r, np))) k'
  | Role1 (r,_,k') -> abstract_p1 (fun c -> f (Role (reverse_p2 r, Qu (Exists, c)))) k'
  | _ -> None
and abstract_p1 f = function
  | Is1 (Qu (Exists,c),_,k') -> abstract_s (f c) k'
  | Qu1 (Exists,_,k') -> abstract_s1 f k'
  | StructN (n,funct,args,k') ->
      abstract_p1 (fun c -> f (Arg (funct, n, args, c))) k'
  | ArgN (j,funct,i,args,st,k') ->
      abstract_p1
	(fun c ->
	  let args' = array_set_nth args (i-1) c in
	  f (Arg (funct, j, args', st)))
	k'
  | ArgT (funct,i,args,_,k') ->
      abstract_p1
	(fun c ->
	  let args' = array_set_nth args (i-1) c in
	  f (Struct (funct, args')))
	k'
  | PredN (n,r,pred,args,k') ->
      abstract_p1
	(fun c ->
	  let rn, _ = List.nth args n in
	  let args' = (r, c)::Common.list_remove_nth args n in
	  f (Pred (rn, pred, args')))
	k'
  | AndN (n,l,k') ->
      abstract_p1 (fun c -> f (And (Common.list_set_nth l n c))) k'
  | _ -> None

let abstract_focus_s1 np k =
  match abstract_s1 (fun c -> c) k with
  | Some (c0,l) ->
      let c = List.fold_left (fun res (vi,ci) -> subst_p1 [(vi,ci)] res) c0 l in
      let np' = List.fold_left (fun res (vi,ci) -> subst_s1 [(vi,ci)] res) np l in
      Some (np',c)
  | None -> None
*)

(* ----- *)

(* TODO *)
(*
let rec abstract_s1 f = function
  | Is0 (_,c,k') -> Some (f c, k')
  | Role1 (r,_,Is1 (np,_,k')) -> Some (f (Role (reverse_p2 r, np)), k')
  | Role1 (r,_,k') -> abstract_p1 (fun c -> f (Role (reverse_p2 r, Qu (Exists, c)))) k'
  | _ -> None
and abstract_p1 f = function
  | Is1 (Qu (Exists,c),_,k') -> Some (f c, k')
  | Qu1 (Exists,_,k') -> abstract_s1 f k'
  | StructN (n,funct,args,k') ->
      abstract_p1 (fun c -> f (Arg (funct, n, args, c))) k'
  | ArgN (j,funct,i,args,st,k') ->
      abstract_p1
	(fun c ->
	  let args' = array_set_nth args (i-1) c in
	  f (Arg (funct, j, args', st)))
	k'
  | ArgT (funct,i,args,_,k') ->
      abstract_p1
	(fun c ->
	  let args' = array_set_nth args (i-1) c in
	  f (Struct (funct, args')))
	k'
  | PredN (n,r,pred,args,k') ->
      abstract_p1
	(fun c ->
	  let rn, _ = List.nth args n in
	  let args' = (r, c)::Common.list_remove_nth args n in
	  f (Pred (rn, pred, args')))
	k'
  | AndN (n,l,k') ->
      abstract_p1 (fun c -> f (And (Common.list_set_nth l n c))) k'
  | _ -> None

let abstract_focus_s1 np v k =
  match k with
  | Is0 (_,c,NilS) -> None
  | Is0 (_,c,SuchThat0 (x,_,k')) ->
      ( match abstract_p1 (fun c -> c) k' with
      | Some (c1,k1) -> Some (subst_s1 [(x,c1)] np, subst_p1 [(x,c1)] c, k1)
      | None -> None)
  | Role1 (r,_,k') ->
      let c = Role (reverse_p2 r, Qu (Exists, Var v)) in
      Some (np, c, SuchThat0 (v, Is (np, c), k'))
  | _ ->
      ( match abstract_s1 (fun c -> c) k with
      | Some (c1,k1) -> Some (np,c1,k1)
      | None -> None)
*)

let rec simpl_context_assoc_p1 = function
  | AndN (n1,l1, AndN (n2,l2, k')) ->
      simpl_context_assoc_p1 (AndN (n2+n1, Common.list_insert_nth l2 n2 l1, k'))
  | OrN (n1,l1, OrN (n2,l2, k')) ->
      simpl_context_assoc_p1 (OrN (n2+n1, Common.list_insert_nth l2 n2 l1, k'))
  | k -> k

(* TODO: complete for s, s2, and p2 *)
(*
let rec simpl_context_thing_s1 (k : context_s1) : s1 * context_s1 =
  match k with
  | NAndN (n,l,k') ->
      ( match Common.list_remove_nth l n with
      | [] -> simpl_context_thing_s1 k'
      | [np] -> np, k'
      | l' -> NAnd l', k')
  | NOrN (n,l,k') ->
      ( match Common.list_remove_nth l n with
      | [] -> simpl_context_thing_s1 k'
      | [np] -> np, k'
      | l' -> NOr l', k')
  | NNot0 (_,k') ->
      simpl_context_thing_s1 k'
  | NMaybe0 (_,k') ->
      simpl_context_thing_s1 k'
  | _ -> top_s1, k
and simpl_context_thing_p1 (k : context_p1) : p1 * context_p1 =
  match k with
  | AndN (n,l,k') ->
      ( match Common.list_remove_nth l n with
      | [] -> simpl_context_thing_p1 k'
      | [f] -> f, k'
      | l' -> And l', k')
  | OrN (n,l,k') ->
      ( match Common.list_remove_nth l n with
      | [] -> simpl_context_thing_p1 k'
      | [f] -> f, k'
      | l' -> Or l', k')
  | Not0 (_,k') ->
      simpl_context_thing_p1 k'
  | Maybe0 (_,k') ->
      simpl_context_thing_p1 k'
  | _ -> top_p1, k
*)

let focus_first : transf =
  fun foc ->
    Some (focus_of_command (command_of_focus foc))

let rec focus_down : transf = function
  | AtC (c,k) -> focus_down_c k c
  | AtS (s,k) -> focus_down_s k s
  | AtS1 (np,k) -> focus_down_s1 k np
  | AtS2 (det,k) -> focus_down_s2 k det
  | AtP1 (f,k) -> focus_down_p1 k f
and focus_down_c k = function
  | Nope -> None
  | Assert s -> Some (AtS (s, Assert0 k))
  | Get np -> Some (AtS1 (np, Get0 k))
  | Call (proc,args) ->
    if Array.length args = 0
    then None
    else Some (AtS1 (args.(0), CallN (0,proc,args,k)))
  | Seq lc ->
    ( match lc with
      | [] -> None
      | c0::_ -> Some (AtC (c0, SeqN (0,lc,k))) )
  | Cond (s,c1,c2) -> Some (AtS (s, Cond0 (c1,c2,k)))
  | For (np,c) -> Some (AtS1 (np, For1 (c,k)))
and focus_down_s k = function
  | Is (np,c) -> Some (AtS1 (np, Is0 (np,c,k)))
  | True -> None
  | SAnd l ->
      ( match l with
      | [] -> None
      | s0::_ -> Some (AtS (s0, SAndN (0,l,k))))
  | SOr l ->
      ( match l with
      | [] -> None
      | s0::_ -> Some (AtS (s0, SOrN (0,l,k))))
  | SNot s0 ->
      Some (AtS (s0, SNot0 (s0,k)))
  | SMaybe s0 ->
      Some (AtS (s0, SMaybe0 (s0,k)))
and focus_down_s1 k = function
  | Det (Quote c,f) -> Some (AtC (c, Quote0 (Det0 (Quote c,f,k))))
  | Det (det,f) when f <> Thing -> Some (AtP1 (f, Det1 (det,f,k)))
  | Det _ -> None
  | NAnd l ->
      ( match l with
      | [] -> None
      | np0::_ -> Some (AtS1 (np0, NAndN (0,l,k))))
  | NOr l ->
      ( match l with
      | [] -> None
      | np0::_ -> Some (AtS1 (np0, NOrN (0,l,k))))
  | NNot np0 ->
      Some (AtS1 (np0, NNot0 (np0, k)))
  | NMaybe np0 ->
      Some (AtS1 (np0, NMaybe0 (np0, k)))
and focus_down_s2 k = function
  | Name _ -> None
  | Quote c -> Some (AtC (c, Quote0 k))
  | Ref _ -> None
  | Qu _ -> None
  | Which -> None
and focus_down_p1 k = function
  | Arg (pred,i,args) ->
      let arity = Array.length args - 1 in
      let j = if i <> 1 then 1 else 2 in
      let j = if j > arity then 0 else j in
      if j = 0 && (i = 0 || not (Pred.reifiable pred))
      then None
      else Some (AtS1 (args.(j), ArgN (j,pred,i,args,k)))
  | SuchThat (x,s) ->
      Some (AtS (s, SuchThat0 (x,s,k)))
  | Thing -> None
  | And l ->
      ( match l with
      | [] -> None
      | f0::_ -> Some (AtP1 (f0, AndN (0,l,k))))
  | Or l ->
      ( match l with
      | [] -> None
      | f0::_ -> Some (AtP1 (f0, OrN (0,l,k))))
  | Not f0 ->
      Some (AtP1 (f0, Not0 (f0, k)))
  | Maybe f0 ->
      Some (AtP1 (f0, Maybe0 (f0, k)))

let rec focus_up : transf = function
  | AtC (c,k) -> focus_up_c c k
  | AtS (s,k) -> focus_up_s s k
  | AtS1 (np,k) -> focus_up_s1 np k
  | AtS2 (det,k) -> focus_up_s2 det k
  | AtP1 (f,k) -> focus_up_p1 f k
and focus_up_c c = function
  | Nil -> None
  | SeqN (n,l,k') ->
    let l' = Common.list_set_nth l n c in
    Some (AtC (Seq l', k'))
  | Cond1 (s,c2,k') -> Some (AtC (Cond (s,c,c2), k'))
  | Cond2 (s,c1,k') -> Some (AtC (Cond (s,c1,c), k'))
  | For2 (np,k') -> Some (AtC (For (np,c), k'))
  | Quote0 (Det0 (_,f,k')) -> Some (AtS1 (Det (Quote c, f), k'))
and focus_up_s s = function
  | Assert0 k' -> Some (AtC (Assert s, k'))
  | Cond0 (c1,c2,k') -> Some (AtC (Cond (s,c1,c2), k'))
  | SuchThat0 (x,_,k') -> Some (AtP1 (SuchThat (x,s), k'))
  | SAndN (n,l,k') ->
      let l' = Common.list_set_nth l n s in
      Some (AtS (SAnd l', k'))
  | SOrN (n,l,k') ->
      let l' = Common.list_set_nth l n s in
      Some (AtS (SOr l', k'))
  | SNot0 (_,k') ->
      Some (AtS (SNot s, k'))
  | SMaybe0 (_,k') ->
      Some (AtS (SMaybe s, k'))
and focus_up_s1 np = function
  | Get0 k' -> Some (AtC (Get np, k'))
  | CallN (j,proc,args,k') ->
    let args' = array_set_nth args j np in
    Some (AtC (Call (proc, args'), k'))
  | For1 (c,k') -> Some (AtC (For (np,c), k'))
  | Is0 (_,c,k') -> Some (AtS (Is (np,c), k'))
  | ArgN (j,pred,i,args,k') ->
      let args = array_set_nth args j np in
      Some (AtP1 (Arg (pred,i,args), k'))
  | NAndN (n,l,k') ->
      let l' = Common.list_set_nth l n np in
      Some (AtS1 (NAnd l', k'))
  | NOrN (n,l,k') ->
      let l' = Common.list_set_nth l n np in
      Some (AtS1 (NOr l', k'))
  | NNot0 (_,k') ->
      Some (AtS1 (NNot np, k'))
  | NMaybe0 (_,k') ->
      Some (AtS1 (NMaybe np, k'))
and focus_up_s2 det = function
  | Det0 (_,c,k') -> Some (AtS1 (Det (det,c), k'))
and focus_up_p1 f = function
  | Is1 (np,_,k') -> Some (AtS (Is (np,f), k'))
  | Det1 (det,_,k') -> Some (AtS1 (Det (det,f), k'))
  | AndN (n,l,k') ->
      let l' = Common.list_set_nth l n f in
      Some (AtP1 (And l', k'))
  | OrN (n,l,k') ->
      let l' = Common.list_set_nth l n f in
      Some (AtP1 (Or l', k'))
  | Not0 (_,k') ->
      Some (AtP1 (Not f, k'))
  | Maybe0 (_,k') ->
      Some (AtP1 (Maybe f, k'))

let rec focus_right : transf = function
  | AtC (c,k) -> focus_right_c c k
  | AtS (s,k) -> focus_right_s s k
  | AtS1 (np,k) -> focus_right_s1 np k
  | AtS2 (det,k) -> focus_right_s2 det k
  | AtP1 (f,k) -> focus_right_p1 f k
and focus_right_c c = function
  | Nil -> None
  | SeqN (n,l,k') ->
    if n >= List.length l - 1
    then None
    else
      (try
	 let l' = Common.list_set_nth l n c in
	 Some (AtC (List.nth l' (n+1), SeqN (n+1,l',k')))
       with _ -> assert false)
  | Cond1 (s,c2,k') -> Some (AtC (c2, Cond2 (s,c,k')))
  | Cond2 (s,c1,k') -> None
  | For2 (np,k') -> None
  | Quote0 k' -> None
and focus_right_s s = function
  | Assert0 k' -> None
  | Cond0 (c1,c2,k') -> Some (AtC (c1, Cond1 (s,c2,k')))
  | SuchThat0 _ -> None
  | SAndN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n s in
	   Some (AtS (List.nth l (n+1), SAndN (n+1,l',k')))
	 with _ -> assert false)
  | SOrN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n s in
	   Some (AtS (List.nth l (n+1), SOrN (n+1,l',k')))
	 with _ -> assert false)
  | SNot0 (_,k') -> None
  | SMaybe0 (_,k') -> None
and focus_right_s1 np = function
  | Get0 _ -> None
  | CallN (j,proc,args,k') ->
    if j >= Array.length args - 1
    then None
    else
      let args' = array_set_nth args j np in
      Some (AtS1 (args'.(j+1), CallN (j+1,proc,args',k')))
  | For1 (c,k') -> Some (AtC (c, For2 (np, k')))
  | Is0 (_,f1,k') -> Some (AtP1 (f1, Is1 (np,f1,k')))
  | ArgN (j,pred,i,args,k') ->
      if j = 0
      then None
      else
	let arity = Array.length args - 1 in
	let j' = if j+1 = i then i+1 else j+1 in
	let j' = if j' > arity then 0 else j' in
	if j' = 0 && (i = 0 || not (Pred.reifiable pred))
	then None
	else
	  let args = array_set_nth args j np in
	  Some (AtS1 (args.(j'), ArgN (j',pred,i,args,k')))
  | NAndN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n np in
	   Some (AtS1 (List.nth l (n+1), NAndN (n+1,l',k')))
	 with _ -> assert false)
  | NOrN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n np in
	   Some (AtS1 (List.nth l (n+1), NOrN (n+1,l',k')))
	 with _ -> assert false)
  | NNot0 (_,k') -> None
  | NMaybe0 (_,k') -> None
and focus_right_s2 det = function
  | Det0 (_,c,k') -> Some (AtP1 (c, Det1 (det,c,k')))
and focus_right_p1 f = function
  | Is1 (np,_,k') -> None
  | Det1 (det,_,k') -> None
  | AndN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n f in
	   Some (AtP1 (List.nth l (n+1), AndN (n+1,l',k')))
	 with _ -> assert false)
  | OrN (n,l,k') ->
      if n >= List.length l - 1
      then None
      else
	(try
	   let l' = Common.list_set_nth l n f in
	   Some (AtP1 (List.nth l (n+1), OrN (n+1,l',k')))
	 with _ -> assert false)
  | Not0 (_,k') -> None
  | Maybe0 (_,k') -> None

let rec focus_left : transf = function
  | AtC (c,k) -> focus_left_c c k
  | AtS (s,k) -> focus_left_s s k
  | AtS1 (np,k) -> focus_left_s1 np k
  | AtS2 (det,k) -> focus_left_s2 det k
  | AtP1 (f,k) -> focus_left_p1 f k
and focus_left_c c = function
  | Nil -> None
  | SeqN (n,l,k') ->
    if n <= 0
    then None
    else
      (try
	 let l' = Common.list_set_nth l n c in
	 Some (AtC (List.nth l' (n-1), SeqN (n-1,l',k')))
       with _ -> assert false)
  | Cond1 (s,c2,k') -> Some (AtS (s, Cond0 (c,c2,k')))
  | Cond2 (s,c1,k') -> Some (AtC (c1, Cond1 (s,c,k')))
  | For2 (np,k') -> Some (AtS1 (np, For1 (c,k')))
  | Quote0 k' -> None
and focus_left_s s = function
  | Assert0 k' -> None
  | Cond0 (c1,c2,k') -> None
  | SuchThat0 _ -> None
  | SAndN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n s in
	   Some (AtS (List.nth l (n-1), SAndN (n-1,l',k')))
	 with _ -> assert false)
  | SOrN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n s in
	   Some (AtS (List.nth l (n-1), SOrN (n-1,l',k')))
	 with _ -> assert false)
  | SNot0 (_,k') -> None
  | SMaybe0 (_,k') -> None
and focus_left_s1 np = function
  | Get0 _ -> None
  | CallN (j,proc,args,k') ->
    if j <= 0
    then None
    else 
      let args' = array_set_nth args j np in
      Some (AtS1 (args'.(j-1), CallN (j-1,proc,args',k')))
  | For1 (c,k') -> None
  | Is0 (_,f1,k') -> None
  | ArgN (j,pred,i,args,k') ->
      let arity = Array.length args - 1 in
      let j' = if j = 0 then arity else j-1 in
      let j' = if j' = i then j'-1 else j' in
      if j' < 1
      then None
      else
	let args = array_set_nth args j np in
	Some (AtS1 (args.(j'), ArgN (j',pred,i,args,k')))
  | NAndN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n np in
	   Some (AtS1 (List.nth l (n-1), NAndN (n-1,l',k')))
	 with _ -> assert false)
  | NOrN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n np in
	   Some (AtS1 (List.nth l (n-1), NOrN (n-1,l',k')))
	 with _ -> assert false)
  | NNot0 (_,k') -> None
  | NMaybe0 (_,k') -> None
and focus_left_s2 det = function
  | Det0 (_,c,k') -> None
and focus_left_p1 f = function
  | Is1 (np,_,k') -> Some (AtS1 (np, Is0 (np,f,k')))
  | Det1 (Quote c,_,k') -> Some (AtC (c, Quote0 (Det0 (Quote c,f,k'))))
  | Det1 (_,_,k') -> None
  | AndN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n f in
	   Some (AtP1 (List.nth l (n-1), AndN (n-1,l',k')))
	 with _ -> assert false)
  | OrN (n,l,k') ->
      if n <= 0
      then None
      else
	(try
	   let l' = Common.list_set_nth l n f in
	   Some (AtP1 (List.nth l (n-1), OrN (n-1,l',k')))
	 with _ -> assert false)
  | Not0 (_,k') -> None
  | Maybe0 (_,k') -> None


let focus_of_c c = AtC (c, default_context_of_c c)
let focus_of_s s = AtS (s, default_context_of_s s)
let focus_of_s1 np = AtS1 (np, default_context_of_s1 np)
let focus_of_p1 f = AtP1 (f, default_context_of_p1 f)

let focus_of_p1 f = focus_of_s (Is (Det (top_s2, f), top_p1))

let focus_top = focus_of_s1 top_s1

let focus_what = focus_of_s1 (Det (Qu (An, Some var_What), top_p1))

let focus_bookmarks =
  AtS1 (top_s1,
	make_role1 Rdf.uri_value `Subject
	  (AndN (1,
		 [has_type Namespace.uri_Bookmark; top_p1],
		 Det1 (Qu (An,None),
		       top_p1,
		       Is0 (top_s1,
			    top_p1,
			    Assert0 Nil)))))

let focus_drafts =
  AtS1 (top_s1,
	make_role1 Rdfs.uri_member `Subject
	  (Det1 (Name (Rdf.URI Namespace.uri_DraftBag),
		 top_p1,
		 Is0 (top_s1,
		      top_p1,
		      Assert0 Nil))))

let focus_of_uri uri = AtS1 (uri_s1 uri, Get0 Nil)
(*AtP1 (top_p1, Is1 (uri_s1 uri, top_p1, Assert0 Nil))*)
  


let rec focus_next_prefix ?(filter = fun foc -> true) : transf = fun foc ->
  match focus_down foc with
  | Some foc' -> focus_next_prefix_aux ~filter foc'
  | None ->
      match focus_next_prefix_right foc with
      | Some foc' -> focus_next_prefix_aux ~filter foc'
      | None -> focus_first foc
and focus_next_prefix_right foc =
  match focus_right foc with
  | Some foc' -> Some foc'
  | None ->
      match focus_up foc with
      | Some foc' -> focus_next_prefix_right foc'
      | None -> None
and focus_next_prefix_aux ~filter foc' =
  if filter foc'
  then Some foc'
  else focus_next_prefix foc'


let rec focus_next_postfix ?(filter = fun foc -> true) : transf = fun foc ->
  match focus_right foc with
  | Some foc' ->
      let foc'' = focus_next_postfix_down foc' in
      focus_next_postfix_aux ~filter foc''
  | None ->
      match focus_up foc with
      | Some foc' -> focus_next_postfix_aux ~filter foc'
      | None -> None (* focus_next_postfix_aux ~filter (focus_next_postfix_down foc) : loops ?*)
(*
	  match focus_first foc with
	  | Some foc'' -> Some (focus_next_postfix_down foc'')
	  | None -> None
*)
and focus_next_postfix_down foc =
  match focus_down foc with
  | None -> foc
  | Some foc' ->
      focus_next_postfix_down foc'
and focus_next_postfix_aux ~filter foc' =
  if filter foc'
  then Some foc'
  else focus_next_postfix ~filter foc'

let focus_first_postfix ?(filter = fun foc -> true) foc =
  let foc' = focus_next_postfix_down foc in
  let foc'' =
    match focus_next_postfix_aux ~filter foc' with
    | Some foc'' -> foc''
    | None -> foc in
  match foc'' with
  | AtC (Get np, ctx) -> AtS1 (np, Get0 ctx)
  | _ -> foc''

(* simplifications *)

(* TODO: avoid simplification altogether by being accurate in focus transformations. *)

let simpl_and_list : p1 list -> p1 =
  fun lf ->
    let lf' =
      List.filter
	(function
	  | Thing
	  | Not Thing
	  | Or [] -> false
	  | _ -> true)
	lf in
    match lf' with
    | [] -> Thing
    | [x] -> x
    | _ -> And (List.sort Pervasives.compare lf')

		(* simplified and of 2 concepts *)
let rec simpl_and : p1 -> p1 -> p1 =
  fun f1 f2 ->
    simpl_and_list (simpl_and_norm f1 @ simpl_and_norm f2)
and simpl_and_norm = function
  | And l -> l
  | f -> [f]
		 
let simpl_or_list : p1 list -> p1 =
  fun lf ->
    if List.mem Thing lf
    then Thing
    else
      match lf with
      | [] -> Not Thing
      | [x] -> x
      | lf' -> Or lf'
		      
		      (* simplified or of 2 concepts *)
let rec simpl_or : p1 -> p1 -> p1 =
  fun f1 f2 -> simpl_or_list (simpl_or_norm f1 @ simpl_or_norm f2)
and simpl_or_norm = function
  | Or alts -> List.fold_left (fun res f -> res @ simpl_or_norm f) [] alts
  | f -> [f]

(* equivalence and entailment *)

let rec same_s1 ~obs store np1 np2 =
  match np1, np2 with
  | Det (det1,f1), Det (det2,f2) -> same_s2 ~obs store det1 det2
  | Det (Qu (An,_), Arg (pred1,0,args1)), Det (Qu (An,_), Arg (pred2,0,args2)) ->
      (* assumes that structures are values, not objects *)
      pred1 = pred2 &&
      Array.length args1 = Array.length args2 &&
      Common.fold_for
	(fun i acc -> acc && same_s1 ~obs store args1.(i) args2.(i))
	1 (Array.length args1 - 1)
	true
  | _ -> false
and same_s2 ~obs store det1 det2 =
  match det1, det2 with
  | Name n1, Name n2 -> n1 = n2
  | Quote s1, Quote s2 -> s1 = s2
  | _ -> false

let rec entails_s1 ~obs store np1 np2 =
  match np1, np2 with
  | Det (Name n, _), Det (Qu (An,_), f2) -> Ext.mem (store#get_entity n) (store#tab_extent ~obs f2 : Ext.t)
  | Det (Qu (An,_), f1), Det (Qu (An,_), f2) -> entails_p1 ~obs store f1 f2
  | _ -> same_s1 ~obs store np1 np2
and entails_s2 det1 det2 =
  match det1, det2 with
  | Name n1, Name n2 -> n1 = n2
  | Name _, Qu _ -> true
  | Ref v1, Ref v2 -> v1 = v2
  | Ref _, Qu _ -> true
  | Qu (qu1,_), Qu (qu2,_) -> entails_qu qu1 qu2
  | _ -> false
and entails_qu qu1 qu2 =
  match qu1, qu2 with
  | _, An -> true
  | _ -> qu1 = qu2
and entails_p1 ~obs store f1 f2 =
  match f1, f2 with
  | Arg (pred1,i1,args1), Arg (pred2,i2,args2) ->
      entails_pred ~obs store pred1 pred2 &&
      i1 = i2 &&
      Array.length args1 = Array.length args2 &&
      Common.fold_for
	(fun i acc -> acc && entails_s1 ~obs store args1.(i) args2.(i))
	0 (Array.length args1 - 1)
	true
  | _, And lf2 ->
      List.for_all (fun f2 -> entails_p1 ~obs store f1 f2) lf2
  | Or lf1, _ ->
      List.for_all (fun f1 -> entails_p1 ~obs store f1 f2) lf1
  | _, Or lf2 ->
      List.exists (fun f2 -> entails_p1 ~obs store f1 f2) lf2
  | And lf1, _ ->
      List.exists (fun f1 -> entails_p1 ~obs store f1 f2) lf1
  | Not f1a, Not f2a -> entails_p1 ~obs store f2a f1a
  | Maybe f1a, Maybe f2a -> entails_p1 ~obs store f1a f2a
  | _, Maybe f2a -> entails_p1 ~obs store f1 f2a
  | _, Thing -> true
  | _, _ -> false
and entails_pred ~obs store pred1 pred2 =
  match pred1, pred2 with
    | Pred.Type a1, Pred.Type a2 ->
        a1 = a2 || store#is_subclass_of ~obs a1 a2
    | Pred.Role (a1, modifs1), Pred.Role (a2, modifs2) ->
        (a1 = a2 || store#is_subproperty_of ~obs a1 a2) &&
	  (modifs1.Pred.direct || not modifs2.Pred.direct) &&
	  (not modifs1.Pred.symmetric || modifs2.Pred.symmetric) &&
	  (not modifs1.Pred.transitive || modifs2.Pred.transitive) &&
	  (not modifs1.Pred.reflexive || modifs2.Pred.reflexive)
    | Pred.Funct f1, Pred.Funct f2 ->
        f1 = f2
    | _ -> false
	
let rec contradicts_s1 ~obs store np1 np2 =
  match np1, np2 with
  | Det (Name n1, _), Det (Name n2, _) -> n1 <> n2
  | Det (Quote s1, _), Det (Quote s2, _) -> s1 <> s2
  | Det (Qu (An,_), f1), Det (Qu (An,_), f2) -> contradicts_p1 ~obs store f1 f2
  | _ -> false
and contradicts_p1 ~obs store f1 f2 =
  match f1, f2 with
  | Arg (pred1,0,args1), Arg (pred2,0,args2) -> (* assumes structures are values *)
      let n1, n2 = Array.length args1, Array.length args2 in
      pred1 <> pred2 ||
      n1 <> n2 ||
      Common.fold_for
	(fun i acc -> acc || contradicts_s1 ~obs store args1.(i) args2.(i))
	1 (n1 - 1)
	false
  | Not f1a, f2a -> entails_p1 ~obs store f2a f1a
  | f1a, Not f2a -> entails_p1 ~obs store f1a f2a
  | _, _ -> false
