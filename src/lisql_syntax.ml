(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Lisql = Lisql_namespace

open Lisql_ast

(* notation for LISQL - for files, not for rendering *)

class context =
  object
    method base : Uri.t = ""
    method xmlns : (string * Uri.t) list = []
  end

(* parsing *)

let rec parse_command = dcg "command"
    [ "@base" then _ = parse_space; uri = parse_uri_rel -> Base uri
    | "@prefix" then _ = parse_space; pre = Name.parse_prefix; _ = parse_space; uri = parse_uri_rel -> Prefix (pre,uri)
    | "@import" then _ = parse_space;
       _ = OPT [ src = parse_uri_rel; _ = parse_space -> src ] ELSE ""; (* for backward compatibility *)
       bas = parse_uri_rel; _ = parse_space;
       filename = Name.parse_string -> Import (bas, filename)
    | "@lod" then _ = parse_space; uri = parse_uri_rel -> LinkedData uri

    | "@rm"; _ = parse_space; n = parse_name -> Command (Call (Proc.RemoveName, [|name_s1 n|]))
    | "@cp"; _ = parse_space; n = parse_name; _ = parse_space; n2 = parse_name -> Command (Call (Proc.CopyName, [|name_s1 n; name_s1 n2|]))
    | "@mv"; _ = parse_space; n = parse_name; _ = parse_space; n2 = parse_name -> Command (Call (Proc.MoveName, [|name_s1 n; name_s1 n2|]))

    | "#" then s = match ".*" as "comment" -> Comment s
    | c = parse_c -> Command c ]
and parse_c = dcg "c"
    [ "nope" -> Nope
    | "from" then _ = parse_space; src = parse_uri_rel; _ = parse_comma; s = parse_s; _ = parse_dot -> Assert s (* src is deprecated *)
    | _ = parse_left_round_bracket then c1 = parse_c;
        ( _ = parse_semicolon; lc1 = LIST1 parse_c SEP parse_semicolon; _ = parse_right_round_bracket -> Seq (c1::lc1)
	| _ = parse_right_round_bracket -> c1 )
    | "if" then _ = parse_space; s = parse_s; _ = parse_space; "then"; _ = parse_space; c1 = parse_c; _ = parse_space; "else"; _ = parse_space; c2 = parse_c -> Cond (s,c1,c2)
    | "for" then _ = parse_space; np = parse_s1; _ = parse_comma; c = parse_c -> For (np,c)
    | "get" then _ = parse_space; np = parse_s1 -> Get np
    | proc, args = parse_call -> Call (proc, args)
    | s = parse_s; _ = OPT parse_dot ELSE "" -> Assert s ]
and parse_call = dcg "call"
    [ "remove"; _ = parse_space; np = parse_s1 -> (Proc.RemoveName, [|np|])
    | "copy"; _ = parse_space; np1 = parse_s1; _ = parse_space; "to"; _ = parse_space; np2 = parse_s1 -> (Proc.CopyName, [|np1;np2|])
    | "move"; _ = parse_space; np1 = parse_s1; _ = parse_space; "to"; _ = parse_space; np2 = parse_s1 -> (Proc.MoveName, [|np1;np2|])
    | proc = parse_proc; _ = parse_left_round_bracket; args = LIST0 parse_s1 SEP parse_comma; _ = parse_right_round_bracket -> (proc, Array.of_list args) ]
and parse_proc = dcg "proc"
    [ "delete" -> Proc.RemoveName
    | "copy" -> Proc.CopyName
    | "move" -> Proc.MoveName
    | op = parse_prim -> Proc.Prim op ]
and parse_s = dcg "s"
    [ np = parse_s1; c = OPT [ _ = parse_space; c = parse_p1_seq -> c ] ELSE Thing -> Is (np,c)
    | "true" -> True
    | _ = parse_left_round_bracket; s1 = parse_s;
	( _ = parse_and; ls1 = LIST1 parse_s SEP parse_and; _ = parse_right_round_bracket -> SAnd (s1::ls1)
        | _ = parse_or; ls1 = LIST1 parse_s SEP parse_or; _ = parse_right_round_bracket -> SOr (s1::ls1)
	| _ = parse_right_round_bracket -> SAnd [s1] )
    | "not"; _ = parse_space; s1 = parse_s -> SNot s1
    | "maybe"; _ = parse_space; s1 = parse_s -> SMaybe s1 ]
(*    | "for"; _ = parse_space; np = parse_s1; _ = parse_comma; a = parse_s -> Is (np, SuchThat ("",a)) ] *)
and parse_s1 = dcg "s1"
    [ q = parse_s2; c = OPT [ _ = parse_space; c = parse_block -> c ] ELSE Thing -> Det (q, c)
    | c = parse_block -> an c
    | _ = parse_left_round_bracket; np1 = parse_s1;
	( _ = parse_and; lnp1 = LIST1 parse_s1 SEP parse_and; _ = parse_right_round_bracket -> NAnd (np1::lnp1)
        | _ = parse_or; lnp1 = LIST1 parse_s1 SEP parse_or; _ = parse_right_round_bracket -> NOr (np1::lnp1)
	| _ = parse_right_round_bracket -> NAnd [np1] )
    | "not"; _ = parse_space; np1 = parse_s1 -> NNot np1
    | "maybe"; _ = parse_space; np1 = parse_s1 -> NMaybe np1 ]
and parse_s2 = dcg "s2"
    [ n = parse_name -> Name n
    | _ = parse_left_curly_bracket; c = parse_c; _ = parse_right_curly_bracket -> Quote c
    | v = parse_var -> Ref v
    | qu = parse_qu; v_opt = OPT [ _ = parse_space; v = parse_var -> Some v ] ELSE None -> Qu (qu,v_opt)
    | "which" -> Which ]
and parse_qu = dcg "qu"
    [ "some" -> An
    | "the" -> The
    | "every" -> Every
    | "only" -> Only
    | "no" -> No
    | "each" -> Each ]
and parse_block = dcg "block"
    [ _ = parse_left_square_bracket; c = OPT parse_p1_seq ELSE Thing; _ = parse_right_square_bracket -> c
    | funct, focus, args = parse_struct_arg parse_uri when "" focus = 0 -> Arg (Pred.Funct funct,0,args) ]
and parse_p1_seq = dcg "p1_seq"
    [ lc = LIST1 parse_p1 SEP parse_semicolon -> match lc with [] -> Thing | [c1] -> c1 | _ -> And lc ]
and parse_p1 = dcg "p1"
    [ _ = parse_equal; np = parse_s1 -> has_equal np
    | "a"; _ = parse_space; ( "thing" -> Thing | a = parse_uri -> has_type a )
    | op, ikind = parse_prim2_ikind; _ = parse_space; np = parse_s1 -> has_prim2 op (Argindex.of_kind ikind) np
    | r, ikind = parse_role_ikind; _ = parse_space; np = parse_s1 -> has_role r (Argindex.of_kind ikind) np
    | _ = parse_left_round_bracket; c1 = parse_p1;
	( _ = parse_and; lc1 = LIST1 parse_p1 SEP parse_and; _ = parse_right_round_bracket -> And (c1::lc1)
        | _ = parse_or; lc1 = LIST1 parse_p1 SEP parse_or; _ = parse_right_round_bracket -> Or (c1::lc1)
	| _ = parse_right_round_bracket -> And [c1] )
    | "not"; _ = parse_space; c1 = parse_p1 -> Not c1
    | "maybe"; _ = parse_space; c1 = parse_p1 -> Maybe c1
    | x = parse_var; _ = parse_space; "such"; _ = parse_space; "that"; _ = parse_space; s = parse_s -> SuchThat (x,s)
    | op, focus, args = parse_struct_arg parse_prim when "" focus <> 0 -> Arg (Pred.Prim op,focus,args)
    | _ = parse_is; _ = parse_space;
	funct, focus, args = parse_struct_arg parse_uri when "" focus = 0 ->
	  Arg (Pred.Funct funct,0,args)
    | _ = parse_has; _ = parse_space;
	  funct, focus, args = parse_struct_arg parse_uri when "" focus <> 0;
	  _ = parse_space; st = parse_s1 ->
	  let args = array_set_nth args 0 st in
	  Arg (Pred.Funct funct,focus,args)
    ]
and parse_struct_arg parse_f = dcg "struct_arg"
    [ f = parse_f;
      _ = parse_left_round_bracket;
      focus, args = parse_args;
      _ = parse_right_round_bracket ->
	f, focus, args ]
and parse_args = dcg "args"
    [ focus, l_args = parse_args_aux 1 -> focus, Array.of_list (top_s1::l_args) ]
and parse_args_aux i = dcg
    [ "this";
      ( _ = parse_comma; focus, l_args = parse_args_aux (i+1) when "" focus = 0 -> i, top_s1::l_args
      |  -> i, [top_s1] )
    | np1 = parse_s1;
      ( _ = parse_comma; focus, l_args = parse_args_aux (i+1) -> focus, np1::l_args
      |  -> 0, [np1] ) ]
and parse_prim2_ikind = dcg "prim2_ikind"
(*
  [ "=" -> Operator.eq, `Subject
  | "!=" -> Operator.neq, `Subject
*)
  [ "<=" -> "leq", `Subject
  | ">=" -> "leq", `Object
  | "<" -> "lt", `Subject
  | ">" -> "lt", `Object
  | "matches" -> "regex", `Subject
  | "matched"; _ = parse_space; "by" -> "regex", `Object
  | "contains" -> "contains", `Subject
  | "contained"; _ = parse_space; "by" -> "contains", `Object ]
and parse_role_ikind = dcg "role_ikind"
  [ _ = parse_has; r = parse_role -> r, `Subject
  | _ = parse_is; r = parse_role; _ = parse_space; "of" -> r, `Object
  | r = parse_role;
    ( _ = parse_colon -> r, `Subject
    | _ = parse_space; "of" -> r, `Object
    |  -> r, `Subject ) ]
and parse_role = dcg "role"
  [ "opt"; _ = parse_space; p, modifs = parse_role -> p, {modifs with Pred.reflexive=true}
  | "trans"; _ = parse_space; p, modifs = parse_role -> p, {modifs with Pred.transitive=true}
  | "direct"; _ = parse_space; p, modifs = parse_role -> p, {modifs with Pred.direct=true}
  | "sym"; _ = parse_space; p, modifs = parse_role -> p, {modifs with Pred.symmetric=true}
  | p = parse_uri -> p, Pred.default_role_modifs ]
and parse_var = dcg "var"
    [ "?"; ( s = Turtle.parse_name -> s
           | s = Turtle.parse_string -> s ) ]
and parse_name = dcg "name"
    [ ?ctx; n = Name.parse ctx#base ctx#xmlns -> n ]
and parse_uri = dcg "uri"
    [ ?ctx; uri = Name.parse_uri ctx#base ctx#xmlns -> uri ]
and parse_uri_rel = dcg "uri_rel"
    [ ?ctx; uri = Name.parse_uri_rel ctx#base -> uri ]
and parse_prim = dcg "prim"
  [ "$"; op = match "[a-z][a-zA-Z0-9_-]+" as "prim" -> op ]
and parse_is = dcg "is"
    [ "is"; _ = parse_space -> "is " ]
and parse_has = dcg "has"
    [ "has"; _ = parse_space -> "has " ]
and parse_or = dcg "or"
    [ s = match "[ \n\t\r]+or[ \n\t\r]+" -> s ]
and parse_and = dcg "and"
    [ s = match "[ \n\t\r]+and[ \n\t\r]+" -> s ]
and parse_space = dcg "space"
    [ s = match "[ \n\t\r]+" -> s ]
and parse_comma = dcg "comma"
    [ s = match "[ \n\t\r]*,[ \n\t\r]*" -> s ]
and parse_semicolon = dcg "semicolon"
    [ s = match "[ \n\t\r]*;[ \n\t\r]*" -> s ]
and parse_colon = dcg "colon"
    [ s = match "[ \n\t\r]+:" -> s ]
and parse_dot = dcg "dot"
    [ s = match "[ \n\t\r]*[.][ \n\t\r]*" -> s ]
and parse_equal = dcg "equal"
  [ s = match "[ \n\t\r]*=[ \n\t\r]*" -> s ]
and parse_left_round_bracket = dcg "left-round-bracket"
    [ s = match "([ \n\t\r]*" -> s ]
and parse_right_round_bracket = dcg "right-round-bracket"
    [ s = match "[ \n\t\r]*)" -> s ]
and parse_left_square_bracket = dcg "left-square-bracket"
    [ s = match "\\[[ \n\t\r]*" -> s ]
and parse_right_square_bracket = dcg "right-square-bracket"
    [ s = match "[ \n\t\r]*\\]" -> s ]
and parse_left_curly_bracket = dcg "left-curly-bracket"
    [ s = match "{[ \n\t\r]*" -> s ]
and parse_right_curly_bracket = dcg "right-curly-bracket"
    [ s = match "[ \n\t\r]*}" -> s ]

let parse_of_string ?(ctx = new context) parse_x =
  let p_eof = dcg [ x = parse_x; EOF -> x ] in
  fun s -> Common.prof "Lisql.Syntax.parse_of_string" (fun () ->
    try
      let ctx', x = Dcg.once p_eof ctx (Matcher.cursor_of_string s) in
      x
    with exn ->
      print_endline (Printexc.to_string exn);
      print_endline ("in: " ^ s);
      raise exn)

let valid_string ?ctx parse_x =
  fun s ->
    try ignore (parse_of_string ?ctx parse_x s); true with _ -> false

let name_of_string ?ctx = parse_of_string ?ctx parse_name
let class_of_string ?ctx = parse_of_string ?ctx parse_p1
let assertion_of_string ?ctx = parse_of_string ?ctx parse_c

(* printing *)

let rec print_command = ipp
  [ Base uri ->
      "@base"; print_space; print_uri_rel of uri
  | Prefix (pre, uri) ->
      "@prefix"; print_space; 'pre; print_space; print_uri_rel of uri
  | Import (bas, filename) ->
      "@import";
      print_space; print_uri_rel of bas;
      print_space; print_filename of filename
  | LinkedData uri ->
      "@lod"; print_space; print_uri_rel of uri
(*
  | CopyName (n,n2) ->
      "@cp"; print_space; print_name of n; print_space; print_name of n2
  | MoveName (n,n2) ->
      "@mv"; print_space; print_name of n; print_space; print_name of n2
  | RemoveName n ->
      "@rm"; print_space; print_name of n
*)
  | Comment s -> 's
  | Command c -> print_c of c ]
and print_c = ipp
  [ Nope -> "nope"
  | Assert s -> print_s of s; print_dot
  | Get np -> "get"; print_space; print_s1 of np
  | Call (proc,args) -> print_proc of proc; "("; LIST0 print_s1 SEP ", " of Array.to_list args; ")"
  | Seq [] -> "nope"
  | Seq lc -> "("; LIST1 print_c SEP "; " of lc; ")"
  | Cond (s,c1,c2) -> "if"; print_space; print_s of s; print_space; "then"; print_space; print_c of c1; print_space; "else"; print_space; print_c of c2
  | For (np,c) -> "for"; print_space; print_s1 of np; print_comma; print_c of c ]
and print_proc = ipp
  [ Proc.RemoveName -> "delete"
  | Proc.CopyName -> "copy"
  | Proc.MoveName -> "move"
  | Proc.Prim op -> print_prim of op ]
and print_s = ipp
    [ Is (np,Thing) -> print_s1 of np
    | Is (np,f) -> print_s1 of np; print_space; print_p1_seq of f
    | True -> "true"
    | SAnd ls -> "("; LIST1 print_s SEP " and " of ls; ")"
    | SOr ls -> "("; LIST1 print_s SEP " or " of ls; ")"
    | SNot s1 -> "not"; print_space; print_s of s1
    | SMaybe s1 -> "maybe"; print_space; print_s of s1 ]
and print_s1 = ipp
    [ Det (Qu (An, None), c) -> print_block of c
    | Det (det,Thing) -> print_s2 of det
    | Det (det,c) -> print_s2 of det; print_space; print_block of c
    | NAnd lnp -> "("; LIST1 print_s1 SEP " and " of lnp; ")"
    | NOr lnp -> "("; LIST1 print_s1 SEP " or " of lnp; ")"
    | NNot np1 -> "not"; print_space; print_s1 of np1
    | NMaybe np1 -> "maybe"; print_space; print_s1 of np1 ]
and print_s2 = ipp
    [ Name n -> print_name of n
    | Quote c -> "{ "; print_c of c; " }"
    | Ref v -> print_var of v
    | Qu (qu,None) -> print_qu of qu
    | Qu (qu,Some v) -> print_qu of qu; print_space; print_var of v
    | Which -> "which" ]
and print_qu = ipp
    [ An -> "some"
    | The -> "the"
    | Every -> "every"
    | Only -> "only"
    | No -> "no"
    | Each -> "each" ]
and print_block = ipp
    [ Thing -> "[]"
    | Arg (Pred.Funct funct,0,args) -> print_struct_arg print_uri of funct, 0, args
    | c -> "[ "; print_p1_seq of c; " ]" ]
and print_p1_seq = ipp
    [ Thing -> 
    | And lc -> LIST0 print_p1 SEP print_semicolon of lc
    | c -> print_p1 of c ]
and print_p1 = ipp
    [ Thing -> "a"; print_space; "thing"
    | And lc -> "("; LIST1 print_p1 SEP " and " of lc; ")"
    | Or lc -> "("; LIST1 print_p1 SEP " or " of lc; ")"
    | Not c1 -> "not"; print_space; print_p1 of c1
    | Maybe c1 -> "maybe"; print_space; print_p1 of c1
    | SuchThat (x,s) -> print_var of x; print_space; "such that"; print_space; print_s of s
    | Arg (Pred.Equal, i, args) -> "="; print_space; print_s1 of args.(Argindex.opposite i)
    | Arg (Pred.Type a,_,_) -> "a"; print_space; print_uri of a
    | Arg (Pred.Role r, i, args) -> print_role_ikind of r, Argindex.to_kind i; print_space; print_s1 of args.(Argindex.opposite i)
    | Arg (Pred.Funct funct,0,args) ->
	"is"; print_space; print_struct_arg print_uri of funct, 0, args
    | Arg (Pred.Funct funct,i,args) ->
      when i <> 0;
	"has"; print_space; print_struct_arg print_uri of funct, i, args;
	print_space; print_s1 of args.(0)
    | Arg (Pred.Prim op,i,args) ->
        print_struct_arg print_prim of op, i, args ]
and print_role_ikind = ipp
  [ r, `Subject -> print_role of r
  | r, `Object -> "is"; print_space; print_role of r; print_space; "of" ]
and print_role = ipp
  [ p, modifs ->
    '(if modifs.Pred.reflexive then "opt " else "");
    '(if modifs.Pred.transitive then "trans " else "");
    '(if modifs.Pred.symmetric then "sym " else "");
    '(if modifs.Pred.direct then "direct " else "");
    print_uri of p ]
and print_struct_arg print_f = ipp
  [ f, i, args -> print_f of f; "("; print_args of (i,args); ")" ]
and print_args = ipp
    [ i, args ->
      for l_args =
	List.tl (Array.to_list
	  (if i = 0
	   then args
	   else array_set_nth args i (Det (Ref var_this, Thing))))
      do LIST0 print_s1 SEP print_comma of l_args ]
and print_name = ipp [ n -> ?ctx; '(Name.to_string ctx#base ctx#xmlns n) ]
and print_uri = ipp [ uri -> ?ctx; '(Name.string_of_uri ctx#base ctx#xmlns uri) ]
and print_uri_rel = ipp [ uri -> ?ctx; '(Name.string_of_uri_rel ctx#base uri) ]
and print_filename = ipp [ filename -> '(Name.string_of_shortString filename) ]
and print_prim = ipp [ op -> "$"; 'op ]
and print_var_opt = ipp [ None -> | Some v -> print_var of v ]
and print_var = ipp [ "this" -> "this" | v -> "?\""; '(Turtle.escape_shortString v); "\"" ]
and print_space = ipp [ _ -> " " ]
and print_comma = ipp [ _ -> ", " ]
and print_semicolon = ipp [ _ -> "; " ]
and print_dot = ipp [ _ -> " ." ]

let print_to_string ?(ctx = new context) print_x =
  let p_eof = ipp [ x -> print_x of x; EOF ] in
  fun x -> Common.prof "Lisql.Syntax.print_to_string" (fun () ->
    try
      let buf_cursor = new Printer.buffer_cursor in
      Ipp.once p_eof x buf_cursor ctx;
      buf_cursor#contents
    with _ -> assert false)

let string_of_name ?ctx = print_to_string ?ctx print_name
let string_of_class ?ctx = print_to_string ?ctx print_block
let string_of_sentence ?ctx = print_to_string ?ctx print_s
let string_of_assertion ?ctx = print_to_string ?ctx print_c
