(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* Terms vocabulary *)

(* namespace *)
let prefix = "term:"
let namespace = "http://www.irisa.fr/LIS/ferre/RDFS/term#"

(* classes *)
let uri_Term = namespace ^ "Term"
let uri_Functor = namespace ^ "Functor"
let uri_ImplicitFunctor = namespace ^ "ImplicitFunctor"
let uri_Operator = namespace ^ "Operator"
let uri_InfixOperator = namespace ^ "InfixOperator"
let uri_PrefixOperator = namespace ^ "PrefixOperator"
let uri_PostfixOperator = namespace ^ "PostfixOperator"
let uri_LeftAssociativeInfixOperator = namespace ^ "LeftAssociativeInfixOperator"
let uri_RightAssociativeInfixOperator = namespace ^ "RightAssociativeInfixOperator"
let uri_MixfixOperator = namespace ^ "MixfixOperator"

let uri_Verb = namespace ^ "Verb"
let uri_IntransitiveVerb = namespace ^ "IntransitiveVerb"
let uri_TransitiveVerb = namespace ^ "TransitiveVerb"

let uri_ArgProperty = namespace ^ "ArgProperty"

(* properties *)
let uri_functor = namespace ^ "functor"
let uri_arity = namespace ^ "arity"
let uri_precedence = namespace ^ "precedence"
let uri_sublist = namespace ^ "sublist"
let uri_subterm = namespace ^ "subterm"
let uri_argAny = namespace ^ "argAny"
let uri_arg_prefix = namespace ^ "arg"
let uri_arg_prefix_length = String.length uri_arg_prefix
let uri_arg n = uri_arg_prefix ^ string_of_int n
let is_arg s =
  s <> uri_argAny
  && String.length s > uri_arg_prefix_length
  && String.sub s 0 uri_arg_prefix_length = uri_arg_prefix
let rank_of_arg s =
  if is_arg s
  then int_of_string (String.sub s uri_arg_prefix_length (String.length s - uri_arg_prefix_length))
  else invalid_arg "Term.rank_of_arg: not arg"

let uri_functorType = namespace ^ "functorType"
let uri_functorArg_prefix = namespace ^ "functorArg"
let uri_functorArg_prefix_length = String.length uri_functorArg_prefix
let uri_functorArg n = uri_functorArg_prefix ^ string_of_int n
let is_functorArg s =
  String.length s > uri_functorArg_prefix_length
  && String.sub s 0 uri_functorArg_prefix_length = uri_functorArg_prefix
let rank_of_functorArg s =
  if is_functorArg s
  then int_of_string (String.sub s uri_functorArg_prefix_length (String.length s - uri_functorArg_prefix_length))
  else invalid_arg "Term.rank_of_functorArg: not functorArg"

(* individuals *)
let uri_stat = namespace ^ "stat"
let uri_nil = namespace ^ "nil"
let uri_cons = namespace ^ "cons"

let uri_maxPrecedence = namespace ^ "maxPrecedence"
let uri_equalPrecedence = namespace ^ "equalPrecedence"
let uri_plusPrecedence = namespace ^ "plusPrecedence"
let uri_timesPrecedence = namespace ^ "timesPrecedence"
let uri_powerPrecedence = namespace ^ "powerPrecedence"
let uri_factPrecedence = namespace ^ "factPrecedence"
