(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Lisql_ast

module Oidmap = Intmap.M
module Varmap = Map.Make (struct type t = var let compare = Pervasives.compare end)

module Ext = Intset.Intmap
module Rel = Intreln.Intmap
module Extension = Extension.Make
module Store = Store.Make
module Fol = Fol.Make
module Display = Lisql_display
module Semantics = Lisql_semantics
module Feature = Lisql_feature
module Transf = Lisql_transf
module Index = Lisql_index

module Descr =
  struct
    type t = { entities : Ext.t;
	       classes : Ext.t;
	       fwd_properties : Ext.t;
	       bwd_properties : Ext.t;
	       structs : Rel.t;
	     }

    let empty = { entities = Ext.empty;
		  classes = Ext.empty;
		  fwd_properties = Ext.empty;
		  bwd_properties = Ext.empty;
		  structs = Rel.empty 3;
		}

    let of_oid store oid =
      let n = store#get_name oid in
      let d = store#get_description oid in
      { entities = Ext.singleton oid;
	classes = begin
	  let cs = d#classes#set in
	  if Ext.is_empty cs
	  then match n with
	    | Rdf.Literal (_, Rdf.Plain lang) ->
	      if lang == ""
	      then Ext.singleton store#xsd_string#oid
	      else Ext.singleton store#rdf_langString#oid
	    | Rdf.Literal (_, Rdf.Typed dt) -> Ext.singleton (store#get_entity (Rdf.URI dt))
	    | _ -> Ext.empty (*Ext.singleton store#rdfs_Resource#oid*)
	  else cs
	end;
	fwd_properties = d#fwd_properties#set;
	bwd_properties = d#bwd_properties#set;
	structs = d#structs#set;
      }

    let inter d1 d2 =
      { entities = Ext.inter d1.entities d2.entities;
	classes = Ext.inter d1.classes d2.classes;
	fwd_properties = Ext.inter d1.fwd_properties d2.fwd_properties;
	bwd_properties = Ext.inter d1.bwd_properties d2.bwd_properties;
	structs = Rel.inter d1.structs d2.structs;
      }
    let inter_r = function
      | [] -> assert false
      | d::ld -> List.fold_left inter d ld

    let union d1 d2 =
      { entities = Ext.union d1.entities d2.entities;
	classes = Ext.union d1.classes d2.classes;
	fwd_properties = Ext.union d1.fwd_properties d2.fwd_properties;
	bwd_properties = Ext.union d1.bwd_properties d2.bwd_properties;
	structs = Rel.union d1.structs d2.structs;
      }
    let union_r = function
      | [] -> empty
      | d::ld -> List.fold_left union d ld

  end


let rec descr_s1_atom_focus (f : p1) (k : context_s1) : p1 =
  descr_s1_context f [] k
and descr_s1_context (f : p1) (lnp' : s1 list) : context_s1 -> p1 = function
  | Get0 k -> f
  | CallN (n,proc,args,k) -> f (* procedure calls are not traversed *)
  | For1 (c,k) -> f
  | Is0 (_,f1,k) ->
      simpl_and_list [f; descr_p1 lnp' f1]
  | ArgN (n,pred,i,args,k) ->
      if Pred.primitive pred
      then f (* operators are not traversed *)
      else
	simpl_and_list
	  [f;
	   descr_p1_context
	     (fun np -> Arg (pred,n,array_set_nth args i np))
	     (List.map (fun np' -> Arg (pred, i, array_set_nth args n np')) lnp')
	     k]
  | NAndN (n,l,k) ->
      descr_s1_context f (Common.list_remove_nth l n @ lnp') k
  | NOrN (n,l,k) -> descr_s1_context f lnp' k
  | NNot0 (_,k) -> descr_s1_context f lnp' k
  | NMaybe0 (_,k) -> descr_s1_context f lnp' k
and descr_p1_focus (f : p1) (k : context_p1) : p1 =
  let rec r_self : s1 -> p1 = function
    | Det (_det,f) -> f (* det is ignored *)
    | NAnd l -> And (List.map r_self l)
    | NOr l -> Or (List.map r_self l)
    | NNot np -> Not (r_self np)
    | NMaybe np -> Maybe (r_self np) in
  descr_p1_context r_self [f] k
and descr_p1_context (r : s1 -> p1) (lf' : p1 list) : context_p1 -> p1 = function
  | Is1 (np,_,k) ->
      r (descr_s1 lf' np)
  | Det1 (det,_,k) ->
      r (Det (det, descr_s1_context (simpl_and_list lf') [] k))
  | AndN (n,l,k) ->
      descr_p1_context r (Common.list_remove_nth l n @ lf') k
  | OrN (n,l,k) -> descr_p1_context r lf' k
  | Not0 (_,k) -> descr_p1_context r lf' k
  | Maybe0 (_,k) -> descr_p1_context r lf' k
and descr_p1 (lnp' : s1 list) (f : p1) : p1 =
  match f with
  | Arg (pred,i,args) ->
      if Pred.primitive pred
      then Thing (* operators are ignored *)
      else
	let arity = Array.length args - 1 in
	let args' = Array.copy args in
	for j = 0 to arity do
	  if j <> i && (i > 0 || Pred.reifiable pred) then
	    args'.(j) <- descr_s1 (List.map (fun np' -> Arg (pred,j, array_set_nth args i np')) lnp') args.(j)
	done;
	Arg (pred,i,args')
  | SuchThat _ -> f (* TODO *)
  | Thing -> f
  | And l -> And (List.map (descr_p1 lnp') l)
  | Or l -> Or (List.map (descr_p1 lnp') l)
  | Not f1 -> Not (descr_p1 lnp' f1)
  | Maybe f1 -> Maybe (descr_p1 lnp' f1)
and descr_s1 lf' np =
  match np with
  | Det (det,f) -> Det (det, simpl_and_list (f::lf'))
  | NAnd l -> NAnd (List.map (descr_s1 lf') l)
  | NOr l -> NOr (List.map (descr_s1 lf') l)
  | NNot np -> NNot (descr_s1 lf' np)
  | NMaybe np -> NMaybe (descr_s1 lf' np)


(* increments *)

type supp = int * int (* ratio *)

let is_empty_supp (i,n) = i = 0

class increment feat ~supp ~anew =
  object (self)
    method feature : Feature.feature = feat
    method supp : supp = supp
    method ext : Ext.t = Ext.empty (* TODO *)
    method anew : bool = anew
  end

let increment feat ~supp ~anew = new increment feat ~supp ~anew

let increment_s12 store =
  increment
    (new Feature.feature_something store)
    ~supp:(1,0) ~anew:false

let increment_name store (n : Name.t) ~supp ~anew =
  increment
    (new Feature.feature_name store n)
    ~supp ~anew
	
let increment_ref store (v : var) ~supp ~anew =
  increment
    (new Feature.feature_ref store v)
    ~supp ~anew

let increment_p12 store =
  increment
    (new Feature.feature_thing store)
    ~supp:(1,0) ~anew:false

let increment_type store (c : Uri.t) ~supp ~anew =
  increment
    (new Feature.feature_type store c)
    ~supp ~anew

let increment_role store (ikind : Argindex.kind) (p : Uri.t) ~supp ~anew =
  increment
    (new Feature.feature_role store ikind p)
    ~supp ~anew

let increment_funct store (funct : Uri.t) (arity : int) (i : int) ~supp ~anew =
  increment
    (new Feature.feature_funct store funct arity i)
    ~supp ~anew

let increment_prim store (op : string) (arity : int) (i : int) ~supp ~anew =
  increment
    (new Feature.feature_prim store op arity i)
    ~supp ~anew


let prim_increments_from_class store (c : Uri.t) ~supp ~anew : increment list =
  let res = ref (LSet.empty ()) in
  store#iter_primitives
    (fun (op,a) prim ->
      List.iter
	(fun t ->
	  ignore (List.fold_left
		    (fun i c1 ->
		      if c1 = c (*|| c1 = Rdfs.uri_Resource*)
		      then res := LSet.add (op,a,i) !res;
		      i+1)
		    1 t))
	prim#types);
  List.map
    (fun (op,a,i) -> increment_prim store op a i ~supp ~anew)
    !res

let increment_proc store (proc : Proc.t) (arity : int) (i : int) ~supp ~anew =
  increment
    (new Feature.feature_proc store proc arity i)
    ~supp ~anew


let proc_increments_from_class store (c : Uri.t) ~supp ~anew : increment list =
  let res = ref (LSet.empty ()) in
  store#iter_procedures
    (fun (op,a) proc ->
      List.iter
	(fun t ->
	  ignore (List.fold_left
		    (fun i c1 ->
		      if c1 = c
		      then res := LSet.add (Proc.Prim op, a, i) !res;
		      i+1)
		    0 t))
	proc#types);
  if c = Rdfs.uri_Resource then
    res := (Proc.RemoveName, 1, 0) :: (Proc.CopyName, 2, 0) :: (Proc.MoveName, 2, 0) :: !res;
  List.map
    (fun (op,a,i) -> increment_proc store op a i ~supp ~anew)
    !res


let root_classes = [Rdfs.uri_Resource; Owl.uri_Thing]
  (* ; Rdfs.uri_Literal]  // removed to have correct primitive suggestions *)

(* TODO: optimize by direct computation *)
let fold_top_classes ~obs store (ff : 'a -> Name.t -> 'a) (init : 'a) : 'a =
  let np_roots = NOr (List.map (fun uri -> name_s1 (Rdf.URI uri)) root_classes) in
  let f = And [Or [has_role (Rdfs.uri_subClassOf, {Pred.default_role_modifs with Pred.direct=true}) 1 np_roots;
		   And [has_type Rdfs.uri_Class;
			Not (has_role (Rdfs.uri_subClassOf, {Pred.default_role_modifs with Pred.direct=true}) 1 top_s1)]];
	       Not (has_equal np_roots)] in
  Ext.fold
    (fun res oid -> ff res (store#get_name oid))
    init (store#tab_extent ~obs f : Ext.t)

let root_properties = [Owl.uri_topObjectProperty; Owl.uri_topDataProperty]
  
(* TODO: optimize by direct computation *)
let fold_top_properties ~obs store (ff : 'a -> Name.t -> 'a) (init : 'a) : 'a =
  let np_roots = NOr (List.map (fun uri -> name_s1 (Rdf.URI uri)) root_properties) in
  let f = And [Or [has_role (Rdfs.uri_subPropertyOf, {Pred.default_role_modifs with Pred.direct=true}) 1 np_roots;
		   And [has_type Rdf.uri_Property;
			Not (has_role (Rdfs.uri_subPropertyOf, {Pred.default_role_modifs with Pred.direct=true}) 1 top_s1)]];
	       Not (has_equal np_roots)] in
  Ext.fold
    (fun res oid -> ff res (store#get_name oid))
    init (store#tab_extent ~obs f : Ext.t)
    

(* concepts *)

class concept_answers ~obs store gv (lv : Extension.var list) (ext : Extension.t) =
  let rel = Common.prof "Lisql_concept.concept_answers/ext_relation" (fun () -> ext#relation (store :> Extension.store) lv) in
  let nb = Rel.cardinal rel in
let _ = prerr_endline ("Lisql_concept/answers_extension: [" ^ String.concat " " lv ^ "] " ^ ext#string) in
  object
    method extension = ext
    method accessible_vars = lv
    method answers = lv, rel
    method nb_answers = nb
    method fold_answers : 'a. ('a -> (var * Extension.oid) list -> 'a) -> 'a -> 'a =
      fun f init -> Common.prof "Lisql_concept.concept_answers#fold_answers" (fun () ->
	Rel.fold
	  (fun res lo -> f res (List.combine lv lo))
	  init
	  rel)
  end

class concept_relax ~obs store descr =
let _ = prerr_endline ("Lisql_concept.relaxed_descr: " ^ Lisql_syntax.string_of_class descr) in
  object (self)
    val mutable closed = false
    val mutable max_rank = 0
    val h_rank_ext : (int, int * Ext.t) Hashtbl.t = Hashtbl.create 5

    method private more_extent (dist, ext) = Common.prof "Lisql_concept.concept_relax#more_extent" (fun () ->
      let res_opt =
	Common.fold_while
	  (function
	    | Some (d,e) ->
		if d <> dist && not (Ext.subset e ext)
		then None
		else Some
		    ( match Lisql_semantics.Relax.safe_extent ~obs store (d+1) descr with
		    | Semantics.SE_Some e -> Some (d+1, e)
		    | Semantics.SE_All
		    | Semantics.SE_Undef -> None)
	    | None -> None)
	  (Some (dist, ext)) in
      if res_opt = None && dist < 0
      then Some (0, store#all_oids ~obs)
      else res_opt)

    method private next_rank_ext =
      let max_dist, max_ext =
	if max_rank = 0
	then -1, Ext.empty
	else
	  try Hashtbl.find h_rank_ext max_rank
	  with _ -> assert false in
      match self#more_extent (max_dist,max_ext) with
      | Some (d,e) ->
	  max_rank <- max_rank+1;
	  Hashtbl.add h_rank_ext max_rank (d,e);
	  Some e
      | None ->
	  closed <- true;
	  None

    method rank_ext (rank : int) : Ext.t option =
      assert (rank > 0);
      try Some (snd (Hashtbl.find h_rank_ext rank))
      with Not_found ->
	if closed
	then None
	else
	  match self#next_rank_ext with
	  | Some e ->
	      if max_rank = rank
	      then Some e
	      else self#rank_ext rank
	  | None -> None

    method rank_succ (rank : int) : int option =
      let rank1 = rank+1 in
      match self#rank_ext rank1 with
      | None -> None
      | Some _ -> Some rank1

    method entails ~obs (f : p1) = entails_p1 ~obs store descr f

  end


type window =
  [ `None
  | `Refs of Extension.var list
  | `Extent of Extension.var list * Uri.t LSet.t ]

let get_count_anew rank total card =
  let c = total * card in
  let count = if rank = 0 then c else total in
  let anew = rank > 0 && c = 0 in
  count, anew

class concept ~obs store (c_which : c) (descr : p1) ~(ltransf : Transf.kind list) ~(insert_s1 : bool) ~(insert_p1 : bool) =
  let _ = prerr_endline ("c_which = " ^ Lisql_syntax.string_of_assertion c_which) in
  let gv = new gen_var in
  let fol = Lisql_semantics.fol_c ~obs store gv c_which in
  let _ = prerr_string "fol: "; prerr_string (Fol.string_of_fol fol); prerr_newline () in
  let w, fol_intent = Fol.intent var_focus fol in
  let _  = prerr_string "window: "; prerr_string (String.concat ", " w); prerr_newline () in
  let _ = prerr_string "fol_intent: "; prerr_string (Fol.string_of_fol fol_intent); prerr_newline () in
  let typing = Fol.typing_of_fol ~obs store fol_intent in
  let _ =
    prerr_string "*** typing ***\n";
    List.iter
      (fun m_t ->
	List.iter
	  (fun (v,t) ->
	    prerr_string v; prerr_string " : "; prerr_string (Fol.string_of_type t); prerr_string ", ")
	  m_t;
	prerr_string "\n")
      typing in
  let w_types : Uri.t LSet.t = (* class types common to all window variables *)
    List.fold_left
      (fun res m_t ->
	let ts =
	  List.fold_left
	    (fun ts v -> try LSet.add (List.assoc v m_t) ts with _ -> ts)
	    (LSet.empty ()) w in
	match ts with
	  | [`Class c] -> LSet.add c res
	  | _ -> res)
      (LSet.empty ()) typing in
  let _ = prerr_string "window types ="; List.iter (fun uri -> prerr_string " "; prerr_string uri) w_types; prerr_newline () in
  let flatten_typing = Fol.flatten_typing typing in
  let _ =
    prerr_endline "typing:";
    List.iter
      (fun (x,la) ->
	prerr_string ("- " ^ x ^ " :");
	List.iter
	  (fun a -> 
	    prerr_string " ";
	    prerr_string (Fol.string_of_type a);
	    prerr_newline ())
	  la)
      flatten_typing in
  let eval, ext = Fol.extension_of_fol ~obs store fol_intent in
  let lv = eval.Fol.optvars in
  let window, descr : window * p1 =
    let bound_w = List.filter (fun v -> List.mem v eval.Fol.vars) w in (* some window variables may have disapeared when computing [ext] *)
    if bound_w <> [] then
      let _ = prerr_string "window ="; List.iter (fun v -> prerr_string " "; prerr_string v) w; prerr_newline () in
      `Refs bound_w, descr
    else if w_types <> [] then
      let descr =
	if descr = top_p1
	then and_p1 (List.map has_type w_types)
	else descr in
(*
	let ext =
	  List.fold_left
	    (fun ext -> function
	      | `Class c -> Ext.union ext ((store#get_class c)#relation#instances ~obs)
	      | _ -> ext)
	    Ext.empty w_types in
	if Ext.is_empty ext
	then `None, descr
	else
*)
      `Extent (w,w_types), descr
    else
      let _ = prerr_endline "window = none" in
      `None, top_p1 in
  object (self)
    inherit concept_answers ~obs store gv lv ext
    inherit concept_relax ~obs store descr

    method window = window

    method total_count rank =
      if rank = 0
      then self#nb_answers
      else
	match self#rank_ext rank with
	| Some e -> Ext.cardinal e
	| None -> Ext.cardinal (store#all_oids ~obs)

    method least_rank : int =
      if self#nb_answers = 0 && self#rank_succ 0 <> None
      then 1
      else 0

    method transformations = ltransf
    method insert_s1 = insert_s1
    method insert_p1 = insert_p1

    method increment_s12 (rank : int) =
      increment_s12 store
    method increment_p12 (rank : int) =
      increment_p12 store

    method completions (rank : int) ?(max_compl = 10) ?(nb_more_relax = 0) key : bool (* partial *) * bool (* relaxed *) * increment list =
      Common.prof "Lisql_concept.concept#completions" (fun () ->
	let matcher =
	  let matches str re =
	    try ignore (Str.search_forward re str 0); true
	    with Not_found -> false in
	  let re_a = Str.regexp "^is a " in
	  let re_fwd = Str.regexp "^has " in
	  let re_bwd = Str.regexp "^is .* of " in
	  let re_key key = Str.regexp_string_case_fold key in
	  fun key0 str -> Common.prof "Lisql_concept.concept#completions/matcher" (fun () ->
	    let len0 = String.length key0 in
	    let keys = Str.split (Str.regexp "[ '\",.;]+") key0 in
	    List.fold_left (* should be at least one significant key *)
	      (fun res key ->
		let len = String.length key in
		if key = "a" && len0 >= 2 && key0.[0] = 'a' && key0.[1] = ' ' then
		  res && matches str re_a
		else if key = "an" && len0 >= 3 && key0.[0] = 'a' && key0.[1] = 'n' && key0.[2] = ' ' then
		  res && matches str re_a
		else if key = "has" && len0 >= 4 && key0.[0] = 'h' && key0.[1] = 'a' && key0.[2] = 's' && key0.[3] = ' ' then
		  res && matches str re_fwd
		else if key = "of" && len0 >= 3 && key0.[len0-3] = ' ' && key0.[len0-2] = 'o' && key0.[len0-1] = 'f' then
		  res && matches str re_bwd
		else if len > 0 then
		  res && matches str (re_key key)
		else res)
	      true keys)
	in
	let rec completions_aux ~obs matching_names rank_opt =
	  let _, _, partial, res =
	    Common.fold_while
	      (fun (ns, i, partial, res) ->
		if partial
		then None
		else
		  match ns with
		  | [] -> None
		  | (sd,s,f)::ns1 ->
		      if i = max_compl then
			Some (ns1, i, true, res)
		      else
			let incr_opt =
			  match rank_opt with
			    | None -> Some (increment f ~supp:(self#nb_answers,self#nb_answers) ~anew:false)
			    | Some rank -> self#get_increment_opt rank f#spec in
			match incr_opt with
			  | Some incr -> Some (ns1, i+1, partial, (sd,s,incr)::res)
			  | None -> Some (ns1, i, partial, res))
	      (matching_names, 0, false, []) in
	  if res <> [] || rank_opt = None then
	    List.length res < List.length matching_names (* partial *), rank_opt <> Some 0, res
	  else
	    let rank_opt' = Option.fold (fun rank -> self#rank_succ rank) None rank_opt in
	    completions_aux ~obs matching_names rank_opt'
	in
	let all_features = store#features ~obs in
	let all_matching_names = Common.prof "Lisql_concept.concept#completions/all_matching_names" (fun () ->
	  Common.mapfilter
	    (fun f ->
	      let sd, s = f#display_string ~obs, "" (* f#string*) in
	      if matcher key sd (*sd ^ s*)
	      then Some (sd,s,f)
	      else None)
	    all_features) in
	let partial, relaxed, matching_names = Common.prof "Lisql_concept.concept#completions/matching_names" (fun () ->
	  if all_matching_names = []
	  then false, false, []
	  else completions_aux ~obs all_matching_names (Some (rank + nb_more_relax))) in
	let sorted_names = Common.prof "Lisql_concept.concept#completions/sorted_names" (fun () ->
	  List.sort
	    (fun (sd1,s1,incr1) (sd2,s2,incr2) ->
	      Pervasives.compare (String.length sd1, sd1) (String.length sd2, sd2))
	    matching_names) in
	let sorted_incrs = List.map (fun (_,_,incr) -> incr) sorted_names in
	partial, relaxed, sorted_incrs)

    method children_increments_no_window rank parent_increment = Common.prof "Lisql_concept.concept_w0#children" (fun () ->
      let open Feature in
      match parent_increment#feature#spec with
      | Spec_Something ->
	  self#update_indexes rank;
	  let total = self#total_count rank in
	  List.fold_left
	    (fun res v ->
	      if gv#generated v
	      then res
	      else increment_ref store v ~supp:(total,total) ~anew:true :: res)
	    [] self#accessible_vars
      | Spec_Name n -> []
      | Spec_Ref v -> []
      | Spec_Some v -> []
      | Spec_Thing ->
	self#update_indexes rank;
	let total = self#total_count rank in
	List.fold_left
	  (fun res uri_c -> prim_increments_from_class store uri_c ~supp:(total,total) ~anew:false @ res)
	  [] root_classes
	@ List.fold_left
	  (fun res uri_c -> proc_increments_from_class store uri_c ~supp:(total,total) ~anew:false @ res)
	  [] root_classes
	@ fold_top_classes ~obs store
	  (fun res -> function
	    | Rdf.URI c ->
	      let count, anew = get_count_anew rank total ((store#get_class c)#relation#cardinal ~obs)in
	      if count = 0
	      then res
	      else increment_type store c ~supp:(count,total) ~anew :: res
	    | _ -> res) []
	@ fold_top_properties ~obs store
	  (fun res -> function
	    | Rdf.URI p ->
	      let sym = store#owl_SymmetricProperty#relation#has_instance ~obs (Rdf.URI p) in
	      let count, anew = get_count_anew rank total ((store#get_property p)#relation#domain_cardinal ~obs) in
	      let res =
		if count = 0
		then res
		else increment_role  store `Subject p ~supp:(count,total) ~anew :: res in
	      if sym
	      then res
	      else
		let count, anew = get_count_anew rank total ((store#get_property p)#relation#range_cardinal ~obs) in
		if count = 0
		then res
		else increment_role store `Object p ~supp:(count,total) ~anew :: res
	    | _ -> res) []
	@ Ext.fold
	  (fun res oid ->
	    let n_funct = store#get_name oid in
	    match n_funct, store#get_arity ~obs n_funct with
	      | Rdf.URI funct, Some arity ->
		let count, anew = get_count_anew rank total ((store#get_functor funct arity)#relation#domain_cardinal ~obs) in
		if count = 0
		then res
		else increment_funct store funct arity Argindex.stat ~supp:(count,total) ~anew :: res
	      | _ -> res)
	  [] ((store#get_class Term.uri_Functor)#relation#instances ~obs)
      | Spec_Argument (pred,arity,i) ->
	( match pred with
	  | Pred.Equal -> []
	  | Pred.Type c ->
	    let total = self#total_count rank in
	    prim_increments_from_class store c ~supp:parent_increment#supp ~anew:false
	    @ (store#rdfs_subClassOf#direct_relation : Store.property_data)#fold_predecessors ~obs
	      (fun res -> function
		| Rdf.URI c' ->
		  let count, anew = get_count_anew rank total ((store#get_class c')#relation#cardinal ~obs) in
		  if count = 0
		  then res
		  else increment_type store c' ~supp:(count,total) ~anew :: res
		| _ -> res)
	      [] (Rdf.URI c)
	  | Pred.Role (p,_) ->
	    let ikind = Argindex.to_kind_role i in
	    let total = self#total_count rank in
	    (store#rdfs_subPropertyOf#direct_relation : Store.property_data)#fold_predecessors ~obs
	      (fun res -> function
		| Rdf.URI p' ->
		  let count, anew = get_count_anew rank total
		    ( match ikind with
		      | `Subject -> (store#get_property p')#relation#domain_cardinal ~obs
		      | `Object -> (store#get_property p')#relation#range_cardinal ~obs ) in
		  if count = 0
		  then res
		  else increment_role store ikind p' ~supp:(count,total) ~anew :: res
		| _ -> res)
	      [] (Rdf.URI p)
	  | Pred.Funct funct -> []
	  | Pred.Prim op -> [] )
      | Spec_Call _ -> [])
      
    method get_increment_opt_no_window rank spec = Common.prof "Lisql_concept.concept_w0#get_increment_opt" (fun () ->
      let open Feature in
	  let total = self#total_count rank in
	  self#update_indexes rank;
	  match spec with
	    | Spec_Something -> None
	    | Spec_Name n -> Some (increment_name store n ~supp:(total,total) ~anew:true)
	    | Spec_Ref v ->
	      if gv#generated v
	      then None
	      else Some (increment_ref store v ~supp:(total,total) ~anew:true)
	    | Spec_Some v -> None
	    | Spec_Thing -> None
	    | Spec_Argument (pred,arity,i) ->
	      ( match pred with
		| Pred.Equal -> assert false (* not a completion *)
		| Pred.Type c ->
		  let count, anew = get_count_anew rank total ((store#get_class c)#relation#cardinal ~obs) in
		  if count = 0
		  then None
		  else Some (increment_type store c ~supp:(count,total) ~anew)
		| Pred.Role (p,_) ->
		  let ikind = Argindex.to_kind_role i in
		  let count, anew = get_count_anew rank total
		    ( match ikind with
		      | `Subject -> (store#get_property p)#relation#domain_cardinal ~obs
		      | `Object -> (store#get_property p)#relation#range_cardinal ~obs ) in
		  if count = 0
		  then None
		  else Some (increment_role store ikind p ~supp:(count,total) ~anew)
		| Pred.Funct funct ->
		  if i <> Argindex.stat
		  then None
		  else
		    let count, anew = get_count_anew rank total ((store#get_functor funct arity)#relation#domain_cardinal ~obs) in
		    if count = 0
		    then None
		    else Some (increment_funct store funct arity i ~supp:(count,total) ~anew)
		| Pred.Prim op ->
		  Some (increment_prim store op arity i ~supp:(total,total) ~anew:false))
	    | Spec_Call (proc,arity,pos) ->
	      Some (increment_proc store proc arity pos ~supp:(total,total) ~anew:false))

    val index_vars : var Index.list = new Index.list
    val index_entities : Extension.oid Index.list = new Index.list
    val index_classes : Index.oid_hierarchy =
      let roots = (* TODO: compute roots once for all: as store method ? *)
	List.fold_left
	  (fun acc uri -> Ext.add (store#get_entity (Rdf.URI uri)) acc)
	  Ext.empty root_classes in
      new Index.oid_hierarchy
	(fun s -> Ext.diff (store#rdfs_subClassOf#direct_relation#objects_oid ~obs s) roots)
    val index_fwd_props : Index.oid_hierarchy =
      let roots = (* TODO: compute roots once for all: as store method ? *)
	List.fold_left
	  (fun acc uri -> Ext.add (store#get_entity (Rdf.URI uri)) acc)
	  Ext.empty root_properties in
      new Index.oid_hierarchy
	(fun s -> Ext.diff (store#rdfs_subPropertyOf#direct_relation#objects_oid ~obs s) roots)
    val index_bwd_props : Index.oid_hierarchy =
      let roots =
	List.fold_left
	  (fun acc uri -> Ext.add (store#get_entity (Rdf.URI uri)) acc)
	  Ext.empty root_properties in
       new Index.oid_hierarchy
	 (fun s -> Ext.diff (store#rdfs_subPropertyOf#direct_relation#objects_oid ~obs s) roots)
    val index_structs : (Extension.oid * int * int) Index.list = new Index.list

    method private update_indexes_aux_window (rank : int) : unit = Common.prof "Lisql_concept.concept_w1#update_indexes" (fun () ->
      let iter_descr descr =
	Ext.iter (index_entities#add ~rank) descr.Descr.entities;
	Ext.iter (index_classes#add ~rank) descr.Descr.classes;
	Ext.iter (index_fwd_props#add ~rank) descr.Descr.fwd_properties;
	Ext.iter (index_bwd_props#add ~rank) descr.Descr.bwd_properties;
	Rel.iter (function [oid; arity; pos] -> index_structs#add ~rank (oid,arity,pos) | _ -> assert false) descr.Descr.structs
      in
      if rank = 0
      then
	match window with
	  | `None -> assert false
	  | `Refs w ->
	    self#fold_answers
	      (fun () m ->
		let w_ext =
		  List.fold_left
		    (fun res v -> try LSet.add (List.assoc v m) res with _ -> res)
		    (LSet.empty ())
		    w in
		assert (not (LSet.is_empty w_ext));
		let descr = Descr.inter_r (List.map (fun oid -> Descr.of_oid store oid) w_ext) in
		iter_descr descr;
		List.iter
		  (fun (v,x) ->
		    if not (gv#generated v) && Ext.mem x descr.Descr.entities
		    then index_vars#add ~rank v)
		  m) ()
	  | `Extent (w,w_types) ->
	    List.iter
	      (fun c ->
		if not (List.mem c root_classes)
		then begin
		  index_classes#add ~rank (store#get_entity (Rdf.URI c));
		  let c_ext = (store#get_class c)#relation#instances ~obs in
		  Ext.iter
		    (fun oid ->
		      let descr = Descr.of_oid store oid in
		      iter_descr descr)
		    c_ext end)
	      w_types;
	    List.iter
	      (fun m_t ->
		List.iter
		  (fun (x,t) ->
		    if not (gv#generated x) && List.for_all (fun v -> try List.assoc v m_t = t with _ -> true) w
		    then index_vars#add ~rank x)
		  m_t)
	      typing
      else
	match self#rank_ext rank with
	| Some ext ->
	    let lv = List.filter (fun v -> not (gv#generated v)) self#accessible_vars in
	    Ext.iter
	      (fun oid ->
		let descr = Descr.of_oid store oid in
		iter_descr descr;
		List.iter (fun v -> index_vars#add ~rank v) lv)
	      ext
	| None -> ())

    method children_increments_window rank parent_increment = Common.prof "Lisql_concept.concept_w1#children" (fun () ->
      let open Feature in
      self#update_indexes rank;
      match parent_increment#feature#spec with
      | Spec_Something ->
	  let total = self#total_count rank in
	  index_vars#fold_children ~rank
	    (fun ~count ~anew v res ->
	      increment_ref store v ~supp:(count,total) ~anew :: res) [] @
	  index_entities#fold_children ~rank
	    (fun ~count ~anew oid res ->
	      increment_name store (store#get_name oid) ~supp:(count,total) ~anew :: res) []
      | Spec_Name n -> []
      | Spec_Ref v -> []
      | Spec_Some v -> []
      | Spec_Thing ->
	let total = self#total_count rank in
	List.fold_left
	  (fun res uri_c -> prim_increments_from_class store uri_c ~supp:(total,total) ~anew:false @ res)
	  [] root_classes @
	  List.fold_left
	  (fun res uri_c -> proc_increments_from_class store uri_c ~supp:(total,total) ~anew:false @ res)
	  [] root_classes @
	  index_classes#fold_children ~rank
	    (fun ~count ~anew c res ->
	      match store#get_name c with
	      | Rdf.URI uri_c -> increment_type store uri_c ~supp:(count,total) ~anew :: res
	      | _ -> res) [] @
	  index_fwd_props#fold_children ~rank
	    (fun ~count ~anew p res ->
	      match store#get_name p with
	      | Rdf.URI uri_p -> increment_role store `Subject uri_p ~supp:(count,total) ~anew :: res
	      | _ -> res) [] @
	  index_bwd_props#fold_children ~rank
	    (fun ~count ~anew p res ->
	      match store#get_name p with
	      | Rdf.URI uri_p -> increment_role store `Object uri_p ~supp:(count,total) ~anew :: res
	      | _ -> res) [] @
	  index_structs#fold_children ~rank
	    (fun ~count ~anew (f,arity,pos) res ->
	      match store#get_name f with
	      | Rdf.URI funct -> increment_funct store funct arity pos ~supp:(count,total) ~anew :: res
	      | _ -> res) []
      | Spec_Argument (pred,arity,pos) ->
	( match pred with
	  | Pred.Equal -> []
	  | Pred.Type c ->
	    let total = self#total_count rank in
	    prim_increments_from_class store c ~supp:parent_increment#supp ~anew:false
	    @ proc_increments_from_class store c ~supp:parent_increment#supp ~anew:false
	    @ index_classes#fold_children ~parent:(store#get_entity (Rdf.URI c)) ~rank
	      (fun ~count ~anew oid res ->
		match store#get_name oid with
		  | Rdf.URI c' ->
		    increment_type store c' ~supp:(count,total) ~anew :: res
		  | _ -> res) []
	  | Pred.Role (p,_) ->
	    let ikind = Argindex.to_kind_role pos in
	    let total = self#total_count rank in
	    let index = match ikind with `Subject -> index_fwd_props | `Object -> index_bwd_props in
	    index#fold_children ~parent:(store#get_entity (Rdf.URI p)) ~rank
	      (fun ~count ~anew oid res ->
		match store#get_name oid with
		  | Rdf.URI p' -> increment_role store ikind p' ~supp:(count,total) ~anew :: res
		  | _ -> res) []
	  | Pred.Funct funct -> []
	  | Pred.Prim op -> [] )
      | Spec_Call _ -> [])

    method get_increment_opt_window rank spec = Common.prof "Lisql_concept.concept_w1#get_increment_opt" (fun () ->
      let open Feature in
	  let total = self#total_count rank in
	  self#update_indexes rank;
	  match spec with
	    | Spec_Something -> None
	    | Spec_Name n ->
	      let count, anew = index_entities#get ~rank (store#get_entity n) in
	      if count = 0
	      then None
	      else Some (increment_name store n ~supp:(count,total) ~anew)
	    | Spec_Ref v ->
	      let count, anew = index_vars#get ~rank v in
	      if count = 0
	      then None
	      else Some (increment_ref store v ~supp:(count,total) ~anew)
	    | Spec_Some v -> None
	    | Spec_Thing -> None
	    | Spec_Argument (pred,arity,pos) ->
	      ( match pred with
		| Pred.Equal -> assert false (* not a completion *)
		| Pred.Type c ->
		  let count, anew = index_classes#get ~rank (store#get_entity (Rdf.URI c)) in
		  if count = 0
		  then None
		  else Some (increment_type store c ~supp:(count,total) ~anew)
		| Pred.Role (p,_) ->
		  let ikind = Argindex.to_kind_role pos in
		  let count, anew =
		    match ikind with
		      | `Subject -> index_fwd_props#get ~rank (store#get_entity (Rdf.URI p))
		      | `Object -> index_bwd_props#get ~rank (store#get_entity (Rdf.URI p)) in
		  if count = 0
		  then None
		  else Some (increment_role store ikind p ~supp:(count,total) ~anew)
		| Pred.Funct funct ->
		  let count, anew = index_structs#get ~rank (store#get_entity (Rdf.URI funct), arity, pos) in
		  if count = 0
		  then None
		  else Some (increment_funct store funct arity pos ~supp:(count,total) ~anew)
		| Pred.Prim op ->
		  let lc = (store#get_primitive op arity)#types_at_index pos in
		  let count, anew =
		    List.fold_left
		      (fun (res_count,res_anew) c ->
			let count, anew = index_classes#get ~rank (store#get_entity (Rdf.URI c)) in
			(res_count + count, res_anew || anew))
		      (0,false)
		      lc in
		  if count = 0
		  then None
		  else Some (increment_prim store op arity pos ~supp:(count,total) ~anew))
	    | Spec_Call (proc,arity,pos) ->
	      let lc =
		match proc with
		  | Proc.RemoveName
		  | Proc.CopyName
		  | Proc.MoveName -> [Rdfs.uri_Resource]
		  | Proc.Prim op -> (store#get_procedure op arity)#types_at_index pos in
	      let count, anew =
		List.fold_left
		  (fun (res_count,res_anew) c ->
		    let count, anew = index_classes#get ~rank (store#get_entity (Rdf.URI c)) in
		    (res_count + count, res_anew || anew))
		  (0,false)
		  lc in
	      if count = 0
	      then None
	      else Some (increment_proc store proc arity pos ~supp:(count,total) ~anew))

    val mutable updated_index_ranks = Ext.empty
    method private update_indexes : int -> unit =
      fun rank -> (* to be defined by each kind of concepts *)
	if not (Ext.mem rank updated_index_ranks)
	then begin
	  ( match window with
	    | `None -> ()
	    | _ -> self#update_indexes_aux_window rank );
	  updated_index_ranks <- Ext.add rank updated_index_ranks
	end

    method children_increments (rank : int) (parent_increment : increment) : increment list =
      match window with
	| `None -> self#children_increments_no_window rank parent_increment
	| _ -> self#children_increments_window rank parent_increment

    method get_increment_opt (rank : int) (spec : Feature.spec) : increment option =
      match window with
	| `None -> self#get_increment_opt_no_window rank spec
	| _ -> self#get_increment_opt_window rank spec

  end


let rec of_focus ~obs store (foc : focus) : concept = Common.prof "Lisql_concept.of_focus" (fun () ->
  let open Transf in
  let ltransf_base on_top =
    if on_top
    then [ToggleNot; ToggleMaybe; Select; Delete]
    else [InsertAnd; InsertOr; InsertAndNot; ToggleNot; ToggleMaybe; Select; Delete] in
  match foc with
  | AtC (c,k) ->
      let ltransf =
	if c = top_c
	then [Select; Delete]
	else [InsertForEach; InsertCond; InsertSeq; Select; Delete] in
      let c = command_of_focus foc in
      new concept ~obs store c Thing ~ltransf ~insert_s1:false ~insert_p1:false
  | AtS (s0,k) ->
      let ltransf = ltransf_base (s0 = top_s) in
      let ltransf =
	match k with
	  | Assert0 _ -> InsertCond :: ltransf
	  | _ -> ltransf in
      let c = command_of_focus foc in
      new concept ~obs store c Thing ~ltransf ~insert_s1:true ~insert_p1:true
  | AtS1 (Det (det,f), k) ->
      let ltransf = ltransf_base (det = top_s2 && f = top_p1) in
      let ltransf =
	match k with
	  | Get0 _ -> InsertForEach :: InsertIsThere :: ltransf
	  | _ -> ltransf in
      let ltransf =
	match det with
	| Name _ -> Describe :: ltransf
	| Qu (qu,_) ->
	  let lqu =
	    match k with
	      | CallN _ | For1 _ -> [An; Each]
	      | _ -> [An; Every; Only; No] in
	  List.fold_left
	    (fun res q -> if q = qu then res else ToggleQu q :: res)
	    ltransf lqu
	| _ -> ltransf in
      let k' = Det1 (det,f,k) in
      let foc' = AtP1 (f,k') in
      let foc_which =
	match Lisql_transf.focus_and_p1_atom eq_which foc' with
	  | Some foc -> foc
	  | None -> assert false in
      let c_which = command_of_focus foc_which in
      let descr = descr_s1_atom_focus f k (*descr_p1 f k'*) in
      new concept ~obs store c_which descr ~ltransf ~insert_s1:true ~insert_p1:true
  | AtS1 (np,k) ->
      let ltransf = ltransf_base (np = top_s1) in
      let ltransf =
	match k with
	  | Get0 _ -> InsertForEach :: InsertIsThere :: ltransf
	  | _ -> ltransf in
      let foc_which =
	match Lisql_transf.focus_and_s1_atom which_s1 foc with
	  | Some foc -> foc
	  | None -> assert false in
      let c_which = command_of_focus foc_which in
      new concept ~obs store c_which Thing ~ltransf ~insert_s1:false ~insert_p1:false
  | AtS2 (det,k) ->
      let ltransf = if det = top_s2 then [] else [Delete] in
      let c = command_of_focus foc in
      new concept ~obs store c Thing ~ltransf ~insert_s1:false ~insert_p1:false
  | AtP1 (f,k) ->
      let ltransf = ltransf_base (f = top_p1) in
      let ltransf =
	match f with
	| Arg (Pred.Role _,_,_) -> ToggleOpt :: ToggleTrans :: ToggleSym :: ltransf (* TODO: be more precise, in particular for operators *)
	| _ -> ltransf in
      let foc_which =
	match Lisql_transf.focus_and_p1_atom eq_which foc with
	  | Some foc -> foc
	  | None -> assert false in
      let c_which = command_of_focus foc_which in
      let descr = descr_p1_focus f k (*descr_p1 f k*) in
      new concept ~obs store c_which descr ~ltransf ~insert_s1:true ~insert_p1:true)
