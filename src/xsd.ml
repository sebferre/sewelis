(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* XSD vocabulary *)

(* namespace *)
let prefix = "xsd:"
let namespace = "http://www.w3.org/2001/XMLSchema#"
(* classes *)
let _anyURI = "xsd:anyURI"
let _language = "xsd:language"
let _string = "xsd:string"
let _boolean = "xsd:boolean"
let _integer = "xsd:integer"
let _double = "xsd:double"
let _decimal = "xsd:decimal"
let _duration = "xsd:duration"
let _dateTime = "xsd:dateTime"
let _time = "xsd:time"
let _date = "xsd:date"
let _gYearMonth = "xsd:gYearMonth"
let _gYear = "xsd:gYear"
let _gMonthDay = "xsd:gMonthDay"
let _gDay = "xsd:gDay"
let _gMonth = "xsd:gMonth"

let make = Uri.make namespace prefix

let uri_anyURI = make _anyURI
let uri_language = make _language
let uri_string = make _string
let uri_boolean = make _boolean
let uri_integer = make _integer
let uri_double = make _double
let uri_decimal = make _decimal
let uri_duration = make _duration
let uri_dateTime = make _dateTime
let uri_time = make _time
let uri_date = make _date
let uri_gYearMonth = make _gYearMonth
let uri_gYear = make _gYear
let uri_gMonthDay = make _gMonthDay
let uri_gDay = make _gDay
let uri_gMonth = make _gMonth
let uri_location = namespace ^ "location" (* datatype: "<lat> <lon> <alt>?" *)
