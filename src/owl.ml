(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* OWL 2 vocabulary *)

(* namespace *)
let prefix = "owl:"
let namespace = "http://www.w3.org/2002/07/owl#" (* ??? *)

(* classes *)
let _AllDifferent = "owl:AllDifferent"
let _AnnotationProperty = "owl:AnnotationProperty"
let _AssymetricProperty = "owl:AssymetricProperty"
let _Class = "owl:Class"
let _DataRange = "owl:DataRange"
let _DatatypeProperty = "owl:DatatypeProperty"
let _DeprecatedClass = "owl:DeprecatedClass"
let _DeprecatedProperty = "owl:DeprecatedProperty"
let _FunctionalProperty = "owl:FunctionalProperty"
let _InverseFunctionalProperty = "owl:InverseFunctionalProperty"
let _IrreflexiveProperty = "owl:IrreflexiveProperty"
let _Nothing = "owl:Nothing"
let _ObjectProperty = "owl:ObjectProperty"
let _Ontology = "owl:Ontology"
let _OntologyProperty = "owl:OntologyProperty"
let _ReflexiveProperty = "owl:ReflexiveProperty"
let _Restriction = "owl:Restriction"
let _SymmetricProperty = "owl:SymmetricProperty"
let _Thing = "owl:Thing"
let _TransitiveProperty = "owl:TransitiveProperty"

(* properties *)
let _allValuesFrom = "owl:allValuesFrom"
let _backwardCompatibleWith = "owl:backwardCompatibleWith"
let _bottomObjectProperty = "owl:bottomObjectProperty"
let _bottomDataProperty = "owl:bottomDataProperty"
let _cardinality = "owl:cardinality"
let _complementOf = "owl:complementOf"
let _differentFrom = "owl:differentFrom"
let _disjointWith = "owl:disjointWith"
let _distinctMembers = "owl:distinctMembers"
let _equivalentClass = "owl:equivalentClass"
let _equivalentProperty = "owl:equivalentProperty"
let _hasValue = "owl:hasValue"
let _imports = "owl:imports"
let _incompatibleWith = "owl:incompatibleWith"
let _intersectionOf = "owl:intersectionOf"
let _inverseOf = "owl:inverseOf"
let _maxCardinality = "owl:maxCardinality"
let _minCardinality = "owl:minCardinality"
let _oneOf = "owl:oneOf"
let _onProperty = "owl:onProperty"
let _priorVersion = "owl:priorVersion"
let _sameAs = "owl:sameAs"
let _someValuesFrom = "owl:someValuesFrom"
let _topObjectProperty = "owl:topObjectProperty"
let _topDataProperty = "owl:topDataProperty"
let _unionOf = "owl:unionOf"
let _versionInfo = "owl:versionInfo"

let make = Uri.make namespace prefix

let uri_AllDifferent = make _AllDifferent
let uri_AnnotationProperty = make _AnnotationProperty
let uri_AssymetricProperty = make _AssymetricProperty
let uri_Class = make _Class
let uri_DataRange = make _DataRange
let uri_DatatypeProperty = make _DatatypeProperty
let uri_DeprecatedClass = make _DeprecatedClass
let uri_DeprecatedProperty = make _DeprecatedProperty
let uri_FunctionalProperty = make _FunctionalProperty
let uri_InverseFunctionalProperty = make _InverseFunctionalProperty
let uri_IrreflexiveProperty = make _IrreflexiveProperty
let uri_Nothing = make _Nothing
let uri_ObjectProperty = make _ObjectProperty
let uri_Ontology = make _Ontology
let uri_OntologyProperty = make _OntologyProperty
let uri_ReflexiveProperty = make _ReflexiveProperty
let uri_Restriction = make _Restriction
let uri_SymmetricProperty = make _SymmetricProperty
let uri_Thing = make _Thing
let uri_TransitiveProperty = make _TransitiveProperty
let uri_allValuesFrom = make _allValuesFrom
let uri_backwardCompatibleWith = make _backwardCompatibleWith
let uri_bottomObjectProperty = make _bottomObjectProperty
let uri_bottomDataProperty = make _bottomDataProperty
let uri_cardinality = make _cardinality
let uri_complementOf = make _complementOf
let uri_differentFrom = make _differentFrom
let uri_disjointWith = make _disjointWith
let uri_distinctMembers = make _distinctMembers
let uri_equivalentClass = make _equivalentClass
let uri_equivalentProperty = make _equivalentProperty
let uri_hasValue = make _hasValue
let uri_imports = make _imports
let uri_incompatibleWith = make _incompatibleWith
let uri_intersectionOf = make _intersectionOf
let uri_inverseOf = make _inverseOf
let uri_maxCardinality = make _maxCardinality
let uri_minCardinality = make _minCardinality
let uri_oneOf = make _oneOf
let uri_onProperty = make _onProperty
let uri_priorVersion = make _priorVersion
let uri_sameAs = make _sameAs
let uri_someValuesFrom = make _someValuesFrom
let uri_topObjectProperty = make _topObjectProperty
let uri_topDataProperty = make _topDataProperty
let uri_unionOf = make _unionOf
let uri_versionInfo = make _versionInfo


type ontology = Uri.t

and class_ =
  | Atom of Uri.t
  | Datatype of Rdf.datatype
  | Literal
  | Thing
  | Nothing
  | OneOf of Rdf.tree list
  | Restriction of Rdf.property * constr
  | IntersectionOf of class_ list
  | UnionOf of class_ list
  | ComplementOf of class_

and constr =
  | AllValuesFrom of class_
  | SomeValuesFrom of class_
  | HasValue of Rdf.tree
  | MaxCardinality of int
  | MinCardinality of int
  | Cardinality of int

and axiom =
  | Ontology of ontology * ontology_axiom list
  | Class of class_ * class_axiom list
  | ObjectProperty of Rdf.property * property_axiom list
  | DatatypeProperty of Rdf.property * property_axiom list
  | AnnotationProperty of Rdf.property
  | OntologyProperty of Rdf.property
  | Individual of Rdf.tree * individual_axiom list
  | AllDifferent of Rdf.tree list

and ontology_axiom =
  | Imports of ontology
  | PriorVersion of ontology
  | BackwardCompatibleWith of ontology
  | IncompatibleWith of ontology

and class_axiom =
  | SubClassOf of class_
  | EquivalentClass of class_
  | DisjointWith of class_
  | DeprecatedClass

and property_axiom =
  | Domain of class_
  | Range of class_
  | SubPropertyOf of Rdf.property
  | EquivalentProperty of Rdf.property
  | InverseOf of Rdf.property (* only for ObjectProperty *)
  | FunctionalProperty
  | InverseFunctionalProperty (* only for ObjectProperty *)
  | TransitiveProperty (* only for ObjectProperty *)
  | SymmetricProperty (* only for ObjectProperty *)
  | DeprecatedProperty

and individual_axiom =
  | SameAs of Uri.t
  | DifferentFrom of Uri.t
