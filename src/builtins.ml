(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Name = Name.Make

type typing = Uri.t list

(* monadic operations for undeterministic evaluation *)
type 'a monad = 'a list
let return x = [x]
let bind m k = List.concat (List.map k m)
let fmap m f = List.map f m
let alt lm = List.concat lm
let fail = []

class virtual pred =
object (self)
  method virtual arity : int
  method virtual types : typing list
  method virtual eval : Name.t list -> bool
end

class preds_union (lp : pred list) =
  let arity = try (List.hd lp)#arity with _ -> invalid_arg "Builtins.preds_union: empty list" in
  let types = List.concat (List.map (fun p -> p#types) lp) in
object
  method arity = arity
  method types = types
  method eval args = List.exists (fun p -> p#eval args) lp
end

let preds_union lp = new preds_union lp

class pred1 
  (types : typing list)
  (conv1 : Name.t -> 'a option) (p1 : 'a -> bool) =
  let _ = assert (List.for_all (fun t -> List.length t = 1) types) in
object
  inherit pred
  method arity = 1
  method types = types
  method eval =
    function
      | [x1] ->
	( match conv1 x1 with
	  | Some v1 -> p1 v1
	  | None -> false )
      | _ -> false
end

class pred2
  (types : typing list)
  (conv1 : Name.t -> 'a option) (conv2 : Name.t -> 'b option) (p2 : 'a -> 'b -> bool) =
object
  inherit pred
  method arity = 2
  method types = types
  method eval =
    function
      | [x1; x2] ->
	( match conv1 x1, conv2 x2 with
	  | Some v1, Some v2 -> p2 v1 v2
	  | _ -> false )
      | _ -> false
end

class pred3
  (types : typing list)
  (conv1 : Name.t -> 'a option) (conv2 : Name.t -> 'b option) (conv3 : Name.t -> 'c option) (p3 : 'a -> 'b -> 'c -> bool) =
object
  inherit pred
  method arity = 3
  method types = types
  method eval =
    function
      | [x1; x2; x3] ->
	( match conv1 x1, conv2 x2, conv3 x3 with
	  | Some v1, Some v2, Some v3 -> p3 v1 v2 v3
	  | _ -> false )
      | _ -> false
end

class virtual ['a] comp =
object (self)
  method virtual arity : int
  method virtual types : typing list
  method virtual eval : Name.t list -> 'a monad
end

class ['a] comps_union (lc : 'a comp list) =
  let arity = try (List.hd lc)#arity with _ -> invalid_arg "Builtins.comps_union: empty list" in
  let types = List.concat (List.map (fun f -> f#types) lc) in
object
  inherit ['a] comp
  method arity = arity
  method types = types
  method eval args = alt (List.map (fun c -> c#eval args) lc)
end

let comps_union lc = new comps_union lc

class ['c] comp2
  (types : typing list)
  (conv1 : Name.t -> 'a option) (conv2 : Name.t -> 'b option) (c2 : 'a -> 'b -> 'c monad) =
object
  inherit ['c] comp
  method arity = 2
  method types = types
  method eval =
    function
      | [x1; x2] ->
	( match conv1 x1, conv2 x2 with
	  | Some v1, Some v2 -> c2 v1 v2
	  | _ -> fail )
      | _ -> fail
end


class virtual func =
object (self)
  method virtual arity : int
  method virtual types : typing list (* BEWARE: result must come first ! *)
  method virtual eval : Name.t list -> Name.t monad
end

class funcs_union (lf : func list) =
  let arity = try (List.hd lf)#arity with _ -> invalid_arg "Builtins.funcs_union: empty list" in
  let types = List.concat (List.map (fun f -> f#types) lf) in
object
  inherit func
  method arity = arity
  method types = types
  method eval args = alt (List.map (fun f -> f#eval args) lf)
end

let funcs_union lf = new funcs_union lf

class func0
  (types : typing list)
  (f0 : unit -> 'a monad) (conv : 'a -> Name.t) =
object
  inherit func
  method arity = 0
  method types = types
  method eval =
    function
      | [] -> fmap (f0 ()) conv
      | _ -> fail
end

class func1
  (types : typing list)
  (conv1 : Name.t -> 'a option) (f1 : 'a -> 'b monad) (conv : 'b -> Name.t) =
object
  inherit func
  method arity = 1
  method types = types
  method eval =
    function
      | [x1] ->
	( match conv1 x1 with
	  | Some v1 -> fmap (f1 v1) conv
	  | _ -> fail )
      | _ -> fail
end

class func2
  (types : typing list)
  (conv1 : Name.t -> 'a option) (conv2 : Name.t -> 'b option) (f2 : 'a -> 'b -> 'c monad) (conv : 'c -> Name.t) =
object
  inherit func
  method arity = 2
  method types = types
  method eval =
    function
      | [x1; x2] ->
	( match conv1 x1, conv2 x2 with
	  | Some v1, Some v2 -> fmap (f2 v1 v2) conv
	  | _ -> fail )
      | _ -> fail
end

class func3
  (types : typing list)
  (conv1 : Name.t -> 'a option) (conv2 : Name.t -> 'b option) (conv3 : Name.t -> 'c option) (f3 : 'a -> 'b -> 'c -> 'd monad) (conv : 'd -> Name.t) =
object
  inherit func
  method arity = 3
  method types = types
  method eval =
    function
      | [x1; x2; x3] ->
	( match conv1 x1, conv2 x2, conv3 x3 with
	  | Some v1, Some v2, Some v3 -> fmap (f3 v1 v2 v3) conv
	  | _ -> fail )
      | _ -> fail
end


class virtual proc =
object (self)
  method virtual arity : int
  method virtual types : typing list
  method virtual run : Name.t list -> unit
end

class proc1
  (types : typing list)
  (conv1 : Name.t -> 'a option) (p1 : 'a -> unit) =
object
  inherit proc
  method arity = 1
  method types = types
  method run =
    function
      | [x1] ->
	( match conv1 x1 with
	  | Some v1 -> p1 v1
	  | _ -> prerr_endline "Builtins.proc1#run: invalid argument" )
      | _ -> prerr_endline "Builtins.proc1#run: invalid number of arguments"
end

class procN
  (types : typing list)
  (convs : (Name.t -> 'a option) list) (p : 'a list -> unit) =
object
  inherit proc
  val arity = List.length convs
  method arity = arity
  method types = types
  method run =
    function
      | lx when List.length lx = arity ->
	let vs_opt =
	  List.fold_right2
	    (fun conv x res ->
	      let v_opt = conv x in
	      match v_opt, res with
		| Some v, Some l -> Some (v::l)
		| _ -> None)
	    convs lx (Some []) in
	( match vs_opt with
	  | Some vs -> p vs
	  | None -> prerr_endline "Builtins.procN#run: invalid argument" )
      | _ -> prerr_endline "Builtins.procN#run: invalid number of arguments"
end
