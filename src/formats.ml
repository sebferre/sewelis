(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* Unique URIs for File Formats *)

(* namespace *)
let prefix = "format:"
let namespace = "http://www.w3.org/ns/formats/"
(* classes *)
let _Format = "format:Format"
(* properties *)
let _media_type = "format:media_type"
let _preferred_suffix = "format:preferred_suffix"
(* individuals *)
let _N3 = "format:N3"
let _NTriples = "format:N-Triples"
let _OWL_XML = "format:OWL_XML"
let _OWL_Functional = "format:OWL_Functional"
let _OWL_Manchester = "format:OWL_Manchester"
let _POWDER = "format:POWDER"
let _POWDERS = "format:POWDER-S"
let _RDFa = "format:RDFa"
let _RDF_XML = "format:RDF_XML"
let _RIF_XML = "format:RIF_XML"
let _SPARQL_Results_XML = "format:SPARQL_Results_XML"
let _SPARQL_Results_JSON = "format:SPARQL_Results_JSON"
let _Turtle = "format:Turtle"

let make = Uri.make namespace prefix

let uri_Format = make _Format
let uri_media_type = make _media_type
let uri_preferred_suffix = make _preferred_suffix
let uri_N3 = make _N3
let uri_NTriples = make _NTriples
let uri_OWL_XML = make _OWL_XML
let uri_OWL_Functional = make _OWL_Functional
let uri_OWL_Manchester = make _OWL_Manchester
let uri_POWDER = make _POWDER
let uri_POWDERS = make _POWDERS
let uri_RDFa = make _RDFa
let uri_RDF_XML = make _RDF_XML
let uri_RIF_XML = make _RIF_XML
let uri_SPARQL_Results_XML = make _SPARQL_Results_XML
let uri_SPARQL_Results_JSON = make _SPARQL_Results_JSON
let uri_Turtle = make _Turtle
