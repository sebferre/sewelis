(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Builtins

module RdfTerm =
  struct
    type t = Rdf.thing
    let from_name n = Some n
    let to_name v = v
  end

module TypedLiteral =
  struct
    type t = string * Uri.t
    let from_name = function
      | Rdf.Literal (s, Rdf.Plain _) -> Some (s, Xsd.uri_string)
      | Rdf.Literal (s, Rdf.Typed dt) -> Some (s,dt)
      | _ -> None
    let to_name (s,dt) = Rdf.Literal (s, Rdf.Typed dt)
  end

module LangString =
  struct
    type t = string * string
    let from_name = function
      | Rdf.Literal (s, Rdf.Plain lang) -> Some (s,lang)
      | _ -> None
    let to_name (s,lang) = Rdf.Literal (s, Rdf.Plain lang)
  end

module AnyURI =
  struct
    type t = Uri.t
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_anyURI -> Some s
      | _ -> None
    let to_name v = Rdf.Literal (v, Rdf.Typed Xsd.uri_anyURI)
  end

module String =
  struct
    include String (* extending standard module String *)

    let from_name = function
      | Rdf.Literal (s, Rdf.Plain _) -> Some s
      | Rdf.Literal (s, Rdf.Typed dt) when List.mem dt [Xsd.uri_string] -> Some s
      | _ -> None
    let to_name v = Rdf.Literal (v, Rdf.Typed Xsd.uri_string)
    let compare = Pervasives.compare
  end

module Bool =
  struct
    type t = bool
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_boolean ->
	( match s with
	  | "true" | "1" -> Some true
	  | "false" | "0" -> Some false
	  | _ -> None )
      | _ -> None
    let to_name v = Rdf.Literal ((if v then "true" else "false"), Rdf.Typed Xsd.uri_boolean)
    let compare = Pervasives.compare
  end

module Int =
  struct
    let types = [Xsd.uri_integer]

    type t = int
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when List.mem dt [Xsd.uri_integer] ->
	(try Some (int_of_string s) with _ -> None)
      | _ -> None
    let to_name v = Rdf.Literal (string_of_int v, Rdf.Typed Xsd.uri_integer)
    let compare = Pervasives.compare
  end

module Float =
  struct
    let types = [Xsd.uri_decimal; Xsd.uri_double]

    type t = float
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when List.mem dt [Xsd.uri_double; Xsd.uri_decimal] ->
	(try Some (float_of_string s) with _ -> None)
      | _ -> None
    let to_name v = Rdf.Literal (string_of_float v, Rdf.Typed Xsd.uri_double)
    let compare = Pervasives.compare
  end

module Numeric =
  struct
    let types = Int.types @ Float.types

    type t = Int of Int.t | Float of Float.t
    let from_name x =
      match Int.from_name x with
	| Some v -> Some (Int v)
	| None ->
	  match Float.from_name x with
	    | Some v -> Some (Float v)
	    | None -> None
    let to_name = function
      | Int v -> Int.to_name v
      | Float v -> Float.to_name v
    let to_float = function
      | Int i -> float i
      | Float f -> f
    let compare v1 v2 =
      match v1, v2 with
	| Int i1, Int i2 -> Int.compare i1 i2
	| Float f1, Float f2 -> Float.compare f1 f2
	| Int i1, Float f2 -> Float.compare (float i1) f2
	| Float f1, Int i2 -> Float.compare f1 (float i2)

    let func1 (fi : int -> int) (ff : float -> float) (v1 : t) : t monad =
      try
	return
	  ( match v1 with
	    | Int i1 -> Int (fi i1)
	    | Float f1 -> Float (ff f1) )
      with _ -> fail
    let func2 (fi : int -> int -> int) (ff : float -> float -> float) (v1 : t) (v2 : t) : t monad =
      try
	return
	  ( match v1, v2 with
	    | Int i1, Int i2 -> Int (fi i1 i2)
	    | Float f1, Float f2 -> Float (ff f1 f2)
	    | Int i1, Float f2 -> Float (ff (float i1) f2)
	    | Float f1, Int i2 -> Float (ff f1 (float i2)) )
      with _ -> fail (* to catch divide by zero, etc. *)
    let func1_float (f : float -> float) (v1 : t) : Float.t monad =
      try return (f (to_float v1))
      with _ -> fail
  end

(* SPARQL builtins *)

exception TODO

(* RDF Terms *)

let rdfterm_pred1_types = [ [Rdfs.uri_Resource] ]

let isURI (x : RdfTerm.t) : bool =
  match x with
    | Rdf.URI _ -> true
    | _ -> false
let isURI_pred = new pred1
  rdfterm_pred1_types
  RdfTerm.from_name isURI

let isBLANK (x : RdfTerm.t) : bool =
  match x with
    | Rdf.Blank _ -> true
    | _ -> false
let isBLANK_pred = new pred1
  rdfterm_pred1_types
  RdfTerm.from_name isBLANK

let isLiteral (x : RdfTerm.t) : bool =
  match x with
    | Rdf.Literal _ -> true
    | _ -> false
let isLiteral_pred = new pred1
  [ [Rdfs.uri_Literal] ]
  RdfTerm.from_name isLiteral

let isNumeric (x : RdfTerm.t) : bool =
  match x with
    | Rdf.Literal (_, Rdf.Typed dt) ->
      List.mem dt [Xsd.uri_integer; Xsd.uri_double; Xsd.uri_decimal]
    | _ -> false
let isNumeric_pred = new pred1
  [ [Xsd.uri_integer]; [Xsd.uri_decimal]; [Xsd.uri_double] ]
  RdfTerm.from_name isNumeric

let lang (x : LangString.t) : TypedLiteral.t monad =
  match x with
    | (_, lang) -> return (lang, Xsd.uri_language)
    | _ -> fail
let lang_func = new func1
  [ [Xsd.uri_language; Rdf.uri_langString] ]
  LangString.from_name lang TypedLiteral.to_name

let datatype (x : TypedLiteral.t) : RdfTerm.t list =
  match x with
    | (_, dt) -> return (Rdf.URI dt)
    | _ -> fail
let datatype_func = new func1
  [ [Rdfs.uri_Datatype; Rdfs.uri_Literal] ]
  TypedLiteral.from_name datatype RdfTerm.to_name

let str (x : RdfTerm.t) : String.t list =
  match x with
    | Rdf.Literal (s, _) -> return s
    | Rdf.URI uri -> return uri
    | _ -> fail
let str_func = new func1
  [ [Xsd.uri_string; Rdfs.uri_Resource] ]
  RdfTerm.from_name str String.to_name

(* conversions *)

let toNumeric (x : String.t) : Numeric.t list =
  try return (Numeric.Int (int_of_string x)) with _ ->
    try return (Numeric.Float (float_of_string x)) with _ ->
      fail
let toNumeric_func = new func1
  [ [Xsd.uri_integer; Xsd.uri_string];
    [Xsd.uri_double; Xsd.uri_string] ]
  String.from_name toNumeric Numeric.to_name

(* strings *)

let string_comp = new comp2
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name String.from_name (fun s1 s2 -> [String.compare s1 s2])

let strStarts (x : String.t) (y : String.t) : bool =
  Str.string_match (Str.regexp_string y) x 0
let strStarts_pred = new pred2
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name String.from_name strStarts

let strEnds (x : String.t) (y : String.t) : bool =
  try ignore (Str.search_forward (Str.regexp (Str.quote y ^ "$")) x 0); true with _ -> false
let strEnds_pred = new pred2
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name String.from_name strEnds

let contains (x : String.t) (y : String.t) : bool =
  try ignore (Str.search_forward (Str.regexp_string y) x 0); true with _ -> false
let contains_pred = new pred2
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name String.from_name contains

let regex (x : String.t) (y : String.t) : bool =
  try ignore (Str.search_forward (Str.regexp y) x 0); true with _ -> false
let regex_pred = new pred2
  [ [Xsd.uri_string; Xsd.uri_string] ] 
  String.from_name String.from_name regex

let strlen (x : String.t) : Int.t monad =
  return (String.length x)
let strlen_func = new func1
  [ [Xsd.uri_integer; Xsd.uri_string] ]
  String.from_name strlen Int.to_name

let substr (s : String.t) (pos : Int.t) (len : Int.t) : String.t monad =
  try return (String.sub s pos len)
  with _ -> fail
let substr_func = new func3
  [ [Xsd.uri_string; Xsd.uri_string; Xsd.uri_integer; Xsd.uri_integer] ]
  String.from_name Int.from_name Int.from_name substr String.to_name

let ucase (s : String.t) : String.t monad =
  return (String.uppercase s)
let ucase_func = new func1
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name ucase String.to_name

let lcase (s : String.t) : String.t monad =
  return (String.lowercase s)
let lcase_func = new func1
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name lcase String.to_name

let capitalize (s : String.t) : String.t monad =
  return (String.capitalize s)
let capitalize_func = new func1
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name capitalize String.to_name

let uncapitalize (s : String.t) : String.t monad =
  return (String.uncapitalize s)
let uncapitalize_func = new func1
  [ [Xsd.uri_string; Xsd.uri_string] ]
  String.from_name uncapitalize String.to_name

let concat (s1 : String.t) (s2 : String.t) : String.t monad =
  return (s1 ^ s2)
let concat_func = new func2
  [ [Xsd.uri_string; Xsd.uri_string; Xsd.uri_string] ] 
  String.from_name String.from_name concat String.to_name

let split (s : String.t) (sep : String.t) : String.t monad =
  Str.split (Str.regexp sep) s
let split_func = new func2
  [ [Xsd.uri_string; Xsd.uri_string; Xsd.uri_string] ]
  String.from_name String.from_name split String.to_name

(* arithmetic *)

let numeric_comp = new comp2
  (List.fold_left
     (fun res t1 ->
       List.fold_left
	 (fun res t2 ->
	   [t1;t2]::res)
	 res Numeric.types)
     [] Numeric.types)
  Numeric.from_name Numeric.from_name (fun x1 x2 -> return (Numeric.compare x1 x2))

let numeric_func1_types = [ [Xsd.uri_integer; Xsd.uri_integer];
			    [Xsd.uri_double; Xsd.uri_double] ]
let numeric_func1 f1 = new func1
  numeric_func1_types
  Numeric.from_name f1 Numeric.to_name

let numeric_func1_float f1 = new func1
  [ [Xsd.uri_double; Xsd.uri_integer];
    [Xsd.uri_double; Xsd.uri_double] ]
  Numeric.from_name f1 Float.to_name

let numeric_func2_types = [ [Xsd.uri_integer; Xsd.uri_integer; Xsd.uri_integer];
			    [Xsd.uri_double; Xsd.uri_integer; Xsd.uri_double];
			    [Xsd.uri_double; Xsd.uri_double; Xsd.uri_integer];
			    [Xsd.uri_double; Xsd.uri_double; Xsd.uri_double] ]
let numeric_func2 f2 = new func2
  numeric_func2_types
  Numeric.from_name Numeric.from_name f2 Numeric.to_name

let plus : Numeric.t -> Numeric.t -> Numeric.t monad = Numeric.func2 (+) (+.)
let plus_func = numeric_func2 plus

let minus : Numeric.t -> Numeric.t -> Numeric.t monad = Numeric.func2 (-) (-.)
let minus_func = numeric_func2 minus

let times : Numeric.t -> Numeric.t -> Numeric.t monad = Numeric.func2 ( * ) ( *. )
let times_func = numeric_func2 times

let div : Numeric.t -> Numeric.t -> Numeric.t monad = Numeric.func2 (/) (/.)
let div_func = numeric_func2 div

let modulo = Numeric.func2 (mod) (mod_float)
let modulo_func = numeric_func2 modulo

let power =
  let rec power_int x n =
    let y = power_int x (n/2) in 
    if n mod 2 = 0
    then y * y
    else x * y * y
  in
  Numeric.func2 power_int ( ** )
let power_func = numeric_func2 power

let sqrt = Numeric.func1_float Pervasives.sqrt
let sqrt_func = numeric_func1_float sqrt

let exp = Numeric.func1_float Pervasives.exp
let exp_func = numeric_func1_float exp

let log = Numeric.func1_float Pervasives.log
let log_func = numeric_func1_float log

let log10 = Numeric.func1_float Pervasives.log10
let log10_func = numeric_func1_float log10

let cos = Numeric.func1_float Pervasives.cos
let cos_func = numeric_func1_float cos

let sin = Numeric.func1_float Pervasives.sin
let sin_func = numeric_func1_float sin

let tan = Numeric.func1_float Pervasives.tan
let tan_func = numeric_func1_float tan

let uminus : Numeric.t -> Numeric.t list = Numeric.func1 (~-) (~-.)
let uminus_func = numeric_func1 uminus

let abs = Numeric.func1 abs abs_float
let abs_func = numeric_func1 abs

let round (x : Float.t) : Int.t monad =
  return (truncate (if x >= 0. then x +. 0.5 else x -. 0.5))
let round_func = new func1
  [ [Xsd.uri_integer; Xsd.uri_double] ]
  Float.from_name round Int.to_name

let floor (x : Float.t) : Float.t monad =
  return (floor x)
let floor_func = new func1
  [ [Xsd.uri_double; Xsd.uri_double] ]
  Float.from_name floor Float.to_name

let ceil (x : Float.t) : Float.t monad =
  return (ceil x)
let ceil_func = new func1
  [ [Xsd.uri_double; Xsd.uri_double] ]
  Float.from_name ceil Float.to_name

(* logic *)

let boolean_comp = new comp2
  [ [Xsd.uri_boolean; Xsd.uri_boolean] ]
  Bool.from_name Bool.from_name (fun b1 b2 -> [Bool.compare b1 b2])

let log_and (b1 : Bool.t) (b2 : Bool.t) : Bool.t monad =
  return (b1 && b2)
let log_and_func = new func2
  [ [Xsd.uri_boolean; Xsd.uri_boolean; Xsd.uri_boolean] ]
  Bool.from_name Bool.from_name log_and Bool.to_name

let log_or (b1 : Bool.t) (b2 : Bool.t) : Bool.t monad =
  return (b1 || b2)
let log_or_func = new func2
  [ [Xsd.uri_boolean; Xsd.uri_boolean; Xsd.uri_boolean] ]
  Bool.from_name Bool.from_name log_or Bool.to_name

let log_not (b : Bool.t) : Bool.t monad =
  return (not b)
let log_not_func = new func1
  [ [Xsd.uri_boolean; Xsd.uri_boolean] ]
  Bool.from_name log_not Bool.to_name


(* basic procedures *)

let echo = new proc1
  [ [Rdfs.uri_Resource] ]
  RdfTerm.from_name
  (fun n -> print_endline (Name.contents n))

let exec = new proc1
  [ [Xsd.uri_string] ]
  String.from_name
  (fun s -> ignore (Sys.command s))

let thunderbird_compose = new procN
  [ (* to, cc, subject, body *)
    [Xsd.uri_string; Xsd.uri_string; Xsd.uri_string; Xsd.uri_string] ]
  [String.from_name; String.from_name; String.from_name; String.from_name]
  (function
    | [s_to; s_cc; s_subject; s_body] ->
      let email = String.concat ","
	(List.map
	   (fun (a,s) -> a ^ "='" ^ s ^ "'")
	   ["to", s_to;
	    "cc", s_cc;
	    "subject", s_subject;
	    "body",s_body]) in
      let cmd = "thunderbird -compose \"" ^ email ^ "\"" in
      ignore (Sys.command cmd)
    | _ -> invalid_arg "Builtins_basic.thunderbird_compose")

(*command: thunderbird -compose "to='ferre@irisa.fr,seb.ferre@gmail.com',cc='eleonore.jouffe@gmail.com',subject='TEST',body='COucou',attachment='file1,file2'"*)
