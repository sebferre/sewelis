(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Intmap = Intmap.M
module Intset = Intset.Intmap
module Intrel2 = Intrel2.Intmap
module Name = Name.Make
module Rel = Extension.Rel
module Extension = Extension.Make
module Code = Code.Make

module Make =
  struct
    open Tarpit

    (* syntax *)

    module ProcSyntax =
      struct
	type t =
	  [ `Verb of (string * string option) * string list (* (verb, prep_opt), preps *)
	  | `Proc of string * int ]

	let verb ?(preps : string list = []) ?(prep : string option) v = `Verb ((v,prep), preps)
	let pred p n = `Proc (p,n)

	let arity = function
	  | `Verb (vp,ps) -> 1 + List.length ps
	  | `Proc (p,n) -> n

      end

    module PredSyntax =
      struct
	type t = 
	  [ `Noun of string * string list (* noun + prepositions for extra-arguments *)
	  | `RelNoun1 of string * string list (* noun + prepositions for extra arguments *)
	  | `RelNoun2 of string * string list (* noun + prepositions for extra arguments *)
	  | `Verb of (string * string option) * (string * string option) * string list (* active-prep? / passive-prep? + prepositions for extra-argument *)
	  | `Unop of string
	  | `Binop of string
	  | `Func of string * int (* last argument is result *)
	  | `Pred of string * int ]
	
	let noun ?(preps : string list = []) n = `Noun (n,preps)
	let relnoun1 ?(preps : string list = []) n = `RelNoun1 (n,preps)
	let relnoun2 ?(preps : string list = []) n = `RelNoun2 (n,preps)
	let verb ?(preps : string list = []) ?(prep1 : string option) ?(prep2 : string option) v1 v2 = `Verb ((v1,prep1), (v2,prep2), preps)
	let unop op = `Unop op
	let binop op = `Binop op
	let func f n = `Func (f,n)
	let pred p n = `Pred (p,n)

	let arity = function
	  | `Noun (n,ps) -> 1 + List.length ps
	  | `RelNoun1 (n,ps) -> 2 + List.length ps
	  | `RelNoun2 (n,ps) -> 2 + List.length ps
	  | `Verb (vp1,vp2,ps) -> 2 + List.length ps
	  | `Unop _ -> 2
	  | `Binop _ -> 3
	  | `Func (f,n) -> n+1
	  | `Pred (p,n) -> n

	let role_string syntax i =
	  match syntax, i with
	    | `Noun (n,_), 1 -> Some n
	    | `RelNoun1 (n,_), 1 -> Some n
	    | `RelNoun2 (n,_), 2 -> Some n
	    | `Verb ((v1,_),_,_), 1 -> Some ("what " ^ v1)
	    | `Verb (_,(v2,_),_), 2 -> Some ("what " ^ v2)
	    | `Unop op, 1 -> Some (op ^ " __")
	    | `Binop op, 1 -> Some ("__ " ^ op ^ " __")
	    | `Func (f,0), 1 -> Some (f ^ "()")
	    | `Func (f,1), 1 -> Some (f ^ "(__)")
	    | `Func (f,2), 1 -> Some (f ^ "(__,__)")
	    | `Func (f,3), 1 -> Some (f ^ "(__,__,__)")
	    | `Func (f,_), 1 -> Some (f ^ "(...)")
	    | _ -> None
      end

    (* expressions *)

    let tag_depth = ref 0
    let begin_tag typ name = () (* Printf.printf "%d<%s %s>\n" !tag_depth typ (Name.to_string "" [] [] name); incr tag_depth *)
    let end_tag typ name = () (* decr tag_depth; Printf.printf "%d</%s %s>\n" !tag_depth typ (Name.to_string "" [] [] name) *)

    let _s, _p, _o = "_s", "_p", "_o"

    let add_link ~(sym : bool) oid1 d1 oid2 d2 = Common.prof "Store.add_link" (fun () ->
      d1#add_succ ~sym oid2;
      d2#add_pred ~sym oid1)

    (* returns the least elements *)
    let entry_points (nodes : Intset.t) (succs : Intrel2.t) : Intset.t =
      let entries, _ =
	Common.fold_while
	  (fun (entries,rest) ->
	    try
	      let z = Intset.choose rest in
	      let succs_z = try Intrel2.assoc z succs with _ -> Intset.empty in
	      Some (Intset.diff entries succs_z, Intset.remove z (Intset.diff rest succs_z))
	    with Not_found -> (* rest is empty *)
	      None)
	  (nodes,nodes) in
      entries
      
    (* set of classes shared by a set of oids, as a type *) 
    let least_general_common_types ~obs store ?(implicit_types : Intset.t = Intset.empty) (oids : Intset.t) : Intset.t =
      let succs, preds = store#rdfs_subClassOf#relation#contents ~obs in
      let infered_types =
	Intset.fold
	  (fun acc c_o ->
	    Intset.union acc (Intset.add c_o (try Intrel2.assoc c_o succs with Not_found -> Intset.empty)))
	  Intset.empty implicit_types in
      let common_types =
	Intset.fold
	  (fun acc_inter o ->
	    let d_o = store#get_description o in
	    let types_o =
	      Intset.fold
		(fun acc_union c_o ->
		  Intset.union acc_union (Intset.add c_o (try Intrel2.assoc c_o succs with Not_found -> Intset.empty)))
		Intset.empty d_o#classes#set in
	    Intset.inter acc_inter types_o)
	  Intset.empty oids in
      entry_points (Intset.union infered_types common_types) succs
      
    type property_val = Intrel2.t * Intrel2.t
    type property_type = Intrel2.t (* a set of couples of classes' oids *)

    let symmetric_closure : property_val -> property_val =
      fun (r1,r2) -> Common.prof "Store.symmetric_closure" (fun () ->
	let rs = Intrel2.union r1 r2 in
	rs, rs)

    let transitive_closure : property_val -> property_val =
      fun (r1,r2) -> Common.prof "Store.transitive_closure" (fun () ->
	(* Roy-Warshall algorithm *)
	let zs = Intset.inter (Intrel2.keys r1) (Intrel2.keys r2) in (* pivots *)
	let rt1, rt2 =
	  Intset.fold
	    (fun (rt1,rt2) z ->
	      let e1_z = Intrel2.assoc z rt1 in
	      let e2_z = Intrel2.assoc z rt2 in
	      Intset.fold (* for each predecessor of z *)
		(fun (rt1',rt2') x ->
		  Intset.fold (* for each successor of z *)
		    (fun (rt1'',rt2'') y ->
		      (Intrel2.add x y rt1'', Intrel2.add y x rt2''))
		    (rt1',rt2') e1_z)
		(rt1,rt2) e2_z)
	    (r1,r2) zs in
	rt1, rt2)

    class virtual property_data store (name : string) =
      object (self)
	inherit [property_val] data
	method extension ~(obs : observer) (s : Extension.var) (o : Extension.var) : Extension.t =
	  Extension.intrel2_duo name s o (fun _ -> self#contents ~obs)
	method domain_cardinal ~obs =
	  let r1, _ = self#contents ~obs in
	  Intrel2.domain_cardinal r1
	method range_cardinal ~obs =
	  let _, r2 = self#contents ~obs in
	  Intrel2.domain_cardinal r2
	method has_instance ~obs n1 n2 =
	  let r1, _ = self#contents ~obs in
	  Intrel2.mem (store#get_entity n1) (store#get_entity n2) r1
	method domain ~(obs : observer) =
	  let r1, _ = self#contents ~obs in
	  Intrel2.keys r1
	method has_subject ~(obs : observer) n =
	  let r1, _ = self#contents ~obs in
	  Intrel2.mem_assoc (store#get_entity n) r1
	method subjects_oid ~(obs : observer) o =
	  let _, r2 = self#contents ~obs in
	  try Intrel2.assoc o r2 with _ -> Intset.empty
	method subjects ~(obs : observer) n_o =
	  let _, r2 = self#contents ~obs in
	  try Intrel2.assoc (store#get_entity n_o) r2 with _ -> Intset.empty
	method range ~(obs : observer) =
	  let _, r2 = self#contents ~obs in
	  Intrel2.keys r2
	method has_object ~(obs : observer) n =
	  let _, r2 = self#contents ~obs in
	  Intrel2.mem_assoc (store#get_entity n) r2
	method objects_oid ~(obs : observer) s =
	  let r1, _ = self#contents ~obs in
	  try Intrel2.assoc s r1 with _ -> Intset.empty
	method objects ~(obs : observer) n_s =
	  let r1, _ = self#contents ~obs in
	  try Intrel2.assoc (store#get_entity n_s) r1 with _ -> Intset.empty
	method iter : (Name.t -> Name.t -> unit) -> unit =
	  fun f -> Common.prof "Store.property_data#iter" (fun () ->
	    effect (fun obs ->
	      let r1, _ = self#contents ~obs in
	      Intrel2.fold_assoc
		(fun _ s oids ->
		  let ns = store#get_name s in
		  Intset.fold
		    (fun _ o ->
		      let no = store#get_name o in
		      f ns no)
		    () oids)
		() r1))
	method fold_instances : 'a. obs:observer -> ('a -> Name.t -> Name.t -> 'a) -> 'a -> 'a =
	  fun ~obs f init -> Common.prof "Store.property_data#fold_instances" (fun () ->
	    let r1, _ = self#contents ~obs in
	    Intrel2.fold_assoc
	      (fun acc s oids ->
		let ns = store#get_name s in
		Intset.fold
		  (fun acc o ->
		    let no = store#get_name o in
		    f acc ns no)
		  acc oids)
	      init r1)
	method fold_successors : 'a. obs:observer -> ('a -> Name.t -> 'a) -> 'a -> Name.t -> 'a =
	  fun ~obs f init n1 -> Common.prof "Store.property_data#fold_successors" (fun () ->
	    let r1, _ = self#contents ~obs in
	    try
	      let e1_n1 = Intrel2.assoc (store#get_entity n1) r1 in
	      Intset.fold (fun res o2 -> f res (store#get_name o2)) init e1_n1
	    with _ -> init)
	method fold_predecessors : 'a. obs:observer -> ('a -> Name.t -> 'a) -> 'a -> Name.t -> 'a =
	  fun ~obs f init n2 -> Common.prof "Store.property_data#fold_predecessors" (fun () ->
	    let _, r2 = self#contents ~obs in
	    try
	      let e2_n2 = Intrel2.assoc (store#get_entity n2) r2 in
	      Intset.fold (fun res o1 -> f res (store#get_name o1)) init e2_n2
	    with _ -> init)
	method least_common_successors ~(obs : observer) (n_x : Name.t) (n_y : Name.t) : Name.t list =
	  Common.prof "Store.property_data#least_common_successors" (fun () ->
	    if n_x = n_y
	    then [n_x]
	    else
	      let x = store#get_entity n_x in
	      let y = store#get_entity n_y in
	      let r1, _ = self#contents ~obs in
	      if Intrel2.mem x y r1 then [n_y]
	      else if Intrel2.mem y x r1 then [n_x]
	      else
		let succs_x = try Intrel2.assoc x r1 with _ -> Intset.empty in
		let succs_y = try Intrel2.assoc y r1 with _ -> Intset.empty in
		let succs_xy = Intset.inter succs_x succs_y in
		if Intset.is_empty succs_xy then []
		else begin
		  let least = entry_points succs_xy r1 in
		  Intset.fold
		    (fun res z -> (store#get_name z) :: res)
		    [] least
		end)
	method greatest_common_predecessors ~(obs : observer) (n_x : Name.t) (n_y : Name.t) : Name.t list =
	  Common.prof "Store.property_data#greatest_common_predecessors" (fun () ->
	    prerr_string "greatest_common_predecessors("; prerr_string (Name.contents n_x); prerr_string ", "; prerr_string (Name.contents n_y); prerr_endline ")";
	    if n_x = n_y
	    then [n_x]
	    else
	      let x = store#get_entity n_x in
	      let y = store#get_entity n_y in
	      let _, r2 = self#contents ~obs in
	      if Intrel2.mem x y r2 then [n_y]
	      else if Intrel2.mem y x r2 then [n_x]
	      else
		let preds_x = try Intrel2.assoc x r2 with Not_found -> Intset.empty in
		let preds_y = try Intrel2.assoc y r2 with Not_found -> Intset.empty in
		let preds_xy = Intset.inter preds_x preds_y in
		if Intset.is_empty preds_xy then []
		else begin
		  let greatest = entry_points preds_xy r2 in
		  Intset.fold
		    (fun res z -> (store#get_name z) :: res)
		    [] greatest
		end)

	val types_view : property_type view = new view ("types of" ^ name)
	initializer types_view#define (fun obs -> Common.prof "Store.property_data#types" (fun () ->
	  let r1, r2 = self#contents ~obs in
	  let keys1, keys2 = Intrel2.keys r1, Intrel2.keys r2 in
	  let types1, types2 = least_general_common_types ~obs store keys1, least_general_common_types ~obs store keys2 in
	  Intset.fold
	    (fun acc c1 ->
	      Intset.fold
		(fun acc c2 ->
		  Intrel2.add c1 c2 acc)
		acc types2)
	    Intrel2.empty types1))
(*	  
	  Intrel2.fold
	    (fun acc s o ->
	      let d_s = store#get_description s in
	      let d_o = store#get_description o in
	      Intset.fold
		(fun acc c_s ->
		  Intset.fold
		    (fun acc c_o ->
		      Intrel2.add c_s c_o acc)
		    acc d_o#classes#set)
		acc d_s#classes#set)
	    Intrel2.empty r1))
*)
	method types ~(obs : observer) : (Uri.t * Uri.t) list =
	  let r = types_view#contents ~obs in
	  Intrel2.fold
	    (fun acc s o ->
	      match store#get_name s, store#get_name o with
		| Rdf.URI cs, Rdf.URI co -> (cs,co)::acc
		| _ -> acc)
	    [] r
      end

    class property_state store name =
      object
	inherit [property_val] state (Intrel2.empty, Intrel2.empty)
	inherit property_data store name
      end

    class property_view store name =
      object
	inherit [property_val] view name
	inherit property_data store name
      end

    class virtual property store =
      object (self)
	method virtual uri : Uri.t
	method name : Name.t = Rdf.URI self#uri
	method virtual oid : Extension.oid
	method virtual add : Name.t -> Name.t -> unit
	method virtual remove : Name.t -> Name.t -> unit
	method explicit_relation = (new property_state store ("explicit_" ^ self#uri) :> property_data)
	method strict_direct_relation = self#explicit_relation
	method strict_relation = self#strict_direct_relation
	method direct_relation = self#strict_direct_relation
	method relation = self#strict_direct_relation
	method iter_triples (f : Rdf.triple -> unit) : unit =
	  self#explicit_relation#iter
	    (fun s o -> 
	      match s with
		| Rdf.Literal _
		| Rdf.XMLLiteral _ -> ()
		| _ -> f {Rdf.s; Rdf.p = self#uri; Rdf.o; Rdf.t_opt=None})
      end

    type class_val = Intset.t
    type class_type = Intset.t

    class virtual class_data store (name : string) =
      object (self)
	inherit [class_val] data
	method extension ~(obs : observer) (s : Extension.var) : Extension.t =
	  Extension.intset name s (fun _ -> self#contents ~obs)
	method cardinal ~(obs : observer) : int =
	  Intset.cardinal (self#contents ~obs)
	method has_instance ~(obs : observer) (n : Name.t) : bool =
	  Intset.mem (store#get_entity n) (self#contents ~obs)
	method instances ~(obs : observer) : Intset.t =
	  self#contents ~obs
	method iter : (Name.t -> unit) -> unit =
	  fun f -> Common.prof "Store.class_data#iter" (fun () ->
	    effect (fun obs ->
	      Intset.fold
		(fun _ -> function
		  | s -> f (store#get_name s)
		  | _ -> assert false)
		()
		(self#contents ~obs)))
	method fold_instances : 'a. obs:observer -> ('a -> Name.t -> 'a) -> 'a -> 'a =
	  fun ~obs f init -> Common.prof "Store.class_data#fold_instances" (fun () ->
	    Intset.fold (fun res o -> f res (store#get_name o)) init (self#contents ~obs))

	val types_view : class_type view = new view ("types of " ^ name)
	initializer types_view#define (fun obs -> Common.prof "Store.class_data#types" (fun () ->
	  least_general_common_types ~obs store (self#contents ~obs)))
	  (*
	  Intset.fold
	    (fun acc s ->
	      let d_s = store#get_description s in
	      Intset.fold
		(fun acc c_s ->
		  Intset.add c_s acc)
		acc d_s#classes#set)
	    Intset.empty (self#contents ~obs)))
	  *)
	method types ~(obs : observer) : Uri.t list =
	  Intset.fold
	    (fun acc s ->
	      match store#get_name s with
		| Rdf.URI cs -> cs::acc
		| _ -> acc)
	    [] (types_view#contents ~obs)
      end

    class class_state store name =
      object (self)
	inherit [class_val] state Intset.empty
	inherit class_data store name
      end

    class class_view store name =
      object (self)
	inherit [class_val] view name
	inherit class_data store name
      end

    class virtual classe store =
      object (self)
	method virtual uri : Uri.t
	method name = Rdf.URI self#uri
	method virtual oid : Extension.oid
	method virtual add : Name.t -> unit
	method virtual remove : Name.t -> unit
	method explicit_relation = (new class_state store ("explicit_" ^ self#uri) :> class_data)
	method relation = self#explicit_relation
	method iter_triples (f : Rdf.triple -> unit) : unit =
	  self#explicit_relation#iter
	    (fun s ->
	      match s with
		| Rdf.Literal _
		| Rdf.XMLLiteral _ -> ()
		| _ -> f {Rdf.s; Rdf.p = Rdf.uri_type; Rdf.o = self#name; Rdf.t_opt=None})
      end

    type functor_val = Rel.t * Rel.t (* args-x, x-args *)
    type functor_type = Rel.t (* x-args types *)

    class virtual functor_data store (name : string) =
      object (self)
	inherit [functor_val] data
	method extension ~(obs : observer) (x : Extension.var) (args : Extension.var array) : Extension.t =
	  let largs = Array.to_list args in
	  Extension.relation_multi name (largs @ [x])
	    (fun _ ->
	      let r1, r2 = self#contents ~obs in
	      r1, [([x],x::largs,r2)])
	method domain_cardinal ~obs =
	  let _, r2 = self#contents ~obs in
	  Intset.cardinal (Rel.keys r2)
	method domain ~obs =
	  let _, r2 = self#contents ~obs in
	  Rel.keys r2
	method fold_args : 'a. obs:observer -> ('a -> Name.t list -> 'a) -> 'a -> Name.t -> 'a =
	  fun ~obs f init n1 -> Common.prof "Store.functor_data#fold_args" (fun () ->
	    let _, r2 = self#contents ~obs in
	    try
	      let r2_n1 = Rel.assoc (store#get_entity n1) r2 in
	      Rel.fold
		(fun res l ->
		  let args = List.map store#get_name l in
		  f res args)
		init r2_n1
	    with _ -> init)

	method iter : (Name.t -> Name.t list -> unit) -> unit =
	  fun f -> Common.prof "Store.functor_data#iter" (fun () ->
	    effect (fun obs ->
	      let r1, r2 = self#contents ~obs in
	      Rel.fold
		(fun _ l ->
		  match l with
		  | x::args -> f (store#get_name x) (List.map store#get_name args)
		  | _ -> assert false)
		()
		r2))

	val types_view : functor_type view = new view ("types of " ^ name)
	initializer
	  let rec fold_product_intset f acc lx = function
	    | [] -> f acc (List.rev lx)
	    | s::ls ->
	      Intset.fold
		(fun acc x -> fold_product_intset f acc (x::lx) ls)
		acc s
	  in
	  types_view#define (fun obs -> Common.prof "Store.functor_data#types" (fun () ->
	    let _, r = self#contents ~obs in
	    let domains = Rel.fold (fun acc l -> List.map2 Intset.add l acc) [] r in
	    let common_types = List.map (least_general_common_types ~obs store) domains in
	    fold_product_intset
	      (fun acc lc -> Rel.add lc acc)
	      (Rel.empty (Rel.dim r)) [] domains))
	    (*
	    Rel.fold
	      (fun acc l ->
		let ls = List.map (fun o -> (store#get_description o)#classes#set) l in
		fold_product_intset
		  (fun acc lc -> Rel.add lc acc)
		  acc [] ls)
	      (Rel.empty (Rel.dim r)) r))
	    *)
	  method types ~(obs : observer) : (Uri.t list) list =
	    let r = types_view#contents ~obs in
	    Rel.fold
	      (fun acc lx ->
		try
		  let ln = List.map (fun x -> match store#get_name x with Rdf.URI c -> c | _ -> raise Not_found) lx in
		  ln::acc
		with Not_found -> acc)
	      [] r
      end

    class functor_state store arity name =
      object (self)
	inherit [functor_val] state (Rel.empty (arity + 1), Rel.empty (arity + 1)) (* 1 for the structure itself *)
	inherit functor_data store name
      end

    class functor_view store name =
      object (self)
	inherit [functor_val] view name
	inherit functor_data store name
      end

    class virtual funct store =
      object (self)
	method virtual uri : Uri.t
	method virtual arity : int
	method name = Rdf.URI self#uri
	method virtual oid : Extension.oid
	method virtual add : Name.t -> Name.t list -> unit
	method virtual remove : Name.t -> Name.t list -> unit
	method explicit_relation = 
	  (new functor_state store self#arity
	     ("explicit_" ^ self#uri ^ "/" ^ string_of_int self#arity) :> functor_data)
	method relation = self#explicit_relation
	method iter_triples (f : Rdf.triple -> unit) : unit =
	  self#explicit_relation#iter
	    (fun x args ->
	      f {Rdf.s = x; Rdf.p = Term.uri_functor; Rdf.o = self#name; Rdf.t_opt = None};
	      ignore (List.fold_left (fun i arg -> f {Rdf.s = x; Rdf.p = Term.uri_arg i; Rdf.o = arg; Rdf.t_opt = None}; i+1) 1 args))
      end


    class virtual primitive =
    object (self)
      method virtual id : string
      method virtual arity : int
      method virtual syntax : PredSyntax.t
      method virtual types : Builtins.typing list
      method virtual bounded : bool list -> bool 
      method virtual extension : obs:observer -> Extension.var list -> Extension.t

      method types_at_index (i : int) : Uri.t LSet.t =
	List.fold_left
	  (fun res t -> LSet.add (List.nth t (i-1)) res)
	  (LSet.empty ())
	  self#types
    end

    class virtual procedure =
    object (self)
      method virtual id : string
      method virtual arity : int
      method virtual syntax : ProcSyntax.t
      method virtual types : Builtins.typing list
      method virtual code : Extension.var list -> Code.t list

      method types_at_index (i : int) : Uri.t LSet.t =
	List.fold_left
	  (fun res t -> LSet.add (List.nth t (i-1)) res)
	  (LSet.empty ())
	  self#types
    end


    (* ---------------------- *)

    class expr_property
	~subproperties
	~equivalents
	?transitive
	?reflexive
	?symmetric
	?functional
	?inverse_functional
	store (r : Uri.t) =
      object (self : 'self)
	inherit property store

	method uri = r

	val oid : Extension.oid = store#get_entity (Rdf.URI r)
	method oid : Extension.oid = oid (*store#get_entity self#name*)

	val explicit_rel = new property_state store ("explicit_" ^ r)
	method explicit_relation = (explicit_rel :> property_data)
(*	val premises : expr subjects = new subjects *)
(*
	val mutable func = Option.get functional (fun () -> false)
	val mutable inverse_func = Option.get inverse_functional (fun () -> false)
*)

	method add ns no = Common.prof "Store.expr_property#add" (fun () ->
	  let p = oid in
	  let s = store#get_entity ns in
	  let o = store#get_entity no in
	  explicit_rel#update (fun (r1,r2) -> (* #update is quick *)
	    if not (Intrel2.mem s o r1)
	    then begin
	      let d_s = store#get_description s in (* quick *)
	      let d_o = store#get_description o in (* quick *)
	      add_link ~sym:true s d_s o d_o;
	      d_s#fwd_properties#add p; (* quick *)
	      d_o#bwd_properties#add p; (* quick *)
	      Some (Intrel2.add s o r1, Intrel2.add o s r2) end
(*
	    if false && func then
	      explicit_rel <- Rel.filter_restr [(_s, List.assoc _s m)] [_s; _o; _t; _src] (fun _ -> false) explicit_rel;
	    if false && inverse_func then
	      explicit_rel <- Rel.filter_restr [(_o, List.assoc _o m)] [_s; _o; _t; _src] (fun _ -> false) explicit_rel;
*)
	    else None))
	  
	method remove ns no =
	  let p = oid in
	  let s = store#get_entity ns in
	  let o = store#get_entity no in
	  explicit_rel#update (fun (r1,r2) ->
	    if Intrel2.mem s o r1
	    then begin
	      let r1, r2 = Intrel2.remove s o r1, Intrel2.remove o s r2 in
	      if not (Intrel2.mem_assoc s r1) then (store#get_description s)#fwd_properties#remove p;
	      if not (Intrel2.mem_assoc o r2) then (store#get_description o)#bwd_properties#remove p;
	      Some (r1,r2) end
	    else None);


	val strict_direct_view = new property_view store ("strict_direct_" ^ r)
	method strict_direct_relation = (strict_direct_view :> property_data)
	initializer strict_direct_view#define (fun obs -> Common.prof "Store.expr_property#strict_direct_relation" (fun () ->
	  let r12 = self#explicit_relation#contents ~obs in
	  let r12 =
	    if subproperties
	    then
	      let p_subpropertyof = (store#rdfs_subPropertyOf : property) in
	      p_subpropertyof#direct_relation#fold_predecessors ~obs
		(fun (r1,r2) -> function
		  | (Rdf.URI uri as sub_name) ->
		    if not (p_subpropertyof#relation#has_instance ~obs self#name sub_name)
		    then
		      let sp = store#get_property uri in
		      let rs1, rs2 = sp#relation#contents ~obs in
		      (Intrel2.union r1 rs1, Intrel2.union r2 rs2)
		    else (r1,r2)
		  | _ -> r1, r2)
		r12 self#name
	    else r12 in
	  r12))

	val strict_view = new property_view store ("strict_" ^ r)
	method strict_relation = (strict_view :> property_data)
	initializer strict_view#define (fun obs -> Common.prof "Store.expr_property#strict_relation" (fun () ->
	  let is_trans = Option.get transitive (fun () -> store#owl_TransitiveProperty#relation#has_instance ~obs self#name) in
	  let is_sym = Option.get symmetric (fun () -> store#owl_SymmetricProperty#relation#has_instance ~obs self#name) in
	  let r12 = self#strict_direct_relation#contents ~obs in
	  let r12 =
	    if Option.get symmetric (fun () -> store#owl_SymmetricProperty#relation#has_instance ~obs self#name)
	    then symmetric_closure r12
	    else r12 in
	  let r12 =
	    if Option.get transitive (fun () -> store#owl_TransitiveProperty#relation#has_instance ~obs self#name)
	    then transitive_closure r12
	    else r12 in
	  r12))

	val direct_view = new property_view store ("direct_" ^ r)
	method direct_relation = (direct_view :> property_data)
	initializer direct_view#define (fun obs -> Common.prof "Store.expr_property#direct_relation" (fun () ->
	  let r12 = self#strict_direct_relation#contents ~obs in
	  let r12 =
	    if equivalents
	    then
	      (store#owl_inverseOf : property)#relation#fold_successors ~obs
		(fun (r1,r2) -> function
		  | Rdf.URI uri ->
		      let p = store#get_property uri in
		      let ri1, ri2 = p#strict_direct_relation#contents ~obs in
		      (Intrel2.union r1 ri2, Intrel2.union r2 ri1)
		  | _ -> (r1,r2))
		r12 self#name
	    else r12 in
	  r12))
    
	val view = new property_view store r
	method relation = (view :> property_data)
	initializer view#define (fun obs -> Common.prof "Store.expr_property#relation" (fun () ->
	  let r12 = self#strict_relation#contents ~obs in
	  let r12 =
	    if equivalents
	    then
	      (store#owl_inverseOf : property)#relation#fold_successors ~obs
		(fun (r1,r2) -> function
		  | Rdf.URI uri ->
		      let p = store#get_property uri in
		      let ri1, ri2 = p#strict_relation#contents ~obs in
		      (Intrel2.union r1 ri2, Intrel2.union r2 ri1)
		  | _ -> r1, r2)
		r12 self#name
	    else r12 in
	  r12))

      end

    class expr_type store =
      object (self)
	inherit property store

	method uri = Rdf.uri_type

	method oid : Extension.oid = store#get_entity self#name

	method add ns no : unit =
	  match no with
	  | Rdf.URI uri ->
	      (store#get_class uri)#add ns
	  | _ -> print_endline "Store.expr_type#add: object is not a URI"

	method remove ns no : unit =
	  match no with
	  | Rdf.URI uri ->
	      (store#get_class uri)#remove ns
	  | _ -> print_endline "Store.expr_type#add: object is not a URI"

	val view = new property_view store "strict_direct_rdf:type"
	method strict_direct_relation = (view :> property_data)
	initializer view#define (fun obs -> Common.prof "Store.expr_type#strict_direct_relation" (fun () ->
	  let r12 =
	    (store#rdfs_Class : classe)#relation#fold_instances ~obs
	      (fun r12 nc ->
		match nc with
		| Rdf.URI uri when not (Name.is_primitive_class uri) ->
		    let c = store#get_class uri in
		    let y = c#oid in
		    Intset.fold
		      (fun (r1,r2) x -> (Intrel2.add x y r1, Intrel2.add y x r2))
		      r12 (c#relation#instances ~obs)
		| _ -> r12)
	      (Intrel2.empty,Intrel2.empty) in
	  r12))
      end

    class expr_class store (r : Uri.t) =
      object (self : 'self)
	inherit classe store

	method uri = r

	val oid : Extension.oid = store#get_entity (Rdf.URI r)
	method oid : Extension.oid = oid (*store#get_entity self#name*)

	val explicit_rel = new class_state store ("explicit_" ^ r)
	method explicit_relation = (explicit_rel :> class_data)

	method add ns = Common.prof "Store.expr_class#add" (fun () ->
	  let c = oid in
	  let s = store#get_entity ns in
	  explicit_rel#update (fun oids -> (* #update is quick *)
	    if not (Intset.mem s oids)
	    then begin
	      let d_s = store#get_description s in (* quick *)
	      d_s#classes#add c; (* quick *)
	      Some (Intset.add s oids) end
	    else None))

	method remove ns =
	  let c = oid in
	  let s = store#get_entity ns in
	  explicit_rel#update (fun oids ->
	    if Intset.mem s oids
	    then begin
	      (store#get_description s)#classes#remove c;
	      Some (Intset.remove s oids) end
	    else None)

	val view = new class_view store r
	method relation = (view :> class_data)
	initializer view#define (fun obs -> Common.prof "Store.expr_class#relation" (fun () ->
	  if List.mem r [Rdfs.uri_Resource; Rdfs.uri_Literal]
	  then explicit_rel#contents ~obs
	  else begin
	    begin_tag "ext" r;
	    let ext = self#explicit_relation#extension ~obs _s in
	    let ext =
	      let p_subclassof = (store#rdfs_subClassOf : property) in
	      p_subclassof#direct_relation#fold_predecessors ~obs
		(fun res -> function
		  | (Rdf.URI uri as sub_name) ->
		    if not (p_subclassof#relation#has_instance ~obs self#name sub_name)
		    then (* 'uri' is not an equivalent class *)
		      let sc : classe = store#get_class uri in
		      Extension.union res (sc#relation#extension ~obs _s)
		    else res
		  | _ -> res)
		ext self#name in
	    let ext =
	      (store#rdfs_domain : property)#relation#fold_predecessors ~obs
		(fun res -> function
		  | Rdf.URI uri ->
		      if Name.is_primitive_property uri || uri = Rdf.uri_type
		      then res
		      else
			let p : property = store#get_property uri in
			Extension.union res (p#relation#extension ~obs _s "")
		  | _ -> res)
		ext self#name in
	    let ext =
	      (store#rdfs_range : property)#relation#fold_predecessors ~obs
		(fun res -> function
		  | Rdf.URI uri ->
		      if Name.is_primitive_property uri || uri = Rdf.uri_type
		      then res
		      else
			let p : property = store#get_property uri in
			Extension.union res (p#relation#extension ~obs "" _s)
		  | _ -> res)
		ext self#name in
	    end_tag "ext" r;
	    ext#intset (store :> Extension.store) _s
	  end))
      end

    class expr_functor store (r : Uri.t) (a : int) =
      object (self : 'self)
	inherit funct store

	method uri = r
	method arity = a

	val oid : Extension.oid = store#get_entity (Rdf.URI r)
	method oid = oid

	val explicit_rel = new functor_state store a ("explicit_" ^ r ^ "/" ^ string_of_int a)
	method explicit_relation = (explicit_rel :> functor_data)

	method add nx nargs = Common.prof "Store.expr_functor#add" (fun () ->
	  let f = oid in
	  let x = store#get_entity nx in
	  let args = List.map store#get_entity nargs in
	  let tup = args @ [x] in
	  explicit_rel#update (fun (r1,r2) ->
	    if not (Rel.mem tup r1)
	    then begin
	      let d_f = store#get_description f in
	      let d_x = store#get_description x in
	      let d_args = List.map (fun arg -> arg, store#get_description arg) args in
	      List.iter
		(fun (arg,d_arg) -> add_link ~sym:true x d_x arg d_arg)
		d_args;
	      d_x#structs#add f a 0;
	      ignore (List.fold_left
			(fun pos (arg,d_arg) -> d_arg#structs#add f a pos; pos+1)
			1 d_args);
	      Some (Rel.add tup r1, Rel.add (x::args) r2) end
	    else None))

	method remove nx nargs =
	  let f = oid in
	  let x = store#get_entity nx in
	  let args = List.map store#get_entity nargs in
	  let tup = args @ [x] in
	  explicit_rel#update (fun (r1,r2) ->
	    if not (Rel.mem tup r1)
	    then begin
	      let r1, r2 = Rel.remove tup r1, Rel.remove (x::args) r2 in
	      if not (Rel.matches [Some x] r2) then (store#get_description x)#structs#remove f a 0;
	      ignore (List.fold_left
			(fun (pos,patt) arg ->
			  if not (Rel.matches (patt@[Some arg]) r1) then (store#get_description arg)#structs#remove f a pos;
			  (pos+1, None::patt))
			(1,[]) args);
	      Some (r1,r2) end
	    else None)

	val view = new functor_view store (r ^ "/" ^ string_of_int a)
	method relation = (view :> functor_data)
	initializer view#define (fun obs -> Common.prof "Store.expr_functor#relation" (fun () ->
	  let ra = r ^ "/" ^ string_of_int a in
	  let _x = "0" in
	  let _args = Array.init a (fun i -> string_of_int (i+1)) in
	  begin_tag "ext" ra;
	  let ext_explicit = self#explicit_relation#extension ~obs _x _args in
	  let ext =
	    if (store#get_class Term.uri_ImplicitFunctor : classe)#relation#has_instance ~obs self#name
	    then
	      let ext_implicit =
		(store#get_property Term.uri_functorType : property)#relation#fold_successors ~obs
		  (fun res -> function
		    | Rdf.URI uri_c ->
			let c : classe = store#get_class uri_c in
			c#relation#extension ~obs _x
		    | _ -> res)
		  Extension.one self#name in
	      let ext_implicit =
		Common.fold_for
		  (fun i res ->
		    (store#get_property (Term.uri_functorArg i) : property)#relation#fold_successors ~obs
		      (fun res -> function
			| Rdf.URI uri_pi ->
			    let p : property = store#get_property uri_pi in
			    Extension.join res (p#relation#extension ~obs _x _args.(i-1))
			| _ -> res)
		      res self#name)
		  1 a ext_implicit in
	      Extension.union ext_explicit ext_implicit
	    else ext_explicit in
	  end_tag "ext" ra;
	  let _largs = Array.to_list _args in
	  match ext#relation_multi (store :> Extension.store)
	      [ _largs @ [_x];
		_x :: _largs ] with
	  | [r1;r2] -> (r1,r2)
	  | _ -> assert false))
      end

    class expr_pred (id : string) (syntax : PredSyntax.t) (pred : Builtins.pred) =
    object (self)
      inherit primitive
      method id = id
      method arity = pred#arity
      method syntax = syntax
      method types = pred#types
      method bounded lb =
	List.for_all ((=) true) lb
      method extension ~obs lv =
	Extension.check (Extension.pred id pred lv)

      initializer
      let a = self#arity in
      if not (List.for_all (fun t -> List.length t = a) self#types)
      then failwith ("Store.expr_pred: wrong arity in typing of '" ^ self#id ^ "'");
      if not (PredSyntax.arity self#syntax = a)
      then failwith ("Store.expr_pred: wrong arity in syntax of '" ^ self#id ^ "'")
    end


    class expr_func (id : string) (syntax : PredSyntax.t) (func : Builtins.func) =
    object (self)
      inherit primitive
      method id = id
      method arity = 1 + func#arity
      method syntax = syntax
      method types = func#types
      method bounded lb = (* all but last is true *)
	List.for_all ((=) true) (List.tl lb)
      method extension ~obs lv =
	match lv with
	  | y::lx -> Extension.bind y (Extension.expr_func id func lx)
	  | _ -> assert false

      initializer
      let a = self#arity in
      if not (List.for_all (fun t -> List.length t = a) self#types)
      then failwith ("Store.expr_func: wrong arity in types of '" ^ self#id ^ "'");
      if not (PredSyntax.arity self#syntax = a)
      then failwith ("Store.expr_func: wrong arity in syntax of '" ^ self#id ^ "'")
    end

    class expr_proc (id : string) (syntax : ProcSyntax.t) (proc : Builtins.proc) =
    object (self)
      inherit procedure
      method id = id
      method arity = proc#arity
      method syntax = syntax
      method types = proc#types
      method code lv = [Code.update id (fun lx -> proc#run lx) lv]

      initializer
      let a = self#arity in
      if not (List.for_all (fun t -> List.length t = a) self#types)
      then failwith ("Store.expr_proc: wrong arity in typing of '" ^ self#id ^ "'");
      if not (ProcSyntax.arity self#syntax = a)
      then failwith ("Store.expr_proc: wrong arity in syntax of '" ^ self#id ^ "'")
    end

    (* descriptions *)

	(* TODO: should use Tarpit for object descriptions *)

    class oid_set store =
    object (self)
	val mutable set : Intset.t = Intset.empty
	method set = set

	method fold_as_uris : 'a. ('a -> Uri.t -> 'a) -> 'a -> 'a =
	  fun f init ->
	    Intset.fold
	      (fun res oid ->
		match store#get_name oid with
		| Rdf.URI uri -> f res uri
		| _ -> res)
	      init set

	method iter_as_uris f = self#fold_as_uris (fun () uri -> f uri) ()

	method add (oid : Extension.oid) : unit =
	  if not (Intset.mem oid set)
	  then set <- Intset.add oid set

	method remove (oid : Extension.oid) : unit =
	  set <- Intset.remove oid set
      end

    class struct_set store =
    object (self)
	val mutable set : Rel.t = Rel.empty 3 (* (functor_oid, arity, pos) *)
	method set = set
	  
	method fold_as_uris : 'a. ('a -> Uri.t -> int -> int -> 'a) -> 'a -> 'a =
	  fun f init ->
	    Rel.fold
	      (fun res -> function
		| [functor_oid; arity; pos] ->
		  ( match store#get_name functor_oid with
		    | Rdf.URI uri -> f res uri arity pos
		    | _ -> res )
		| _ -> assert false)
	      init set

	method iter_as_uris f = self#fold_as_uris (fun () uri arity pos -> f uri arity pos) ()

	method add (functor_oid : Extension.oid) (arity : int) (pos : int) =
	  set <- Rel.add [functor_oid; arity; pos] set

	method remove (functor_oid : Extension.oid) (arity : int) (pos : int) =
	  set <- Rel.remove [functor_oid; arity; pos] set
      end

    class description store oid =
      object (self : 'self)
	val oid : Extension.oid = oid
	method oid = oid

	val classes : oid_set = new oid_set store
	method classes = classes

	val fwd_properties : oid_set = new oid_set store
	method fwd_properties = fwd_properties

	val bwd_properties : oid_set = new oid_set store
	method bwd_properties = bwd_properties

	val structs : struct_set = new struct_set store
	method structs = structs

        (* for pagerank *)
	val mutable rank = 1.
	val mutable preds : Intset.t = Intset.empty
	val mutable succs : Intset.t = Intset.empty
	val mutable nb_succs : int = 0

	method rank = rank
	method preds = preds
	method succs = succs
	method nb_succs = nb_succs

	method set_rank r =
	  rank <- r
	method add_pred ~sym o =
	  if not (Intset.mem o preds)
	  then preds <- Intset.add o preds;
	  if sym && not (Intset.mem o succs)
	  then begin
	    succs <- Intset.add o succs;
	    nb_succs <- nb_succs + 1
	  end;
	method add_succ ~sym o =
	  if not (Intset.mem o succs)
	  then begin
	    succs <- Intset.add o succs;
	    nb_succs <- nb_succs + 1
	  end;
	  if sym && not (Intset.mem o preds)
	  then preds <- Intset.add o preds

      end

    let make_description store oid = (* quick *)
      new description store oid
      

    (* Main class: the RDF store *)

    class store (uri : Uri.t) =
      object (self : 'store)
	inherit subject

	val mutable axiom_triples : Rdf.triple list = []
	  
	val mutable oid_cpt = 1
	val oids = new state Intset.empty
	val h_name : (Extension.oid, Name.t) Hashtbl.t = Hashtbl.create 1003
	val h_entity : (Name.t, Extension.oid) Hashtbl.t = Hashtbl.create 1003
	val h_description : (Extension.oid, description) Hashtbl.t = Hashtbl.create 1003
	val h_class : (Uri.t, classe) Hashtbl.t = Hashtbl.create 101
	val h_property : (Uri.t, property) Hashtbl.t = Hashtbl.create 71
	val h_functor : (Uri.t * int, funct) Hashtbl.t = Hashtbl.create 53
	val h_primitive : (string * int, primitive) Hashtbl.t = Hashtbl.create 47
	val h_procedure : (string * int, procedure) Hashtbl.t = Hashtbl.create 13

	val store_integer = new Store_order.Integer.store
	val store_double = new Store_order.Double.store
	val store_date = new Store_order.Date.store
	val store_time = new Store_order.Time.store
	val store_match = new Store_match.store

	method uri = uri
	method name = Rdf.URI uri

	method is_axiom_triple (triple : Rdf.triple) : bool =
	  List.mem triple axiom_triples
	  
	method iter_classes : (Uri.t -> classe -> unit) -> unit =
	  fun f ->
	    Hashtbl.iter f h_class

	method iter_properties : (Uri.t -> property -> unit) -> unit =
	  fun f ->
	    Hashtbl.iter f h_property

	method iter_functors : (Uri.t * int -> funct -> unit) -> unit =
	  fun f ->
	    Hashtbl.iter f h_functor

	method iter_primitives : (string * int -> primitive -> unit) -> unit =
	  fun f ->
	    Hashtbl.iter f h_primitive

	method fold_primitives : 'a. (string * int -> primitive -> 'a -> 'a) -> 'a -> 'a =
	  fun f init ->
	    Hashtbl.fold f h_primitive init

	method iter_procedures : (string * int -> procedure -> unit) -> unit =
	  fun f ->
	    Hashtbl.iter f h_procedure

	method fold_procedures : 'a. (string * int -> procedure -> 'a -> 'a) -> 'a -> 'a =
	  fun f init ->
	    Hashtbl.fold f h_procedure init


	method iter_triples : (Rdf.triple -> unit) -> unit =
	  let excluded_classes = [Rdfs.uri_Resource; Rdfs.uri_Literal; Xsd.uri_integer; Xsd.uri_string; Xsd.uri_date; Xsd.uri_dateTime; Xsd.uri_time; Xsd.uri_decimal] in
	  fun f -> Common.prof "Store.store#iter_triples" (fun () ->
	    self#iter_classes (fun uri cc -> if not (List.mem uri excluded_classes) then cc#iter_triples f);
	    self#iter_properties (fun uri pp -> pp#iter_triples f);
	    self#iter_functors (fun (uri,arity) ff -> ff#iter_triples f))


	method private alloc_oid : Extension.oid =
	  oid_cpt <- oid_cpt+1;
	  oids#update (fun oids -> Some (Intset.add oid_cpt oids));
	  oid_cpt

	method private free_oid (oid : Extension.oid) : unit =
	  oids#update (fun oids -> Some (Intset.remove oid oids))

	method all_oids ~(obs : observer) =
	  oids#contents ~obs

	method nb_oids ~(obs : observer) =
	  Intset.cardinal (oids#contents ~obs)

	method get_name (oid : Extension.oid) : Name.t =
	  try Hashtbl.find h_name oid
	  with Not_found -> Name.of_oid oid

	method compare_name (n1 : Name.t) (n2 : Name.t) : int =
	  match n1, n2 with
	  | Rdf.URI _, Rdf.URI _ -> Pervasives.compare (self#get_pagerank n2) (self#get_pagerank n1)
	  | Rdf.URI _, _ -> -1
	  | _, Rdf.URI _ -> 1
	  | Rdf.Literal l1, Rdf.Literal l2 -> Name.compare_literal l1 l2
	  | _ -> Pervasives.compare n1 n2

	method new_resource : Name.t = Common.prof "Store.store#new_resource" (fun () ->
	  let oid = self#alloc_oid in
	  let name = Name.of_oid oid in
	  Hashtbl.add h_entity name oid;
	  Hashtbl.add h_description oid (make_description self oid);
(*	  self#rdfs_Resource#add name; *)
	  name)

	method private make_name r : unit = Common.prof "Store.store#make_name" (fun () ->
	  match r with
	  | Rdf.URI uri -> ()
	  | Rdf.Blank s -> ()
	  | Rdf.Literal (s, dt) ->
(*	      self#rdfs_Literal#add r; *)
	      let dt_uri =
		match dt with
		  | Rdf.Plain _ -> Rdf.uri_langString
		  | Rdf.Typed uri -> uri in
	      let cl_datatype = self#get_class dt_uri in
	      let dt_name = Rdf.URI dt_uri in
	      self#rdfs_Datatype#add dt_name;
	      cl_datatype#add r;
	      store_integer#add r;
	      store_double#add r;
	      store_date#add r;
	      store_time#add r;
	      store_match#add r
	  | Rdf.XMLLiteral xml ->
	      self#rdf_XMLLiteral#add r)

	method has_name (name : Name.t) : bool =
	  Hashtbl.mem h_entity name

	method get_entity (name : Name.t) : Extension.oid =
	  try Hashtbl.find h_entity name
	  with Not_found -> Common.prof "Store.store#get_entity/new" (fun () ->
	    let oid = self#alloc_oid in
	    Hashtbl.add h_name oid name;
	    Hashtbl.add h_entity name oid;
	    Hashtbl.add h_description oid (make_description self oid);
	    self#make_name name;
	    oid)

	method remove_entity (name : Name.t) : unit =
(*
	  let is_predicate =
	    match name with
	      | Rdf.URI uri ->
		Hashtbl.mem h_class uri ||
		  Hashtbl.mem h_property uri ||
		  Hashtbl.fold (fun (f,ar) _ res -> res || f = uri) h_functor false
	      | _ -> false in
	  if is_predicate
	  then failwith "Cannot remove this entity because it is used as a class, property or functor"
	  else
*)
	    try
	      let oid = Hashtbl.find h_entity name in
	      (* removing triples and structures involving 'name' *)
	      let d = Hashtbl.find h_description oid in
	      d#classes#iter_as_uris
		(fun uri -> (self#get_class uri)#remove name);
	      d#fwd_properties#iter_as_uris
		(fun uri ->
		  let pr = self#get_property uri in
		  Intset.iter
		    (fun o -> pr#remove name (self#get_name o))
		    (pr#explicit_relation#objects ~obs:Tarpit.blind_observer name));
	      d#bwd_properties#iter_as_uris
		(fun uri ->
		  let pr = self#get_property uri in
		  Intset.iter
		    (fun s -> pr#remove (self#get_name s) name)
		    (pr#explicit_relation#subjects ~obs:Tarpit.blind_observer name));
	      d#structs#iter_as_uris
		(fun uri ar pos ->
		  let ff = self#get_functor uri ar in
		  ff#explicit_relation#iter
		    (fun nx nargs ->
		      if (pos=0 && nx=name) || List.nth nargs (pos-1) = name
		      then ff#remove nx nargs));
	      Hashtbl.remove h_description oid;
	      (* removing 'name' from secondary stores *)
	      store_integer#remove name;
	      store_double#remove name;
	      store_date#remove name;
	      store_time#remove name;
	      store_match#remove name;
	      (* removing the entity itself *)
	      Hashtbl.remove h_name oid;
	      Hashtbl.remove h_entity name;
	      self#free_oid oid
	    with Not_found -> ()

	method copy_entity (name : Name.t) (name2 : Name.t) : unit =
	  let oid = self#get_entity name in
	  let d = self#get_description oid in
	  d#classes#iter_as_uris
	    (fun uri -> (self#get_class uri)#add name2);
	  d#fwd_properties#iter_as_uris
	    (fun uri ->
	      if uri <> Rdfs.uri_label (* otherwise, new entity gets the label of the original one *)
	      then
		let pr = self#get_property uri in
		Intset.iter
		  (fun o -> pr#add name2 (self#get_name o))
		  (pr#explicit_relation#objects ~obs:Tarpit.blind_observer name));
	  d#bwd_properties#iter_as_uris
	    (fun uri ->
	      let pr = self#get_property uri in
	      Intset.iter
		(fun s -> pr#add (self#get_name s) name2)
		(pr#explicit_relation#subjects ~obs:Tarpit.blind_observer name));
	  d#structs#iter_as_uris
	    (fun uri ar pos ->
	      let ff = self#get_functor uri ar in
	      ff#explicit_relation#iter
		(fun nx nargs ->
		  if (pos=0 && nx=name) then ff#add name2 nargs
		  else if List.nth nargs (pos-1) = name then ff#add nx (Common.list_set_nth nargs (pos-1) name2)
		  else ()))

	method move_entity (name : Name.t) (name2 : Name.t) : unit =
	  self#copy_entity name name2;
	  self#remove_entity name

	method get_description oid =
	  try Hashtbl.find h_description oid
	  with _ -> make_description self oid (*assert false*)

	method entity_types (name : Name.t) : Uri.t list =
	  let oid = self#get_entity name in
	  let d = self#get_description oid in
	  Intset.fold (fun lc oc ->
	    match self#get_name oc with
	      | Rdf.URI c -> c::lc
	      | _ -> lc)
	    [] d#classes#set


	method get_class_prim (r : Uri.t) (fc : unit -> classe) : classe =
	  try Hashtbl.find h_class r
	  with Not_found ->
	    let c = fc () in
	    Hashtbl.add h_class r (c :> classe);
	    self#rdfs_Class#add c#name; (* r is a Class *)
	    self#make_name c#name;
	    c

	method get_class : Uri.t -> classe =
	  fun r ->
	    self#get_class_prim r (fun () -> (new expr_class self r :> classe))
	    
	method has_class : obs:observer -> Uri.t -> bool =
	  fun ~obs r ->
	    self#rdfs_Class#relation#has_instance ~obs (Rdf.URI r)

	method remove_class (uri : Uri.t) : unit =
	  try
	    let cl = Hashtbl.find h_class uri in
	    cl#explicit_relation#iter cl#remove;
	    Hashtbl.remove h_class uri;
	    self#remove_entity (Rdf.URI uri)
	  with Not_found -> failwith "The class cannot be removed. It does not exists."

	method copy_class (uri : Uri.t) (uri2 : Uri.t) : unit =
	  try
	    let cl = Hashtbl.find h_class uri in
	    let cl2 = self#get_class uri2 in
	    cl#explicit_relation#iter cl2#add
	  with Not_found -> failwith "The class cannot be copied. It does not exists."

	method move_class (uri : Uri.t) (uri2 : Uri.t) : unit =
	  self#copy_class uri uri2;
	  self#remove_class uri

	method xsd_uri = self#get_class Xsd.uri_anyURI
	method xsd_lang = self#get_class Xsd.uri_language
	method xsd_string = self#get_class Xsd.uri_string
	method xsd_integer = self#get_class Xsd.uri_integer
	method rdf_Property = self#get_class Rdf.uri_Property
	method rdf_Statement = self#get_class Rdf.uri_Statement
	method rdf_langString = self#get_class Rdf.uri_langString
	method rdf_XMLLiteral = self#get_class Rdf.uri_XMLLiteral
	method rdf_Bag = self#get_class Rdf.uri_Bag
	method rdf_Seq = self#get_class Rdf.uri_Seq
	method rdf_Alt = self#get_class Rdf.uri_Alt
	method rdf_List = self#get_class Rdf.uri_List
	method rdfs_Resource = self#get_class Rdfs.uri_Resource
	method rdfs_Class = self#get_class Rdfs.uri_Class
	method rdfs_Literal = self#get_class Rdfs.uri_Literal
	method rdfs_Datatype = self#get_class Rdfs.uri_Datatype
	method rdfs_Container = self#get_class Rdfs.uri_Container
	method rdfs_ContainerMembershipProperty = self#get_class Rdfs.uri_ContainerMembershipProperty
	method owl_TransitiveProperty = self#get_class Owl.uri_TransitiveProperty
	method owl_ReflexiveProperty = self#get_class Owl.uri_ReflexiveProperty
	method owl_SymmetricProperty = self#get_class Owl.uri_SymmetricProperty
	method owl_FunctionalProperty = self#get_class Owl.uri_FunctionalProperty
	method owl_InverseFunctionalProperty = self#get_class Owl.uri_InverseFunctionalProperty


	method private get_property_prim (r : Uri.t) (fp : unit -> property) (proc : Uri.t -> unit) : property =
	  try Hashtbl.find h_property r
	  with Not_found ->
	    let p = fp () in
	    Hashtbl.add h_property r p;
	    (*self#rdf_Property#add p#name; (* TODO: make it implicit rather than explicit *) *)
	    self#make_name p#name;
	    proc r;
	    p

	method private get_property_aux
	    ?(subproperties = false)
	    ?(equivalents = false)
	    ?transitive
	    ?reflexive
	    ?symmetric
	    ?functional
	    ?inverse_functional
	    r =
	  self#get_property_prim r
	    (fun () ->
	      (new expr_property
		 ~subproperties ~equivalents ?transitive ?reflexive ?symmetric ?functional ?inverse_functional
		 self r :> property))
	    (fun r ->
	      let n = Rdf.URI r in
	      self#rdf_Property#add n;
	      ())
	    
	method private get_rdf_n r =
	  self#get_property_prim r
	    (fun () ->
	      (new expr_property
		 ~subproperties:false ~equivalents:false
		 ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
		 self r :> property))
	    (fun r ->
	      let n = Rdf.URI r in
	      self#add_axiom_triple n Rdf.uri_type (Rdf.URI Rdfs.uri_ContainerMembershipProperty);
	      self#add_axiom_triple n Rdfs.uri_domain (Rdf.URI Rdfs.uri_Container);
	      (* self#rdfs_ContainerMembershipProperty#add n;
		 self#rdfs_domain#add n Name.rdfs_Container; *)
	      ())

	method rdf_type = self#get_property_prim
	    Rdf.uri_type 
	    (fun () -> (new expr_type self :> property)) (fun r -> ())
	method rdf_subject = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
	    Rdf.uri_subject
	method rdf_predicate = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
	    Rdf.uri_predicate
	method rdf_object = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
	    Rdf.uri_object
	method rdf_first = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
	    Rdf.uri_first
	method rdf_rest = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:true ~inverse_functional:false
	    Rdf.uri_rest
	method rdf_value = self#get_property_aux
	    ~subproperties:true ~equivalents:true 
	    ~transitive:false ~reflexive:false ~symmetric:false 
	    Rdf.uri_value
	method rdf_n n = self#get_rdf_n
	    (Rdf.uri_n n)
	method rdfs_subPropertyOf = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:true ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false
	    Rdfs.uri_subPropertyOf
	method rdfs_subClassOf = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:true ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false
	    Rdfs.uri_subClassOf
	method rdfs_domain = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false
	    Rdfs.uri_domain
	method rdfs_range = self#get_property_aux 
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false
	    Rdfs.uri_range
(*
	method rdfc_self = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false
	    Name.rdfc_self
*)
	method rdfs_member = self#get_property_aux 
	    ~subproperties:true ~equivalents:true
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false 
	    Rdfs.uri_member
	method owl_inverseOf = self#get_property_aux
	    ~subproperties:false ~equivalents:false
	    ~transitive:false ~reflexive:false ~symmetric:true ~functional:true ~inverse_functional:true
	    Owl.uri_inverseOf
	method rdfs_label = self#get_property_aux 
	    ~subproperties:true ~equivalents:true
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false 
	    Rdfs.uri_label
	method rdfs_comment = self#get_property_aux 
	    ~subproperties:true ~equivalents:true
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false 
	    Rdfs.uri_comment
	method rdfs_seeAlso = self#get_property_aux 
	    ~subproperties:true ~equivalents:true
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false 
	    Rdfs.uri_seeAlso
	method rdfs_isDefinedBy = self#get_property_aux 
	    ~subproperties:true ~equivalents:true
	    ~transitive:false ~reflexive:false ~symmetric:false ~functional:false ~inverse_functional:false 
	    Rdfs.uri_isDefinedBy

	method add_triple : Name.t -> Uri.t -> Name.t -> unit =
	  fun s p o ->
	    (self#get_property p)#add s o

	method add_axiom_triple : Name.t -> Uri.t -> Name.t -> unit =
	  fun s p o ->
	    axiom_triples <- {Rdf.s=s; Rdf.p=p; Rdf.o=o; Rdf.t_opt=None} :: axiom_triples;
	    self#add_triple s p o
	      
	method get_property : Uri.t -> property =
	  fun r ->
(* TODO : if in RDF(S)/OWL vocabulary call self methods... *)
	    if Rdf.is_uri_n r
	    then self#get_rdf_n r
	    else self#get_property_aux ~subproperties:true ~equivalents:true r

	method has_property : obs:observer -> Uri.t -> bool =
	  fun ~obs r ->
	    self#rdf_Property#relation#has_instance ~obs (Rdf.URI r)

	method remove_property (uri : Uri.t) : unit =
	  try
	    let pr = Hashtbl.find h_property uri in
	    pr#explicit_relation#iter pr#remove;
	    Hashtbl.remove h_property uri;
	    self#remove_entity (Rdf.URI uri)
	  with Not_found -> failwith "The property cannot be removed. It does not exists."

	method copy_property (uri : Uri.t) (uri2 : Uri.t) : unit =
	  try
	    let pr = Hashtbl.find h_property uri in
	    let pr2 = self#get_property uri2 in
	    pr#explicit_relation#iter pr2#add
	  with Not_found -> failwith "The property cannot be copied. It does not exists."

	method move_property (uri : Uri.t) (uri2 : Uri.t) : unit =
	  self#copy_property uri uri2;
	  self#remove_property uri

	method get_functor : Uri.t -> int -> funct =
	  fun r a ->
	    try Hashtbl.find h_functor (r,a)
	    with Not_found ->
	      let f = (new expr_functor self r a :> funct) in
	      Hashtbl.add h_functor (r,a) f;
	      let c_Functor = self#get_class Term.uri_Functor in
	      c_Functor#add f#name;
	      self#make_name f#name;
	      f

	method remove_functor (uri : Uri.t) (arity : int) : unit =
	  try
	    let ff = Hashtbl.find h_functor (uri,arity) in
	    ff#explicit_relation#iter ff#remove;
	    Hashtbl.remove h_functor (uri,arity);
	    self#remove_entity (Rdf.URI uri)
	  with Not_found -> failwith "The functor cannot be removed. It does not exists."

	method copy_functor (uri : Uri.t) (arity : int) (uri2 : Uri.t) : unit =
	  try
	    let ff = Hashtbl.find h_functor (uri,arity) in
	    let ff2 = self#get_functor uri2 arity in
	    ff#explicit_relation#iter ff2#add
	  with Not_found -> failwith "The functor cannot be copied. It does not exists."

	method move_functor (uri : Uri.t) (arity : int) (uri2 : Uri.t) : unit =
	  self#copy_functor uri arity uri2;
	  self#remove_functor uri arity

	method get_primitive : string -> int -> primitive =
	  fun id a ->
	    try Hashtbl.find h_primitive (id,a)
	    with Not_found -> failwith ("Store.store#get_primitive: unknown primitive " ^ id ^ "/" ^ string_of_int a)

	method define_primitive : primitive -> unit =
	  fun prim ->
	    Hashtbl.add h_primitive (prim#id,prim#arity) prim

	method get_procedure : string -> int -> procedure =
	  fun id a ->
	    try Hashtbl.find h_procedure (id,a)
	    with Not_found -> failwith ("Store.store#get_procedure: unknown procedure " ^ id ^ "/" ^ string_of_int a)

	method define_procedure : procedure -> unit =
	  fun proc ->
	    Hashtbl.add h_procedure (proc#id,proc#arity) proc

(*
	method leq_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y ->
	    Extension.union (store_integer#leq_extension ~obs x y)
	      (Extension.union (store_double#leq_extension ~obs x y)
		 (Extension.union (store_date#leq_extension ~obs x y)
		    (store_time#leq_extension ~obs x y)))

	method lt_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y ->
	    Extension.union (store_integer#lt_extension ~obs x y)
	      (Extension.union (store_double#lt_extension ~obs x y)
		 (Extension.union (store_date#lt_extension ~obs x y)
		    (store_time#lt_extension ~obs x y)))

	method matches_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y ->
	    store_match#matches_extension ~obs x y

	method contains_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y ->
	    store_match#contains_extension ~obs x y
*)

        (* Import functions *)

	method import_triple_iteration iterator = Common.prof "Store.store#import_triple_iteration" (fun () ->
	  let open Rdf in
	  let norm =
	    let h = Hashtbl.create 13 in
	    function
	      | Blank "" -> self#new_resource
	      | Blank id ->
		  begin
		    try Hashtbl.find h id
		    with Not_found ->
		      let r = self#new_resource in
		      Hashtbl.add h id r;
		      r
		  end
	      | n -> n
	  in
	  let cpt = ref 0 in
	  iterator
	    (fun {s; p; o; t_opt} ->
	      incr cpt;
	      if !cpt mod 100000 = 0 then begin print_int !cpt; print_endline " triples loaded" end;
	      match t_opt with
	      | None ->
		  (self#get_property p)#add (norm s) (norm o)
	      | Some t ->
		  (self#get_property Rdf.uri_subject)#add (norm t) (norm s);
		  (self#get_property Rdf.uri_predicate)#add (norm t) (norm (Rdf.URI p));
		  (self#get_property Rdf.uri_object)#add (norm t) (norm o)))

	method import_triples triples = Common.prof "Store.store#import_triples" (fun () ->
	  self#import_triple_iteration (fun f -> List.iter f triples))

	method import_rdf_xml ~base (rdf_file : string) : unit =
	  Common.prof "Store.store#import_rdf_xml" (fun () ->
	    print_endline "Processing triples RDF/XML file";
	    try
	      self#import_triple_iteration
		(fun f -> Rdf.iter_triples f base rdf_file)
	    with exn -> prerr_endline (Printexc.to_string exn))

(*
	method import_rdf_xml ~base (rdf_file : string) : unit =
	  Common.prof "Store.store#import_rdf_xml" (fun () ->
	    try
	      print_endline "Parsing RDF/XML...";
	      let xml = Xml.parse_file rdf_file in
	      let rdf = Rdf.from_xml base xml in
	      let triples = Rdf.triples_of_rdf rdf in
	      print_endline "Importing RDF...";
	      self#import_triples triples
	    with
	    | Xml.Error e -> prerr_endline (Xml.error e)
	    | Dtd.Parse_error e -> prerr_endline (Dtd.parse_error e)
	    | Dtd.Check_error e -> prerr_endline (Dtd.check_error e)
	    | Dtd.Prove_error e -> prerr_endline (Dtd.prove_error e))
*)

	method import_ntriples (nt_file : string) : unit =
	  Common.prof "Store.store#import_ntriples" (fun () ->
	    let triples = Ntriples.parse_file nt_file in
	    self#import_triples triples)

	method import_turtle ~base (ttl_file : string) : unit =
	  Common.prof "Store.store#import_turtle" (fun () ->
	    let turtle = Turtle.parse_file ttl_file in
	    let triples = Turtle.triples_of_turtle base turtle in
	    self#import_triples triples)

	method import_rdf_file ~base (filename : string) : unit =
	  Common.prof "Store.store#import_rdf_file" (fun () ->
	    if
	      Filename.check_suffix filename ".rdf" ||
	      Filename.check_suffix filename ".owl" ||
	      Filename.check_suffix filename ".xml"
	    then
	      self#import_rdf_xml ~base filename
	    else if Filename.check_suffix filename ".nt" then
	      self#import_ntriples filename
	    else if Filename.check_suffix filename ".ttl" then
	      self#import_turtle ~base filename
	    else
	      invalid_arg "Store.store#import_rdf_file: unrecognized format";
	    self#pagerank_iterations 5)

        (* Export functions *)

	method export_triples output_triple =
	  self#iter_triples (fun triple ->
	    if not (self#is_axiom_triple triple) (* only non axiomatic triples are exported *)
	    then output_triple triple)

	method export_ntriples nt_file = Common.prof "Store.store#export_ntriples" (fun () ->
	  let out = open_out nt_file in
	  let fmt = Format.formatter_of_out_channel out in
	  self#export_triples
	    (fun triple -> Ntriples.print_triple fmt triple);
	  Format.pp_print_flush fmt ();
	  close_out out)

	method export_turtle ~base ~xmlns ttl_file = Common.prof "Store.store#export_turtle" (fun () ->
	  let out = open_out ttl_file in
	  List.iter
	    (fun (pre,uri) -> Turtle.output out [Turtle.Prefix (pre, Turtle.Absolute uri)])
	    xmlns;
	  self#export_triples
	    (fun triple -> Turtle.output out [Turtle.turtle_of_triple base xmlns triple]);
(*
	  let cursor = Printer.cursor_of_formatter (Format.formatter_of_out_channel out) in
	  List.iter
	    (fun (pre,uri) ->
	      Ipp.once Turtle.print_statement (Turtle.Prefix (pre, Turtle.Absolute uri)) cursor ())
	    xmlns;
	  self#export_triples
	    (fun triple ->
	      Ipp.once Turtle.print_statement (Turtle.turtle_of_triple base xmlns triple) cursor ());
*)
	  close_out out)

	method export_rdf_file ~(base : Uri.t) ~(xmlns : Rdf.xmlns) (filename : string) : unit =
	  if Filename.check_suffix filename ".nt" then
	    self#export_ntriples filename
	  else if Filename.check_suffix filename ".ttl" then
	    self#export_turtle ~base ~xmlns filename
	  else invalid_arg "Store.store#export_rdf_file: unsupported format"

        (* PageRank *)

	method get_pagerank (n : Name.t) : float =
	  (self#get_description (self#get_entity n))#rank

	method pagerank_iterations n = Common.prof "Store.store#pagerank_iterations" (fun () ->
	  print_endline "Performing PageRank iterations";
	  for i = 1 to n do
	    Hashtbl.iter
	      (fun oid page ->
		let sum =
		  Intset.fold
		    (fun res pred ->
		      let page_pred = self#get_description pred in
		      res +. page_pred#rank /. float page_pred#nb_succs)
		    0.
		    page#preds in
		page#set_rank (0.15 +. 0.85 *. sum))
	      h_description
	  done)


	method init =
(* MUST create base RDF/RDFS classes/properties to avoid dependency cycle (Tarpit) *)
print_endline "Creating core properties and classes...";
	  let res = self#rdfs_Resource in
	  let lit = self#rdfs_Literal in
	  let prop = self#rdf_Property in
	  let cl = self#rdfs_Class in
print_endline "Creating primitive properties...";
	  let subj = self#rdf_subject in
	  let obj = self#rdf_object in
	  let pred = self#rdf_predicate in
	  let typ = self#rdf_type in
	  let first = self#rdf_first in
	  let rest = self#rdf_rest in
	  let valu = self#rdf_value in
	  let sub_prop = self#rdfs_subPropertyOf in
	  let sub_cl = self#rdfs_subClassOf in
	  let dom = self#rdfs_domain in
	  let ran = self#rdfs_range in
(*	  let slf = self#rdfc_self in *)
	  let mem = self#rdfs_member in
	  let label = self#rdfs_label in
	  let comment = self#rdfs_comment in
	  let seeAlso = self#rdfs_seeAlso in
	  let isDefinedBy = self#rdfs_isDefinedBy in
	  let inv = self#owl_inverseOf in
print_endline "Creating primitive classes...";
	  let dt_uri = self#xsd_uri in
	  let dt_lang = self#xsd_lang in
	  let dt_string = self#xsd_string in
	  let dt_integer = self#xsd_integer in
	  let stat = self#rdf_Statement in
	  let bag = self#rdf_Bag in
	  let seq = self#rdf_Seq in
	  let alt = self#rdf_Alt in
	  let list = self#rdf_List in
	  let plain = self#rdf_langString in
	  let xml = self#rdf_XMLLiteral in
	  let data = self#rdfs_Datatype in
	  let cont = self#rdfs_Container in
	  let memprop = self#rdfs_ContainerMembershipProperty in
	  let trans = self#owl_TransitiveProperty in
	  let sym = self#owl_SymmetricProperty in
	  let refl = self#owl_ReflexiveProperty in
	  let func = self#owl_FunctionalProperty in
	  let inv_func = self#owl_InverseFunctionalProperty in
	  
	  let triples lpos =
	    List.rev (
	      List.fold_left (fun acc (p,los) ->
		List.fold_left (fun acc (o_uri,ls) ->
		  let o = Rdf.URI o_uri in
		  List.fold_left (fun acc s_uri ->
		    let s = Rdf.URI s_uri in
		    (s,p,o) :: acc
		  ) acc ls
		) acc los
	      ) [] lpos
	    ) in
	  let axiom_triples = triples
	    [Rdf.uri_type, [Rdfs.uri_Datatype, [Xsd.uri_anyURI; Xsd.uri_language; Xsd.uri_string; Xsd.uri_integer;
						Xsd.uri_gYear; Xsd.uri_gMonth; Xsd.uri_gDay;
						Rdf.uri_XMLLiteral; Rdf.uri_langString];
			    Rdfs.uri_Class, [Xsd.uri_anyURI; Xsd.uri_language; Xsd.uri_string; Xsd.uri_integer;
					     Xsd.uri_gYear; Xsd.uri_gMonth; Xsd.uri_gDay;
					     Rdf.uri_XMLLiteral; Rdf.uri_langString;
					     Rdf.uri_Property; Rdf.uri_Statement; Rdf.uri_Bag; Rdf.uri_Seq; Rdf.uri_Alt; Rdf.uri_List;
					     Rdfs.uri_Resource; Rdfs.uri_Class; Rdfs.uri_Literal; Rdfs.uri_Datatype; Rdfs.uri_Container; Rdfs.uri_ContainerMembershipProperty;
					     Owl.uri_TransitiveProperty; Owl.uri_SymmetricProperty; Owl.uri_ReflexiveProperty; Owl.uri_FunctionalProperty; Owl.uri_InverseFunctionalProperty];
			    Rdf.uri_Property, [Rdf.uri_subject; Rdf.uri_predicate; Rdf.uri_object; Rdf.uri_type; Rdf.uri_first; Rdf.uri_rest; Rdf.uri_value;
					       Rdfs.uri_subClassOf; Rdfs.uri_subPropertyOf; Rdfs.uri_domain; Rdfs.uri_range; Rdfs.uri_member;
					       Rdfs.uri_label; Rdfs.uri_comment; Rdfs.uri_seeAlso; Rdfs.uri_isDefinedBy;
					       Owl.uri_inverseOf];
			    Owl.uri_TransitiveProperty, [Rdfs.uri_subClassOf; Rdfs.uri_subPropertyOf];
			    Owl.uri_ReflexiveProperty, [Rdfs.uri_subClassOf; Rdfs.uri_subPropertyOf];
			    Owl.uri_SymmetricProperty, [Owl.uri_inverseOf];
			    Owl.uri_FunctionalProperty, [Rdf.uri_subject; Rdf.uri_predicate; Rdf.uri_object; Rdf.uri_first; Rdf.uri_rest];
			    Rdfs.uri_ContainerMembershipProperty, [Rdfs.uri_member];
			    Rdf.uri_List, [Rdf.uri_nil]
			   ];
	     Rdfs.uri_subClassOf, [Rdfs.uri_Container, [Rdf.uri_Bag; Rdf.uri_Seq; Rdf.uri_Alt];
				   Rdfs.uri_Class, [Rdfs.uri_Datatype];
				   Xsd.uri_string, [Rdf.uri_langString; Xsd.uri_anyURI; Xsd.uri_language];
				   Xsd.uri_integer, [Xsd.uri_gYear; Xsd.uri_gMonth; Xsd.uri_gDay];
				   Rdf.uri_Property, [Rdfs.uri_ContainerMembershipProperty;
						      Owl.uri_TransitiveProperty; Owl.uri_SymmetricProperty; Owl.uri_ReflexiveProperty;
						      Owl.uri_FunctionalProperty; Owl.uri_InverseFunctionalProperty]
				  ];
	     Rdfs.uri_domain, [Rdf.uri_Statement, [Rdf.uri_subject; Rdf.uri_predicate; Rdf.uri_object];
			       Rdf.uri_List, [Rdf.uri_first; Rdf.uri_rest];
			       Rdf.uri_Property, [Rdfs.uri_subPropertyOf; Rdfs.uri_domain; Rdfs.uri_range; Owl.uri_inverseOf];
			       Rdfs.uri_Class, [Rdfs.uri_subClassOf];
			       Rdfs.uri_Container, [Rdfs.uri_member]
			      ];
	     Rdfs.uri_range, [Rdf.uri_Property, [Rdf.uri_predicate; Rdfs.uri_subPropertyOf; Owl.uri_inverseOf];
			      Rdfs.uri_Class, [Rdf.uri_type; Rdfs.uri_subClassOf; Rdfs.uri_domain; Rdfs.uri_range];
			      Rdf.uri_List, [Rdf.uri_rest];
			      Rdfs.uri_Literal, [Rdfs.uri_label]
			     ];
	     
	    ] in
	  List.iter (fun (s,p,o) -> self#add_axiom_triple s p o) axiom_triples;

	  begin
	    let comp = Builtins.comps_union
	      [ Builtins_basic.string_comp;
		Builtins_basic.numeric_comp;
		Builtins_basic.boolean_comp;
		Builtins_temporal.duration_comp;
		Builtins_temporal.temporal_comp ] in
	    let leq_pred = object
	      inherit Builtins.pred
	      method arity = comp#arity
	      method types = comp#types
	      method eval args = List.exists (fun c -> c <= 0) (comp#eval args) end in
	    let lt_pred = object
	      inherit Builtins.pred
	      method arity = comp#arity
	      method types = comp#types
	      method eval args = List.exists (fun c -> c < 0) (comp#eval args) end in
	    let between_pred = object
	      inherit Builtins.pred
	      method arity = 3
	      method types = List.map (function [t1;t2] -> [t1;t2;t2] | _ -> assert false) comp#types
	      method eval = function
		| [x;y1;y2] -> List.exists (fun c -> c >= 0) (comp#eval [x;y1]) && List.exists (fun c -> c <= 0) (comp#eval [x;y2])
		| _ -> assert false end in
	    let min_func = object
	      inherit Builtins.func
	      method arity = 2
	      method types = List.fold_left (fun res -> function [t1;t2] -> [t1;t1;t2]::[t2;t1;t2]::res | _ -> res) [] comp#types
	      method eval = function
		| [x;y] -> if List.exists (fun c -> c <= 0) (comp#eval [x;y]) then [x] else [y]
		| _ -> assert false end in
	    let max_func = object
	      inherit Builtins.func
	      method arity = 2
	      method types = List.fold_left (fun res -> function [t1;t2] -> [t1;t1;t2]::[t2;t1;t2]::res | _ -> res) [] comp#types
	      method eval = function
		| [x;y] -> if List.exists (fun c -> c <= 0) (comp#eval [x;y]) then [y] else [x]
		| _ -> assert false end in

	    let def_pred id syntax pred = self#define_primitive (new expr_pred id syntax pred) in
	    let def_func id syntax func = self#define_primitive (new expr_func id syntax func) in
	    let def_proc id syntax proc = self#define_procedure (new expr_proc id syntax proc) in
	    def_pred "leq" (PredSyntax.verb "\226\137\164" "\226\137\165") leq_pred; (* includes geq *)
	    def_pred "lt" (PredSyntax.verb "<" ">") lt_pred; (* includes gt *)
	    def_pred "between" (PredSyntax.verb "is between" "" ~preps:["and"]) between_pred;
	    def_func "min" (PredSyntax.func "min" 2) min_func;
	    def_func "max" (PredSyntax.func "max" 2) max_func;

	    def_func "plus" (PredSyntax.binop "+")
	      (Builtins.funcs_union
		 [ Builtins_basic.plus_func;
		   Builtins_basic.concat_func;
		   Builtins_temporal.duration_plus_func;
		   Builtins_temporal.temporal_plus_duration_func ]);
	    def_func "minus" (PredSyntax.binop "-")
	      (Builtins.funcs_union
		 [ Builtins_basic.minus_func;
		   Builtins_temporal.duration_minus_func;
		   Builtins_temporal.temporal_minus_duration_func;
		   Builtins_temporal.temporal_minus_temporal_func ]);

	    def_pred "isLiteral" (PredSyntax.noun "literal") Builtins_basic.isLiteral_pred;
	    def_pred "isNumeric" (PredSyntax.noun "number") Builtins_basic.isNumeric_pred;
	    def_func "lang" (PredSyntax.relnoun1 "language") Builtins_basic.lang_func;
	    def_func "datatype" (PredSyntax.relnoun1 "datatype") Builtins_basic.datatype_func;
	    def_func "str" (PredSyntax.relnoun1 "string") Builtins_basic.str_func;
	    def_func "toNumeric" (PredSyntax.relnoun1 "number") Builtins_basic.toNumeric_func;
	    def_pred "strStarts" (PredSyntax.relnoun2 "prefix") Builtins_basic.strStarts_pred;
	    def_pred "strEnds" (PredSyntax.relnoun2 "suffix") Builtins_basic.strEnds_pred;
	    def_pred "contains" (PredSyntax.verb "contains" "is contained" ~prep2:"by") Builtins_basic.contains_pred;
	    def_pred "regex" (PredSyntax.verb "matches" "is matched" ~prep2:"by") Builtins_basic.regex_pred;
	    def_func "strlen" (PredSyntax.relnoun1 "length") Builtins_basic.strlen_func;
	    def_func "subsrt" (PredSyntax.relnoun1 "substring" ~preps:["at position"; "with length"]) Builtins_basic.substr_func;
	    def_func "ucase" (PredSyntax.relnoun1 "uppercase") Builtins_basic.ucase_func;
	    def_func "lcase" (PredSyntax.relnoun1 "lowercase") Builtins_basic.lcase_func;
	    def_func "capitalize" (PredSyntax.relnoun1 "capitalization") Builtins_basic.capitalize_func;
	    def_func "uncapitalize" (PredSyntax.relnoun1 "uncapitalization") Builtins_basic.uncapitalize_func;
	    def_func "split" (PredSyntax.relnoun1 "substring" ~preps:["delimited by"]) Builtins_basic.split_func;
	    def_func "times" (PredSyntax.binop "*") Builtins_basic.times_func;
	    def_func "div" (PredSyntax.binop "/") Builtins_basic.div_func;
	    def_func "mod" (PredSyntax.binop "modulo") Builtins_basic.modulo_func;
	    def_func "power" (PredSyntax.binop "^") Builtins_basic.power_func;
	    def_func "sqrt" (PredSyntax.func "sqrt" 1) Builtins_basic.sqrt_func;
	    def_func "exp" (PredSyntax.unop "e ^") Builtins_basic.exp_func;
	    def_func "log" (PredSyntax.unop "log") Builtins_basic.log_func;
	    def_func "log10" (PredSyntax.unop "log10") Builtins_basic.log10_func;
	    def_func "cos" (PredSyntax.unop "cos") Builtins_basic.cos_func;
	    def_func "sin" (PredSyntax.unop "sin") Builtins_basic.sin_func;
	    def_func "tan" (PredSyntax.unop "tan") Builtins_basic.tan_func;
	    def_func "uminus" (PredSyntax.unop "-") Builtins_basic.uminus_func;
	    def_func "abs" (PredSyntax.func "abs" 1) Builtins_basic.abs_func;
	    def_func "round" (PredSyntax.func "round" 1) Builtins_basic.round_func;
	    def_func "floor" (PredSyntax.func "floor" 1) Builtins_basic.floor_func;
	    def_func "ceil" (PredSyntax.func "ceil" 1) Builtins_basic.ceil_func;
	    def_func "round" (PredSyntax.func "round" 1) Builtins_basic.round_func;
	    def_func "and" (PredSyntax.binop "and") Builtins_basic.log_and_func;
	    def_func "or" (PredSyntax.binop "or") Builtins_basic.log_or_func;
	    def_func "not" (PredSyntax.unop "not") Builtins_basic.log_not_func;
	    def_proc "echo" (ProcSyntax.verb "echo") Builtins_basic.echo;
	    def_proc "exec" (ProcSyntax.verb "execute") Builtins_basic.exec;
	    def_proc "email" (ProcSyntax.verb "email" ~prep:"to" ~preps:["cc"; "subject"; "body"]) Builtins_basic.thunderbird_compose;

	    def_func "makeTime" (PredSyntax.func "new time" 3) Builtins_temporal.makeTime_func;
	    def_func "makeDate" (PredSyntax.func "new date" 3) Builtins_temporal.makeDate_func;
	    def_func "makeDateTime" (PredSyntax.func "new dateTime" 2) Builtins_temporal.makeDateTime_func;
	    def_func "makePeriod" (PredSyntax.func "new period" 2) Builtins_temporal.makePeriod_func;
	    def_func "today" (PredSyntax.func "today" 0) Builtins_temporal.today_func;
	    def_func "now" (PredSyntax.func "now" 0) Builtins_temporal.now_func;
	    def_func "totalSeconds" (PredSyntax.relnoun1 "total seconds") Builtins_temporal.duration_totalSeconds_func;
	    def_func "totalHours" (PredSyntax.relnoun1 "total hours") Builtins_temporal.duration_totalHours_func;
	    def_func "totalDays" (PredSyntax.relnoun1 "total days") Builtins_temporal.duration_totalDays_func;
	    def_func "year" (PredSyntax.relnoun1 "year") Builtins_temporal.year_func;
	    def_func "month" (PredSyntax.relnoun1 "month") Builtins_temporal.month_func;
	    def_func "day" (PredSyntax.relnoun1 "day") Builtins_temporal.day_func;
	    def_func "weekday" (PredSyntax.relnoun1 "weekday") Builtins_temporal.weekday_func;
	    def_func "yearday" (PredSyntax.relnoun1 "yearday") Builtins_temporal.yearday_func;
	    def_pred "duringDaylighSavings" (PredSyntax.noun "daylight saving day") Builtins_temporal.during_daylight_savings_pred;
	    def_func "hours" (PredSyntax.relnoun1 "hours") Builtins_temporal.hours_func;
	    def_func "minutes" (PredSyntax.relnoun1 "minutes") Builtins_temporal.minutes_func;
	    def_func "seconds" (PredSyntax.relnoun1 "seconds") Builtins_temporal.seconds_func;
	    def_func "periodStart" (PredSyntax.relnoun1 "start") Builtins_temporal.start_func;
	    def_func "periodEnd" (PredSyntax.relnoun1 "end") Builtins_temporal.end_func;
	    def_func "toTime" (PredSyntax.relnoun1 "time") Builtins_temporal.toTime_func;
	    def_func "toDate" (PredSyntax.relnoun1 "date") Builtins_temporal.toDate_func;
	    def_func "toYearMonth" (PredSyntax.relnoun1 "year-month") Builtins_temporal.toYearMonth_func;
	    def_func "toMonthDay" (PredSyntax.relnoun1 "month-day") Builtins_temporal.toMonthDay_func;
	    def_func "toPeriod" (PredSyntax.relnoun1 "period") Builtins_temporal.toPeriod_func;
	    def_func "periodDuration" (PredSyntax.relnoun1 "duration") Builtins_temporal.period_duration_func;
	    def_pred "intervalBeforeMeets" (PredSyntax.verb "is before" "is after")
	      (Builtins.preds_union [Builtins_temporal.before_pred; Builtins_temporal.meets_pred]);
	    def_pred "intervalMeets" (PredSyntax.verb "meets" "is met" ~prep2:"by")
	      Builtins_temporal.meets_pred;
	    def_pred "intervalOverlaps" (PredSyntax.verb "overlaps" "is overlaped" ~prep2:"by")
	      Builtins_temporal.overlaps_pred;
	    def_pred "intervalStarts" (PredSyntax.verb "starts" "is started" ~prep2:"by")
	      Builtins_temporal.starts_pred;
	    def_pred "intervalEnds" (PredSyntax.verb "ends" "is ended" ~prep2:"by")
	      Builtins_temporal.ends_pred;
	    def_pred "intervalDuringWide" (PredSyntax.verb "is contained" ~prep1:"by" "contains")
	      (Builtins.preds_union [Builtins_temporal.during_pred;
			       Builtins_temporal.starts_pred;
			       Builtins_temporal.ends_pred;
			       Builtins_temporal.equals_pred])
	  end;

print_endline "Done";
	  ()
      end

  end
