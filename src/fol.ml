(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Rel = Intreln.Intmap
module Name = Name.Make
module Ext = Extension.Make
module Code = Code.Make
module Store = Store.Make

module Make =
  struct

    type modifier =
      | Direct
      | Symmetric
      | Reflexive
      | Transitive

    type fol =
      | FBound of Ext.var
      | FBind of Ext.var * Name.t
      | FSame of Ext.var * Ext.var
      | FDiff of Ext.var * Ext.var
      | FClass of Uri.t * Ext.var (* cc, s *)
      | FProp of modifier list * Uri.t * Ext.var * Ext.var (* lm, pp, s, o *)
      | FFunct of Uri.t * Ext.var * Ext.var array (* ff, x, args *)
      | FPrim of string * Ext.var list (* prim, args *)
      | FRemove of Ext.var
      | FCopy of Ext.var * Ext.var
      | FMove of Ext.var * Ext.var
      | FProc of string * Ext.var list (* proc, args *)
      | FTrue
      | FAnd of fol * fol
      | FOr of fol * fol
      | FNot of fol
      | FMaybe of fol
      | FCond of fol * fol * fol option
      | FForThe of fol * fol
      | FForEach of fol * fol
      | FForEvery of fol * fol
      | FForNo of fol * fol
      | FForOnly of fol * fol


    let and_list lf =
      let lf = List.filter ((<>) FTrue) lf in
      let rec aux = function
	| [] -> FTrue
	| [f] -> f
	| f::lf -> FAnd (f, aux lf)
      in
      aux lf

    let rec or_list = function
      | [] -> FNot FTrue
      | [f] -> f
      | f::lf -> FOr (f, or_list lf)


    type var_set = Ext.var LSet.t

(* ---- intent: transforming a fol to make focus variables accessibles ---- *)

    let intent (prefix : string) (fol : fol) : var_set * fol =
      let re_prefix = Str.regexp_string prefix in (* prefix is the prefix of focus variables *)
      let matches_prefix s = Str.string_match re_prefix s 0 in
      let xs_empty = LSet.empty () in
      let rec aux fol : bool * var_set * fol = (* the var_set is the set of variables starting with [prefix], and the bool tells whether this var_set is non-empty *)
	match fol with
	  | FSame (v1,v2) -> (* assumes that the variable that matches the prefix does not occur elsewhere *)
	    if matches_prefix v2 then true, LSet.singleton v1, FTrue
	    else if matches_prefix v1 then true, LSet.singleton v2, FTrue
	    else false, LSet.empty (), fol
	  | FBound v -> aux_atom [v] fol
	  | FBind (v,n) -> aux_atom [v] fol
	  | FDiff (v1,v2) -> aux_atom [v1;v2] fol
	  | FClass (cc,s) -> aux_atom [s] fol
	  | FProp (lm,pp,s,o) -> aux_atom [s;o] fol
	  | FFunct (ff,x,args) -> aux_atom (x::Array.to_list args) fol
	  | FPrim (prim,args) -> aux_atom args fol
	  | FRemove v -> aux_atom [v] FTrue
	  | FCopy (v1,v2) -> aux_atom [v1;v2] FTrue
	  | FMove (v1,v2) -> aux_atom [v1;v2] FTrue
	  | FProc (proc,args) -> aux_atom args FTrue (* procedures are not part of intents, because have no extensions *)
	  | FTrue -> false, xs_empty, fol
	  | FAnd (f1,f2) ->
	    let b1, xs1, f1' = aux f1 in
	    let b2, xs2, f2' = aux f2 in
	    b1 || b2, LSet.union xs1 xs2, and_list [f1'; f2']
	  | FOr (f1,f2) ->
	    let b1, xs1, f1' = aux f1 in
	    let b2, xs2, f2' = aux f2 in
	    if b1 && b2 then true, LSet.union xs1 xs2, or_list [f1'; f2']
	    else if b1 then true, xs1, f1'
	    else if b2 then true, xs2, f2'
	    else false, xs_empty, or_list [f1'; f2']
	  | FNot f1 ->
	    let b1, xs1, f1' = aux f1 in
	    if b1 then true, xs1, f1'
	    else false, xs_empty, FNot f1'
	  | FMaybe f1 ->
	    let b1, xs1, f1' = aux f1 in
	    if b1 then true, xs1, f1'
	    else false, xs_empty, FMaybe f1'
	  | FCond (f0,f1,f2_opt) ->
	    let b0, xs0, f0' = aux f0 in
	    let b1, xs1, f1' = aux f1 in
	    let b2, xs2, f2_opt' =
	      match f2_opt with
		| None -> false, xs_empty, None
		| Some f2 ->
		  let b2, xs2, f2' = aux f2 in
		  b2, xs2, Some f2' in
	    if b0 then true, xs0, f0'
	    else if b1 && b2 then true, LSet.union xs1 xs2, FCond (f0',f1',f2_opt')
	    else if b1 then true, xs1, FCond (f0', f1',None)
	    else if b2 then true, xs2, FCond (f0', FNot FTrue, f2_opt')
	    else false, xs_empty, FCond (f0',f1',f2_opt')
	  | FForThe (f1,f2) -> aux (FAnd (f1,f2))
	  | FForEach (f1,f2) -> aux (FForEvery (f1,f2))
	  | FForEvery (f1,f2) ->
	    let b1, xs1, f1' = aux f1 in
	    let b2, xs2, f2' = aux f2 in
	    if b1 && not b2 then true, xs1, f1'
	    else if b1 || b2 then true, LSet.union xs1 xs2, and_list [f1'; f2']
	    else false, xs_empty, FForEvery (f1',f2')
	  | FForOnly (f1,f2) -> aux (FForEvery (f2,f1))
	  | FForNo (f1,f2) -> aux (FForEvery (f1, FNot f2)) (* (FNot (FAnd (f1,f2))) *)
      and aux_atom xs fol =
	let xs' = List.fold_left (fun res x -> if matches_prefix x then LSet.add x res else res) xs_empty xs in
	not (LSet.is_empty xs'), xs', fol
      in
      let b, xs, fol' = aux fol in
      xs, fol'

(* ---- Type inference of formulas ---- *)

    type var_type = [ `Var of string | `Class of Uri.t | `Xml ]
    type type_subst = (string * var_type) list

    let type_of_class gv c =
      if c = Rdfs.uri_Resource || c = Rdfs.uri_Literal
      then `Var gv#get
      else `Class c

    let string_of_type = function
      | `Var v -> v
      | `Class uri -> uri
      | `Xml -> "XML"

    let apply_subst (subst : type_subst) (a : var_type) : var_type =
      match a with
	| `Var v -> (try List.assoc v subst with Not_found -> a)
	| `Class _ -> a
	| `Xml -> a

    module TypingMonad =
    struct
      type 'a t = 'a list
      let return t = [t]
      let bind m k = List.fold_left (fun res t -> k t @ res) [] m
      let zero = []
      let mplus m1 m2 = m1 @ m2
    end

    class gen_var_type = object
      val mutable cpt = 0
      method get = cpt <- cpt+1; "T" ^ string_of_int cpt
    end

    let rec typing_fol ~obs store gv fol : (Ext.var * var_type) list TypingMonad.t =
      let open TypingMonad in
      (* gv : a generator of type variables *)
      match fol with
	| FBound x ->
	  let a = `Var gv#get in
	  return [(x,a)]
	| FBind (x,n) ->
	  let la : var_type list =
	    match n with
	      | Rdf.URI uri -> List.map (type_of_class gv) (store#entity_types n)
	      | Rdf.Blank name -> List.map (type_of_class gv) (store#entity_types n)
	      | Rdf.Literal (_, Rdf.Plain _) -> [`Class Xsd.uri_string]
	      | Rdf.Literal (_, Rdf.Typed uri) -> [`Class uri]
	      | Rdf.XMLLiteral _ -> [`Xml] in
	  if la = []
	  then return [(x, `Var gv#get)]
	  else bind la (fun a -> return [(x,a)])
	| FSame (x,y) ->
	  let a = `Var gv#get in
	  return [(x,a); (y,a)]
	| FDiff (x,y) ->
	  let a = `Var gv#get in
	  return [(x,a); (y,a)]
	| FClass (cc,s) ->
	  let lc = (store#get_class cc)#relation#types ~obs in
	  if lc = []
	  then return [(s, `Var gv#get)]
	  else bind lc (fun c1 -> return [(s, type_of_class gv c1)])
	| FProp (lm,pp,s,o) ->
	  let lcc = (store#get_property pp)#relation#types ~obs in
	  if lcc = []
	  then return [(s, `Var gv#get); (o, `Var gv#get)]
	  else bind lcc (fun (c1,c2) -> return [(s, type_of_class gv c1); (o, type_of_class gv c2)])
	| FFunct (ff,x,args) ->
	  let lcs = (store#get_functor ff (Array.length args))#relation#types ~obs in
	  if lcs = []
	  then return (List.map (fun x -> (x, `Var gv#get)) (x :: Array.to_list args))
	  else bind lcs (fun cs ->
	    return (List.combine (x::Array.to_list args) (List.map (type_of_class gv) cs)))
	| FPrim (prim,args) ->
	  let lcs = (store#get_primitive prim (List.length args))#types in
	  if lcs = []
	  then return (List.map (fun x -> (x, `Var gv#get)) args)
	  else bind lcs (fun cs ->
	    return (List.combine args (List.map (type_of_class gv) cs)))
	| FRemove v -> return [(v, `Var gv#get)]
	| FCopy (v1,v2) -> return [(v1, `Var gv#get); (v2, `Var gv#get)]
	| FMove (v1,v2) -> return [(v1, `Var gv#get); (v2, `Var gv#get)]
	| FProc (proc,args) ->
	  let lcs = (store#get_procedure proc (List.length args))#types in
	  if lcs = []
	  then return (List.map (fun x -> (x, `Var gv#get)) args)
	  else bind lcs (fun cs ->
	    return (List.combine args (List.map (type_of_class gv) cs)))
	| FTrue ->
	  return []
	| FAnd (f1,f2) ->
	  bind (typing_fol ~obs store gv f1) (fun t1 ->
	    bind (typing_fol ~obs store gv f2) (fun t2 ->
	      typing_join ~obs store gv t1 t2))
	| FOr (f1,f2) ->
	  mplus
	    (typing_fol ~obs store gv f1)
	    (typing_fol ~obs store gv f2)
	| FNot f1 ->
	  typing_fol ~obs store gv f1
	| FMaybe f1 ->
	  typing_fol ~obs store gv f1
	| FCond (f0,f1,f2_opt) ->
	  let f12 = match f2_opt with None -> f1 | Some f2 -> FOr (f1,f2) in
	  bind (typing_fol ~obs store gv f0) (fun t0 ->
	    bind (typing_fol ~obs store gv f12) (fun t12 ->
	      typing_join ~obs store gv t0 t12))
	| FForThe (f1,f2) -> typing_fol ~obs store gv (FAnd (f1,f2))
	| FForEach (f1,f2) -> typing_fol ~obs store gv (FAnd (f1,f2))
	| FForEvery (f1,f2) -> typing_fol ~obs store gv (FAnd (f1,f2))
	| FForNo (f1,f2) -> typing_fol ~obs store gv (FAnd (f1,f2))
	| FForOnly (f1,f2) -> typing_fol ~obs store gv (FAnd (f1,f2))
    and typing_join ~obs store gv ?(subst = []) t1 t2 =
	let open TypingMonad in
	match t2 with
	  | [] -> return t1
	  | (x,a)::t2' ->
	    let a = apply_subst subst a in
	    try
	      let b = List.assoc x t1 in
	      let b = apply_subst subst b in
	      bind (typing_unify ~obs store gv a b) (fun (s,c) ->
		let subst = List.map (fun (v,a) -> (v, apply_subst s a)) subst in
		let t1 = List.map (fun (x,a) -> (x, apply_subst s a)) t1 in
		typing_join ~obs store gv ~subst:(s@subst) ((x,c)::List.remove_assoc x t1) t2')
	    with Not_found ->
	      typing_join ~obs store gv ~subst ((x,a)::t1) t2'
    and typing_unify ~obs store gv a b =
      let open TypingMonad in
      match a, b with
	| `Var v1, `Var v2 ->
	  return ([(v1, `Var v2)], `Var v2)
	| `Var v, _ -> return ([(v,b)], b)
	| _, `Var v -> return ([(v,a)], a)
	| `Class c1, `Class c2 ->
	  if c1 = c2
	  then return ([], `Class c1)
	  else
	    let lc3 = store#rdfs_subClassOf#relation#greatest_common_predecessors ~obs (Rdf.URI c1) (Rdf.URI c2) in
	    bind lc3 (function
	      | Rdf.URI c3 -> prerr_string c3; prerr_string "\n"; return ([], `Class c3)
	      | _ -> zero)
	| `Xml, `Xml -> return ([], `Xml)
	| _ -> zero


    let typing_of_fol ~obs store (fol : fol) : (Ext.var * var_type) list list =
      let gv = new gen_var_type in
      typing_fol ~obs store gv fol


(* typing flattening *)

    let flatten_typing lt =
      List.fold_left
	(fun res t ->
	  List.fold_left
	    (fun res (x,a) ->
	      try
		let la = List.assoc x res in
		(x, LSet.add a la) :: List.remove_assoc x res
	      with Not_found ->
		(x, LSet.singleton a) :: res)
	    res
	    t)
	[]
	lt

    let flatten_typing_of_fol ~obs store fol : (Ext.var * var_type LSet.t) list =
      let lt = typing_of_fol ~obs store fol in
      flatten_typing lt


(* ---- Abstract Analysis of formulas ---- *)

(* should be used for queries only *)

    exception Unbound of Ext.var

    let is_bound v vars = LSet.mem v vars
    let are_bound vs vars = List.for_all (fun v -> LSet.mem v vars) vs

    let bind vs vars = List.fold_left (fun res v -> LSet.add v res) vars vs

    let rec list_of_fol = function
      | FAnd (p1,p2) ->
	  list_of_fol p1 @ list_of_fol p2
      | FForThe (p1,p2) ->
	  list_of_fol p1 @ list_of_fol p2
      | p -> [p]

    type eval_res = { cost : int; (* estimated cost of evaluating as query (dimensionality) *)
		      vars : Ext.var LSet.t; (* variables that are necessary bound *)
		      optvars : Ext.var LSet.t; (* variables that may be bound *)
		      fol : fol; (* transformed fol *)
		      full : bool; (* whether transformed fol is full (not truncated) *)
		    }

    let rec eval_fol ~obs store vars optvars (fol : fol) : eval_res =
      eval_fol_and ~obs store vars optvars (list_of_fol fol)
    and eval_fol_and ~obs store vars optvars lp =
      let _, _, e =
	Common.fold_while
	  (fun (lp1,first,e1) ->
	    if lp1 = []
	    then None
	    else
	      let res_opt =
		List.fold_left
		  (fun res_opt p2 ->
		    try
		      let e2 = eval_fol_atom ~obs store e1.vars e1.optvars p2 in
		      Option.fold
			(fun (_, e0) ->
			  if e2.cost < e0.cost &&
			    ( match p2 with
			      | FTrue | FBind _ | FSame _ | FDiff _
			      | FClass _ | FProp _ | FFunct _ | FPrim _ -> true
			      | _ -> false)
                              (* to avoid pushing negations too early *)
                              (* only atoms can be swapped *)
			  then Some (p2, e2)
			  else res_opt)
			(Some (p2, e2))
			res_opt
		    with Unbound _ ->
		      res_opt)
		  None lp1 in
	      match res_opt with
		| Some (p2, e2) ->
		  let lp2 = List.filter ((!=) p2) lp1 in
		  Some (lp2, false, { cost=e1.cost+e2.cost; vars=e2.vars; optvars=e2.optvars; fol=if first then e2.fol else FAnd (e1.fol,e2.fol); full=e1.full&&e2.full; })
		| None ->
		  Some ([], first, {e1 with full=false})) (* we ignore the rest to return a partial answer *)
	  (lp, true, eval_fol_atom ~obs store vars optvars FTrue) in
      e
    and eval_fol_atom ~obs store vars optvars fol =
      match fol with
      | FBound v ->
	  if is_bound v optvars
	  then { cost=0; vars=bind [v] vars; optvars; fol; full=true; }
	  else raise (Unbound v)
      | FBind (v,n) ->
	  { cost=0; vars=bind [v] vars; optvars=bind [v] optvars; fol=FBind (v,n); full=true; }
      | FSame (v1,v2) ->
	  if is_bound v1 vars then
	    if is_bound v2 vars then
	      { cost=0; vars; optvars; fol; full=true; }
	    else
	      { cost=0; vars=bind [v2] vars; optvars=bind [v2] optvars; fol; full=true; }
	  else
	    if is_bound v2 vars then
	      { cost=0; vars=bind [v1] vars; optvars=bind [v2] optvars; fol; full=true; }
	    else
	      raise (Unbound v1)
      | FDiff (v1,v2) ->
	  if are_bound [v1;v2] vars
	  then { cost=0; vars; optvars; fol; full=true; }
	  else raise (Unbound v1)
      | FClass (cl, s) ->
	  let cost = if s = "" || is_bound s vars then 0 else 1 in
	  { cost; vars=bind [s] vars; optvars=bind [s] optvars; fol; full=true; }
      | FProp (lm, pr, s, o) ->
	  let cost =
	    (if s = "" || is_bound s vars then 0 else 1) * (* YES! multiplication *)
	      (if o = "" || is_bound o vars then 0 else 1) in
	  { cost; vars=bind [s;o] vars; optvars=bind [s;o] optvars; fol; full=true; }
      | FFunct (funct, x, args) ->
	  let cost, largs =
	    if x <> "" && is_bound x vars
	    then 0, []
	    else Array.fold_left (fun (cost,largs) arg -> if arg = "" || is_bound arg vars then (cost,arg::largs) else (cost+1,arg::largs)) (0,[]) args in
	  { cost; vars=bind (x::largs) vars; optvars=bind (x::largs) optvars; fol; full=true; }
      | FPrim (prim,args) ->
	if (store#get_primitive prim (List.length args))#bounded (List.map (fun v -> is_bound v vars) args)
	then { cost=0; vars=bind args vars; optvars=bind args optvars; fol; full=true; }
	else raise (Unbound prim)
      | FRemove v -> eval_fol_call vars optvars fol "remove" [v]
      | FCopy (v1,v2) -> eval_fol_call vars optvars fol "copy" [v1;v2]
      | FMove (v1,v2) -> eval_fol_call vars optvars fol "move" [v1;v2]
      | FProc (proc,args) -> eval_fol_call vars optvars fol proc args
      | FForThe (p1,p2) ->
	  eval_fol_atom ~obs store vars optvars (FAnd (p1,p2))
(*
	let e1 = eval_fol ~obs store vars optvars p1 in
	let e2 = eval_fol ~obs store e1.vars e1.optvars p2 in
	{ cost=e1.cost+e2.cost; vars=e2.vars; optvars=e2.optvars; fol=FForthe (e1.fol,e2.fol); full=e1.full&&e2.full; }
*)
      | FForEvery (p1, FForEvery (p21, p22)) ->
	  eval_fol_atom ~obs store vars optvars (FForEvery (FAnd (p1,p21), p22))
      | FForEvery (p1,p2) ->
	let e1 = eval_fol ~obs store vars optvars p1 in
	let e2 = eval_fol ~obs store e1.vars e1.optvars p2 in
	{ cost=e1.cost+e2.cost; vars; optvars; fol=FForEvery (e1.fol,e2.fol); full=e1.full&&e2.full; }
      | FForEach (p1,p2) ->
	  eval_fol_atom ~obs store vars optvars (FForEvery (p1,p2))
      | FForOnly (p1,p2) ->
	  eval_fol_atom ~obs store vars optvars (FForEvery (p2,p1))
      | FForNo (p1,p2) ->
	  eval_fol_atom ~obs store vars optvars (FForEvery (p1, FNot p2))
      | FTrue ->
	  { cost=0; vars; optvars; fol; full=true; }
      | FAnd (p1,p2) ->
	  eval_fol_and ~obs store vars optvars (list_of_fol fol)
      | FOr (p1,p2) ->
	let e1 = eval_fol ~obs store vars optvars p1 in
	let e2 = eval_fol ~obs store vars optvars p2 in
	{ cost=max e1.cost e2.cost; vars=LSet.inter e1.vars e2.vars; optvars=LSet.union e1.optvars e2.optvars; fol=FOr (e1.fol,e2.fol); full=e1.full&&e2.full; }
      | FMaybe p1 ->
	let e1 = eval_fol ~obs store vars optvars p1 in
	{ cost=e1.cost; vars; optvars=LSet.union optvars e1.optvars; fol=FMaybe e1.fol; full=e1.full; }
      | FNot (FSame (v1,v2)) ->
	  eval_fol ~obs store vars optvars (FDiff (v1,v2))
      | FNot p1 ->
	let e1 = eval_fol ~obs store vars optvars p1 in
	{ cost=e1.cost; vars; optvars; fol=FNot e1.fol; full=e1.full; }
      | FCond (p0,p1,p2_opt) ->
	let e0 = eval_fol ~obs store vars optvars p0 in
	let e1 = eval_fol ~obs store vars optvars p1 in
	( match p2_opt with
	  | None ->
	    { cost=e0.cost+e1.cost; vars; optvars=e1.optvars; fol=FCond (e0.fol,e1.fol,None); full=e0.full&&e1.full; }
	  | Some p2 ->
	    let e2 = eval_fol ~obs store vars optvars p2 in
	    { cost=e0.cost+(max e1.cost e2.cost); vars=LSet.inter e1.vars e2.vars; optvars=LSet.union e1.optvars e2.optvars; fol=FCond (e0.fol,e1.fol,Some e2.fol); full=e0.full&&e1.full&&e2.full; } )
    and eval_fol_call vars optvars fol proc args =
      if List.for_all (fun v -> is_bound v vars) args
      then { cost=0; vars; optvars; fol; full=true; }
      else raise (Unbound proc)


(* ---- Extension/Code for formulas -------- *)

    type fol_ext = Ext.t
    type fol_code = Code.t

    class type rel =
      object
	method uri : Uri.t
	method add : Ext.map -> unit
	method remove : Ext.map -> unit
      end


(* should be called after 'eval_fol' *)
    let rec extension_fol ~obs store : fol -> fol_ext = function
      | FAnd (p1,p2) ->
	  let ext1 = extension_fol ~obs store p1 in
	  let ext2 = extension_fol ~obs store p2 in
	  Ext.join ~ordered:true ext1 ext2
      | FOr (p1,p2) ->
	  let ext1 = extension_fol ~obs store p1 in
	  let ext2 = extension_fol ~obs store p2 in
	  Ext.union ext1 ext2
      | FMaybe p1 ->
	  let ext1 = extension_fol ~obs store p1 in
	  Ext.optional ext1
      | FNot p1 ->
	  let ext1 = extension_fol ~obs store p1 in
	  Ext.negation ext1
      | FCond (p0,p1,p2_opt) ->
	  let ext0 = extension_fol ~obs store p0 in
	  let ext1 = extension_fol ~obs store p1 in
	  let ext2 = Option.fold (extension_fol ~obs store) Ext.empty p2_opt in
	  Ext.cond (Ext.succeeds ext0) ext1 ext2
      | FTrue ->
	  Ext.one
      | FBound x ->
	  Ext.check (Ext.bound x)
      | FBind (x,n) ->
	  Ext.single x n
      | FSame (x,y) ->
	  Ext.unify x y
      | FDiff (x,y) ->
	  Ext.neq x y
      | FClass (cl, s) ->
	  let cc = store#get_class cl in
	  cc#relation#extension ~obs s
      | FProp (lm, pr, s, o) ->
	  let pp = store#get_property pr in
	  let name, pp_ext =
	    if List.mem Direct lm
	    then "direct " ^ pr, pp#direct_relation#extension ~obs
	    else pr, pp#relation#extension ~obs in
	  let ext = pp_ext in
	  let name, ext = if List.mem Transitive lm then "trans "^name, Ext.transitive ext else name, ext in
	  let name, ext = if List.mem Reflexive lm then "opt "^name, Ext.reflexive ext else name, ext in
	  let name, ext = if List.mem Symmetric lm then "sym "^name, Ext.symmetric ext else name, ext in
	  ext s o
      | FFunct (funct, x, args) ->
	  let ff = store#get_functor funct (Array.length args) in
	  ff#relation#extension ~obs x args
      | FPrim (prim,args) ->
	  let pr = store#get_primitive prim (List.length args) in
	  pr#extension ~obs args
      | FRemove _
      | FCopy _
      | FMove _
      | FProc _ -> invalid_arg "procedure call in query"
      | FForThe (p1,p2) -> assert false (* failed to call 'eval_fol' *)
(*
	  let ext1 = extension_fol ~obs store p1 in
 	  let ext2 = extension_fol ~obs store p2 in
	  Ext.join ~ordered:true ext1 ext2
*)
      | FForEvery (p1,p2) ->
	  let ext1 = extension_fol ~obs store p1 in
	  let ext2 = extension_fol ~obs store p2 in
	  Ext.check (Ext.forall ext1 ext2)
      | FForEach _ -> assert false (* failed to call 'eval_fol' *)
      | FForOnly _ -> assert false (* failed to call 'eval_fol' *)
      | FForNo _ -> assert false (* failed to call 'eval_fol' *)

    let extension_of_fol ~obs (store : #Store.store) ?(bounded_vars : Ext.var LSet.t = []) (p : fol) : eval_res * Ext.t =
      let eval1 = eval_fol ~obs store bounded_vars bounded_vars p in
      eval1, extension_fol ~obs store eval1.fol

   (* code generation *)

    let rec cmd_fol ?(positive = true) p =
      match p with
	| FBound _ -> invalid_arg "bound"
	| FBind _ -> p
	| FSame _ -> p
	| FDiff _ -> invalid_arg "diff"
	| FClass _ -> p
	| FProp _ -> p
	| FFunct _ -> p
	| FPrim _ -> invalid_arg "primitive"
	| FRemove _ -> if positive then p else invalid_arg "retracted remove"
	| FCopy _ -> if positive then p else invalid_arg "retracted copy"
	| FMove _ -> if positive then p else invalid_arg "retracted move"
	| FProc _ -> if positive then p else invalid_arg "retracted procedure call"
	| FTrue -> p
	| FAnd (p1,p2) -> FAnd (cmd_fol ~positive p1, cmd_fol ~positive p2)
	| FOr _ -> invalid_arg "disjunction"
	| FMaybe _ -> invalid_arg "optional"
	| FCond (p0,p1,p2_opt) -> FCond (p0, cmd_fol ~positive p1, Option.map (fun p2 -> cmd_fol ~positive p2) p2_opt)
	| FNot p1 -> cmd_fol_foreach_retract ~positive p1 p1
	| FForThe (p1,p2) -> cmd_fol_foreach ~positive (FAnd (p1, FNot p2)) p2
	| FForEach (p1,p2) -> cmd_fol_foreach ~positive p1 p2
	| FForEvery (p1,p2) -> cmd_fol_foreach ~positive (FAnd (p1, FNot p2)) p2
	| FForOnly (p1,p2) -> cmd_fol_foreach_retract ~positive (FAnd (p2, FNot p1)) p2
	| FForNo (p1,p2) -> cmd_fol_foreach_retract ~positive (FAnd (p1,p2)) p2
    and cmd_fol_foreach ~positive p1 p2 =
      if positive
      then
	match cmd_fol p2 with
	  | FForEach (p1',p2') -> FForEach (FAnd (p1,p1'),p2')
	  | p2' -> FForEach (p1,p2')
      else invalid_arg "retracted foreach"
    and cmd_fol_foreach_retract ~positive p1 p2 =
      if positive
      then FForEach (p1, FNot (cmd_fol ~positive:false p2))
      else invalid_arg "retracted foreach retraction"

(*    type code_mode = Assert | Retract *)

   (* assumes the fol argument is the result of a successful call to 'cmd_fol' *)
    let rec code_fol ~obs store ?(positive = true) vars optvars = function
      | FAnd (p1,p2) ->
	  code_fol ~obs store ~positive vars optvars p1 @
	  code_fol ~obs store ~positive vars optvars p2
      | FOr _ -> assert false
      | FMaybe _ -> assert false
      | FNot p1 ->
	if positive
	then code_fol ~obs store ~positive:false vars optvars p1
	else assert false
(*
	  if ~positive = Assert
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars p1 in
	    let lc = code_fol ~obs store Retract eval1.vars eval1.optvars p1 in
	    [Code.foreach ext lc]
	  else invalid_arg "Ambiguous update: retracting a negation"
*)
(*      | FMaybe _ -> invalid_arg "optional in update" *)
      | FCond (p0,p1,p2_opt) ->
	  let _eval, ext0 = code_fol_ext ~obs store vars optvars p0 in
	  let lc1 = code_fol ~obs store ~positive vars optvars p1 in
	  let lc2 = Option.fold (fun p2 -> code_fol ~obs store ~positive vars optvars p2) [] p2_opt in
	  [Code.cond (Ext.succeeds ext0) lc1 lc2]
      | FTrue -> []
      | FBound v -> assert false
      | FBind (x,n) -> [Code.single x n]
      | FSame (x,y) -> [Code.unify x y]
      | FDiff (x,y) -> assert false
      | FClass (cl,s) ->
	  let cc = store#get_class cl in
	  if positive
	  then [Code.update (cc#uri ^ "#add") (function [x] -> cc#add x | _ -> assert false) [s]]
	  else [Code.update (cc#uri ^ "#remove") (function [x] -> cc#remove x | _ -> assert false) [s]]
      | FProp (lm,pr,s,o) ->
	  let pp = store#get_property pr in
	  if positive
	  then [Code.update (pp#uri ^ "#add") (function [x;y] -> pp#add x y | _ -> assert false) [s;o]]
	  else [Code.update (pp#uri ^ "#remove") (function [x;y] -> pp#remove x y | _ -> assert false) [s;o]]
      | FFunct (funct,x,args) ->
	  let ff = store#get_functor funct (Array.length args) in
	  if positive
	  then [Code.update (ff#uri ^ string_of_int ff#arity ^ "#add")
		  (function x::args -> ff#add x args | _ -> assert false)
		  (x::Array.to_list args)]
	  else [Code.update (ff#uri ^ string_of_int ff#arity ^ "#remove")
		  (function x::args -> ff#remove x args | _ -> assert false)
		  (x::Array.to_list args)]
      | FPrim _ -> invalid_arg "Fol.code_fol: primitives cannot be asserted"
      | FRemove v -> [Code.remove v]
      | FCopy (v1,v2) -> [Code.copy v1 v2]
      | FMove (v1,v2) -> [Code.move v1 v2]
      | FProc (proc,args) ->
	if positive
	then
	  let pr = store#get_procedure proc (List.length args) in
	  pr#code args
	else assert false (* invalid_arg "procedure calls cannot be retracted" *)
      | FForThe (p1,p2) -> assert false (* code_fol ~obs store ~positive vars optvars (FForEach (p1,p2)) *)
(*
	  if ~positive = Assert
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars p1 in
	    let lc = code_fol ~obs store Assert eval1.vars eval1.optvars p2 in
	    [Code.foreach ext lc]
	  else invalid_arg "Ambiguous update: retracting on 'the'"
*)
      | FForEach (p1,p2) ->
	  if positive
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars p1 in
	    let lc = code_fol ~obs store eval1.vars eval1.optvars p2 in
	    [Code.foreach ext lc]
	  else assert false (* invalid_arg "Ambiguous update: retracting on 'each/every/only/no/the'" *)
      | FForEvery (p1,p2) -> assert false (* code_fol ~obs store ~positive vars optvars (FForEach (FAnd (p1, FNot p2), p2)) *)
(*
	  if ~positive = Assert
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars (FAnd (p1, FNot p2)) in
	    let lc = code_fol ~obs store Assert eval1.vars eval1.optvars p2 in
	    [Code.foreach ext lc]
	  else invalid_arg "Ambiguous update: retracting on 'every'"
*)
      | FForOnly (p1,p2) -> assert false (* code_fol ~obs store ~positive vars optvars (FForEach (FAnd (FNot p1, p2), FNot p2)) *)
(*
	  if ~positive = Assert
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars (FAnd (FNot p1, p2)) in
	    let lc = code_fol ~obs store Retract eval1.vars eval1.optvars p2 in
	    [Code.foreach ext lc]
	  else invalid_arg "Ambiguous update: retracting on 'only'"
*)
      | FForNo (p1,p2) -> assert false (* code_fol ~obs store ~positive vars optvars (FForEach (FAnd (p1,p2), FNot p2)) *)
(*
	  if ~positive = Assert
	  then
	    let eval1, ext = code_fol_ext ~obs store vars optvars (FAnd (p1, p2)) in
	    let lc = code_fol ~obs store Retract eval1.vars eval1.optvars p2 in
	    [Code.foreach ext lc]
	  else invalid_arg "Ambiguous update: retracting on 'no'"
*)
    and code_fol_ext ~obs store vars optvars p =
      let eval1 = eval_fol_atom ~obs store vars optvars p in
      let ext = extension_fol ~obs store eval1.fol in
      eval1, ext

    let code_of_fol ~obs store ?(bounded_vars : Ext.var LSet.t = []) f =
      try
	let f_cmd = cmd_fol f in
	code_fol ~obs store bounded_vars bounded_vars f_cmd
      with Invalid_argument msg -> invalid_arg ("Invalid command: " ^ msg)

(*
    let assert_of_fol ~obs store ?(bounded_vars : Ext.var LSet.t = []) f =
      code_fol ~obs store Assert bounded_vars bounded_vars f

    let retract_of_fol ~obs store ?(bounded_vars : Ext.var LSet.t = []) f =
      code_fol ~obs store Assert bounded_vars bounded_vars (FNot f)
*)

  (* printing *)

    let rec print_fol = ipp
      [ FBound v -> "BOUND("; 'v; ")"
      | FBind (v,n) -> 'v; " := "; print_name of n
      | FSame (v1,v2) -> 'v1; " == "; 'v2
      | FDiff (v1,v2) -> 'v1; " != "; 'v2
      | FClass (cc,s) -> 'cc; "("; 's; ")"
      | FProp (lm,pp,s,o) -> 'pp; "("; print_modifiers of lm; 's; ","; 'o; ")"
      | FFunct (ff,x,args) -> 'x; "="; 'ff; "("; LIST0 [ arg -> 'arg ] SEP "," of Array.to_list args; ")"
      | FPrim (prim,args) -> 'prim; "("; LIST0 [ arg -> 'arg ] SEP "," of args; ")"
      | FRemove v -> "REMOVE "; 'v
      | FCopy (v1,v2) -> "COPY "; 'v1; " TO "; 'v2
      | FMove (v1,v2) -> "MOVE "; 'v1; " TO "; 'v2
      | FProc (proc,args) -> 'proc; "("; LIST0 [ arg -> 'arg ] SEP "," of args; ")"
      | FTrue -> "TRUE"
      | FAnd (f1,f2) -> "("; print_fol of f1; " AND "; print_fol of f2; ")"
      | FOr (f1,f2) -> "("; print_fol of f1; " OR "; print_fol of f2; ")"
      | FNot f1 -> "NOT "; print_fol of f1
      | FMaybe f1 -> "OPT "; print_fol of f1
      | FCond (f0,f1,f2_opt) -> "IF "; print_fol of f0; " THEN "; print_fol of f1;
	[ None -> 
	| Some f2 -> " ELSE "; print_fol of f2 ] of f2_opt
      | FForThe (f1,f2) -> print_quantif of ("THE",f1,f2)
      | FForEach (f1,f2) -> print_quantif of ("EACH",f1,f2)
      | FForEvery (f1,f2) -> print_quantif of ("EVERY",f1,f2)
      | FForNo (f1,f2) -> print_quantif of ("NO",f1,f2)
      | FForOnly (f1,f2) -> print_quantif of ("ONLY",f1,f2) ]
    and print_quantif = ipp
      [ qu, f1,f2 -> 'qu; "("; print_fol of f1; " -> "; print_fol of f2; ")" ]
    and print_modifiers = ipp
      [ [] -> 
      | lm -> LIST1 print_modifier SEP " " of lm; " : " ]
    and print_modifier = ipp
      [ Direct -> "direct"
      | Symmetric -> "symmetric"
      | Reflexive -> "reflexive"
      | Transitive -> "transitive" ]
    and print_name = ipp
      [ n -> ' (Name.contents n) ]

(*
    let print (fol : fol) : unit =
      Ipp.once (ipp [ fol -> print_fol of fol; EOF ]) fol (Printer.cursor_of_formatter Format.std_formatter) ()
*)

    let string_of_fol (fol : fol) : string =
      let buf_cursor = new Printer.buffer_cursor in
      Ipp.once (ipp [ fol -> print_fol of fol; EOF ]) fol buf_cursor ();
      buf_cursor#contents

  end
