(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Builtins

module Basic = Builtins_basic
module Store = Lisql.Store

(* Geographical locations *)

module Location =
  struct
    type t = { latitude : float;
	       longitude : float;
	       altitude : float option}
    let make ~latitude ~longitude ?(altitude = None) () =
      { latitude; longitude; altitude }

    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_location ->
	( try
	  let slat, slon, salt_opt =
	    match Str.split (Str.regexp "[ ]+") s with
	      | [s1;s2;s3] -> s1, s2, Some s3
	      | [s1;s2] -> s1, s2, None
	      | _ -> invalid_arg "" in
	  Some
	    { latitude=(float_of_string slat);
	      longitude=(float_of_string slon);
	      altitude=Option.map (fun salt -> float_of_string salt) salt_opt }
	  with _ -> None )
      | _ -> None
    let to_name loc =
      let s =
	match loc.altitude with
	  | Some alt -> Printf.sprintf "%f %f %f" loc.latitude loc.longitude alt
	  | None -> Printf.sprintf "%f %f" loc.latitude loc.longitude in
      Rdf.Literal (s, Rdf.Typed Xsd.uri_location)

    let latitude loc = return loc.latitude
    let longitude loc = return loc.longitude
    let altitude loc = match loc.altitude with None -> fail | Some alt -> return alt

(*
 * Returns the orthodromic distance between two geographic coordinates.
 * The orthodromic distance is the shortest distance between two points
 * on a sphere's surface. The orthodromic path is always on a great circle.
 * This is different from the <cite>loxodromic distance</cite>, which is a
 * longer distance on a path with a constant direction on the compass.
 *)
    let distance loc1 loc2 = (* orthodromic distance *)
      let pi = acos (-1.) in
      let radians_of_degrees deg = deg *. pi /. 180. in
(*
 * Solution of the geodetic inverse problem after T.Vincenty.
 * Modified Rainsford's method with Helmert's elliptical terms.
 * Effective in any azimuth and at any distance short of antipodal.
 *
 * Latitudes and longitudes in radians positive North and East.
 * Forward azimuths at both points returned in radians from North.
 *
 * Programmed for CDC-6600 by LCDR L.Pfeifer NGS ROCKVILLE MD 18FEB75
 * Modified for IBM SYSTEM 360 by John G.Gergen NGS ROCKVILLE MD 7507
 * Ported from Fortran to Java by Martin Desruisseaux.
 *
 * Source: ftp://ftp.ngs.noaa.gov/pub/pcsoft/for_inv.3d/source/inverse.for
 * subroutine INVER1
 *)
      let x1 = radians_of_degrees loc1.longitude in
      let y1 = radians_of_degrees loc1.latitude in
      let x2 = radians_of_degrees loc2.longitude in
      let y2 = radians_of_degrees loc2.latitude in
      let uMAX_ITERATIONS = 100 in
      let uEPS = 0.5E-13 in
(*
        final double F = 1/getInverseFlattening();
        final double R = 1-F;
*)
      let semiMajorAxis = 6378137.0 in (* from Wikipedia *)
      let uF = 1. /. 298.257223563 in (* from Wikipedia *)
      let uR = 1. -. uF in

      let result = ref None in
      let tu1 = ref (uR *. sin y1 /. cos y1) in
      let tu2 = ref (uR *. sin y2 /. cos y2) in
      let cu1 = 1. /. sqrt (!tu1 *. !tu1 +. 1.) in
      let cu2 = 1. /. sqrt (!tu2 *. !tu2 +. 1.) in
      let su1 = cu1 *. !tu1 in
      let s = ref (cu1 *. cu2) in
      let baz = ref (!s *. !tu2) in
      let faz = ref (!baz *. !tu1) in
      let x = ref (x2 -. x1) in
      for i = 0 to uMAX_ITERATIONS - 1 do
        let sx = sin !x in
        let cx = cos !x in
        tu1 := cu2 *. sx;
        tu2 := !baz -. su1 *. cu2 *. cx;
        let sy = hypot !tu1 !tu2 in
        let cy = !s *. cx +. !faz in
        let y = atan2 sy cy in
        let uSA = !s *. sx /. sy in
        let c2a = 1. -. uSA *. uSA in
        let cz = ref (!faz +. !faz) in
        if c2a > 0. then
	  begin
            cz := -. !cz /. c2a +. cy
	  end;
        let e = !cz *. !cz *. 2. -. 1. in
        let c = ref (((-3. *. c2a +. 4.) *. uF +. 4.) *. c2a *. uF /. 16.) in
        let d = ref !x in
        x := ((e *. cy *. !c +. !cz) *. sy *. !c +. y) *. uSA;
        x := (1. -. !c) *. !x *. uF +. x2 -. x1;

        if abs_float (!d -. !x) <= uEPS then (
          if false then (
	    (*
             * 'faz' and 'baz' are forward azimuths at both points.
             * Since the current API can't returns this result, it
             * doesn't worth to compute it at this time.
	     *)
            faz := atan2 !tu1 !tu2;
            baz := (atan2 (cu1 *. sx) (!baz *. cx -. su1 *. cu2)) +. pi;
          );
          x := sqrt ((1. /. (uR *. uR) -. 1.) *. c2a +. 1.) +. 1.;
          x := (!x -. 2.) /. !x;
          c := 1. -. !x;
          c := (!x *. !x /. 4. +. 1.) /. !c;
          d := (0.375 *. !x *. !x -. 1.) *. !x;
          x := e *. cy;
          s := 1. -. 2. *. e;
          s := ((((sy *. sy *. 4. -. 3.) *. !s *. !cz *. !d /. 6. -. !x) *. !d /. 4. +. !cz) *. sy *. !d +. y) *. !c *. uR *. semiMajorAxis;
          result := Some !s
        )
      done;
      (*
       * No convergence. It may be because coordinate points
       * are equals or because they are at antipodes.
       *)
      let uLEPS = 1E-10 in
      if abs_float (x1 -. x2) <= uLEPS && abs_float (y1 -. y2) <= uLEPS then (
        result := Some 0. (* Coordinate points are equals *)
      );
      if abs_float y1 <= uLEPS && abs_float y2 <= uLEPS then (
        result := Some (abs_float (x1 -. x2) *. semiMajorAxis) (* Points are on the equator. *)
      );
      match !result with
	| Some r -> return r
	| None -> fail (* Other cases: no solution for this algorithm. *)

  end

let latitude_func = new func1
  [ [Xsd.uri_double; Xsd.uri_location] ]
  Location.from_name Location.latitude Basic.Float.to_name
let longitude_func = new func1
  [ [Xsd.uri_double; Xsd.uri_location] ]
  Location.from_name Location.longitude Basic.Float.to_name
let altitude_func = new func1
  [ [Xsd.uri_double; Xsd.uri_location] ]
  Location.from_name Location.altitude Basic.Float.to_name

let locationDistance_func = new func2
  [ [Xsd.uri_double; Xsd.uri_location; Xsd.uri_location] ]
  Location.from_name Location.from_name Location.distance Basic.Float.to_name


(* JTS geometries *)

module Geometry =
  struct
    let types = [Strdf.uri_WKT] (* TODO: add Strdf.uri_GML, ... *)
    type t = Jts.Geometry.obj

    let srid_WGS84 = 4326

    let wkt_reader = lazy (Jts.WKTReader.create ())
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Strdf.uri_WKT ->
	if Jts.WKTReader.defined
	then begin
	  let g = (Lazy.force wkt_reader)#read s in
	  g#setSRID srid_WGS84;
	  Some g end
	else None
      | _ -> None
    let to_name (g : t) =
      let s = g#toText in
      Rdf.Literal (s, Rdf.Typed Strdf.uri_WKT)

    let isEmpty g = g#isEmpty

    let contains g1 g2 = g1#contains g2
    let covers g1 g2 = g1#covers g2
    let coveredBy g1 g2 = g1#coveredBy g2
    let crosses g1 g2 = g1#crosses g2
    let disjoint g1 g2 = g1#disjoint g2
    let intersects g1 g2 = g1#intersects g2
    let overlaps g1 g2 = g1#overlaps g2
    let touches g1 g2 = g1#touches g2
    let equals g1 g2 = g1#equals g2
    let relates g1 g2 (pattern : string) = g1#relates g2 pattern
    let isWithinDistance g1 g2 (dist : float) = g1#isWithingDistance g2 dist

    let getSRID g : int monad = return g#getSRID
    let getDimension g : int monad = return g#getDimension
    let getNumGeometries g : int monad = return g#getNumGeometries
    let getNumPoints g : int monad = return g#getNumPoints
    let getArea g : float monad = return g#getArea
    let getLength g : float monad = return g#getLength
    let distance g1 g2 : float monad = return (g1#distance g2)
      (* TODO: use algorithm at https://github.com/geotools/geotools/blob/master/modules/library/referencing/src/main/java/org/geotools/referencing/datum/DefaultEllipsoid.java #orthodromicDistance
	 between the two closest points (available in JTS) *)

    let intersection g1 g2 : t monad =
      let g = g1#intersection g2 in
      if g#isEmpty then fail else return g
    let union g1 g2 : t monad = return (g1#union g2)
    let difference g1 g2 : t monad =
      let g = g1#difference g2 in
      if g#isEmpty then fail else return g
    let symDifference g1 g2 : t monad =
      let g = g1#symDifference g2 in
      if g#isEmpty then fail else return g
    let getBoundary g : t monad = return g#getBoundary
    let convexHull g : t monad = return g#convexHull
    let getEnvelope g : t monad = return g#getEnvelope
    let buffer g (dist : float) : t monad = return (g#buffer dist)
  end

let geo_types1 =
  List.map (fun uri -> [uri]) Geometry.types
let geo_types2 =
  List.fold_left
    (fun res uri1 ->
      List.fold_left
	(fun res uri2 ->
	  [uri1; uri2]::res)
	res Geometry.types)
    [] Geometry.types

(* predicates *)

let geo_pred1 p1 = new pred1
  geo_types1
  Geometry.from_name p1

let isEmpty_pred = geo_pred1 Geometry.isEmpty

let geo_pred2 p2 = new pred2
  geo_types2
  Geometry.from_name Geometry.from_name p2

let contains_pred = geo_pred2 Geometry.contains
let covers_pred = geo_pred2 Geometry.covers
let crosses_pred = geo_pred2 Geometry.crosses
let intersects_pred = geo_pred2 Geometry.intersects
let overlaps_pred = geo_pred2 Geometry.overlaps
let touches_pred = geo_pred2 Geometry.touches
let equals_pred = geo_pred2 Geometry.equals

let relates_pred = new pred3
  (List.map (fun t -> t @ [Xsd.uri_string]) geo_types2)
  Geometry.from_name Geometry.from_name Basic.String.from_name Geometry.relates

(* metric and geometric functions *)

let geo_func1 ltres f1 conv = new func1
  (List.fold_left
     (fun res tres ->
       List.fold_left
	 (fun res t ->
	   (tres::t)::res)
	 res geo_types1)
     [] ltres)
  Geometry.from_name f1 conv

let getSRID_func = geo_func1 [Xsd.uri_integer] Geometry.getSRID Basic.Int.to_name
let getDimension_func = geo_func1 [Xsd.uri_integer] Geometry.getDimension Basic.Int.to_name
let getNumGeometries_func = geo_func1 [Xsd.uri_integer] Geometry.getNumGeometries Basic.Int.to_name
let getNumPoints_func = geo_func1 [Xsd.uri_integer] Geometry.getNumPoints Basic.Int.to_name
let getArea_func = geo_func1 [Xsd.uri_double] Geometry.getArea Basic.Float.to_name
let getLength_func = geo_func1 [Xsd.uri_double] Geometry.getLength Basic.Float.to_name

let getBoundary_func = geo_func1 Geometry.types Geometry.getBoundary Geometry.to_name
let convexHull_func = geo_func1 Geometry.types Geometry.convexHull Geometry.to_name
let getEnvelope_func = geo_func1 Geometry.types Geometry.getEnvelope Geometry.to_name

let buffer_func = new func2
  (List.map (fun t -> t@[Xsd.uri_double]) geo_types2)
  Geometry.from_name Basic.Float.from_name Geometry.buffer Geometry.to_name


let geo_func2 ltres f2 conv = new func2
  (List.fold_left
     (fun res tres ->
       List.fold_left
	 (fun res t ->
	   (tres::t)::res)
	 res geo_types2)
     [] ltres)
  Geometry.from_name Geometry.from_name f2 conv

let distance_func = geo_func2 [Xsd.uri_double] Geometry.distance Basic.Float.to_name

let intersection_func = geo_func2 Geometry.types Geometry.intersection Geometry.to_name
let union_func = geo_func2 Geometry.types Geometry.union Geometry.to_name
let difference_func = geo_func2 Geometry.types Geometry.difference Geometry.to_name
let symDifference_func = geo_func2 Geometry.types Geometry.symDifference Geometry.to_name


let _ =
  Lisql.add_store_initializer (fun store ->
    let def_pred id syntax pred = store#define_primitive (new Store.expr_pred id syntax pred) in
    let def_func id syntax func = store#define_primitive (new Store.expr_func id syntax func) in
    if Jts.Geometry.defined
    then begin
      def_func "geo:latitude" (Store.PredSyntax.relnoun1 "latitude") latitude_func;
      def_func "geo:longitude" (Store.PredSyntax.relnoun1 "longitude") longitude_func;
      def_func "geo:altitude" (Store.PredSyntax.relnoun1 "altitude") altitude_func;
      def_pred "geo:isEmpty" (Store.PredSyntax.noun "empty geometry") isEmpty_pred;
      def_pred "geo:covers" (Store.PredSyntax.verb "covers" "is covered" ~prep2:"by") covers_pred;
      def_pred "geo:crosses" (Store.PredSyntax.verb "crosses" "is crossed" ~prep2:"by") crosses_pred;
      def_pred "geo:intersects" (Store.PredSyntax.verb "intersects" "intersects") intersects_pred;
      def_pred "geo:touches" (Store.PredSyntax.verb "touches" "is touched" ~prep2:"by") touches_pred;
      def_pred "geo:equals" (Store.PredSyntax.verb "equals" "equals") equals_pred;
      def_pred "geo:relates" (Store.PredSyntax.verb "relates" ~prep1:"to" "is related" ~prep2:"to" ~preps:["with DE-9IM pattern"]) relates_pred;
      def_func "geo:getSRID" (Store.PredSyntax.relnoun1 "SRID") getSRID_func;
      def_func "geo:getDimension" (Store.PredSyntax.relnoun1 "dimension") getDimension_func;
      def_func "geo:getNumGeometries" (Store.PredSyntax.relnoun1 "number of geometries") getNumGeometries_func;
      def_func "geo:getNumPoints" (Store.PredSyntax.relnoun1 "number of points") getNumPoints_func;
      def_func "geo:getArea" (Store.PredSyntax.relnoun1 "area") getArea_func;
      def_func "geo:getLength" (Store.PredSyntax.relnoun1 "length") getLength_func;
      def_func "geo:getBoundary" (Store.PredSyntax.relnoun1 "boundary") getBoundary_func;
      def_func "geo:getEnvelope" (Store.PredSyntax.relnoun1 "envelope") getEnvelope_func;
      def_func "geo:convexHull" (Store.PredSyntax.relnoun1 "convex hull") convexHull_func;
      def_func "geo:buffer" (Store.PredSyntax.relnoun1 "buffer" ~preps:["at distance"]) buffer_func;
      def_func "geo:distance" (Store.PredSyntax.noun "distance" ~preps:["from"; "to"])
	(Builtins.funcs_union [locationDistance_func;
			       distance_func]);
      def_func "geo:intersection" (Store.PredSyntax.binop "\226\136\169") intersection_func;
      def_func "geo:union" (Store.PredSyntax.binop "\226\136\170") union_func;
      def_func "geo:difference" (Store.PredSyntax.binop "\\") difference_func;
      def_func "geo:symDifference" (Store.PredSyntax.binop "\226\138\150") symDifference_func
    end)
