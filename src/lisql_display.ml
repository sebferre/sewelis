(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Lisql_ast

type kind = [ `Entity | `Class | `Property | `Functor | `Datatype ]
type t = token list
and token =
    [ `Space
    | `Kwd of string
    | `Var of string
    | `URI of Uri.t * kind * string * Uri.t option
    | `Prim of string
    | `Plain of string * string
    | `Typed of string * Uri.t * string
    | `Xml of Xml.xml
    | `List of t list
    | `Tuple of t list
    | `Seq of t list
    | `And of t list
    | `Or of t list
    | `Not of t
    | `Maybe of t
    | `Brackets of t
    | `Quote of t
    | `Pair of bool * t * t
    | `Focus of focus * t ]

(* utilities *)

let get_object_as_string ~obs store s p : string option = Common.prof "Lisql_display.get_object_as_string" (fun () ->
  match store#choose_object ~obs s p with
    | Some (Rdf.Literal (str, (Rdf.Plain ("" | "en") | Rdf.Typed _))) -> Some str
    | _ -> None)

let string_of_uri ~obs store uri : string =
  match get_object_as_string ~obs store (Rdf.URI uri) Rdfs.uri_label with
  | Some s -> s
  | None -> Name.string_of_uri store#base store#xmlns uri

let string_of_uri_role ~obs store (ikind : [`Subject | `Object]) uri =
  match get_object_as_string ~obs store (Rdf.URI uri)
      (match ikind with `Subject -> Rdfs.uri_label | `Object -> Namespace.uri_inverseLabel) with
  | Some s -> `Subject, s
  | None ->
      match get_object_as_string ~obs store (Rdf.URI uri)
	  (match ikind with `Object -> Rdfs.uri_label | `Subject -> Namespace.uri_inverseLabel) with
      | Some s -> `Object, s
      | None -> ikind, Name.string_of_uri store#base store#xmlns uri

let image_of_uri ~obs store uri =
  store#get_image ~obs (Rdf.URI uri)

(* precedences *)

let prec_seq = 13
let prec_call = 12
let prec_or = 11
let prec_and = 10
let prec_not = 9
let prec_restr = 8
let prec_infix = 7
let prec_equal = 6
let prec_plus = 5
let prec_times = 4
let prec_power = 3
let prec_fact = 2
let prec_app = 1
let prec_atom = 0

(* display generation *)

let brackets_opt ctx op toks =
  if op > ctx
  then [`Brackets toks]
  else toks

let index_list l =
  let rec aux i = function
    | [] -> []
    | x::l -> (i,x)::aux (i+1) l in
  aux 0 l

(* splits a 'p1' into an optional class, and a list of 'p1'
   all associated with a context *)
let rec split_p1 find_head k = function
  | Thing -> None, []
  | And l -> split_p1_list find_head 0 (fun n -> AndN (n,l,k)) l
  | f -> split_p1_list find_head 0 (fun _ -> k) [f]
and split_p1_list find_head n kn = function
  | [] -> None, []
  | ((Arg (Pred.Type _, 1, _) | Arg (Pred.Funct _, 0, _) as head) :: l) when find_head ->
      let _, l' = split_p1_list false (n+1) kn l in
      Some (kn n, head), l'
  | x::l ->
      let head_opt, l' = split_p1_list find_head (n+1) kn l in
      head_opt, (kn n,x)::l'


let rec display_c ~obs store k prec c =
  [`Focus (AtC (c,k), display_c_aux ~obs store k prec c)]
and display_c_aux ~obs store k prec = function
  | Nope -> [`Kwd "nope"]
  | Assert s -> display_s ~obs store (Assert0 k) prec_or s
  | Get np -> display_s1 ~obs store (Get0 k) prec_or np
  | Call (proc,args) -> display_call ~obs store k proc args
  | Seq lc -> brackets_opt prec prec_seq
    [`Seq (List.map (fun (n,c) -> display_c ~obs store (SeqN (n,lc,k)) prec_call c) (index_list lc))]
  | Cond (s,c1,c2) ->
    `Kwd "if" :: `Space :: display_s ~obs store (Cond0 (c1,c2,k)) prec_or s @
      `Space :: `Kwd "then" :: `Space :: display_c ~obs store (Cond1 (s,c2,k)) prec_call c1 @
      `Space :: `Kwd "else" :: `Space :: display_c ~obs store (Cond2 (s,c1,k)) prec_call c2
  | For (np,c) ->
    [`Pair (true,
	    `Kwd "for" :: `Space :: display_s1 ~obs store (For1 (c,k)) prec_or np,
	    display_c ~obs store (For2 (np,k)) prec_atom c)]
and display_call ~obs store k proc args =
  let dargs =
    Array.mapi
      (fun j np -> display_s1 ~obs store (CallN (j,proc,args,k)) prec_or np)
      args in
  match proc with
    | Proc.RemoveName -> `Kwd "delete" :: `Space :: dargs.(0)
    | Proc.CopyName -> `Kwd "duplicate" :: `Space :: dargs.(0) @ `Brackets (`Kwd "as" :: `Space :: dargs.(1)) :: []
    | Proc.MoveName -> `Kwd "replace" :: `Space :: dargs.(0) @ `Brackets (`Kwd "by" :: `Space :: dargs.(1)) :: []
    | Proc.Prim op -> display_call_prim ~obs store op (Array.to_list dargs)
and display_call_prim ~obs store op ldargs =
  let syntax = (store#get_procedure op (List.length ldargs))#syntax in
  match syntax, ldargs with
    | `Proc (p,_), _ -> `Prim p :: `Tuple ldargs :: []
    | `Verb ((v,prep_opt), preps), d::ld -> `Prim v :: display_prep_opt prep_opt @ `Space :: d @ display_preps_args preps ld
    | _ -> invalid_arg "Lisql_display.display_call_prim"

and display_s ~obs store k prec s =
  [`Focus (AtS (s,k), display_s_aux ~obs store k prec s)]
and display_s_aux ~obs store k prec = function
  | Is (np,f) -> [`Pair (false (*true*),
			 display_s1 ~obs store (Is0 (np,f,k)) prec_or np,
			 display_p1_vp ~obs store (Is1 (np,f,k)) prec_or f)]
  | True -> [`Kwd "true"]
  | SAnd l -> brackets_opt prec prec_and
	[`And (List.map (fun (n,s1) -> display_s ~obs store (SAndN (n,l,k)) prec_not s1) (index_list l))]
  | SOr l -> brackets_opt prec prec_or
	[`Or (List.map (fun (n,s1) -> display_s ~obs store (SOrN (n,l,k)) prec_and s1) (index_list l))]
  | SNot s1 -> brackets_opt prec prec_not
	[`Not (display_s ~obs store (SNot0 (s1,k)) prec_atom s1)]
  | SMaybe s1 -> brackets_opt prec prec_not
	[`Maybe (display_s ~obs store (SMaybe0 (s1,k)) prec_atom s1)] 
and display_s1 ~obs store k prec np =
  [`Focus (AtS1 (np,k), display_s1_aux ~obs store k prec np)]
and display_s1_aux ~obs store k prec = function
  | Det (Qu (An, None), Thing) -> [`Kwd "?"]
  | Det (det,f) ->
    let kdet = Det0 (det,f,k) in
    let kf = Det1 (det,f,k) in
    display_det_p1 ~obs store kdet det kf f
  | NAnd l -> brackets_opt prec prec_and
	[`And (List.map (fun (n,np1) -> display_s1 ~obs store (NAndN (n,l,k)) prec_not np1) (index_list l))]
  | NOr l -> brackets_opt prec prec_or
	[`Or (List.map (fun (n,np1) -> display_s1 ~obs store (NOrN (n,l,k)) prec_and np1) (index_list l))]
  | NNot np1 -> brackets_opt prec prec_not
	[`Not (display_s1 ~obs store (NNot0 (np1,k)) prec_atom np1)]
  | NMaybe np1 -> brackets_opt prec prec_not
	[`Maybe (display_s1 ~obs store (NMaybe0 (np1,k)) prec_atom np1)]
and display_det_p1 ~obs store kdet det kf f =
  let head_opt, l = split_p1 (match det with Qu _ -> true | _ -> false) kf f in
  match l with
    | [] ->
      display_s2 ~obs store kdet head_opt det
    | [(kx,x)] ->
      [`Pair (true,
	      display_s2 ~obs store kdet head_opt det,
	      [`Focus (AtP1 (f,kf), display_p1_rel ~obs store kx prec_or x)])]
    | _ ->
      [`Pair (true,
	      display_s2 ~obs store kdet head_opt det,
	      [`Focus (AtP1 (f,kf), [`And (List.map (fun (kx,x) -> display_p1_rel ~obs store kx prec_and x) l)])])]
and display_s2 ~obs store k head_opt = function
  | Name (Rdf.Literal (s, Rdf.Typed dt) as n) when dt = Namespace.uri_Assertion ->
    (try
       let c = store#unquote_assertion s in
       display_s2 ~obs store k None (Quote c)
     with _ -> display_name ~obs store n) (* fallback in case of syntax error when unquoting *)
  | Name n -> display_name ~obs store n
  | Quote c -> [`Quote (display_c ~obs store (Quote0 k) prec_seq c)]
  | Ref v -> [`Var v]
  | Which -> [`Kwd "which"]
  | Qu (qu, v_opt) ->
      ( match head_opt with
      | None ->
	  display_qu qu @
	  [`Space; `Kwd "thing"] @
	  ( match v_opt with
	  | None -> []
	  | Some v -> [`Space; `Var v] )
      | Some (kh, (Arg (Pred.Type c, 1, _) as h)) ->
	  display_qu qu @
	  [`Space;
	   `Focus (AtP1 (h, kh),
		   [`URI (c, `Class, string_of_uri ~obs store c, image_of_uri ~obs store c)])] @
	  ( match v_opt with
	  | None -> []
	  | Some v -> [`Space; `Var v] )	  
      | Some (kh, (Arg (Pred.Funct funct, 0, args) as h)) ->
	  ( match qu with
	  | An | The -> []
	  | _ -> display_qu qu @ [`Space] ) @
	  [`Focus (AtP1 (h,kh), display_arg ~obs store kh (Pred.Funct funct) 0 args)] @
(*		   display_p1_vp_aux ~obs store kh prec_atom h)] @ *)
	  ( match v_opt with
	  | None -> []
	  | Some v -> [`Space; `Kwd "as"; `Space; `Var v] )
      | Some _ -> assert false )
and display_qu = function
  | An -> [`Kwd "a"]
  | The -> [`Kwd "the"]
  | Every -> [`Kwd "every"]
  | Only -> [`Kwd "only the"]
  | No -> [`Kwd "no" ]
  | Each -> [`Kwd "each" ]
and display_p1_rel ~obs store k prec f =
  [`Focus (AtP1 (f,k), `Kwd "that" :: `Space :: display_p1_vp_aux ~obs store k prec f)]
and display_p1_vp ~obs store k prec f =
  [`Focus (AtP1 (f,k), display_p1_vp_aux ~obs store k prec f)]
and display_p1_vp_aux ~obs store k prec = function
  | Arg (pred,i,args) ->
      display_arg ~obs store k pred i args
  | SuchThat (x,s) ->
      [`Pair (true,
	      [`Kwd "is"; `Space; `Var x; `Space; `Kwd "such that"],
	      display_s ~obs store (SuchThat0 (x,s,k)) prec_atom s)]
  | Thing -> [`Kwd "is there"]
  | And l -> brackets_opt prec prec_and
	[`And (List.map (fun (n,f1) -> display_p1_vp ~obs store (AndN (n,l,k)) prec_not f1) (index_list l))]
  | Or l -> brackets_opt prec prec_or
	[`Or (List.map (fun (n,f1) -> display_p1_vp ~obs store (OrN (n,l,k)) prec_and f1) (index_list l))]
  | Not f1 -> brackets_opt prec prec_not
	[`Not (display_p1_vp ~obs store (Not0 (f1,k)) prec_atom f1)]
  | Maybe f1 -> brackets_opt prec prec_not
	[`Maybe (display_p1_vp ~obs store (Maybe0 (f1,k)) prec_atom f1)]
and display_arg ~obs store k pred i args =
  let dargs =
    Array.mapi
      (fun j np ->
	if j <> i && (j>0 || Pred.reifiable pred)
	then display_s1 ~obs store (ArgN (j,pred,i,args,k)) prec_or np
	else [`Kwd "this"])
      args in
  match pred with
    | Pred.Equal ->
        let dnp = dargs.(Argindex.opposite i) in
        [`Pair (false,
		[`Kwd "is"],
		dnp)]
    | Pred.Type c ->
        [`Kwd "is a"; `Space;
	 `URI (c, `Class, string_of_uri ~obs store c, image_of_uri ~obs store c)]
    | Pred.Role r ->
        let dnp = dargs.(Argindex.opposite i) in
	[`Pair (false,
		display_role_ikind ~obs store r (Argindex.to_kind_role i),
		dnp)]
    | Pred.Funct funct ->
      let ldargs = List.tl (Array.to_list dargs) in
        if i = Argindex.stat
	then `Kwd "is" :: `Space ::
	  display_funct ~obs store funct @
	  `Tuple ldargs :: []
	else `Kwd "has" :: `Space :: 
	  `Pair (false,
		 display_funct ~obs store funct @ `Tuple ldargs :: [],
		 dargs.(Argindex.stat)) :: []
    | Pred.Prim prim ->
      let ldargs = List.tl (Array.to_list dargs) in
      display_arg_prim ~obs store prim i ldargs
and display_arg_prim ~obs store op i ldargs =
let syntax = (store#get_primitive op (List.length ldargs))#syntax in
match syntax, i, ldargs with
  | `Pred (p,_), _, _ -> `Prim p :: `Tuple ldargs :: []
  | `Func (f,_), 1, d1::ld -> `Kwd "=" :: `Space :: `Prim f :: `Tuple ld :: []
  | `Func (f,_), _, d1::ld -> `Kwd "where" :: `Space :: `Prim f :: `Tuple ld :: `Space :: `Kwd "=" :: `Space :: d1
  | `Unop op, 1, [_;d2] -> `Kwd "=" :: `Space :: `Prim op :: `Space :: d2
  | `Unop op, _, [d1;d2] -> `Kwd "where" :: `Space :: `Prim op :: `Space :: d2 @ `Space :: `Kwd "=" :: `Space :: d1
  | `Binop op, 1, [_;d2;d3] -> `Kwd "=" :: `Space :: d2 @ `Space :: `Prim op :: `Space :: d3
  | `Binop op, 2, [d1;d2;d3] -> `Kwd "where" :: `Space :: d2 @ `Space :: `Prim op :: `Space :: d3 @ `Space :: `Kwd "=" :: `Space :: d1
  | `Binop op, 3, [d1;d2;d3] -> `Kwd "where" :: `Space :: d2 @ `Space :: `Prim op :: `Space :: d3 @ `Space :: `Kwd "=" :: `Space :: d1
  | `Noun (n,preps), 1, _::ld -> `Kwd "is a" :: `Space :: `Prim n :: display_preps_args preps ld
  | `Noun (n,preps), _, d1::ld -> d1 @ `Space :: `Kwd "is a" :: `Space :: `Prim n :: display_preps_args preps ld
  | `RelNoun1 (n,preps), 1, _::d2::ld -> `Kwd "is" :: `Space :: `Prim n :: `Space :: `Kwd "of" :: `Space :: d2 @ display_preps_args preps ld
  | `RelNoun1 (n,preps), 2, d1::_::ld -> `Kwd "has" :: `Space :: `Prim n :: `Space :: d1 @ display_preps_args preps ld
  | `RelNoun1 (n,preps), _, d1::d2::ld -> d1 @ `Space :: `Kwd "is" :: `Space :: `Prim n :: `Space :: `Kwd "of" :: `Space :: d2 @ display_preps_args preps ld
  | `RelNoun2 (n,preps), 1, _::d2::ld -> `Kwd "has" :: `Space :: `Prim n :: `Space :: d2 @ display_preps_args preps ld
  | `RelNoun2 (n,preps), 2, d1::_::ld -> `Kwd "has" :: `Space :: `Prim n :: `Space :: `Kwd "of" :: `Space :: d1 @ display_preps_args preps ld
  | `RelNoun2 (n,preps), _, d1::d2::ld -> d2 @ `Space :: `Kwd "is" :: `Space :: `Prim n :: `Space :: `Kwd "of" :: `Space :: d1 @ display_preps_args preps ld
  | `Verb ((v1,prep1_opt),_,preps), 1, _::d2::ld when v1 <> "" ->
    `Prim v1 :: display_prep_opt prep1_opt @ `Space :: d2 @ display_preps_args preps ld
  | `Verb (_, (v2,prep2_opt),preps), 2, d1::_::ld when v2 <> "" ->
    `Prim v2 :: display_prep_opt prep2_opt @ `Space :: d1 @ display_preps_args preps ld
  | `Verb ((v1,prep1_opt),_,preps), _, d1::d2::ld ->
    d1 @ `Space :: `Prim v1 :: display_prep_opt prep1_opt @ `Space :: d2 @ display_preps_args preps ld
  | _ -> invalid_arg "Lisql_display.display_arg_prim"
and display_prep_opt prep_opt =
match prep_opt with
  | None -> []
  | Some prep -> `Space :: `Prim prep :: []
and display_preps_args preps dargs =
try
  List.concat
    (List.map2
       (fun prep darg -> [`Brackets (`Prim prep :: `Space :: darg)])
       preps dargs)
with _ -> failwith "Lisql_display.display_preps_args: incompatible lengths"
and display_role_ikind ~obs store (p,modifs) ikind =
    match string_of_uri_role ~obs store ikind p with
    | `Subject, s -> `Kwd "has" :: `Space :: display_role_modifs modifs @ `URI (p, `Property, s, None) :: []
    | `Object, s -> `Kwd "is" :: `Space :: display_role_modifs modifs @ `URI (p, `Property, s, None) :: `Space :: `Kwd "of" :: []
    | _ -> assert false
and display_role_modifs modifs =
  let l = [] in
  let l = if modifs.Pred.direct then `Kwd "directly" :: `Space :: l else l in
  let l = if modifs.Pred.symmetric then `Kwd "symmetrically" :: `Space :: l else l in
  let l = if modifs.Pred.transitive then `Kwd "transitively" :: `Space :: l else l in
  let l = if modifs.Pred.reflexive then `Kwd "optionally" :: `Space :: l else l in
  l
and display_funct ~obs store funct =
  [`URI (funct, `Functor, string_of_uri ~obs store funct, None)]
and display_prim ~obs store op =
  [`Prim op]
and display_name_no_blank ~obs store n =
  match n with
  | Rdf.Blank _ ->
      let np = store#description ~obs n in
      display_s1 ~obs store (default_context_of_s1 np) prec_or np
  | _ -> display_name ~obs store n
and display_name ~obs store n =
  match n with
  | Rdf.URI uri -> [`URI (uri, `Entity, string_of_uri ~obs store uri, image_of_uri ~obs store uri)]
  | Rdf.Literal (s, Rdf.Plain lang) -> [`Plain (s, lang)]
  | Rdf.Literal (s, Rdf.Typed uri) ->
    if uri = Namespace.uri_Assertion then
      (try
	 let c = store#unquote_assertion s in
	[`Quote (display_c ~obs store Nil prec_seq c)]
       with _ -> [`Typed (s, uri, string_of_uri ~obs store uri)]) (* fallback in case of syntax error when unquoting *)
    else if uri = Xsd.uri_string then
      [`Plain (s, "")]
    else
      let s_dt =
	if List.mem uri
	  [Xsd.uri_integer; Xsd.uri_decimal; Xsd.uri_double;
	   Xsd.uri_date; Xsd.uri_time; Xsd.uri_dateTime;
	   Xsd.uri_gYearMonth; Xsd.uri_gMonthDay; Xsd.uri_gDay; Xsd.uri_gMonth]
	then ""
	else string_of_uri ~obs store uri in
      [`Typed (s, uri, s_dt)]
  | Rdf.XMLLiteral xml -> [`Xml xml]
  | Rdf.Blank id -> [`Kwd ("_:" ^ id)] (* assert false: should not happen as we avoid to insert blank nodes in sentences *)

let of_c ~obs store c = display_c ~obs store (default_context_of_c c) prec_seq c
let of_s ~obs store s = display_s ~obs store (default_context_of_s s) prec_or s
let of_s1 ~obs store np = display_s1 ~obs store (default_context_of_s1 np) prec_or np
let of_s2 ~obs store det = display_s2 ~obs store (default_context_of_s2 det) None det
let of_p1 ~obs store f = display_p1_vp ~obs store (default_context_of_p1 f) prec_or f
let of_name ~obs store n = display_name ~obs store n
let of_name_no_blank ~obs store n = display_name_no_blank ~obs store n

let of_focus ~obs store foc =
  let c = command_of_focus foc in
  of_c ~obs store c

let rec to_string (l : t) : string = Common.prof "Lisql_display.to_string" (fun () ->
  String.concat "" (List.map to_string_token l))
and to_string_token : token -> string = function
  | `Space -> " "
  | `Kwd s -> s
  | `Var v -> "?" ^ v
  | `URI (uri, kind, s, img_opt) -> s
  | `Prim op -> op
  | `Plain (s,lang) -> "\"" ^ String.escaped s ^ "\"" ^ (if lang = "" then "" else "@" ^ lang)
  | `Typed (s, uri_dt, s_dt) -> "\"" ^ String.escaped s ^ "\"" ^ (if s_dt = "" then "" else "^^" ^ s_dt)
  | `Xml xml -> Xml.to_string xml
  | `List ltoks -> "(" ^ String.concat " " (List.map to_string ltoks) ^ ")"
  | `Tuple ltoks -> "(" ^ String.concat ", " (List.map to_string ltoks) ^ ")"
  | `Seq ltoks -> "(" ^ String.concat "; " (List.map to_string ltoks) ^ ")"
  | `And ltoks -> String.concat " and " (List.map to_string ltoks)
  | `Or ltoks -> String.concat " or " (List.map to_string ltoks)
  | `Not toks -> "not " ^ to_string toks
  | `Maybe toks -> "maybe " ^ to_string toks
  | `Brackets toks -> "(" ^ to_string toks ^ ")"
  | `Quote toks -> "``" ^ to_string toks ^ "''"
  | `Pair (force_indent,toks1,toks2) -> to_string toks1 ^ " " ^ to_string toks2
  | `Focus (foc,toks) -> to_string toks

let string_of_s ~obs store s = to_string (of_s ~obs store s)
let string_of_s1 ~obs store np = to_string (of_s1 ~obs store np)
let string_of_s2 ~obs store det = to_string (of_s2 ~obs store det)
let string_of_p1 ~obs store f = to_string (of_p1 ~obs store f)
let string_of_name ~obs store n = to_string (of_name ~obs store n)


let rec is_short_display (l : t) : bool =
  List.for_all is_short_token l
and is_short_token : token -> bool = function
  | `Space -> true
  | `Kwd _ -> true
  | `Var _ -> true
  | `URI _ -> true
  | `Prim _ -> true
  | `Plain _ -> true
  | `Typed _ -> true
  | `Xml _ -> false
  | `List _ -> false
  | `Tuple ltoks -> List.for_all is_short_display ltoks
  | `Seq _ -> false
  | `Seq _ -> false
  | `And _ -> false
  | `Or _ -> false
  | `Not toks -> is_short_display toks
  | `Maybe toks -> is_short_display toks
  | `Brackets _ -> false
  | `Quote toks -> is_short_display toks
  | `Pair (force_indent,toks1,toks2) -> not force_indent && is_short_display toks1 && is_short_display toks2
  | `Focus (foc,toks) -> is_short_display toks

class virtual markup_ctx =
  let tab = "     " in
  object (self)
    val nl = "\n"
    method indent = 
      let new_ctx = {< nl = nl ^ tab; >} in
      new_ctx#newline;
      new_ctx

    method open_focus (foc : focus) = ()
    method close_focus (foc : focus) = ()

    method virtual kwd : string -> unit
    method newline = self#kwd nl
    method space = self#kwd " "
    method virtual var : var -> unit
    method virtual uri : Uri.t -> kind -> string -> Uri.t option -> unit
    method virtual prim : string -> unit
    method virtual plain : string -> string -> unit
    method virtual typed : string -> Uri.t -> string -> unit
    method virtual xml : Xml.xml -> unit
    (* at beginning of focus *)
    method focus_space (foc : focus) = ()
    method focus_newline (foc : focus) = ()
    (* at end of focus *)
    method space_focus (foc : focus) = ()
    method newline_focus (foc : focus) = ()
  end


let rec markup after_indent(* not used*) (ctx : markup_ctx) = function
  | [] -> ()
  | tok::l -> markup_token after_indent ctx tok; List.iter (markup_token false ctx) l
and markup_token after_indent ctx = function
  | `Space -> ctx#space
  | `Kwd s -> ctx#kwd s
  | `Var v -> ctx#var v
  | `URI (uri,kind,s,img) -> ctx#uri uri kind s img
  | `Prim op -> ctx#prim op
  | `Plain (s,lang) -> ctx#plain s lang
  | `Typed (s, uri_dt, s_dt) -> ctx#typed s uri_dt s_dt
  | `Xml xml -> ctx#xml xml
  | `Tuple [] -> ctx#kwd "()"
  | `Tuple (toks::ltoks) ->
      if List.for_all is_short_display (toks::ltoks)
      then begin
	ctx#kwd "(";
	markup false ctx toks;
	List.iter (fun toks -> ctx#kwd ", "; markup false ctx toks) ltoks;
	ctx#kwd ")" end
      else begin
	ctx#kwd "(";
	markup true ctx#indent toks;
	List.iter (fun toks -> ctx#kwd ","; markup true ctx#indent toks) ltoks;
	ctx#kwd ")"
      end
  | `List [] -> assert false
  | `List (toks::ltoks) ->
      markup false ctx toks;
      List.iter (fun toks -> ctx#newline; markup false ctx toks) ltoks
  | `Seq [] -> assert false
  | `Seq (toks::ltoks) ->
      markup false ctx toks;
      List.iter (fun toks -> ctx#kwd "; "; ctx#newline; markup false ctx toks) ltoks
  | `And [] -> assert false
  | `And (toks::ltoks) ->
      markup false ctx toks;
      List.iter (fun toks -> ctx#kwd " and "; ctx#newline; markup false ctx toks) ltoks
  | `Or [] -> assert false
  | `Or (toks::ltoks) ->
      markup false ctx toks;
      List.iter
	(fun toks ->
	  ctx#newline; ctx#kwd " or ";
	  if is_short_display toks
	  then markup false ctx toks
	  else markup true ctx#indent toks)
	ltoks
  | `Not toks ->
      ctx#kwd "not";
      if is_short_display toks
      then begin ctx#space; markup false ctx toks end
      else markup true ctx#indent toks
  | `Maybe toks ->
      ctx#kwd "maybe";
      if is_short_display toks
      then begin ctx#space; markup false ctx toks end
      else markup true ctx#indent toks
  | `Brackets toks ->
      markup true ctx#indent toks
  | `Quote toks ->
      if true || is_short_display toks
      then begin ctx#kwd "``"; markup false ctx toks; ctx#kwd "''" end
      else begin ctx#kwd "``"; markup true ctx#indent toks; ctx#kwd "''" end
  | `Pair (force_indent,toks1,toks2) ->
      if not force_indent && is_short_display toks1 && is_short_display toks2
      then begin markup false ctx toks1; ctx#space; markup false ctx toks2 end
      else begin markup false ctx toks1; markup true ctx#indent toks2 end
  | `Focus (foc,toks) ->
      let short = is_short_display toks in
      ctx#open_focus foc;
      if short (*not after_indent*)
      then ctx#focus_space foc
      else ctx#focus_newline foc;
      markup false ctx toks;
      ctx#close_focus foc;
      if short (*not after_indent*)
      then ctx#space_focus foc
      else ctx#newline_focus foc
