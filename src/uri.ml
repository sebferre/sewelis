(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

type t = string

(* makes an absolute URI from a qname, given namespace and prefix *)
(* assumes qname starts with prefix *)
let make namespace prefix =
  let lp = String.length prefix in
  fun qname ->
    assert (String.sub qname 0 lp = prefix);
    namespace ^ String.sub qname lp (String.length qname - lp)

let relative base uri =
  let k = String.length base in
  if String.length uri > k && String.sub uri 0 k = base
  then String.sub uri k (String.length uri - k)
  else uri

let is_absolute =
  let re = Str.regexp "[a-zA-Z][-+.a-zA-Z0-9]*:" in
  fun reluri ->
    Str.string_match re reluri 0

let absolute base uri_rel =
  if is_absolute uri_rel
  then uri_rel
  else base ^ uri_rel

let belong_to_namespace ns uri =
  let k = String.length ns in
  let n = String.length uri in
  k <= n && String.sub uri 0 k = ns

let rec qname xmlns uri =
  (* xmlns is an association list between prefixes (e.g., rdf:) and uris *)
  match xmlns with
  | [] -> failwith "Uri.qname"
  | (pre,ns)::l ->
      let k = String.length ns in
      let n = String.length uri in
      if k < n && String.sub uri 0 k = ns && not (String.contains uri ' ')
      then pre, String.sub uri k (n - k) (* TODO : should check that the fragment is valid! see RFC 3986 *)
      else qname l uri

let unqualified xmlns (pre,name) =
  try List.assoc pre xmlns ^ name
  with Not_found -> failwith ("Uri.unqualified: undefined namespace " ^ pre)

let uri xmlns qname =
  try
    let k = 1 + String.index qname ':' in
    let pre = String.sub qname 0 k in
    let name = String.sub qname k (String.length qname - k) in
    unqualified xmlns (pre,name)
  with Not_found ->
    invalid_arg "Uri.uri: invalid qname or missing namespace definition"

let resolve base xmlns name =
  try uri xmlns name
  with _ -> absolute base name

let as_filename uri =
  let n = String.length uri in
  if n > 7 && String.sub uri 0 7 = "file://"
  then Some (String.sub uri 7 (n - 7))
  else None
