(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Name = Name.Make
module Extension = Extension.Make

module type ELT =
  sig
    type t
    val compare : t -> t -> int
    val from_name : Name.t -> t option
    val to_name : t -> Name.t
  end

module Make (Elt : ELT) =
  struct
    module Set = Set.Make(Elt)

    type find_elt_return = NoVar | UnboundVar | InvalidVal | Val of Elt.t

    let find_elt x m : find_elt_return =
      Common.prof "Store_order.find_elt" (fun () ->
	if x = ""
	then NoVar
	else
	  try
	    match Elt.from_name (List.assoc x m) with
	    | Some e -> Val e
	    | None -> InvalidVal
	  with Not_found -> UnboundVar)

    class store =
      object (self)
	val state : Set.t Tarpit.state = new Tarpit.state Set.empty

	method add : Name.t -> unit =
	  fun n -> Common.prof "Store_order.store#add" (fun () ->
	    Option.iter
	      (fun e -> state#update (fun bt -> if Set.mem e bt then None else Some (Set.add e bt)))
	      (Elt.from_name n))

	method remove : Name.t -> unit =
	  fun n -> Common.prof "Store_order.store#remove" (fun () ->
	    Option.iter
	      (fun e -> state#update (fun bt -> if Set.mem e bt then Some (Set.remove e bt) else None))
	      (Elt.from_name n))

	method private split ~obs l =
	  Common.prof "Store_order.store#split" (fun () ->
	    Set.split l (state#contents ~obs))

	method mem : obs:Tarpit.observer -> Elt.t -> bool =
	  fun ~obs e -> Common.prof "Store_order.store#mem" (fun () ->
	    Set.mem e (state#contents ~obs))

	method has_successor : obs:Tarpit.observer -> Elt.t -> bool =
	  fun ~obs e -> Common.prof "Store_order.store#has_successor" (fun () ->
	    let left, present, right = self#split ~obs e in
	    not (Set.is_empty right))
	      
	method has_predecessor : obs:Tarpit.observer -> Elt.t -> bool =
	  fun ~obs e -> Common.prof "Store_order.store#has_predecessor" (fun () ->
	    let left, present, right = self#split ~obs e in
	    not (Set.is_empty left))
	      
	method get_successor : obs:Tarpit.observer -> Elt.t -> Elt.t option =
	  fun ~obs e ->
	    let left, present, right = self#split ~obs e in
	    try Some (Set.min_elt right)
	    with Not_found -> None

	method get_predecessor : obs:Tarpit.observer -> Elt.t -> Elt.t option =
	  fun ~obs e ->
	    let left, present, right = self#split ~obs e in
	    try Some (Set.max_elt left)
	    with Not_found -> None

	method cardinal : obs:Tarpit.observer -> int =
	  fun ~obs -> Common.prof "Store_order.store#cardinal" (fun () ->
	    Set.cardinal (state#contents ~obs))

	method min_elt : obs:Tarpit.observer -> Elt.t option =
	  fun ~obs -> Common.prof "Store_order.store#min_elt" (fun () ->
	    try Some (Set.min_elt (state#contents ~obs))
	    with Not_found -> None)

	method max_elt : obs:Tarpit.observer -> Elt.t option =
	  fun ~obs -> Common.prof "Store_order.store#max_elt" (fun () ->
	    try Some (Set.max_elt (state#contents ~obs))
	    with Not_found -> None)

      	method fold : 'a. obs:Tarpit.observer -> ('a -> Elt.t -> 'a) -> 'a -> 'a =
	  fun ~obs f init -> Common.prof "Store_order.store#fold" (fun () ->
	    Set.fold (fun e res -> f res e) (state#contents ~obs) init)

	method fold_successors : 'a. obs:Tarpit.observer -> ('a -> Elt.t -> 'a) -> 'a -> Elt.t -> 'a =
	  fun ~obs f init e -> Common.prof "Store_order.store#fold_successors" (fun () ->
	    let left, present, right = self#split ~obs e in
	    Set.fold (fun e res -> f res e) right init)

	method fold_predecessors : 'a. obs:Tarpit.observer -> ('a -> Elt.t -> 'a) -> 'a -> Elt.t -> 'a =
	  fun ~obs f init e -> Common.prof "Store_order.store#fold_predecessors" (fun () ->
	    let left, present, right = self#split ~obs e in
	    Set.fold (fun e res -> f res e) left init)

	method private succ ~obs (x : Extension.var) (y : Extension.var) =
	  object
	    inherit Extension.t
	    method vars = [x; y]
	    method string = "succ(" ^ x ^ "," ^ y ^ ")"
	    method fold =
	      fun store ff acc m ->
		Common.prof "Store_order.store#succ" (fun () ->
		  match find_elt x m with
		  | Val e1 ->
		      ( match find_elt y m with
		      | Val e2 ->
			  ( match self#get_successor ~obs e1 with
			  | Some e when e = e2 -> ff acc m
			  | _ -> acc)
		      | InvalidVal -> acc
		      | NoVar ->
			  if self#has_successor ~obs e1
			  then ff acc m
			  else acc
		      | UnboundVar ->
			  ( match self#get_successor ~obs e1 with
			  | Some e2 -> ff acc ((y, Elt.to_name e2)::m)
			  | None -> acc))
		  | InvalidVal -> acc
		  | NoVar ->
		      ( match find_elt y m with
		      | Val e2 ->
			  if self#has_predecessor ~obs e2
			  then ff acc m
			  else acc
		      | InvalidVal -> acc
		      | NoVar ->
			  if self#cardinal ~obs >= 2
			  then ff acc m
			  else acc
		      | UnboundVar ->
			  ( match self#min_elt ~obs with
			  | Some min_elt ->
			      self#fold ~obs
				(fun acc e2 ->
				  if e2 <> min_elt
				  then ff acc ((y, Elt.to_name e2)::m)
				  else acc)
				acc
			  | None -> acc))
		  | UnboundVar ->
		      ( match find_elt y m with
		      | Val e2 ->
			  ( match self#get_predecessor ~obs e2 with
			  | Some e1 -> ff acc ((x, Elt.to_name e1)::m)
			  | None -> acc)
		      | InvalidVal -> acc
		      | NoVar ->
			  ( match self#max_elt ~obs with
      			  | Some max_elt ->
			      self#fold ~obs
				(fun acc e1 ->
				  if e1 <> max_elt
				  then ff acc ((x, Elt.to_name e1)::m)
				  else acc)
				acc
			  | None -> acc)
		      | UnboundVar ->
			  self#fold ~obs
			    (fun acc e1 ->
			      let m = (x, Elt.to_name e1)::m in
			      match self#get_successor ~obs e1 with
			      | Some e2 -> ff acc ((y, Elt.to_name e2)::m)
			      | None -> acc)
			    acc))
	  end

	    
	method private lesser strict ~obs (x : Extension.var) (y : Extension.var) =
	  object
	    inherit Extension.t
	    method vars = [x; y]
	    method string = x ^ " <= " ^ y
	    method fold (*: 'a. 'a Extension.fold*) =
	      fun store ff acc m ->
		Common.prof "Store_order.store#lesser" (fun () ->
		  match find_elt x m with
		  | Val e1 ->
		      ( match find_elt y m with
		      | Val e2 ->
			  let cmp = Elt.compare e1 e2 in
			  if (if strict then cmp < 0 else cmp <= 0)
			  then ff acc m
			  else acc
		      | InvalidVal -> acc
		      | NoVar ->
			  if not strict || self#has_successor ~obs e1
			  then ff acc m
			  else acc
		      | UnboundVar ->
			  let acc = if strict then acc else ff acc ((y, Elt.to_name e1)::m) in
			  let acc =
			    self#fold_successors ~obs
			      (fun acc e2 -> ff acc ((y, Elt.to_name e2)::m))
			      acc e1 in
			  acc)
		  | InvalidVal -> acc
		  | NoVar ->
		      ( match find_elt y m with
		      | Val e2 ->
			  if not strict || self#has_predecessor ~obs e2
			  then ff acc m
			  else acc
		      | InvalidVal -> acc
		      | NoVar ->
			  if not strict || self#cardinal ~obs >= 2
			  then ff acc m
			  else acc
		      | UnboundVar ->
			  ( match self#min_elt ~obs with
			  | Some min_elt ->
			      self#fold ~obs
				(fun acc e2 ->
				  if not strict || e2 <> min_elt
				  then ff acc ((y, Elt.to_name e2)::m)
				  else acc)
				acc
			  | None -> acc))
		  | UnboundVar ->
		      ( match find_elt y m with
		      | Val e2 ->
			  let acc =
			    self#fold_predecessors ~obs
			      (fun acc e1 -> ff acc ((x, Elt.to_name e1)::m))
			      acc e2 in
			  let acc = if strict then acc else ff acc ((x, Elt.to_name e2)::m) in
			  acc
		      | InvalidVal -> acc
		      | NoVar ->
			  ( match self#max_elt ~obs with
      			  | Some max_elt ->
			      self#fold ~obs
				(fun acc e1 ->
				  if not strict || e1 <> max_elt
				  then ff acc ((x, Elt.to_name e1)::m)
				  else acc)
				acc
			  | None -> acc)
		      | UnboundVar ->
			  self#fold ~obs
			    (fun acc e1 ->
			      let m = (x, Elt.to_name e1)::m in
			      let acc = if strict then acc else ff acc ((y, Elt.to_name e1)::m) in
			      let acc =
				self#fold_successors ~obs
				  (fun acc e2 -> ff acc ((y, Elt.to_name e2)::m))
				  acc e1 in
			      acc)
			    acc))
	  end

	method succ_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y -> self#succ ~obs x y

	method neighbour_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y ->
	    Extension.union
	      (self#succ_extension ~obs x y)
	      (self#succ_extension ~obs y x)

	method leq_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y -> self#lesser false ~obs x y

	method lt_extension : obs:Tarpit.observer -> Extension.var -> Extension.var -> Extension.t =
	  fun ~obs x y -> self#lesser true ~obs x y

(*    method virtual diff_extension : Extension.var -> Extension.var -> Extension.var -> Extension.t *)
      end

  end

module Integer =
  Make
    (struct
      type t = int
      let from_name = function
	| Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_integer ->
	    ( try Some (int_of_string s) with _ -> None)
	| _ -> None
      let to_name i = Rdf.Literal (string_of_int i, Rdf.Typed Xsd.uri_integer)
      let compare i1 i2 = Pervasives.compare i1 i2
    end)

module Double =
  Make
    (struct
      type t = float * Uri.t
      let from_name = function
	| Rdf.Literal (s, Rdf.Typed dt)
	  when List.mem dt [Xsd.uri_decimal; Xsd.uri_double] ->
	    ( try Some (float_of_string s, dt) with _ -> None)
	| _ -> None
      let to_name (f,dt) = Rdf.Literal (string_of_float f, Rdf.Typed dt)
      let compare (f1,_) (f2,_) = Pervasives.compare f1 f2
    end)

module Date =
  Make
    (struct
      type t = string * Uri.t
      let from_name = function
	| Rdf.Literal (s, Rdf.Typed dt)
	  when List.mem dt [Xsd.uri_dateTime; Xsd.uri_date; Xsd.uri_gYearMonth; Xsd.uri_gYear] -> Some (s,dt)
	| _ -> None
      let to_name (s,dt) = Rdf.Literal (s, Rdf.Typed dt)
      let compare (s1,_) (s2,_) = Pervasives.compare s1 s2
    end)

module Time =
  Make
    (struct
      type t = string
      let from_name = function
	| Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_time -> Some s
	| _ -> None
      let to_name s = Rdf.Literal (s, Rdf.Typed Xsd.uri_time)
      let compare s1 s2 = Pervasives.compare s1 s2
    end)
