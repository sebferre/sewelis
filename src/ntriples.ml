(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(*
   created: 28/07/2011
   description: parsing and printing of N-Triples files
   dependencies: rdf.ml, DCG parsers
*)

let unescape_string (s : string) : string =
  let r = ref 0 in
  let w = ref 0 in
  let l = String.length s in
  while !r < l do
    let c = s.[!r] in
    if c = '\\'
    then begin
      incr r;
      match s.[!r] with
      | 't' -> s.[!w] <- '\t'
      | 'r' -> s.[!w] <- '\r'
      | 'n' -> s.[!w] <- '\n'
      | '\\' -> s.[!w] <- '\\'
      | '"' -> s.[!w] <- '"'
      | 'u' ->
	  incr r;
	  let utf8 = Unicode.utf8_of_codepoint (Unicode.codepoint_of_string (String.sub s !r 4)) in
	  let l_utf8 = String.length utf8 in (* in [1,3] *)
	  String.blit utf8 0 s !w l_utf8;
	  r := !r + 4 - 1;
	  w := !w + l_utf8 - 1
      | 'U' ->
	  incr r;
	  let utf8 = Unicode.utf8_of_codepoint (Unicode.codepoint_of_string (String.sub s !r 8)) in
	  let l_utf8 = String.length utf8 in (* in [1,4] *)
	  String.blit utf8 0 s !w l_utf8;
	  r := !r + 8 - 1;
	  w := !w + l_utf8 - 1
      | c' -> invalid_arg "Ntriples.unescape_string: unknown escape code" end
    else s.[!w] <- c;
    incr r;
    incr w
  done;
  String.sub s 0 !w

let parse_comment = dcg "comment"
    [ s = match "[#][^\r\n]*" as "comment" -> s ]
let parse_space_star = dcg "space?"
    [ s = match "[ \t]*" as "space?" -> "" ]
let parse_space_plus = dcg "space"
    [ s = match "[ \t]+" as "space" -> " " ]
let parse_dot = dcg "dot"
    [ s = match "[ \t]*\\.[ \t]*" as "dot" -> "." ]
let parse_eoln = dcg "eoln"
    [ s = match "\\(\r\\|\n\\|\r\n\\)" as "eoln" -> s ]
let parse_absoluteURI = dcg "absoluteURI"
    [ s = match "[^>\t\r\n]*" as "absoluteURI" -> s ]
let parse_name = dcg "name"
    [ s = match "[A-Za-z][A-Za-z0-9]*" as "name" -> s ]
let parse_string = dcg "string"
    [ s = match "[^\\\"\t\r\n]*\\([\\].[^\\\"\t\r\n]*\\)*" as "string"; s1 in "invalid escape in string" (try [unescape_string s] with _ -> []) -> s1 ]
let parse_language = dcg "language"
    [ s = match "[a-z]+\\(-[a-z0-9]+\\)*" as "language" -> s ]

open Rdf

let rec parse_ntripleDoc = dcg
    [ lt = parse_lines -> lt ]
and parse_lines = dcg
    [ lt1 = parse_line then lt2 = parse_lines -> lt1@lt2
    | EOF -> [] ]
and parse_line = dcg
    [ _ = parse_space_plus; lt1 = parse_line_aux; _ = parse_eoln -> lt1
    | lt1 = parse_line_aux; _ = parse_eoln -> lt1 ]
and parse_line_aux = dcg
    [ _ = parse_comment -> []
    | t = parse_triple -> [t]
    |  -> [] ]
and parse_triple = dcg
    [ s = parse_subject; _ = parse_space_plus;
      p = parse_predicate; _ = parse_space_plus;
      o = parse_object; _ = parse_dot -> {s; p; o; t_opt = None} ]
and parse_subject = dcg
    [ uri = parse_uriref -> URI uri
    | name = parse_namedNode -> Blank name ]
and parse_predicate = dcg
    [ uri = parse_uriref -> uri ]
and parse_object = dcg
    [ uri = parse_uriref -> URI uri
    | name = parse_namedNode -> Blank name
    | lit = parse_literal -> lit ]
and parse_uriref = dcg
    [ "<"; uri = parse_absoluteURI; ">" -> uri ]
and parse_namedNode = dcg
    [ "_:"; name = parse_name -> name ]
and parse_literal = dcg
    [ "\""; s = parse_string; "\""; t = parse_literal_type -> Literal (s,t) ]
and parse_literal_type = dcg
    [ "@"; lang = parse_language -> Plain lang
    | "^^"; uri = parse_uriref -> Typed uri
    |  -> Typed Xsd.uri_string ]

let parse_file (filename : string) : triple list =
  let ch = open_in filename in
  let res = snd (Dcg.once parse_ntripleDoc [] (Matcher.cursor_of_channel ch)) in
  close_in ch;
  res

open Format

let rec print_triples fmt l =
  List.iter (print_triple fmt) l
and print_triple fmt {s; p; o; t_opt} = Common.prof "Ntriples.print_triple" (fun () ->
  match t_opt with
    | None ->
      print_subject fmt s; pp_print_string fmt " ";
      print_predicate fmt p; pp_print_string fmt " ";
      print_object fmt o; pp_print_string fmt " .";
      pp_print_newline fmt ()
    | Some t ->
      print_triple fmt {s=t; p=Rdf.uri_subject; o=s; t_opt=None};
      print_triple fmt {s=t; p=Rdf.uri_predicate; o=URI p; t_opt=None};
      print_triple fmt {s=t; p=Rdf.uri_object; o=o; t_opt=None})
and print_subject fmt = function
  | URI uri -> print_uriref fmt uri
  | Blank name -> print_namedNode fmt name
  | Literal (s,t) -> prerr_string "error: literal as subject: "; print_endline s
  | XMLLiteral _ -> prerr_endline "error: XML literal as subject"
and print_predicate fmt uri =
  print_uriref fmt uri
and print_object fmt = function
  | URI uri -> print_uriref fmt uri
  | Blank name -> print_namedNode fmt name
  | Literal (s,t) -> print_literal fmt (s,t)
  | XMLLiteral _ -> prerr_endline "error: XML literal as object"
and print_uriref fmt uri = Common.prof "Ntriples.print_uriref" (fun () ->
  pp_print_string fmt "<";
  pp_print_string fmt (Turtle.escape_uri uri);
  pp_print_string fmt ">")
and print_namedNode fmt name = Common.prof "Ntriples.print_namedNode" (fun () ->
  pp_print_string fmt "_:";
  pp_print_string fmt name)
and print_literal fmt (s,t) = Common.prof "Ntriples.print_literal" (fun () ->
  pp_print_string fmt "\"";
  pp_print_string fmt (Turtle.escape_shortString s);
  pp_print_string fmt "\"";
  print_literal_type fmt t)
and print_literal_type fmt = function
  (*| Plain "" -> ()*)
  | Plain lang -> pp_print_string fmt "@"; pp_print_string fmt lang
  | Typed uri -> pp_print_string fmt "^^"; print_uriref fmt uri

let output (out : out_channel) (triples : triple list) =
  let fmt = Format.formatter_of_out_channel out in
  print_triples fmt triples

let print (triples : triple list) : unit =
  let fmt = Format.std_formatter in
  print_triples fmt triples

let to_string triples : string =
  let fmt = Format.str_formatter in
  print_triples fmt triples;
  Format.flush_str_formatter ()

(*
let rec print_ntripleDoc = ipp
    [ lt -> print_lines of lt ]
and print_lines = ipp
    [ t::lt -> print_line of t then print_lines of lt
    | [] -> EOF ]
and print_line = ipp
    [ {s; p; o; t_opt=None} ->
        print_subject of s; print_space;
        print_predicate of p; print_space;
        print_object of o; print_eoln
    | {s; p; o; t_opt=Some t} ->
	print_line of {s=t; p=Rdf.uri_subject; o=s; t_opt=None};
	print_line of {s=t; p=Rdf.uri_predicate; o=URI p; t_opt=None};
	print_line of {s=t; p=Rdf.uri_object; o=o; t_opt=None} ]
and print_subject = ipp
    [ URI uri -> print_uriref of uri
    | Blank name -> print_namedNode of name ]
and print_predicate = ipp
    [ uri -> print_uriref of uri ]
and print_object = ipp
    [ URI uri -> print_uriref of uri
    | Blank name -> print_namedNode of name
    | Literal (s,t) -> print_literal of (s,t) ]
and print_uriref = ipp
    [ uri -> "<"; '(Turtle.escape_uri uri); ">" ]
and print_namedNode = ipp
    [ name -> "_:"; 'name ]
and print_literal = ipp
    [ s,t -> "\""; '(Turtle.escape_shortString s); "\""; print_literal_type of t ]
and print_literal_type = ipp
    [ Plain "" -> 
    | Plain lang -> "@"; 'lang
    | Typed uri -> "^^"; print_uriref of uri ]
and print_space = ipp [ _ -> " " ]
and print_eoln = ipp [ _ -> " .\n" ]

let output (out : out_channel) (triples : triple list) =
  Ipp.once print_ntripleDoc triples (Printer.cursor_of_formatter (Format.formatter_of_out_channel out)) ()

let print (triples : triple list) : unit =
  Ipp.once print_ntripleDoc triples (Printer.cursor_of_formatter Format.std_formatter) ()

let to_string triples : string =
  let buf_cursor = new Printer.buffer_cursor in
  Ipp.once print_ntripleDoc triples buf_cursor ();
  buf_cursor#contents
*)
