(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Make =
  struct
    type t = Rdf.thing

        (* datatypes *)
    let xsd_uri = Rdf.URI Xsd.uri_anyURI
    let xsd_lang = Rdf.URI Xsd.uri_language
    let xsd_string = Rdf.URI Xsd.uri_string
    let xsd_integer = Rdf.URI Xsd.uri_integer

        (* properties *)
    let rdf_subject = Rdf.URI Rdf.uri_subject
    let rdf_predicate = Rdf.URI Rdf.uri_predicate
    let rdf_object = Rdf.URI Rdf.uri_object
    let rdf_type = Rdf.URI Rdf.uri_type
    let rdf_first = Rdf.URI Rdf.uri_first
    let rdf_rest = Rdf.URI Rdf.uri_rest
    let rdf_n n = Rdf.URI (Rdf.uri_n n)
    let rdf_value = Rdf.URI Rdf.uri_value
    let rdfs_subClassOf = Rdf.URI Rdfs.uri_subClassOf
    let rdfs_subPropertyOf = Rdf.URI Rdfs.uri_subPropertyOf
    let rdfs_domain = Rdf.URI Rdfs.uri_domain
    let rdfs_range = Rdf.URI Rdfs.uri_range
    let rdfs_member = Rdf.URI Rdfs.uri_member
    let rdfs_label = Rdf.URI Rdfs.uri_label
    let rdfs_comment = Rdf.URI Rdfs.uri_comment
    let rdfs_seeAlso = Rdf.URI Rdfs.uri_seeAlso
    let rdfs_isDefinedBy = Rdf.URI Rdfs.uri_isDefinedBy
    let owl_inverseOf = Rdf.URI Owl.uri_inverseOf
    let rdfc_self = Rdf.URI "rdfc#self"

        (* classes *)
    let rdf_Property = Rdf.URI Rdf.uri_Property
    let rdf_Statement = Rdf.URI Rdf.uri_Statement
    let rdf_Bag = Rdf.URI Rdf.uri_Bag
    let rdf_Seq = Rdf.URI Rdf.uri_Seq
    let rdf_Alt = Rdf.URI Rdf.uri_Alt
    let rdf_List = Rdf.URI Rdf.uri_List
    let rdf_XMLLiteral = Rdf.URI Rdf.uri_XMLLiteral
    let rdfs_Resource = Rdf.URI Rdfs.uri_Resource
    let rdfs_Class = Rdf.URI Rdfs.uri_Class
    let rdfs_Literal = Rdf.URI Rdfs.uri_Literal
    let rdfs_Datatype = Rdf.URI Rdfs.uri_Datatype
    let rdfs_Container = Rdf.URI Rdfs.uri_Container
    let rdfs_ContainerMembershipProperty = Rdf.URI Rdfs.uri_ContainerMembershipProperty
    let owl_TransitiveProperty = Rdf.URI Owl.uri_TransitiveProperty
    let owl_SymmetricProperty = Rdf.URI Owl.uri_SymmetricProperty
    let owl_AsymmetricProperty = Rdf.URI Owl.uri_AssymetricProperty
    let owl_ReflexiveProperty = Rdf.URI Owl.uri_ReflexiveProperty
    let owl_IrreflexiveProperty = Rdf.URI Owl.uri_IrreflexiveProperty
    let owl_FunctionalProperty = Rdf.URI Owl.uri_FunctionalProperty
    let owl_InverseFunctionalProperty = Rdf.URI Owl.uri_InverseFunctionalProperty

      (* rdfc for RDF Computation (tentative) *)
    let rdfc_Primitive = Rdf.URI "rdfc#Primitive"
    let rdfc_Predicate = Rdf.URI "rdfc#Predicate"
    let rdfc_Function = Rdf.URI "rdfc#Function"
    let rdfc_Procedure = Rdf.URI "rdfc#Procedure"
    let rdfc_Aggregator = Rdf.URI "rdfc#Aggregator"
    let rdfc_Syntax = Rdf.URI "rdfc#Syntax"

        (* roles *)
    let rdfc_from = Rdf.URI "rdfc#from"
    let rdfc_to = Rdf.URI "rdfc#to"
    let rdfc_with = Rdf.URI "rdfc#with"
    let rdfc_by = Rdf.URI "rdfc#by"

        (* predicates *)
    let rdfc_URI = Rdf.URI "rdfc#URI"
    let rdfc_literal = Rdf.URI "rdfc#literal"
    let rdfc_blank = Rdf.URI "rdfc#blank"
    let rdfc_eq = Rdf.URI "rdfc#eq"
    let rdfc_neq = Rdf.URI "rdfc#neq"
    let rdfc_lt = Rdf.URI "rdfc#lt"
    let rdfc_leq = Rdf.URI "rdfc#leq"
    let rdfc_gt = Rdf.URI "rdfc#gt"
    let rdfc_geq = Rdf.URI "rdfc#geq"
    let rdfc_between = Rdf.URI "rdfc#between"
    let rdfc_matches = Rdf.URI "rdfc#matches"
    let rdfc_beginsWith = Rdf.URI "rdfc#beginsWith"
    let rdfc_endsWith = Rdf.URI "rdfc#endsWith"
    let rdfc_contains = Rdf.URI "rdfc#contains"
    let rdfc_even = Rdf.URI "rdfc#even"
    let rdfc_odd = Rdf.URI "rdfc#odd"

        (* functions *)
    let rdfc_uri = Rdf.URI "rdfc#uri"
    let rdfc_str = Rdf.URI "rdfc#str"
    let rdfc_lang = Rdf.URI "rdfc#lang"
    let rdfc_datatype = Rdf.URI "rdfc#datatype"
    let rdfc_add = Rdf.URI "rdfc#add"
    let rdfc_sub = Rdf.URI "rdfc#sub"
    let rdfc_mul = Rdf.URI "rdfc#mul"
    let rdfc_div = Rdf.URI "rdfc#div"

        (* procedures *)
    let rdfc_print = Rdf.URI "rdfc#print"
    let rdfc_shell = Rdf.URI "rdfc#shell"

        (* aggregators *)
    let rdfc_choose = Rdf.URI "rdfc#choose"
    let rdfc_unique = Rdf.URI "rdfc#unique"
    let rdfc_count = Rdf.URI "rdfc#count"
    let rdfc_sum = Rdf.URI "rdfc#sum"
    let rdfc_concat = Rdf.URI "rdfc#concat"


    let contents : t -> string = function
      | Rdf.XMLLiteral xml -> Xml.to_string xml
      | Rdf.Literal (s,d) -> s
      | Rdf.Blank id -> "_:" ^ id
      | Rdf.URI uri -> "<" ^ uri ^ ">"

	(* entities *)
    let rdf_nil = Rdf.URI Rdf.uri_nil


    let typed_literal (s : string) (dt : Uri.t) =
      Rdf.Literal (s, Rdf.Typed dt)

    let plain_literal (s : string) (lang : string) =
      Rdf.Literal (s, Rdf.Plain lang)

    let string_date_of_year_month_day y m d =
      let s = Printf.sprintf "%4d-%2d-%2d" y m d in
      for i = 0 to String.length s - 1 do
	if s.[i] = ' ' then s.[i] <- '0'
      done;
      s

    let date_of_year_month_day y m d =
      Rdf.Literal (string_date_of_year_month_day y m d, Rdf.Typed Xsd.uri_date)

    let string_time_of_hour_min_sec h m s =
      let s = Printf.sprintf "%2d:%2d:%2d" h m s in
      for i = 0 to String.length s - 1 do
	if s.[i] = ' ' then s.[i] <- '0'
      done;
      s

    let time_of_hour_min_sec h m s =
      Rdf.Literal (string_time_of_hour_min_sec h m s, Rdf.Typed Xsd.uri_time)

    let string_dateTime y m d h min sec =
      string_date_of_year_month_day y m d ^
      "T" ^
      string_time_of_hour_min_sec h min sec

    let dateTime y m d h min sec =
      Rdf.Literal (string_dateTime y m d h min sec, Rdf.Typed Xsd.uri_dateTime)

    let string_dateTime_of_UnixTime (t : float) : string =
      let tm = Unix.gmtime t in
      string_date_of_year_month_day (1900 + tm.Unix.tm_year) (1 + tm.Unix.tm_mon) tm.Unix.tm_mday ^
      "T" ^
      string_time_of_hour_min_sec tm.Unix.tm_hour tm.Unix.tm_min tm.Unix.tm_sec ^
      "Z"
(*
      let s = Printf.sprintf "%4d-%2d-%2dT%2d:%2d:%2dZ"
	  (1900 + tm.Unix.tm_year) (1 + tm.Unix.tm_mon) tm.Unix.tm_mday
	  tm.Unix.tm_hour tm.Unix.tm_min tm.Unix.tm_sec in
      for i = 0 to String.length s - 1 do
	if s.[i] = ' ' then s.[i] <- '0'
      done;
      s
*)

    let dateTime_of_UnixTime (t : float) : Rdf.thing =
      typed_literal (string_dateTime_of_UnixTime t) Xsd.uri_dateTime

    let string_date_of_UnixTime (t : float) : string =
      let tm = Unix.gmtime t in
      string_date_of_year_month_day (1900 + tm.Unix.tm_year) (1 + tm.Unix.tm_mon) tm.Unix.tm_mday ^
      "Z"
(*
      let s = Printf.sprintf "%4d-%2d-%2dZ"
	  (1900 + tm.Unix.tm_year) (1 + tm.Unix.tm_mon) tm.Unix.tm_mday in
      for i = 0 to String.length s - 1 do
	if s.[i] = ' ' then s.[i] <- '0'
      done;
      s
*)

    let date_of_UnixTime (t : float) : Rdf.thing =
      typed_literal (string_date_of_UnixTime t) Xsd.uri_date

    let uri_of_file file = "file://" ^ file

    let is_uri = function Rdf.URI _ -> true | _ -> false
    let is_xmlliteral = function Rdf.XMLLiteral _ -> true | _ -> false
    let is_literal = function Rdf.Literal _ -> true | _ -> false
    let is_blank = function Rdf.Blank _ -> true | _ -> false
    let is_rdf_n = function Rdf.URI uri -> Rdf.is_uri_n uri | _ -> false

    let as_uri = function Rdf.URI uri -> uri | n -> invalid_arg ("Name.as_uri: " ^ contents n)

    let is_primitive_class uri =
      uri = Rdf.uri_Statement || uri = Rdf.uri_XMLLiteral || uri = Rdfs.uri_Resource || uri = Rdfs.uri_Literal

    let is_primitive_property uri =
      uri = Rdf.uri_subject || uri = Rdf.uri_predicate || uri = Rdf.uri_object

(*    let unify_datatypes dt1 dt2 = *)
      (* * < string *)
      (* integer < decimal < double *)
      (* dateTime < date & time *)
      (* date < gYearMonth & gMonthDay *)
      (* gYearMonth < gYear & gMonth *)
      (* gMonthDay < gMonth & gDay *)

    let compare_literal (l1 : Rdf.literal) (l2 : Rdf.literal) =
      match l1, l2 with
	| (s1, Rdf.Typed dt1), (s2, Rdf.Typed dt2) ->
	  if List.for_all (fun dt -> List.mem dt [Xsd.uri_integer; Xsd.uri_decimal; Xsd.uri_double]) [dt1;dt2] then
	    try Pervasives.compare (float_of_string s1) (float_of_string s2)
	    with _ -> Pervasives.compare s1 s2
	  else
	    Pervasives.compare s1 s2
	| _ -> Pervasives.compare l1 l2

    let compare (n1 : t) (n2 : t) : int = 
      match n1, n2 with
	| Rdf.Literal l1, Rdf.Literal l2 -> compare_literal l1 l2
	| _ -> Pervasives.compare n1 n2

    let blank_id_prefix = "oid"

    let oid_of_blank id =
      let n = String.length id in
      if n >= 4 && String.sub id 0 3 = blank_id_prefix
      then int_of_string (String.sub id 3 (n-3))
      else invalid_arg ("Name.oid_of_blank: " ^ id)

    let of_oid oid = Rdf.Blank (blank_id_prefix ^ string_of_int oid)

    let of_string s = Rdf.Literal (s, Rdf.Typed Xsd.uri_string)
    let of_int i = Rdf.Literal (string_of_int i, Rdf.Typed Xsd.uri_integer)

    let escape_string (end_char : char) (s : string) : string =
      let l = String.length s in
      let buf = Buffer.create (2*l) in
      String.iter
	(function
	  | '\t' -> Buffer.add_string buf "\\t"
	  | '\n' -> Buffer.add_string buf "\\n"
	  | '\r' -> Buffer.add_string buf "\\r"
	  | '\\' -> Buffer.add_string buf "\\\\"
	  | c ->
	      if c = end_char then Buffer.add_char buf '\\';
	      Buffer.add_char buf c)
	s;
      Buffer.contents buf

    let string_of_uri_rel base uri =
      let rel_uri = Uri.relative base uri in
      "<" ^ Turtle.escape_uri rel_uri ^ ">"

    let string_of_uri base xmlns uri =
      try
	let pre, name = Uri.qname xmlns uri in
	pre ^ name
      with _ -> string_of_uri_rel base uri

    let string_of_shortString s =
      "\"" ^ Turtle.escape_shortString s ^ "\""

    let to_string base xmlns : t -> string =
      function
      | Rdf.XMLLiteral xml -> Xml.to_string xml
      | Rdf.Literal (s,d) ->
	  let quoted_s = "\"" ^ escape_string '"' s ^ "\""  in
	  ( match d with
	  (*| Rdf.Plain "" -> quoted_s (* should not happen *) *)
	  | Rdf.Plain lang -> quoted_s ^ "@" ^ lang
	  | Rdf.Typed uri ->
	      if uri = Xsd.uri_string
	      then quoted_s
	      else
		if List.mem uri
		    [ Xsd.uri_decimal; Xsd.uri_integer; Xsd.uri_duration; Xsd.uri_dateTime; Xsd.uri_time; Xsd.uri_date]
(*		      Xsd.uri_gYearMonth; Xsd.uri_gYear; Xsd.uri_gMonthDay; Xsd.uri_gDay; Xsd.uri_gMonth] *) (* not safe *)
		then s
		else quoted_s ^ "^^" ^ string_of_uri base xmlns uri)
      | Rdf.Blank id -> "_:" ^ id
      | Rdf.URI uri -> string_of_uri base xmlns uri

    let unescape_string (end_char : char) (s : string) : string =
      let r = ref 0 in
      let w = ref 0 in
      let l = String.length s in
      while !r < l do
	let c = s.[!r] in
	if c = '\\'
	then begin
	  incr r;
	  match s.[!r] with
	  | 't' -> s.[!w] <- '\t'
	  | 'r' -> s.[!w] <- '\r'
	  | 'n' -> s.[!w] <- '\n'
	  | '\\' -> s.[!w] <- '\\'
	  | 'u' -> invalid_arg "Name.unescape_string: Unicode codepoint unsupported"
	  | c' -> 
	      if c' = end_char
	      then s.[!w] <- c'
	      else invalid_arg "Name.unescape_string: unknown escape code" end
	else s.[!w] <- c;
	incr r;
	incr w
      done;
      String.sub s 0 !w

    let parse_uri_rel base = dcg "uri_rel"
	[ uri_rel = Turtle.parse_uriref -> Uri.absolute base uri_rel ]
    let parse_prefix = Turtle.parse_prefix (*dcg "prefix"
	[ s = match "[-a-z]*:" -> s ]*)
    let parse_qname xmlns = dcg "qname"
	[ pre, name = Turtle.parse_qname -> Uri.unqualified xmlns (pre, name) ]
	(* [ s = match "[-a-z0-9]+:[_0-9a-zA-Z]+\\([-.][_0-9a-zA-Z]+\\)*" -> s ] *)
    let parse_blank = Turtle.parse_nodeID (*dcg "blank"
	[ s = match "_:[_0-9a-zA-Z]+" -> String.sub s 2 (String.length s - 2) ] *)
    let parse_lang = Turtle.parse_language (* dcg "lang"
	[ s = match "[a-z]+\\(-[a-z0-9]+\\)*" -> s ] *)
    let parse_string = Turtle.parse_string (* dcg "string"
	[ s = match "\"[^\\\"]*\\([\\].[^\\\"]*\\)*\"" ->
	  let s1 = String.sub s 1 (String.length s - 2) in
	  unescape_string '"' s1 ] *)
    let parse_int = dcg "int"
	[ s = match "[+-]?[0-9]+" -> int_of_string s ]

    let parse_integer = Turtle.parse_integer (* dcg "integer"
	[ s = match "[+-]?[0-9]+" -> s ] *)
    let parse_decimal = Turtle.parse_decimal (* dcg "decimal" (* compulsory decimal part to avoid ambiguity with integer *)
	[ s = match "[+-]?[0-9]+\\.[0-9]+" -> s ] *)
    let parse_duration = dcg "duration"
	[ s = match "-?P\\([0-9]+Y\\)?\\([0-9]+M\\)?\\([0-9]+D\\)?\\(T\\([0-9]+H\\)?\\([0-9]+M\\)?\\([0-9]+\\(\\.[0-9]+\\)?\\)?\\)?" -> s ]
    let re_timezone = "\\(Z\\|[+-][0-1][0-9]:[0-5][0-9]\\)"
    let re_timezone_opt = re_timezone ^ "?"
    let re_time = "\\([0-1][0-9]\\|2[0-3]\\):[0-5][0-9]:[0-5][0-9]\\(\\.[0-9]+\\)?"
    let re_year = "-?[0-9][0-9][0-9][0-9][0-9]*"
    let re_month = "\\(0[1-9]\\|1[0-2]\\)" (* "[0-1][0-9]" *)
    let re_day = "\\(0[1-9]\\|[1-2][0-9]\\|3[0-1]\\)"
    let re_date = re_year ^ "-" ^ re_month ^ "-" ^ re_day
    let parse_dateTime = dcg "dateTime"
	[ s = match (re_date ^ "T" ^ re_time ^ re_timezone_opt) -> s ]
    let parse_time = dcg "time"
	[ s = match (re_time ^ re_timezone_opt) -> s ]
    let parse_date = dcg "date"
	[ s = match (re_date ^ re_timezone_opt) -> s ]
    let parse_gYearMonth = dcg "gYearMonth"
	[ s = match (re_year ^ "-" ^ re_month ^ re_timezone_opt) -> s ]
    let parse_gYear = dcg "gYear" (* compulsory timezone to avoid ambiguity with decimals *)
	[ s = match (re_year ^ re_timezone) -> s ]
    let parse_gMonthDay = dcg "gMonthDay"
	[ s = match ("--" ^ re_month ^ "-" ^ re_day ^ re_timezone_opt) -> s ]
    let parse_gDay = dcg "gDay"
	[ s = match ("---" ^ re_day ^ re_timezone_opt) -> s ]
    let parse_gMonth = dcg "gMonth"
	[ s = match ("--" ^ re_month ^ re_timezone_opt) -> s ]

    let parse_uri base xmlns = dcg
	[ uri = parse_uri_rel base -> uri
        | uri = parse_qname xmlns -> uri ]

    let parse_datatype base xmlns = dcg
	[ "@"; lang = parse_lang -> Rdf.Plain lang
        | "^^"; uri = parse_uri base xmlns -> Rdf.Typed uri
	|  -> Rdf.Typed Xsd.uri_string ]

    let parse_literal base xmlns = dcg
	[ s = parse_string; d = parse_datatype base xmlns -> Rdf.Literal (s,d)
        | s = parse_decimal -> Rdf.Literal (s, Rdf.Typed Xsd.uri_decimal)
        | s = parse_integer -> Rdf.Literal (s, Rdf.Typed Xsd.uri_integer)
        | s = parse_duration -> Rdf.Literal (s, Rdf.Typed Xsd.uri_duration)
        | s = parse_dateTime -> Rdf.Literal (s, Rdf.Typed Xsd.uri_dateTime)
        | s = parse_date -> Rdf.Literal (s, Rdf.Typed Xsd.uri_date)
        | s = parse_time -> Rdf.Literal (s, Rdf.Typed Xsd.uri_time)
        | s = parse_gYearMonth -> Rdf.Literal (s, Rdf.Typed Xsd.uri_gYearMonth)
        | s = parse_gYear -> Rdf.Literal (s, Rdf.Typed Xsd.uri_gYear)
        | s = parse_gMonthDay -> Rdf.Literal (s, Rdf.Typed Xsd.uri_gMonthDay)
        | s = parse_gDay -> Rdf.Literal (s, Rdf.Typed Xsd.uri_gDay)
        | s = parse_gMonth -> Rdf.Literal (s, Rdf.Typed Xsd.uri_gMonth) ]

    let parse_name base xmlns = dcg
	[ uri = parse_uri base xmlns -> Rdf.URI uri ]

    let parse base xmlns = dcg
	[ id = parse_blank -> Rdf.Blank id
	| l = parse_literal base xmlns -> l
        | n = parse_name base xmlns -> n ]

    let from_string base xmlns : string -> t =
      fun s ->
	let _ctx, n : unit * t = Dcg.once (parse base xmlns) () (Matcher.cursor_of_string s) in
	n

  end
