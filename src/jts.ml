(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Jni_common

module Geometry =
  struct
    let path = "com/vividsolutions/jts/geom/Geometry"
    let path_point = "com/vividsolutions/jts/geom/Point"
    let c = find_class path
    let defined = try ignore (Lazy.force c); true with _ -> false
    (* basic *)
    let m_getSRID = get_methodID c "getSRID" (signature [] I)
    let m_setSRID = get_methodID c "setSRID" (signature [I] V)
    let m_toText = get_methodID c "toText" (signature [] (L JString.path))
    (* Unary geomtric predicates *)
    let m_isEmpty = get_methodID c "isEmpty" (signature [] Z)
    (* Binary geometric predicates *)
    let m_contains = get_methodID c "contains" (signature [L path] Z)
    let m_covers = get_methodID c "covers" (signature [L path] Z)
    let m_coveredBy = get_methodID c "coveredBy" (signature [L path] Z)
    let m_crosses = get_methodID c "crosses" (signature [L path] Z)
    let m_disjoint = get_methodID c "disjoint" (signature [L path] Z)
    let m_intersects = get_methodID c "intersects" (signature [L path] Z)
    let m_overlaps = get_methodID c "overlaps" (signature [L path] Z)
    let m_touches = get_methodID c "touches" (signature [L path] Z)
    let m_equals = get_methodID c "equals" (signature [L path] Z)
    let m_relates = get_methodID c "relate" (signature [L path; L JString.path] Z)
    let m_isWithinDistance = get_methodID c "isWithinDistance" (signature [L path; D] Z)
    (* Metric functions *)
    let m_getDimension = get_methodID c "getDimension" (signature [] I)
    let m_getNumGeometries = get_methodID c "getNumGeometries" (signature [] I)
    let m_getNumPoints = get_methodID c "getNumPoints" (signature [] I)
    let m_getArea = get_methodID c "getArea" (signature [] D)
    let m_getLength = get_methodID c "getLength" (signature [] D)
    let m_distance = get_methodID c "distance" (signature [L path] D)
    (* Geometric functions *)
    let m_getCentroid = get_methodID c "getCentroid" (signature [] (L path_point))
    let m_intersection = get_methodID c "intersection" (signature [L path] (L path))
    let m_union = get_methodID c "union" (signature [L path] (L path))
    let m_difference = get_methodID c "difference" (signature [L path] (L path))
    let m_symDifference = get_methodID c "symDifference" (signature [L path] (L path))
    let m_getBoundary = get_methodID c "getBoundary" (signature [] (L path))
    let m_convexHull = get_methodID c "convexHull" (signature [] (L path))
    let m_getEnvelope = get_methodID c "getEnvelope" (signature [] (L path))
    let m_buffer = get_methodID c "buffer" (signature [D] (L path))

    class obj (this : Jni.obj) =
    object
      inherit JObject.obj this

      method getSRID : int =
	call_camlint_method this m_getSRID [||]
      method setSRID (srid : int) : unit =
	call_void_method this m_setSRID [|Jni.Camlint srid|]

      method toText : string = Common.prof "Jts.Geometry.obj#toText" (fun () ->
	Jni.string_from_java (call_object_method this m_toText [||]))

      method isEmpty : bool = Common.prof "Jts.Geometry.obj#isEmpty" (fun () ->
	call_boolean_method this m_isEmpty [||])

      method contains (g : obj) : bool = Common.prof "Jts.Geometry.obj#contains" (fun () ->
	call_boolean_method this m_contains [|Jni.Obj g#jni|])
      method covers (g : obj) : bool = Common.prof "Jts.Geometry.obj#covers" (fun () ->
	call_boolean_method this m_covers [|Jni.Obj g#jni|])
      method coveredBy (g : obj) : bool = Common.prof "Jts.Geometry.obj#coveredBy" (fun () ->
	call_boolean_method this m_coveredBy [|Jni.Obj g#jni|])
      method crosses (g : obj) : bool = Common.prof "Jts.Geometry.obj#crosses" (fun () ->
	call_boolean_method this m_crosses [|Jni.Obj g#jni|])
      method disjoint (g : obj) : bool = Common.prof "Jts.Geometry.obj#disjoint" (fun () ->
	call_boolean_method this m_disjoint [|Jni.Obj g#jni|])
      method intersects (g : obj) : bool = Common.prof "Jts.Geometry.obj#intersects" (fun () ->
	call_boolean_method this m_intersects [|Jni.Obj g#jni|])
      method overlaps (g : obj) : bool = Common.prof "Jts.Geometry.obj#overlaps" (fun () ->
	call_boolean_method this m_overlaps [|Jni.Obj g#jni|])
      method touches (g : obj) : bool = Common.prof "Jts.Geometry.obj#touches" (fun () ->
	call_boolean_method this m_touches [|Jni.Obj g#jni|])
      method equals (g : obj) : bool = Common.prof "Jts.Geometry.obj#equals" (fun () ->
	call_boolean_method this m_equals [|Jni.Obj g#jni|])
      method relates (g : obj) (pattern : string) : bool = Common.prof "Jts.Geometry.obj#relates" (fun () ->
	call_boolean_method this m_relates [|Jni.Obj g#jni; Jni.Obj (Jni.string_to_java pattern)|])
      method isWithinDistance (g : obj) (dist : float) : bool = Common.prof "Jts.Geometry.obj#isWithinDistance" (fun () ->
	call_boolean_method this m_isWithinDistance [|Jni.Obj g#jni; Jni.Double dist|])

      method getDimension : int =
	call_camlint_method this m_getDimension [||]
      method getNumGeometries : int =
	call_camlint_method this m_getNumGeometries [||]
      method getNumPoints : int =
	call_camlint_method this m_getNumPoints [||]
      method getArea : float =
	call_double_method this m_getArea [||]
      method getLength : float =
	call_double_method this m_getLength [||]

      method distance (g : obj) : float = Common.prof "Jts.Geometry.obj#distance" (fun () ->
	call_double_method this m_distance [|Jni.Obj g#jni|])

      method intersection (g : obj) : obj = Common.prof "Jts.Geometry.obj#intersection" (fun () ->
	new obj (call_object_method this m_intersection [|Jni.Obj g#jni|]))
      method union (g : obj) : obj = Common.prof "Jts.Geometry.obj#union" (fun () ->
	new obj (call_object_method this m_union [|Jni.Obj g#jni|]))
      method difference (g : obj) : obj = Common.prof "Jts.Geometry.obj#difference" (fun () ->
	new obj (call_object_method this m_difference [|Jni.Obj g#jni|]))
      method symDifference (g : obj) : obj = Common.prof "Jts.Geometry.obj#symDifference" (fun () ->
	new obj (call_object_method this m_symDifference [|Jni.Obj g#jni|]))
      method getBoundary : obj = Common.prof "Jts.Geometry.obj#getBoundary" (fun () ->
	new obj (call_object_method this m_getBoundary [||]))
      method convexHull : obj = Common.prof "Jts.Geometry.obj#convexHull" (fun () ->
	new obj (call_object_method this m_convexHull [||]))
      method getEnvelope : obj = Common.prof "Jts.Geometry.obj#getEnvelope" (fun () ->
	new obj (call_object_method this m_getEnvelope [||]))
      method buffer (dist : float) : obj = Common.prof "Jts.Geometry.obj#buffer" (fun () ->
	new obj (call_object_method this m_buffer [|Jni.Double dist|]))
    end
  end

module WKTReader =
  struct
    let path = "com/vividsolutions/jts/io/WKTReader"
    let c = find_class path
    let defined = try ignore (Lazy.force c); true with _ -> false
    let init = get_methodID c "<init>" (signature [] V)
    let m_read = get_methodID c "read" (signature [L JString.path] (L Geometry.path))

    class obj (this : Jni.obj) =
    object
      inherit JObject.obj this

      method read (wkt : string) : Geometry.obj = Common.prof "Jts.WKTReader.obj#read" (fun () ->
	new Geometry.obj (call_object_method this m_read [|Jni.Obj (Jni.string_to_java wkt)|]))
    end

    let create () : obj = Common.prof "Jts.WKTReader.create" (fun () ->
      let this = alloc_object c in
      call_nonvirtual_void_method this c init [||];
      new obj this)
  end

let _ = prerr_endline "End of jts.ml"; flush stderr
