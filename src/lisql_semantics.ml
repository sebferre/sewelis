(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Ext = Intset.Intmap
module Rel = Intreln.Intmap
module Extension = Extension.Make
module Code = Code.Make
module Store = Store.Make
module Namespace = Lisql_namespace
module Syntax = Lisql_syntax
module Display = Lisql_display
module Fol = Fol.Make

open Lisql_ast

(* translation from LISQL to FOL *)

let rec fol_c ~obs store gv : c -> Fol.fol = function
  | Nope -> Fol.FTrue
  | Assert s -> fol_s ~obs store gv s
  | Get np -> fol_s1 ~obs store gv ~as_command:true (fun x -> Fol.FTrue) np
  | Call (proc, args) ->
    Common.fold_for_down
      (fun i res ->
	(fun rev_xargs -> fol_s1 ~obs store gv ~as_command:true (fun xi -> res (xi::rev_xargs)) args.(i)))
      (Array.length args - 1)
      0
      (fun rev_xargs -> fol_proc ~obs store gv proc (List.rev rev_xargs))
      []
  | Seq lc -> Fol.and_list (List.map (fun c -> fol_c ~obs store gv c) lc)
  | Cond (s,c1,c2) -> Fol.FCond (fol_s ~obs store gv s, fol_c ~obs store gv c1, Some (fol_c ~obs store gv c2))
  | For (np,c) -> fol_s1 ~obs store gv ~as_command:true (fun _ -> fol_c ~obs store gv c) np
and fol_proc ~obs store gv proc xargs =
  match proc, xargs with
    | Proc.RemoveName, [x] -> Fol.FRemove x
    | Proc.CopyName, [x1;x2] -> Fol.FCopy (x1,x2)
    | Proc.MoveName, [x1;x2] -> Fol.FMove (x1,x2)
    | Proc.Prim op, xs -> Fol.FProc (op,xs)
    | _ -> assert false
and fol_s ~obs store gv : s -> Fol.fol = function
  | Is (np,c) ->
      let prefix = fol_p1_prefix ~obs store c in
      fol_s1 ~obs store gv ~prefix
	(fun x -> fol_p1 ~obs store gv x c)
	np
  | True -> Fol.FTrue
  | SAnd l ->
      Fol.and_list (List.map (fun s -> fol_s ~obs store gv s) l)
  | SOr l ->
      Fol.or_list (List.map (fun s -> fol_s ~obs store gv s) l)
  | SNot s ->
      Fol.FNot (fol_s ~obs store gv s)
  | SMaybe s ->
      Fol.FMaybe (fol_s ~obs store gv s)
and fol_s1 ~obs store gv ?(prefix = (0,"")) ?(as_command = false)
    (d : Extension.var -> Fol.fol) : s1 -> Fol.fol = function
  | Det (det, c) ->
      let prefix2 = fol_p1_prefix ~obs store c in
      fol_s2 ~obs store gv ~prefix:(max prefix prefix2) ~as_command (fun x -> fol_p1 ~obs store gv x c) d det
  | NAnd l ->
      Fol.and_list (List.map (fun np -> fol_s1 ~obs store gv ~prefix ~as_command d np) l)
  | NOr l ->
      Fol.or_list (List.map (fun np -> fol_s1 ~obs store gv ~prefix ~as_command d np) l)
  | NNot np1 ->
      Fol.FNot (fol_s1 ~obs store gv ~prefix ~as_command d np1)
  | NMaybe np1 ->
      Fol.FMaybe (fol_s1 ~obs store gv ~prefix ~as_command d np1)
and fol_s2 ~obs store gv ~prefix ~as_command
    (d1 : Extension.var -> Fol.fol) (d2 : Extension.var -> Fol.fol) : s2 -> Fol.fol = function
  | Name n ->
      let x = gv#get_prefix var_name in
      Fol.and_list [Fol.FBind (x,n); d1 x; d2 x]
  | Quote c ->
      let str = Syntax.string_of_assertion c in
      let x = gv#get_prefix var_quote in
      Fol.and_list [Fol.FBind (x, Rdf.Literal (str, Rdf.Typed Namespace.uri_Assertion)); d1 x; d2 x]
  | Ref v ->
      Fol.and_list [d1 v; d2 v]
  | Qu (qu, v_opt) ->
      let qu = match qu with Every | The when as_command -> Each | _ -> qu in
      let x = match v_opt with Some v -> v | None -> gv#get_prefix (snd prefix) in
      fol_qu ~obs store gv x d1 d2 qu
  | Which ->
      let x = gv#get_prefix var_focus in
      Fol.and_list [d1 x; d2 x]
and fol_qu ~obs store gv (x : Extension.var) (d1 : Extension.var -> Fol.fol) (d2 : Extension.var -> Fol.fol) : qu -> Fol.fol = function
  | An ->
      Fol.and_list [d1 x; d2 x]
  | The | Every ->
      Fol.FForEvery (d1 x, d2 x)
  | Each ->
      Fol.FForEach (d1 x, d2 x)
  | Only ->
      Fol.FForOnly (d1 x, d2 x)
  | No ->
      Fol.FForNo (d1 x, d2 x)
and fol_p1 ~obs store gv (x : Extension.var) : p1 -> Fol.fol = function
  | Arg (pred,i,args) ->
      let args' = array_set_nth args i (ref_s1 x) in
      fol_arg ~obs store gv pred args'
  | SuchThat (v,s) -> fol_s ~obs store gv (subst_s [(v, Ref x)] s)
  | Thing -> Fol.FTrue
  | And l ->
      Fol.and_list (List.map (fun f -> fol_p1 ~obs store gv x f) l)
  | Or l ->
      Fol.or_list (List.map (fun f -> fol_p1 ~obs store gv x f) l)
  | Not f1 ->
      Fol.FNot (fol_p1 ~obs store gv x f1)
  | Maybe f1 ->
      Fol.FMaybe (fol_p1 ~obs store gv x f1)
and fol_arg ~obs store gv pred args =
  let arity = Array.length args - 1 in
  Common.fold_for_down
    (fun i res ->
      let prefix = fol_p1_prefix ~obs store (Arg (pred,i,args)) in
      (fun rev_xargs -> fol_s1 ~obs store gv ~prefix (fun xi -> res (xi::rev_xargs)) args.(i)))
    arity
    (if Pred.reifiable pred then 0 else 1)
    (fun rev_xargs -> fol_pred ~obs store gv pred (List.rev rev_xargs))
    []
and fol_pred ~obs store gv pred xargs =
  match pred, xargs with
    | Pred.Equal, [x;y] ->
        Fol.FSame (x,y)
    | Pred.Type c, [x1] ->
      if c = Rdfs.uri_Resource then Fol.FTrue
      else Fol.FClass (c,x1)
    | Pred.Role (p,modifs), [x;y] ->
        let lm = [] in
	let lm = if modifs.Pred.direct then Fol.Direct::lm else lm in
	let lm = if modifs.Pred.symmetric then Fol.Symmetric::lm else lm in
	let lm = if modifs.Pred.reflexive then Fol.Reflexive::lm else lm in
	let lm = if modifs.Pred.transitive then Fol.Transitive::lm else lm in
	Fol.FProp (lm,p,x,y)
    | Pred.Funct funct, x0::xs ->
        Fol.FFunct (funct,x0,Array.of_list xs)
    | Pred.Prim op, xs ->
        Fol.FPrim (op,xs)
    | _ -> assert false
and fol_p1_prefix ~obs store = function
  | Arg (Pred.Type c,1,_) -> 2, Display.string_of_uri ~obs store c
  | Arg (Pred.Role (p,_),i,_) ->
      let s = Display.string_of_uri ~obs store p in
      (match i with 1 -> 1, "is " ^ s ^ " of" | _ -> 3, s)
  | Arg (Pred.Funct funct,0,_) -> 4, Display.string_of_uri ~obs store funct
  | Arg (Pred.Prim op,i,args) ->
    let prim = store#get_primitive op (Array.length args - 1) in
    ( match Store.PredSyntax.role_string prim#syntax i with Some s -> 1, s | None -> 0, "" )
  | And l -> List.fold_left (fun prefix f -> max prefix (fol_p1_prefix ~obs store f)) (0,"") l
  | _ -> 0, ""

let rec fol_of_assertion ~obs store a =
  let gv = new gen_var in
  fol_s ~obs store gv a

let fol_of_class ~obs store x f =
  let gv = new gen_var in
  fol_p1 ~obs store gv x f


           (* assertions *)

let rec check_update_c = function
  | Nope -> ()
  | Assert s -> check_update_s s
  | Get np -> check_update_s1 np
  | Call (proc,args) -> ()
  | Seq lc -> List.iter check_update_c lc
  | Cond (s,c1,c2) -> check_update_c c1; check_update_c c2
  | For (np,c) -> check_update_s1 np; check_update_c c
and check_update_s = function
  | Is (np,c) -> check_update_s1 np; check_update_p1 c
  | True -> ()
  | SAnd ls -> List.iter check_update_s ls
  | SOr ls -> failwith "Invalid update: some disjunction between Ss"
  | SNot s -> check_update_s s
  | SMaybe s -> failwith "Invalid update: optional S (maybe)"
and check_update_s1 = function
  | Det (Qu (An,_), f1) -> check_update_p1 f1
  | Det (Qu ((The | Every | Each | Only | No),_), f1) -> ()
  | Det (Which, _) -> failwith "Invalid update: which"
  | Det ((Name _ | Quote _ | Ref _), f1) -> check_update_p1 f1
  | NAnd lnp -> List.iter check_update_s1 lnp
  | NOr lnp -> failwith "Invalid update: some disjunction between NPs"
  | NNot np -> check_update_s1 np
  | NMaybe np -> failwith "Invalid update: optional NP (maybe)"
and check_update_p1 = function
  | Arg (pred,i,args) -> check_update_p1_args i args
  | SuchThat (x,s) -> check_update_s s
  | Thing -> () (*failwith "Invalid description: some element is undefined (?)"*)
  | And lf -> List.iter check_update_p1 lf
  | Or _ -> failwith "Invalid description: disjunctions are not allowed (or)"
  | Not f1 -> check_update_p1 f1 (* failwith "Invalid description: negations are not allowed (not)" *)
  | Maybe _ -> failwith "Invalid description: optionals are not allowed (maybe)"
and check_update_p1_args i args =
  Array.iteri (fun j a -> if j <> i then check_update_s1 a) args
    (* the i-th argument is unsignificant, and therefore allowed to be Thing *)
	   
let rec retract_s = function s -> SNot s

type results = { nb : int;
		 vars : Extension.var list;
		 relation : Rel.t; }

let results ~obs store (c : c) : results =
  let fol = fol_c ~obs store (new gen_var) c in
  let eval, ext = Fol.extension_of_fol ~obs store fol in
  let vars = eval.Fol.optvars in
  let relation = ext#relation (store :> Extension.store) vars in
  let nb = Rel.cardinal relation in
  {nb; vars; relation}

let run store (c : c) : unit =
  Tarpit.effect (fun obs ->
    let fol = fol_c ~obs store (new gen_var) c in
    let lc = Fol.code_of_fol ~obs store fol in
    print_string "run: ";
    List.iter (fun c -> print_string c#string; print_string "; ") lc;
    print_newline ();
    ignore (Code.list_run (store :> Code.store) [] lc))

let tell store s = run store (Assert s)
let retract store s = run store (Assert (SNot s))

let ask ~obs store (a : s) : bool =
  let fol = fol_of_assertion ~obs store a in
  let _, ext = Fol.extension_of_fol ~obs store fol in
  ext#succeeds (store :> Extension.store) []

           (* extension *)

let extension_s ~obs store (s : s) : Extension.t =
  Common.prof "Lisql_semantics.extension_s" (fun () ->
    let fol = fol_of_assertion ~obs store s in
    let _, ext = Fol.extension_of_fol ~obs store fol in
    ext)

let rec ext_of_class ~obs store bounded_vars x f =
  let f1 = fol_of_class ~obs store x f in
  let eval, ext = Fol.extension_of_fol ~obs store ~bounded_vars f1 in
  if List.mem x eval.Fol.vars
  then ext
  else raise (Fol.Unbound x)

let extension_p1 ~obs store ?(bounded_vars = LSet.empty ()) (f : p1) : Extension.t =
  Common.prof "Root.extension" (fun () ->
    let ext = ext_of_class ~obs store bounded_vars var_root f in
(*print_endline ("Lisql.extension of: " ^ ext#string);*)
    ext)

let extent_fold ~obs store (ff : 'a -> Extension.map -> 'a) (init : 'a) (m : Extension.map) (f : p1) : 'a =
  Common.prof "Root.extent_fold" (fun () ->
    let ext = extension_p1 ~obs store f in
    ext#fold (store :> Extension.store) ff init m)

              (* get the extent of a concept *)
let extent_p1 ~obs store (f : p1) : Ext.t =
  Common.prof "Root.extent_p1" (fun () ->
    match f with
    | Thing -> print_endline "extent Thing"; store#extent_all ~obs
    | _ ->
        let ext = extension_p1 ~obs store f in
	ext#fold (store :> Extension.store)
	  (fun res m ->
	    try Ext.add (store#get_entity (List.assoc var_root m)) res
	    with _ ->
	      prerr_endline ext#string;
	      prerr_endline "Lisql.extent: root variable undefined";
	      res)
	  Ext.empty [])


let extent_inter ~obs store (e : Ext.t) (f : p1) : Ext.t =
  Common.prof "Root.extent_inter" (fun () ->
    let ext = extension_p1 ~obs store f in
    Ext.filter
      (fun oid ->
	ext#succeeds (store :> Extension.store)
	  [(var_root, store#get_name oid)])
      e)

let apply_ext ~obs store (ext : Extension.t) (e : Ext.t) : Ext.t =
  Ext.fold
    (fun res oid ->
      ext#fold (store :> Extension.store)
	(fun res1 m ->
	  try Ext.add (store#get_entity (List.assoc var_root m)) res1
	  with _ -> res1)
	res [(var_this, store#get_name oid)])
    Ext.empty e


let extent_restriction ~obs store (r : Pred.role) (i : int) (e : Ext.t) : Ext.t =
  Common.prof "Root.extent_cross" (fun () ->
    let ext = extension_p1 ~obs store ~bounded_vars:(LSet.singleton var_this)
	(has_role r i (ref_s1 var_this)) in
    apply_ext ~obs store ext e)

let extent_is_arg_of_arg ~obs store funct arity (i : int) (j : int) (e : Ext.t) : Ext.t =
  Common.prof "Lisql.extent_is_arg_of_arg" (fun () ->
    let q =
      let args = Array.make (1+arity) top_s1 in
      args.(j) <- ref_s1 var_this;
      Arg (Pred.Funct funct, i, args) in
    let ext = extension_p1 ~obs store ~bounded_vars:(LSet.singleton var_this) q in
    apply_ext ~obs store ext e)

let extent_has_arg ~obs store funct arity (i : int) (e : Ext.t) : Ext.t =
  extent_is_arg_of_arg ~obs store funct arity 0 i e

let extent_arg_of ~obs store funct arity (i : int) (e : Ext.t) : Ext.t =
  extent_is_arg_of_arg ~obs store funct arity i 0 e

	      (* get the extent of a concept where the variable ?This is bound to [o]. *)
let apply ~obs store f o =
  extent_fold ~obs store
    (fun res vars ->
      try Ext.add (store#get_entity (List.assoc var_root vars)) res
      with _ -> assert false)
    Ext.empty
    [(var_this, store#get_name o)]
    f


type 'a safe = SE_Some of 'a | SE_All | SE_Undef
  
module Relax =
  struct
    
    let safe_union se1 se2 =
      match se1, se2 with
      | SE_Undef, _ -> se2
      | _, SE_Undef -> se1
      | SE_All, _ -> SE_All
      | _, SE_All -> SE_All
      | SE_Some e1, SE_Some e2 -> SE_Some (Ext.union e1 e2)
	    
    let safe_super_classes ~obs (store : #Store.store) (d : int) (c : Uri.t) : Uri.t list safe =
      Common.prof "Lisql.Relax.safe_super_classes" (fun () ->
	if d = 0
	then SE_Some [c]
	else
	  match store#tab_safe_super_classes ~obs (d-1) c with
	  | SE_Undef -> SE_Undef
	  | SE_All -> SE_Undef
	  | SE_Some lc ->
	      List.fold_left
		(fun res c ->
		  store#rdfs_subClassOf#direct_relation#fold_successors ~obs
		    (fun res1 -> function
		      | Rdf.URI c1 ->
			  ( match res1 with
			  | SE_Undef -> assert false
			  | SE_All ->
			      if c1 = Rdfs.uri_Resource then SE_All else SE_Some [c1]
			  | SE_Some lc1 ->
			      if c1 = Rdfs.uri_Resource then SE_All else SE_Some (c1::lc1))
		      | _ -> assert false)
		    res (Rdf.URI c))
		SE_All lc)
	
    let union_classes ~obs store lc =
      List.fold_left
	(fun e c ->
	  Ext.union e (store#tab_extent ~obs (has_type c) : Ext.t))
	Ext.empty lc
	
    let safe_super_props ~obs (store : #Store.store) (d : int) (p : Uri.t) : Uri.t list safe =
      Common.prof "Lisql.Relax.safe_super_props" (fun () ->
	if d = 0
	then SE_Some [p]
	else
	  match store#tab_safe_super_props ~obs (d-1) p with
	  | SE_Undef -> SE_Undef
	  | SE_All -> SE_Undef
	  | SE_Some lp ->
	      List.fold_left
		(fun res p ->
		  store#rdfs_subPropertyOf#direct_relation#fold_successors ~obs
		    (fun res1 -> function
		      | Rdf.URI p1 ->
			  ( match res1 with
			  | SE_Undef -> assert false
			  | SE_All -> SE_Some [p1]
			  | SE_Some lp1 -> SE_Some (p1::lp1))
		      | _ -> assert false)
		    res (Rdf.URI p))
		SE_All lp)
	
    let domain_union_props ~obs store lp modifs i =
      List.fold_left
	(fun e p ->
	  Ext.union e (store#tab_extent ~obs (has_role (p,modifs) i top_s1) : Ext.t))
	Ext.empty lp
	
    let antecedents_union_props ~obs store lp modifs i e1 =
      List.fold_left
	(fun e p ->
	  Ext.union e (extent_restriction ~obs store (p,modifs) i e1))
	Ext.empty lp

		      
    let rec safe_extent ~obs store d q =
      match q with
      | Arg (Pred.Type c, _, _) ->
	  ( match store#tab_safe_super_classes ~obs d c with
	  | SE_Undef -> SE_Undef
	  | SE_All -> SE_All
	  | SE_Some lc ->
	      assert (lc <> []);
	      SE_Some (union_classes ~obs store lc))
      | Arg (Pred.Role r, 1, [|_; _; NAnd lnp|]) ->
	  safe_extent ~obs store d (And (List.map (fun np -> has_role r 1 np) lnp))
      | Arg (Pred.Role r, 2, [|_; NAnd lnp; _|]) ->
	  safe_extent ~obs store d (And (List.map (fun np -> has_role r 2 np) lnp))
      | Arg (Pred.Role (p,modifs),i,args) ->
	  let np = args.(Argindex.opposite i) in
	  Common.fold_for
	    (fun n se ->
	      let se1 = safe_extent_s1 ~obs store (d-n) np in
	      let slp = store#tab_safe_super_props ~obs n p in
	      let sep1 =
		match slp, se1 with
		| SE_Undef, _ -> SE_Undef
		| _, SE_Undef -> SE_Undef
		| SE_All, SE_All -> SE_All
		| SE_All, _ -> SE_Undef
		| SE_Some lp, SE_All ->
		    assert (lp <> []);
			      SE_Some (domain_union_props ~obs store lp modifs i)
		| SE_Some lp, SE_Some e1 ->
		    assert (lp <> []);
		    SE_Some (antecedents_union_props ~obs store lp modifs i e1) in
	      safe_union se sep1)
	    0 d SE_Undef
      | Arg (Pred.Funct funct, i, args) ->
	  let arity = Array.length args - 1 in
	  let se0 = (* not relaxing the functor *)
	    match store#tab_safe_extent_struct ~obs d funct arity 0 (Array.to_list args) with
	    | SE_Undef -> SE_Undef
	    | SE_All -> SE_Some (store#tab_extent ~obs (Arg (Pred.Funct funct, i, Array.make (1+arity) top_s1)) : Ext.t)
	    | SE_Some e -> if i = 0 then SE_Some e else SE_Some (extent_arg_of ~obs store funct arity i e) in
	  let se1 = (* relaxing the functor to SE_All *)
	    if d > 0
	    then
	      match store#tab_safe_extent_struct ~obs (d-1) funct arity 0 (Array.to_list args) with
	      | SE_All -> SE_All
	      | _ -> SE_Undef (* the functor can only be relaxed if all arguments are exactly maximally relaxed *)
	    else SE_Undef in
	  safe_union se0 se1
      | Thing ->
	  if d = 0 then SE_All
	  else SE_Undef
      | And [] -> safe_extent ~obs store d Thing
      | And [q1] ->
	  store#tab_safe_extent ~obs d q1
      | And (q1::lq) ->
	  Common.fold_for
	    (fun n se ->
	      let se1 = store#tab_safe_extent ~obs (d-n) q1 in
	      let se2 = store#tab_safe_extent ~obs n (And lq) in
	      let se12 =
		match se1, se2 with
		| SE_Undef, _ -> SE_Undef
		| _, SE_Undef -> SE_Undef
		| SE_All, SE_All -> SE_All
		| SE_All, _ -> se2
		| _, SE_All -> se1
		| SE_Some e1, SE_Some e2 -> SE_Some (Ext.inter e1 e2) in
	      safe_union se se12)
	    0 d SE_Undef
      | _ ->
	  if d = 0 (*then SE_Some (store#tab_extent ~obs q) (* this entails unbound variables problems with variables *)
		      else if d = 1*)
	  then SE_All
	  else SE_Undef
    and safe_extent_struct store ~obs d funct arity i largs =
      match largs with
      | [] -> SE_All
      | arg1::largs1 ->
	  Common.fold_for
	    (fun n se ->
	      let se1 = safe_extent_s1 ~obs store n arg1 in
	      let se2 =
		match largs1 with
		| [] -> SE_All
		| _ -> store#tab_safe_extent_struct ~obs (d-n) funct arity (i+1) largs1 in
	      let se12 =
		match se1, se2 with
		| SE_Undef, _
		| _, SE_Undef -> SE_Undef
		| SE_All, SE_All -> SE_All
		| SE_All, _ -> se2
		| SE_Some e1, SE_All ->
		    SE_Some (if i = 0 then e1 else extent_has_arg ~obs store funct arity i e1)
		| SE_Some e1, SE_Some e2 ->
		    SE_Some (Ext.inter e2 (if i = 0 then e1 else extent_has_arg ~obs store funct arity i e1)) in
	      safe_union se se12)
	    0 d SE_Undef
    and safe_extent_s1 ~obs store d np =
      match np with
      | Det (Name n, c) ->
	  if d < 3 then SE_Some (Ext.singleton (store#get_entity n))
	  else store#tab_safe_extent ~obs (d-3) c (* higher weight for relaxing names/values *)
      | Det (Qu (An, _), c) ->
	  store#tab_safe_extent ~obs d c
      | _ ->
	  if d = 0
	  then SE_All
	  else SE_Undef

  end
