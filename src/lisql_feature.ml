(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Lisql_ast

module Ext = Intset.Intmap
module Syntax = Lisql_syntax
module Display = Lisql_display
module Transf = Lisql_transf

(* features *)

type kind =
  | Kind_Something
  | Kind_Variable
  | Kind_Entity
  | Kind_Literal
  | Kind_Thing
  | Kind_Class
  | Kind_Operator
  | Kind_Property
  | Kind_InverseProperty
  | Kind_Structure
  | Kind_Argument

type spec =
  | Spec_Something
  | Spec_Some of var
  | Spec_Ref of var
  | Spec_Name of Name.t
  | Spec_Thing
  | Spec_Argument of Pred.t * int * Argindex.t
  | Spec_Call of Proc.t * int * int


class virtual feature =
  object (self)
    method virtual kind : kind
    method virtual spec : spec
    method virtual string : string
    method virtual display : obs:Tarpit.observer -> Display.t
    method display_string : obs:Tarpit.observer -> string =
      fun ~obs -> Common.prof "Lisql_feature.feature#display_string" (fun () ->
	Display.to_string (self#display ~obs))
    method uri_opt : Uri.t option = None
    method prop_uri_opt : Uri.t option = None
    method name_opt : Name.t option = match self#uri_opt with Some uri -> Some (Rdf.URI uri) | None -> None
    method p1_opt : p1 option = None
    method lisql_opt : (Name.t * c * focus) option = None
    method oids : obs:Tarpit.observer -> Ext.t = fun ~obs -> Ext.empty
    method virtual pagerank : float
  end

let compare f1 f2 =
  match f1#name_opt, f2#name_opt with
    | Some n1, Some n2 ->
      ( match n1, n2 with
	| Rdf.URI _, Rdf.URI _ -> Pervasives.compare (f1#kind,f2#pagerank) (f2#kind,f1#pagerank)
	| Rdf.Literal l1, Rdf.Literal l2 -> Name.compare_literal l1 l2
	| _ -> Pervasives.compare f1#spec f2#spec )
    | _ -> Pervasives.compare f1#spec f2#spec

class virtual feature_s1 store (np : s1) =
  object
    inherit feature
    method string = Syntax.print_to_string Syntax.print_s1 np
    method display ~obs = Display.of_s1 ~obs store np
  end

and feature_something store =
  object
    inherit feature_s1 store top_s1
    method kind = Kind_Something
    method spec = Spec_Something
    method pagerank = 1.
  end

and feature_name store (n : Name.t) =
  object
    inherit feature
    method kind =
      match n with
      | Rdf.Literal _ -> Kind_Literal
      | _ -> Kind_Entity
    method spec = Spec_Name n
    method string = Syntax.print_to_string Syntax.print_s1 (name_s1 n)
    method display ~obs =
      let np =
	match n with
	| Rdf.URI uri when uri = Rdf.uri_nil -> an nil
	| Rdf.Blank _ -> store#description ~obs n
	| _ -> name_s1 n in
      Display.of_s1 ~obs store np
    method uri_opt =
      match n with
      | Rdf.URI uri -> Some uri
      | _ -> None
    method name_opt = Some n
    method lisql_opt =
      match n with
      | Rdf.Literal (str, Rdf.Typed uri) when uri = Namespace.uri_Assertion ->
	(try
	  let c = store#unquote_assertion str in
	  let foc = focus_of_command c in
	  let foc =
	    match focus_next_postfix ~filter:Transf.focus_default_filter foc with
	    | Some foc' -> foc'
	    | None -> foc in
	  Some (n,c,foc)
	with _ -> None) (* in case of syntax error when unquoting *)
      | _ -> None
    method oids ~obs = Ext.singleton (store#get_entity n)
    method pagerank = store#get_pagerank n
  end
	
and feature_ref store (v : var) =
  let np = ref_s1 v in
  object
    inherit feature_s1 store np
    method kind = Kind_Variable
    method spec = Spec_Ref v
    method pagerank = 0.
  end

and feature_some store (v : var) =
  let np = var_s1 v in
  object
    inherit feature_s1 store np
    method kind = Kind_Variable
    method spec = Spec_Some v
    method pagerank = 0.
  end

and virtual feature_p1 store (f : p1) =
  object
    inherit feature
    method string = Syntax.print_to_string Syntax.print_p1 f
    method display ~obs = Display.of_p1 ~obs store f
    method p1_opt = Some f
    method oids ~obs : Ext.t = store#tab_extent ~obs f
  end

and feature_thing store =
  let f = Thing in
  object
    inherit feature_p1 store f
    method kind = Kind_Thing
    method spec = Spec_Thing
    method pagerank = 1.
  end

and feature_type store (c : Uri.t) =
  let f = has_type c in
  object
    inherit feature_p1 store f
    method kind = Kind_Class
    method spec = Spec_Argument (Pred.Type c,1,Argindex.subj)
    method uri_opt = Some c
    method pagerank = store#get_pagerank (Rdf.URI c)
  end

and feature_role store (ikind : Argindex.kind) (p : Uri.t) =
  let i, f = Argindex.of_kind ikind, make_role p ikind in
  object
    inherit feature_p1 store f
    method kind = match ikind with `Subject -> Kind_Property | `Object -> Kind_InverseProperty | _ -> assert false
    method spec = Spec_Argument (Pred.Role (Pred.make_role p), 2, i)
    method uri_opt = Some p
    method prop_uri_opt = Some p
    method pagerank = store#get_pagerank (Rdf.URI p)
  end

and feature_prim store (op : string) (arity : int) (i : int) =
  let f = make_prim op arity i in
  object
    inherit feature_p1 store f
    method kind = Kind_Operator
    method spec = Spec_Argument (Pred.Prim op,arity,i)
    method pagerank = 0.
  end

and feature_proc store (proc : Proc.t) (arity : int) (i : int) =
  let c = make_call_of_np proc arity i (ref_s1 var_this) in
  object
    inherit feature
    method kind = Kind_Operator
    method spec = Spec_Call (proc, arity, i)
    method string = Syntax.print_to_string Syntax.print_c c
    method display ~obs = Display.of_c ~obs store c
    method pagerank = 0.
  end

and feature_funct store (funct : Uri.t) (arity : int) (i : int) =
  let f = make_funct funct arity i in
  object
    inherit feature_p1 store f
    method kind = if i = 0 then Kind_Structure else Kind_Argument
    method spec = Spec_Argument (Pred.Funct funct,arity,i)
    method uri_opt = Some funct
    method pagerank = store#get_pagerank (Rdf.URI funct)
  end

(* insertion of features *)

let ast_of_spec store (spec : spec) : [`S1 of s2 * p1 | `P1 of p1 | `Call of Proc.t * int * int] =
  match spec with
  | Spec_Something -> `S1 (top_s2, top_p1)
  | Spec_Name n ->
      ( match n with
      | Rdf.Blank _ ->
	  ( match store#description ~obs:Tarpit.blind_observer n with 
	  | Det (det,f) -> `S1 (det,f)
	  | _ -> assert false )
      | _ -> `S1 (name_s2 n, top_p1) )
  | Spec_Ref v -> `S1 (ref_s2 v, top_p1)
  | Spec_Some v -> `S1 (var_s2 v, top_p1)
  | Spec_Thing -> `P1 top_p1
  | Spec_Argument (pred,arity,i) -> `P1 (make_pred pred arity i)
  | Spec_Call (proc,arity,i) -> `Call (proc,arity,i)

let focus_insert_aux store (x : feature) (foc : focus) : focus option =
  match ast_of_spec store x#spec with
  | `S1 (d,f) ->
      ( match foc with
	| AtC _ -> Transf.focus_and_c (Get (Det (d,f))) foc
	| AtS _ -> Transf.focus_and_s (Is (Det (d,f), top_p1)) foc
	| AtS1 (Det (det,c),k) when (entails_s2 det d || entails_s2 d det) ->
	  Some (AtS1 (Det (Transf.merge_s2 det d, simpl_and c f), k))
	| AtS1 _ -> Transf.focus_and_s1 (Det (d,f)) foc
	| AtP1 (c, Det1 (det,_,k)) -> Some (AtS1 (Det (Transf.merge_s2 det d, simpl_and c f), k))
	| AtP1 _ -> Transf.focus_and_p1 (has_equal (Det (d,f))) foc
	| _ -> None )
  | `P1 f ->
      ( match foc with
	| AtC _ -> Transf.focus_and_c (Get (an f)) foc
	| AtS _ ->
	  ( match f with
	    | Arg (Pred.Type _, 1, _) | Arg (Pred.Funct _, 0, _) -> Transf.focus_and_s (Is (an f, top_p1)) foc
	    | _ -> Transf.focus_and_s (Is (top_s1,f)) foc )
	| AtS1 (Det (det,c),k) -> Transf.focus_and_p1 f (AtP1 (c, Det1 (det,c,k)))
	| AtS1 _ -> Transf.focus_and_s1 (an f) foc
	(*      | AtS2 (det, Det0 (_,c,k)) -> Transf.focus_and_p1 f (AtP1 (c, Det1 (det,c,k))) *)
	| AtP1 (Thing,k) -> Some (AtP1 (f,k))
	| AtP1 _ -> Transf.focus_and_p1 f foc
	| _ -> None )
  | `Call (proc,arity,i) ->
    ( match foc with
      | AtC _ -> Transf.focus_and_c (Call (proc, Array.make arity top_s1)) foc
      | AtS1 (np, k) ->
	( match k with
	  | CallN (i',proc',args',k') when i' = i && proc' = proc && Array.length args' = arity -> Some (AtS1 (np, Get0 k'))
	  | Get0 k' | CallN (_,_,_,k') -> Some (AtC (make_call_of_np proc arity i (Transf.s1_each np), k'))
	  | _ -> None )
      | _ -> None )


let focus_insert store x foc =
  Option.bind
    (focus_insert_aux store x foc)
    Transf.focus_tab_after_insert

let focus_insert_list store xs foc =
  Option.bind
    (List.fold_left
       (fun res x ->
	 Option.bind res
	   (fun foc -> focus_insert_aux store x foc))
       (Some foc) xs)
    Transf.focus_tab_after_insert

let focus_insert_list_or store xs foc =
  Option.bind
    (match xs with
    | [] -> None
    | x::xs1 ->
	List.fold_left
	  (fun res x ->
	    Option.bind res
	      (fun foc ->
		Option.bind (Transf.focus_or foc)
		  (fun foc -> focus_insert_aux store x foc)))
	  (focus_insert_aux store x foc) xs1)
    Transf.focus_tab_after_insert

let focus_insert_list_not store xs foc =
  Option.bind
    (List.fold_left
       (fun res x ->
	 Option.bind res
	   (fun foc ->
	     Option.bind (Transf.focus_and_not foc)
	       (fun foc ->
		 Option.bind (focus_insert_aux store x foc)
		   (fun foc -> focus_up foc))))
       (Some foc) xs)
    Transf.focus_tab_after_insert
