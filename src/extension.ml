(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Intset = Intset.Intmap
module Intmap = Intmap.M
module Rel = Intreln.Intmap
module Intrel2 = Intrel2.Intmap
module Name = Name.Make

module Make =
  struct
    type oid = int
    type var = string
    type map = (var * Name.t) list

    class type store =
      object
	method get_name : oid -> Name.t
	method get_entity : Name.t -> oid
	method new_resource : Name.t
      end

    type 'a fold = store -> ('a -> map -> 'a) -> 'a -> map -> 'a
    type 'a expr_fold = store -> ('a -> map -> Name.t -> 'a) -> 'a -> map -> 'a

    let string_of_lv lv = String.concat "," lv

    let union_vars lv1 lv2 =
      List.fold_left (fun lv v2 -> if List.mem v2 lv1 then lv else v2::lv) lv1 lv2

    let inter_vars lv1 lv2 =
      List.filter (fun v1 -> List.mem v1 lv2) lv1

    let diff_vars lv1 lv2 =
      List.filter (fun v1 -> not (List.mem v1 lv2)) lv1

    let oid_null = 0

    exception Found of map
    exception Not_unique

    (* class types for extensions, constraints, and expressions *)

    class virtual t =
      object (self)
	method virtual vars : var list
	method virtual fold : 'a. 'a fold

	method virtual string : string

	method fails (store : store) (m : map) : bool = Common.prof "Extension.t#fails" (fun () ->
	  try
	    self#fold store (fun _ m' -> raise (Found m')) () m;
	    true
	  with Found _ -> false)

	method succeeds (store : store) (m : map) : bool = Common.prof "Extension.t#succeeds" (fun () ->
	  try
	    self#fold store (fun _ m' -> raise (Found m')) () m;
	    false
	  with Found _ -> true)

	method choose (store : store) (m : map) : map = Common.prof "Extension.t#choose" (fun () ->
	  try
	    self#fold store (fun _ m' -> raise (Found m')) () m;
	    raise Not_found
	  with Found m' -> m')

	method unique (store : store) (m : map) : map = Common.prof "Extension.t#unique" (fun () ->
	  let n, m' =
	    self#fold store (fun (n,m') m'' -> if n = 0 then (1,m'') else raise Not_unique) (0,[]) m in
	  m')

	method count (store : store) (m : map) : int = Common.prof "Extension.t#count" (fun () ->
	  self#fold store (fun c _ -> c+1) 0 m)

	method fold_limit : 'a. limit:int -> store -> ('a -> map -> 'a) -> 'a -> map -> int * 'a =
	  fun ~limit store ff acc m ->
	    let ref_acc = ref acc in
	    let ref_n = ref 0 in
	    begin try
	      self#fold store
		(fun () m ->
		  if !ref_n >= limit
		  then raise Not_found
		  else begin
		    incr ref_n;
		    ref_acc := ff !ref_acc m
		  end)
		() m
	    with Not_found -> () end;
	    !ref_n, !ref_acc

	method intset (store : store) (v : var) : Intset.t = Common.prof "Extension.t#intset" (fun () ->
	  assert (v <> "");
	  self#fold store
	    (fun oids m ->
	      try Intset.add (store#get_entity (List.assoc v m)) oids
	      with _ -> oids)
	    Intset.empty
	    [])

	method intrel2_duo (store : store) (v1 : var) (v2 : var) : Intrel2.t * Intrel2.t =
	  Common.prof "Extension.t#intrel2" (fun () ->
	    assert (v1 <> "" && v2 <> "");
	    self#fold store
	      (fun (r1,r2) m ->
		let o1 = try store#get_entity (List.assoc v1 m) with Not_found -> oid_null in
		let o2 = try store#get_entity (List.assoc v2 m) with Not_found -> oid_null in
		(Intrel2.add o1 o2 r1, Intrel2.add o2 o1 r2))
	      (Intrel2.empty, Intrel2.empty)
	      [])

	method relation (store : store) (lv : var list) : Rel.t = Common.prof "Extension.t#relation" (fun () ->
	  assert (not (List.mem "" lv));
	  self#fold store
	    (fun r m -> Common.prof "Extension.t#relation_add" (fun () ->
	      let t = List.map (fun v -> try store#get_entity (List.assoc v m) with Not_found -> oid_null) lv in
	      Rel.add t r))
	    (Rel.empty (List.length lv))
	    [])

	method relation_multi (store : store) (llv : var list list) : Rel.t list = Common.prof "Extension.t#relation_multi" (fun () ->
	  assert (List.for_all (fun lv -> not (List.mem "" lv)) llv);
	  self#fold store
	    (fun lr m ->
	      List.map2
		(fun lv r ->
		  let t = List.map (fun v -> try store#get_entity (List.assoc v m) with Not_found -> oid_null) lv in
		  Rel.add t r)
		llv lr)
	    (List.map (fun lv -> Rel.empty (List.length lv)) llv)
	    [])
	    

	method relation1 store x ys =
	  self#relation store (x::ys)

	method relation2 (store : store) (x : var) (y : var) (zs : var list) : Rel.t * Rel.t = Common.prof "Extension.t#relation2" (fun () ->
	  let lv = x::y::zs in
	  let n = List.length lv in
	  assert (not (List.mem "" lv));
	  self#fold store
	    (fun (r1,r2) m -> Common.prof "Extension.t#relation_bin_add" (fun () ->
	      let t1 = List.map (fun v -> try store#get_entity (List.assoc v m) with Not_found -> oid_null) lv in
	      let t2 =
		match t1 with
		| a::b::l -> b::a::l
		| _ -> assert false in
	      Rel.add t1 r1,
	      Rel.add t2 r2))
	    (Rel.empty n, Rel.empty n)
	    [])

(* version that retains only maximal tuples *)
(*
	method relation (store : store) (lv : var list) : Rel.t = Common.prof "Extension.t#relation" (fun () ->
	  assert (not (List.mem "" lv));
	  self#fold store
	    (fun r m ->
	      let mu = List.fold_left (fun res v -> try (v, store#get_entity (List.assoc v m))::res with Not_found -> res) [] lv in
              (* filling tuples with 0s for unbound variables *)
	      if Rel.fold_restr mu lv (fun res t -> true) false r
	      then r (* a tuple containing t already exists in r *)
	      else
 		let t = List.map (fun v -> try store#get_entity (List.assoc v m) with Not_found -> oid_null) lv in
		let mu = List.fold_left (fun res v -> if List.mem_assoc v m then res else (v,0)::res) [] lv in
		let r' = Rel.filter_restr mu lv (fun _ -> true) r in
		Rel.add t r')
	    (Rel.empty (List.length lv))
	    [])
*)

      end

    class virtual constr =
      object
	method virtual string : string
	method virtual succeeds : store -> map -> bool
      end

    class virtual expr =
      object
	method virtual string : string
	method virtual vars : var list
	method virtual fold : 'a. 'a expr_fold
      end

    (* extensions *)

    let fold_intset v oids =
      fun store ff acc m -> Common.prof "Extension.fold_intset" (fun () ->
	try
	  let o = store#get_entity (List.assoc v m) in
	  if Intset.mem o oids
	  then ff acc m
	  else acc
	with Not_found ->
	  Intset.fold
	    (fun acc1 o ->
	      if o <> oid_null
	      then ff acc1 ((v,store#get_name o)::m)
	      else ff acc1 m)
	    acc oids)

    class intset name (v : var) (f_oids : unit -> Intset.t) =
      object
	inherit t
	method string = name ^ "[" ^ v ^ "]"
	method vars = [v]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    let oids = f_oids () in
	    fold_intset v oids store ff acc m
      end
    let intset name v f_oids = new intset name v f_oids

    let fold_intrel2 v1 v2 r =
      fun store ff acc m -> Common.prof "Extension.fold_intrel2" (fun () ->
	try
	  let o1 = store#get_entity (List.assoc v1 m) in
	  try
	    let oids = Intrel2.assoc o1 r in
	    fold_intset v2 oids store ff acc m
	  with Not_found ->
	    acc
	with Not_found ->
	  Intrel2.fold_assoc
	    (fun acc1 o1 oids ->
	      let m1 = if o1 <> oid_null then (v1,store#get_name o1)::m else m in
	      fold_intset v2 oids store ff acc1 m1)
	    acc r)

    class intrel2_duo name (v1 : var) (v2 : var) (f_rs : unit -> Intrel2.t * Intrel2.t) =
      object
	inherit t
	method string = name ^ "[" ^ string_of_lv [v1;v2] ^ "]"
	method vars = [v1;v2]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    let r1, r2 = f_rs () in
	    if List.mem_assoc v1 m
	    then fold_intrel2 v1 v2 r1 store ff acc m
	    else fold_intrel2 v2 v1 r2 store ff acc m
      end
    let intrel2_duo name v1 v2 f_rs = new intrel2_duo name v1 v2 f_rs


    let fold_relation lv r =
      fun store ff acc m -> Common.prof "Extension.fold_relation" (fun () ->
	let mu = Common.mapfilter (fun v -> try Some (v,store#get_entity (List.assoc v m)) with _ -> None) lv in
	Rel.fold_mu
	  mu lv
	  (fun acc1 mu' ->
	    let m' =
	      List.fold_left
		(fun res (v,o) ->
		  if o <> oid_null && not (List.mem_assoc v mu)
		  then (v, store#get_name o)::res
		  else res)
		m mu' in
	    ff acc1 m')
	  acc r)

    class virtual relation_gen name (lv : var list) =
      object
	inherit t
	method string = name ^ "[" ^ string_of_lv lv ^ "]"
	method vars = List.filter (fun v -> v <> "") lv
	method virtual fold : 'a. 'a fold
      end

    class relation_multi name lv (rels : unit -> Rel.t * (var list * var list * Rel.t) list) =
      object
	inherit relation_gen name lv
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    let r, rs = rels () in
	    let key1, lv1, r1 =
	      try List.find (fun (key, _, _) -> List.for_all (fun v -> List.mem_assoc v m) key) rs
	      with _ -> [], lv, r in
	    fold_relation lv1 r1 store ff acc m
      end

    let relation_multi name lv rels = (new relation_multi name lv rels : t)

    let relation name lv rel = relation_multi name lv (fun () -> rel (), [])
    let relation1 name x ys rel = relation_multi name (x::ys) (fun () -> rel (), [])
    let relation2 name x y zs rels = relation_multi name (x::y::zs)
	(fun () ->
	  let r1, r2 = rels () in
	  r1, [([y],(y::x::zs),r2)])

    class projection (lv : var list) (e : t) =
      object
	inherit t
	method string = "proj[" ^ string_of_lv lv ^ "](" ^ e#string ^ ")"
	method vars = lv
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    e#fold store
	      (fun acc1 m1 ->
		ff acc1
		  (List.filter (fun (v,x) -> List.mem v lv) m1))
	      acc m
      end

    let projection lv e = (new projection lv e : t)

    class co_projection (lv : var list) (e : t) =
      object
	inherit t
	method string = "co-proj[" ^ string_of_lv lv ^ "](" ^ e#string ^ ")"
	method vars = List.filter (fun v -> not (List.mem v lv)) e#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    e#fold store
	      (fun acc1 m1 ->
		ff acc1
		  (List.filter (fun (v,x) -> not (List.mem v lv)) m1))
	      acc m
      end

    let co_projection lv e = (new co_projection lv e : t)


    class stat
	~(from_class : Uri.t -> var -> var -> var -> t)
	~(from_property : Uri.t -> var -> var -> var -> var -> t)
	~(from_statement : var -> var -> var -> var -> var -> t)
	(s : var) (p : var) (o : var) (t : var) (src : var) =
      object
	inherit t
	method string = "rdf:Statement[" ^ String.concat "," [s; p; o; t; src] ^ "]"
	method vars = [s; p; o; t; src]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    try
	      let uri_p = Name.as_uri (List.assoc p m) in
	      try
		if uri_p = Rdf.uri_type
		then
		  let uri_c = Name.as_uri (List.assoc o m) in
		  (from_class uri_c s t src)#fold store ff acc m
		else raise Not_found
	      with Not_found ->
		(from_property uri_p s o t src)#fold store ff acc m
	    with Not_found ->
	      (from_statement s p o t src)#fold store ff acc m
      end

    let stat ~from_class ~from_property ~from_statement s p o t src = (new stat ~from_class ~from_property ~from_statement s p o t src : t)


    class empty =
      object
	inherit t
	method string = "empty"
	method vars : var list = []
	method fold : 'a. 'a fold = fun store ff acc m -> acc
      end

    let empty = (new empty : t)

    class one =
      object
	inherit t
	method string = "one"
	method vars : var list = []
	method fold : 'a. 'a fold = fun store ff acc m -> ff acc m
      end

    let one = (new one : t)

    class single (v : var) (x : Name.t) =
      object
	inherit t
	method string = v ^ " = " ^ Name.contents x
	method vars = [v]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    try
	      let x' = List.assoc v m in
	      if x' = x
	      then ff acc m
	      else acc
	    with Not_found ->
	      ff acc ((v,x)::m)
      end

    let single v x = (new single v x : t)


    class bind (v : var) (expr : expr) =
      object
	inherit t
	method string = v ^ "=" ^ expr#string
	method vars = v :: expr#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    expr#fold store
	      (fun acc1 m1 n1 ->
		try
		  let n1' = List.assoc v m1 in
		  if n1' = n1
		  then ff acc1 m1
		  else acc1
		with Not_found ->
		  ff acc1 ((v,n1)::m1))
	      acc m
      end

    let bind v expr = (new bind v expr : t)

    class assign (v1 : var) (v2 : var) =
      object
	inherit t
	method string = v1 ^ " :=" ^ v2
	method vars = [v1; v2]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    try
	      let n = List.assoc v2 m in
	      ff acc ((v1,n) :: List.remove_assoc v1 m)
	    with Not_found ->
	      failwith "Ext.assign: unbound variable"
      end

    let assign v1 v2 = (new assign v1 v2 : t)

    class join ?(ordered = false) (e1 : t) (e2 : t) =
      (* TODO: optimization when [inter e1#var e2#vars is empty]
	 (e2#fold can be computed and stored once, and folded several times) *)
      object
	inherit t
	method string = e1#string ^ ", " ^ e2#string
	method vars = union_vars e1#vars e2#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    let nb1 = List.fold_left (fun res v1 -> if List.mem_assoc v1 m then res else res+1) 0 e1#vars in
	    let nb2 = List.fold_left (fun res v2 -> if List.mem_assoc v2 m then res else res+1) 0 e2#vars in
	    if ordered || nb1 <= nb2 (* optimization *)
	    then e1#fold store (fun acc' m' -> e2#fold store ff acc' m') acc m
	    else e2#fold store (fun acc' m' -> e1#fold store ff acc' m') acc m
      end

    let join ?ordered e1 e2 =
      if e1 == one then e2
      else if e2 == one then e1
      else (new join ?ordered e1 e2 : t)

    class union (e1 : t) (e2 : t) =
      object
	inherit t
	method string = "(" ^ e1#string ^ " ; " ^ e2#string ^ ")"
	method vars = union_vars e1#vars e2#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    e2#fold store ff (e1#fold store ff acc m) m
      end

    let union e1 e2 = (new union e1 e2 : t)

    class negation (e1 : t) =
      object
	inherit t
	method string = "not(" ^ e1#string ^ ")"
	method vars = []
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    if e1#succeeds store m
	    then acc
	    else ff acc m
      end

    let negation e1 = (new negation e1 : t)

    class minus (e1 : t) (e2 : t) =
      object
	inherit t
	method string = "(" ^ e1#string ^ " / " ^ e2#string ^ ")"
	method vars = e1#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    e1#fold store
	      (fun acc1 m1 ->
		if e2#fails store m1
		then ff acc1 m1
		else acc1)
	      acc m
      end

    let minus e1 e2 = (new minus e1 e2 : t)

    class optional (e1 : t) =
      object
	inherit t
	method string = "maybe(" ^ e1#string ^ ")"
	method vars = e1#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    let succ, acc1 =
	      e1#fold store
		(fun (succ,acc1) m1 -> true, ff acc1 m1)
		(false,acc) m in
	    if succ
	    then acc1
	    else ff acc1 m
      end

    let optional e1 = (new optional e1 : t)

    class check (c : constr) =
      object
	inherit t
	method string = "check(" ^ c#string ^ ")"
	method vars = []
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    if c#succeeds store m
	    then ff acc m
	    else acc
      end

    let check c = (new check c : t)

    class filter (e : t) (c : constr) =
      object
	inherit t
	method string = "(" ^ e#string ^ " when " ^ c#string ^ ")"
	method vars = e#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    e#fold store
	      (fun acc1 m1 ->
		if c#succeeds store m1
		then ff acc1 m1
		else acc1)
	      acc m
      end

    let filter e c = (new filter e c : t)

    class cond (c : constr) (e1 : t) (e2 : t) =
      object
	inherit t
	method string = "if " ^ c#string ^ " then " ^ e1#string ^ " else " ^ e2#string
	method vars = union_vars e1#vars e2#vars
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    if c#succeeds store m
	    then e1#fold store ff acc m
	    else e2#fold store ff acc m
      end

    let cond c e1 e2 = (new cond c e1 e2 : t)

    class unify (v1 : var) (v2 : var) =
      object
	inherit t
	method string = v1 ^ " == " ^ v2
	method vars = [v1; v2]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    try
	      let x1 = List.assoc v1 m in
	      try
		let x2 = List.assoc v2 m in
		if x1 = x2
		then ff acc m
		else acc
	      with Not_found ->
		ff acc ((v2,x1)::m)
	    with Not_found ->
	      try
		let x2 = List.assoc v2 m in
		ff acc ((v1,x2)::m)
	      with Not_found ->
		prerr_endline "Extension: open unification not handled.";
		ff acc m

(*		
	method is_true (m : map) =
	  try List.assoc v1 m = List.assoc v2 m
	  with Not_found -> true
*)
      end

    let unify v1 v2 = (new unify v1 v2 : t)

    class neq (v1 : var) (v2 : var) = (* SHOULD be a constraint *)
      object
	inherit t
	method string = v1 ^ " != " ^ v2
	method vars = [v1; v2]
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    try
	      if List.assoc v1 m <> List.assoc v2 m
	      then ff acc m
	      else acc
	    with Not_found ->
	      prerr_endline "Extension: open inequality not handled";
	      ff acc m
(*
	method is_true (m : map) =
	  try List.assoc v1 m <> List.assoc v2 m
	  with Not_found -> true
*)
      end

    let neq v1 v2 = (new neq v1 v2 : t)


    (* constraints *)

    class bound (v : var) =
      object
	method string = "bound(" ^ v ^ ")"
	method succeeds (store : store) (m : map) = List.mem_assoc v m
      end

    let bound v = (new bound v : constr)

    class pred (name : string) (p : Builtins.pred) (lv : var list) =
    object
      method string = name ^ "(" ^ String.concat "," lv ^ ")"
      method succeeds (store : store) (m : map) =
	try
	  let args = List.map (fun v -> List.assoc v m) lv in
	  p#eval args
	with Not_found -> failwith "Extension.pred: some variable is unbound"
    end

    let pred name p lv = new pred name p lv

    class succeeds (e : t) =
      object
	method string = "succeeds(" ^ e#string ^ ")"
	method succeeds (store : store) (m : map) = e#succeeds store m
      end

    let succeeds e = (new succeeds e : constr)

    class fails (e : t) =
      object
	method string = "fails(" ^ e#string ^ ")"
	method succeeds (store : store) (m : map) = e#fails store m
      end

    class conj (c1 : constr) (c2 : constr) =
      object
	method string = "(" ^ c1#string ^ " /\ " ^ c2#string ^ ")"
	method succeeds (store : store) (m : map) = c1#succeeds store m && c2#succeeds store m
      end

    let conj c1 c2 = (new conj c1 c2 : constr)

    class forall (e1 : t) (e2 : t) =
      object
	method string = "forall(" ^ e1#string ^ " => " ^ e2#string ^ ")"
	method succeeds (store : store) (m : map) =
	  let exists, forall =
	    e1#fold store
	      (fun (_,acc) m1 ->
		true, acc && e2#succeeds store m1)
	      (false,true) m in
	  exists && forall
      end

    let forall e1 e2 = (new forall e1 e2 : constr)

    class forno (e1 : t) (e2 : t) =
      object
	method string = "forno(" ^ e1#string ^ " , " ^ e2#string ^ ")"
	method succeeds (store : store) (m : map) =
	  e1#fold store
	    (fun acc1 m1 ->
	      acc1 && e2#fails store m1)
	    true m
      end

    let forno e1 e2 = (new forno e1 e2 : constr)

    class exactly (n : int) (v : var) (e1 : t) =
      object
	method string = "exactly(" ^ string_of_int n ^ "," ^ v ^ "," ^ e1#string ^ ")"
	method succeeds (store : store) (m : map) =
	  let pos = ref (Rel.empty 1) in
	  e1#fold store
	    (fun acc1 m1 ->
	      try
		let o = store#get_entity (List.assoc v m1) in
		pos := Rel.add [o] !pos
	      with Not_found -> ())
	    () m;
	  Rel.cardinal !pos = n
      end

    let exactly n v e1 = (new exactly n v e1 : constr)

(*
    class count (p : int -> int -> bool) (v : var) (e1 : t) (e2 : t) =
      object
	method string = "count(" ^ e1#string ^ "," ^ e2#string ^ ")"
	method succeeds (store : store) (m : map) =
	  let pos, neg = ref (Rel.empty 1), ref (Rel.empty 1) in
	  e1#fold store
	    (fun acc1 m1 ->
	      try
		let x = List.assoc v m1 in
		if Rel.mem [x] !pos then ()
		else if Rel.mem [x] !neg then ()
		else
		  if e2#succeeds store m1
		  then pos := Rel.add [x] !pos
		  else neg := Rel.add [x] !neg
	      with Not_found -> ())
	    () m;
	  p (Rel.cardinal !pos) (Rel.cardinal !neg)
      end

    let count p v e1 e2 = (new count p v e1 e2 : constr)

    let at_least n = count (fun pos neg -> pos >= n)
    let at_most n = count (fun pos neg -> pos <= n)
    let more_than n = count (fun pos neg -> pos > n)
    let less_than n = count (fun pos neg -> pos < n)
    let between n1 n2 = count (fun pos neg -> n1 <= pos && pos <= n2)
    let exactly n = count (fun pos neg -> pos = n)

    let at_least_percent p = count (fun pos neg -> (float pos) /. (float (pos+neg)) >= p)
*)

    (* closures of binary relations *)

    class transitive (e : var -> var -> t) =
      (fun x y ->
	object (self)
	  inherit t
	  method string = "trans[" ^ string_of_lv [x;y] ^ "](" ^ (e x y)#string ^ ")"

	  method vars = (e x y)#vars

	  method fold : 'a. 'a fold =
	    let rec star_fwd (n,path) x y =
	      fun store ff acc m ->
		let acc1 = (unify x y)#fold store ff acc m in
		plus_fwd (n,path) x y store ff acc1 m

	    and star_bwd (n,path) x y =
	      fun store ff acc m ->
		let acc1 = (unify x y)#fold store ff acc m in
		plus_bwd (n,path) x y store ff acc1 m

	    and plus_fwd (n,path) x y =
	      fun store ff acc m ->
		let z = "_trans_" ^ string_of_int n in
		(e x z)#fold store
		  (fun acc1 m1 ->
		    try
		      let v = List.assoc z m1 in
		      if List.for_all (fun p -> List.assoc p m1 <> v) path
		      then star_fwd (n+1,z::path) z y store ff acc1 m1
		      else acc1
		    with Not_found ->
		      acc1)
		  acc m

	    and plus_bwd (n,path) x y =
	      fun store ff acc m ->
		let z = "_trans_" ^ string_of_int n in
		(e z y)#fold store
		  (fun acc1 m1 ->
		    try
		      let v = List.assoc z m1 in
		      if List.for_all (fun p -> List.assoc p m1 <> v) path
		      then star_bwd (n+1,z::path) x z store ff acc1 m1
		      else acc1
		    with Not_found ->
		      acc1)
		  acc m
	    in
	    fun store ff acc m ->
	      if List.mem_assoc x m
	      then plus_fwd (1,[x]) x y store ff acc m
	      else plus_bwd (1,[y]) x y store ff acc m
	end)

    let transitive e = (new transitive e : var -> var -> t)

    (* combinators *)

    let inverse (e : var -> var -> t) =
      (fun x y -> e y x)

    let symmetric (e : var -> var -> t) =
      (fun x y ->
	union (e x y) (e y x))

    let reflexive (e : var -> var -> t) =
      (fun x y -> union (unify x y) (e x y))

    (* cache *)
(*
    class cache (e : t) =
      let lv = e#vars in
      object (self)
	inherit t
	val mutable valid = false
	val mutable cached_rel : Rel.t = Rel.empty 0

	method string = "cache(" ^ e#string ^ ")"
	method vars = lv
	method fold : 'a. 'a fold =
	  fun store ff acc m ->
	    if not valid
	    then begin
	      cached_rel <- e#relation store lv;
	      valid <- true
	    end;
	    let mu = Common.mapfilter (fun v -> try Some (v,store#get_entity (List.assoc v m)) with _ -> None) lv in
	    Rel.fold_mu
	      mu lv
	      (fun acc1 mu' ->
		let m' =
		  List.fold_left
		    (fun res (v,o) ->
		      if o <> oid_null && not (List.mem_assoc v mu)
		      then (v, store#get_name o)::res
		      else res)
		    m mu' in
		ff acc1 m')
	      acc cached_rel
      end

    let cache e = (new cache e : t)
*)

    (* expressions *)

    class expr_empty =
      object
	inherit expr
	method string = "empty"
	method vars = []
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    acc
      end

    let expr_empty = (new expr_empty : expr)

    class expr_var (v : var) =
      object
	inherit expr
	method string = "?"^v
	method vars = []
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    try
	      let n = List.assoc v m in
	      eff acc m n
	    with Not_found ->
	      failwith ("unbound variable: " ^ v)
	      (* acc *)
      end

    let expr_var v = (new expr_var v : expr)

    class expr_oid (o : oid) =
      object
	inherit expr
	method string = "#" ^ string_of_int o
	method vars = []
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    try
	      let n = store#get_name o in
	      eff acc m n
	    with _ ->
	      acc
      end

    let expr_oid o = (new expr_oid o : expr)

    class expr_name (n : Name.t) =
      object
	inherit expr
	method string = "\"" ^ String.escaped (Name.contents n) ^ "\""
	method vars = []
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    eff acc m n
      end

    let expr_name n = (new expr_name n : expr)

    class expr_st (expr : expr) (e : t) =
      object
	inherit expr
	method string = "(" ^ expr#string ^ " st " ^ e#string ^ ")"
	method vars = union_vars e#vars expr#vars
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    e#fold store
	      (fun acc1 m1 ->
		expr#fold store eff acc1 m1)
	      acc m
      end

    let expr_st expr e = (new expr_st expr e : expr)

    class expr_union (expr1 : expr) (expr2 : expr) =
      object
	inherit expr
	method string = "(" ^ expr1#string ^ " or " ^ expr2#string ^ ")"
	method vars = inter_vars expr1#vars expr2#vars
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    let acc1 = expr1#fold store eff acc m in
	    let acc2 = expr2#fold store eff acc1 m in
	    acc2
      end

    let expr_union expr1 expr2 = (new expr_union expr1 expr2 : expr)

    let rec expr_enum_fold (store : store) (eff : 'a -> map -> Name.t -> 'a) (acc : 'a) (m : map) : expr list -> 'a =
      function
	| [] -> acc
	| e::le ->
	    let acc1 = e#fold store eff acc m in
	    expr_enum_fold store eff acc1 m le

    class expr_enum (lexpr : expr list) =
      object
	inherit expr
	method string = "enum(" ^ String.concat ", " (List.map (fun e -> e#string) lexpr) ^ ")"
	method vars = []
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    expr_enum_fold store eff acc m lexpr
      end

    let expr_enum lexpr = (new expr_enum lexpr : expr)


    let expr_quote_vars (le : expr list) : var list =
      List.fold_left (fun res e -> union_vars res e#vars) [] le
    let rec expr_quote_fold (store : store) (elff : 'a -> map -> string list -> 'a) (acc : 'a) (m : map) (ls : string list) : expr list -> 'a =
      function
	| [] -> elff acc m ls
	| e::le ->
	    try
	      e#fold store
		(fun acc1 m1 n1 -> expr_quote_fold store elff acc1 m1 (ls@[Name.contents n1]) le)
		acc m
	    with _ ->
	      expr_quote_fold store elff acc m (ls@["_"]) le

    class expr_quote (lexpr : expr list) =
      object
	inherit expr
	method string = "quote(" ^ String.concat ", " (List.map (fun e -> e#string) lexpr) ^ ")"
	method vars = expr_quote_vars lexpr
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    expr_quote_fold store
	      (fun acc1 m1 ls ->
		let s = String.concat "" ls in
		eff acc1 m1 (Name.of_string s))
	      acc m [] lexpr
      end

    let expr_quote le = (new expr_quote le : expr)


    class expr_cond (c : constr) (expr1 : expr) (expr2 : expr) =
      object
	inherit expr
	method string = "if " ^ c#string ^ " then " ^ expr1#string ^ " else " ^ expr2#string ^ " endif"
	method vars = union_vars expr1#vars expr2#vars
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    if c#succeeds store m
	    then expr1#fold store eff acc m
	    else expr2#fold store eff acc m
      end

    let expr_cond c expr1 expr2 = (new expr_cond c expr1 expr2 : expr)

    class expr_func (name : string) (f : Builtins.func) (lv : var list) =
    object
      inherit expr
      method string = name ^ "(" ^ String.concat "," lv ^ ")"
      method vars = lv
      method fold : 'a. 'a expr_fold =
	fun store eff acc m ->
	  try
	    let args = List.map (fun v -> List.assoc v m) lv in
	    let ln = f#eval args in
	    List.fold_left
	      (fun acc1 n -> eff acc1 m n)
	      acc ln
	  with _ -> acc
    end

    let expr_func name f lv = (new expr_func name f lv : expr)

(*
    class aggreg_table (g : Prim.aggreg) (keys : var list) =
      object
	val ht : (Name.t list, Prim.accu) Hashtbl.t = Hashtbl.create 13
	method take dims n =
	  let accu =
	    try Hashtbl.find ht dims
	    with Not_found ->
	      let accu = g#get_accu in
	      Hashtbl.add ht dims accu;
	      accu in
	  accu#take n
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    Hashtbl.fold
	      (fun dims accu acc1 ->
		let n = accu#return in
		let m1 = List.combine keys dims @ m in
		eff acc1 m1 n)
	      ht acc
	initializer
	  if keys = []
	  then Hashtbl.add ht [] g#get_accu
      end

    class expr_aggreg (g : Prim.aggreg) (e : expr) (dims : (var * t) list) =
      let keys, lext = List.split dims in
      object
	inherit expr
	method string = g#uri ^ " per (" ^ String.concat "," keys ^ ") of " ^ e#string ^ " where (" ^ list_string lext ^ ")"
	method vars = keys
	method fold : 'a. 'a expr_fold =
	  fun store eff acc m ->
	    let tab = new aggreg_table g keys in
	    e#fold store
	      (fun acc1 m1 n ->
		list_fold store
		  (fun _ m2 ->
		    tab#take (List.map (fun k -> List.assoc k m2) keys) n)
		  acc1 m1 lext)
	      () m;
	    tab#fold store eff acc m
      end

    let expr_aggreg g e keys = (new expr_aggreg g e keys : expr)
*)

(*
    class expr_choose (e : expr) (ext : t) =
      object
	inherit expr
	method string = "choose " ^ e#string ^ " st " ^ ext#string
	method eval store m =
	  let m' = ext#choose store m in
	  e#eval store m'
      end

    let expr_choose : aggreg = fun e ext -> (new expr_choose e ext : expr)

    class expr_unique (e : expr) (ext : t) =
      object
	inherit expr
	method string = "the " ^ e#string ^ " st " ^ ext#string
	method eval store m =
	  let m' = ext#unique store m in
	  e#eval store m'
      end

    let expr_unique : aggreg = fun e ext -> (new expr_unique e ext : expr)
*)

  end
