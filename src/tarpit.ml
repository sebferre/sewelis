(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(*
   Module providing "Out of the tarpit" programming
*)

module Intmap = Intmap.M

class virtual observer =
  object (self)
    val mutable subjects : subject Intmap.t = Intmap.empty
    method register_to (s : subject) =
      let id = Oo.id s in
      if not (Intmap.mem id subjects)
      then subjects <- Intmap.set id s subjects;
      s#register (self :> observer)
    method unregister_to_all =
      Intmap.iter (fun id s -> s#unregister (self :> observer)) subjects
    method virtual notified_by : subject -> unit
  end

and subject =
object (self)
  val mutable observers : observer Intmap.t = Intmap.empty
  method register o =
    let id = Oo.id o in (* relying on the fact that objects have unique ids *)
    if not (Intmap.mem id observers)
    then observers <- Intmap.set id o observers
  method unregister (o : observer) =
    let id = Oo.id o in
    observers <- Intmap.remove id observers
  method notify = (* quick *)
    Intmap.iter (fun id o -> o#notified_by (self :> subject)) observers
end
  
let blind_observer =
  object
    inherit observer
    method register_to s = ()
    method notified_by s = ()
  end

let void_subject =
object
  method register o = ()
  method unregister o = ()
  method notify = ()
end

class virtual ['a] data =
  object
    method virtual contents : obs:observer -> 'a
  end
    
class ['a] state (init : 'a) =
  object (self)
    inherit subject
    val mutable c : 'a = init
	
    method update (f : 'a -> 'a option) : unit =
      match f c with
      | None -> () (* no change *)
      | Some c' -> (* new contents *)
	  c <- c';
	  self#notify
	    
    method contents ~(obs : observer) : 'a =
      obs#register_to (self :> subject);
      c
  end

class ['a] feeder (init : 'a) =
  object (self)
    inherit subject
    val mutable def : 'a -> 'a option = fun _ -> None
    val mutable c : 'a = init
	
    method define (f : 'a -> 'a option) =
      (* should be passed to constructor, but impractical as class instances *)
      def <- f
	  
    method contents ~(obs : observer) =
      obs#register_to (self :> subject);
      c
	
    method notified =
      match def c with
      | None -> () (* no change *)
      | Some c' -> (* new contents *)
	  c <- c';
	  self#notify
  end

exception Undefined_view
    
class ['a] view (label : string) =
  object (self)
    inherit subject
    inherit observer
    val mutable def : observer -> 'a = fun _ -> raise Undefined_view
    val mutable c_opt : 'a option = None
    val mutable lock : bool = false

    method define (f : observer -> 'a) =
      (* should be passed to constructor, but impractical as class instances *)
      def <- f
	  
    method contents ~(obs : observer) : 'a =
      obs#register_to (self :> subject);
      match c_opt with
      | Some c -> c
      | None ->
	if lock then failwith ("Tarpit.observer#contents: dependency cycle! @ " ^ label);
	lock <- true;
	try
	  let c = def (self :> observer) in
	  c_opt <- Some c;
	  lock <- false;
	  c
	with exn ->
	  lock <- false;
	  raise exn
	    
    method notified_by s =
      if c_opt <> None then begin
	c_opt <- None; self#notify
      end
  end
    
class ['a] incremental_view (label : string) (init : 'a) =
  object (self)
    inherit subject
    inherit observer
    val mutable def : observer -> 'a -> 'a = fun _ -> raise Undefined_view
    val mutable previous_c = init
    val mutable c_opt : 'a option = None
    val mutable lock : bool = false

    method define (f : observer -> 'a -> 'a) =
      (* should be passed to constructor, but impractical as class instances *)
      def <- f
	  
    method contents ~(obs : observer) : 'a =
      obs#register_to (self :> subject);
      match c_opt with
      | Some c -> c
      | None ->
	  if lock then failwith ("Tarpit.incremental_view#contents: dependency cycle! @" ^ label);
	  lock <- true;
	  try
	    let c = def (self :> observer) previous_c in
	    previous_c <- c;
	    c_opt <- Some c;
	    lock <- false;
	    c
	  with exn ->
	    lock <- false;
	    raise exn
	    
    method notified_by s =
      if c_opt <> None then begin
	c_opt <- None; self#notify
      end
  end

class trigger =
  object (self)
    inherit observer
    val mutable def : observer -> unit = fun _ -> ()
    val mutable valid : bool = false
	
    method define (f : observer -> unit) =
      def <- f
	  
    method refresh =
      if not valid then begin
	def (self :> observer);
	valid <- true
      end
	  
    method notified_by s =
      valid <- false
  end
    
let effect (f : observer -> unit) : unit =
  f blind_observer
