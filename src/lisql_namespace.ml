(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

let prefix = "lisql:"
let namespace = "http://www.irisa.fr/LIS/ferre/RDFS/lisql#"

let uri_store = namespace ^ "store"

let uri_Class = namespace ^ "Class"
let uri_Assertion = namespace ^ "Assertion"
let uri_XMLElement = namespace ^ "XMLElement"
let uri_Bookmark = namespace ^ "Bookmark"

let uri_inverseLabel = namespace ^ "inverseLabel"
let uri_subEntityOf = namespace ^ "subEntityOf"

let uri_guest = namespace ^ "guest"
let uri_HomeQuery = namespace ^ "HomeQuery"
let uri_DraftBag = namespace ^ "DraftBag"
