(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* VoID vocabulary *)

(* namespace *)
let prefix = "void:"
let namespace = "http://rdfs.org/ns/void#"
(* classes *)
let _Dataset = "void:Dataset"
let _DatasetDescription = "void:DatasetDescription"
let _Linkset = "void:Linkset"
let _TechnicalFeature = "void:TechnicalFeature"
(* properties *)
let _class = "void:class"
let _classes = "void:classes"
let _classPartition = "void:classPartition"
let _dataDump = "void:dataDump"
let _distinctObjects = "void:distinctObjects"
let _distinctSubjects = "void:distinctSubjects"
let _documents = "void:documents"
let _entities = "void:entities"
let _exampleResource = "void:exampleResource"
let _feature = "void:feature"
let _inDataset = "void:inDataset"
let _linkPredicate = "void:linkPredicate"
let _objectsTarget = "void:objectsTarget"
let _openSearchDescription = "void:openSearchDescription"
let _properties = "void:properties"
let _property = "void:property"
let _propertyPartition = "void:propertyPartition"
let _rootResource = "void:rootResource"
let _sparqlEndpoint = "void:sparqlEndpoint"
let _subjectsTarget = "void:subjectsTarget"
let _subset = "void:subset"
let _target = "void:target"
let _triples = "void:triples"
let _uriLookupEndpoint = "void:uriLookupEndpoint"
let _uriRegexPattern = "void:uriRegexPattern"
let _uriSpace = "void:uriSpace"
let _vocabulary = "void:vocabulary"

let make = Uri.make namespace prefix

let uri_Dataset = make _Dataset
let uri_DatasetDescription = make _DatasetDescription
let uri_Linkset = make _Linkset
let uri_TechnicalFeature = make _TechnicalFeature
let uri_class = make _class
let uri_classes = make _classes
let uri_classPartition = make _classPartition
let uri_dataDump = make _dataDump
let uri_distinctObjects = make _distinctObjects
let uri_distinctSubjects = make _distinctSubjects
let uri_documents = make _documents
let uri_entities = make _entities
let uri_exampleResource = make _exampleResource
let uri_feature = make _feature
let uri_inDataset = make _inDataset
let uri_linkPredicate = make _linkPredicate
let uri_objectsTarget = make _objectsTarget
let uri_openSearchDescription = make _openSearchDescription
let uri_properties = make _properties
let uri_property = make _property
let uri_propertyPartition = make _propertyPartition
let uri_rootResource = make _rootResource
let uri_sparqlEndpoint = make _sparqlEndpoint
let uri_subjectsTarget = make _subjectsTarget
let uri_subset = make _subset
let uri_target = make _target
let uri_triples = make _triples
let uri_uriLookupEndpoint = make _uriLookupEndpoint
let uri_uriRegexPattern = make _uriRegexPattern
let uri_uriSpace = make _uriSpace
let uri_vocabulary = make _vocabulary
