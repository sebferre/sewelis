(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(** Graphical interface for Sewelis. *)

module Ext = Lisql.Ext
module Name = Name.Make

open Format
open StdLabels
open GdkKeysyms
open Gobject.Data


(* command line arguments *)

(*let current_src = ref (Lisql.Namespace.uri_guest)*)
let log_file = ref "default"
let logui = ref false
let readonly = ref false
let rdf_files = ref []

let _ =
  Gc.set { (Gc.get ()) with
(*    Gc.verbose = 0x01 + 0x10; *)
    Gc.minor_heap_size = 32 * 1024 * 1024;
    Gc.major_heap_increment = 32 * 1024 * 1024;
  };
  prerr_endline "Gui: parsing command line arguments...";
  Arg.parse
(*    [ ("-user", Arg.String (fun uri -> current_src := uri), "user URI"); *)
    [ ("-logui", Arg.Unit (fun () -> logui := true), "logs the GUI session");
      ("-readonly", Arg.Unit (fun () -> readonly := true), "makes open contexts read only")
    ]
    (fun f ->
      if Filename.check_suffix f "rdf" || Filename.check_suffix f "owl" then
	rdf_files := f::!rdf_files
      else
	log_file := f)
    "./lisql.exe logfile [-logui] [-readonly]"

module Logui =
  struct
    open Turtle

    let prefix = "logui:"
    let namespace = "http://www.irisa.fr/LIS/ferre/RDFS/logui#"

    let xsd_string = Qname (Xsd.prefix, "string")
    let xsd_dateTime = Qname (Xsd.prefix, "dateTime")
    let xsd_integer = Qname (Xsd.prefix, "integer")
    let rdf_type = Qname (Rdf.prefix, "type")
    let lisql_Class = Qname (Lisql.Namespace.prefix, "Class")
    let lisql_Assertion = Qname (Lisql.Namespace.prefix, "Assertion")
    let uri_Session = Qname (prefix, "Session")
    let uri_QueryChange = Qname (prefix, "QueryChange")
    let uri_Back = Qname (prefix, "Back")
    let uri_Forward = Qname (prefix, "Forward")
    let uri_Root = Qname (prefix, "Root")
    let uri_Home = Qname (prefix, "Home")
    let uri_Bookmarks = Qname (prefix, "Bookmarks")
    let uri_Drafts = Qname (prefix, "Drafts")
    let uri_Run = Qname (prefix, "Run")
    let uri_ToggleEach = Qname (prefix, "ToggleEach")
    let uri_FocusUp = Qname (prefix, "FocusUp")
    let uri_FocusDown = Qname (prefix, "FocusDown")
    let uri_FocusLeft = Qname (prefix, "FocusLeft")
    let uri_FocusRight = Qname (prefix, "FocusRight")
    let uri_FocusTab = Qname (prefix, "FocusTab")
    let uri_CrossOver = Qname (prefix, "CrossOver")
    let uri_InsertForEach = Qname (prefix, "InsertForEach")
    let uri_InsertCond = Qname (prefix, "InsertCond")
    let uri_InsertSeq = Qname (prefix, "InsertSeq")
    let uri_InsertAnd = Qname (prefix, "InsertAnd")
    let uri_InsertOr = Qname (prefix, "InsertOr")
    let uri_InsertAndNot = Qname (prefix, "InsertAndNot")
    let uri_InsertAndMaybe = Qname (prefix, "InsertAndMaybe")
    let uri_ToggleNot = Qname (prefix, "ToggleNot")
    let uri_ToggleMaybe = Qname (prefix, "ToggleMaybe")
    let uri_InsertIsThere = Qname (prefix, "InsertIsThere")
    let uri_InsertAn = Qname (prefix, "InsertAn")
    let uri_InsertEach = Qname (prefix, "InsertEach")
    let uri_InsertEvery = Qname (prefix, "InsertEvery")
    let uri_InsertOnly = Qname (prefix, "InsertOnly")
    let uri_InsertNo = Qname (prefix, "InsertNo")
    let uri_For = Qname (prefix, "For")
    let uri_QuoteUnquote = Qname (prefix, "QuoteUnquote")
    let uri_ToggleOpt = Qname (prefix, "ToggleOpt")
    let uri_ToggleTrans = Qname (prefix, "ToggleTrans")
    let uri_ToggleSym = Qname (prefix, "ToggleSym")
    let uri_Name = Qname (prefix, "Name")
    let uri_Describe = Qname (prefix, "Describe")
    let uri_Select = Qname (prefix, "Select")
    let uri_Delete = Qname (prefix, "Delete")
    let uri_Reverse = Qname (prefix, "Reverse")
    let uri_Etc = Qname (prefix, "Etc")
    let uri_LeastObjects = Qname (prefix, "LeastObjects")
    let uri_LessObjects = Qname (prefix, "LessObjects")
    let uri_MoreObjects = Qname (prefix, "MoreObjects")
    let uri_MostObjects = Qname (prefix, "MostObjects")
    let uri_SingleFeatureSelection = Qname (prefix, "SingleFeatureSelection")
    let uri_AllOfFeaturesSelection = Qname (prefix, "AllOfFeaturesSelection")
    let uri_OneOfFeaturesSelection = Qname (prefix, "OneOfFeaturesSelection")
    let uri_NoneOfFeaturesSelection = Qname (prefix, "NoneOfFeaturesSelection")
    let uri_VariableCreation = Qname (prefix, "VariableCreation")
    let uri_TextCreation = Qname (prefix, "TextCreation")
    let uri_DateCreation = Qname (prefix, "DateCreation")
    let uri_DateTimeCreation = Qname (prefix, "DateTimeCreation")
    let uri_DurationCreation = Qname (prefix, "DurationCreation")
    let uri_PeriodCreation = Qname (prefix, "PeriodCreation")
    let uri_FilenameCreation = Qname (prefix, "FilenameCreation")
    let uri_EntityCreation = Qname (prefix, "EntityCreation")
    let uri_ClassCreation = Qname (prefix, "ClassCreation")
    let uri_ForwardPropertyCreation = Qname (prefix, "ForwardPropertyCreation")
    let uri_BackwardPropertyCreation = Qname (prefix, "BackwardPropertyCreation")
    let uri_StructureCreation = Qname (prefix, "StructureCreation")
    let uri_CompletionSelection = Qname (prefix, "CompletionSelection")
    let uri_VariableEntry = Qname (prefix, "VariableEntry")
    let uri_NameEntry = Qname (prefix, "NameEntry")
    let uri_ClassEntry = Qname (prefix, "ClassEntry")
    let uri_ForwardPropertyEntry = Qname (prefix, "ForwardPropertyEntry")
    let uri_BackwardPropertyEntry = Qname (prefix, "BackwardPropertyEntry")
    let uri_StructureEntry = Qname (prefix, "StructureEntry")
    let uri_EntityEntry = Qname (prefix, "EntityEntry")
    let uri_AnswerSelection = Qname (prefix, "AnswerSelection")
    let uri_session = Qname (prefix, "session")
    let uri_login = Qname (prefix, "login")
    let uri_date = Qname (prefix, "date")
    let uri_time = Qname (prefix, "time")
    let uri_previous = Qname (prefix, "previous")
    let uri_class = Qname (prefix, "class")
    let uri_assertion = Qname (prefix, "assertion")
    let uri_feature = Qname (prefix, "feature")
    let uri_focus = Qname (prefix, "focus")

    let uri_this_session = Relative "#session"

    let out =
      if !logui
      then
	let filename = !log_file ^ "-session" ^ Name.string_dateTime_of_UnixTime (Unix.time ()) ^ ".ttl" in
	open_out filename
      else stdout

    let start_time = Unix.time ()

    let previous_item = ref uri_this_session

(*    let getlogin () = Unix.getlogin () *)
	(* known bug: does not work with redirected input *)
    let getlogin () = (Unix.getpwuid (Unix.getuid ())).Unix.pw_name

    let prelude =
      if !logui then begin
	let stats =
	  Prefix (Xsd.prefix, Absolute Xsd.namespace) ::
	  Prefix (Rdf.prefix, Absolute Rdf.namespace) ::
	  Prefix (Rdfs.prefix, Absolute Rdfs.namespace) ::
	  Prefix (Lisql.Namespace.prefix, Absolute Lisql.Namespace.namespace) ::
	  Prefix (prefix, Absolute namespace) ::
	  Descr (URI uri_this_session,
		 (rdf_type, [URI uri_Session])::
		 (uri_date, [Typed (Name.string_dateTime_of_UnixTime start_time, xsd_dateTime)])::
		 (uri_login, [Typed (getlogin (), xsd_string)])::
		 [])::
	  [] in
	output out stats
      end
	  
    let item : Turtle.uri -> (Tarpit.observer -> feature list) -> unit =
      let cpt = ref 0 in
      fun cl features ->
	if !logui then
	  Tarpit.effect (fun obs ->
	    let time = Unix.time () in
	    let item = incr cpt; Relative ("#item" ^ string_of_int !cpt) in
	    let stat =
	      Descr (URI item,
		     (rdf_type, [URI cl])::
		     (uri_session, [URI uri_this_session])::
		     (uri_previous, [URI !previous_item])::
		     (uri_date, [Typed (Name.string_dateTime_of_UnixTime time, xsd_dateTime)])::
		     (uri_time, [Typed (string_of_int (int_of_float (time -. start_time)), xsd_integer)])::
		     features obs) in
	    previous_item := item;
	    output out [stat])

    let feature_class x = (uri_class, [Typed (Lisql.Syntax.string_of_class x, lisql_Class)])
    let feature_assertion a = (uri_assertion, [Typed (Lisql.Syntax.string_of_assertion a, lisql_Assertion)])
    let feature_feature (f : Lisql.Feature.feature) = (uri_feature, [Plain (f#string, "")])

  end

(* utilities *)

let do_while_idle : (unit -> unit) Queue.t -> Glib.Idle.id =
  fun q ->
    Glib.Idle.add (fun () ->
      try Queue.take q () ; true
      with Queue.Empty -> false)

let utf s =
  if Glib.Utf8.validate s
  then s
  else try Glib.Convert.locale_to_utf8 s with _ -> s

let loc s =
  try Glib.Convert.locale_from_utf8 s with _ -> s

let rec create_directory dirname =
  let parent = Filename.dirname dirname in
  if not (Sys.file_exists parent) then
    create_directory parent;
  Unix.mkdir dirname 0o751

let create_file filename =
  let dirname = Filename.dirname filename in
  if not (Sys.file_exists dirname) then
    create_directory dirname;
  let ch = open_out filename in (* creating the file *)
  close_out ch

let wget url path =
  let code = Sys.command ("wget -q -O " ^ path ^ " \"" ^ url ^ "\"") in
  if code <> 0 then invalid_arg ("Gui.path_of_url: invalid url: " ^ url)

let path_of_url =
  let file_prefix = "file://" in
  let k = String.length file_prefix in
  fun url ->
    let n = String.length url in
    if k < n && String.sub url 0 k = file_prefix
    then Str.global_replace (Str.regexp_string "%20") " " (String.sub url k (n-k))
	(* hack for converting escaped URL to path: only for spaces (TODO: generalize) *)
    else
      let path = Filename.temp_file "url" "" in
      wget url path;
      path


let dir_sep =
  match Sys.os_type with
  | "Unix" -> "/"
  | "Cygwin" -> "/"
  | "Win32" -> "\\"
  | _ -> "/"

let last_dir = ref (Sys.getcwd () ^ dir_sep)

let awaken f final =
  ignore (f ());
  final ()

(* graphical utilities *)

let pixbuf_of_picture =
  let ht = Hashtbl.create 100 in
  fun ~maxside url ->
    try Hashtbl.find ht (url,maxside)
    with Not_found ->
      let filename = path_of_url url in
      let pixbuf = GdkPixbuf.from_file_at_size filename ~width:maxside ~height:maxside in
      Hashtbl.add ht (filename,maxside) pixbuf;
      pixbuf

let error_message msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`ERROR
      ~buttons:GWindow.Buttons.ok
      ~modal:true () in
  md#run ();
  md#destroy ()

let info_message ?title ?icon msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`INFO
      ?title
      ?icon
      ~buttons:GWindow.Buttons.ok
      ~modal:true () in
  md#run ();
  md#destroy ()

let question msg =
  let md = GWindow.message_dialog
      ~message:(utf msg)
      ~message_type:`QUESTION
      ~buttons:GWindow.Buttons.yes_no
      ~modal:true () in
  let res = md#run () in
  md#destroy ();
  res = `YES

let confirm_overwrite filename =
  not (Sys.file_exists filename) || question "This file already exists. Are you sure you want to overwrite it ?"

let file_dialog ~title ?filename ?filter ~callback () =
  let sel = GWindow.file_selection ~title ~modal:true ?filename () in
  sel#cancel_button#connect#clicked ~callback:sel#destroy;
  sel#ok_button#connect#clicked
    ~callback:(fun () ->
      let name = loc sel#filename in
      sel#destroy ();
      try callback name
      with exn -> error_message (Printexc.to_string exn));
  sel#show ();
  ( match filter with
  | Some s -> sel#complete ~filter:s
  | None -> ())

let open_url uri =
  let code = Sys.command ("xdg-open \"" ^ uri ^ "\"") in
  if code <> 0 then error_message "The resource could not be opened"

(* custom inputs and dialogs *)

class type ['a] input =
object
  method get : 'a
end

let time_input ~packing () : Builtins_temporal.Time.t input =
  let hbox_time = GPack.hbox ~spacing:8 ~packing () in
  let label_time = GMisc.label ~text:"Time " ~packing:hbox_time#pack () in
  let tm = Unix.localtime (Unix.gettimeofday ()) in
  let spin_hour = GEdit.spin_button
      ~adjustment:(GData.adjustment ~page_size:0. ~upper:23. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_hour)
      ~packing:hbox_time#pack
      () in
  let _ = GMisc.label ~text:":" ~packing:hbox_time#pack () in
  let spin_min = GEdit.spin_button
      ~adjustment:(GData.adjustment ~page_size:0. ~upper:59. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_min)
      ~packing:hbox_time#pack
      () in
  let _ = GMisc.label ~text:":" ~packing:hbox_time#pack () in
  let spin_sec = GEdit.spin_button
      ~adjustment:(GData.adjustment ~page_size:0. ~upper:59. ())
      ~digits:0
      ~value:(float_of_int tm.Unix.tm_sec)
      ~packing:hbox_time#pack
      () in
object
  method get = Builtins_temporal.Time.make
    ~hours:spin_hour#value_as_int
    ~minutes:spin_min#value_as_int
    ~seconds:(float_of_int (spin_sec#value_as_int))
    ~timezone:Builtins_temporal.local_timezone
end

let date_input ~packing () : Builtins_temporal.Date.t input =
  let cal = GMisc.calendar
    ~options:[`SHOW_DAY_NAMES; `SHOW_HEADING; `SHOW_WEEK_NUMBERS]
    ~packing
    () in
object
  method get = Builtins_temporal.Date.make
    ~year:cal#year
    ~month:(1+cal#month)
    ~day:cal#day
end

let dateTime_input ~packing () : Builtins_temporal.DateTime.t input =
  let vbox = GPack.vbox ~spacing:8 ~packing () in
  let date_input = date_input ~packing:vbox#pack () in
  let time_input = time_input ~packing:vbox#pack () in
object
  method get = Builtins_temporal.DateTime.make date_input#get time_input#get
end

let duration_input ~packing () : Builtins_temporal.Duration.t input =
  let table = GPack.table ~columns:2 ~rows:7 ~col_spacings:4 ~packing () in
  let add_line row label =
    ignore (GMisc.label ~text:label ~xalign:0. ~packing:(table#attach ~left:1 ~top:row) ());
    GEdit.spin_button ~adjustment:(GData.adjustment ~page_size:0. ~value:0. ()) ~packing:(table#attach ~left:0 ~top:row) () in
  let check_negative = GButton.check_button ~label:"negative" ~active:false ~packing:(table#attach ~left:0 ~top:0) () in
  let spin_years = add_line 1 "years" in
  let spin_months = add_line 2 "months" in
  let spin_days = add_line 3 "days" in
  let spin_hours = add_line 4 "hours" in
  let spin_minutes = add_line 5 "minutes" in
  let spin_seconds = add_line 6 "seconds" in
object
  method get = Builtins_temporal.Duration.make
    ~negative:check_negative#active
    ~years:spin_years#value_as_int
    ~months:spin_months#value_as_int
    ~days:spin_days#value_as_int
    ~hours:spin_hours#value_as_int
    ~minutes:spin_minutes#value_as_int
    ~seconds:(float_of_int spin_seconds#value_as_int)
    ()
end

let period_input ~packing () : Builtins_temporal.Period.t input =
  let hbox = GPack.hbox ~spacing:12 ~packing () in
  let start_input = dateTime_input ~packing:hbox#pack () in
  let end_input = dateTime_input ~packing:hbox#pack () in
object
  method get = Builtins_temporal.Period.make start_input#get end_input#get
end

let period_duration_input ~packing () : Builtins_temporal.Period.t input =
  let hbox = GPack.hbox ~spacing:12 ~packing () in
  let start_input = dateTime_input ~packing:hbox#pack () in
  let dur_input = duration_input ~packing:hbox#pack () in
object
  method get =
    let a = start_input#get in
    let b = Builtins_temporal.DateTime.add a dur_input#get in
    Builtins_temporal.Period.make a b
end

let custom_dialog (make_input : packing:'pack -> unit -> 'a input) ~(title : string) ~(callback : 'a -> unit) =
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let input = make_input ~packing:vbox#add () in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:(fun () -> w#destroy (); callback input#get);
  w#show ()

let date_dialog = custom_dialog date_input
let dateTime_dialog = custom_dialog dateTime_input
let duration_dialog = custom_dialog duration_input
let period_dialog = custom_dialog period_input
let period_duration_dialog = custom_dialog period_duration_input


let syntax_error_logic msg =
  error_message (if msg="" then "Syntax error" else msg)

let fields_dialog ~title ~(fields: (string * 'a) list) ~(callback : 'b list -> unit) =
  (* takes a list of (label, field spec) and a callback on entered values *)
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~modal:true
      ~title
      () in
  let vbox = w#vbox in
  let widgets =
    List.map
      (fun (label, spec) ->
	match spec with
	| `String text ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `String (GEdit.entry ~text ~width_chars:60 ~packing:vbox#add ())
	| `Integer v ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `Integer (GEdit.spin_button ~adjustment:(GData.adjustment ~value:(float_of_int v) ()) ~packing:vbox#add ())
	| `Bool b ->
	    `Bool (GButton.check_button ~label ~active:b ~packing:vbox#add ())
	| `Enum (active,l) ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `Enum (l, GEdit.combo_box_text ~strings:(List.map fst l) ~active ~packing:vbox#add ())
	| `Time ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `Time (time_input ~packing:vbox#add ())
	| `Date ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `Date (date_input ~packing:vbox#add ())
	| `DateTime ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `DateTime (dateTime_input ~packing:vbox#add ())
	| `Duration ->
	    ignore (GMisc.label ~text:label ~xalign:0. ~packing:vbox#add ());
	    `Duration (duration_input ~packing:vbox#add ())
      )
      fields in
  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  let action () =
    try
      let res =
	List.map
	  (function
	    | `String entry -> `String entry#text
	    | `Integer spin_button -> `Integer spin_button#value_as_int
	    | `Bool check_button -> `Bool check_button#active
	    | `Enum (l, combo_box_text) -> `Enum (List.assoc (Option.find (GEdit.text_combo_get_active combo_box_text)) l)
	    | `Time time_input -> `Time time_input#get
	    | `Date date_input -> `Date date_input#get
	    | `DateTime dateTime_input -> `DateTime dateTime_input#get
	    | `Duration duration_input -> `Duration duration_input#get
	  )
	  widgets in
      callback res;
      w#destroy ()
    with
    | Stream.Error msg
    | Failure msg -> syntax_error_logic msg
    | e -> error_message (Printexc.to_string e) in
  List.iter (function
    | `String entry -> ignore (entry#connect#activate ~callback:action)
    | `Integer spin_button -> ignore (spin_button#connect#activate ~callback:action)
    | _ -> ())
    widgets;
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:action;
  w#show ()



let mint_uri store label uri =
  if uri = ""
  then store#base ^ label (* TODO: escape non-URI characters *)
  else Uri.resolve store#base store#xmlns uri

let var_dialog ~title ?(name = "") ~(callback : string -> unit) () =
  fields_dialog ~title
    ~fields:[("Name", `String name)]
    ~callback:(function
      | [`String name] -> callback name
      | _ -> assert false)

let uri_dialog store ~title ?(label = "") ?(uri = "") ~(callback : string -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label);
	     ("URI (optional)", `String uri);
	     ("Generate a draft description for that entity.", `Bool true)]
    ~callback:(function
      | [`String label; `String uri; `Bool draft] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  if draft then store#add_draft_for_uri uri;
	  callback uri
      | _ -> assert false)

let class_dialog store ~title ?(label = "") ?(uri = "") ~(callback : Uri.t -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label); ("URI (optional)", `String uri)]
    ~callback:(function
      | [`String label; `String uri] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  store#add_type (Rdf.URI uri) Rdfs.uri_Class;
	  callback uri
      | _ -> assert false)

let property_dialog store ~title ?(label = "") ?(inverse_label = "") ?(uri = "") ~(callback : Uri.t -> unit) () =
  fields_dialog ~title
    ~fields:[("Label", `String label);
	     ("Inverse label (optional)", `String inverse_label);
	     ("URI (optional)", `String uri)]
    ~callback:(function
      | [`String label; `String inverse_label; `String uri] ->
	  let uri = mint_uri store label uri in
	  if label <> "" then store#set_label uri label;
	  if inverse_label <> "" then store#set_inverseLabel uri inverse_label;
	  store#add_type (Rdf.URI uri) Rdf.uri_Property;
	  (* should add property properties: transitive, symmetric, ... *)
	  callback uri
      | _ -> assert false)

let functor_dialog store ~title ?(label = "") ?(uri = "")
    ?(arity = 2) ?(implicit = None) ?(focus = 0)
    ?(functor_type = Term.uri_Functor)
    ~(callback : Uri.t * int * int -> unit) () : unit =
  fields_dialog ~title
    ~fields:[("label", `String label);
	     ("URI (optional)", `String uri);
	     ("number of arguments (not used for verbs and operators)", `Integer arity);
	     ("focus on argument (0 for none)", `Integer focus);
	     ("functor type",
	      `Enum
		((if functor_type = Term.uri_Functor then 0
		else if functor_type = Term.uri_IntransitiveVerb then 1
		else if functor_type = Term.uri_TransitiveVerb then 2
		else if functor_type = Term.uri_InfixOperator then 3
		else if functor_type = Term.uri_PrefixOperator then 6
		else if functor_type = Term.uri_PostfixOperator then 7
		else 0),
		 [("functor", Term.uri_Functor);
		  ("intransitive verb", Term.uri_IntransitiveVerb);
		  ("transitive verb", Term.uri_TransitiveVerb);
		  ("infix operator", Term.uri_InfixOperator);
		  ("infix operator, left associative", Term.uri_LeftAssociativeInfixOperator);
		  ("infix operator, right associative", Term.uri_RightAssociativeInfixOperator);
		  ("prefix operator", Term.uri_PrefixOperator);
		  ("postfix operator", Term.uri_PostfixOperator);
		  ("mixfix operator", Term.uri_MixfixOperator)]));
	     ("precedence (only used for operators)",
	      `Enum (0,
		     [("maximal", Term.uri_maxPrecedence);
		      ("like comparators (=, <, >, ...)", Term.uri_equalPrecedence);
		      ("like addition (+, -)", Term.uri_plusPrecedence);
		      ("like multiplication (*, /)", Term.uri_timesPrecedence);
		      ("like exponentiation (^)", Term.uri_powerPrecedence);
		      ("like factorial (!)", Term.uri_factPrecedence)]))
	   ]
    ~callback:(function
      | [`String label; `String uri; `Integer arity; `Integer focus; `Enum functor_type; `Enum precedence] ->
	  let uri = mint_uri store label uri in
(*	  if label <> "" then store#set_label uri label; *)
	  let arity = store#define_functor
	      ~uri ?label:(if label = "" then None else Some label)
	      ~arity ?implicit
	      ~functor_type ~precedence in
	  callback (uri, arity, focus)
      | _ -> assert false)

let paned_set_ratio paned n d =
  paned#set_position (((d-n) * paned#min_position + n * paned#max_position) / d) (* position aux n/d *)

(* Application specific code *)

let filter_nt = "*.nt"
let filter_log = "*.log"
let filter_jpg = "*.jpg"

let weight_normal = Pango.Tags.weight_to_int `NORMAL
let weight_new = Pango.Tags.weight_to_int `BOLD

let str_white = "white"
let str_black = "black"
let str_lightgrey = "lightgrey"
let str_darkgrey = "darkgrey"
let str_darkred = "darkred"
let str_darkorange = "darkorange"
let str_darkyellow = "darkyellow"
let str_darkgreen = "darkgreen"
let str_darkcyan = "darkcyan"
let str_darkblue = "darkblue"
let str_darkviolet = "darkviolet"
let str_default_color = "lightgrey"


let str_color_of_rgb (r,g,b) = Printf.sprintf "#%02x%02x%02x" r g b

let str_color_query_root = str_color_of_rgb (200,200,255)
let str_color_query_type = str_color_of_rgb (255,200,150)
let str_color_query_val = str_color_of_rgb (200,255,200)
let str_color_update = str_color_of_rgb (255,200,200)

let str_color_of_place_mode = function
  | `Root -> str_color_query_root
  | `Type -> str_color_query_type
  | `Val -> str_color_query_val
  | `Relaxed -> str_color_update

let str_color_variable = str_darkred
let str_color_entity = str_darkblue
let str_color_literal = str_darkgreen
let str_color_class = str_darkorange
let str_color_property = str_darkviolet
let str_color_functor = str_darkcyan
let str_color_prim = "darkgoldenrod"
let str_color_datatype = str_darkorange


let expected_supp nbo nbq nbx = (nbx * nbq) / nbo

let scale_of_ratio n m =
  if n <= m
  then 0.8 +. (1.5 -. 0.8) *. log10 (float_of_int (2 + n)) /. log10 (float_of_int (2 + m))
  else 1.5 +. 0.2 *. log10 (float_of_int n /. float_of_int m)

let color_of_ratio n m = (* assuming 0 <= n <= m *)
  let max_intensity = 65535. in
  let min_intensity = 16000. in
  let rgb = int_of_float (
    if m = 0 then max_intensity
    else if n = 0 then min_intensity
    else
      min_intensity +.
	(max_intensity -. min_intensity) *.
	log10 (float_of_int (2 * n)) /. log10 (float_of_int (2 * m))) in
  str_color_of_rgb (rgb,rgb,rgb) (* Gdk.Color.alloc ~colormap (`RGB (rgb,rgb,rgb)) *)


class lift nbq nbx nbqx =
  object
    val supp : int = nbx
    method supp = supp

    val scale = scale_of_ratio nbqx nbx
    method scale = scale

    val color = color_of_ratio nbqx nbx
    method color = color
  end
let make_lift nbq nbx nbqx = new lift nbq nbx nbqx

class incr (increment : Lisql.Concept.increment) =
  let nbqx, nbq = increment#supp in
  object
    method feature = increment#feature
    method increment = increment
    method kind = increment#feature#kind
    method spec = increment#feature#spec
    method string = increment#feature#string
    method display = increment#feature#display
    method anew = increment#anew
    method supp = nbqx
    method ext = increment#ext
    method uri_opt = increment#feature#uri_opt
    method prop_uri_opt = increment#feature#prop_uri_opt
    method name_opt = increment#feature#name_opt
    method p1_opt = increment#feature#p1_opt
    method lisql_opt = increment#feature#lisql_opt
(*    method pagerank = increment#pagerank *)
(*    method children = (*increment#children*) *)
    
    val lift = make_lift nbq (*increment#card*) nbqx nbqx
    method lift = lift

    val scale = scale_of_ratio nbqx nbq
    method scale = scale

    val color = color_of_ratio nbqx nbq
    method color = color
  end
let make_incr i = new incr i

type links_sort_fun = Lisql.Concept.increment -> Lisql.Concept.increment -> int
let sort_by_count i i' =
  Common.compare_pair (Pervasives.compare, Pervasives.compare) (i'#supp, i#feature#spec) (i#supp, i'#feature#spec)
let sort_by_name i i' =
  Common.compare_pair (Pervasives.compare, Pervasives.compare) (i#feature#spec, i'#supp) (i'#feature#spec, i#supp)
let sort_by_feature i i' =
  Lisql.Feature.compare i#feature i'#feature

type cb_copy =
  | Nothing
  | Features of incr list
  | Objects of Ext.t

let cb_copy : cb_copy ref = ref Nothing (* clipboard of object ids for copy, paste, ... *)

let all_ext_colors, last_ext_color =
(*      let splits = [| 0; 16; 8; 24; 4; 12; 20; 28; 2; 6; 10; 14; 18; 22; 26; 30;
   1; 3; 5; 7; 9; 11; 13; 15; 17; 19; 21; 23; 25; 27; 29; 31 |] in *)
(*      let splits = [| 0; 8; 4; 12; 2; 6; 10; 14; 1; 3; 5; 7; 9; 11; 13; 15 |] in *)
  let splits = [| 0; 4; 2; 6; 1; 3; 5; 7|] in
  let nbsplits = Array.length splits in
  let base = 30000 in
  let levels = [| 20000; 40000|] in (* should not include base so as to avoid doublons *)
  let nblevels = Array.length levels in
  let chunk = (65535 - base) / nbsplits in
  let nbcolors = 3 * nblevels * nbsplits + 1 in
  let t = Array.make nbcolors str_default_color in
  for i = 0 to nbsplits - 1 do (* for all split between 2 colors *)
    let x1 = splits.(i) in
    let x2 = nbsplits - x1 in
    for l = 0 to nblevels - 1 do (* for all levels *)
      let x, y, z = base + x1*chunk, base + x2*chunk, levels.(l) in 
      for j = 0 to 2 do (* for every combination of 2 colors *)
	let rgb =
	  match j with
	  | 0 -> z, x, y (* green-blue *)
	  | 1 -> y, z, x (* blue-red *)
	  | _ -> x, y, z (* red-green *) in 
	t.(3*nblevels*i + 3*l + j) <- str_color_of_rgb rgb (* Gdk.Color.alloc ~colormap (`RGB rgb) *)
      done
    done
  done;
  t, nbcolors-1


type id_view = Lisql.Feature.spec

let default_links_page_size = 100

class state_view (v : id_view) =
  object (self)
    val view = v
    val mutable expanded : bool = false
    val mutable links_sort_fun : links_sort_fun = sort_by_feature
    val mutable links_page_start : int = 0 (* offset *)
    val mutable links_page_size : int = default_links_page_size (* limit, use max_int for no limit *)

    method view = view
    method expanded = expanded
    method links_sort_fun = links_sort_fun
    method links_page_start = links_page_start
    method links_page_size = links_page_size

    method set_expanded b = expanded <- b
    method set_links_sort_fun f = links_sort_fun <- f
    method set_links_page_start s = links_page_start <- s
    method set_links_page_size s = links_page_size <- s

    method copy (v : id_view) =
      {< view = v;
	 links_page_start = 0>}
  end

class state store (foc : Lisql.AST.focus) =
  object (self)
    val place_view : Lisql.place option Tarpit.incremental_view = new Tarpit.incremental_view "some place" None
    initializer
      place_view#define (fun obs -> function
	| None -> Some (store#place ~obs foc)
	| Some place -> Some (place#copy ~obs foc))
    method place =
      match place_view#contents ~obs:Tarpit.blind_observer with
      | Some place -> place
      | None -> assert false
    method unregister_to_all = place_view#unregister_to_all

    val mutable views : (id_view,state_view) Hashtbl.t = Hashtbl.create 13
    val mutable int_preselect : Ext.t option = None (* set of selected objects *)

    method views = views
    method int_preselect = int_preselect

    method new_view (v : id_view) = new state_view v

    method copy_view (sv : state_view) (v : id_view) = sv#copy v

    method get_view v =
      try Hashtbl.find views v
      with Not_found ->
	let sv = new state_view v in
	Hashtbl.add views v sv;
	sv

    method replace_view : id_view -> id_view -> unit =
      fun v v' ->
	try
	  Hashtbl.replace views v' (Hashtbl.find views v);
	  Hashtbl.remove views v
	with _ -> ()

    method fold_views : 'a.(id_view -> state_view -> 'a -> 'a) -> 'a -> 'a = fun f e -> Hashtbl.fold f views e

    method set_int_preselect oids = int_preselect <- oids

    method copy foc =
      {< place_view =
	 begin
	   let view = new Tarpit.incremental_view "some place" (Some self#place) in
	   view#notified_by Tarpit.void_subject;
	   view#define (fun obs -> function
	     | None -> assert false
	     | Some place -> Some (place#copy ~obs foc));
	   view
	 end;
	 int_preselect = None;
	 views =
         let ht = Hashtbl.create (Hashtbl.length views) in
	 Hashtbl.iter (fun v sv -> Hashtbl.add ht v (sv # copy v)) views;
	 ht;>}

    initializer
      List.iter
	(fun v ->
	  Hashtbl.add views v (new state_view v))
	[Lisql.Feature.Spec_Thing; Lisql.Feature.Spec_Something]
  end

let get_store filepath =
  prerr_endline "Gui: building the store...";
  try Lisql.load_store filepath
  with _ ->
    if question "The database is missing or deprecated. Do you want to load from the logfile ?"
    then
      let base =
	let dirname = Filename.dirname filepath in
	let dirname =
	  if dirname = Filename.current_dir_name then Unix.getcwd ()
	  else if dirname = Filename.parent_dir_name then Filename.dirname (Unix.getcwd ())
	  else dirname in
	Name.uri_of_file (Filename.concat dirname "") in
      Lisql.create_store ~base filepath
    else failwith "The logfile could not be read."

class history filepath0 =
  object (self)
    val mutable filepath = filepath0
    val mutable store : Lisql.store = get_store filepath0

    val st_backward : state Stack.t = Stack.create ()
    method private clear_backward =
      Stack.iter (fun state -> state#unregister_to_all) st_backward;
      Stack.clear st_backward

    val st_forward : state Stack.t = Stack.create ()
    method private clear_forward =
      Stack.iter (fun state -> state#unregister_to_all) st_forward;
      Stack.clear st_forward

    method filepath = filepath

    method store = store

    method root : Lisql.AST.focus = Lisql.AST.focus_top

    method home : Lisql.AST.focus =
      match store#get_home_focus ~obs:Tarpit.blind_observer with
      | Some foc -> foc
      | None -> Lisql.AST.focus_top

    method current = Stack.top st_backward

    method init_store filepath0 =
      try
	filepath <- filepath0;
	store <- get_store filepath0;
	self#init
      with exn ->
	error_message ("The store could not be loaded: " ^ Printexc.to_string exn)
	      
    method save_store =
      try
	store#save
      with exn ->
	error_message ("The store could not be saved :" ^ Printexc.to_string exn)


    method init =
      store#add_prefix Logui.prefix Logui.namespace;
      self#clear_backward;
      self#clear_forward;
      Stack.push (new state store self#home) st_backward

    method cd foc_q =
      self#clear_forward;
      self # current # set_int_preselect None;
      let st = self # current # copy foc_q in
      Stack.push st st_backward

    method reset foc_q =
      self#clear_forward;
      self # current # set_int_preselect None;
      Stack.push (new state store foc_q) st_backward

    method backward =
      if Stack.length st_backward > 1
      then begin
	let s = Stack.pop st_backward in
	s # set_int_preselect None;
	Stack.push s st_forward
      end

    method forward =
      if Stack.length st_forward > 0
      then begin
	self # current # set_int_preselect None;
	let s = Stack.pop st_forward in
	Stack.push s st_backward
      end

  end

let history =
  prerr_endline "Gui: creating and initializing the history...";
  let h = new history !log_file in
  h#init;
  h

(* ---------------------- *)


let display_ctx_markup (buf : Buffer.t) =
  object (self)
    inherit Lisql_display.markup_ctx
    method private string s = Buffer.add_string buf s
    method private span la s =
      self#string "<span";
      List.iter (fun (a,v) -> self#string " "; self#string a; self#string "=\""; self#string v; self#string "\"") la;
      self#string ">";
      self#string s;
      self#string "</span>"
    method kwd s =
      self#string (Glib.Markup.escape_text s)
    method var v =
      self#span [("foreground",str_color_variable)] (Glib.Markup.escape_text ("?" ^ v))
    method uri u k s iopt =
      let color =
	match k with
	| `Entity -> str_color_entity
	| `Class -> str_color_class
	| `Property -> str_color_property
	| `Functor -> str_color_functor
	| `Datatype -> str_color_datatype in
      self#span [("foreground",color)] (Glib.Markup.escape_text s)
    method prim op =
      self#span [("foreground",str_color_prim)] (Glib.Markup.escape_text op)
    method plain s lang =
      self#span [("foreground",str_color_literal)] (Glib.Markup.escape_text s);
      if lang <> "" then self#string (" (" ^ Glib.Markup.escape_text lang ^ ")")
    method typed s dt sdt =
      self#span [("foreground",str_color_literal)] (Glib.Markup.escape_text s);
      if sdt <> "" then begin
	self#string " (";
	self#span [("foreground",str_color_datatype)] (Glib.Markup.escape_text sdt);
	self#string ")"
      end
    method xml x =
      self#span [("foreground",str_color_literal)] (Xml.to_string x)	
  end

let make_markup (d : Lisql.Display.t) : string (* markup *) =
  let buf = Buffer.create 200 in
  Lisql.Display.markup false (display_ctx_markup buf) d;
  Buffer.contents buf

let display_ctx_csv_cell (buf : Buffer.t) =
  object (self)
    inherit Lisql_display.markup_ctx
    method private string s = Buffer.add_string buf s
    method private span la s =
      self#string "<span";
      List.iter (fun (a,v) -> self#string " "; self#string a; self#string "=\""; self#string v; self#string "\"") la;
      self#string ">";
      self#string s;
      self#string "</span>"
    method kwd s = self#string s
    method var v = self#string ("?" ^ v)
    method uri u k s iopt = self#string s
    method prim op = self#string op
    method plain s lang = self#string s
    method typed s dt sdt = self#string s
    method xml x = self#string (Xml.to_string x)
  end

let escape_csv_cell =
  let regexp_doublequote = Str.regexp_string "\"" in
  fun s -> "\"" ^ Str.global_replace regexp_doublequote "\"\"" s ^ "\""
let make_csv_cell (d : Lisql.Display.t) : string =
  let buf = Buffer.create 200 in
  Lisql.Display.markup false (display_ctx_csv_cell buf) d;
  escape_csv_cell (Buffer.contents buf) 


let display_ctx_query (buffer : GText.buffer) h_foc_offsets focus_mark view focus_box =
  let tag_variable = buffer # create_tag [`FOREGROUND str_color_variable] in
  let tag_class = buffer # create_tag [`FOREGROUND str_color_class] in
  let tag_literal = buffer # create_tag [`FOREGROUND str_color_literal] in
  let tag_entity = buffer # create_tag [`FOREGROUND str_color_entity] in
  let tag_property = buffer # create_tag [`FOREGROUND str_color_property] in
  let tag_functor = buffer # create_tag [`FOREGROUND str_color_functor] in
  let tag_prim = buffer # create_tag [`FOREGROUND str_color_prim] in
  let tag_datatype = buffer # create_tag [`FOREGROUND str_color_datatype] in
(*
  let tag_query = buffer # create_tag [`BACKGROUND str_color_query] in
  let tag_update = buffer # create_tag [`BACKGROUND str_color_update] in
*)
  object (self)
    inherit Lisql_display.markup_ctx

    method private string s = buffer#insert s
    method kwd s =
      buffer#insert s
    method var v =
      buffer#insert ~tags:[tag_variable] ("?" ^ v)
    method uri u k s iopt =
      let tag =
	match k with
	| `Entity -> tag_entity
	| `Class -> tag_class
	| `Property -> tag_property
	| `Functor -> tag_functor
	| `Datatype -> tag_datatype in
      buffer#insert ~tags:[tag] s;
      Option.iter
	(fun uri_img -> buffer#insert_pixbuf ~iter:buffer#end_iter ~pixbuf:(pixbuf_of_picture ~maxside:100 uri_img))
	iopt
    method prim op =
      buffer#insert ~tags:[tag_prim] op
    method plain s lang =
      buffer#insert ~tags:[tag_literal] s;
      if lang <> "" then buffer#insert (" (" ^ lang ^ ")")
    method typed s dt sdt =
      buffer#insert ~tags:[tag_literal] s;
      if sdt <> "" then begin
	buffer#insert " (";
	buffer#insert ~tags:[tag_datatype] sdt;
	buffer#insert ")"
      end
    method xml x =
      buffer#insert ~tags:[tag_literal] (Xml.to_string x)

    method private focus foc =
      focus_mark := Some (buffer#create_mark buffer#end_iter);
      let focus_anchor = buffer#create_child_anchor buffer#end_iter in
      view#add_child_at_anchor focus_box#coerce focus_anchor
    method space_focus foc =
      if Lisql.AST.same_focus foc history#current#place#focus
      then begin self#space; self#focus foc end
    method newline_focus foc =
      if Lisql.AST.same_focus foc history#current#place#focus
      then begin self#newline; self#focus foc end
    method open_focus foc =
      let offset = buffer#end_iter#offset in
      Hashtbl.add h_foc_offsets foc (offset,offset)
    method close_focus foc =
      try
	let start_offset, _ = Hashtbl.find h_foc_offsets foc in
	let end_offset = buffer#end_iter#offset in
	Hashtbl.replace h_foc_offsets foc (start_offset, end_offset);
	if Lisql.AST.same_focus foc history#current#place#focus
	then begin
	  let tag = buffer # create_tag [`BACKGROUND (str_color_of_place_mode history#current#place#mode)] in
(*
	  let tag =
	    if history#current#place#rank = 0 && history#current#place#nb_answers <> 0
	    then tag_query
	    else tag_update in
*)
	  buffer#apply_tag tag ~start:(buffer#get_iter (`OFFSET start_offset)) ~stop:buffer#end_iter
	end
      with _ -> assert false
  end


let picture_of_name ~obs name =
  history#store#get_image ~obs name

(* deprecated *)
(*
let rec ls ~obs sv (incr : incr) : incr list = Common.prof "ls" (fun () ->
  let proprs = history#current#place#children_increments incr#increment in
  let incrs : incr list =
    List.rev_map ~f:make_incr proprs in
  let incrs1 : incr list =
    if incr#spec = Lisql.Feature.Spec_Something || sv#suppmin_opt <> None
    then incrs
    else
      let incrs =
	List.sort
	  ~cmp:(fun i1 i2 -> Pervasives.compare (i2#supp,i2#lift#supp) (i1#supp,i1#lift#supp))
	  incrs in
      let _, _, incrs1, _ =
	Common.fold_while
	  (fun (incrs,n1,incrs1,supp_card) ->
	    match incrs with
	    | [] -> None
	    | i::incrs' ->
		let supp_card' = (i#supp,i#lift#supp) in
		if n1 < sv#links_page_size || supp_card' >= supp_card
		then Some (incrs', n1+1, i::incrs1, supp_card')
		else None)
	  (incrs,0,[],(max_int,max_int)) in
      incrs1 in
  let incrs2 = List.sort ~cmp:sv#links_sort_fun incrs1 in
  incrs2)

let ls_opt ~obs force sv incr =
  if force || sv#links_opt = None
  then begin
    let links = ls ~obs sv incr in
    sv#set_links_opt (Some links);
    links end
  else match sv#links_opt with
  | Some links -> links
  | None -> assert false
*)

(* building the interface *)

let _ = prerr_endline "Gui: creating the widgets..."

let accel_group = GtkData.AccelGroup.create ()

let tooltips = GData.tooltips ()

let title () =
  let name = history#store#log#filename in
  let readonly = false (* Context.readonly () *) in
  "Sewelis" ^
  (if name="" then "" else " - " ^ Filename.basename name) ^
  (if name <> "" && readonly then " (read only)" else "")

let window = GWindow.window ~title:(title ()) ()

let vbox = GPack.vbox ~border_width:8 ~spacing:8 ~packing:window#add ()

let menubar = GMenu.menu_bar ~packing:vbox#pack ()
let menubar_factory = new GMenu.factory menubar

let file_menu = menubar_factory#add_submenu "File"
let file_menu_factory = new GMenu.factory file_menu
let cmd_new = file_menu_factory#add_item "New..."
let cmd_open = file_menu_factory#add_item "Open..."
let cmd_save = file_menu_factory#add_item ~key:_s "Save"
(*
   let cmd_saveas = file_menu_factory#add_item "Save as..."
   let cmd_close = file_menu_factory#add_item "Close"
 *)
let _ = file_menu_factory#add_separator ()
let import_file_menu = file_menu_factory#add_submenu "Import"
let import_file_factory = new GMenu.factory import_file_menu
let cmd_import_from_rdf = import_file_factory#add_item "Import RDF..."
(*let cmd_import_from_xml = import_file_factory#add_item "Import XML..."*)
let cmd_import_from_uri = import_file_factory#add_item "Import URI..."
let cmd_import_from_path = import_file_factory#add_item "Import from path..."
let export_file_menu = file_menu_factory#add_submenu "Export"
let export_file_factory = new GMenu.factory export_file_menu
let cmd_export_to_rdf = export_file_factory#add_item "Export RDF..."
let _ = file_menu_factory#add_separator ()
let cmd_stat = file_menu_factory#add_item "Statistics"
let _ = file_menu_factory#add_separator ()
let cmd_quit = file_menu_factory#add_item "Quit"

let view_menu = menubar_factory#add_submenu "Browsing"
let view_menu_factory = new GMenu.factory view_menu
let cmd_back = view_menu_factory#add_item ~key:_b "Back"
let cmd_fwd = view_menu_factory#add_item ~key:_f "Forward"
let cmd_refresh = view_menu_factory#add_item ~key:_r "Refresh"
let cmd_root = view_menu_factory#add_item ~key:_0 "Root"
let cmd_home = view_menu_factory#add_item "Home"
let cmd_bookmarks = view_menu_factory#add_item "Bookmarks"
let cmd_drafts = view_menu_factory#add_item "Drafts"
let _ = view_menu_factory#add_separator ()
let cmd_give_more = view_menu_factory#add_item ~key:_plus "Show MORE similar objects"
let cmd_give_less = view_menu_factory#add_item ~key:_minus "Show LESS similar objects"
let _ = view_menu_factory#add_separator ()
let check_scale = view_menu_factory#add_check_item ~active:true "Variable font size"
let check_feat_color = view_menu_factory#add_check_item ~active:true "Foreground color"
let check_lift = view_menu_factory#add_check_item ~active:false "Total count"
let check_lift_color = view_menu_factory#add_check_item ~active:true "Total count grey level"
let check_ext_color = view_menu_factory#add_check_item ~active:false "Equal extent colors"
let check_int_preselect = view_menu_factory#add_check_item ~active:false "Features preselection"
let check_lazy_completion = view_menu_factory#add_check_item ~active:false "Lazy completion (triggered by spaces)"
let _ = view_menu_factory#add_separator ()
let cmd_expand = view_menu_factory#add_item "Expand..."
let cmd_collapse = view_menu_factory#add_item "Collapse"

let objects_menu = menubar_factory#add_submenu "Updating"
let objects_menu_factory = new GMenu.factory objects_menu
let cmd_run = objects_menu_factory#add_item ~key:_x "Assert/Run"
let _ = objects_menu_factory#add_separator ()
let cmd_set_home = objects_menu_factory#add_item "Define as the home query"
let cmd_add_bookmark = objects_menu_factory#add_item "Add to bookmarks"
let cmd_add_draft = objects_menu_factory#add_item "Add to drafts"

let help_menu = menubar_factory#add_submenu "Help"
let help_menu_factory = new GMenu.factory help_menu
let cmd_about = help_menu_factory#add_item "About Sewelis..."

let hbox_buttons = GPack.hbox ~spacing:8 ~height:30 ~packing:vbox#pack ()
let button_back = GButton.button ~stock:(`STOCK " Back ") ~packing:(hbox_buttons#pack) ()
let button_fwd = GButton.button ~stock:(`STOCK " Forward ") ~packing:(hbox_buttons#pack) ()
let button_refresh = GButton.button ~stock:(`STOCK " Refresh ") ~packing:(hbox_buttons#pack) ()
let button_root = GButton.button ~stock:(`STOCK " Root ") ~packing:(hbox_buttons#pack) ()
let button_home = GButton.button ~stock:(`STOCK " Home ") ~packing:(hbox_buttons#pack) ()
let button_bookmarks = GButton.button ~stock:(`STOCK " Bookmarks ") ~packing:(hbox_buttons#pack) ()
let button_drafts = GButton.button ~stock:(`STOCK " Drafts ") ~packing:(hbox_buttons#pack) ()
let _ = GMisc.separator `VERTICAL ~packing:(hbox_buttons#pack) ()
let button_run = GButton.button ~stock:(`STOCK " Assert/Run ") ~packing:(hbox_buttons#pack) ()

type navigation_mode = Name | Default | Zoom | Pivot | Cross

class default_navig_callbacks =
  object
    method insert : Turtle.uri -> Lisql.Feature.feature -> unit = fun _ _ -> ()
    method name : unit -> unit = fun _ -> ()
    method transf : Lisql.Transf.kind -> unit = fun _ -> ()
  end

class text_query_callbacks =
  object
    inherit default_navig_callbacks
    method ctx_menu : string -> GMenu.menu -> unit = fun _ _ -> ()
    method focus : Lisql.AST.focus -> unit = fun _ -> ()
  end

class text_query ~width_chars ~pack ~focus_widget =
  (* the query buffer *)
  let tag_table = GText.tag_table () in
  let buffer = GText.buffer ~tag_table:tag_table () in
  let tag_scale_up = buffer # create_tag [`SCALE (`CUSTOM 1.5)] in
(*      let tag_foreground_red = buffer # create_tag [`FOREGROUND str_darkred] in *)
  let tag_variable = buffer # create_tag [`FOREGROUND str_color_variable] in
  let tag_class = buffer # create_tag [`FOREGROUND str_color_class] in
  let tag_literal = buffer # create_tag [`FOREGROUND str_color_literal] in
  let tag_entity = buffer # create_tag [`FOREGROUND str_color_entity] in
  let tag_property = buffer # create_tag [`FOREGROUND str_color_property] in
  let tag_functor = buffer # create_tag [`FOREGROUND str_color_functor] in
  (* graphical display *)
(*  let vbox = GPack.vbox ~spacing:8 ~packing:pack () in *)
  let sw = GBin.scrolled_window ~shadow_type:`ETCHED_IN
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC ~packing:pack (*fun w -> pack ~expand:true ~fill:true w*) () in
  let view = GText.view ~buffer:buffer
      ~editable:false
      ~cursor_visible:true
      ~wrap_mode:`NONE (* `WORD *)
      ~width:width_chars
      ~height:2
      ~packing:(*fun w -> hbox#pack ~expand:true ~fill:true w*)  sw#add () in
  object (self)
(*    val mutable clipboard = Lisql.AST.top_p1 *) (*TODO*)
    val h_foc_offsets = Hashtbl.create 7

    val mutable callbacks = new text_query_callbacks

    method connect c = callbacks <- c

    val mutable focus_on_extent = false

    method set_focus_on_extent b =
      focus_on_extent <- b

    method button_press ev =
      let button = GdkEvent.Button.button ev in
      if (button = 1 || button = 2) then
	try
	  let win =
	    match view#get_window `WIDGET with
	    | None -> assert false
	    | Some w -> w in
	  let x,y = Gdk.Window.get_pointer_location win in
	  let b_x,b_y = view#window_to_buffer_coords ~tag:`WIDGET ~x ~y in
	  let iter = view#get_iter_at_location ~x:b_x ~y:b_y in
	  let offset = iter#offset in
	  let foc_opt, _ =
	    Hashtbl.fold
	      (fun fc (s,e) (foc_opt,delta_opt) ->
		let d = e - s in
		if s <= offset && offset < e && Option.fold (fun delta -> d < delta) true delta_opt
		then (Some fc, Some d)
		else (foc_opt,delta_opt))
	      h_foc_offsets (None,None) in
	  Option.iter (fun foc -> callbacks#focus foc) foc_opt;
	  false
	with _ -> false
      else if button = 3 then begin
	let assertion = buffer # get_text () in
	let menu = GMenu.menu () in
	callbacks#ctx_menu assertion menu;
	menu#popup ~button:button ~time:(GdkEvent.Button.time ev);
	true end
      else false

    val mutable focus_box = GPack.hbox ()
    initializer focus_box#add focus_widget#coerce

    method refresh = Common.prof "Gui.text_query#refresh" (fun () ->
      let focus_mark = ref None in
      Tarpit.effect (fun obs ->
	Hashtbl.clear h_foc_offsets;
	focus_box#remove focus_widget#coerce;
	focus_box <- GPack.hbox ();
	focus_box#add focus_widget#coerce;
	buffer # set_text "";
	let a = history#current#place#assertion in
	Lisql.Display.markup
	  true
	  (display_ctx_query buffer h_foc_offsets focus_mark view focus_box)
	  (Lisql.Display.of_c ~obs history#store a);
	buffer # apply_tag tag_scale_up ~start:buffer#start_iter ~stop:buffer#end_iter;
	Option.iter (fun mark -> view#scroll_to_mark (`MARK mark)) !focus_mark;
	focus_widget#grab_focus ()))

    initializer
      (* callbacks *)
      ignore (view#connect#delete_from_cursor ~callback:(fun _ _ -> callbacks#transf Lisql.Transf.Delete));
      view # event # connect # button_press ~callback:self#button_press;
      ()
  end

class more_less_callbacks =
  object
    method give_least = ()
    method give_less = ()
    method give_more = ()
    method give_most = ()
  end

class more_less ~pack () =
  let box = GPack.hbox ~spacing:8 ~packing:pack () in
  let _ = GMisc.label ~text:"Suggested features for" ~packing:box#pack () in
  let label_dist = GMisc.label ~text:"0" ~packing:box#pack () in
  let _ = GMisc.label ~text: "solutions" ~packing:box#pack () in
  let button_least = GButton.button ~label:"Least" (*~relief:`NONE*) ~packing:box#pack () in
  let button_less = GButton.button ~label:"Less" (*~relief:`NONE*) ~packing:box#pack () in
  let button_more = GButton.button ~label:"More" (*~relief:`NONE*) ~packing:box#pack () in
  let button_most = GButton.button ~label:"Most" (*~relief:`NONE*) ~packing:box#pack () in
  object (self)
    val mutable callbacks = new more_less_callbacks

    method connect c = callbacks <- c

    method refresh =
      let place = history#current#place in
      label_dist#set_text (string_of_int place#nb_answers);
      button_least#misc#set_sensitive place#has_less;
      button_less#misc#set_sensitive place#has_less;
      button_more#misc#set_sensitive place#has_more;
      button_most#misc#set_sensitive place#has_more

    initializer
      button_least#connect#clicked ~callback:(fun () -> callbacks#give_least);
      button_less#connect#clicked ~callback:(fun () -> callbacks#give_less);
      button_more#connect#clicked ~callback:(fun () -> callbacks#give_more);
      button_most#connect#clicked ~callback:(fun () -> callbacks#give_most);
      tooltips#set_tip button_less#coerce
	~text:"Give less objetcs and suggestions";
      tooltips#set_tip label_dist#coerce
	~text:"Number of objects in the current extent";
      tooltips#set_tip button_more#coerce
	~text:"Give more objects and suggestions";
  end

class entry_feature_callbacks =
  object
    inherit default_navig_callbacks
    method input : mode:navigation_mode -> custom:string -> bool = fun ~mode ~custom -> true
  end

class entry_feature ?(full = true) ?pack () =
  let box = GPack.hbox ~spacing:8 ?packing:pack () in
  let opt pack = if full then Some pack else None in
  (* menus *)
  let menubar = GMenu.menu_bar ~packing:box#pack () in
  let menubar_factory = new GMenu.factory menubar in
  (* transformations *)
  let menu_apply = menubar_factory#add_submenu "Apply" in
  let menu_apply_factory = new GMenu.factory menu_apply in
  let item_seq = menu_apply_factory#add_item "__ ; __" in
  let item_cond = menu_apply_factory#add_item "if __ then __ else __" in
  let item_for_each = menu_apply_factory#add_item "for each __, __" in
  let item_and = menu_apply_factory#add_item "__ and __" in
  let item_or = menu_apply_factory#add_item "__ or __" in
  let item_and_not = menu_apply_factory#add_item "__ and not __" in
  let item_and_maybe = menu_apply_factory#add_item "__ and maybe __" in
  let item_not = menu_apply_factory#add_item "not __" in
  let item_maybe = menu_apply_factory#add_item "maybe __" in
  let item_is_there = menu_apply_factory#add_item "__ is __" in
  let _ = menu_apply_factory#add_separator () in
  let item_an = menu_apply_factory#add_item "a __" in
  let item_each = menu_apply_factory#add_item "each __" in
  let item_every = menu_apply_factory#add_item "every __" in
  let item_only = menu_apply_factory#add_item "only the __" in
  let item_no = menu_apply_factory#add_item "no __" in
  let item_opt = menu_apply_factory#add_item "optionally __" in
  let item_trans = menu_apply_factory#add_item "transitively/directly __" in
  let item_sym = menu_apply_factory#add_item "symmetrically __" in
  let _ = menu_apply_factory#add_separator () in
  let item_name = menu_apply_factory#add_item "Name" in
  let item_describe = menu_apply_factory#add_item "Describe" in
  let item_select = menu_apply_factory#add_item "Select" in
  let item_delete = menu_apply_factory#add_item "Delete" in
  (* creating and inserting new resources *)
  let menu_create = menubar_factory#add_submenu "Create" in
  let menu_create_factory = new GMenu.factory menu_create in
  let item_var = menu_create_factory#add_item "a variable" in
  let item_uri = menu_create_factory#add_item "an entity" in
  let item_class = menu_create_factory#add_item "a class (a ...)" in
  let item_prop = menu_create_factory#add_item "a property (... ?)" in
  let item_prop_inv = menu_create_factory#add_item "an inverse property (is ... of ?)" in
  let item_struct = menu_create_factory#add_item "a structure (...(...))" in
  let _ = menu_create_factory#add_separator () in
  let item_plain = menu_create_factory#add_item "a text" in
  let item_date = menu_create_factory#add_item "a date" in
  let item_dateTime = menu_create_factory#add_item "a date and time" in
  let item_duration = menu_create_factory#add_item "a duration" in
  let item_period = menu_create_factory#add_item "a period (start and end)" in
  let item_period_duration = menu_create_factory#add_item "a period (start and duration)" in
  let item_file = menu_create_factory#add_item "a filename (file:///...)" in
  (* custom entry and completions *)
(*  let _ = GMisc.label ~text:"Search" ?packing:(opt (fun w -> box#pack w)) () in *)
  let entry_custom = GEdit.entry ~width_chars:20 ~packing:(box#pack ~expand:true) () in
  let cols_completion = new GTree.column_list in
  let markup = cols_completion#add string in
  let feature = cols_completion#add string in
  let bgcolor = cols_completion#add string in
  let model_completion = GTree.list_store cols_completion in
  let entry_custom_completion = GEdit.entry_completion ~model:model_completion ~minimum_key_length:1 ~entry:entry_custom () in
  (* button toolbar *)
  let toolbar = GButton.toolbar ~orientation:`HORIZONTAL (*~style:`ICONS*) ~width:50 ~packing:box#pack ~show:(not full) () in
  let button_delete = GButton.tool_button (*~label:"Delete"*) ~stock:`DELETE (* `CANCEL *) (*~expand:true*)
      ~packing:(fun item -> toolbar#insert item) () in
  object (self)
    val mutable callbacks = new entry_feature_callbacks
    val mutable feat_custom = ""
    val mutable relative_rank = 0

    method coerce = box#coerce
    method entry = entry_custom

    method connect c = callbacks <- c

    method grab_focus () = entry_custom#misc#grab_focus ()

    method refresh = Common.prof "Gui.entry_features#refresh" (fun () ->
      relative_rank <- 0;
      let place = history#current#place in
      let ltransf = place#transformations in
      let insert_s1 = place#insert_s1 in
      let insert_p1 = place#insert_p1 in
      let refresh_widget w b = if b then w#misc#show () else w#misc#hide () in
      refresh_widget item_var insert_s1;
      refresh_widget item_uri insert_s1;
      refresh_widget item_class insert_p1;
      refresh_widget item_prop insert_p1;
      refresh_widget item_prop_inv insert_p1;
      refresh_widget item_struct insert_p1;      
      refresh_widget item_plain insert_s1;
      refresh_widget item_date insert_s1;
      refresh_widget item_dateTime insert_s1;
      refresh_widget item_duration insert_s1;
      refresh_widget item_period insert_s1;
      refresh_widget item_period_duration insert_s1;
      refresh_widget item_file insert_s1;
      let refresh_widget_transf w t = refresh_widget w (List.mem t ltransf) in
      let open Lisql.Transf in
      refresh_widget_transf item_seq InsertSeq;
      refresh_widget_transf item_cond InsertCond;
      refresh_widget_transf item_for_each InsertForEach;
      refresh_widget_transf item_and InsertAnd;
      refresh_widget_transf item_or InsertOr;
      refresh_widget_transf item_and_not InsertAndNot;
      refresh_widget_transf item_and_maybe InsertAndMaybe;
      refresh_widget_transf item_not ToggleNot;
      refresh_widget_transf item_maybe ToggleMaybe;
      refresh_widget_transf item_is_there InsertIsThere;
      refresh_widget_transf item_an (ToggleQu Lisql.AST.An);
      refresh_widget_transf item_each (ToggleQu Lisql.AST.Each);
      refresh_widget_transf item_every (ToggleQu Lisql.AST.Every);
      refresh_widget_transf item_only (ToggleQu Lisql.AST.Only);
      refresh_widget_transf item_no (ToggleQu Lisql.AST.No);
      refresh_widget_transf item_opt ToggleOpt;
      refresh_widget_transf item_trans ToggleTrans;
      refresh_widget_transf item_sym ToggleSym;
      refresh_widget item_name insert_s1;
      refresh_widget_transf item_describe Describe;
      refresh_widget_transf item_select Select;
      refresh_widget_transf item_delete Delete;
      if not full then refresh_widget_transf button_delete Delete)
      
    method input mode () =
      let custom =
	if feat_custom = ""
	then entry_custom#text
	else feat_custom in
      if callbacks#input ~mode ~custom
      then entry_custom#set_text ""

    val h_features : (string, Lisql.Feature.feature) Hashtbl.t = Hashtbl.create 101

    method private clear_model_completion =
      Hashtbl.clear h_features;
      model_completion#clear ()

    method private set_model_completion =
      Tarpit.effect (fun obs -> Common.prof "Gui.entry_feature#set_model_completion" (fun () ->
	let key = entry_custom#text in
	let partial, relaxed, sorted_increments = history#current#place#completions ~nb_more_relax:relative_rank key in
	feat_custom <- "";
	entry_custom_completion#misc#freeze_notify ();
	Hashtbl.clear h_features;
	model_completion#clear ();
	List.iter
	  (fun incr ->
	    let f = incr#feature in
	    let (a,b) = incr#supp in
	    let s = f#string in
	    let d = f#display ~obs in
	    let d = `Kwd ("(" ^ string_of_int a ^ ")") :: `Space :: d in
	    Hashtbl.add h_features s f;
	    let iter = model_completion#append () in
	    model_completion#set ~row:iter ~column:markup (make_markup d);
	    model_completion#set ~row:iter ~column:feature s;
	    model_completion#set ~row:iter ~column:bgcolor (if relaxed then str_color_update else str_color_query_val))
	  sorted_increments;
	if partial then begin
	  let iter = model_completion#append () in
	  model_completion#set ~row:iter ~column:markup "..."
	end;
	entry_custom_completion#misc#thaw_notify ()))

    initializer
      (* creation of features *)
      item_plain#connect#activate ~callback:
	(fun () ->
	  match GToolbox.input_text ~title:"Enter a text" "" with
	  | None -> ()
	  | Some text ->
	      let x = new Lisql.Feature.feature_name history#store (Name.plain_literal text "en") in
	      callbacks#insert Logui.uri_TextCreation x);
      item_date#connect#activate ~callback:
	(fun () ->
	  date_dialog
	    ~title:"Select a date"
	    ~callback:
	    (fun date ->
	      let x = new Lisql.Feature.feature_name history#store (Builtins_temporal.Date.to_name date) in
	      callbacks#insert Logui.uri_DateCreation x));
      item_dateTime#connect#activate ~callback:
	(fun () ->
	  dateTime_dialog
	    ~title:"Select a date and time"
	    ~callback:
	    (fun dateTime ->
	      let x = new Lisql.Feature.feature_name history#store (Builtins_temporal.DateTime.to_name dateTime) in
	      callbacks#insert Logui.uri_DateTimeCreation x));
      item_duration#connect#activate ~callback:
	(fun () ->
	  duration_dialog
	    ~title:"Select a duration"
	    ~callback:
	    (fun duration ->
	      let x = new Lisql.Feature.feature_name history#store (Builtins_temporal.Duration.to_name duration) in
	      callbacks#insert Logui.uri_DurationCreation x));
      item_period#connect#activate ~callback:
	(fun () ->
	  period_dialog
	    ~title:"Select a period by start and end"
	    ~callback:
	    (fun period ->
	      let x = new Lisql.Feature.feature_name history#store (Builtins_temporal.Period.to_name period) in
	      callbacks#insert Logui.uri_PeriodCreation x));
      item_period_duration#connect#activate ~callback:
	(fun () ->
	  period_duration_dialog
	    ~title:"Select a period by start and duration"
	    ~callback:
	    (fun period ->
	      let x = new Lisql.Feature.feature_name history#store (Builtins_temporal.Period.to_name period) in
	      callbacks#insert Logui.uri_PeriodCreation x));
      item_file#connect#activate ~callback:
	(fun () ->
	  let default =
	    Filename.concat
	      history#store#log#dirname
	      (Name.string_dateTime_of_UnixTime (Unix.gettimeofday ())) in
	  file_dialog
	    ~title:"Select a file or a directory"
	    ~filename:default
	    ~callback:
	    (fun filename ->
	      if not (Sys.file_exists filename)
		  && question "Are you sure you want to create this file ?" then
		create_file filename;
	      let x = new Lisql.Feature.feature_name history#store (Rdf.URI (Name.uri_of_file filename)) in
	      callbacks#insert Logui.uri_FilenameCreation x)
	    ());
      item_var#connect#activate ~callback:(fun () ->
	var_dialog
	  ~title:"Creation of a variable"
	  ~name:"X"
	  ~callback:(fun name ->
	    let x = new Lisql.Feature.feature_some history#store name in (* TODO: should be variable definition instead of ref *)
	    callbacks#insert Logui.uri_VariableCreation x)
	  ());
      item_uri#connect#activate ~callback:(fun () ->
	uri_dialog history#store
	  ~title:"Creation of an entity"
	  ~callback:(fun uri ->
	    let x = new Lisql.Feature.feature_name history#store (Rdf.URI uri) in
	    callbacks#insert Logui.uri_EntityCreation x)
	  ());
      item_class#connect#activate ~callback:(fun () ->
	class_dialog history#store
	  ~title:"Creation of a class"
	  ~callback:(fun uri ->
	    let x = new Lisql.Feature.feature_type history#store uri in
	    callbacks#insert Logui.uri_ClassCreation x)
	  ());
      item_prop#connect#activate ~callback:(fun () ->
	property_dialog history#store
	  ~title:"Creation of a property"
	  ~callback:(fun uri ->
	    let x = new Lisql.Feature.feature_role history#store `Subject uri in
	    callbacks#insert Logui.uri_ForwardPropertyCreation x)
	  ());
      item_prop_inv#connect#activate ~callback:(fun () ->
	property_dialog history#store
	  ~title:"Creation of an inverse property"
	  ~callback:(fun uri ->
	    let x = new Lisql.Feature.feature_role history#store `Object uri in
  	    callbacks#insert Logui.uri_BackwardPropertyCreation x)
	  ());
      item_struct#connect#activate ~callback:
	(fun () ->
	  functor_dialog history#store
	    ~title:"Creation of a structure"
	    ~callback:(fun (uri_funct, args_arity, focus) ->
	      let x = new Lisql.Feature.feature_funct history#store uri_funct args_arity focus in
	      callbacks#insert Logui.uri_StructureCreation x)
	    ());
      (* transformations *)
      if full then menu_apply#misc#hide ();
      item_for_each # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.InsertForEach);
      item_cond # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.InsertCond);
      item_seq # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.InsertSeq);
      item_and # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.InsertAnd);
      item_or # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.InsertOr));
      item_and_not # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.InsertAndNot));
      item_and_maybe # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.InsertAndMaybe));
      item_not # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.ToggleNot);
      item_maybe # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.ToggleMaybe);
      item_is_there # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.InsertIsThere);
      item_an # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.ToggleQu Lisql.AST.An));
      item_each # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.ToggleQu Lisql.AST.Each));
      item_every # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.ToggleQu Lisql.AST.Every));
      item_only # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.ToggleQu Lisql.AST.Only));
      item_no # connect # activate ~callback:
	(fun () -> callbacks#transf (Lisql.Transf.ToggleQu Lisql.AST.No));
      item_opt # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.ToggleOpt);
      item_trans # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.ToggleTrans);
      item_sym # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.ToggleSym);
      item_name # connect # activate ~callback:
	(fun () -> callbacks#name ());
      item_describe # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.Describe);
      item_select # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.Select);
      item_delete # connect # activate ~callback:
	(fun () -> callbacks#transf Lisql.Transf.Delete);
      button_delete # connect # clicked ~callback:
	(fun () -> callbacks#transf Lisql.Transf.Delete);
      (* completions *)
      let renderer = GTree.cell_renderer_text [] in
      entry_custom_completion#pack renderer;
      entry_custom_completion#add_attribute renderer "markup" markup;
      entry_custom_completion#add_attribute renderer "background" bgcolor;
(* for systematic completion *)
(*
      ignore (entry_custom#connect#changed ~callback:(fun () ->
	self#set_model_completion));
*)
      entry_custom#event#connect#key_press ~callback:(fun key ->
	let keyval = GdkEvent.Key.keyval key in
	let modifs = GdkEvent.Key.state key in
	if keyval = _plus && List.mem `CONTROL modifs then
	  begin relative_rank <- relative_rank + 1; self#set_model_completion; true end
	else if keyval = _minus && List.mem `CONTROL modifs && relative_rank > 0 then
	  begin relative_rank <- relative_rank - 1; self#set_model_completion; true end
	else
	  begin
	    if not check_lazy_completion#active || keyval = _space then self#set_model_completion (* SPACE to trigger completion *)
	    else if not (List.mem `CONTROL modifs) then self#clear_model_completion; (* to avoid controls clearing completion *)
	    false
	  end);
      entry_custom_completion # set_match_func
	(fun key iter -> true); (* matching is done upon change in custom_entry *)
      ignore (entry_custom_completion # connect # match_selected
		~callback:(fun fmodel fiter ->
		  let iter = fmodel#convert_iter_to_child_iter fiter in
		  let s = model_completion#get ~row:iter ~column:feature in
(*
		  let pre, s =
		    if s = "today" then Name.string_date_of_UnixTime (Unix.time ()), ""
		    else if s = "now" then Name.string_dateTime_of_UnixTime (Unix.gettimeofday ()), ""
		    else if String.length pre >= 2 && pre.[0] = '?' && pre.[1] <> ' ' then s, ""
		    else pre, s in
*)
		  try
		    let x = Hashtbl.find h_features s in
		    callbacks#insert Logui.uri_CompletionSelection x;
		    entry_custom#set_text "";
		    relative_rank <- 0;
		    true
		  with _ -> (* ... selected *)
		    false));
(*      entry_custom # set_completion entry_custom_completion;*)
      entry_custom # connect # activate ~callback:(self#input Default);
      tooltips#set_tip entry_custom#coerce
	~text:"Enter a custom selection";
      (* tooltips *)
(*
      tooltips#set_tip cmd_up#coerce
	~text:"Move the focus up";
*)
      tooltips#set_tip item_seq#coerce
	~text:"Add after instruction (;) to the current query focus";
      tooltips#set_tip item_cond#coerce
	~text:"Add a conditional (if) to the current query focus";
      tooltips#set_tip item_for_each#coerce
	~text:"Add a global quantifier (for each) to the current query focus";
      tooltips#set_tip item_and#coerce
	~text:"Add or create a conjunct (and) to the current query focus";
      tooltips#set_tip item_or#coerce
	~text:"Add or create an alternative (or) to the current query focus";
      tooltips#set_tip item_and_not#coerce
	~text:"Add or create an exception (and not) to the current query focus";
      tooltips#set_tip item_and_maybe#coerce
	~text:"Add or create an optional (and maybe) to the current query focus";
      tooltips#set_tip item_not#coerce
	~text:"Apply negation (not) to the current query focus";
      tooltips#set_tip item_maybe#coerce
	~text:"Apply optional (maybe) to the current query focus";
      tooltips#set_tip item_is_there#coerce
	~text:"Use the query focus as the subject of a sentence";
      tooltips#set_tip item_an#coerce
	~text:"Set 'a' as the determiner";
      tooltips#set_tip item_each#coerce
	~text:"Set 'each' as the determiner";
      tooltips#set_tip item_every#coerce
	~text:"Set 'every' as the determiner";
      tooltips#set_tip item_only#coerce
	~text:"Set 'only the' as the determiner";
      tooltips#set_tip item_no#coerce
	~text:"Set 'no' as the determiner";
(*
      tooltips#set_tip cmd_subject#coerce
	~text:"Reformulate the sentence so as to put the current focus as subject";
      tooltips#set_tip cmd_quote#coerce
	~text:"Quote/unquote the subquery at the current focus";
*)
      tooltips#set_tip item_opt#coerce
	~text:"Toggle reflexive closure on the property at the current focus";
      tooltips#set_tip item_trans#coerce
	~text:"Toggle transitive closure on the property at the current focus";
      tooltips#set_tip item_sym#coerce
	~text:"Toggle symmetric closure on the property at the current focus";
      tooltips#set_tip item_name#coerce
	~text:"Give a variable name to the current query focus";
      tooltips#set_tip item_describe#coerce
	~text:"Expand the name at current focus with its description";
      tooltips#set_tip item_select#coerce
	~text:"Set the current query focus as the whole query";
      tooltips#set_tip item_delete#coerce
	~text:"Delete the current query focus";
      tooltips#set_tip button_delete#coerce
	~text:"Delete the current query focus";
  end

type custom_iter =
    { incr : incr;
      idx : int; (* rank as child of parent *)
      parent : custom_iter option;
      mutable children : custom_iter array option; (* None means not yet computed *)
    }

class custom_tree (root : incr) column_list =
  let inbound i a = i >= 0 && i < Array.length a in
  let make_children (parent_incr : incr) (parent_opt : custom_iter option) : custom_iter array =
    (* TODO: optimize? *)
    let view = history#current#get_view parent_incr#spec in
    let increments = history#current#place#children_increments parent_incr#increment in
    let increments = List.sort ~cmp:view#links_sort_fun increments in
    let sub_increments = Common.sub_list increments view#links_page_start view#links_page_size in
    let ar_increments = Array.of_list sub_increments in
    let ar_iters = Array.mapi (fun i increment -> { incr = make_incr increment; idx = i; parent = parent_opt; children = None}) ar_increments in
    ar_iters
  in
object (self)
  inherit [custom_iter,custom_iter,unit,unit] GTree.custom_tree_model column_list

  method custom_encode_iter iter = iter, (), ()
  method custom_decode_iter iter () () = iter

  val root_children : custom_iter array = make_children root None

  method private children (iter_opt : custom_iter option) : custom_iter array =
    match iter_opt with
      | None -> root_children
      | Some iter ->
	( match iter.children with
	    | None ->
	      let children = make_children iter.incr (Some iter) in
	      iter.children <- Some children;
	      children
	    | Some children -> children )

  method custom_get_iter (path : Gtk.tree_path) : custom_iter option =
    let indices : int array = GTree.Path.get_indices path in
    let d = Array.length indices in
    if d = 0
    then None
    else
      if inbound indices.(0) root_children
      then
	let result = ref (root_children.(indices.(0))) in
	try
	  for depth = 1 to d - 1 do
	    let children = self#children (Some !result) in
	    let index = indices.(depth) in
	    if inbound index children
	    then result := children.(index)
	    else raise Not_found
	  done;
	  Some !result
	with Not_found -> None
      else None

  method custom_get_path (row : custom_iter) : Gtk.tree_path =
    let current_row = ref row in
    let path = ref [] in
    while !current_row.parent <> None do
      path := !current_row.idx::!path;
      current_row := match !current_row.parent with Some p -> p | None -> assert false
    done;
    GTree.Path.create ((!current_row.idx)::!path)

  method get_incr (row : custom_iter) = row.incr

  method get_feature (row : custom_iter) = row.incr#string
  method get_markup (row : custom_iter) = make_markup (row.incr#display ~obs:Tarpit.blind_observer)
  method get_count (row : custom_iter) = string_of_int row.incr#supp
  method get_lift_label (row : custom_iter) =
    if not check_lift#active
    then " "
    else string_of_int row.incr#lift#supp
  method get_lift_color (row : custom_iter) =
    if not check_lift_color#active
    then str_white
    else row.incr#lift#color
  method get_row_color (row : custom_iter) =
    if not check_feat_color#active
    then str_black
    else
      let open Lisql.Feature in
	  match row.incr#kind with
	    | Kind_Variable -> str_color_variable
	    | Kind_Entity -> str_color_entity
	    | Kind_Literal -> str_color_literal
	    | Kind_Class -> str_color_class
	    | Kind_Property -> str_color_property
	    | Kind_InverseProperty -> str_color_property
	    | Kind_Structure -> str_color_functor
	    | Kind_Argument -> str_color_functor
	    | _ -> str_black
  method get_row_scale (row : custom_iter) =
    if not check_scale#active
    then 1.
    else row.incr#scale
  method get_row_weight (row : custom_iter) =
    if row.incr#anew
    then weight_new
    else weight_normal
  method get_bg_color (row : custom_iter) =
    str_white
  method get_thumb_file (row : custom_iter) = 
    match row.incr#uri_opt with
      | None -> ""
      | Some uri ->
	match picture_of_name ~obs:Tarpit.blind_observer (Rdf.URI uri) with
	  | None -> ""
	  | Some f -> f

  method custom_value (t : Gobject.g_type) (row : custom_iter) ~(column : int) =
    match column with
      | 0 -> (* feature *) `STRING (Some (self#get_feature row))
      | 1 -> (* markup *) `STRING (Some (self#get_markup row))
      | 2 -> (* count *) `STRING (Some (self#get_count row))
      | 3 -> (* lift_label *) `STRING (Some (self#get_lift_label row))
      | 4 -> (* lift_color *) `STRING (Some (self#get_lift_color row))
      | 5 -> (* row color *) `STRING (Some (self#get_row_color row))
      | 6 -> (* row_scale *) `FLOAT (self#get_row_scale row)
      | 7 -> (* row_weight *) `INT (self#get_row_weight row)
      | 8 -> (* bg_color *) `STRING (Some (self#get_bg_color row))
      | 9 -> (* thumb_file *) `STRING (Some (self#get_thumb_file row))
      | _ -> assert false

  method custom_iter_next (row : custom_iter) : custom_iter option =
    let children = self#children row.parent in
    let nidx = succ row.idx in
    if inbound nidx children then Some children.(nidx) else None

  method custom_iter_children (row_opt : custom_iter option) : custom_iter option =
    let children = self#children row_opt in
    if inbound 0 children then Some children.(0) else None

  method custom_iter_has_child (row : custom_iter) : bool =
    Array.length (self#children (Some row)) > 0

  method custom_iter_n_children (row_opt : custom_iter option) : int =
    Array.length (self#children row_opt)

  method custom_iter_nth_child (row_opt : custom_iter option) (n : int) : custom_iter option =
    let children = self#children row_opt in
    if inbound n children then Some children.(n) else None

  method custom_iter_parent (row : custom_iter) : custom_iter option =
    row.parent

end

class tree_features_callbacks =
  object
    method row_activated : Gtk.tree_path -> unit = fun path -> ()
    method refresh_preselect : bool -> unit = fun _ -> ()
    method ctx_menu : incr list -> GMenu.menu -> unit = fun lx menu -> ()
    method data_received : unit -> unit = fun () -> ()
  end

class tree_features multi_tree ~pack (v : id_view) (get_incr : unit -> incr) =
  let sw = GBin.scrolled_window ~shadow_type:`ETCHED_IN
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC ~packing:pack () in
  let cols = new GTree.column_list in
  let feature = cols#add string in
  let markup = cols#add string in
  let count = cols#add string in
  let lift_label = cols#add string in
(*      let lift_scale = cols#add float in *)
(*      let lift_fg_color = cols#add string in *)
  let lift_color = cols#add string in
  let row_color = cols#add string in
  let row_scale = cols#add float in
  let row_weight = cols#add int in
  let bg_color = cols#add string in
  let thumb_file = cols#add string in
  let view = GTree.view (*~model:model*) ~headers_visible:false ~packing:sw#add () in
  let cell_count =  GTree.cell_renderer_text [`XALIGN 1.; `YALIGN 0.] in
  let col_count =
    let col =
      GTree.view_column ~title:"Count" ()
	~renderer:(cell_count, ["text",count]) in
    col # add_attribute cell_count "scale" row_scale;
    col # set_sizing `AUTOSIZE;
    col in
  let cell_lift = GTree.cell_renderer_text [`XALIGN 1.; `YALIGN 0.] in
  let col_lift =
    let col =
      GTree.view_column ~title:"Lift" ()
	~renderer:(cell_lift, ["text",lift_label]) in
(*	col # add_attribute cell_lift "scale" lift_scale; *)
(*	col # add_attribute cell_lift "foreground" lift_fg_color; *)
    col # add_attribute cell_lift "background" lift_color;
    col # set_sizing `AUTOSIZE;
    col in
  let cell_feature = GTree.cell_renderer_text [`YALIGN 0.] in
  let col_feature =
    let col =
      GTree.view_column ~title:"Feature" ()
	~renderer:(cell_feature, ["markup",markup]) in
    col # add_attribute cell_feature "scale" row_scale;
    col # add_attribute cell_feature "background" bg_color;
    col # add_attribute cell_feature "weight" row_weight;
    col # set_sizing `AUTOSIZE;
    col in
  let cell_thumb = GTree.cell_renderer_pixbuf [`YALIGN 0.] in
  let col_thumb =
    let col =
      GTree.view_column ~title:"Thumbnail" ()
	~renderer:(cell_thumb, []) in
    col # set_sizing `AUTOSIZE;
    col # set_cell_data_func cell_thumb
      (fun model iter ->
	let file = model#get ~row:iter ~column:thumb_file in
	if file = ""
	then cell_thumb#set_properties [`VISIBLE false]
	else
	  let pixbuf = pixbuf_of_picture ~maxside:200 file in
	  let height = GdkPixbuf.get_height pixbuf in
	  let width = GdkPixbuf.get_width pixbuf in
	  cell_thumb#set_properties [`HEIGHT (height+20); `PIXBUF pixbuf; `VISIBLE true]);
    col in
  let function_top = "(top)" in
  let function_up = "(up)" in
  let function_down = "(down)" in
  let function_bottom = "(bottom)" in
  object (self)
    val mutable callbacks = new tree_features_callbacks

    method connect c = callbacks <- c

    method widget = (sw :> GObj.widget)

    method root = v

    val mutable model = new custom_tree (get_incr ()) cols

    method model = model

    val mutable ignore_selection_changed = false

    method selection_changed () =
      multi_tree#set_focus v;
      if ignore_selection_changed
      then ignore_selection_changed <- false
      else begin
	let paths = view # selection # get_selected_rows in
	let new_preselection =
	  match paths with
	  | [] -> None
	  | lp ->
	      let ext =
		Ext.union_r
		  (Common.mapfilter
		     (fun path ->
		       try Some (self # ext_from_path path)
		       with _ -> None)
		     lp) in
	      if Ext.is_empty ext
	      then None
	      else Some ext in
	let st = history # current in
	if new_preselection <> st # int_preselect
	then begin
	  st # set_int_preselect new_preselection;
	  if check_int_preselect#active then self # refresh_preselect false
	end
      end

    method row_activated path vcol = callbacks#row_activated path

    method button_press ev =
      let path_opt =
	let x = int_of_float (GdkEvent.Button.x ev) in
	let y = int_of_float (GdkEvent.Button.y ev) in
	match view#get_path_at_pos ~x ~y with
	| None -> None
	| Some (path, _, _, _) -> Some path in
      let button = GdkEvent.Button.button ev in
      if button = 3 then
	let selection = view#selection in
	let nb = selection#count_selected_rows in
	if nb = 0 then begin
	  Option.iter
	    (fun path ->
	      selection#unselect_all ();
	      selection#select_path path)
	    path_opt
	end;
	let menu = GMenu.menu () in
	let lx = self#selected_increments in
	callbacks # ctx_menu lx menu;
	menu#popup ~button:(GdkEvent.Button.button ev) ~time:(GdkEvent.Button.time ev);
	true
      else
	false

    method incr_from_path path : incr =
      match model#custom_get_iter path with
	| None -> failwith "Gui.tree_features#incr_from_path: invalid path"
	| Some iter -> model#get_incr iter

    method incr_from_iter iter : incr =
      self#incr_from_path (model#get_path iter)

    method ext_from_path path : Ext.t =
      (self#incr_from_path path)#ext

    method spec_from_path path : Lisql.Feature.spec =
      (self#incr_from_path path)#spec

    method spec_from_iter iter : Lisql.Feature.spec =
      (self#incr_from_iter iter)#spec

    method state_view = history # current # get_view v

	(* get the state_view of the path, and create it if necessary *)
    method state_view_from_path path =
      let st = history # current in
      let views = st # views in
      let v = self # spec_from_path path in
      try
	Hashtbl.find views v
      with _ ->
	let sv =
	  try
	    match model#iter_parent (model#get_iter path) with
	    | None -> st # copy_view (self # state_view) v
	    | Some iter -> st # copy_view (Hashtbl.find views (self # spec_from_iter iter)) v
	  with _ -> (* should not happen *)
	    st # new_view v in
	Hashtbl.add views v sv;
	sv

    method selected_paths = view # selection # get_selected_rows

    method selected_increments =
      let paths = self#selected_paths in
      List.map self#incr_from_path paths

    method selected_update ~callback () =
      Tarpit.effect (fun obs ->
	let paths = view#selection#get_selected_rows in
	let lu = List.map self#incr_from_path paths in
	callback (lu, paths))

    method copy_features =
      let paths = view#selection#get_selected_rows in
      let fs = List.map (fun path -> self # incr_from_path path) paths in
      cb_copy := Features fs

    method refresh (force : bool) =
      Common.prof "Gui.tree_features#refresh" (fun () ->
	view#selection#unselect_all ();
	let new_model = new custom_tree (get_incr ()) cols in
	model <- new_model;
	view#set_model (Some (new_model :> GTree.model)))

    method refresh_preselect unselect = ()
(*
      if unselect && view#selection#get_selected_rows <> [] then begin
	ignore_selection_changed <- true;
	view#selection#unselect_all ()
      end;
      if check_int_preselect#active
      then
	let int_preselect = history # current # int_preselect in
	model # foreach
	  (fun path iter ->
	    let e : Ext.t =
	      try (self#incr_from_iter iter)#ext
	      with exn -> Ext.empty in (* mystery: fails with exception, but [e] has the right value ! *)
	    model#set ~row:iter ~column:bg_color (self # bg_color_from_ext int_preselect e);
	    false)
      else
	model # foreach
	  (fun path iter ->
	    model#set ~row:iter ~column:bg_color str_white;
	    false)
*)

    method expand_row path = view#expand_row path
    method collapse_row path = view#collapse_row path

    initializer
      view#selection#set_mode `MULTIPLE;
      col_lift#set_sizing `AUTOSIZE;
      col_count#set_sizing `AUTOSIZE;
      col_count#set_cell_data_func
	cell_count
	(fun model iter ->
	  let bg_color =
	    if check_ext_color#active (* && row_color = str_black *)
	    then
	      try
		let current = history#current in
		let place = current#place in
		let incr = self#incr_from_iter iter in
		if incr#supp = place#nb_answers
		then str_color_of_place_mode place#mode
(*		  if place#rank = 0 then str_color_query else str_color_update *)
		else str_white
	      with _ -> str_white
	    else str_white in
	  cell_count#set_properties [`CELL_BACKGROUND bg_color]);
      view#append_column col_lift;
      view#append_column col_count;
      view#append_column col_thumb;
      view#append_column col_feature;
      view#set_expander_column (Some col_feature);

      view#selection#connect#after#changed ~callback:self#selection_changed;
      view#connect#after#row_activated ~callback:self#row_activated;
      view#event#connect#button_press ~callback:self#button_press;
      view#drag#source_set ~modi:[`BUTTON1] ~actions:[`MOVE]
	[ { Gtk.target = "STRING"; Gtk.flags = []; Gtk.info = 0} ];
      view#drag#connect#data_get ~callback:(fun _ sel ~info ~time ->
	self#copy_features;
	view#selection#unselect_all ();
	sel#return "");
      view#drag#dest_set ~actions:[`MOVE]
	[ { Gtk.target = "STRING"; Gtk.flags = []; Gtk.info = 0} ];
      view#drag#connect#data_received ~callback:(fun context ~x ~y sel ~info ~time ->
	GtkSignal.stop_emit ();
	begin try
	  view#selection#unselect_all ();
	  ( match view#get_path_at_pos ~x ~y with
	  | Some (path,_,_,_) -> view#selection#select_path path
	  | None -> ()
	   );
	  callbacks # data_received ()
	with _ -> () end;
	context#finish ~success:true ~del:false ~time);
      view#drag#connect#data_delete ~callback:(fun context ->
	GtkSignal.stop_emit ());
      ()
  end

class multi_tree_features navlink ~pack =
  let paned = GPack.paned `HORIZONTAL ~packing:pack () in
  object (self)
    val ht : ([`P12 | `S12],tree_features) Hashtbl.t = Hashtbl.create 2

    initializer
      let tree_p12 = new tree_features self
	  ~pack:(paned#pack1 ~resize:true ~shrink:true)
	  Lisql.Feature.Spec_Thing (fun () -> make_incr history#current#place#increment_p12) in
      Hashtbl.add ht `P12 tree_p12;
      let tree_s12 = new tree_features self
	  ~pack:(paned#pack2 ~resize:true ~shrink:true)
	  Lisql.Feature.Spec_Something (fun () -> make_incr history#current#place#increment_s12) in
      Hashtbl.add ht `S12 tree_s12

    method get_tree focus =
      try Hashtbl.find ht focus
      with _ -> assert false

    method tree_p12 = self#get_tree `P12
    method tree_s12 = self#get_tree `S12

    val mutable callbacks = new tree_features_callbacks

    method connect c =
      callbacks <- c;
      self#tree_p12#connect c;
      self#tree_s12#connect c

    val mutable focus : [`P12 | `S12] = `P12

    method set_focus (v : id_view) : unit =
      focus <-
	( match v with
	| Lisql.Feature.Spec_Thing -> `P12
	| Lisql.Feature.Spec_Something -> `S12
	| _ -> assert false );
      navlink#set_focus_on_extent false

    method current =
      self#get_tree focus

    method selection_changed = self # current # selection_changed

    method spec_from_path = self # current # spec_from_path

    method incr_from_path = self # current # incr_from_path

    method ext_from_path = self # current # ext_from_path

    method state_view = self # current # state_view

    method state_view_from_path = self # current # state_view_from_path

    method selected_paths = self # current # selected_paths

    method selected_increments = self # current # selected_increments

    method selected_update = self # current # selected_update

    method copy_features = self # current # copy_features

    method refresh force =
      Common.prof "Gui.navig#refresh/p12" (fun () -> self#tree_p12#refresh force);
      Common.prof "Gui.navig#refresh/s12" (fun () -> self#tree_s12#refresh force)

    method refresh_preselect unselect =
      self#tree_p12#refresh_preselect unselect;
      self#tree_s12#refresh_preselect unselect

    method expand_row path = self#current#expand_row path
    method collapse_row path = self#current#collapse_row path

    method set_ratio n d = paned_set_ratio paned n d
  end

class answer_cell answer_list ~(row : int) ~(col : int) (data_cell : Lisql.cell) =
  let disp = data_cell#display in
  let markup = make_markup disp in
  let box = GBin.event_box () in
  let label_cell = GMisc.label ~markup (*~selectable:true*) ~xalign:0. ~yalign:0. ~packing:box#add () in
  object (self)
    method widget = box#coerce

    method row = row
    method col = col
    method name_opt = data_cell#name_opt
    method display = disp

    initializer
      box#event#connect#button_press
	~callback:(fun ev -> answer_list#callbacks#ctx_menu self ev);
      ()
  end

class list_answers_callbacks =
  object
    method row_activated : Name.t option list -> unit = fun line -> ()
    method ctx_menu : answer_cell -> GdkEvent.Button.t -> bool = fun cell ev -> false
  end

class list_answers ~pack =
  let vbox = GPack.vbox ~spacing:4 ~packing:pack () in
  let sw = GBin.scrolled_window ~shadow_type:`ETCHED_IN
      ~hpolicy:`AUTOMATIC ~vpolicy:`AUTOMATIC ~packing:(vbox#pack ~expand:true ~fill:true) () in
  let hbox = GPack.hbox ~spacing:8 ~packing:vbox#pack () in
  let label_nb_answers = GMisc.label ~text:"No answer" ~packing:hbox#pack () in
  let button_top = GButton.button ~stock:`GOTO_TOP (*~label:"Top"*) (*~relief:`NONE*) ~packing:hbox#pack () in
  let button_up = GButton.button ~stock:`GO_UP (*~label:"Up"*) (*~relief:`NONE*) ~packing:hbox#pack () in
  let entry_start = GEdit.entry ~width_chars:6 ~xalign:1. ~packing:hbox#pack () in
  let _ = GMisc.label ~text:" - " ~packing:hbox#pack () in
  let entry_end = GEdit.entry ~width_chars:6 ~xalign:1. ~packing:hbox#pack () in
  let button_down = GButton.button ~stock:`GO_DOWN (*~label:"Down"*) (*~relief:`NONE*) ~packing:hbox#pack () in
  let button_bottom = GButton.button ~stock:`GOTO_BOTTOM (*~label:"Bottom"*) (*~relief:`NONE*) ~packing:hbox#pack () in
  let button_export = GButton.button ~label:"Export" ~packing:(hbox#pack ~from:`END) () in

  let function_top = "<top>" in
  let function_up = "<up>" in
  let function_down = "<down>" in
  let function_bottom = "<bottom>" in

  let nb_headers = 2 in
  object (self)
    val mutable callbacks = new list_answers_callbacks
    method callbacks = callbacks
    method connect c = callbacks <- c

    val mutable answers = history#current#place#answers

    val mutable table = GPack.table ()
    val mutable persistent_widgets : GObj.widget list = []
    method add_persistent_widget w = persistent_widgets <- w::persistent_widgets
    method mem_persistent_widget w = List.exists (fun w1 -> w#misc#get_oid = w1#misc#get_oid) persistent_widgets
    method clean_persistent_widgets = persistent_widgets <- []

    method refresh = Common.prof "Gui.list_answers#refresh" (fun () ->
      answers <- history#current#place#answers;
      ignore (answers#page_top);
      self#refresh_aux)

    method private refresh_aux =
      List.iter ~f:sw#remove sw#children;
      self#clean_persistent_widgets;
      let lv = answers#columns in
      if lv <> []
      then begin
	table <- GPack.table
	    ~columns:(List.length lv)
	    ~rows:(nb_headers + answers#page_size)
	    ~homogeneous:false
	    ~row_spacings:8
	    ~col_spacings:16
	    ~border_width:1
	    ~packing:sw#add_with_viewport
	    ();
	let _ =
	  List.fold_left
	    ~f:(fun (state,col) v ->
	      let hidden = answers#get_hidden v in
	      let order = answers#get_order v in
	      let aggreg = answers#get_aggreg v in
	      let menubar = GMenu.menu_bar () in
	      let menubar_factory = new GMenu.factory menubar in
	      let menu = menubar_factory#add_submenu v in
	      GToolbox.build_menu menu
		~entries:( (if v = Lisql.v_count
		            then []
		            else `C ("hidden", hidden,
				     (fun b -> answers#set_hidden v b; self#refresh_contents)) ::
		                  ( match state with
				  | `Dimensions ->
				      `S ::
				      `R [ ("default", order = `DEFAULT,
					    (function true -> answers#set_order v `DEFAULT; self#refresh_contents | _ -> ()));
					   ("increasing", order = `ASC,
					    (function true -> answers#set_order v `ASC; self#refresh_contents | _ -> ()));
					   ("decreasing", order = `DESC,
					    (function true -> answers#set_order v `DESC; self#refresh_contents | _ -> ())) ] ::
				      [`S]
				  | `Measures ->
				      `S ::
				      `R [ ("distribution", aggreg = `INDEX,
					    (function true -> answers#set_aggreg v `INDEX; self#refresh_contents | _ -> ()));
					   ("distinct count", aggreg = `DISTINCT_COUNT,
					    (function true -> answers#set_aggreg v `DISTINCT_COUNT; self#refresh_contents | _ -> ()));
					   ("sum", aggreg = `SUM,
					    (function true -> answers#set_aggreg v `SUM; self#refresh_contents | _ -> ()));
					   ("average", aggreg = `AVG,
					    (function true -> answers#set_aggreg v `AVG; self#refresh_contents | _ -> ())) ] ::
				      [`S])) @
			   (if col <> 0
			   then [`I ("Move left", (fun () -> answers#move_column_left v; self#refresh_aux))]
			   else []) @
			   (if col <> List.length answers#columns - 1
			   then [`I ("Move right", (fun () -> answers#move_column_right v; self#refresh_aux))]
			   else []));
	      self#add_persistent_widget menubar#coerce;
	      table#attach ~left:col ~top:0 ~fill:`X menubar#coerce;
	      (if v = Lisql.v_count then `Measures else state), col+1)
	    ~init:(`Dimensions,0)
	    lv in
	let _ =
	  List.fold_left
	    ~f:(fun (is_dim,col) v ->
	      let is_dim = if v = Lisql.v_count then false else is_dim in
	      if is_dim then begin
		let entry = GEdit.entry ~text:(answers#get_pattern v) ~width_chars:8 () in
		entry#connect#changed ~callback:(fun () -> answers#set_pattern v entry#text; self#refresh_contents);
		self#add_persistent_widget entry#coerce;
		table#attach ~left:col ~top:1 ~fill:`X entry#coerce
	      end;
	      (is_dim, col+1))
	    ~init:(true,0)
	    lv in
	self#refresh_contents end
      else begin
	ignore (GMisc.label
		  ~text:"Name entities in the query with variables for a list of query answers."
		  ~packing:sw#add_with_viewport ())
      end

    method refresh_contents =
      List.iter ~f:(fun w -> if not (self#mem_persistent_widget w) then table#remove w) table#children;
      let _ =
	answers#fold ~obs:Tarpit.blind_observer
	  (fun row line ->
	    List.fold_left
	      ~f:(fun col data_cell ->
		let cell = new answer_cell self ~row ~col data_cell in
		table#attach ~left:col ~top:row ~fill:`BOTH cell#widget;
		col+1)
	      ~init:0
	      line;
	    row+1)
	  nb_headers in
      label_nb_answers#set_text (Printf.sprintf "%d answers" answers#count);
      entry_start#set_text (string_of_int answers#page_start);
      entry_end#set_text (string_of_int answers#page_end);
      ()

    method export_contents =
      let temp_file = Filename.temp_file "sewelis_answers" ".csv" in
      let chout = open_out temp_file in
      let _ =
	List.fold_left
	  ~f:(fun first_col v ->
	    if not first_col then output_string chout ",";
	    output_string chout (escape_csv_cell v);
	    false)
	  ~init:true
	  answers#columns in
      output_string chout "\r\n";
      let _ =
	answers#fold ~obs:Tarpit.blind_observer
	  (fun row line ->
	    let _ =
	      List.fold_left
		~f:(fun first_col data_cell ->
		  if not first_col then output_string chout ",";
		  output_string chout (make_csv_cell data_cell#display);
		  false)
		~init:true
		line in
	    output_string chout "\r\n")
	  () in
      close_out chout;
      open_url temp_file

    initializer
      button_top#connect#clicked ~callback:(fun () -> if answers#page_top then self#refresh_contents);
      button_down#connect#clicked ~callback:(fun () -> if answers#page_down then self#refresh_contents);
      button_up#connect#clicked ~callback:(fun () -> if answers#page_up then self#refresh_contents);
      button_bottom#connect#clicked ~callback:(fun () -> if answers#page_bottom then self#refresh_contents);
      button_export#connect#clicked ~callback:(fun () -> self#export_contents);
      entry_start#connect#activate
	~callback:(fun () ->
	  try if answers#set_start (int_of_string entry_start#text) then self#refresh_contents
	  with _ -> error_message "Please enter a positive integer");
      entry_end#connect#activate
	~callback:(fun () ->
	  try if answers#set_end (int_of_string entry_end#text) then self#refresh_contents
	  with _ -> error_message "Please enter a positive integer");
      ()
  end

let paned_global = GPack.paned `VERTICAL ~packing:(vbox#pack ~expand:true ~fill:true) ()

let paned_intension = GPack.paned `HORIZONTAL ~packing:(paned_global#pack1 ~resize:true ~shrink:true) ()

let focus_ef = new entry_feature ~full:false ()
let wq = new text_query
    ~width_chars:30
    ~pack:(paned_intension#pack1 ~resize:true ~shrink:false)
    ~focus_widget:focus_ef

let vbox = GPack.vbox ~spacing:4 ~packing:(paned_intension#pack2 ~resize:true ~shrink:true) ()

let ml = new more_less ~pack:vbox#pack ()

let ef = new entry_feature ~full:true ~pack:vbox#pack ()

let navig = new multi_tree_features wq ~pack:(vbox#pack ~expand:true ~fill:true) (*paned_sw#pack1 ~resize:true ~shrink:true*)

let ans = new list_answers ~pack:(paned_global#pack2 ~resize:true ~shrink:true)

(* global *)

let refresh_ui force =
  focus_ef#refresh;
  wq#refresh;
  ml#refresh;
  ef#refresh;
  navig#refresh force;
  ans#refresh

let refresh force = Common.prof "Gui.refresh" (fun () ->
  refresh_ui force)

let refresh_force () = refresh true

let open_name = function
  | Rdf.URI uri -> open_url uri
  | Rdf.Literal (s, _) ->
    open_url ("http://www.google.com/search?q=" ^
		 Str.global_replace (Str.regexp "[ \t\n\r]+") "+" s)
  | _ -> error_message "The resource cannot be opened"

let import_rdf_tree (abs_path : string) : unit =
  Lisql.import_file_tree
    ~f_file:(fun filename ->
      try
	prerr_string "Importing "; prerr_endline filename;
	let uri = "file://" ^ filename in
	history#store#import_rdf ~base:uri filename
      with exn -> print_endline ("import_rdf: " ^ Printexc.to_string exn))
    abs_path

let import_uris uris =
  if question ("Are you sure you want to import linked data for " ^ string_of_int (List.length uris) ^ " URIs")
  then
    awaken
      (fun () -> List.iter (fun uri -> history#store#import_uri uri) uris)
      refresh_force

let import_path path =
  awaken
    (fun () -> history#store#import_path path)
    refresh_force

let delete_names names =
  if question ("Are you sure you want to delete those "
	       ^ string_of_int (List.length names)
	       ^ " resources, and all their relations to other resources?")
  then awaken (fun () -> List.iter history#store#remove_entity names) refresh_force

(* Event handling *)

let menu_paste_aux get_update =
  match !cb_copy with
  | Nothing -> ()
  | Features fs ->
      get_update
	~callback:(fun (lu,_) ->
	  List.iter
	    (fun f -> 
	      match f#p1_opt with
	      | None -> ()
	      | Some a ->
		  List.iter
		    (fun u ->
		      match u#p1_opt with
		      | None -> ()
		      | Some b -> history#store#add_axiom a b)
		    lu)
	    fs;
	  refresh true) ()
  | Objects oids ->
      get_update
	~callback:(fun (lu, paths) ->
	  Ext.iter
	    (fun oid ->
	      List.iter
		(fun u ->
		  match u#p1_opt with
		  | None -> ()
		  | Some b -> history#store#move_obj oid b)
		lu)
	    oids;
	  refresh true) ()

let menu_paste () =
  menu_paste_aux navig#selected_update

let menu_new () =
  file_dialog ~title:"Create a log" ~filter:filter_log ~filename:!last_dir
    ~callback:(fun name ->
      if confirm_overwrite name then begin
	history#init_store name;
	window#set_title (utf (title ()));
	refresh true end) ()

let menu_open () =
  file_dialog ~title:"Open a log" ~filter:filter_log ~filename:!last_dir
    ~callback:(fun name ->
      history#init_store name;
      window#set_title (utf (title ()));
      refresh true)
    ()

let menu_save () =
  history#save_store;
  history#store#export_rdf
    ~base:history#store#base
    ~xmlns:history#store#xmlns
    (history#filepath ^ ".nt")


let menu_define_label uri_label default_lang uri () =
  fields_dialog ~title:"Define a new label"
    ~fields:[("label", `String (Lisql.Display.string_of_uri ~obs:Tarpit.blind_observer history#store uri))]
    ~callback:(function
      | [`String label] ->
	  awaken
	    (fun () ->
	      if uri_label = Rdfs.uri_label then
		history#store#set_label uri label
	      else if uri_label = Lisql.Namespace.uri_inverseLabel then
		history#store#set_inverseLabel uri label
	      else assert false)
	    refresh_force
      | _ -> assert false)

let menu_define_ns uri () =
  fields_dialog ~title:"Define a new namespace"
    ~fields:[("prefix",`String ""); ("URI prefix", `String uri)]
    ~callback:(function
      | [`String pre; `String ns] ->
	  let pre = if pre = "" || pre.[String.length pre - 1] <> ':' then pre ^ ":" else pre in
	  if Lisql.Syntax.valid_string Name.parse_prefix pre
	  then awaken (fun () -> history#store#add_prefix pre ns) refresh_force
	  else failwith "Invalid prefix"
      | _ -> assert false)

let menu_toggle_transitive uri =
  history#store#toggle_type (Rdf.URI uri) Owl.uri_TransitiveProperty

let menu_toggle_symmetric uri =
  history#store#toggle_type (Rdf.URI uri) Owl.uri_SymmetricProperty


let menu_import_rdf () =
  file_dialog ~title:"Import an RDF document"
    ~callback:(fun path ->
      awaken (fun () -> import_rdf_tree path) refresh_force)
    ()

let menu_export_rdf () =
  file_dialog ~title:"Export an RDF document" ~filter:filter_nt
    ~callback:(fun path ->
      history#store#export_rdf ~base:history#store#base ~xmlns:history#store#xmlns path)
    ()

(* TO BE REVISED... *)
(*
let menu_import_xml () =
  file_dialog ~title:"Import an XML document"
    ~callback:(fun path ->
      awaken (fun () -> history#store#import_xml path) refresh_force)
    ()
*)

let menu_import_uri () =
  fields_dialog ~title:"Import a URI as Linked Data"
    ~fields:[("URI", `String "")]
    ~callback:(function
      | [`String uri] ->
	  awaken (fun () -> history#store#import_uri uri) refresh_force
      | _ -> assert false)

let menu_import_path () =
  file_dialog ~title:"Import files" ~filename:!last_dir ~filter:""
    ~callback:(fun path ->
      last_dir := Filename.dirname path ^ dir_sep;
      import_path path)
    ()

let menu_stat () =
  info_message "No stat available"
(*
   let average sum nb = if nb = 0 then 0 else sum / nb in
   let st = Context.get_stat () in
   let msg = String.concat "\n" [
   "number of objects: " ^  string_of_int st.Logcache.nb_obj;
   "number of features: " ^ string_of_int st.Logcache.nb_feat;
   "context size:";
   "- " ^ string_of_int st.Logcache.nb_link ^ " object-feature (" ^ string_of_int st.Logcache.mem_link ^ " words), "
   ^ string_of_int (average st.Logcache.nb_link st.Logcache.nb_obj) ^ " features by object";
   "- " ^ string_of_int st.Logcache.nb_map ^ " object-feature-object (" ^ string_of_int st.Logcache.mem_map ^ " words), "
   ^ string_of_int (average st.Logcache.nb_map st.Logcache.nb_obj) ^ " connections by object"
   ] in
   info_message msg
 *)

let menu_quit () =
  if question "Do you want to save the current store before exiting ?" then
    menu_save ();
  GMain.quit ();
  Printf.printf "Profiling...\n";
  let l =
    Hashtbl.fold
      (fun s elem res -> (elem.Common.prof_time, elem.Common.prof_nb, elem.Common.prof_mem,s)::res)
      Common.tbl_prof [] in
  let l = List.sort Pervasives.compare l in
  List.iter
    (fun (t,n,m,s) -> Printf.printf "%s: %d calls, %.1f seconds\n" s n t)
    l

let menu_cd ?msg = function
  | Some foc ->
      history#cd foc;
      Logui.item Logui.uri_QueryChange (fun obs ->
	[ Logui.feature_assertion (Lisql.AST.command_of_focus foc) ]);
      refresh true
  | None -> Option.iter error_message msg

let menu_insert logui_uri (x : Lisql.Feature.feature) =
(* print_endline ("menu_insert: " ^ Lisql.Syntax.string_of_assertion ~ctx:(history#store :> Lisql.Syntax.context) history#current#place#assertion); *)
  Logui.item logui_uri (fun obs -> [Logui.feature_feature x]);
  menu_cd (Lisql.Feature.focus_insert history#store x history#current#place#focus)

let menu_name () =
  menu_insert Logui.uri_Name (new Lisql.Feature.feature_some history#store history#current#place#new_var)

let menu_transf (k : Lisql.Transf.kind) =
  let logui_uri =
    let open Lisql.Transf in
    match k with
    | FocusUp -> Logui.uri_FocusUp
    | FocusDown -> Logui.uri_FocusDown
    | FocusLeft -> Logui.uri_FocusLeft
    | FocusRight -> Logui.uri_FocusRight
    | FocusTab -> Logui.uri_FocusTab
    | InsertForEach -> Logui.uri_InsertForEach
    | InsertCond -> Logui.uri_InsertCond
    | InsertSeq -> Logui.uri_InsertSeq
    | InsertAnd -> Logui.uri_InsertAnd
    | InsertOr -> Logui.uri_InsertOr
    | InsertAndNot -> Logui.uri_InsertAndNot
    | InsertAndMaybe -> Logui.uri_InsertAndMaybe
    | InsertIsThere -> Logui.uri_InsertIsThere
    | ToggleQu qu ->
	( match qu with
	| Lisql.AST.An -> Logui.uri_InsertAn
	| Lisql.AST.The -> assert false
	| Lisql.AST.Each -> Logui.uri_InsertEach
	| Lisql.AST.Every -> Logui.uri_InsertEvery
	| Lisql.AST.Only -> Logui.uri_InsertOnly
	| Lisql.AST.No -> Logui.uri_InsertNo )
    | ToggleNot -> Logui.uri_ToggleNot
    | ToggleMaybe -> Logui.uri_ToggleMaybe
    | ToggleOpt -> Logui.uri_ToggleOpt
    | ToggleTrans -> Logui.uri_ToggleTrans
    | ToggleSym -> Logui.uri_ToggleSym
    | Describe -> Logui.uri_Describe
    | Select -> Logui.uri_Select
    | Delete -> Logui.uri_Delete in
  Logui.item logui_uri (fun obs -> []);
  menu_cd (Lisql.Transf.focus_apply history#store k history#current#place#focus)


let menu_reset foc =
  history#reset foc;
  Logui.item Logui.uri_QueryChange (fun obs ->
    [ Logui.feature_assertion (Lisql.AST.command_of_focus foc) ]);
  refresh true

let menu_give_least () =
    history#current#place#least_extent;
    Logui.item Logui.uri_LeastObjects (fun obs -> []);
    refresh_ui true

let menu_give_less () =
  history#current#place#less_extent;
  Logui.item Logui.uri_LessObjects (fun obs -> []);
  refresh_ui true

let menu_give_more () =
    history#current#place#more_extent;
    Logui.item Logui.uri_MoreObjects (fun obs -> []);
    refresh_ui true

let menu_give_most () =
    history#current#place#most_extent;
    Logui.item Logui.uri_MostObjects (fun obs -> []);
    refresh_ui true


let menu_root () =
  Logui.item Logui.uri_Root (fun obs -> []);
  menu_reset history#root

let menu_home () =
  Logui.item Logui.uri_Home (fun obs -> []);
  menu_reset history#home

let menu_bookmarks () =
  Logui.item Logui.uri_Bookmarks (fun obs -> []);
  menu_reset Lisql.AST.focus_bookmarks

let menu_drafts () =
  Logui.item Logui.uri_Drafts (fun obs -> []);
  menu_reset Lisql.AST.focus_drafts


let menu_back () =
  history # backward;
  Logui.item Logui.uri_Back (fun obs -> []);
  refresh false

let menu_fwd () =
  history # forward;
  Logui.item Logui.uri_Forward (fun obs -> []);
  refresh false

let menu_refresh () =
  refresh true

let menu_expand () =
  let paths = navig # selected_paths in
  let sv =
    match paths with
    | [] -> navig # state_view
    | path::_ -> navig # state_view_from_path path in
  let w = GWindow.dialog
      ~destroy_with_parent:true
      ~title:"Expanding a view with options"
      () in
  let vbox = w#vbox in
  let check_lexicalorder = GButton.check_button ~label:"logical (vs. decreasing count) sorting of increments"
      ~active:(sv # links_sort_fun==sort_by_feature) ~packing:vbox#add () in
  let page_size = let ps = sv # links_page_size in if ps = max_int then None else Some (string_of_int ps) in
  let check_page_size = GButton.check_button ~label:"maximum number of increments displayed" ~active:(page_size<>None) ~packing:vbox#add () in
  let entry_page_size = GEdit.entry ?text:page_size ~editable:(page_size<>None) ~width_chars:10 ~packing:vbox#add () in
  check_page_size#connect#toggled ~callback:(fun () -> entry_page_size#set_editable check_page_size#active);

  let action_area = w#action_area in
  let button_cancel = GButton.button ~stock:`CANCEL ~packing:action_area#add () in
  let button_ok = GButton.button ~stock:`OK ~packing:action_area#add () in
  button_cancel#connect#clicked ~callback:w#destroy;
  button_ok#connect#clicked ~callback:(fun () ->
    try
      let sort_fun =
	if check_lexicalorder#active
	then sort_by_feature
	else sort_by_count in
      let page_size =
	if check_page_size#active
	then int_of_string entry_page_size#text
	else max_int in
      sv # set_links_sort_fun sort_fun;
      sv # set_links_page_size page_size;
      List.iter navig#expand_row paths;
      navig # refresh true;
      w#destroy ()
    with _ -> ());
  w#show ()

let menu_expand_with_options f () =
  let paths = navig # selected_paths in
  let sv =
    match paths with
    | [] -> navig # state_view
    | path::_ -> navig # state_view_from_path path in
  f sv;
  List.iter navig#expand_row paths;
  navig#refresh true

let menu_collapse () =
  let paths = navig # selected_paths in
  List.iter navig#collapse_row paths

let menu_pivot_uri uri =
  menu_reset (Lisql.AST.focus_of_uri uri)

let menu_and_list fs =
  Logui.item Logui.uri_AllOfFeaturesSelection (fun obs ->
    List.map Logui.feature_feature fs);
  menu_cd (Lisql.Feature.focus_insert_list history#store fs history#current#place#focus)

let menu_or_list fs =
  Logui.item Logui.uri_OneOfFeaturesSelection (fun obs ->
    List.map Logui.feature_feature fs);
  menu_cd (Lisql.Feature.focus_insert_list_or history#store fs history#current#place#focus)

let menu_not_list fs =
  Logui.item Logui.uri_NoneOfFeaturesSelection (fun obs ->
    List.map Logui.feature_feature fs);
  menu_cd (Lisql.Feature.focus_insert_list_not history#store fs history#current#place#focus)


let menu_join kind lf =
  let open Lisql.Feature in
  uri_dialog history#store
    ~title:
    ( match kind with
    | Kind_Class -> "Creation of a class"
    | Kind_Property
    | Kind_InverseProperty -> "Creation of a property"
    | _ -> assert false)
    ~callback:(fun uri ->
      let f0 =
	match kind with
	| Kind_Class -> Lisql.AST.has_type uri
	| Kind_Property -> Lisql.AST.make_role uri `Subject
	| Kind_InverseProperty -> Lisql.AST.make_role uri `Object
	| _ -> assert false in
      List.iter (fun f -> history#store#add_axiom f f0) lf;
      refresh_force ())
    ()

let menu_view_obj name () =
  Tarpit.effect (fun obs ->
    let descr = history#store#description ~obs name in
    let markup = make_markup (Lisql.Display.of_s1 ~obs history#store descr) in
    let w = GWindow.dialog
	~destroy_with_parent:true
	~modal:false
	~deletable:true
	~title:"Object description"
	~width:300 ~height:600 ~allow_shrink:true
	() in
    let vbox = w#vbox in
    let sw = GBin.scrolled_window
	~shadow_type:`NONE
	~placement:`TOP_LEFT
	~hpolicy:`AUTOMATIC
	~vpolicy:`AUTOMATIC
	~packing:(vbox#pack ~expand:true ~fill:true)
	() in
    let label = GMisc.label ~markup ~selectable:true ~xalign:0. ~yalign:0. ~packing:sw#add_with_viewport () in
    let action_area = w#action_area in
    let button_ok = GButton.button ~stock:`CLOSE ~packing:action_area#add () in
    button_ok#connect#clicked ~callback:w#destroy;
    w#show ())


let ctx_menu_clicked lx ctx_menu =
print_endline "menu_clicked";
List.iter (fun x -> print_endline x#string; Option.iter print_endline x#uri_opt) lx;
  let nb_selected = List.length lx in
  let selected_features = List.map (fun x -> x#feature) lx in
  let selected_names = Common.mapfilter (fun x -> x#name_opt) lx in
  let selected_uris = Common.mapfilter (fun x -> x#uri_opt) lx in
  let selected_prop_uris = Common.mapfilter (fun x -> x#prop_uri_opt) lx in
  let selected_classes = Common.mapfilter (fun x -> x#p1_opt) lx in
  let selected_uri = match lx with [x] -> x#uri_opt | _ -> None in
  let selected_prop_uri = match lx with [x] -> x#prop_uri_opt | _ -> None in
  let selected_name = match lx with [x] -> x#name_opt | _ -> None in
  let selected_lisql = match lx with [x] -> x#lisql_opt | _ -> None in
  let selected_image = Option.fold (fun name -> picture_of_name ~obs:Tarpit.blind_observer name) None selected_name in
  let selected_kinds =
    List.fold_left
      ~f:(fun res x -> LSet.add x#kind res)
      ~init:(LSet.empty ())
      lx in

  let ctx_menu_open_name = GMenu.menu_item ~label:"Open resource"
      ~show:(nb_selected = 1 && selected_name <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_open_image = GMenu.menu_item ~label:"Open image"
      ~show:(nb_selected = 1 && selected_image <> None && selected_kinds = [Lisql.Feature.Kind_Entity])
      ~packing:ctx_menu#append () in
  let ctx_menu_open_lisql = GMenu.menu_item ~label:"Open query"
      ~show:(nb_selected = 1 && selected_lisql <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_view = GMenu.menu_item ~label:"View description"
      ~show:(nb_selected = 1 && selected_name <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_define_label = GMenu.menu_item ~label:"Define label..."
      ~show:(nb_selected = 1 && selected_uri <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_define_inverse_label = GMenu.menu_item ~label:"Define inverse label..."
      ~show:(nb_selected = 1 && selected_prop_uri <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_define_ns = GMenu.menu_item ~label:"Define namespace..."
      ~show:(nb_selected = 1 && selected_uri <> None)
      ~packing:ctx_menu#append () in
  let ctx_menu_toggle_symmetric = GMenu.menu_item ~label:"Toggle symmetricity"
      ~show:(selected_prop_uris <> [])
      ~packing:ctx_menu#append () in
  let ctx_menu_toggle_transitive = GMenu.menu_item ~label:"Toggle transitiveness"
      ~show:(selected_prop_uris <> [])
      ~packing:ctx_menu#append () in
  let ctx_menu_super_class = GMenu.menu_item ~label:"Create a superclass..."
      ~show:(selected_kinds = [Lisql.Feature.Kind_Class])
      ~packing:ctx_menu#append () in
  let ctx_menu_super_property = GMenu.menu_item ~label:"Create a superproperty..."
      ~show:(selected_kinds = [Lisql.Feature.Kind_Property])
      ~packing:ctx_menu#append () in
  let ctx_menu_super_inverse_property = GMenu.menu_item ~label:"Create a superproperty..."
      ~show:(selected_kinds = [Lisql.Feature.Kind_InverseProperty])
      ~packing:ctx_menu#append () in
  let ctx_menu_import_uris = GMenu.menu_item ~label:"Import linked data"
    ~show:(selected_uris <> [])
    ~packing:ctx_menu#append () in
  let ctx_menu_delete_names = GMenu.menu_item ~label:"Delete resources"
    ~show:(selected_names <> [] && List.for_all (fun kind -> List.mem kind [Lisql.Feature.Kind_Entity; Lisql.Feature.Kind_Literal]) selected_kinds)
    ~packing:ctx_menu#append () in
  if selected_uris <> [] || selected_uri <> None || selected_name <> None || selected_image <> None || selected_lisql <> None then
    ignore (GMenu.separator_item ~packing:ctx_menu#append ());
  let ctx_menu_uri =
    let label =
      match selected_kinds with
      | [Lisql.Feature.Kind_Entity] -> "<this entity>"
      | [Lisql.Feature.Kind_Class] -> "<this class>"
      | [Lisql.Feature.Kind_Property] | [Lisql.Feature.Kind_InverseProperty] -> "<this property>"
      | _ -> "" in
    GMenu.menu_item ~label ~show:(nb_selected = 1 && label <> "") ~packing:ctx_menu#append () in
  let ctx_menu_and = GMenu.menu_item ~label:"_ and <all of those>" ~show:(nb_selected <> 1) ~packing:ctx_menu#append () in
  let ctx_menu_or = GMenu.menu_item ~label:"_ and <one of those>" ~show:(nb_selected <> 1) ~packing:ctx_menu#append () in
  let ctx_menu_not = GMenu.menu_item ~label:"_ and <none of those>" ~show:(nb_selected <> 0) ~packing:ctx_menu#append () in
  let _ = GMenu.separator_item ~packing:ctx_menu#append () in
  let ctx_menu_expand = GMenu.menu_item ~label:"Expand..." ~packing:ctx_menu#append () in
  let ctx_menu_collapse = GMenu.menu_item ~label:"Collapse" ~packing:ctx_menu#append () in
  let ctx_sep4 = GMenu.separator_item ~packing:ctx_menu#append () in
  let ctx_menu_copy_features = GMenu.menu_item ~label:"Copy" ~packing:ctx_menu#append () in
  let ctx_menu_paste_features = GMenu.menu_item ~label:"Paste" ~packing:ctx_menu#append () in
  let ctx_menu_paste_not_features = GMenu.menu_item ~label:"Paste not" ~packing:ctx_menu#append () in
  
  ctx_menu_uri#connect#activate ~callback:(fun () -> Option.iter menu_pivot_uri selected_uri);
  ctx_menu_and#connect#activate ~callback:(fun () -> menu_and_list selected_features);
  ctx_menu_or#connect#activate ~callback:(fun () -> menu_or_list selected_features);
  ctx_menu_not#connect#activate ~callback:(fun () -> menu_not_list selected_features);
  ctx_menu_expand#connect#activate ~callback:menu_expand;
  ctx_menu_collapse#connect#activate ~callback:menu_collapse;
  ctx_menu_open_name#connect#activate
    ~callback:(fun () -> Option.iter open_name selected_name);
  ctx_menu_open_image#connect#activate
    ~callback:(fun () -> Option.iter open_url selected_image);
  ctx_menu_open_lisql#connect#activate
    ~callback:(fun () ->
      match selected_lisql with
      | Some (_n,_s,foc) ->
	  menu_reset (Lisql.AST.focus_first_postfix ~filter:Lisql.Transf.focus_default_filter foc);
	history#current#place#remove_as_draft
      | None -> error_message "Invalid query: cannot be opened");
  ctx_menu_view#connect#activate
    ~callback:(fun () -> Option.iter (fun name -> menu_view_obj name ()) selected_name);
  ctx_menu_define_label#connect#activate
    ~callback:(fun () -> Option.iter (fun uri -> menu_define_label Rdfs.uri_label "en" uri ()) selected_uri);
  ctx_menu_define_inverse_label#connect#activate
    ~callback:(fun () -> Option.iter (fun uri -> menu_define_label Lisql.Namespace.uri_inverseLabel "en" uri ()) selected_uri);
  ctx_menu_define_ns#connect#activate
    ~callback:(fun () -> Option.iter (fun uri -> menu_define_ns uri ()) selected_uri);
  ctx_menu_toggle_transitive#connect#activate
    ~callback:(fun () -> List.iter menu_toggle_transitive selected_prop_uris);
  ctx_menu_toggle_symmetric#connect#activate
    ~callback:(fun () -> List.iter menu_toggle_symmetric selected_prop_uris);
  ctx_menu_super_class#connect#activate
    ~callback:(fun () -> menu_join Lisql.Feature.Kind_Class selected_classes);
  ctx_menu_super_property#connect#activate
    ~callback:(fun () -> menu_join Lisql.Feature.Kind_Property selected_classes);
  ctx_menu_super_inverse_property#connect#activate
    ~callback:(fun () -> menu_join Lisql.Feature.Kind_InverseProperty selected_classes);
  ctx_menu_import_uris#connect#activate
    ~callback:(fun () -> import_uris selected_uris);
  ctx_menu_delete_names#connect#activate
    ~callback:(fun () -> delete_names selected_names);
  ctx_menu_copy_features#connect#activate ~callback:(fun () -> navig#copy_features);
  ctx_menu_paste_features#connect#activate ~callback:menu_paste;
  ctx_menu_paste_not_features#connect#activate ~callback:menu_paste;
  ()


(*
   let menu_delete_objects () =
   let oids = extent#selected_oids in
   if question "Are you sure you want to delete the selected objects ?"
   then begin
   cb_copy := Nothing;
   awaken
   (fun () ->
   Ext.iter (fun oid -> try history#store#move_obj ~src:!current_src oid Lisql.bottom with _ -> ()) oids)
   refresh_force
   end
 *)

let menu_run_command () =
  try
    history#current#place#do_run;
    Logui.item Logui.uri_Run (fun obs -> [Logui.feature_assertion history#current#place#assertion]);
    refresh true
  with exn -> error_message (Printexc.to_string exn)

let menu_set_home_query () =
  history#current#place#set_as_home_query

let menu_add_bookmark () =
  history#current#place#add_as_bookmark

let menu_add_draft () =
  history#current#place#add_as_draft

let menu_define_struct () =
  Lisql.Transf.focus_define_struct
    (fun ~arity ~implicit k ->
      functor_dialog history#store
	~title:"Creation of a functor"
	~arity
	~implicit
	~callback:(fun (uri,_ar,_foc) -> k uri)
	())
    ~callback:menu_cd
    history#current#place#focus


let query_menu_clicked query query_menu =
  let query_menu_factory = new GMenu.factory query_menu in
  let cmd_run = query_menu_factory#add_item "Assert/Run" in
  let _ = query_menu_factory#add_separator () in
  let cmd_set_home = query_menu_factory#add_item "Define as the home query" in
  let cmd_add_bookmark = query_menu_factory#add_item "Add to bookmarks" in
  let cmd_add_draft = query_menu_factory#add_item "Add to drafts" in
  let cmd_define_struct = query_menu_factory#add_item "Define a structure..." in
  cmd_run#connect#activate ~callback:menu_run_command;
  cmd_set_home#connect#activate ~callback:menu_set_home_query;
  cmd_add_bookmark#connect#activate ~callback:menu_add_bookmark;
  cmd_add_draft#connect#activate ~callback:menu_add_draft;
  cmd_define_struct#connect#activate ~callback:menu_define_struct;
  ()

let answer_menu_clicked cell ev =
  let button = GdkEvent.Button.button ev in
  let ev_type = GdkEvent.get_type ev in
  if button = 1 && ev_type = `TWO_BUTTON_PRESS then begin
    match cell#name_opt with
    | Some n ->
	error_message "Don't know at which focus to insert this.";
	true
    | None -> false end
  else if button = 3 then begin
    match cell#display with
    | [`URI (uri,kind,s,img_opt)] ->
	let ctx_menu = GMenu.menu () in
	let ctx_menu_open_url = GMenu.menu_item ~label:"Open resource" ~packing:ctx_menu#append () in
	let ctx_menu_open_image = GMenu.menu_item ~label:"Open image" ~show:(img_opt <> None) ~packing:ctx_menu#append () in
	let ctx_menu_view = GMenu.menu_item ~label:"View description" ~packing:ctx_menu#append () in
	let ctx_menu_define_label = GMenu.menu_item ~label:"Define label..." ~packing:ctx_menu#append () in
	let ctx_menu_define_ns = GMenu.menu_item ~label:"Define namespace..." ~packing:ctx_menu#append () in
	let ctx_menu_import_uri = GMenu.menu_item ~label:"Import linked data" ~packing:ctx_menu#append () in
	let _ = GMenu.separator_item ~packing:ctx_menu#append () in
	let ctx_menu_uri = GMenu.menu_item ~label:"<this entity>" ~packing:ctx_menu#append () in
	ctx_menu_open_url#connect#activate
	  ~callback:(fun () -> open_url uri);
	ctx_menu_open_image#connect#activate
	  ~callback:(fun () -> Option.iter open_url img_opt);
	ctx_menu_view#connect#activate
	  ~callback:(fun () -> menu_view_obj (Rdf.URI uri) ());
	ctx_menu_define_label#connect#activate
	  ~callback:(fun () -> menu_define_label Rdfs.uri_label "en" uri ());
	ctx_menu_define_ns#connect#activate
	  ~callback:(fun () -> menu_define_ns uri ());
	ctx_menu_import_uri#connect#activate
	  ~callback:(fun () -> import_uris [uri]);
	ctx_menu_uri#connect#activate
	  ~callback:(fun () -> menu_pivot_uri uri);
	ctx_menu#popup ~button ~time:(GdkEvent.Button.time ev);
	true
    | _ -> false end
  else false

(* Building the GUI *)

class navig_callbacks =
  object
    method insert logui_uri x = menu_insert logui_uri x
    method name () = menu_name ()
    method transf k = menu_transf k
  end

let main () =
  window#set_title (utf (title ()));

  (* callbacks *)
  window#event#connect#delete ~callback:(fun _ -> menu_quit (); true) (*fun () -> Context.init ""; GMain.quit ()*);

  window#event#connect#key_press ~callback:(fun key ->
    let keyval = GdkEvent.Key.keyval key in
    let modifs = GdkEvent.Key.state key in
    if keyval = _Tab then(* && List.mem `CONTROL modifs && not (List.mem `SHIFT modifs) *)
      begin menu_transf Lisql.Transf.FocusTab; true end
    else if keyval = _Right && List.mem `CONTROL modifs then
      begin menu_transf Lisql.Transf.FocusRight; true end
    else if keyval = _Left && List.mem `CONTROL modifs then
      begin menu_transf Lisql.Transf.FocusLeft; true end
    else if keyval = _Up && List.mem `CONTROL modifs then
      begin menu_transf Lisql.Transf.FocusUp; true end
    else if keyval = _Down && List.mem `CONTROL modifs then
      begin menu_transf Lisql.Transf.FocusDown; true end
    else false);

  cmd_new#connect#activate ~callback:menu_new;
  cmd_open#connect#activate ~callback:menu_open;
  cmd_save#connect#activate ~callback:menu_save;
  cmd_import_from_rdf#connect#activate ~callback:menu_import_rdf;
(*  cmd_import_from_xml#connect#activate ~callback:menu_import_xml; *)
  cmd_import_from_uri#connect#activate ~callback:menu_import_uri;
  cmd_import_from_path#connect#activate ~callback:menu_import_path;
  cmd_export_to_rdf#connect#activate ~callback:menu_export_rdf;
  cmd_stat#connect#activate ~callback:menu_stat;
  cmd_quit#connect#activate ~callback:menu_quit;

  cmd_back#connect#activate ~callback:menu_back;
  cmd_back#add_accelerator ~group:accel_group ~modi:[`CONTROL] _b;
  cmd_fwd#connect#activate ~callback:menu_fwd;
  cmd_fwd#add_accelerator ~group:accel_group ~modi:[`CONTROL] _f;
  cmd_refresh#connect#activate ~callback:menu_refresh;
  cmd_refresh#add_accelerator ~group:accel_group ~modi:[`CONTROL] _r;
  cmd_root#connect#activate ~callback:menu_root;
  cmd_root#add_accelerator ~group:accel_group ~modi:[`CONTROL] _0;
  cmd_home#connect#activate ~callback:menu_home;
  cmd_bookmarks#connect#activate ~callback:menu_bookmarks;
  cmd_drafts#connect#activate ~callback:menu_drafts;
  cmd_give_more#connect#activate ~callback:menu_give_more;
  cmd_give_more#add_accelerator ~group:accel_group ~modi:[`CONTROL] _plus;
  cmd_give_less#connect#activate ~callback:menu_give_less;
  cmd_give_less#add_accelerator ~group:accel_group ~modi:[`CONTROL] _minus;
  check_scale#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_feat_color#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_lift#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_lift_color#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_ext_color#connect#activate ~callback:(fun () -> refresh false); (* only graphical refresh *)
  check_int_preselect#connect#activate ~callback:(fun () -> navig # refresh_preselect false); (* only preselection refresh *)
  cmd_expand#connect#activate ~callback:menu_expand;
  cmd_collapse#connect#activate ~callback:menu_collapse;

  cmd_run#connect#activate ~callback:menu_run_command;
  cmd_run#add_accelerator ~group:accel_group ~modi:[`CONTROL] _x;
  cmd_set_home#connect#activate ~callback:menu_set_home_query;
  cmd_add_bookmark#connect#activate ~callback:menu_add_bookmark;
  cmd_add_draft#connect#activate ~callback:menu_add_draft;
(*      cmd_delete#connect#activate ~callback:(fun () -> menu_delete_objects ()); *)

  cmd_about#connect#activate ~callback:(fun () -> info_message
      ~title:"About Sewelis"
(*	  ~icon:(pixbuf_of_picture ~maxside:30 "/local/ferre/prog/ocaml/camelis/bin/lis.gif") *)
      "Sewelis\n\n\
      An implementation of Logical Information Systems (LIS)\n\
      for browsing and editing Semantic Web data\n\
    developped by Sebastien Ferre (ferre@irisa.fr).");

    button_back#connect#clicked ~callback:menu_back;
  tooltips#set_tip button_back#coerce ~text:"Go backward in the history of queries";
  button_fwd#connect#clicked ~callback:menu_fwd;
  tooltips#set_tip button_fwd#coerce ~text:"Go forward in the history of queries";
  button_refresh#connect#clicked ~callback:menu_refresh;
  tooltips#set_tip button_refresh#coerce ~text:"Refresh the navigation and extent windows";
  button_root#connect#clicked ~callback:menu_root;
  tooltips#set_tip button_root#coerce ~text:"Go to the root query";
  button_home#connect#clicked ~callback:menu_home;
  tooltips#set_tip button_home#coerce ~text:"Go to the home query";
  button_bookmarks#connect#clicked ~callback:menu_bookmarks;
  tooltips#set_tip button_bookmarks#coerce ~text:"Go to the list of bookmarked queries";
  button_drafts#connect#clicked ~callback:menu_drafts;
  tooltips#set_tip button_drafts#coerce ~text:"Go to the list of drafts";
  button_run#connect#clicked ~callback:menu_run_command;
  tooltips#set_tip button_run#coerce ~text:"Assert/Execute the current statement/command";

  wq#connect
    (object
      inherit navig_callbacks
      method ctx_menu = query_menu_clicked
      method focus foc = menu_cd (Some foc)
    end);

  ml#connect
    (object
      method give_least = menu_give_least ()
      method give_less = menu_give_less ()
      method give_more = menu_give_more ()
      method give_most = menu_give_most ()
    end);

  let re_var = Str.regexp "^[?]\\([^ \t].*\\)$" in
  let re_a = Str.regexp "^[ \t]*an?[ \t]+\\([^?]*[^ \t]\\)[ \t]*$" in
  let re_fwd = Str.regexp "^[ \t]*has[ \t]+\\([^?]*[^ \t]\\)[ \t?]*$" in
  let re_bwd = Str.regexp "^[ \t]*is[ \t]+\\([^?]*[^ \t]\\)[ \t]+of[ \t?]*$" in
  let re_text = Str.regexp "^[ \t]*[\"]\\(.*\\)$" in
  let re_funct = Str.regexp "^[ \t]*\\([^(]+\\)(" in
  let re_funct_intransitive = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+with[ \t?]*$" in
  let re_funct_transitive = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+[?][ \t]+with[ \t?]*$" in
  let re_funct_infix = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_infix_arg1 = Str.regexp "^[ \t]*this[ \t]+\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_infix_arg2 = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]+this[ \t]*$" in
  let re_funct_prefix = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+[?][ \t]*$" in
  let re_funct_prefix_arg1 = Str.regexp "^[ \t]*\\([^?]*[^? \t]\\)[ \t]+this[ \t]*$" in
  let re_funct_postfix = Str.regexp "^[ \t]*[?][ \t]+\\([^?]*[^? \t]\\)[ \t]*$" in
  let re_funct_postfix_arg1 = Str.regexp "^[ \t]*this[ \t]+\\([^?]*[^? \t]\\)[ \t]*$" in

  let ef_callbacks =
    object (self)
      inherit navig_callbacks

      method input ~mode ~custom =
	let foc = history#current#place#focus in
	try
	  if custom = "" then menu_transf Lisql.Transf.FocusUp
	  else if custom = "for" || custom = "foreach" || custom = "for each" then menu_transf Lisql.Transf.InsertForEach
	  else if custom = "if" then menu_transf Lisql.Transf.InsertCond
	  else if custom = ";" then menu_transf Lisql.Transf.InsertSeq
	  else if custom = "and" then menu_transf Lisql.Transf.InsertAnd
	  else if custom = "or" then menu_transf Lisql.Transf.InsertOr
	  else if custom = "and not" then menu_transf Lisql.Transf.InsertAndNot
	  else if custom = "and maybe" then menu_transf Lisql.Transf.InsertAndMaybe
	  else if custom = "not" then menu_transf Lisql.Transf.ToggleNot
	  else if custom = "maybe" then menu_transf Lisql.Transf.ToggleMaybe
(*	  else if custom = "quote" then menu_transf Lisql.Transf.ToggleQuote *)
	  else if custom = "is" then menu_transf Lisql.Transf.InsertIsThere
	  else if custom = "a" || custom = "an" then menu_transf (Lisql.Transf.ToggleQu Lisql.AST.An)
	  else if custom = "each" then menu_transf (Lisql.Transf.ToggleQu Lisql.AST.Each)
	  else if custom = "every" then menu_transf (Lisql.Transf.ToggleQu Lisql.AST.Every)
	  else if custom = "only" then menu_transf (Lisql.Transf.ToggleQu Lisql.AST.Only)
	  else if custom = "no" then menu_transf (Lisql.Transf.ToggleQu Lisql.AST.No)
	  else if custom = "opt" then menu_transf Lisql.Transf.ToggleOpt
	  else if custom = "trans" then menu_transf Lisql.Transf.ToggleTrans
	  else if custom = "sym" then menu_transf Lisql.Transf.ToggleSym
	  else if custom = "?" then menu_name ()
	  else if custom = "select" || custom = "sel" then menu_transf Lisql.Transf.Select
	  else if custom = "delete" || custom = "del" then menu_transf Lisql.Transf.Delete
	  else begin
	    try
	      let x = new Lisql.Feature.feature_name history#store
		  (Lisql.Syntax.name_of_string ~ctx:(history#store :> Lisql.Syntax.context) custom) in
	      self#insert Logui.uri_NameEntry x
	    with _ ->
	      let kind =
		if Str.string_match re_var custom 0 then `Var (Str.matched_group 1 custom)
		else if Str.string_match re_a custom 0 then `A (Str.matched_group 1 custom)
		else if Str.string_match re_fwd custom 0 then `Fwd (Str.matched_group 1 custom)
		else if Str.string_match re_bwd custom 0 then `Bwd (Str.matched_group 1 custom)
		else if Str.string_match re_text custom 0 then `Text (Str.matched_group 1 custom)
		else if Str.string_match re_funct custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_Functor)
		else if Str.string_match re_funct_intransitive custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_IntransitiveVerb)
		else if Str.string_match re_funct_transitive custom 0 then `Funct (Str.matched_group 1 custom, 2, 1, Term.uri_TransitiveVerb)
		else if Str.string_match re_funct_infix custom 0 then `Funct (Str.matched_group 1 custom, 2, 0, Term.uri_InfixOperator)
		else if Str.string_match re_funct_infix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 2, 1, Term.uri_InfixOperator)
		else if Str.string_match re_funct_infix_arg2 custom 0 then `Funct (Str.matched_group 1 custom, 2, 2, Term.uri_InfixOperator)
		else if Str.string_match re_funct_prefix custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_PrefixOperator)
		else if Str.string_match re_funct_prefix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_PrefixOperator)
		else if Str.string_match re_funct_postfix custom 0 then `Funct (Str.matched_group 1 custom, 1, 0, Term.uri_PostfixOperator)
		else if Str.string_match re_funct_postfix_arg1 custom 0 then `Funct (Str.matched_group 1 custom, 1, 1, Term.uri_PostfixOperator)
		else `Other (custom) in
	      match kind with
	      | `Var name ->
		  var_dialog
		    ~title:"Creation of a variable"
		    ~name
		    ~callback:(fun name ->
		      let x = new Lisql.Feature.feature_some history#store name in
		      self#insert Logui.uri_VariableEntry x)
		    ()
	      | `A name ->
		  class_dialog history#store
		    ~title:"Creation of a class"
		    ~label:name
		    ~callback:(fun uri ->
		      let x = new Lisql.Feature.feature_type history#store uri in
		      self#insert Logui.uri_ClassEntry x)
		    ()
	      | `Fwd name ->
		  property_dialog history#store
		    ~title:"Creation of a property"
		    ~label:name
		    ~callback:(fun uri ->
		      let x = new Lisql.Feature.feature_role history#store `Subject uri in
		      self#insert Logui.uri_ForwardPropertyEntry x)
		    ()
	      | `Bwd name ->
		  property_dialog history#store
		    ~title:"Creation of an inverse property"
		    ~label:name
		    ~callback:(fun uri ->
		      let x = new Lisql.Feature.feature_role history#store `Object uri in
		      self#insert Logui.uri_BackwardPropertyEntry x)
		    ()
	      | `Text text ->
		  ( match GToolbox.input_text ~title:"Creation of a text" ~text:text "Enter a text" with
		  | None -> ()
		  | Some text ->
		      let x = new Lisql.Feature.feature_name history#store (Name.plain_literal text "en") in
		      self#insert Logui.uri_TextCreation x)
	      | `Funct (name, arity, focus, functor_type) ->
		  functor_dialog history#store
		    ~title:"Creation of a functor"
		    ~label:name
		    ~arity:arity
		    ~focus:focus
		    ~functor_type:functor_type
		    ~callback:(fun (uri_funct,args_arity,focus) ->
		      let x = new Lisql.Feature.feature_funct history#store uri_funct args_arity focus in
		      self#insert Logui.uri_StructureEntry x)
		    ()
	      | `Other text ->
		  uri_dialog history#store
		    ~title:"Creation of an entity"
		    ~label:text
		    ~callback:(fun uri ->
		      let x = new Lisql.Feature.feature_name history#store (Rdf.URI uri) in
		      self#insert Logui.uri_EntityEntry x)
		    ()
	  end;
	  true
	with exn ->
	  error_message ("This is not a valid navigation link: " (* ^ Printexc.to_string exn *));
	  false
    end in
  focus_ef#connect ef_callbacks;
  ef#connect ef_callbacks;

  navig#connect
    (object
      method row_activated path =
	let x = (navig#incr_from_path path)#feature in
	menu_insert Logui.uri_SingleFeatureSelection x

      method refresh_preselect _ = ()

      method ctx_menu = ctx_menu_clicked

      method data_received () = menu_paste ()
    end);

  ans#connect
    (object
      method row_activated line =
	Logui.item Logui.uri_AnswerSelection (fun obs -> []); (* TODO: line description as feature list *)
	let subst = List.combine history#current#place#answers#columns line in
	let subst = Common.mapfilter (function (v,None) -> None | (v,Some n) -> Some (v,n)) subst in
	menu_cd (Lisql.Transf.focus_subst subst history#current#place#focus)

      method ctx_menu = answer_menu_clicked
    end);

  window#set_default_size ~width:1000 ~height:800;
  window#add_accel_group accel_group;
  window#show ();
  paned_set_ratio paned_global 2 3;
  paned_set_ratio paned_intension 2 5;
  navig#set_ratio 1 2;
  List.iter
    (fun file ->
      try import_rdf_tree file
      with e ->
	print_string "Importing RDF: "; print_endline file;
	print_endline (Printexc.to_string e))
    (List.rev !rdf_files);
  prerr_endline "Gui: refresh all widgets...";
  awaken (fun () -> ()) refresh_force;
  GMain.main ()

let _ = main ()
