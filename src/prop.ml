(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

type ('a,'u) t =
  | Atom of 'a
  | And of ('a,'u) t * ('a,'u) t
  | Or of ('a,'u) t * ('a,'u) t
  | Maybe of ('a,'u) t
  | Not of ('a,'u) t
  | Cond of 'u * ('a,'u) t * ('a,'u) t option

let fold (f : 'b -> 'a -> 'b) (init : 'b) (p : ('a,'u) t) =
  let rec aux acc = function
    | Atom x -> f acc x
    | And (p1,p2) -> let acc1 = aux acc p1 in aux acc1 p2
    | Or (p1,p2) -> let acc1 = aux acc p1 in aux acc1 p2
    | Maybe p1 -> aux acc p1
    | Not p1 -> aux acc p1
    | Cond (c,p1,p2_opt) -> let acc1 = aux acc p1 in Option.fold (aux acc1) acc1 p2_opt
  in
  aux init p

let rec map (f : 'a -> 'b) : ('a,'u) t -> ('b,'u) t = function
  | Atom x -> Atom (f x)
  | And (p1,p2) -> And (map f p1, map f p2)
  | Or (p1,p2) -> Or (map f p1, map f p2)
  | Maybe p1 -> Maybe (map f p1)
  | Not p1 -> Not (map f p1)
  | Cond (c,p1,p2_opt) -> Cond (c, map f p1, Option.map (map f) p2_opt)

let rec compose_0 (f : 'a -> 'b) (g : 'u -> 'v) : ('a,'u) t -> 'b = function
  | Atom a -> f a
  | And (p1,p2) ->
      let q1 = compose_0 f g p1 in
      let q2 = compose_0 f g p2 in
      And (q1, q2)
  | Or (p1,p2) ->
      let q1 = compose_0 f g p1 in
      let q2 = compose_0 f g p2 in
      Or (q1, q2)
  | Maybe p1 ->
      let q1 = compose_0 f g p1 in
      Maybe q1
  | Not p1 ->
      let q1 = compose_0 f g p1 in
      Not q1
  | Cond (c,p1,p2_opt) ->
      let q1 = compose_0 f g p1 in
      let q2_opt = Option.map (compose_0 f g) p2_opt in
      Cond (g c, q1, q2_opt)

let rec compose_1 (f : 'a -> 'b -> 'c) (g : 'u -> 'v) : ('a,'u) t -> 'b -> 'c = function
  | Atom a -> f a
  | And (p1,p2) ->
      let q1 = compose_1 f g p1 in
      let q2 = compose_1 f g p2 in
      (fun x -> And (q1 x, q2 x))
  | Or (p1,p2) ->
      let q1 = compose_1 f g p1 in
      let q2 = compose_1 f g p2 in
      (fun x -> Or (q1 x, q2 x))
  | Maybe p1 ->
      let q1 = compose_1 f g p1 in
      (fun x -> Maybe (q1 x))
  | Not p1 ->
      let q1 = compose_1 f g p1 in
      (fun x -> Not (q1 x))
  | Cond (c,p1,p2_opt) ->
      let q1 = compose_1 f g p1 in
      let q2_opt = Option.map (compose_1 f g) p2_opt in
      (fun x -> Cond (g c, q1 x, Option.apply_1 q2_opt x))

let rec compose_2 (f : 'a -> 'b -> 'c -> 'd) (g : 'u -> 'v) : ('a,'u) t -> 'b -> 'c -> 'd = function
  | Atom a -> f a
  | And (p1,p2) ->
      let q1 = compose_2 f g p1 in
      let q2 = compose_2 f g p2 in
      (fun x y -> And (q1 x y, q2 x y))
  | Or (p1,p2) ->
      let q1 = compose_2 f g p1 in
      let q2 = compose_2 f g p2 in
      (fun x y -> Or (q1 x y, q2 x y))
  | Maybe p1 ->
      let q1 = compose_2 f g p1 in
      (fun x y -> Maybe (q1 x y))
  | Not p1 ->
      let q1 = compose_2 f g p1 in
      (fun x y -> Not (q1 x y))
  | Cond (c,p1,p2_opt) ->
      let q1 = compose_2 f g p1 in
      let q2_opt = Option.map (compose_2 f g) p2_opt in
      (fun x y -> Cond (g c, q1 x y, Option.apply_2 q2_opt x y))

let rec compose_3 (f : 'a -> 'b -> 'c -> 'd -> 'e) (g : 'u -> 'v) : ('a,'u) t -> 'b -> 'c -> 'd -> 'e = function
  | Atom a -> f a
  | And (p1,p2) ->
      let q1 = compose_3 f g p1 in
      let q2 = compose_3 f g p2 in
      (fun x y z -> And (q1 x y z, q2 x y z))
  | Or (p1,p2) ->
      let q1 = compose_3 f g p1 in
      let q2 = compose_3 f g p2 in
      (fun x y z -> Or (q1 x y z, q2 x y z))
  | Maybe p1 ->
      let q1 = compose_3 f g p1 in
      (fun x y z -> Maybe (q1 x y z))
  | Not p1 ->
      let q1 = compose_3 f g p1 in
      (fun x y z -> Not (q1 x y z))
  | Cond (c, p1,p2_opt) ->
      let q1 = compose_3 f g p1 in
      let q2_opt = Option.map (compose_3 f g) p2_opt in
      (fun x y z -> Cond (g c, q1 x y z, Option.apply_3 q2_opt x y z))

let rec compose_4 (f : 'a -> 'b -> 'c -> 'd -> 'e -> 'f) (g : 'u -> 'v) : ('a,'u) t -> 'b -> 'c -> 'd -> 'e -> 'f = function
  | Atom a -> f a
  | And (p1,p2) ->
      let q1 = compose_4 f g p1 in
      let q2 = compose_4 f g p2 in
      (fun x y z t -> And (q1 x y z t, q2 x y z t))
  | Or (p1,p2) ->
      let q1 = compose_4 f g p1 in
      let q2 = compose_4 f g p2 in
      (fun x y z t -> Or (q1 x y z t, q2 x y z t))
  | Maybe p1 ->
      let q1 = compose_4 f g p1 in
      (fun x y z t -> Maybe (q1 x y z t))
  | Not p1 ->
      let q1 = compose_4 f g p1 in
      (fun x y z t -> Not (q1 x y z t))
  | Cond (c, p1,p2_opt) ->
      let q1 = compose_4 f g p1 in
      let q2_opt = Option.map (compose_4 f g) p2_opt in
      (fun x y z t -> Cond (g c, q1 x y z t, Option.apply_4 q2_opt x y z t))
