(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Ext = Extension.Make
module Name = Name.Make

module Make =
  struct

    class type store =
      object
	inherit Ext.store
	method new_resource : Name.t
	method remove_entity : Name.t -> unit
	method copy_entity : Name.t -> Name.t -> unit
	method move_entity : Name.t -> Name.t -> unit
      end

    class virtual t =
      object (self)
	method virtual string : string
	method virtual run : store -> Ext.map -> Ext.map
(*	method virtual undo : store -> Ext.map -> Ext.map *)
      end

    let list_string = function
      | [] -> "skip"
      | lp -> String.concat "; " (List.map (fun p -> p#string) lp)
    let list_run store m lp =
      List.fold_left (fun m' p -> p#run store m') m lp
    let list_undo store m' lp =
      List.fold_right (fun p m -> p#undo store m) lp m'


    let instantiate store m v =
      try m, List.assoc v m
      with Not_found ->
	let x = store#new_resource in
	(v,x)::m, x

    let instantiate_list store m lv =
      List.fold_right
	(fun v (m1,lx) ->
	  let m2, x = instantiate store m1 v in
	  m2, x::lx)
	lv (m,[])

    class update (name : string) (f : Name.t list -> unit) (lv : Ext.var list) =
      object
	inherit t
	method string = name ^ "(" ^ String.concat "," lv ^ ")"
	method run store m =
	  let m', lx = instantiate_list store m lv in
	  f lx;
	  m'
      end
    let update name f lv = new update name f lv

    class remove (v : Ext.var) =
    object
      inherit t
      method string = "remove(" ^ v ^ ")"
      method run store m =
	try
	  let x = List.assoc v m in
	  store#remove_entity x; 
	  m
	with _ -> m (* the thing to be removed is not bound *)
    end
    let remove v = new remove v

    class copy (v1 : Ext.var) (v2 : Ext.var) =
    object
      inherit t
      method string = "copy(" ^ String.concat "," [v1;v2] ^ ")"
      method run store m =
	try
	  let x1 = List.assoc v1 m in
	  let x2 = List.assoc v2 m in
	  store#copy_entity x1 x2; 
	  m
	with _ -> m
    end
    let copy v1 v2 = new copy v1 v2

    class move (v1 : Ext.var) (v2 : Ext.var) =
    object
      inherit t
      method string = "move(" ^ String.concat "," [v1;v2] ^ ")"
      method run store m =
	try
	  let x1 = List.assoc v1 m in
	  let x2 = List.assoc v2 m in
	  store#move_entity x1 x2; 
	  m
	with _ -> m
    end
    let move v1 v2 = new move v1 v2

    class single (v : Ext.var) (n : Name.t) =
      object
	inherit t
	method string = v ^ ":=" ^ Name.contents n
	method run store m =
	  (v,n) :: List.remove_assoc v m
      end

    let single v n = (new single v n : t)

    class unify (v1 : Ext.var) (v2 : Ext.var) = (* SHOULD be a constraint or assignment *)
      object
	inherit t
	method string = v1 ^ " == " ^ v2
	method run store m =
	  try
	    let x1 = List.assoc v1 m in
	    try
	      let x2 = List.assoc v2 m in
	      if x1 = x2
	      then m
	      else failwith "some unification fails"
	    with Not_found ->
	      (v2,x1)::m
	  with Not_found ->
	    try
	      let x2 = List.assoc v2 m in
	      (v1,x2)::m
	    with Not_found ->
	      prerr_endline "Extension: open unification not handled.";
	      m

      end

    let unify v1 v2 = (new unify v1 v2 : t)

    class cond (c : Ext.constr) (lp1 : t list) (lp2 : t list) =
      object
	inherit t
(*	val mutable cond = false *)
	method string = "if " ^ c#string ^ " then " ^ list_string lp1 ^ " else " ^ list_string lp2 ^ " endif"
	method run store m =
	  if c#succeeds (store :> Ext.store) m
	  then list_run store m lp1
	  else list_run store m lp2
(*
	  try
	    let m' = ext#choose store m in
	    cond <- true;
	    list_run store m' lp1
	  with Not_found ->
	    list_run store m lp2
*)
(*
	method undo store m =
	  if cond
	  then list_undo store m lp1
	  else list_undo store m lp2
*)
      end

    let cond ext p1 p2 = (new cond ext p1 p2 : t)

    class foreach (e : Ext.t) (lp : t list) =
      object
	inherit t
	method string = "foreach " ^ e#string ^ " do " ^ list_string lp ^ " done"
	method run store m =
	  if lp = []
	  then m
	  else
	    let _ = e#fold (store :> Ext.store) (fun () m' -> ignore (list_run store m' lp)) () m in
	    m
(*
	method undo store m =
	  let _ = e#fold (store :> Ext.store) (fun () m' -> ignore (list_undo store m' lp)) () m in
	  m
*)
      end

    let foreach e lp = (new foreach e lp : t)

(* -------------------------------------------------------------*)

(*
    class ['a] quote (other_src : Ext.oid t) (p : 'a t) =
      object
	inherit ['a] t
	val mutable o_src = 0
	method run ~src store m =
	  let x, m' = other_src#run ~src store m in
	  o_src <- x;
	  p#run ~src:x store m'
	method undo ~src store m =
	  let res, m' = p#undo ~src:o_src store m in
	  let _, m'' = other_src#undo ~src store m' in
	  res, m''
      end

    let quote other_src p = (new quote other_src p : 'a t)
*)


(*
    class add r (largs : (Ext.var * Ext.var) list) =
      object
	inherit t
	method string = "add(" ^ r#uri ^ ": " ^ String.concat "," (List.map (fun (arg,v) -> arg ^ "=" ^ v) largs) ^ ")"
	method run store m =
	  let args, m' =
	    List.fold_left
	      (fun (args,m1) (arg,v) ->
		let x, m2 =
		  try List.assoc v m1, m1
		  with Not_found ->
		    let x = store#new_resource in x, (v,x)::m1 in
		(arg,x)::args, m2)
	      ([],m) largs in
	  r#add args;
	  m'
(*
	method undo store m =
	  let vxs = List.map (fun (v,e) -> (v,store#get_entity (e#eval store m))) le in
	  if effective then ignore (r#remove vxs);
	  m
*)
      end

    let add r largs = (new add r largs : t)

    class remove r (largs : (Ext.var * Ext.var) list) =
      object
	inherit t
	method string = "remove(" ^ r#uri ^ ": " ^ String.concat "," (List.map (fun (arg,v) -> arg ^ "=" ^ v) largs) ^ ")"
	method run store m =
	  let args = List.map (fun (arg,v) -> (arg, List.assoc v m)) largs in
	  r#remove args;
	  m
(*
	method undo store m =
	  let vxs = List.map (fun (v,e) -> (v,store#get_entity (e#eval store m))) le in
	  if effective then ignore (r#add vxs);
	  m
*)
      end

    let remove r le = (new remove r le : t)
*)

    class equal (v : Ext.var) (expr : Ext.expr) =
      object
	inherit t
	method string = v ^ "=" ^ expr#string
	method run store m =
	  expr#fold (store :> Ext.store)
	    (fun _ m1 n ->
	      (v,n)::List.remove_assoc v m1)
	    m m
(*
	method undo store m =
	  List.remove_assoc v m
*)
      end

    let equal v expr = (new equal v expr : t)


    class assign (v1 : Ext.var) (v2 : Ext.var) =
      object
	inherit t
	method string = v1 ^ ":=" ^ v2
	method run store m =
	  let n = List.assoc v2 m in
	  (v1,n) :: List.remove_assoc v1 m
      end

    let assign v1 v2 = (new assign v1 v2 : t)



(*
    class seq (p1 : t) (p2 : t) =
      object
	inherit t
	method run store m =
	  let m1 = p1#run store m in
	  p2#run store m1
	method undo store m =
	  let m1 = p2#undo store m in
	  let m2 = p1#undo store m1 in
	  m2
      end

    let seq p1 p2 = (new seq p1 p2 : t)
*)
(*
    class fork (p1 : t) (p2 : t) =
      object
	inherit t
	method run store m =
	  p1#run store m;
	  p2#run store m
	method undo store m1=
	  let m = p2#undo store m1 in
	  let _ = p1#undo store m1 in
	  m
      end

    let fork p1 p2 = (new fork p1 p2 : t)
*)


(*
    class choose (ext : Ext.t) =
      object
	inherit t
	method string = "choose(" ^ ext#string ^ ")"
	method run store m =
	  ext#choose store m
      end

    let choose ext = (new choose ext : t)
*)


    class given (lp_cond_assert : t list) (lp_cond_retract : t list) (lp : t list) =
      object
	inherit t
	method string = "given " ^ list_string lp_cond_assert ^ " do " ^ list_string lp ^ " finally " ^ list_string lp_cond_retract ^ " done"
	method run store m =
	  let m1 = list_run store m lp_cond_assert in
	  let m2 = list_run store m1 lp in
	  let _ = list_run store m1 lp_cond_retract in
	  m2
      end

    let given lpa lpr lp = (new given lpa lpr lp : t)


    class forany (e : Ext.t) (lp : t list) =
      object
	inherit t
	method string = "forany " ^ e#string ^ " do " ^ list_string lp ^ " done"
	method run store m =
	  try
	    let m1 = e#choose (store :> Ext.store) m in
	    list_run store m1 lp
	  with Not_found -> m
      end

    let forany e lp = (new forany e lp : t)

    class repeat (n : int) (lp : t list) =
      object
	inherit t
	method string = "repeat " ^ string_of_int n ^ " " ^ list_string lp ^ " done"
	method run store m =
	  for i = 1 to n do
	    ignore (list_run store m lp)
	  done;
	  m
      end

    let repeat n lp = (new repeat n lp : t)

    class loop (e : Ext.t) (lp : t list) =
      object
	inherit t
	method string = "while " ^ e#string ^ " do " ^ list_string lp ^ " done"
	method run store m =
	  let ref_m = ref m in
	  (try
	    while true do
	      let m' = e#choose (store :> Ext.store) !ref_m in
	      let m'' = list_run store m' lp in
	      ref_m := List.map (fun (v,o) -> (v, try List.assoc v m'' with Not_found -> o)) m
	    done
	  with _ -> ());
	  !ref_m
      end

    let loop e p = (new loop e p : t)


(*
    class ['a] alt (p1 : 'a t) (p2 : 'a t) =
      object
	inherit ['a] t
	method run store m =
	  try p1#run store m
	  with _ -> p2#run store m
      end

    let alt p1 p2 = (new alt p1 p2 : 'a t)
*)

  (* display *)

    class display (e : Ext.expr) =
      object
	inherit t
	method string = "display(" ^ e#string ^ ")"
	method run store m =
	  e#fold (store :> Ext.store)
	    (fun _ m1 n1 ->
	      print_string (Name.contents n1))
	    () m;
	  m
      end

    let display e = (new display e : t)

(*
    let rec display_run (store : store) (first : bool) (m : Ext.map) (ls : string list) : Ext.expr list -> Ext.map =
      function
	| [] ->
	    print_endline (String.concat " " (List.rev ls));
	    m
	| e::le ->
	    try
	      e#fold store
		(fun _ m1 n1 ->
		  display_run store false m1 (Name.contents n1 :: ls) le)
		m m
	    with _ ->
	      display_run store false m ("_" :: ls) le

    class display (le : Ext.expr list) =
      object
	inherit t
	method string = "display(" ^ String.concat "," (List.map (fun e -> e#string) le) ^ ")"
	method run store m =
	  display_run store true m [] le
      end

    let display le = (new display le : t)
*)

  (* custom procedures *)
(*
    class proc (p : Prim.proc) (expr_args : Ext.arg list) =
      object
	inherit t
	method string = p#uri ^ "(" ^ String.concat "," (List.map (fun (pp,e) -> e#string) expr_args) ^ ")"
	method run store m =
	  Ext.expr_list_fold store
	    (fun _ m1 args -> p#run args)
	    () m [] expr_args;
	  m
      end

    let proc p expr_args = (new proc p expr_args : t)
*)
  end
