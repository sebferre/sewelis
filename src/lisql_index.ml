
(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Intmap = Intmap.M
module Intset = Intset.Intmap

(* Hashtbl signature using Intmap *)
module IntHashtbl =
  struct
    type 'a t = 'a Intmap.t ref
    let create () = ref Intmap.empty
    let clear h = h := Intmap.empty
    let copy h = ref !h
    let add h k v = h := Intmap.set k v !h
    let find h k = Intmap.get k !h
    let mem h k = Intmap.mem k !h
    let remove h k = h := Intmap.remove k !h
    let replace h k v = h := Intmap.set k v !h
    let length h = Intmap.cardinal !h
    let iter f h = Intmap.iter f !h
    let fold f h init = Intmap.fold (fun k v acc -> f acc k v) init !h
  end

type oid = int

let hashtbl_get h k f_init =
  try Hashtbl.find h k
  with _ ->
    let v = f_init () in
    Hashtbl.add h k v;
    v

let intmap_get k f_init m =
  try Intmap.get k m, m
  with _ ->
    let v = f_init () in
    v, Intmap.set k v m

(*
type 'a node = { supp : int;
		 anew : bool;
		 element : 'a }
*)

class virtual ['a] index =
  object
    method virtual add : 'a -> rank:int -> unit
    method virtual get : 'a -> rank:int -> int * bool (* count, anew: count=0 if absent *)
    method virtual fold_children : 'b. ?parent:'a -> rank:int -> (count:int -> anew:bool -> 'a -> 'b -> 'b) -> 'b -> 'b
  end

(* empty index *)

class ['a] empty =
  object
    inherit ['a] index
    method add elt ~rank = ()
    method get elt ~rank = 0, true
    method fold_children ?parent ~rank f init = init
  end

(* list index *)

class ['a] list =
object (self)
    inherit ['a] index

    val index : ('a, int ref IntHashtbl.t) Hashtbl.t = Hashtbl.create 13
	(* elt -> (rank -> ref count) *)

    method add elt ~rank =
      let h_elt = hashtbl_get index elt (fun () -> IntHashtbl.create ()) in
      try incr (IntHashtbl.find h_elt rank)
      with _ -> IntHashtbl.add h_elt rank (ref 1)

    method private get_count_anew rank h_elt =
      let count = try !(IntHashtbl.find h_elt rank) with _ -> assert false in
      let anew = rank > 0 && not (IntHashtbl.mem h_elt (rank-1)) in
      count, anew

    method get elt ~rank =
      try
	let h_elt = Hashtbl.find index elt in
	self#get_count_anew rank h_elt
      with _ -> 0, true

    method fold_children ?parent ~rank f init =
      if parent = None
      then
	Hashtbl.fold
	  (fun elt h_elt res ->
	    try
	      let count, anew = self#get_count_anew rank h_elt in
	      f ~count ~anew elt res
	    with _ -> res)
	  index init
      else init
  end

(* oid hierarchical index *)

let rec iter_ancestors get_parents f oid =
  iter_ancestors_aux get_parents f (Intset.singleton oid)
    [(oid, get_parents oid)]
and iter_ancestors_aux get_parents f expanded = function
  | [] -> ()
  | (child, parents)::l ->
      let l' =
	if Intset.is_empty parents
	then begin
	  f child 0;
	  l end
	else
	  Intset.fold
	    (fun l' parent ->
	      f child parent;
	      if Intset.mem parent expanded
	      then l'
	      else (parent, get_parents parent)::l')
	    l parents in
      iter_ancestors_aux get_parents f (Intset.union parents expanded) l'
  

class oid_hierarchy (get_parents : oid -> Intset.t) =
  object (self)
    inherit [oid] index

    val index : (oid, (oid, int ref IntHashtbl.t) Hashtbl.t) Hashtbl.t = Hashtbl.create 13
	(* parent -> (child -> (rank -> ref count))) *)

    method add oid ~rank =
      iter_ancestors get_parents
	(fun child parent ->
	  let h_parent = hashtbl_get index parent (fun () -> Hashtbl.create 3) in
	  let h_child = hashtbl_get h_parent child (fun () -> IntHashtbl.create ()) in
	  try incr (IntHashtbl.find h_child rank)
	  with _ -> IntHashtbl.add h_child rank (ref 1))
	oid

    method private get_count_anew rank h_child =
      let count = try !(IntHashtbl.find h_child rank) with _ -> assert false in
      let anew = rank > 0 && not (IntHashtbl.mem h_child (rank-1)) in
      count, anew
      
    method get oid ~rank =
      try
	let parent = try Intset.choose (get_parents oid) with _ -> 0 in
	let h_parent = Hashtbl.find index parent in
	let h_child = Hashtbl.find h_parent oid in
	self#get_count_anew rank h_child
      with _ -> 0, true

    method fold_children ?(parent = 0) ~rank f init =
      try
	let h_parent = Hashtbl.find index parent in
	Hashtbl.fold
	  (fun child h_child res ->
	    try
	      let count, anew = self#get_count_anew rank h_child in
	      f ~count ~anew child res
	    with _ -> res)
	  h_parent init
      with _ -> init

  end
