(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Name = Name.Make
module Intmap = Intmap.M
module Ext = Intset.Intmap
module Rel = Intreln.Intmap
module Extension = Extension.Make
module Code = Code.Make
module Store = Store.Make
module Namespace = Lisql_namespace
module AST = Lisql_ast
(*module Syntax1 = Lisql_syntax1 (* old-version parsing *)*)
module Syntax = Lisql_syntax (* current-version parsing and printing *)
module Display = Lisql_display
module Semantics = Lisql_semantics
module Transf = Lisql_transf
module Feature = Lisql_feature
module Concept = Lisql_concept

open AST

(* sets an axiom *)

let axiom_sub ~obs store  (uri_sub : Uri.t) x1 x2_opt : s list =
  let sub x1 x2 : s =
    name_is (Rdf.URI x1) (prop_name uri_sub (Rdf.URI x2)) in
  match x2_opt with
  | Some x2 -> [sub x1 x2]
  | None ->
      let p : Store.property = store#get_property uri_sub in
      p#relation#fold_successors ~obs
	(fun res -> function
	  | Rdf.URI x2 -> sub x1 x2 :: res
	  | _ -> res)
	[] (Rdf.URI x1)
	
let axiom_subentity ~obs store e1 e2_opt = axiom_sub ~obs store  Namespace.uri_subEntityOf e1 e2_opt
let axiom_subclass ~obs store c1 c2_opt = axiom_sub ~obs store Rdfs.uri_subClassOf c1 c2_opt
let axiom_subproperty ~obs store p1 p2_opt = axiom_sub ~obs store Rdfs.uri_subPropertyOf p1 p2_opt

(* TODO: revise *)
let rec axiom_s1 store np1 np2 =
  match np1, np2 with
  | Det (det1,_), Det (det2,_) -> axiom_s2 store det1 det2
  | _ -> ()
and axiom_s2 store det1 det2 =
  match det1, det2 with
  | Name (Rdf.URI e1), Qu (An,_) -> store#axiom_subentity e1 None
  | Name (Rdf.URI e1), Name (Rdf.URI e2) -> store#axiom_subentity e1 (Some e2)
  | _ -> ()
and axiom_p1 store f1 f2 : unit =
  match f1, f2 with
  | Arg (pred, _, _), Thing -> axiom_pred store pred None
  | Arg (pred1,i1,args1), Arg (pred2,i2,args2) ->
      let arity1 = Array.length args1 - 1 in
      let arity2 = Array.length args2 - 1 in
      if i1=i2 && arity1 = arity2 then begin
	axiom_pred store pred1 (Some pred2);
	for j = 0 to arity1 do
	  if j <> i1 && (j > 0 || (Pred.reifiable pred1 && Pred.reifiable pred2)) then
	    axiom_s1 store args1.(j) args2.(j)
	done
      end
  | _, And lf2 -> List.iter (fun f2 -> axiom_p1 store f1 f2) lf2
  | _, _ -> ()
and axiom_pred store pred1 pred2 =
  match pred1, pred2 with
    | Pred.Type a, None -> store#axiom_subclass a None
    | Pred.Type a, Some (Pred.Type b) -> store#axiom_subclass a (Some b)
    | Pred.Role (p1,_), None -> store#axiom_subproperty p1 None
    | Pred.Role (p1,_), Some (Pred.Role (p2,_)) -> store#axiom_subproperty p1 (Some p2)
    | _ -> ()


	      (* description - with new structures *)

let rec description ~obs (store : #Store.store) (n : Name.t) : s1 = Common.prof "Lisql.description" (fun () ->
  if n = Rdf.URI Rdf.uri_nil
  then an nil
  else
    let path = [] in
    let backrole = ((Rdf.uri_value,Argindex.obj), Rdf.Blank "") in (* fake backrole *)
    let _, np = expand_aux ~obs store path backrole n in
    let blank2cpt = description_blanks_s1 [] np in
    let blank2var =
      let _, b2v =
	List.fold_left
	  (fun (vars,b2v) (n,cpt) ->
	    if !cpt > 1
	    then
	      let v = get_new_var vars in
	      v::vars, (n, (v, ref true (* first *)))::b2v
	    else vars, b2v)
	  ([],[]) blank2cpt in
      b2v in
    match description_unblank_s1 blank2var np with
    | Det (Qu (An, v_opt), c) -> Det (Qu (The, v_opt), c)
    | np -> np)
and description_aux ~obs store path backrole_opt n =
  let path1 = n::path in
  let d_n = store#get_description (store#get_entity n) in
  let ltype = description_type ~obs store n d_n in
  let lrole = description_role ~obs store n d_n in
  let lstruct = description_struct ~obs store n d_n in
  let lrole = (* removing backrole *)
    Option.fold
      (fun backrole -> List.filter ((<>) backrole) lrole)
      lrole
      backrole_opt in
  let path, lrole = (* expanding role images *)
    List.fold_left
      (fun (path,lrole) ((a,i),n1) ->
	if n1 = Rdf.URI Rdf.uri_nil
	then path, ((a,i), an nil)::lrole
	else
	  match n1 with
	  | Rdf.Blank _ ->
	      let path, np_n1 = expand_aux ~obs store path ((a,Argindex.opposite i),n) n1 in
	      path, ((a,i), np_n1)::lrole
	  | _ ->
	      path, ((a,i), name_s1 n1)::lrole)
      (path,[]) lrole in
  let path, lstruct = (* expanding struct args *)
    List.fold_left
      (fun (path,lstruct') (funct,args) ->
	let path, _, rev_args' =
	  List.fold_left
	    (fun (path,i,rev_args') n1 ->
	      if n1 = Rdf.URI Rdf.uri_nil
	      then path, i+1, (an nil)::rev_args'
	      else
		match n1 with
		| Rdf.Blank _ ->
		    let path, np_n1 = expand_aux ~obs store path ((Term.uri_arg i,Argindex.obj),n) n1 in
		    path, i+1, np_n1::rev_args'
		| _ ->
		    path, i+1, (name_s1 n1)::rev_args')
	    (path,1,[]) args in
	path, (funct, List.rev rev_args')::lstruct')
      (path,[]) lstruct in
  path,
  ltype,
  lrole,
  lstruct
and expand_aux ~obs store path ((a,i), _ as backrole) n =
  if List.mem n path
  then
    n::path, Det (Name n, Thing)
  else
    let path1, ltype, lrole, lstruct = description_aux ~obs store path (Some backrole) n in
    let lrole = (* removing remaining backward roles  *)
      List.filter
	(fun ((a,i),_) ->
	  not (i = Argindex.obj &&
	       (a = Term.uri_functor ||
	       Term.is_arg a ||
	       (store#get_class Term.uri_ArgProperty)#relation#has_instance ~obs (Rdf.URI a))))
	lrole in
    let ld = [] in
    let ld =
      List.fold_left
	(fun res c -> has_type c::res)
	ld ltype in
    let ld =
      List.fold_left
	(fun res ((a,i),np) -> has_role (Pred.make_role a) i np::res)
	ld lrole in
    let ld =
      List.fold_left
	(fun res (funct,args) -> Arg (Pred.Funct funct, 0, Array.of_list (top_s1::args))::res)
	ld lstruct in
    let d = simpl_and_list ld in
    path1, Det (Name n, d)
and description_type ~obs store n d_n =
  let p_functorType = store#get_property Term.uri_functorType in
  d_n#classes#fold_as_uris
    (fun res a ->
      if not (List.mem a [Rdfs.uri_Resource; Rdfs.uri_Literal])
	    && not (p_functorType#relation#has_object ~obs (Rdf.URI a))
	    && (store#get_class a)#explicit_relation#has_instance ~obs n
      then a :: res
      else res)
    []
and description_role ~obs store n d_n =
  let c_ArgProperty = store#get_class Term.uri_ArgProperty in
  d_n#fwd_properties#fold_as_uris
    (fun res a ->
      if not (List.mem a [Rdfs.uri_label; Namespace.uri_inverseLabel])
	  && not (c_ArgProperty#relation#has_instance ~obs (Rdf.URI a))
      then
	let pr = store#get_property a in
	pr#explicit_relation#fold_successors ~obs
	  (fun res y -> ((a,Argindex.subj), y) :: res)
	  res n
      else res) [] @
  d_n#bwd_properties#fold_as_uris
    (fun res a ->
      match n with
	| Rdf.URI _ ->
	    if not (c_ArgProperty#relation#has_instance ~obs (Rdf.URI a))
	    then
	      let pr = store#get_property a in
	      pr#explicit_relation#fold_predecessors ~obs
		(fun res y -> if y = n then res else ((a,Argindex.obj), y) :: res)
		res n
	    else res
	| _ -> res) []
and description_struct ~obs store (n : Name.t) d_n =
  d_n#structs#fold_as_uris
    (fun res funct arity pos ->
      if pos = 0
      then
	let fu = store#get_functor funct arity in
	fu#relation#fold_args ~obs
	  (fun res args -> (funct,args) :: res)
	  res n
      else res (* args are not included in descriptions *))
    []
and description_blanks_s1 blank2cpt = function
  | Det (Name (Rdf.Blank _ as n), f) ->
      let blank2cpt =
	try
	  let cpt = List.assoc n blank2cpt in
	  incr cpt;
	  blank2cpt
	with Not_found ->
	  (n, ref 1)::blank2cpt in
      description_blanks_p1 blank2cpt f
  | Det (_, f) ->
      description_blanks_p1 blank2cpt f
  | _ -> blank2cpt
and description_blanks_p1 blank2cpt = function
  | Arg (pred,i,args) ->
      let arity = Array.length args - 1 in
      Common.fold_for
	(fun n res -> if n<>i && (n>0 || Pred.reifiable pred) then description_blanks_s1 res args.(n) else res)
	0 arity
	blank2cpt
  | And l ->
      List.fold_left
	(fun res f -> description_blanks_p1 res f)
	blank2cpt l
  | _ -> blank2cpt
and description_unblank_s1 blank2var = function
  | Det (Name (Rdf.Blank _ as n), f) ->
      let det =
	try
	  let v, first = List.assoc n blank2var in
	  if !first
	  then begin first := false; Qu (An, Some v) end
	  else Ref v
	with Not_found ->
	  Qu (An, None) in
      Det (det, description_unblank_p1 blank2var f)
  | Det (det, f) ->
      Det (det, description_unblank_p1 blank2var f)
  | np -> np
and description_unblank_p1 blank2var = function
  | Arg (pred,i,args) ->
      Arg (pred, i,
	   Array.mapi
	     (fun n arg ->
	       if n <> i && (n > 0 || Pred.reifiable pred)
	       then description_unblank_s1 blank2var arg
	       else arg)
	     args)
  | And l ->
      And (List.map (fun f -> description_unblank_p1 blank2var f) l)
  | f -> f

(* answers *)

type order = [ `DEFAULT | `DESC | `ASC ]
type filter_atom = [ `REGEXP of Str.regexp | `INTERV of bool * string * string * bool ]
type filter = filter_atom list
type aggreg = [ `INDEX | `DISTINCT_COUNT | `SUM | `AVG ]
type measure = [ `VOID | `AGGREG of aggreg ]
type cube = [ `HIDDEN of cube | `DIMENSION of filter * order * cube | `COUNT of measure list ]

class virtual cell =
  object
    method name_opt : Name.t option = None
    method virtual display : Display.t
  end

let display_null = [`Kwd "-"]

class cell_null =
  object
    inherit cell
    method display = display_null
  end

class cell_name ~obs store (n : Name.t) =
  let disp = Display.display_name_no_blank ~obs store n in
  object
    inherit cell
    val n = n
    method name_opt = Some n
    val disp = disp
    method display = disp
  end

let rec list_heads n = function (* return boolean 'partial', and n heads *)
  | [] -> false, []
  | x::l ->
      if n = 0
      then true, []
      else
	let partial, heads = list_heads (n-1) l in
	partial, x::heads

class cell_index ~obs store (l : (int * Name.t option) list) =
  let disp =
    if l = []
    then display_null
    else
      let partial, heads = list_heads 100 l in
      [`List (List.fold_right
		(fun (c,n_opt) res ->
		  let d_c =
		    if c = 1
		    then []
		    else [`Kwd (Printf.sprintf "(%d)" c)] in
		  let d_n =
		    match n_opt with
		    | None -> display_null
		    | Some n -> Display.display_name_no_blank ~obs store n in
		  (d_n @ `Space :: d_c) :: res)
		heads (if partial then [[`Kwd "..."]] else []))] in
  object
    inherit cell
    val disp = disp
    method display = disp
  end


let v_count = "#"
  
let regexp_match re s = Common.prof "Lisql.regexp_match" (fun () ->
  try ignore (Str.search_forward re s 0); true with _ -> false)

let compare_low (s : string) (n : Name.t) : int = Common.prof "Lisql.compare_low" (fun () ->
  match n with
  | Rdf.Literal (sn, Rdf.Typed dt) ->
      if s = ""
      then -1
      else Name.compare (Rdf.Literal (s, Rdf.Typed dt)) n
  | _ -> 1)

let compare_high (n : Name.t) (s : string) : int = Common.prof "Lisql.compare_high" (fun () ->
  match n with
  | Rdf.Literal (sn, Rdf.Typed dt) ->
      if s = ""
      then -1
      else Name.compare n (Rdf.Literal (s, Rdf.Typed dt))
  | _ -> 1)

let rec apply_filter (filter : filter) (n_s : Name.t option * string) : bool =
  List.for_all (fun f -> apply_filter_atom f n_s) filter
and apply_filter_atom (filter : filter_atom) (n_opt, s : Name.t option * string) : bool =
  match filter with
  | `REGEXP re -> regexp_match re s
  | `INTERV (in1,s1,s2,in2) ->
      Option.fold
	(fun n ->
	  compare_low s1 n <= (if in1 then 0 else -1) && compare_high n s2 <= (if in2 then 0 else -1))
	false
	n_opt

let compare_name_default store n1 n2 =
  match n1, n2 with
  | Rdf.Literal _, Rdf.Literal _ -> Name.compare n1 n2
  | _ -> Pervasives.compare (store#get_pagerank n2) (store#get_pagerank n1)

let compare_name_opt comp opt1 opt2 =
  match opt1, opt2 with
  | Some n1, Some n2 -> comp n1 n2
  | Some _, None -> -1
  | None, Some _ -> 1
  | None, None -> 0

let compare_order store order : (Name.t option * 'a) -> (Name.t option * 'a) -> int =
  Common.prof "Lisql.compare_order" (fun () ->
    fun (n1_opt,_) (n2_opt,_) ->
      compare_name_opt
	(fun n1 n2 ->
	  match order with
	  | `DESC -> Name.compare n2 n1
	  | `ASC -> Name.compare n1 n2
	  | `DEFAULT -> compare_name_default store n1 n2 )
	n1_opt n2_opt)

let index col rel : int ref Intmap.t = Common.prof "Lisql.index" (fun () ->
  Rel.fold
    (fun res l ->
      let o = List.nth l col in
      try
	incr (Intmap.get o res);
	res
      with Not_found ->
	Intmap.set o (ref 1) res)
    Intmap.empty rel)

let sum_count store index : float * int =
  Intmap.fold
    (fun (sum,n) o counter ->
      let c = !counter in
      if o = 0 then (sum,n)
      else begin
	match store#get_name o with
	| Rdf.Literal (s, dt) ->
	    (try (sum +. (float_of_int c *. float_of_string s), n + c)
	    with _ -> (sum,n))
	| _ -> (sum,n)
      end)
    (0., 0) index


let make_aggreg ~obs store g index : cell = Common.prof "Lisql.make_aggreg" (fun () ->
  match g with
  | `INDEX ->
      let l_index =
	Intmap.fold
	  (fun res o counter -> (!counter, if o=0 then None else Some (store#get_name o))::res)
	  [] index in
      let sorted_l_index =
	List.sort
	  (fun (c1,n1_opt) (c2,n2_opt) ->
	    Common.compare_pair (Pervasives.compare, compare_name_opt (compare_name_default store)) (c2,n1_opt) (c1,n2_opt))
	  l_index in
        (* decreasing count *)
      new cell_index ~obs store sorted_l_index
  | `DISTINCT_COUNT ->
      new cell_name ~obs store (Name.typed_literal (string_of_int (Intmap.cardinal index)) Xsd.uri_integer)
  | `SUM ->
      let sum, count = sum_count store index in
      if count = 0
      then new cell_null
      else new cell_name ~obs store (Name.typed_literal (string_of_float sum) Xsd.uri_double)
  | `AVG ->
      let sum, count = sum_count store index in
      if count = 0
      then new cell_null
      else
	let avg = sum /. (float_of_int count) in
	new cell_name ~obs store (Name.typed_literal (string_of_float avg) Xsd.uri_double)
  | _ -> new cell_null)

let rec fold_answers ~obs store (offset : int ref) (limit : int ref) (cube : cube)
    (f : 'a -> cell list -> 'a) (init : 'a) (rev_line : cell list) rel : 'a =
  match cube with
  | `COUNT sub_cube ->
      if !offset > 0 then
	begin decr offset; init end
      else if !limit = 0 then
	init
      else begin
	decr limit;
	let n_count = Name.typed_literal (string_of_int (Rel.cardinal rel)) Xsd.uri_integer in
	let line =  Common.prof "Lisql.fold_answers/fold_measures" (fun () ->
	  fold_measures ~obs store sub_cube (new cell_name ~obs store n_count :: rev_line) 0 rel) in
	f init line
      end
  | `DIMENSION (filter,order,sub_cube) ->
      let key_names =
	Ext.fold
	  (fun res oid ->
	    let cell = if oid = 0 then new cell_null else new cell_name ~obs store (store#get_name oid) in
	    let n_opt = cell#name_opt in
	    let s_cell = Display.to_string cell#display in
	    if apply_filter filter (n_opt, s_cell)
	    then (n_opt, (oid, cell))::res
	    else res)
	  [] (Rel.keys rel) in
      let sorted_key_names = List.sort (compare_order store order) key_names in
      List.fold_left
	(fun res (n_opt,(o,cell)) ->
	  let sub_rel = Rel.assoc o rel in
	  fold_answers ~obs store offset limit sub_cube f res (cell::rev_line) sub_rel)
	init sorted_key_names
  | `HIDDEN sub_cube ->
      fold_answers ~obs store offset limit sub_cube f init (new cell_null :: rev_line) rel
and fold_measures ~obs store cube rev_line col rel =
  match cube with
  | [] -> List.rev rev_line
  | `VOID::sub_cube ->
      fold_measures ~obs store sub_cube (new cell_null :: rev_line) (col+1) rel
  | `AGGREG g::sub_cube ->
      let index = index col rel in
      let cell_aggreg = make_aggreg ~obs store g index in
      fold_measures ~obs store sub_cube (cell_aggreg :: rev_line) (col+1) rel


class answers store (lv, ext : var list * Extension.t) =
  let lv = List.filter (fun v -> not (AST.var_has_prefix AST.var_name v)) lv in
  let rel = Common.prof "Lisql.answers/ext#relation" (fun () -> ext#relation (store :> Extension.store) lv) in
  object (self)
    val mutable lines : Rel.t = rel

    val mutable count : int = Rel.cardinal rel
    method count = count

    val mutable columns : Extension.var list = lv @ [v_count] (* v_count represents COUNT *)
    method columns = columns
    method move_column_left v = Common.prof "Lisql.answers#move_column_left" (fun () ->
      let state, mask, lv' =
	List.fold_right
	  (fun w (state,mask,lv') ->
	    if state = 1 then
	      if v = w then
		if w = v_count then 22, mask, lv'
		else 2, true::mask, lv'
	      else
		if w = v_count then 1, mask, w::lv'
		else 1, false::mask, w::lv'
	    else if state = 2 then
	      if w = v_count then 33, mask, v::w::lv'
	      else 3, false::mask, v::w::lv'
	    else if state = 22 then 33, false::mask, v::w::lv'
	    else if state = 3 then
	      if w = v_count then 3, mask, w::lv'
	      else 3, true::mask, w::lv'
	    else if state = 33 then 33, true::mask, w::lv'
	    else assert false)
	  columns (1,[],[]) in
      if state = 3 then begin (* otherwise, v missing or first column or only # moved *)
	columns <- lv';
	lines <- Rel.group_by mask lines end
      else if state = 33 then begin
	columns <- lv'
      end)
    method move_column_right v = Common.prof "Lisql.answers#move_column_right" (fun () ->
      let state, mask, lv' = 
	List.fold_right
	  (fun w (state,mask,lv') ->
	    if state = `Last then
	      if w = v then
		if w = v_count then `Nop, mask, w::lv'
		else `Nop, false::mask, w::lv'
	      else
		if w = v_count then `Search, mask, w::lv'
		else `Search, false::mask, w::lv'
	    else if state = `Search then
	      if w = v then
		if w = v_count then `Nop, mask, (List.hd lv')::w::(List.tl lv')
		else if List.hd lv' = v_count then `Nop, true::mask, (List.hd lv')::w::(List.tl lv')
		else `Found, false::true::(List.tl mask), (List.hd lv')::w::(List.tl lv')
	      else
		if w = v_count then `Search, mask, w::lv'
		else `Search, false::mask, w::lv'
	    else if state = `Found then
	      if w = v_count then `Found, mask, w::lv'
	      else `Found, true::mask, w::lv'
	    else if state = `Nop then
	      if w = v_count then `Nop, mask, w::lv'
	      else `Nop, true::mask, w::lv'
	    else assert false)
	  columns (`Last,[],[]) in
      if state = `Found then begin (* otherwise, v missing or first column or only # moved *)
	columns <- lv';
	lines <- Rel.group_by mask lines end
      else if state = `Nop then begin
	columns <- lv'
      end)

    val mutable page_start = 1
    val mutable page_size = 10
    method page_start = page_start
    method page_end = min (page_start + page_size - 1) self#count
    method page_size = page_size
    method page_pos pos = min (max 1 pos) self#count

    method page_down : bool =
      if self#page_end < self#count
      then begin page_start <- page_start + page_size; true end
      else false

    method page_top : bool =
      if page_start <> 1
      then begin page_start <- 1; true end
      else false

    method page_up : bool =
      if page_start - page_size >= 1
      then begin page_start <- page_start - page_size; true end
      else false

    method page_bottom : bool =
      let pos = ((self#count - 1) / page_size) * page_size + 1 in
      if pos <> page_start
      then begin page_start <- pos; true end
      else false

    method set_start pos : bool =
      let pos = self#page_pos pos in
      if pos <> page_start
      then begin page_start <- pos; true end
      else false

    method set_end pos : bool =
      let size = self#page_pos pos - page_start + 1 in
      if size <> page_size
      then begin page_size <- size; true end
      else false

    val mutable hidden : var LSet.t = LSet.empty ()
    method get_hidden v = LSet.mem v hidden
    method set_hidden v b =
      if b
      then hidden <- LSet.add v hidden
      else hidden <- LSet.remove v hidden

    val h_order : (var, order) Hashtbl.t = Hashtbl.create 11
    method get_order v = try Hashtbl.find h_order v with _ -> `DEFAULT
    method set_order v ord = 
      match ord with
      | `DEFAULT -> Hashtbl.remove h_order v
      | _ -> Hashtbl.replace h_order v ord

    val h_pattern : (var, string) Hashtbl.t = Hashtbl.create 11
    method get_pattern v = try Hashtbl.find h_pattern v with _ -> ""
    method set_pattern v re =
      match re with
      | "" -> Hashtbl.remove h_pattern v
      | _ -> Hashtbl.replace h_pattern v re
    method get_filter v =
      match self#get_pattern v with
      | "" -> []
      | s ->
	  List.map
	    (fun s1 ->
	      let s1_0 = s1.[0] in
	      if s1_0 = '[' || s1_0 = ']'
	      then
		let s1_len = String.length s1 in
		let s1_last = s1.[s1_len-1] in
		let s1_mid = try String.index s1 ',' with _ -> s1_len in
		let s1_end = if s1_len > 1 && (s1_last = '[' || s1_last = ']') then s1_len-1 else s1_len in
		let in1 = s1_0 = '[' in
		let in2 = s1_last <> '[' in
		let s1_low = String.sub s1 1 (s1_mid-1) in
		let s1_high = if s1_mid+1 >= s1_len then "" else String.sub s1 (s1_mid+1) (s1_end-(s1_mid+1)) in
		`INTERV (in1,s1_low,s1_high,in2)
	      else
		`REGEXP (Str.regexp_string_case_fold s1))
	    (Str.split (Str.regexp "[ ]+") s)

    val h_aggreg : (var, aggreg) Hashtbl.t = Hashtbl.create 11
    method get_aggreg v : aggreg = try Hashtbl.find h_aggreg v with _ -> `INDEX
    method set_aggreg v (g : aggreg) =
      match g with
      | `INDEX -> Hashtbl.remove h_aggreg v
      | _ -> Hashtbl.replace h_aggreg v g

    method fold : 'a. obs:Tarpit.observer -> ('a -> cell list -> 'a) -> 'a -> 'a =
      fun ~obs f init -> Common.prof "Lisql.answers#fold" (fun () ->
	let offset = page_start - 1 in
	let limit = page_size in
	let rel = Rel.project
	    (snd (List.fold_right
		    (fun v (dim,mask) ->
		      if v = v_count then (false,mask)
		      else dim, not (dim && self#get_hidden v) :: mask)
		    columns (true,[])))
	    lines in
	let _, mask_dims =
	  List.fold_right
	    (fun v (state,mask) ->
	      if v = v_count then `Dimensions, mask
	      else if state = `Measures then state, false::mask
	      else if state = `Dimensions then state, (not (self#get_hidden v))::mask
	      else assert false)
	    columns (`Measures,[]) in
	count <-
	  begin
	    let rel_dims = Rel.project mask_dims lines in
	    Rel.cardinal rel_dims
	  end;
	let pre_cube =
	  List.fold_right
	    (fun v -> function
	      | `Measures l ->
		  if v = v_count then `Cube (`COUNT l)
		  else if self#get_hidden v then `Measures (`VOID::l)
		  else `Measures (`AGGREG (self#get_aggreg v)::l)
	      | `Cube c ->
		  if self#get_hidden v then `Cube (`HIDDEN c)
		  else `Cube (`DIMENSION (self#get_filter v, self#get_order v, c)))
	    columns (`Measures []) in
	let cube =
	  match pre_cube with
	  | `Measures l -> `COUNT l
	  | `Cube c -> c in
	Common.prof "Lisql.answers#fold/fold_answers" (fun () ->
	  fold_answers ~obs store (ref offset) (ref limit) cube f init [] rel))

    method copy (lv, ext : var list * Extension.t) =
      let lv = List.filter (fun v -> not (AST.var_has_prefix AST.var_name v)) lv in
      (* removing old columns *)
      let cols = List.filter (fun v -> v = v_count || List.mem v lv) columns in
      (* adding new columns *)
      let cols = cols @ List.filter (fun v -> not (List.mem v columns)) lv in
      let rel = ext#relation (store :> Extension.store) (List.filter ((<>) v_count) cols) in
      {< columns = cols;
	 lines = rel;
	 count = Rel.cardinal rel;
	 page_start = 1;
	 >}

  end

(* place *)

type place_mode = [`Root | `Type | `Val | `Relaxed ]

class place ~obs store (foc : focus) =
  let con = Concept.of_focus ~obs store foc in
  object (self)
    val focus = foc
    method focus = focus

    val assertion : c = command_of_focus foc
    method assertion = assertion
    method new_var = new_var_c assertion
    method do_run : unit = store#run assertion
    method set_as_home_query : unit =
      let quoted_a = store#quote_assertion assertion in
      store#set_value (Rdf.URI Namespace.uri_HomeQuery) Rdf.uri_value quoted_a
    method add_as_bookmark : unit =
      let quoted_a = store#quote_assertion assertion in
      store#create_blank (Some Namespace.uri_Bookmark)
	[(Rdf.uri_value, quoted_a)]
    method add_as_draft : unit =
      store#add_draft assertion
    method remove_as_draft : unit =
      let quoted_a = store#quote_assertion assertion in
      store#remove_value (Rdf.URI Namespace.uri_DraftBag) Rdfs.uri_member quoted_a

    val concept = con
    method accessible_vars = concept#accessible_vars
    method nb_answers = concept#nb_answers

    val mutable rank = 0
    method rank = rank
    method has_more = concept#rank_succ rank <> None
    method has_less = rank > concept#least_rank
    method more_extent = if self#has_more then rank <- rank+1
    method less_extent = if self#has_less then rank <- rank-1
    method most_extent = while self#has_more do self#more_extent done
    method least_extent = while self#has_less do self#less_extent done
    initializer rank <- concept#least_rank

    method mode : place_mode =
      if rank > 0 || concept#nb_answers = 0
      then `Relaxed
      else
	match concept#window with
	  | `None -> `Root
	  | `Extent _ -> `Type
	  | `Refs _ -> `Val

    method transformations = concept#transformations
    method insert_s1 = concept#insert_s1
    method insert_p1 = concept#insert_p1
    method children_increments parent_increment = concept#children_increments rank parent_increment
    method increment_s12 = concept#increment_s12 rank
    method increment_p12 = concept#increment_p12 rank
    method completions ?max_compl ?nb_more_relax key = concept#completions rank ?max_compl ?nb_more_relax key

    val answers = new answers store (con#accessible_vars, con#extension)
    method answers = answers

    method copy ~obs (foc : focus) =
      let con = Concept.of_focus ~obs store foc in
      {< focus = foc;
	 assertion = command_of_focus foc;
	 concept = con;
	 rank = con#least_rank;
	 answers = answers#copy (con#accessible_vars, con#extension);
	 >}
  end

(* TO BE REVISED: because this used reification *)
(*
   let rec descr_of_xml base uri = function
   | Xml.Element (tag, attribs, elements) ->
   let uri = uri ^ "-" ^ tag in
   let cc = simpl_and_list
   (List.map
   (fun (a,v) ->
   Role (RAtom (Fwd, Uri.resolve base [] a), Name (Name.plain_literal v "")))
   attribs) in
   let _, children =
   List.fold_left
   (fun (i,res) e -> i+1, descr_of_xml base (uri ^ "/" ^ string_of_int i) e :: res)
   (0,[]) elements in
   Role
   (RAtom (Fwd, (Uri.resolve base [] tag, cc)),
   And
   ( Name (Rdf.URI uri) ::
   Type (Namespace.uri_XMLElement, Thing) ::
   children ))
   | Xml.PCData text ->
   Role
   (RAtom (Fwd, (Rdf.uri_value, Thing)),
   Name (Name.plain_literal text ""))
   
   let import_xml_file store (filename : string) : unit =
   print_endline "Parsing XML...";
   let xml = Xml.parse_file filename in
   print_endline "Translating XML to RDF...";
   let uri = "file://" ^ filename in
   let base = uri ^ "#" in
   let a = name_is (Rdf.URI uri) (descr_of_xml base (uri ^ "/0") xml) in
(*	  let a = NameIs (Rdf.URI uri, Role (RAtom (Fwd, ("xml", Thing)), descr_of_xml uri xml)) in *)
   print_endline "Importing RDF...";
   store#tell ~src:uri a
 *)

module File =
  struct
    let prefix = "file:"
    let namespace = "http://www.irisa.fr/LIS/ferre/RDFS/file#"
    let uri_File = namespace ^ "File"
    let uri_Directory = namespace ^ "Directory"
    let uri_directory = namespace ^ "directory"
    let uri_mimetype = namespace ^ "mimetype"
    let uri_size = namespace ^ "size"
    let uri_created = namespace ^ "created"
    let uri_modified = namespace ^ "modified"
    let uri_extension = namespace ^ "extension"
  end

let import_directory store (abs_path : string) =
  let st = Unix.stat abs_path in
  let uri = "file://" ^ abs_path in
  let a = name_is (Rdf.URI uri)
		  (And
		    [ has_type File.uri_Directory;
		      prop_name Rdfs.uri_label
			(Name.typed_literal (Filename.basename abs_path) Xsd.uri_string);
		      prop_name File.uri_directory
			(Rdf.URI ("file://" ^ Filename.dirname abs_path));
		      prop_name File.uri_modified
			(Name.dateTime_of_UnixTime st.Unix.st_mtime);
		      prop_name File.uri_created
			(Name.dateTime_of_UnixTime st.Unix.st_ctime);
		    ]) in
  store#tell a

let import_file store (abs_path : string) =
  let st = Unix.stat abs_path in
  if st.Unix.st_kind = Unix.S_REG (* regular file *)
  then begin
    let uri = "file://" ^ abs_path in
    let lf =
      [ has_type File.uri_File;
	prop_name Rdfs.uri_label
	  (Name.typed_literal (Filename.basename abs_path) Xsd.uri_string);
	prop_name File.uri_directory
	  (Rdf.URI ("file://" ^ Filename.dirname abs_path));
	prop_name File.uri_size
	  (Name.typed_literal (string_of_int st.Unix.st_size) Xsd.uri_integer);
	prop_name File.uri_modified
	  (Name.dateTime_of_UnixTime st.Unix.st_mtime);
	prop_name File.uri_created
	  (Name.dateTime_of_UnixTime st.Unix.st_ctime);
      ] in
    let lf =
      let extension =
	try
	  let n = String.length abs_path in
	  let dot_pos = String.length (Filename.chop_extension abs_path) in
	  String.sub abs_path (dot_pos + 1) (n - dot_pos -1)
	with _ -> "" in
      if extension = ""
      then lf
      else prop_name File.uri_extension (Name.typed_literal extension Xsd.uri_string) :: lf in
    let a = name_is (Rdf.URI uri) (And lf) in
    store#tell a
  end

type stat = unit (* TODO *)

type data = unit (* TODO *)

let eval_command store = function
  | Base uri -> store#set_base uri
  | Prefix (pre,uri) -> store#add_prefix pre uri
  | Import (base,filename) -> store#import_rdf_file ~base filename
  | LinkedData uri -> store#import_uri uri
(*
  | CopyName (n,n2) -> store#copy_entity n n2
  | MoveName (n,n2) -> store#move_entity n n2
  | RemoveName n -> store#remove_entity n
  | Assert (src,a) -> store#tell ~src a
*)
  | Comment s -> ()
  | Command c -> store#run c

(*
let import_logfile ~src store filename =
  Common.fold_in_channel
    (fun () s ->
      print_string "parsing old logline: "; print_endline s;
      let l_uri_label, cmd = Syntax1.parse_of_string (Syntax1.parse_command store) [] s in
      Syntax1.define_labels store ~src l_uri_label;
      eval_command store cmd)
    ()
    (open_in filename)
*)

let rec import_file_tree ?(f_dir : string -> unit = fun _ -> ()) ~(f_file : string -> unit) (abs_path : string) : unit =
  let st = Unix.stat abs_path in
  if st.Unix.st_kind = Unix.S_DIR
  then begin
    f_dir abs_path;
    Array.iter
      (fun name -> import_file_tree ~f_dir ~f_file (Filename.concat abs_path name))
      (Sys.readdir abs_path) end
  else
    f_file abs_path


class store ~(base : Uri.t) (filepath : string) =
  object (self : 'self)
    inherit Store.store Namespace.uri_store as super

    method filepath = filepath

	(* log file *)

    val log = new Log.log filepath

    method log = log

    method log_append_command (cmd : command) : unit =
      log#append (Syntax.print_to_string ~ctx:self#as_context Syntax.print_command cmd);

        (* db file *)

    method save = Common.prof "Lisql.store#save" (fun () ->
      let filename = filepath ^ ".db" in
      let ch = open_out_bin filename in
      Marshal.to_channel ch self [Marshal.Closures];
      close_out ch)

	(* base and xmlns *)

    val mutable base : Uri.t = base
      (*
      let dirname = Filename.dirname filepath in
      let dirname =
	if dirname = Filename.current_dir_name then Unix.getcwd ()
	else if dirname = Filename.parent_dir_name then Filename.dirname (Unix.getcwd ())
	else dirname in
      Name.uri_of_file (Filename.concat dirname "")
      *)

    val mutable xmlns : Rdf.xmlns = 
      [ ("xml:", "xml#");
	(Xsd.prefix, Xsd.namespace);
	(Rdf.prefix, Rdf.namespace);
	(Rdfs.prefix, Rdfs.namespace);
	(Owl.prefix, Owl.namespace);
	(Foaf.prefix, Foaf.namespace);
	(Strdf.prefix, Strdf.namespace);
	(Term.prefix, Term.namespace);
(*	(Primitive.Lisprim.prefix, Primitive.Lisprim.namespace); *)
	(Namespace.prefix, Namespace.namespace); (* LISQL namespace *)
	(File.prefix, File.namespace); (* file properties *)
      ]
    val mutable languages = ["en"]

    method base = base
    method xmlns =
      if List.mem_assoc ":" xmlns
      then xmlns
      else (":", base^"#")::xmlns
    method languages = languages

    method as_context = (self :> Syntax.context)

    method set_base uri =
      if uri <> base
      then
	Tarpit.effect (fun obs ->
	  self#log_append_command (Base uri);
	  base <- uri)

    method add_prefix pre uri =
      if not (List.mem (pre,uri) xmlns)
      then
	Tarpit.effect (fun obs ->
	  self#log_append_command (Prefix (pre,uri));
	  xmlns <- (pre,uri)::List.remove_assoc pre xmlns)


    method get_property uri =
      if Term.is_arg uri then begin
	super#add_axiom_triple (Rdf.URI uri) Rdf.uri_type (Rdf.URI Rdf.uri_Property);
	super#add_axiom_triple (Rdf.URI uri) Rdfs.uri_subPropertyOf (Rdf.URI Term.uri_argAny);
	super#add_axiom_triple (Rdf.URI uri) Rdf.uri_type (Rdf.URI Term.uri_ArgProperty) end
      else if Term.is_functorArg uri then begin
	super#add_axiom_triple (Rdf.URI uri) Rdf.uri_type (Rdf.URI Rdf.uri_Property);
	super#add_axiom_triple (Rdf.URI uri) Rdfs.uri_domain (Rdf.URI Term.uri_ImplicitFunctor);
	super#add_axiom_triple (Rdf.URI uri) Rdfs.uri_range (Rdf.URI Term.uri_ArgProperty)
      end;
      super#get_property uri

	(* axioms *)

    method is_subclass_of ~obs c1 c2 =
      self#rdfs_subClassOf#relation#has_instance ~obs (Rdf.URI c1) (Rdf.URI c2)
    method is_subproperty_of ~obs p1 p2 =
      self#rdfs_subPropertyOf#relation#has_instance ~obs (Rdf.URI p1) (Rdf.URI p2)

    method axiom_subentity e1 e2_opt =
      Tarpit.effect (fun obs ->
	List.iter self#tell (axiom_subentity ~obs self e1 e2_opt))

    method axiom_subclass c1 c2_opt =
      Tarpit.effect (fun obs ->
	List.iter self#tell (axiom_subclass ~obs self c1 c2_opt))

    method axiom_subproperty p1 p2_opt =
      Tarpit.effect (fun obs ->
	List.iter self#tell (axiom_subproperty ~obs self p1 p2_opt))

    method add_axiom f g = axiom_p1 self f g

	(* database *)

(*
   method save : unit =
   match filename with
   | None -> failwith "Lisq.save: no filename specified"
   | Some fn -> failwith "Lisql.save: undefined"

   method save_as (fn : string) : unit =
   filename <- Some fn;
   failwith "Lisql.save_as: undefined"
 *)

    method get_stat : stat =
      failwith "Lisql.get_stat: undefined"

	(* import and export *)
	
(*
    method import_logfile ~src filename = Common.prof "Lisql.store#import_logfile" (fun () ->
      import_logfile ~src self filename)
*)

    method import_rdf ~base filename = Common.prof "Lisql.store#import_rdf" (fun () ->
      Tarpit.effect (fun obs ->
	self#log_append_command (Import (base,filename));
	self#import_rdf_file ~base filename))

    method import_uri (uri : Uri.t) : unit = (* import linked data about [uri] *)
      Tarpit.effect (fun obs ->
	self#log_append_command (LinkedData uri);
	match Uri.as_filename uri with
	| None ->
	    let rdf_file = Filename.temp_file "sewelis" ".rdf" in
	    print_endline "HTTP GET of RDF data";
	    Sys.command ("wget --header \"Accept:application/rdf+xml\" -O \"" ^ rdf_file ^ "\" \"" ^ uri ^ "\"");
	    self#import_rdf_file ~base:uri rdf_file;
	    Sys.remove rdf_file
	| Some filename -> ())
	(* extract facts about this filename *)

    method remove_entity (name : Name.t) : unit = Common.prof "Lisql.store#remove_entity" (fun () ->
      Tarpit.effect (fun obs ->
	let c = Call (Proc.RemoveName, [|name_s1 name|]) in
	self#log_append_command (Command c);
	super#remove_entity name))

(* TO BE REVISED... *)
(*
   method import_xml (abs_path : string) : unit =
   import_file_tree
   ~f_file:(fun xml_file ->
   try import_xml_file self xml_file
   with _ -> ())
   abs_path
 *)

    method import_path (abs_path : string) : unit =
      import_file_tree
	~f_dir:(import_directory self)
	~f_file:(import_file self)
	abs_path

    method export_rdf ~base ~xmlns filename =
      self#export_rdf_file ~base ~xmlns filename

	(* parsing and printing *)

    method quote_class c : Name.t =
      let s = Syntax.string_of_class c in
      Rdf.Literal (s, Rdf.Typed Namespace.uri_Class)

    method quote_assertion (c : c) : Name.t =
      let str = Syntax.string_of_assertion c in
      Rdf.Literal (str, Rdf.Typed Namespace.uri_Assertion)

    method unquote_assertion (str : string) : c =
      Syntax.assertion_of_string str

	(* queries and updates *)

    method results ~obs (c : c) : Semantics.results =
      Semantics.results ~obs self c
	
    method run (c : c) : unit =
      Semantics.run self c;
      self#log_append_command (Command c);
      self#pagerank_iterations 1

    method tell (s : s) : unit =
      self#run (Assert s)

    method retract (s : s) : unit =
      self#tell (Semantics.retract_s s)

    method move_obj (oid : Extension.oid) (c : p1) : unit =
      let a = name_is (self#get_name oid) c in
      self#tell a

    method add_type (s : Name.t) (c : Uri.t) : unit =
      Common.prof "Lisql.store#add_type" (fun () ->
	let a = name_is s (has_type c) in
	self#tell a)

    method remove_type (s : Name.t) (c : Uri.t) : unit =
      Common.prof "Lisql.store#remove_type" (fun () ->
	let a = name_is s (Not (has_type c)) in
	self#tell a)

    method toggle_type (s : Name.t) (c : Uri.t) : unit =
      Common.prof "Lisql.store#toggle_type" (fun () ->
	Tarpit.effect (fun obs ->
	  let a =
	    if (self#get_class c)#relation#has_instance ~obs s
	    then name_is s (Not (has_type c))
	    else name_is s (has_type c) in
	  self#tell a))


    method add_value (s : Name.t) (p : Uri.t) (o : Name.t) : unit =
      Common.prof "Lisql.store#add_value" (fun () ->
	let a = name_is s (prop_name p o) in
	self#tell a)

    method remove_value (s : Name.t) (p : Uri.t) (o : Name.t) : unit =
      Common.prof "Lisql.store#remove_value" (fun () ->
	let a = name_is s (prop_name p o) in
	self#retract a)

    method set_value (s : Name.t) (p : Uri.t) (o : Name.t) : unit = (* for functional properties *)
      Common.prof "Lisql.store#set_value" (fun () ->
	List.iter
	  (fun prev_o ->
	    let remove_previous_value = name_is s (Not (prop_name p prev_o)) in
	    self#tell remove_previous_value)
	  (self#get_objects ~obs:Tarpit.blind_observer s p);
	let add_new_value = name_is s (prop_name p o) in
	self#tell add_new_value)

    method set_label (uri : Uri.t) (label : string) : unit =
      self#set_value (Rdf.URI uri) Rdfs.uri_label (Rdf.Literal (label, Rdf.Plain "en"))
    method set_inverseLabel (uri : Uri.t) (label : string) : unit =
      self#set_value (Rdf.URI uri) Namespace.uri_inverseLabel (Rdf.Literal (label, Rdf.Plain "en"))
    method add_draft (c : c) : unit =
      let quoted_a = self#quote_assertion c in
      self#add_value (Rdf.URI Namespace.uri_DraftBag) Rdfs.uri_member quoted_a
    method add_draft_for_uri (uri : Uri.t) : unit =
      self#add_draft (Assert (uri_s uri))

    method create_blank (c_opt : Uri.t option) (lpo : (Uri.t * Name.t) list) : unit =
      Common.prof "Lisql.store#create_blank" (fun () ->
	let a =
	  Is (Option.fold (fun c -> an (has_type c)) top_s1 c_opt,
	      simpl_and_list (List.map (fun (p,o) -> prop_name p o) lpo)) in
	self#tell a)

    method define_functor ~(uri : Uri.t) ?(label : string option)
	~(arity : int) ?(implicit : (Uri.t list * Uri.t array) option)
	~(functor_type : Uri.t) ~(precedence : Uri.t) : int =
      Common.prof "Lisql.store#define_functor" (fun () ->
	let arity, ld =
	  match implicit with
	  | None ->
	      print_endline "An explicit functor";
	      (if List.mem functor_type [Term.uri_IntransitiveVerb; Term.uri_PrefixOperator; Term.uri_PostfixOperator] then 1
	      else if List.mem functor_type [Term.uri_TransitiveVerb; Term.uri_InfixOperator] then 2
	      else arity),
	      []
	  | Some (types, args) ->
	      print_endline "An implicit functor";
	      Array.length args,
	      has_type Term.uri_ImplicitFunctor ::
	      List.map (fun c ->
		prop_name Term.uri_functorType (Rdf.URI c))
		types @
	      Common.fold_for
		(fun i res ->
		  prop_name (Term.uri_functorArg i) (Rdf.URI args.(i-1)) :: res)
		1 (Array.length args)
		[] in
	let ld =
	  Option.fold (fun label -> [prop_name Rdfs.uri_label (Name.plain_literal label "en")]) [] label @
	  has_type functor_type ::
	  prop_name Term.uri_arity (Name.typed_literal (string_of_int arity) Xsd.uri_integer) ::
	  (if precedence = Term.uri_maxPrecedence then [] else [prop_name Term.uri_precedence (Rdf.URI precedence)]) @
	  ld in
	let a = name_is (Rdf.URI uri) (And ld) in
	self#tell a;
	arity)

	(* querying *)

    method description ~obs (n : Name.t) : s1 =
      Common.prof "Lisql.store#description" (fun () ->
	description ~obs self n)

    method get_subjects ~obs (p : Uri.t) (o : Name.t) : Name.t list =
      Common.prof "Lisql.store#get_subjects" (fun () ->
	let e : Ext.t = (self#get_property p)#relation#subjects ~obs o in
	  (* self#tab_extent ~obs (prop_name p o) in *)
	Ext.map self#get_name e)

    method choose_subject ~obs (p : Uri.t) (o : Name.t) : Name.t option =
      Common.prof "Lisql.store#choose_subject" (fun () ->
	let e = (self#get_property p)#relation#subjects ~obs o in
	try Some (self#get_name (Ext.choose e))
	with _ -> None)

    method get_objects ~obs (s : Name.t) (p : Uri.t) : Name.t list =
      Common.prof "Lisql.store#get_objects" (fun () ->
	let e : Ext.t = (self#get_property p)#relation#objects ~obs s in
	  (*self#tab_extent ~obs (inv_prop_name p s) in*)
	Ext.map self#get_name e)

    method choose_object ~obs (s : Name.t) (p : Uri.t) : Name.t option =
      Common.prof "Lisql.store#choose_object" (fun () ->
	let e = (self#get_property p)#relation#objects ~obs s in
	try Some (self#get_name (Ext.choose e))
	with _ -> None)

    method get_arity ~obs (funct : Name.t) : int option =
      Common.prof "Lisql.store#get_arity" (fun () ->
	match self#choose_object ~obs funct Term.uri_arity with
	  | Some (Rdf.Literal (s, Rdf.Typed dt)) when dt = Xsd.uri_integer ->
	    (try Some (int_of_string s) with _ -> None)
	  | _ -> None)

    method get_precedence ~obs (op : Uri.t) : Uri.t =
      Common.prof "Lisql.store#get_precedence" (fun () ->
	match self#choose_object ~obs (Rdf.URI op) Term.uri_precedence with
	| Some (Rdf.URI prec) -> prec
	| _ -> Term.uri_maxPrecedence)

    method get_image ~obs name =
      Common.prof "Lisql.store#get_image" (fun () ->
	match name with
	  | Rdf.URI uri when Filename.check_suffix uri ".jpg" -> Some uri
	  | _ ->
	    match self#choose_object ~obs name Foaf.uri_img with
	      | Some (Rdf.URI uri) when Filename.check_suffix uri ".jpg" -> Some uri
	      | _ -> None)

    method get_home_focus ~obs : focus option =
      match self#choose_object ~obs (Rdf.URI Namespace.uri_HomeQuery) Rdf.uri_value with
      | Some (Rdf.Literal (s, Rdf.Typed t)) when t = Namespace.uri_Assertion ->
	  let a = Syntax.assertion_of_string s in
	  let foc = focus_of_command a in
	  Some (focus_first_postfix ~filter:Transf.focus_default_filter foc)
      | _ -> None


    val h_ext_views : (p1, Ext.t Tarpit.view) Hashtbl.t = Hashtbl.create 13

    method tab_extent ~obs (q : p1) : Ext.t =
      Common.prof "Lisql.store#tab_extent" (fun () ->
	if q = Thing then
	  self#extent_all ~obs
	else
	  let ext_view =
	    try
	      Hashtbl.find h_ext_views q
	    with Not_found ->
	      let ext_view = new Tarpit.view ("tab_extent of " ^ Syntax.string_of_class q) in
	      ext_view#define (fun obs -> Common.prof "Lisql.store#tab_extent/define" (fun () ->
(*prerr_string "tab_extent: "; prerr_endline (Syntax.string_of_class q);*)
		match q with
		| Arg (Pred.Type c,1,_) ->
		    (self#get_class c)#relation#instances ~obs
		| Arg (Pred.Role (p,modifs),i,args) when modifs = Pred.default_role_modifs ->
		    let np = args.(Argindex.opposite i) in
		    let ikind = Argindex.to_kind_role i in
		    ( match np with
		      | Det (Name n, _) ->
			( match ikind with
			  | `Subject -> (self#get_property p)#relation#subjects ~obs n
			  | `Object -> (self#get_property p)#relation#objects ~obs n )
		      | Det (Qu (An, _), Thing) ->
			( match ikind with
			  | `Subject -> (self#get_property p)#relation#domain ~obs
			  | `Object -> (self#get_property p)#relation#range ~obs )
		      | _ -> Semantics.extent_p1 ~obs self q )
		| Arg (Pred.Funct funct,0,args) when Array.fold_left (fun ok arg -> ok && arg = top_s1) true args ->
		  (self#get_functor funct (Array.length args))#relation#domain ~obs
		| _ -> Semantics.extent_p1 ~obs self q ));
	      Hashtbl.add h_ext_views q ext_view;
	      ext_view in
	  ext_view#contents ~obs)

    val h_safe_sc_views : (int * Uri.t, Uri.t list Semantics.safe Tarpit.view) Hashtbl.t = Hashtbl.create 13

    method tab_safe_super_classes ~obs (d : int) (c : Uri.t) : Uri.t list Semantics.safe =
      Common.prof "Lisql.store#tab_safe_super_classes" (fun () ->
	let safe_sc_view =
	  try
	    Hashtbl.find h_safe_sc_views (d,c)
	  with Not_found ->
	    let safe_sc_view = new Tarpit.view "safe_super_classes" in
	    safe_sc_view#define (fun obs ->
	      Semantics.Relax.safe_super_classes ~obs self d c);
	    Hashtbl.add h_safe_sc_views (d,c) safe_sc_view;
	    safe_sc_view in
	safe_sc_view#contents ~obs)

    val h_safe_sp_views : (int * Uri.t, Uri.t list Semantics.safe Tarpit.view) Hashtbl.t = Hashtbl.create 13

    method tab_safe_super_props ~obs (d : int) (p : Uri.t) : Uri.t list Semantics.safe =
      Common.prof "Lisql.store#tab_safe_super_props" (fun () ->
	let safe_sp_view =
	  try
	    Hashtbl.find h_safe_sp_views (d,p)
	  with Not_found ->
	    let safe_sp_view = new Tarpit.view "safe_super_props" in
	    safe_sp_view#define (fun obs ->
	      Semantics.Relax.safe_super_props ~obs self d p);
	    Hashtbl.add h_safe_sp_views (d,p) safe_sp_view;
	    safe_sp_view in
	safe_sp_view#contents ~obs)

    val h_safe_ext_views : (int * p1, Ext.t Semantics.safe Tarpit.view) Hashtbl.t = Hashtbl.create 101
    method tab_safe_extent ~obs (d : int) (q : p1) : Ext.t Semantics.safe =
      Common.prof "Lisql.store#tab_safe_extent" (fun () ->
	let safe_ext_view =
	  try
	    Hashtbl.find h_safe_ext_views (d,q)
	  with Not_found ->
	    let safe_ext_view = new Tarpit.view "safe_extent" in
	    safe_ext_view#define (fun obs ->
	      Semantics.Relax.safe_extent ~obs self d q);
	    Hashtbl.add h_safe_ext_views (d,q) safe_ext_view;
	    safe_ext_view in
	safe_ext_view#contents ~obs)

    val h_safe_ext_struct_views : (int * Uri.t * int * int * s1 list, Ext.t Semantics.safe Tarpit.view) Hashtbl.t = Hashtbl.create 101
    method tab_safe_extent_struct ~obs (d : int) (funct : Uri.t) (arity : int) (i : int) (args : s1 list) : Ext.t Semantics.safe =
      Common.prof "Lisql.store#tab_safe_extent_struct" (fun () ->
	let safe_ext_struct_view =
	  let key = (d,funct,arity,i,args) in
	  try
	    Hashtbl.find h_safe_ext_struct_views key
	  with Not_found ->
	    let safe_ext_struct_view = new Tarpit.view "safe_extent_struct" in
	    safe_ext_struct_view#define (fun obs ->
	      Semantics.Relax.safe_extent_struct ~obs self d funct arity i args);
	    Hashtbl.add h_safe_ext_struct_views key safe_ext_struct_view;
	    safe_ext_struct_view in
	  safe_ext_struct_view#contents ~obs)

    method extent_all ~obs : Ext.t =
      self#all_oids ~obs

    method bindings ~obs (vs : Extension.var list) (q : p1) : Extension.map list =
      Semantics.extent_fold ~obs self
	(fun res m ->
	  let m' = List.filter (fun (v,n) -> List.mem v vs) m in
	  m'::res)
	[] [] q

    method table ~obs (ext : Ext.t) (lf : (string * p1) list) : (Extension.oid * (string * Ext.t) list) list =
      Ext.fold
	(fun res o ->
	  let lfo =
	    List.fold_left
	      (fun l (f,c) -> (f, Semantics.apply ~obs self c o)::l)
	      [] lf in
	  (o,lfo)::res)
	[] ext


    val features_view : (Feature.feature list * (Ext.t * Ext.t * Ext.t * Ext.t)) Tarpit.incremental_view =
      new Tarpit.incremental_view "features" ([], (Ext.empty, Ext.empty, Ext.empty, Ext.empty))
    method features ~obs =
      fst (features_view#contents ~obs)
    initializer
      let q_label = make_role Rdfs.uri_label `Object in
      let q_class = has_type Rdfs.uri_Class in
      let q_prop = has_type Rdf.uri_Property in
      let q_funct = has_type Term.uri_Functor (*role Bwd Term.uri_functor*) in
      features_view#define
	(fun obs (lf, (ext_name0, ext_class0, ext_prop0, ext_funct0)) -> Common.prof "Lisql.features/define" (fun () ->
	  let ext_name : Ext.t = Ext.diff (self#all_oids ~obs) (self#tab_extent ~obs q_label) in
	  let ext_class : Ext.t = self#tab_extent ~obs q_class in
	  let ext_prop : Ext.t = self#tab_extent ~obs q_prop in
	  let ext_funct : Ext.t = self#tab_extent ~obs q_funct in
	  let delta_name = Ext.diff ext_name ext_name0 in
	  let delta_class = Ext.diff ext_class ext_class0 in
	  let delta_prop = Ext.diff ext_prop ext_prop0 in
	  let delta_funct = Ext.diff ext_funct ext_funct0 in
	  let lf =
	    if lf = [] (* at first definition *)
	    then
	      self#fold_primitives
		(fun (op,arity) prim res ->
		  Common.fold_for
		    (fun i res ->
		      new Feature.feature_prim self op arity i :: res)
		    1 arity res)
		[] 
	    else lf in
	  let lf : Feature.feature list =
	    Ext.fold
	      (fun lf o ->
		let n = self#get_name o in
		(new Feature.feature_name self n) :: lf)
	      lf delta_name in
	  let lf =
	    Ext.fold
	      (fun lf o ->
		match self#get_name o with
		| Rdf.URI uri -> (new Feature.feature_type self uri) :: lf
		| _ -> lf)
	      lf delta_class in
	  let lf =
	    Ext.fold
	      (fun lf o ->
		match self#get_name o with
		| Rdf.URI uri as n ->
		    if self#owl_SymmetricProperty#relation#has_instance ~obs n
		    then (new Feature.feature_role self `Subject uri) :: lf
		    else (new Feature.feature_role self `Subject uri) :: (new Feature.feature_role self `Object uri) :: lf
		| _ -> lf)
	      lf delta_prop in
	  let lf =
	    Ext.fold
	      (fun lf o ->
		let n = self#get_name o in
		match n, self#get_arity ~obs n with
		| Rdf.URI funct, Some ar_args ->
		    Common.fold_for
		      (fun i lf1 ->
			(new Feature.feature_funct self funct ar_args i) :: lf1)
		      0 ar_args
		      lf
		| _ -> lf)
	      lf delta_funct in
	  lf, (ext_name, ext_class, ext_prop, ext_funct)))

    method place ~obs (foc : focus) = new place ~obs self foc

    method init =
      super#init;
      (* base knowledge *)
      let triples_pos lpos =
	List.rev (
	  List.fold_left (fun acc (p,los) ->
	    List.fold_left (fun acc (o_uri,ls) ->
	      let o = Rdf.URI o_uri in
	      List.fold_left (fun acc s_uri ->
		let s = Rdf.URI s_uri in
		(s,p,o) :: acc
	      ) acc ls
	    ) acc los
	  ) [] lpos
	) in
      let triples_spo lspo =
	List.rev (
	  List.fold_left (fun acc (s_uri,lpo) ->
	    let s = Rdf.URI s_uri in
	    List.fold_left (fun acc (p,lo) ->
	      List.fold_left (fun acc o ->
		(s,p,o) :: acc
	      ) acc lo
	    ) acc lpo
	  ) [] lspo
	) in
      let axiom_triples =
	triples_pos
	  [Rdf.uri_type, [Rdfs.uri_Class, [Term.uri_Term; Term.uri_Functor; Term.uri_ImplicitFunctor; Term.uri_ArgProperty;
					   Term.uri_Verb; Term.uri_IntransitiveVerb; Term.uri_TransitiveVerb;
					   Term.uri_Operator; Term.uri_InfixOperator; Term.uri_PrefixOperator; Term.uri_PostfixOperator; Term.uri_MixfixOperator; Term.uri_LeftAssociativeInfixOperator; Term.uri_RightAssociativeInfixOperator;
					   Namespace.uri_Assertion];
			  Rdfs.uri_Datatype, [Namespace.uri_Assertion];
			  Rdf.uri_Property, [Term.uri_functor; Term.uri_arity; Term.uri_sublist; Term.uri_subterm; Term.uri_argAny; Term.uri_functorType;
					     File.uri_directory;
					     Foaf.uri_img;
					     Namespace.uri_inverseLabel];
			  Owl.uri_TransitiveProperty, [Term.uri_subterm; Term.uri_sublist; File.uri_directory];
			  Term.uri_Functor, [Term.uri_nil; Term.uri_cons; Term.uri_stat]
			 ];
	   Rdfs.uri_subClassOf, [Term.uri_Functor, [Term.uri_ImplicitFunctor; Term.uri_Verb; Term.uri_Operator];
				 Term.uri_Verb, [Term.uri_IntransitiveVerb; Term.uri_TransitiveVerb];
				 Term.uri_Operator, [Term.uri_InfixOperator; Term.uri_PrefixOperator; Term.uri_PostfixOperator; Term.uri_MixfixOperator];
				 Term.uri_InfixOperator, [Term.uri_LeftAssociativeInfixOperator; Term.uri_RightAssociativeInfixOperator];
				 Rdf.uri_Property, [Term.uri_ArgProperty]
				];
	   Rdfs.uri_subPropertyOf, [Term.uri_subterm, [Term.uri_argAny];
				    Term.uri_sublist, [Rdf.uri_rest]
				   ];
	  ]
	@ triples_spo
	  [Term.uri_functor, [Rdfs.uri_domain, [Rdf.URI Term.uri_Term];
			      Rdfs.uri_range, [Rdf.URI Term.uri_Functor]];
	   Term.uri_functorType, [Rdfs.uri_domain, [Rdf.URI Term.uri_ImplicitFunctor];
				  Rdfs.uri_range, [Rdf.URI Rdfs.uri_Class]];
	   Term.uri_cons, [Term.uri_arity, [Rdf.Literal ("2", Rdf.Typed Xsd.uri_integer)];
			   Term.uri_functorArg 1, [Rdf.URI Rdf.uri_first];
			   Term.uri_functorArg 2, [Rdf.URI Rdf.uri_rest]];
	   Term.uri_nil, [Term.uri_arity, [Rdf.Literal ("0", Rdf.Typed Xsd.uri_integer)]];
	   Term.uri_stat, [Term.uri_arity, [Rdf.Literal ("3", Rdf.Typed Xsd.uri_integer)];
			   Term.uri_functorArg 1, [Rdf.URI Rdf.uri_subject];
			   Term.uri_functorArg 2, [Rdf.URI Rdf.uri_predicate];
			   Term.uri_functorArg 3, [Rdf.URI Rdf.uri_object]];
	   Rdfs.uri_subClassOf, [Namespace.uri_inverseLabel, [Rdf.Literal ("subclass", Rdf.Plain "en")]];
	   Rdfs.uri_subPropertyOf, [Namespace.uri_inverseLabel, [Rdf.Literal ("subproperty", Rdf.Plain "en")]]
	  ] in
      List.iter (fun (s,p,o) -> self#add_axiom_triple s p o) axiom_triples;
      (* perform PageRank iterations *)
      self#pagerank_iterations 5;
      (* load user knowledge *)
      log#iter
	(fun s ->
	  print_string "parsing logline: "; print_endline s;
	  let cmd = Syntax.parse_of_string ~ctx:self#as_context Syntax.parse_command s in
	  eval_command self cmd)

  end


let store_initializers : (store -> unit) list ref = ref []
let add_store_initializer (f : store -> unit) = store_initializers := f::!store_initializers
let init store =
  store#init;
  List.iter (fun f -> f store) !store_initializers

let create_store ~base log_file = Common.prof "Lisql.create_store" (fun () ->
  let store = new store ~base log_file in
  init store;
  store)

let load_store db_file = Common.prof "Lisql.load_store" (fun () ->
  let filename = db_file ^ ".db" in
  let ch = open_in_bin filename in
  let store = (Marshal.from_channel ch : store) in
  close_in ch;
  store)

(* store opening with cascading fallbacks *)
let open_store ~base path = Common.prof "Lisql.open_store" (fun () ->
  try (* loading from binary *)
    let ch = open_in_bin (path ^ ".db") in
    try
      let store = (Marshal.from_channel ch : store) in
      close_in ch;
      store
    with exn ->
      close_in ch;
      raise exn
  with exn -> (* deprecated binary file, Sewelis changed version, reloading from .log *)
    prerr_endline (Printexc.to_string exn);
    let path_log = path ^ ".log" in
    try
      let store = new store ~base path in
      init store;
      store
    with exn -> (* failing to read log, renaming .log, importing .nt in fresh .log *)
      prerr_endline (Printexc.to_string exn);
      try
	Sys.rename path_log (Filename.temp_file ~temp_dir:(Filename.dirname path) (Filename.basename path ^ "_backup") ".log");
	let store = new store ~base path in
	init store;
	store#import_ntriples (path ^ ".nt");
	store
      with exn ->
	prerr_endline (Printexc.to_string exn);
	failwith ("The store at " ^ path ^ " could not be open"))

