(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Name = Name.Make
module Extension = Extension.Make

module Elt =
  struct
    type t = string * Rdf.datatype
    let from_name n =
      match n with
      | Rdf.Literal (s, dt) ->
	  ( match dt with
	  | Rdf.Plain _ -> Some (s,dt)
	  | Rdf.Typed uri when uri = Xsd.uri_string -> Some (s,dt)
	  | _ -> None)
      | _ -> None
    let to_name (s,dt) = Rdf.Literal (s,dt)
    let regexp (s,_) = Str.regexp s
    let regexp_string (s,_) = Str.regexp_string_case_fold s
    let matches (s,_) re = Str.string_match re s 0
    let contains (s,_) re = try ignore (Str.search_forward re s 0); true with _ -> false
  end

type find_elt_return = NoVar | UnboundVar | InvalidVal | Val of Elt.t

let find_elt x m : find_elt_return =
  Common.prof "Store_match.find_elt" (fun () ->
    if x = ""
    then NoVar
    else
      try
	match Elt.from_name (List.assoc x m) with
	| Some e -> Val e
	| None -> InvalidVal
      with Not_found -> UnboundVar)
    
class store =
  object (self)
    val state : Elt.t list Tarpit.state = new Tarpit.state []

    method add : Name.t -> unit =
      fun n -> Common.prof "Store_match.store#add" (fun () ->
	Option.iter
	  (fun e -> state#update (fun l -> Some (e::l)))
	  (Elt.from_name n))

    method remove : Name.t -> unit =
      fun n -> Common.prof "Store_match.store#remove" (fun () ->
	Option.iter
	  (fun e -> state#update (fun l -> Some (List.filter ((<>) e) l)))
	  (Elt.from_name n))

    method private matches_gen name f_regexp f_matches ~(obs : Tarpit.observer) (x : Extension.var) (y : Extension.var) =
      object
	inherit Extension.t
	method vars = [x;y]
	method string = name ^ "(" ^ x ^ "," ^ y ^ ")"
	method fold =
	  fun store ff acc m ->
	    Common.prof ("Store_match.store#" ^ name) (fun () ->
	      match find_elt x m with
	      | Val e1 ->
		  ( match find_elt y m with
		  | Val e2 ->
		      if f_matches e1 (f_regexp e2)
		      then ff acc m
		      else acc
		  | InvalidVal -> acc
		  | NoVar -> ff acc m
		  | UnboundVar -> acc)
	      | InvalidVal -> acc
	      | NoVar ->
		  ( match find_elt y m with
		  | Val e2 -> ff acc m
		  | InvalidVal -> acc
		  | NoVar -> ff acc m
		  | UnboundVar -> acc)
	      | UnboundVar ->
		  ( match find_elt y m with
		  | Val e2 ->
		      let re = f_regexp e2 in
		      List.fold_left
			(fun acc e1 ->
			  if f_matches e1 re
			  then ff acc ((x, Elt.to_name e1)::m)
			  else acc)
			acc (state#contents ~obs)
		  | InvalidVal -> acc
		  | NoVar ->
		      List.fold_left
			(fun acc e1 ->
			  ff acc ((x, Elt.to_name e1)::m))
			acc (state#contents ~obs)
		  | UnboundVar -> acc))
      end

    method matches_extension ~obs x y = self#matches_gen "matches" Elt.regexp Elt.contains ~obs x y

    method contains_extension ~obs x y = self#matches_gen "contains" Elt.regexp_string Elt.contains ~obs x y

  end
