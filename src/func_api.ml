(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

module Name = Name.Make

(** {1 Exported types} *)

type uri = Uri.t (* string *)
    (** The type of URIs. Simply an abstraction of [string]. Caution: use only absolute URIs. *)
type term = Rdf.thing
(* 
  | URI of uri
  | XMLLiteral of Xml.xml
  | Literal of (string * (Plain of string | Typed of uri))
 *)
  (** The type of RDF terms. The most important variants are: URIs, plain literals, and typed literals. *)

let resolve_uri (store : Lisql.store) : uri -> uri =
  fun uri -> Uri.resolve store#base store#xmlns uri
let resolve_term (store : Lisql.store) : term -> term =
  fun term ->
    match term with
    | Rdf.URI uri -> Rdf.URI (resolve_uri store uri)
    | Rdf.XMLLiteral _ -> term
    | Rdf.Literal (s, Rdf.Plain lang) -> term
    | Rdf.Literal (s, Rdf.Typed uri) -> Rdf.Literal (s, Rdf.Typed (resolve_uri store uri))
    | Rdf.Blank _ -> term

type results =
  { results_columns : string list;
    results_rows : term list list }
  (** The type of query results. Each line is a list of terms, aligned with the list of columns. *)

type transf = Lisql.Transf.kind
(*
  | FocusUp
  | FocusDown
  | FocusLeft
  | FocusRight
  | FocusTab
  | InsertAnd
  | InsertOr
  | InsertAndNot
  | InsertAndMaybe
  | InsertQu of (An | The | Every | Only | No)
  | InsertIsThere
  | InsertEachIsThere
  | ToggleNot
  | ToggleMaybe
  | ToggleOpt
  | ToggleTrans
  | ToggleSym
  | Describe
  | Select
  | Delete
*)
    (** The type of statement transformations. An enumerated type. *)
type increment_kind = Lisql.Feature.kind
(*
  | Kind_Something
  | Kind_Variable
  | Kind_Entity
  | Kind_Literal
  | Kind_Thing
  | Kind_Class
  | Kind_Operator
  | Kind_Property
  | Kind_InverseProperty
  | Kind_Structure
  | Kind_Argument
*)
    (** The type of increment kinds. An enumerated type. *)
type column = Lisql.Extension.var (* string *)
    (** The type of columns in the answer view. Simple an abstraction of [string]. *)
type column_order = Lisql.order (* [ `DEFAULT | `DESC | `ASC ] *)
    (** The type of column orderings. An enumerated type. *)
type column_aggreg = Lisql.aggreg (* [ `INDEX | `DISTINCT_COUNT | `SUM | `AVG ] *)
    (** The type of column aggregations. An enumerated type. *)

type 'a tree = Node of 'a * 'a tree list
    (** A polymorphic type of n-ary trees. *)

type focus_id = int
    (** The type of foci. Simply an abstraction of [int]. *)
type display =
    token list
and token =
    [ `Space
    | `Kwd of string
    | `Var of string
    | `URI of Uri.t * uri_kind * string * Uri.t option
    | `Prim of string
    | `Plain of string * string
    | `Typed of string * Uri.t * string
    | `Xml of Xml.xml
    | `List of display list
    | `Tuple of display list
    | `Seq of display list
    | `And of display list
    | `Or of display list
    | `Not of display
    | `Maybe of display
    | `Brackets of display
    | `Quote of display
    | `Pair of bool * display * display
    | `Focus of focus_id * display ]
and uri_kind = [ `Entity | `Class | `Property | `Functor | `Datatype ]
(** The type of statement and increment displays. Such displays may
    include focus identifiers as decorators. *)
type focused_display = { focus : focus_id; display : display }
(** The type of displays with a active focus. *)

type place_mode = Lisql.place_mode
(*
 [ `Root (* no clue for increments *)
 | `Type (* type clue for increments *)
 | `Val (* val clue for increments *)
 | `Relaxed (* relaxed/update mode *)
 ] *)
(** The type of place modes. *)


(** {1 Constants for internal use} *)

let obs = Tarpit.blind_observer (* clients cannot be notified *)

(** {1 Utilities for internal use} *)

class ['id1,'id2] refs =
object
  val ht : ('id1,'id2 list) Hashtbl.t = Hashtbl.create 13

  method add (x : 'id1) (y : 'id2) : unit =
    let l = try Hashtbl.find ht x with _ -> [] in
    Hashtbl.replace ht x (y::l)
  method get (x : 'id1) : 'id2 list =
    try Hashtbl.find ht x with _ -> []
  method remove (x : 'id1) : unit =
    Hashtbl.remove ht x
end

exception Undefined of string * int

class ['parent,'id,'t] dico (name : string) (refs : ('parent,'id) refs) =
object
  val mutable cpt = 0
  val ht : ('id,'t) Hashtbl.t = Hashtbl.create 13

  method insert ~(parent : 'parent) (x : 't) : 'id =
    cpt <- cpt+1;
    let id = cpt in
    Hashtbl.add ht id x;
    refs#add parent id;
    id
  method get (id : 'id) : 't =
    try Hashtbl.find ht id
    with Not_found -> raise (Undefined (name, id))
  method remove (id : 'id) : unit =
    Hashtbl.remove ht id

(* TODO: method to get parent, and reduce function signatures *)
end


let map_foci (f : Lisql.AST.focus -> int) (toks : Lisql.Display.t) : display =
  let rec aux_list toks =
    List.map aux_token toks
  and aux_token = function
    | `Focus (foc, toks) -> `Focus (f foc, aux_list toks)
    | `Space -> `Space
    | `Kwd w -> `Kwd w
    | `Var v -> `Var v
    | `URI uri -> `URI uri
    | `Prim op -> `Prim op
    | `Plain plain -> `Plain plain
    | `Typed typed -> `Typed typed
    | `Xml xml -> `Xml xml
    | `List ltoks -> `List (List.map aux_list ltoks)
    | `Tuple ltoks -> `Tuple (List.map aux_list ltoks)
    | `Seq ltoks -> `Seq (List.map aux_list ltoks)
    | `And ltoks -> `And (List.map aux_list ltoks)
    | `Or ltoks -> `Or (List.map aux_list ltoks)
    | `Not toks -> `Not (aux_list toks)
    | `Maybe toks -> `Maybe (aux_list toks)
    | `Brackets toks -> `Brackets (aux_list toks)
    | `Quote toks -> `Quote (aux_list toks)
    | `Pair (b,toks1,toks2) -> `Pair (b, aux_list toks1, aux_list toks2)
  in
  aux_list toks

let remove_foci (toks : Lisql.Display.t) : display =
  map_foci (fun foc -> 0) toks

(* Root *)

type root_id = int
let root = 0

(** {1 Stores} *)

type store_id = int
    (** The type for store ids. Simply an abstraction of [int]. *)
let root_stores : (root_id,store_id) refs = new refs
let stores : (root_id,store_id,Lisql.store) dico = new dico "store" root_stores

(** {2 Store creators} *)

let open_store ~(base : Uri.t) ~(filepath : string) : store_id =
  let store = Lisql.open_store ~base filepath in
  stores#insert ~parent:root store
(** [open_store ~base ~filepath] opens the store, where [filepath]
    provides the directory and basename of persistent files, and
    [base] provides a base URI to interpret relative URIs. It returns
    its id.  If the store does not yet exists, it is created.  If the
    binary persistent file is unreadable, the store is recreated from
    the log file.  If the log file cannot be read, the store is
    created anew from the exported N-Triples file. *)
    
let save_store ~(store : store_id) : unit =
  (stores#get store)#save
(** [save_store ~store] saves the current state of [store] as a binary
    persistent file for quick reloading in future sessions. Apart from
    this file, all updates are immediatly written in a logfile, so
    that nothing is lost in case [save_store] fails or in case the
    binary file is unreadable. *)

let export_rdf ~(store : store_id) ?(base : uri option) ?(xmlns : (string * string) list option) ~(filepath : string) () : unit =
  let the_store = stores#get store in
  let base = match base with Some base -> base | None -> the_store#base in
  let xmlns = match xmlns with Some xmlns -> xmlns | None -> the_store#xmlns in
  (stores#get store)#export_rdf ~base ~xmlns filepath
(** [export_rdf ~store ?base ?xmlns ~filepath] exports the RDF
    contents of [store] into [filepath].  The format of the exported
    file will depend on the extension of [filepath] (Turtle for
    [.ttl], N-Triples for [.nt]). In Turtle, [base] (the base URI) and
    [xmlns] (a list of namespace definitions) are used in the RDF
    serialization. *)


(** {2 Store accessors} *)

let store_base ~(store : store_id) : uri =
  (stores#get store)#base
(** [store_base ~store] returns the default base URI of [store]. *)

let store_xmlns ~(store : store_id) : (string * string) list = (* list of (prefix,namespace) *)
  (stores#get store)#xmlns
(** [store_xmlns ~store] returns the default set of namespace
    definitions of [store]. *)

let store_namespace_of_prefix ~(store : store_id) ~(prefix : string) : string option =
  try Some (List.assoc prefix ((stores#get store)#xmlns))
  with Not_found -> None
(** [store_namespace_of_prefix ~store ~prefix] returns the namespace defining [prefix] in [store]. *)

let uri_description ~(store : store_id) ~(uri : uri) : display =
  let the_store = stores#get store in
  let uri = resolve_uri the_store uri in
  let descr = the_store#description ~obs (Rdf.URI uri) in
  let toks = Lisql.Display.of_s1 ~obs the_store descr in
  let toks' = remove_foci toks in
  toks'
(** [uri_description ~store ~uri] returns the structured display of
    an RDF description of [uri]. *)

let results_of_statement ~(store : store_id) ~(statement : string) : results =
  let store = stores#get store in
  let c = Lisql.Syntax.assertion_of_string statement in
  let res = store#results ~obs:Tarpit.blind_observer c in
  { results_columns = res.Lisql.Semantics.vars;
    results_rows = Lisql.Rel.fold
      (fun acc lo -> List.map store#get_name lo :: acc)
      [] res.Lisql.Semantics.relation }
(** [results_of_statement ~store ~statement] returns the results (a table) for the given statement. Warning: not all statements have results. *)
    
(** {2 Store modifiers} *)

let define_base ~(store : store_id) ~(base : uri) : unit =
  (stores#get store)#set_base base
(** [define_base ~store ~base] replaces the default base URI of
    [store] by [base]. *)

let define_namespace ~(store : store_id) ~(prefix : string) ~(uri : uri) : unit =
  (stores#get store)#add_prefix prefix uri
(** [define_namespace ~store ~prefix ~uri] (re-)defines the namespace
    associated to [prefix] by [uri] in [store]. *)

let add_triple ~(store : store_id) ~(s : uri) ~(p : uri) ~(o : term) : unit =
  let store = stores#get store in
  let s, p, o = resolve_uri store s, resolve_uri store p, resolve_term store o in
  store#add_value (Rdf.URI s) p o
(** [add_triple ~store ~s ~p ~o] adds a triple [(s,p,o)] into
    [store]. *)

let remove_triple ~(store : store_id) ~(s : uri) ~(p : uri) ~(o : term) : unit =
  let store = stores#get store in
  let s, p, o = resolve_uri store s, resolve_uri store p, resolve_term store o in
  store#remove_value (Rdf.URI s) p o
(** [remove_triple ~store ~s ~p ~o] removes the triple [(s,p,o)] from
    [store]. *)

let replace_object ~(store : store_id) ~(s : uri) ~(p : uri) ~(o : term) : unit =
  let store = stores#get store in
  let s, p, o = resolve_uri store s, resolve_uri store p, resolve_term store o in
  store#set_value (Rdf.URI s) p o
(** [replace_object ~store ~s ~p ~o] removes any triple [(s,p,_)], and
    adds the triple [(s,p,o)] into [store]. *)

let import_rdf ~(store : store_id) ~(base : uri) ~(filepath : string) : unit =
  (stores#get store)#import_rdf ~base filepath
(** [import_rdf ~store ~base ~filepath] imports the RDF contents of
    [filepath] using [base] as a base URI. Supported file formats are
    RDF/XML (so far, without XML entities), Turtle, and N-Triples. *)

let import_uri ~(store : store_id) ~(uri : uri) : unit =
  let store = stores#get store in
  let uri = resolve_uri store uri in
  store#import_uri uri
(** [import_uri ~store ~uri] imports in [store] the RDF description of
    [uri] obtained by dereferencing it. *)

let run_statement ~(store : store_id) ~(statement : string) : unit =
  let store = stores#get store in
  let c = Lisql.Syntax.assertion_of_string statement in
  store#run c
(** [run_statement ~store ~statement] runs the given statement, performing an update and/or executing commands. Warning: not all statements can be run. *)
    

(** {1 Navigation places} *)

type place_id = int
(** The type of place ids. Simply an abstraction of [int]. *)
let store_places : (store_id,place_id) refs = new refs
let places : (store_id,place_id,Lisql.place) dico = new dico "place" store_places

(** {2 Place creators (navigation starting points)} *)

let get_place_root ~(store : store_id) : place_id =
  let place = (stores#get store)#place ~obs Lisql.AST.focus_top in
  places#insert ~parent:store place
(** [get_place_root ~store] returns the root place of [store]. *)

let get_place_home ~(store : store_id) : place_id =
  let the_store = stores#get store in
  let focus =
    match the_store#get_home_focus ~obs with
      | Some foc -> foc
      | None -> Lisql.AST.focus_top in
  let place = the_store#place ~obs focus in
  places#insert ~parent:store place
(** [get_place_home ~store] returns the home place of [store]. See
    [set_as_home] for how to define the home place. *)

let get_place_bookmarks ~(store : store_id) : place_id =
  let place = (stores#get store)#place ~obs Lisql.AST.focus_bookmarks in
  places#insert ~parent:store place
(** [get_place_bookmarks ~store] returns the place containing all
    bookmarked statements in [store]. *)

let get_place_drafts ~(store : store_id) : place_id =
  let place = (stores#get store)#place ~obs Lisql.AST.focus_drafts in
  places#insert ~parent:store place
(** [get_place_drafts ~store] returns the place containing all draft
    statements in [store]. *)

let get_place_uri ~(store : store_id) ~(uri : uri) : place_id =
  let the_store = stores#get store in
  let uri = resolve_uri the_store uri in
  let place = the_store#place ~obs (Lisql.AST.focus_of_uri uri) in
  places#insert ~parent:store place
(** [get_place_uri ~store ~uri] returns the place containing only the
    entity [uri]. *)

let get_place_statement ~(store : store_id) ~(statement : string) : place_id =
  let c = Lisql.Syntax.assertion_of_string statement in
  let place = (stores#get store)#place ~obs (Lisql.AST.focus_of_command c) in
  places#insert ~parent:store place
(** [get_place_statement ~store ~statement] returns the place
    identified by [statement]. See [statement_string] for how to
    obtain such place identifiers. *)

(** {2 Place accessors} *)

(** {3 Place mode} *)

let place_mode ~(place : place_id) : place_mode =
  (places#get place)#mode

(** {3 Place relaxation} *)

let rank ~(place : place_id) : int =
  (places#get place)#rank
(** [rank ~place] returns the current relaxation rank of
    [place]. i.e., the number of performed relaxations. *)

let has_more ~(place : place_id) : bool =
  (places#get place)#has_more
(** [has_more ~place] returns whether it is possible to apply more
    relaxation. *)

let has_less ~(place : place_id) : bool =
  (places#get place)#has_less
(** [has_less ~place] returns whether it is possible to apply less
    relaxations. *)

type place_relaxation =
    { rank : int;
      has_more : bool;
      has_less : bool }

let place_relaxation ~(place : place_id) : place_relaxation =
  { rank = rank ~place;
    has_more = has_more ~place;
    has_less = has_less ~place }

(** {3 Place statement} *)

let place_foci : (place_id,focus_id) refs = new refs
let foci : (place_id,focus_id, Lisql.AST.focus) dico = new dico "focus" place_foci

let statement_string ~(store : store_id) ~(place : place_id) : string =
  Lisql.Syntax.string_of_assertion ((places#get place)#assertion)
(** [statement_string ~store ~place] returns a string identifying
    [place] in [store]. It is not designed for display, for not
    unambiguous access to a place with [get_place_statement]. *)

let statement_focused_display ~(store : store_id) ~(place : place_id) : focused_display =
  let the_store = stores#get store in
  let the_place = places#get place in
  let toks = Lisql.Display.of_c ~obs the_store the_place#assertion in
  let foc0 = the_place#focus in
  let foc_id_ref = ref 0 in
  let toks' =
    map_foci
      (fun foc -> 
	let id = foci#insert ~parent:place foc in
	if Lisql.AST.same_focus foc foc0 then foc_id_ref := id;
	id)
      toks in
  if !foc_id_ref = 0 then assert false;
  {focus = !foc_id_ref; display = toks'}
(** [statement_focused_display ~store ~place] returns the display of the
    statement of [place], plus the active focus id. *)

type place_statement =
    { string : string;
      focused_display : focused_display }

let place_statement ~store ~place : place_statement =
  { string = statement_string ~store ~place;
    focused_display = statement_focused_display ~store ~place }

(** {3 Place answers} *)

let answers_count ~(place : place_id) : int =
  (places#get place)#answers#count
(** [answers_count ~place] returns the total number of answers in
    [place]. *)
let answers_page_start ~(place : place_id) : int =
  (places#get place)#answers#page_start
(** [answers_page_start ~place] returns the offset of the first answer
    in the current page of answers. *)
let answers_page_end ~(place : place_id) : int =
  (places#get place)#answers#page_end
(** [answers_page_end ~place] returns the offset of the last answer
    in the current page of answers. *)
let answers_page_size ~(place : place_id) : int =
  (places#get place)#answers#page_size
(** [answers_page_size ~place] returns the size of page answers. *)

type answers_paging =
    { count : int;
      start : int;
      ende : int;
      size : int }

let answers_paging ~place : answers_paging =
  { count = answers_count ~place;
    start = answers_page_start ~place;
    ende = answers_page_end ~place;
    size = answers_page_size ~place }


let answers_columns ~(place : place_id) : column list =
  (places#get place)#answers#columns
(** [answers_columns ~place] returns the list of columns in [place]'s answers. *)

let column_hidden ~(place : place_id) ~(column : column) : bool =
  (places#get place)#answers#get_hidden column
(** [column_hidden ~place ~column] returns whether [column] is hidden
    in [place]. *)
let column_order ~(place : place_id) ~(column : column) : column_order =
  (places#get place)#answers#get_order column
(** [column_order ~place ~column] returns the order used in
    [column]. *)
let column_pattern ~(place : place_id) ~(column : column) : string =
  (places#get place)#answers#get_pattern column
(** [column_order ~place ~column] returns the filetring pattern used in
    [column]. *)
let column_aggreg ~(place : place_id) ~(column : column) : column_aggreg =
  (places#get place)#answers#get_aggreg column
(** [column_order ~place ~column] returns the aggregation used in
    [column]. *)

type place_column =
    { name : column;
      hidden : bool;
      order : column_order;
      pattern : string;
      aggreg : column_aggreg }

let place_column ~(place : place_id) ~(column : column) : place_column =
  { name = column;
    hidden = column_hidden ~place ~column;
    order = column_order ~place ~column;
    pattern = column_pattern ~place ~column;
    aggreg = column_aggreg ~place ~column }

type place_columns = place_column list

let place_columns ~(place : place_id) : place_column list =
  List.map
    (fun column -> place_column ~place ~column)
    (places#get place)#answers#columns


type cell_id = int
(** The type of cell ids. Simply an abstraction of [string]. *)
let place_cells : (place_id,cell_id) refs = new refs
let cells : (place_id,cell_id,Lisql.cell) dico = new dico "cell" place_cells

let cell_display ~(cell : cell_id) : display =
  let toks = (cells#get cell)#display in
  let toks' = remove_foci toks in
  toks'
(** [cell_display ~cell] returns the display of [cell].*)

type place_cell =
    { id : cell_id;
      cell_display : display }

let place_cell ~(cell : cell_id) : place_cell =
  { id = cell;
    cell_display = cell_display ~cell }

type place_row = place_cell list

let place_rows ~(place : place_id) : place_row list =
  List.rev
    ((places#get place)#answers#fold ~obs
	(fun res row ->
	  List.map
	    (fun c ->
	      let cell = cells#insert ~parent:place c in
	      place_cell ~cell)
	    row
	  :: res)
	[])
(** [place_rows ~place] returns the table of answers, by row. Each row
    is a list of cell, one for each column. *)

type place_answers =
    { paging : answers_paging;
      columns : place_column list;
      rows : place_row list }

let place_answers ~(place : place_id) : place_answers =
  { paging = answers_paging ~place;
    columns = place_columns ~place;
    rows = place_rows ~place }

(** {3 Place increments and transformations} *)

type increment_id = int
(** The type of increment ids. Simply an abstraction of [int]. *)
let place_increments : (place_id,increment_id) refs = new refs
let increments : (place_id,increment_id, Lisql.Concept.increment) dico = new dico "increment" place_increments

let increment_kind ~(increment : increment_id) : increment_kind =
  (increments#get increment)#feature#kind
(** [increment_kind ~increment] returns the kind of [increment]. *)

let increment_display ~(increment : increment_id) : 'display =
  let the_incr = increments#get increment in
  let toks = the_incr#feature#display ~obs in
  let toks' = remove_foci toks in
  toks'
(** [increment_display ~increment] returns the display of
    [increment]. *)

let increment_ratio ~(increment : increment_id) : int * int =
  (increments#get increment)#supp
(** [increment_ratio ~increment] returns the proportion of answers
    covered by [increment]. *)

let increment_new ~(increment : increment_id) : bool =
  (increments#get increment)#anew
(** [increment_new ~increment] returns whether [increment] is not an
    increment at lower ranks. *)

let increment_uri_opt ~(increment : increment_id) : uri option =
  (increments#get increment)#feature#uri_opt
(** [increment_uri_opt ~increment] returns the URI present in
    [increment], if any. *)

let increment_term_opt ~(increment : increment_id) : term option =
  (increments#get increment)#feature#name_opt
(** [increment_term_opt ~increment] returns the RDF term in
    [increment], if any. *)

let increment_is_lisql ~(increment : increment_id) : bool =
  (increments#get increment)#feature#lisql_opt <> None
(** [increment_is_lisql ~increment] returns whether [increment] is a LISQL literal. *)

type place_increment =
    { increment_id : increment_id;
      kind : increment_kind;
      increment_display : display;
      ratio : int * int;
      is_new : bool;
      uri_opt : uri option;
      term_opt : term option;
      is_lisql : bool }

let place_increment ~increment : place_increment =
  { increment_id = increment;
    kind = increment_kind ~increment;
    increment_display = increment_display ~increment;
    ratio = increment_ratio ~increment;
    is_new = increment_new ~increment;
    uri_opt = increment_uri_opt ~increment;
    term_opt = increment_term_opt ~increment;
    is_lisql = increment_is_lisql ~increment }


let get_increment_entity ~(place : place_id) : increment_id =
  let incr = (places#get place)#increment_s12 in
  increments#insert ~parent:place incr
(** [get_increment_entity ~place] returns the root of the entity
    increment tree in [place]. *)

let get_increment_relation ~(place : place_id) : increment_id =
  let incr = (places#get place)#increment_p12 in
  increments#insert ~parent:place incr
(** [get_increment_relation ~place] returns the root of the relation
    increment tree in [place]. *)

let get_children_increments ~(place : place_id) ~(parent : increment_id) : increment_id list =
  List.map (increments#insert ~parent:place)
    (List.sort (fun i1 i2 -> Lisql.Feature.compare i1#feature i2#feature)
       ((places#get place)#children_increments (increments#get parent)))
(** [get_children_increments ~place ~parent] returns the sorted list
    of children increments of the increment [parent] in [place]. *)

let rec get_increment_tree ~(place : place_id) ~(parent : increment_id) : place_increment tree =
  Node (place_increment parent,
	List.map
	  (fun child -> get_increment_tree ~place ~parent:child) 
	  (get_children_increments ~place ~parent))
(** [get_increment_tree ~place ~parent] returns the increment tree rooted at [parent]. *)

let get_increment_tree_entity ~(place : place_id) : place_increment tree =
  get_increment_tree ~place ~parent:(get_increment_entity ~place)
let get_increment_tree_relation ~(place : place_id) : place_increment tree =
  get_increment_tree ~place ~parent:(get_increment_relation ~place)


let get_transformations ~(place : place_id) : transf list =
  (places#get place)#transformations
(** [get_transformations ~place] returns the set of applicable
    transformations on [place]. *)

let can_insert_entity ~(place : place_id) : bool =
  (places#get place)#insert_s1
(** [can_insert_entity ~place] returns whether entity increments can
    be inserted at [place]. *)

let can_insert_relation ~(place : place_id) : bool =
  (places#get place)#insert_p1
(** [can_insert_relation ~place] returns whether relation increments
    can be inserted at [place]. *)

type place_suggestions =
    { increment_tree_entity : place_increment tree;
      increment_tree_relation : place_increment tree;
      transformations : transf list;
      can_insert_entity : bool;
      can_insert_relation : bool }

let place_suggestions ~(place : place_id) : place_suggestions =
  { increment_tree_entity = get_increment_tree_entity ~place;
    increment_tree_relation = get_increment_tree_relation ~place;
    transformations = get_transformations ~place;
    can_insert_entity = can_insert_entity ~place;
    can_insert_relation = can_insert_relation ~place }


(** {3 Place aggregated content} *)

type place_content =
    { place_id : place_id;
      mode : place_mode;
      relaxation : place_relaxation;
      statement : place_statement;
      answers : place_answers;
      suggestions : place_suggestions }

let place_content ~(store : store_id) ~(place : place_id) : place_content =
  { place_id = place;
    mode = place_mode ~place;
    relaxation = place_relaxation ~place;
    statement = place_statement ~store ~place;
    answers = place_answers ~place;
    suggestions = place_suggestions ~place }


(** {2 Place completions} *)

type completions =
    { partial : bool;
      relaxed : bool;
      completion_increments : place_increment list }
(** The type of completions, as returned by the function
    [get_completions]. Completions are increments. The Boolean field
    [partial] says whether only a subset of valid completions is
    returned. The Boolean field [relaxed] says whether relaxation was
    necessary to find completions. *)

let get_completions ~(place : place_id) ?(max_compl : int option) ?(nb_more_relax : int option) ~(key : string) : completions =
  let partial, relaxed, incrs = (places#get place)#completions ?max_compl ?nb_more_relax key in
  { partial;
    relaxed;
    completion_increments =
      List.map
	(fun incr ->
	  let id = increments#insert ~parent:place incr in
	  place_increment ~increment:id)
	incrs }
(** [get_completions ~place ?max_compl ?nb_more_relax ~key] returns
    completions at [place] matching [key]. The optional parameter
    [max_compl] specifies the maximum number of completions to be
    returned (default is 10). The optional parameter [nb_more_relax]
    specifies how many relaxations must be performed before looking
    for completions (default is 0). *)


(** {2 Place modifiers} *)

(** {3 Statement relaxation} *)

let show_more ~(place : place_id) : unit =
  (places#get place)#more_extent
(** [show_more ~place] increment the relaxion rank of [place]. *)

let show_less ~(place : place_id) : unit =
  (places#get place)#less_extent
(** [show_less ~place] decrement the relaxion rank of [place]. *)

let show_most ~(place : place_id) : unit =
  (places#get place)#most_extent
(** [show_most ~place] sets the relaxation rank of [place] at
    maximum. *)

let show_least ~(place : place_id) : unit =
  (places#get place)#least_extent
(** [show_most ~place] sets the relaxation rank of [place] at
    minimum. *)

(** {3 Place answers} *)

let page_down ~(place : place_id) : bool =
  (places#get place)#answers#page_down
(** [page_down ~place] jumps to the next page of answers. *)
let page_top ~(place : place_id) : bool =
  (places#get place)#answers#page_top
(** [page_top ~place] jumps to the first page of answers. *)
let page_up ~(place : place_id) : bool =
  (places#get place)#answers#page_up
(** [page_up ~place] jumps to the previous page of answers. *)
let page_bottom ~(place : place_id) : bool =
  (places#get place)#answers#page_bottom
(** [page_bottom ~place] jumps to the last page of answers. *)

let set_page_start ~(place : place_id) ~(pos : int) : bool =
  (places#get place)#answers#set_start pos
(** [set_page_start ~place ~pos] sets answer page start of [place] at position [pos]. *)
let set_page_end ~(place : place_id) ~(pos : int) : bool =
  (places#get place)#answers#set_end pos
(** [set_page_end ~place ~pos] sets answer page end of [place] at position [pos]. *)

let move_column_left ~(place : place_id) ~(column : column) : unit =
  (places#get place)#answers#move_column_left column
(** [move_column_left ~place ~column] moves [column] one position to the left. *)
let move_column_right ~(place : place_id) ~(column : column) : unit =
  (places#get place)#answers#move_column_right column
(** [move_column_right ~place ~column] moves [column] one position to the right. *)

let set_column_hidden ~(place : place_id) ~(column : column) ~(hidden : bool) : unit =
  (places#get place)#answers#set_hidden column hidden
(** [set_column_hidden ~place ~column ~hidden] sets whether [column] is hidden in [place]. *)
let set_column_order ~(place : place_id) ~(column : column) ~(order : column_order) : unit =
  (places#get place)#answers#set_order column order
(** set_column_order ~place ~column ~order] uses [order] for ordering [column] in [place]. *)
let set_column_pattern ~(place : place_id) ~(column : column) ~(pattern : string) : unit =
  (places#get place)#answers#set_pattern column pattern
(** set_column_pattern ~place ~column ~order] uses [pattern] for filtering [column] in [place]. *)
let set_column_aggreg ~(place : place_id) ~(column : column) ~(aggreg : column_aggreg) : unit =
  (places#get place)#answers#set_aggreg column aggreg
(** set_column_aggreg ~place ~column ~order] uses [aggreg] for aggregating [column] in [place]. *)


(** {2 Navigation links} *)

let _cd (store : store_id) (the_place : Lisql.place) (foc_opt : Lisql.AST.focus option) : place_id =
  match foc_opt with
    | Some foc -> places#insert ~parent:store (the_place#copy ~obs foc)
    | None -> failwith "This transformation is not applicable"

let do_run ~(store : store_id) ~(place : place_id) : place_id =
  let the_place = places#get place in
  the_place#do_run;
  let foc_opt = Lisql.Transf.focus_after_run the_place#focus in
  match foc_opt with
  | Some foc -> _cd store the_place (Some foc)
  | None -> get_place_home ~store
(** [do_run ~store ~place] runs/asserts the statement of [place] in its [store], and returns a refreshed place. *)

let change_focus ~(store : store_id) ~(place : place_id) ~(focus : focus_id) : place_id =
  let the_place = places#get place in
  let the_focus = foci#get focus in
  _cd store the_place (Some the_focus)
(** [change_focus ~store ~place ~focus] returns a new place after
    moving the focus to [focus]. *)

let apply_transformation ~(store : store_id) ~(place : place_id) ~(transformation : transf) : place_id =
  let the_store = stores#get store in
  let the_place = places#get place in
  let foc_opt = Lisql.Transf.focus_apply the_store transformation the_place#focus in
  _cd store the_place foc_opt
(** [apply_transformation ~store ~place ~transformation] returns a new
    place by applying [transformation] to [place]. *)

let insert_increment ~(store : store_id) ~(place : place_id) ~(increment : increment_id) : place_id =
  let the_store = stores#get store in
  let the_place = places#get place in
  let the_incr = increments#get increment in
  let foc_opt = Lisql.Feature.focus_insert the_store the_incr#feature the_place#focus in
  _cd store the_place foc_opt
(** [insert_increment ~store ~place ~increment] returns a new place by
    inserting [increment] in [place]. *)

let insert_increment_list_and ~(store : store_id) ~(place : place_id) ~(increment_list : increment_id list) : place_id =
  let the_store = stores#get store in
  let the_place = places#get place in
  let the_incrs = List.map increments#get increment_list in
  let foc_opt = Lisql.Feature.focus_insert_list the_store (List.map (fun i -> i#feature) the_incrs) the_place#focus in
  _cd store the_place foc_opt
(** [insert_increment_list_and ~store ~place ~increment_list] returns
    a new place by inserting the conjunction of [increment_list] in
    [place]. *)

let insert_increment_list_or ~(store : store_id) ~(place : place_id) ~(increment_list : increment_id list) : place_id =
  let the_store = stores#get store in
  let the_place = places#get place in
  let the_incrs = List.map increments#get increment_list in
  let foc_opt = Lisql.Feature.focus_insert_list_or the_store (List.map (fun i -> i#feature) the_incrs) the_place#focus in
  _cd store the_place foc_opt
(** [insert_increment_list_or ~store ~place ~increment_list] returns
    a new place by inserting the disjunction of [increment_list] in
    [place]. *)

let insert_increment_list_not ~(store : store_id) ~(place : place_id) ~(increment_list : increment_id list) : place_id =
  let the_store = stores#get store in
  let the_place = places#get place in
  let the_incrs = List.map increments#get increment_list in
  let foc_opt = Lisql.Feature.focus_insert_list_not the_store (List.map (fun i -> i#feature) the_incrs) the_place#focus in
  _cd store the_place foc_opt
(** [insert_increment_list_not ~store ~place ~increment_list] returns
    a new place by inserting the conjunction of the negations of
    [increment_list] in [place]. *)

let unquote_increment ~(store : store_id) ~(place : place_id) ~(increment : increment_id) : place_id =
  let the_place = places#get place in
  let the_increment = increments#get increment in
  let foc_opt = Option.map
    (fun (_,_,foc) -> Lisql.AST.focus_first_postfix ~filter:Lisql.Transf.focus_default_filter foc)
    the_increment#feature#lisql_opt in
  _cd store the_place foc_opt
(** [unquote_increment ~store ~place ~increment] returns the place
    that results from unquoting [increment], if applicable. *)

let _insert_feature store place f =
  let the_store = stores#get store in
  let the_place = places#get place in
  let feat = f the_store the_place in
  let foc_opt = Lisql.Feature.focus_insert the_store feat the_place#focus in
  _cd store the_place foc_opt

let insert_plain_literal ~(store : store_id) ~(place : place_id) ~(text : string) ~(lang : string) : place_id =
  _insert_feature store place (fun the_store the_place ->
    new Lisql.Feature.feature_name the_store (Name.plain_literal text lang))
(** [insert_plain_literal ~store ~place ~text ~lang] returns a new
    place by inserting in [place] a plain literal, composed of a [text] and
    a language [lang]. *)

let insert_typed_literal ~(store : store_id) ~(place : place_id) ~(text : string) ~(datatype : uri) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let datatype = resolve_uri the_store datatype in
    new Lisql.Feature.feature_name the_store (Name.typed_literal text datatype))
(** [insert_typed_literal ~store ~place ~text ~datatype] returns a new
    place by inserting in [place] a typed literal, composed of a [text],
    and a [datatype].  *)

let insert_date ~(store : store_id) ~(place : place_id) ~(year : int) ~(month : int) ~(day : int) : place_id =
  _insert_feature store place (fun the_store the_place ->
    new Lisql.Feature.feature_name the_store (Name.date_of_year_month_day year month day))
(** [insert_date ~store ~place ~year ~month ~day] returns a new place
    by inserting in [place] a date literal. *)

let insert_dateTime ~(store : store_id) ~(place : place_id) ~(year : int) ~(month : int) ~(day : int) ~(hours: int) ~(minutes : int) ~(seconds : int) : place_id =
  _insert_feature store place (fun the_store the_place ->
    new Lisql.Feature.feature_name the_store (Name.dateTime year month day hours minutes seconds))
(** [insert_dateTime ~store ~place ~year ~month ~day ~hours ~minutes
    ~seconds] returns a new place by inserting in [place] a dateTime
    literal. *)

let insert_filename ~(store : store_id) ~(place : place_id) ~(filename : string) : place_id =
  _insert_feature store place (fun the_store the_place ->
    new Lisql.Feature.feature_name the_store (Rdf.URI (Name.uri_of_file filename)))
(** [insert_filename ~store ~place ~filename] returns a new place by
    inserting in [place] a file URI. *)

let insert_var ~(store : store_id) ~(place : place_id) ~(varname : string option) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let v = match varname with Some v -> v | None -> the_place#new_var in
    new Lisql.Feature.feature_some the_store v)
(** [insert_var ~store ~place ~varname] returns a new place by
    inserting in [place] a variable whose name can optionally be
    specified in [varname]. *)

let insert_uri ~(store : store_id) ~(place : place_id) ~(uri : uri) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let uri = resolve_uri the_store uri in
    new Lisql.Feature.feature_name the_store (Rdf.URI uri))
(** [insert_uri ~store ~place ~uri] returns a new place by inserting
    in [place] the [uri]. *)

let insert_class ~(store : store_id) ~(place : place_id) ~(uri : uri) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let uri = resolve_uri the_store uri in
    new Lisql.Feature.feature_type the_store uri)
(** [insert_class ~store ~place ~uri] returns a new place by inserting
    in [place] the class [uri]. *)

let insert_property ~(store : store_id) ~(place : place_id) ~(uri : uri) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let uri = resolve_uri the_store uri in
    new Lisql.Feature.feature_role the_store `Subject uri)
(** [insert_property ~store ~place ~uri] returns a new place by
    inserting in [place] a property [uri]. *)

let insert_inverse_property ~(store : store_id) ~(place : place_id) ~(uri : uri) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let uri = resolve_uri the_store uri in
    new Lisql.Feature.feature_role the_store `Object uri)
(** [insert_inverse_property ~store ~place ~uri] returns a new place
    by inserting in [place] an inverse property [uri]. *)

let insert_structure ~(store : store_id) ~(place : place_id) ~(uri : uri) ~(arity : int) ~(index : int) : place_id =
  _insert_feature store place (fun the_store the_place ->
    let uri = resolve_uri the_store uri in
    new Lisql.Feature.feature_funct the_store uri arity index)
(** [insert_structure ~store ~place ~uri ~arity ~index] returns a new
    place by inserting in [place] a structure with functor [uri], [arity]
    arguments, and with current focus on argument at position [index]. *)


(** {2 Place-based store modifiers} *)

let set_as_home ~(place : place_id) : unit =
  (places#get place)#set_as_home_query
(** [set_as_home ~place] defines [place] as the home place of the
    store. *)

let add_as_bookmark ~(place : place_id) : unit =
  (places#get place)#add_as_bookmark
(** [add_as_bookmark ~place] adds [place] as a bookmark. *)

let add_as_draft ~(place : place_id) : unit =
  (places#get place)#add_as_draft
(** [add_as_draft ~place] adds [place] as a draft. *)

let remove_as_draft ~(place : place_id) : unit =
  (places#get place)#remove_as_draft
(** [remove_as_draft ~place] removes [place] as a draft. *)


(** {1 Freeing memory} *)

let free_focus ~(focus : focus_id) : unit =
  foci#remove focus
(** [free_focus ~focus] frees [focus] *)

let free_cell ~(cell : cell_id) : unit =
  cells#remove cell
(** [free_cell ~cell] frees [cell]. *)

let free_increment ~(increment : increment_id) : unit =
  increments#remove increment
(** [free_increment ~increment] frees [increment]. *)

let free_place ~(place : place_id) : unit =
  List.iter (fun focus -> free_focus ~focus) (place_foci#get place);
  List.iter (fun cell -> free_cell ~cell) (place_cells#get place);
  List.iter (fun increment -> free_increment ~increment) (place_increments#get place);
  places#remove place
(** [free_place ~place] frees [place], and its dependent foci, cells,
    and increments. *)

let free_store ~(store : store_id) : unit =
  try
    let the_store = stores#get store in
    the_store#save;
    the_store#export_rdf ~base:the_store#base ~xmlns:the_store#xmlns (the_store#filepath ^ ".nt"); (* TODO: failure? *)
    List.iter (fun place -> free_place ~place) (store_places#get store);
    stores#remove store
  with Undefined _ -> ()
(** [free_store ~store] frees [store], and its dependent places. *)

let free_root () : unit =
  List.iter (fun store -> free_store ~store) (root_stores#get root)
(** [free_root ()] frees all stores. *)
