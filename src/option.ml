(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

let fold f e = function None -> e | Some x -> f x

let map f = function None -> None | Some x -> Some (f x)

let iter f = function None -> () | Some x -> f x

let bind opt f = match opt with None -> None | Some x -> f x

let get opt f = match opt with None -> f () | Some x -> x

let find opt = match opt with None -> raise Not_found | Some x -> x

let apply_1 opt x = match opt with None -> None | Some f ->  Some (f x)
let apply_2 opt x y = match opt with None -> None | Some f ->  Some (f x y)
let apply_3 opt x y z = match opt with None -> None | Some f ->  Some (f x y z)
let apply_4 opt x y z t = match opt with None -> None | Some f ->  Some (f x y z t)

let to_list opt = fold (fun x -> [x]) [] opt
