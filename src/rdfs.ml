(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* RDFS vocabulary *)

(* namespace *)
let prefix = "rdfs:"
let namespace = "http://www.w3.org/2000/01/rdf-schema#"
(* classes *)
let _Resource = "rdfs:Resource"
let _Literal = "rdfs:Literal"
let _Class = "rdfs:Class"
let _Datatype = "rdfs:Datatype"
let _Container = "rdfs:Container"
let _ContainerMembershipProperty = "rdfs:ContainerMembershipProperty"
(* properties *)
let _subClassOf = "rdfs:subClassOf"
let _subPropertyOf = "rdfs:subPropertyOf"
let _domain = "rdfs:domain"
let _range = "rdfs:range"
let _label = "rdfs:label"
let _comment = "rdfs:comment"
let _member = "rdfs:member"
let _seeAlso = "rdfs:seeAlso"
let _isDefinedBy = "rdfs:isDefinedBy"

let make = Uri.make namespace prefix

let uri_Resource = make _Resource
let uri_Literal = make _Literal
let uri_Class = make _Class
let uri_Datatype = make _Datatype
let uri_Container = make _Container
let uri_ContainerMembershipProperty = make _ContainerMembershipProperty
let uri_subClassOf = make _subClassOf
let uri_subPropertyOf = make _subPropertyOf
let uri_domain = make _domain
let uri_range = make _range
let uri_label = make _label
let uri_comment = make _comment
let uri_member = make _member
let uri_seeAlso = make _seeAlso
let uri_isDefinedBy = make _isDefinedBy
