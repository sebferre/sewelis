(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(*
   author: Sébastien Ferré
   created: 04/08/2011
   description: parsing and printing of Turtle files
   dependencies: uri.ml, rdf.ml, DCG parsers
*)

let prefix = "ttl:"
let namespace = "http://www.w3.org/2008/turtle#"
let _turtle = "ttl:turtle"
let uri_turtle = Uri.make namespace prefix _turtle

type turtle = statement list
and statement =
  | Base of uri
  | Prefix of string * uri (* the prefix must include the trailing colon *)
  | Descr of term * feature list
and feature = uri * term list
and term =
  | URI of uri
  | Blank of string
  | Plain of string * string
  | Typed of string * uri
  | Anonymous of feature list
  | Collection of term list
and uri =
  | Absolute of string
  | Relative of string
  | Qname of string * string

type ctx = { mutable base : string;
	     mutable prefixes : (string * string) list;
	     mutable cpt : int; }

let set_base ctx uri = ctx.base <- Uri.absolute ctx.base uri
let set_prefix ctx pre uri = ctx.prefixes <- (pre, Uri.absolute ctx.base uri)::List.remove_assoc pre ctx.prefixes
let get_name ctx = ctx.cpt <- ctx.cpt+1; "_" ^ string_of_int ctx.cpt

let resolve ctx : uri -> Uri.t = function
  | Absolute abs -> abs
  | Relative rel -> Uri.absolute ctx.base rel
  | Qname (pre, name) -> Uri.unqualified ctx.prefixes (pre,name)

let rec triples_of_turtle base turtle : Rdf.triple list = Common.prof "Turtle.triples_of_turtle" (fun () ->
  let ctx =
    { base = base;
      prefixes = [ ("rdf:", Rdf.namespace);
		   ("rdfs:", Rdfs.namespace);
		   ("owl:", Owl.namespace);
		   ("xsd:", Xsd.namespace) ];
      cpt = 1; } in
  triples_of_statements ctx [] turtle)
and triples_of_statements ctx lt lstat =
  List.fold_left
    (fun lt -> function
      | Base uri -> set_base ctx (resolve ctx uri); lt
      | Prefix (pre,uri) -> set_prefix ctx pre (resolve ctx uri); lt
      | Descr (s, lf) ->
	  let s, lt = triples_of_term ctx lt s in
	  triples_of_features ctx lt s lf)
    lt lstat
and triples_of_features ctx lt s lf =
  List.fold_left
    (fun lt (p,lo) ->
      let p = resolve ctx p in
      triples_of_objects ctx lt s p lo)
    lt lf
and triples_of_objects ctx lt s p lo =
  List.fold_left
    (fun lt o ->
      let o, lt = triples_of_term ctx lt o in
      {Rdf.s=s; Rdf.p=p; Rdf.o=o; Rdf.t_opt=None}::lt)
    lt lo
and triples_of_term ctx lt = function
  | URI uri -> Rdf.URI (resolve ctx uri), lt
  | Blank name -> Rdf.Blank name, lt
  | Plain (s,lang) -> Rdf.Literal (s, Rdf.Plain lang), lt
  | Typed (s,uri) -> Rdf.Literal (s, Rdf.Typed (resolve ctx uri)), lt
  | Anonymous lf ->
      let s = Rdf.Blank (get_name ctx) in
      s, triples_of_features ctx lt s lf
  | Collection lo ->
      List.fold_right
	(fun o (subcoll,lt) ->
	  let o', lt' = triples_of_term ctx lt o in
	  let coll = Rdf.Blank (get_name ctx) in
	  coll,
	  {Rdf.s=coll; Rdf.p=Rdf.uri_first; Rdf.o=o'; Rdf.t_opt=None}::
	  {Rdf.s=coll; Rdf.p=Rdf.uri_rest; Rdf.o=subcoll; Rdf.t_opt=None}::
	  lt')
	lo (Rdf.URI Rdf.uri_nil, lt)


let regexp_ws = "\\([\t\r\n ]\\|#[^\r\n]*[\r\n]*\\)" (* white spaces and comments *)
let regexp_ws_plus = regexp_ws ^ "+"
let regexp_ws_star = regexp_ws ^ "*"

let rec parse_turtleDoc = dcg
    [ dir = parse_directive then l = parse_turtleDoc -> dir::l
    | tri = parse_triples then l = parse_turtleDoc -> tri::l
    | _ = parse_ws_plus then l = parse_turtleDoc -> l
  | EOF -> [] ]
and parse_sentence = dcg
  [ dir = parse_directive -> Some dir
  | tri = parse_triples -> Some tri
  | _ = parse_ws_plus -> None ]
and parse_directive = dcg
    [ "@prefix"; _ = parse_ws_plus; pre = parse_prefix; _ = parse_ws_star; uri = parse_uriref; _ = parse_dot -> Prefix (pre, Relative uri)
    | "@base"; _ = parse_ws_plus; uri = parse_uriref; _ = parse_dot -> Base (Relative uri) ]
and parse_triples = dcg
  [ "["; _ = parse_ws_star; lf = parse_predicateObjectList; _ = parse_ws_star; "]"; _ = parse_dot -> Descr (Anonymous [], lf)
  | s = parse_subject; _ = parse_ws_star; lf = parse_predicateObjectList; _ = parse_dot -> Descr (s,lf) ]
and parse_dot = dcg
    [ _ = match (regexp_ws_star ^ "[.]") as "dot" -> () ]
and parse_predicateObjectList = dcg
    [ l = LIST1 [ p = parse_verb; _ = parse_ws_star; lo = parse_objectList -> (p,lo) ] SEP parse_semicolon -> l ]
and parse_semicolon = dcg
    [ _ = match (regexp_ws_star ^ ";" ^ regexp_ws_star) as "semicolon" -> () ]
and parse_objectList = dcg
    [ l = LIST1 parse_object SEP parse_comma -> l ]
and parse_comma = dcg
    [ _ = match (regexp_ws_star ^ "," ^ regexp_ws_star) as "comma" -> () ]
and parse_verb = dcg
    [ "a" -> Absolute Rdf.uri_type
    | p = parse_predicate -> p ]
and parse_subject = dcg
    [ uri = parse_resource -> URI uri
    | t = parse_blank -> t ]
and parse_predicate = dcg
    [ uri = parse_resource -> uri ]
and parse_object = dcg
    [ uri = parse_resource -> URI uri
    | t = parse_blank -> t
    | t = parse_literal -> t ]
and parse_literal = dcg
    [ s = parse_quotedString;
      ( "@"; lang = parse_language -> Plain (s, lang)
      | "^^"; uri = parse_resource -> Typed (s, uri)
      |  -> Typed (s, Absolute Xsd.uri_string) )
    | s = parse_boolean -> Typed (s, Absolute Xsd.uri_boolean)
    | s = parse_double -> Typed (s, Absolute Xsd.uri_double)
    | s = parse_decimal -> Typed (s, Absolute Xsd.uri_decimal)
    | s = parse_integer -> Typed (s, Absolute Xsd.uri_integer) ]
and parse_integer = dcg
    [ s = match "[-+]?[0-9]+" as "integer" -> s ]
and parse_double = dcg
    [ s = match "[-+]?\\([0-9]+\\.[0-9]*[eE][-+]?[0-9]+\\|\\.[0-9]+[eE][-+]?[0-9]+\\|[0-9]+[eE][-+]?[0-9]+\\)" as "double" -> s ]
and parse_decimal = dcg
    [ s = match "[-+]?\\([0-9]+\\.[0-9]*\\|\\.[0-9]+\\)" as "decimal" -> s ]
and parse_boolean = dcg
    [ "true" -> "true"
    | "false" -> "false" ]
and parse_blank = dcg
    [ id = parse_nodeID -> Blank id
    | "["; _ = parse_ws_star;
      lf = OPT [ lf = parse_predicateObjectList; _ = parse_ws_star -> lf ] ELSE [];
      "]" -> Anonymous lf
    | "("; _ = parse_ws_star;
      lo = OPT [ lo = parse_collection; _ = parse_ws_star -> lo ] ELSE [];
      ")" -> Collection lo ]
and parse_collection = dcg
    [ lo = LIST1 parse_object SEP parse_ws_star -> lo ]
and parse_ws_plus = dcg
    [ _ = match regexp_ws_plus as "ws+" -> () ]
and parse_ws_star = dcg
    [ _ = match regexp_ws_star as "ws*" -> () ]
and parse_resource = dcg
    [ rel = parse_uriref -> Relative rel
    | pre, name = parse_qname -> Qname (pre, name) ]
and parse_nodeID = dcg
    [ "_:"; name = parse_name -> name ]
and parse_qname = dcg
    [ pre = parse_prefix; name = OPT parse_name ELSE "" -> pre, name ]
and parse_uriref = dcg
    [ "<"; uri = parse_relativeURI; ">" -> uri ]
and parse_language = dcg
    [ lang = match "[a-z]+\\(-[a-z0-9]+\\)*" as "language" -> lang ]
and parse_name = dcg
    [ s = parse_nameStartChar; ls = MANY parse_nameChars -> s ^ String.concat "" ls ]
and parse_prefix = dcg
    [ ":" -> ":"
    | s1 = parse_nameStartChar when "prefix cannot start with '_'" s1 <> "_";
      s2 = parse_prefix_aux -> s1 ^ s2 ]
and parse_prefix_aux = dcg
    [ ":" -> ":"
    | s1 = parse_nameChars; s2 = parse_prefix_aux -> s1 ^ s2 ]
and parse_nameStartChar = dcg
    [ s = match "[A-Z_a-z]" -> s
    | s = parse_urange
	[ (0xC0,0xD6); (0xD8,0xF6);
	  (0xF8,0x2FF); (0x370,0x37D);
	  (0x37F,0x1FFF); (0x200C,0x200D);
	  (0x2070,0x218F); (0x2C00,0x2FEF);
	  (0x3001,0xD7FF); (0xF900,0xFDCF);
	  (0xFDF0,0xFFFD); (0x10000,0xEFFFF) ] -> s ]
and parse_nameChars = dcg
    [ s = match "[-0-9A-Z_a-z]+" -> s
    | s = parse_urange
	[ (0xB7,0xB7); (0xC0,0xD6);
	  (0xD8,0xF6); (0xF8,0x37D);
	  (0x37F,0x1FFF); (0x200C,0x200D); (0x203F,0x2040);
	  (0x2070,0x218F); (0x2C00,0x2FEF);
	  (0x3001,0xD7FF); (0xF900,0xFDCF);
	  (0xFDF0,0xFFFD); (0x10000,0xEFFFF) ] -> s ]
and parse_quotedString = dcg
    [ s = parse_longString -> s
    | s = parse_string -> s ]
and parse_relativeURI = dcg
    [ ls = MANY parse_ucharacters -> String.concat "" ls ]
and parse_string = dcg
    [ "\""; ls = MANY parse_scharacters; "\"" -> String.concat "" ls ]
and parse_longString = dcg
    [ "\"\"\""; ls = MANY parse_lcharacter; "\"\"\"" -> String.concat "" ls ]
and parse_character = dcg
    [ "\\u"; code = parse_4hex -> Unicode.utf8_of_codepoint code
    | "\\U"; code = parse_8hex -> Unicode.utf8_of_codepoint code
    | "\\\\" -> "\\"
    | s = parse_urange [(0x20,0x5B); (0x5D,0x10FFFF)] -> s ] (* except \ *)
and parse_echaracter = dcg
    [ "\\t" -> "\t"
    | "\\n" -> "\n"
    | "\\r" -> "\r"
    | s = parse_character -> s ]
and parse_ucharacters = dcg
    [ "\\>" -> ">"
    | s = match "[ -=?-Z^-~]+" -> s (* ASCII minus '>' and '\' *)
    | s = parse_character when "" (s <> ">") -> s ]
and parse_scharacters = dcg
    [ "\\\"" -> "\""
    | s = match "[ -!#-Z^-~]+" -> s (* ASCII minus double-quote and escape *)
    | s = parse_echaracter when "" (s <> "\"") -> s ]
and parse_lcharacter = dcg
    [ "\\\"" -> "\""
    | s = match "[ -!#-Z^-~\t\r\n]+" -> s (* ASCII minus double-quote and escape *)
    | s = parse_echaracter when "" (s <> "\"") -> s ]
and parse_4hex = dcg
    [ s = match "[0-9A-F][0-9A-F][0-9A-F][0-9A-F]" as "4hex" -> Unicode.codepoint_of_string s ]
and parse_8hex = dcg
    [ s = match "[0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F][0-9A-F]" as "8hex" -> Unicode.codepoint_of_string s ]
and parse_urange l_interv = dcg
    [ s = match Unicode.regexp_utf8_char as "UTF8 char";
      c = try Unicode.codepoint_of_utf8_char s
	when "out of range" (List.exists (fun (low,high) -> low <= c && c <= high) l_interv) -> s ]

let parse_file (filename : string) : turtle = Common.prof "Turtle.parse_file" (fun () ->
  let ch = open_in filename in
  (*  let res = snd (Dcg.once parse_turtleDoc [] (Matcher.cursor_of_channel ch)) in*)
  let cursor = Matcher.cursor_of_channel ch in
  let _, rev_l = Dcg.once_fold (fun acc -> function None -> acc | Some x -> x::acc) [] parse_sentence [] cursor in
  close_in ch;
  List.rev rev_l)


let turtle_of_uri base xmlns uri : uri = Common.prof "Turtle.turtle_of_uri" (fun () ->
  try
    let pre, name = Uri.qname xmlns uri in
    Qname (pre, name)
  with _ ->
    let reluri = Uri.relative base uri in
    if reluri = uri
    then Absolute uri
    else Relative reluri)

let turtle_of_term base xmlns = function
  | Rdf.URI uri -> URI (turtle_of_uri base xmlns uri)
  | Rdf.Blank name -> Blank name
  | Rdf.Literal (s, Rdf.Plain lang) -> Plain (s, lang)
  | Rdf.Literal (s, Rdf.Typed uri) -> Typed (s, turtle_of_uri base xmlns uri)
  | Rdf.XMLLiteral xml -> invalid_arg "Turtle.turtle_of_term: XMLLiteral"

let turtle_of_triple base xmlns triple : statement = Common.prof "Turtle.turtle_of_triple" (fun () ->
  let s = turtle_of_term base xmlns triple.Rdf.s in
  let p = turtle_of_uri base xmlns triple.Rdf.p in
  let o = turtle_of_term base xmlns triple.Rdf.o in
  (* triple.Rdf.t_opt is ignored *)
  Descr (s, [p, [o]]))

let escape l s = Common.prof "Turtle.escape" (fun () ->
  let s = Str.global_replace (Str.regexp_string "\\") "\\\\" s in
  List.fold_left
    (fun s (a,b) -> Str.global_replace (Str.regexp_string a) b s)
    s l)

let escape_uri = escape [(">","\\>")]
let escape_shortString = escape [("\"","\\\""); ("\n","\\n"); ("\r","\\r"); ("\t","\\t")]
let escape_longString = escape [("\"","\\\"")]

let rec print_turtle = ipp
    [ st::l -> print_statement of st then print_turtle of l
    | [] -> EOF ]
(*    [ l -> MANY print_statement of l; EOF ] *)
and print_statement = ipp
    [ Base uri -> "@base"; print_space; print_uri of uri; print_dot
    | Prefix (pre, uri) -> "@prefix"; print_space; 'pre; print_space; print_uri of uri; print_dot
    | Descr (s, lf) -> print_term of s; print_space; LIST1 print_feature SEP print_semicolon of lf; print_dot ]
and print_feature = ipp
    [ p, lo -> print_uri of p; print_space; LIST1 print_term SEP print_comma of lo ]
and print_term = ipp
    [ URI uri -> print_uri of uri
    | Blank name -> "_:"; 'name
    (*| Plain (s,"") -> print_string of s*)
    | Plain (s,lang) -> print_string of s; "@"; 'lang
    | Typed (s,dt) -> print_string of s; "^^"; print_uri of dt
    | Anonymous lf -> "[ "; LIST0 print_feature SEP print_semicolon of lf; " ]"
    | Collection lo -> "( "; LIST0 print_term SEP print_space of lo; ")" ]
and print_uri = ipp
    [ Absolute uri -> "<"; '(escape_uri uri); ">"
    | Relative uri -> "<"; '(escape_uri uri); ">"
    | Qname (ns,name) -> 'ns; 'name ]
and print_string = ipp
    [ s -> when String.contains s '\n'; "\n\"\"\""; '(escape_longString s); "\"\"\""
    | s -> "\""; '(escape_shortString s); "\"" ]
and print_space = ipp [ _ -> " " ]
and print_comma = ipp [ _ -> ", " ]
and print_semicolon = ipp [ _ -> " ;\n\t" ]
and print_dot = ipp [ _ -> " .\n" ]

let output (out : out_channel) (turtle : turtle) = Common.prof "Turtle.output" (fun () ->
  try Ipp.once print_turtle turtle (Printer.cursor_of_formatter (Format.formatter_of_out_channel out)) ()
  with _ -> assert false)

let print (turtle : turtle) : unit = Common.prof "Turtle.print" (fun () ->
  try Ipp.once print_turtle turtle (Printer.cursor_of_formatter Format.std_formatter) ()
  with _ -> assert false)

let to_string turtle : string = Common.prof "Turtle.to_string" (fun () ->
  try
    let buf_cursor = new Printer.buffer_cursor in
    Ipp.once print_turtle turtle buf_cursor ();
    buf_cursor#contents
  with _ -> assert false)

