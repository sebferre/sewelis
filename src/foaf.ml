(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    S�bastien Ferr� <ferre@irisa.fr>, �quipe LIS, IRISA/Universit� Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

(* FOAF vocabulary : Friend Of A Friend *)

(* namespace *)
let prefix = "foaf:"
let namespace = "http://xmlns.com/foaf/0.1/"
(* classes *)
let _Agent = "foaf:Agent"
let _Group = "foaf:Group" (* a set of agents *)
let _Organization = "foaf:Organization" (* more solid than a group *)
let _Person = "foaf:Person"
let _Document = "foaf:Document" (* in abstract and copies *)
let _Image = "foaf:Image"
let _OnlineAccount = "foaf:OnlineAccount"
let _PersonalProfileDocument = "foaf:PersonalProfileDocument" (* bof *)
let _Project = "foaf:Project"
let _LabelProperty = "foaf:LabelProperty" (* subproperties of rdfs:label *)
let _OnlineChatAccount = "foaf:OnlineChatAccount"
let _OnlineEcommerceAccount = "foaf:OnlineEcommerceAccount"
let _OnlineGamingAccount = "foaf:OnlineGamingAccount"
(* properties *)
let _homepage = "foaf:homepage" (* inverse functional *)
let _isPrimaryTopicOf = "foaf:isPrimaryTopicOf" (* inverse functional, superproperty of homepage *)
let _knows = "foaf:knows"
let _made = "foaf:made"
let _maker = "foaf:maker" (* inverse of 'made' *)
let _mbox = "foaf:mbox" (* email address *)
let _member = "foaf:member" (* from groups to agents *)
let _primaryTopic = "foaf:primaryTopic" (* inverse of 'isPrimaryTopicOf' *)
let _account = "foaf:account" (* from Agents to OnlineAccounts *)
let _accountName = "foaf:accountName" (* from Accounts to login IDs *)
let _accountServiceHomepage = "foaf:accountServiceHomepage" (* from accounts to Documents *)
let _based_near = "foaf:based_near"
let _currentProject = "foaf:currentProject"
let _pastProject = "foaf:pastProject"
let _depiction = "foaf:depiction" (* from things to images that depicts them *)
let _depicts = "foaf:depicts" (* the inverse of depiction *)
let _name = "foaf:name"
let _familyName = "foaf:familyName"
let _givenName = "foaf:givenName"
let _firstName = "foaf:firstName" (* givenName is preferred *)
let _lastName = "foaf:lastName" (* familyName is preferred *)
let _focus = "foaf:focus" (* unclear, SKOS related *)
let _gender = "foaf:gender" (* from agents to strings (e.g., "male", "female") *)
let _img = "foaf:img" (* a subproperty of 'depiction', only for Persons, most representative *)
let _interest = "foaf:interest" (* from Agents to Documents *)
let _logo = "foaf:logo" (* inverse functional *)
let _mbox_sha1sum = "foaf:sha1sum" (* inverse functional, SHA1SUM of email address *)
let _nick = "foaf:nick" (* nickname *)
let _openid = "foaf:openid" (* inverse functional *)
let _page = "foaf:page" (* inverse of topic, from things to documents *)
let _phone = "foaf:phone" (* from agents? to tel: URIs *)
let _plan = "foaf:plan" (* related to .plan files, bof *)
let _publications = "foaf:publications" (* from persons to documents listing publications *)
let _schoolHomepage = "foaf:schoolHomepage"
let _myersBriggs = "foaf:myersBriggs" (* from persons to one of "INTJ", ... *)
let _thumbnail = "foaf:thumbnail" (* from images to their thumbnail images *)
let _title = "foaf:title" (* e.g., Mr, Ms *)
let _topic = "foaf:topic" (* from documents to things *)
let _topic_interest = "foaf:topic_interest" (* from agents to things *)
let _weblog = "foaf:weblog" (* from agents to documents *)
let _workInfoHomepage = "foaf:workInfoHomepage" (* the professional homepage *)
let _workplaceHomepage = "foaf:workplaceHomepage" (* the workplace's homepage *)
let _age = "foaf:age" (* functional *)
let _birthday = "foaf:birthday" (* functional, a gDay: mm-dd *)
let _membershipClass = "foaf:membershipClass" (* from groups to definitions of classes of agents *)
let _sha1 = "foaf:sha1" (* unstable, needs clarification on documents vs copies *)
let _status = "foaf:status" (* from agents to their current activity *)
let _aimChatID = "foaf:aimChatID" (* AOL Instant Messenger *)
let _icqChatID = "foaf:icqChatID" (* ICQ *)
let _jabberID = "foaf:jabberID" (* Jabber *)
let _msnChatID = "foaf:msnChatID" (* MSN *)
let _skypeID = "foaf:skypeID" (* Skype *)
let _yahooChatID = "foaf:yahooChatID" (* Yahoo *)
let _tipjar = "foaf:tipjar" (* a document that allows to pay or reward this person *)

let make = Uri.make namespace prefix

let uri_Agent = make _Agent
let uri_Group = make _Group
let uri_Organization = make _Organization
let uri_Person = make _Person
let uri_Document = make _Document
let uri_Image = make _Image
let uri_OnlineAccount = make _OnlineAccount
let uri_PersonalProfileDocument = make _PersonalProfileDocument
let uri_Project = make _Project
let uri_LabelProperty = make _LabelProperty
let uri_OnlineChatAccount = make _OnlineChatAccount
let uri_OnlineEcommerceAccount = make _OnlineEcommerceAccount
let uri_OnlineGamingAccount = make _OnlineGamingAccount
let uri_homepage = make _homepage
let uri_isPrimaryTopicOf = make _isPrimaryTopicOf
let uri_knows = make _knows
let uri_made = make _made
let uri_maker = make _maker
let uri_mbox = make _mbox
let uri_member = make _member
let uri_primaryTopic = make _primaryTopic
let uri_account = make _account
let uri_accountName = make _accountName
let uri_accountServiceHomepage = make _accountServiceHomepage
let uri_based_near = make _based_near
let uri_currentProject = make _currentProject
let uri_pastProject = make _pastProject
let uri_depiction = make _depiction
let uri_depicts = make _depicts
let uri_name = make _name
let uri_familyName = make _familyName
let uri_givenName = make _givenName
let uri_firstName = make _firstName
let uri_lastName = make _lastName
let uri_focus = make _focus
let uri_gender = make _gender
let uri_img = make _img
let uri_interest = make _interest
let uri_logo = make _logo
let uri_mbox_sha1sum = make _mbox_sha1sum
let uri_nick = make _nick
let uri_openid = make _openid
let uri_page = make _page
let uri_phone = make _phone
let uri_plan = make _plan
let uri_publications = make _publications
let uri_schoolHomepage = make _schoolHomepage
let uri_myersBriggs = make _myersBriggs
let uri_thumbnail = make _thumbnail
let uri_title = make _title
let uri_topic = make _topic
let uri_topic_interest = make _topic_interest
let uri_weblog = make _weblog
let uri_workInfoHomepage = make _workInfoHomepage
let uri_workplaceHomepage = make _workplaceHomepage
let uri_age = make _age
let uri_birthday = make _birthday
let uri_membershipClass = make _membershipClass
let uri_sha1 = make _sha1
let uri_status = make _status
let uri_aimChatID = make _aimChatID
let uri_icqChatID = make _icqChatID
let uri_jabberID = make _jabberID
let uri_msnChatID = make _msnChatID
let uri_skypeID = make _skypeID
let uri_yahooChatID = make _yahooChatID
let uri_tipjar = make _tipjar
