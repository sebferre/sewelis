(*
    This file is part of SEWELIS <http://www.irisa.fr/LIS/softwares/sewelis/>

    Sébastien Ferré <ferre@irisa.fr>, équipe LIS, IRISA/Université Rennes 1

    Copyright 2012.

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*)

open Builtins

module Basic = Builtins_basic

let div_mod (a : int) (b : int) : int * int = 
  let q, r = a / b, a mod b in
  if a >= 0 || r = 0
  then q, r
  else q-1, r+b

let div_mod_float (a : float) (b : int) : int * float =
  let a' = truncate (Pervasives.floor a) in
  let q = a' / b in
  let r = a -. float (q * b) in
  if a >= 0. || r = 0.
  then q, r
  else q - 1, r +. float b

let mod_in_range offset width x =
  let _, r = div_mod (x - offset) width in
  r + offset

module Duration =
  struct
    type t = { negative : bool;
	       years : int;
	       months : int;
	       days : int;
	       hours : int;
	       minutes : int;
	       seconds : float;
	     }
    let make ?(negative=false)
	?(years=0) ?(months=0) ?(days=0)
	?(hours=0) ?(minutes=0) ?(seconds=0.) () =
      { negative;
	years; months; days;
	hours; minutes; seconds }

    let regexp = Str.regexp 
      "\\(-\\)?P\\(\\([0-9]+\\)Y\\)?\\(\\([0-9]+\\)M\\)?\\(\\([0-9]+\\)D\\)?\\(T\\(\\([0-9]+\\)H\\)?\\(\\([0-9]+\\)M\\)?\\(\\([0-9]+\\([.][0-9]+\\)?\\)S\\)?\\)?$"
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_duration ->
	if Str.string_match regexp s 0
	then Some
	  { negative = (try Str.matched_group 1 s = "-" with Not_found -> false);
	    years = (try int_of_string (Str.matched_group 3 s) with Not_found -> 0);
	    months = (try int_of_string (Str.matched_group 5 s) with Not_found -> 0);
	    days = (try int_of_string (Str.matched_group 7 s) with Not_found -> 0);
	    hours = (try int_of_string (Str.matched_group 10 s) with Not_found -> 0);
	    minutes = (try int_of_string (Str.matched_group 12 s) with Not_found -> 0);
	    seconds = (try float_of_string (Str.matched_group 14 s) with Not_found -> 0.);
	  }
	else None (* not a valid lexical value *)
      | _ -> None (* not a duration *)
    let to_name dur =
      let buf = Buffer.create 30 in
      if dur.negative then Buffer.add_char buf '-';
      Buffer.add_char buf 'P';
      if dur.years <> 0 then begin Buffer.add_string buf (string_of_int dur.years); Buffer.add_char buf 'Y' end;
      if dur.months <> 0 then begin Buffer.add_string buf (string_of_int dur.months); Buffer.add_char buf 'M' end;
      if dur.days <> 0 then begin Buffer.add_string buf (string_of_int dur.days); Buffer.add_char buf 'D' end;
      if dur.hours <> 0 || dur.minutes <> 0 || dur.seconds <> 0. then Buffer.add_char buf 'T';
      if dur.hours <> 0 then begin Buffer.add_string buf (string_of_int dur.hours); Buffer.add_char buf 'H' end;
      if dur.minutes <> 0 then begin Buffer.add_string buf (string_of_int dur.minutes); Buffer.add_char buf 'M' end;
      if dur.seconds <> 0. then begin
	let sseconds =
	  let s = string_of_float dur.seconds in
	  let n = String.length s in
	  let i = String.index s '.' in
	  let s = if i = n-1 then String.sub s 0 (n-1) else s in
	  let s = if i = 0 then "0" ^ s else s in
	  s in
	Buffer.add_string buf sseconds;
	Buffer.add_char buf 'S' end;
      let s = Buffer.contents buf in
      Rdf.Literal (s, Rdf.Typed Xsd.uri_duration)

    let years d = d.years
    let months d = d.months
    let days d = d.days
    let hours d = d.hours
    let minutes d = d.minutes
    let seconds d = d.seconds

    let totalSeconds d =
      let nyears = d.years in
      let nmonths = nyears * 12 + d.months in
      let ndays = nmonths * 30 + d.days in
      let nhours = ndays * 24 + d.hours in
      let nminutes = nhours * 60 + d.minutes in
      let nseconds = float (nminutes * 60) +. d.seconds in
      if d.negative then -. nseconds else nseconds

    let compare d1 d2 = Pervasives.compare (totalSeconds d1) (totalSeconds d2)

    let normalize d =
      let d = let q, r = div_mod_float d.seconds 60 in {d with minutes = d.minutes + q; seconds = r} in
      let d = let q, r = div_mod d.minutes 60 in {d with hours = d.hours + q; minutes = r} in
      let d = let q, r = div_mod d.hours 24 in {d with days = d.days + q; hours = r} in
      let d = let q, r = div_mod d.days 30 in {d with months = d.months + q; days = r} in
      let d = let q, r = div_mod d.months 12 in {d with years = d.years + q; months = r} in
      d

    let zero = { negative = false;
		 years = 0;
		 months = 0;
		 days = 0;
		 hours = 0;
		 minutes = 0;
		 seconds = 0.;
	       }

    let abs d = { d with negative=false}

    let rec add d1 d2 =
      match d1.negative, d2.negative with
	| false, true -> sub d1 (abs d2)
	| true, false -> sub d2 (abs d1)
	| true, true -> { add (abs d1) (abs d2) with negative=true }
	| false, false ->
	  normalize
	    { negative = false;
	      years = d1.years + d2.years;
	      months = d1.months + d2.months;
	      days = d1.days + d2.days;
	      hours = d1.hours + d2.hours;
	      minutes = d1.minutes + d2.minutes;
	      seconds = d1.seconds +. d2.seconds;
	    }
    and sub d1 d2 =
      match d1.negative, d2.negative with
	| true, true -> sub (abs d2) d1
	| true, false -> { add (abs d1) d2 with negative=true }
	| false, true -> add d1 (abs d2)
	| false, false ->
	  normalize
	    { negative = false;
	      years = d1.years - d2.years;
	      months = d1.months - d2.months;
	      days = d1.days - d2.days;
	      hours = d1.hours - d2.hours;
	      minutes = d1.minutes - d2.minutes;
	      seconds = d1.seconds -. d2.seconds;
	    }
  end

module Time =
  struct
    type t = { hours : int;
	       minutes : int;
	       seconds : float;
	       timezone : bool * int * int; (* the bool is 'negative' *)
	     }
    let utc_timezone = (false,0,0)
    let make ~hours ~minutes ~seconds ~timezone =
      {hours; minutes; seconds; timezone}

    let regexp = Str.regexp
      "\\([0-9]+\\):\\([0-9]+\\):\\([0-9]+\\([.][0-9]+\\)?\\)\\(Z\\|\\([+-]\\)\\([0-9]+\\)\\(:\\([0-9]+\\)\\)?\\)?"
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_time ->
	if Str.string_match regexp s 0
	then Some
	  { hours = int_of_string (Str.matched_group 1 s);
	    minutes = int_of_string (Str.matched_group 2 s);
	    seconds = float_of_string (Str.matched_group 3 s);
	    timezone = 
	      (try
		 (Str.matched_group 6 s = "-"),
		 (int_of_string (Str.matched_group 7 s)),
		(try int_of_string (Str.matched_group 9 s) with _ -> 0)
	       with _ -> utc_timezone) }
	else None
      | _ -> None
    let to_name v =
      let sseconds =
	let s = string_of_float v.seconds in
	let n = String.length s in
	let i = String.index s '.' in
	let s = if i = n-1 then String.sub s 0 (n-1) else s in
	let s = if i = 0 then "00" ^ s else if i = 1 then "0" ^ s else s in
	s in
      let stz = 
	match v.timezone with
	  | (_,0,0) -> "Z"
	  | (neg,h,m) -> Printf.sprintf "%s%02d:%02d" (if neg then "-" else "+") h m in
      let s = Printf.sprintf "%02d:%02d:%s%s" v.hours v.minutes sseconds stz in
      Rdf.Literal (s, Rdf.Typed Xsd.uri_time)

    let hours time : int = time.hours
    let minutes time : int = time.minutes
    let seconds time : float = time.seconds
    let timezone time : bool * int * int = time.timezone

    let normalize time : int * t = (* returns extra days, and normalized time *)
      let time = let q, r = div_mod_float time.seconds 60 in {time with minutes = time.minutes + q; seconds = r} in
      let time = let q, r = div_mod time.minutes 60 in {time with hours = time.hours + q; minutes = r} in
      let q, r = div_mod time.hours 24 in
      q, {time with hours = r}

    let shift_utc time : int * t =
      let (neg,dh,dm) = time.timezone in
      normalize
	{ time with
	  hours = if neg then time.hours + dh else time.hours - dh;
	  minutes = if neg then time.minutes + dm else time.minutes + dm;
	  timezone = utc_timezone }

    let compare t1 t2 =
      Pervasives.compare (shift_utc t1) (shift_utc t2)

    let rec add time dur : int * t =
      if dur.Duration.negative
      then sub time (Duration.abs dur)
      else
	normalize
	  {hours = time.hours + dur.Duration.hours;
	   minutes = time.minutes + dur.Duration.minutes;
	   seconds = time.seconds +. dur.Duration.seconds;
	   timezone = time.timezone}
    and sub time dur : int * t =
      if dur.Duration.negative
      then add time (Duration.abs dur)
      else
	normalize
	  {hours = time.hours - dur.Duration.hours;
	   minutes = time.minutes - dur.Duration.minutes;
	   seconds = time.seconds -. dur.Duration.seconds;
	   timezone = time.timezone}

    let duration t1 t2 = (* from t1 to t2 *)
      let q1, t1 = shift_utc t1 in
      let q2, t2 = shift_utc t2 in
      Duration.normalize
	{ Duration.zero with
	  Duration.days = q2 - q1;
	  Duration.hours = t2.hours - t1.hours;
	  Duration.minutes = t2.minutes - t1.minutes;
	  Duration.seconds = t2.seconds -. t1.seconds }
  end

module Date =
  struct
    type t = { year : int;
	       month : int;
	       day : int; }
    let make ~year ~month ~day =
      {year; month; day}

    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_date ->
	(try Scanf.sscanf s "%d-%d-%d" (fun year month day -> Some {year; month; day})
	 with _ -> None)
      | _ -> None
    let to_name date =
      Rdf.Literal (Printf.sprintf "%04d-%02d-%02d" date.year date.month date.day, Rdf.Typed Xsd.uri_date)

    let year date : int = date.year
    let month date : int = date.month
    let day date : int = date.day

    let to_tm date =
      let t, tm =
	Unix.mktime
	  { Unix.tm_year = date.year - 1900;
	    Unix.tm_mon = date.month - 1;
	    Unix.tm_mday = date.day;
	    Unix.tm_hour = 12;
	    Unix.tm_min = 0;
	    Unix.tm_sec = 0;
	    Unix.tm_wday = 0;
	    Unix.tm_yday = 0;
	    Unix.tm_isdst = false } in
      tm

    let weekday date : string =
      match (to_tm date).Unix.tm_wday with
	| 0 -> "Sunday" (* TODO: use URIs for weekdays so as to allow for multilingual labels *)
	| 1 -> "Monday"
	| 2 -> "Tuesday"
	| 3 -> "Wednesday"
	| 4 -> "Thursday"
	| 5 -> "Friday"
	| 6 -> "Saturday"
	| _ -> assert false

    let yearday date : int =
      1 + (to_tm date).Unix.tm_yday

    let during_daylight_savings date : bool =
      (to_tm date).Unix.tm_isdst

    let last_day_of_year_month y m =
      if m = 1 || m = 3 || m = 5 || m = 7 || m = 8 || m = 10 || m = 12 then 31
      else if m = 4 || m = 6 || m = 9 || m = 11 then 30
      else if m = 2 then
	if y mod 4 = 0 && (y mod 100 <> 0 || y mod 400 = 0)
	then 29
	else 28
      else invalid_arg "Builtins.Date.last_day_of_year_month"

    let compare d1 d2 =
      Pervasives.compare
	(d1.year,d1.month,d1.day)
	(d2.year,d2.month,d2.day)

    let normalize date : t =
      Common.fold_while
	(fun date ->
	  if date.month > 12 then Some {year = date.year + 1; month = date.month - 12; day = date.day}
	  else if date.month < 1 then Some {year = date.year - 1; month = date.month + 12; day = date.day}
	  else 
	    let n = last_day_of_year_month date.year date.month in
	    if date.day > n then Some {year = date.year; month = date.month + 1; day = date.day - n}
	    else if date.day < 1 then
	      if date.month = 1
	      then Some {year = date.year - 1; month = 12; day = date.day + 31}
	      else Some {year = date.year; month = date.month - 1; day = date.day + last_day_of_year_month date.year (date.month-1)}
	    else None)
	date

    let rec add date dur : t =
      if dur.Duration.negative
      then sub date (Duration.abs dur)
      else
	normalize
	  { year = date.year + dur.Duration.years;
	    month = date.month + dur.Duration.months;
	    day = date.day + dur.Duration.days }
    and sub date dur : t =
      if dur.Duration.negative
      then add date (Duration.abs dur)
      else
	normalize
	  { year = date.year - dur.Duration.years;
	    month = date.month - dur.Duration.months;
	    day = date.day - dur.Duration.days }

    let duration d1 d2 =
      Duration.normalize
	{ Duration.zero with
	  Duration.years = d2.year - d1.year;
	  Duration.months = d2.month - d1.month;
	  Duration.days = d2.day - d1.day }
  end

module YearMonth =
  struct
    type t = { year : int;
	       month : int }
    let make ~year ~month =
      {year; month}

    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_gYearMonth ->
	(try Scanf.sscanf s "%d-%d" (fun year month -> Some {year; month})
	 with _ -> None)
      | _ -> None
    let to_name date =
      Rdf.Literal (Printf.sprintf "%04d-%02d" date.year date.month, Rdf.Typed Xsd.uri_gYearMonth)

    let last_day_of_year_month y m =
      if m = 1 || m = 3 || m = 5 || m = 7 || m = 8 || m = 10 || m = 12 then 31
      else if m = 4 || m = 6 || m = 9 || m = 11 then 30
      else if m = 2 then
	if y mod 4 = 0 && (y mod 100 <> 0 || y mod 400 = 0)
	then 29
	else 28
      else invalid_arg "Builtins.Date.last_day_of_year_month"

    let compare d1 d2 =
      Pervasives.compare
	(d1.year,d1.month)
	(d2.year,d2.month)

    let normalize date : t =
      Common.fold_while
	(fun date ->
	  if date.month > 12 then Some {year = date.year + 1; month = date.month - 12}
	  else if date.month < 1 then Some {year = date.year - 1; month = date.month + 12}
	  else None)
	date

    let rec add date dur : t =
      if dur.Duration.negative
      then sub date (Duration.abs dur)
      else
	normalize
	  { year = date.year + dur.Duration.years;
	    month = date.month + dur.Duration.months }
    and sub date dur : t =
      if dur.Duration.negative
      then add date (Duration.abs dur)
      else
	normalize
	  { year = date.year - dur.Duration.years;
	    month = date.month - dur.Duration.months }

    let duration d1 d2 =
      Duration.normalize
	{ Duration.zero with
	  Duration.years = d2.year - d1.year;
	  Duration.months = d2.month - d1.month }
  end

module MonthDay =
  struct
    type t = { month : int;
	       day : int; }
    let make ~month ~day =
      {month; day}

    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_gMonthDay ->
	(try Scanf.sscanf s "--%d-%d" (fun month day -> Some {month; day})
	 with _ -> None)
      | _ -> None
    let to_name date =
      Rdf.Literal (Printf.sprintf "--%02d-%02d" date.month date.day, Rdf.Typed Xsd.uri_gMonthDay)

    let month date : int = date.month
    let day date : int = date.day

    let last_day_of_month m =
      if m = 1 || m = 3 || m = 5 || m = 7 || m = 8 || m = 10 || m = 12 then 31
      else if m = 4 || m = 6 || m = 9 || m = 11 then 30
      else if m = 2 then 28
      else invalid_arg "Builtins.Date.last_day_of_month"

    let compare d1 d2 =
      Pervasives.compare
	(d1.month,d1.day)
	(d2.month,d2.day)

    let normalize date : t =
      Common.fold_while
	(fun date ->
	  if date.month > 12 then Some {month = date.month - 12; day = date.day}
	  else if date.month < 1 then Some {month = date.month + 12; day = date.day}
	  else 
	    let n = last_day_of_month date.month in
	    if date.day > n then Some {month = date.month + 1; day = date.day - n}
	    else if date.day < 1 then
	      if date.month = 1
	      then Some {month = 12; day = date.day + 31}
	      else Some {month = date.month - 1; day = date.day + last_day_of_month (date.month-1)}
	    else None)
	date

    let rec add date dur : t =
      if dur.Duration.negative
      then sub date (Duration.abs dur)
      else
	normalize
	  { month = date.month + dur.Duration.months;
	    day = date.day + dur.Duration.days }
    and sub date dur : t =
      if dur.Duration.negative
      then add date (Duration.abs dur)
      else
	normalize
	  { month = date.month - dur.Duration.months;
	    day = date.day - dur.Duration.days }

    let duration d1 d2 =
      Duration.normalize
	{ Duration.zero with
	  Duration.months = d2.month - d1.month;
	  Duration.days = d2.day - d1.day }
  end

module DateTime =
  struct
    type t = Date.t * Time.t
    let make ~date ~time = (date,time)

    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Xsd.uri_dateTime ->
	( match Str.split (Str.regexp "T") s with
	  | [sd; st] ->
	    ( match Date.from_name (Rdf.Literal (sd, Rdf.Typed Xsd.uri_date)), Time.from_name (Rdf.Literal (st, Rdf.Typed Xsd.uri_time)) with
	      | Some d, Some t -> Some (d,t)
	      | _ -> None )
	  | _ -> None )
      | _ -> None
    let to_name (d,t) =
      match Date.to_name d, Time.to_name t with
	| Rdf.Literal (sd, _), Rdf.Literal (st,_) -> Rdf.Literal (sd ^ "T" ^ st, Rdf.Typed Xsd.uri_dateTime)
	| _ -> assert false

    let from_tm tm timezone =
      let t = { Time.hours = tm.Unix.tm_hour;
		Time.minutes = tm.Unix.tm_min;
		Time.seconds = float tm.Unix.tm_sec;
		Time.timezone } in
      let d = { Date.year = 1900 + tm.Unix.tm_year;
		Date.month = 1 + tm.Unix.tm_mon;
		Date.day = tm.Unix.tm_mday } in
      (d,t)

    let date dt = fst dt
    let time dt = snd dt

    let compare (d1,t1) (d2,t2) =
      let c = Date.compare d1 d2 in
      if c <> 0
      then c
      else Time.compare t1 t2

    let min dt1 dt2 =
      if compare dt1 dt2 <= 0
      then dt1
      else dt2
    let max dt1 dt2 =
      if compare dt1 dt2 <= 0
      then dt2
      else dt1

    let date_add_days d qd = Date.add d { Duration.zero with Duration.days = qd }

    let normalize (d,t) =
      let qd, t' = Time.normalize t in
      let d' = date_add_days d qd in
      (d',t')

    let rec add (d,t as dt) dur =
      if dur.Duration.negative
      then sub dt (Duration.abs dur)
      else
	let qd, t' = Time.add t dur in
	let d' = Date.add (date_add_days d qd) dur in
	(d',t')
    and sub (d,t as dt) dur =
      if dur.Duration.negative
      then add dt (Duration.abs dur)
      else
	let qd, t' = Time.sub t dur in
	let d' = Date.sub (date_add_days d qd) dur in
	(d',t')

    let duration (d1,t1) (d2,t2) =
      let dur_t = Time.duration t1 t2 in
      let dur_d = Date.duration d1 d2 in
      Duration.add dur_d dur_t
  end

module Temporal =
  struct
    let types = [Xsd.uri_time; Xsd.uri_date; Xsd.uri_dateTime]

    type t =
      | Time of Time.t
      | Date of Date.t
      | YearMonth of YearMonth.t
      | MonthDay of MonthDay.t
      | DateTime of DateTime.t

    let from_name n =
      match n with
	| Rdf.Literal (s, Rdf.Typed dt) ->
	  if dt = Xsd.uri_time then Option.map (fun t -> Time t) (Time.from_name n)
	  else if dt = Xsd.uri_date then Option.map (fun d -> Date d) (Date.from_name n)
	  else if dt = Xsd.uri_gYearMonth then Option.map (fun ym -> YearMonth ym) (YearMonth.from_name n)
	  else if dt = Xsd.uri_gMonthDay then Option.map (fun md -> MonthDay md) (MonthDay.from_name n)
	  else if dt = Xsd.uri_dateTime then Option.map (fun dt -> DateTime dt) (DateTime.from_name n)
	  else None
	| _ -> None
    let to_name = function
      | Time t -> Time.to_name t
      | Date d -> Date.to_name d
      | YearMonth ym -> YearMonth.to_name ym
      | MonthDay md -> MonthDay.to_name md
      | DateTime dt -> DateTime.to_name dt

    let year = function
      | Date d -> return d.Date.year
      | YearMonth ym -> return ym.YearMonth.year
      | DateTime (d,_) -> return d.Date.year
      | _ -> fail
    let month = function
      | Date d -> return d.Date.month
      | YearMonth ym -> return ym.YearMonth.month
      | MonthDay md -> return md.MonthDay.month
      | DateTime (d,_) -> return d.Date.month
      | _ -> fail
    let day = function
      | Date d -> return d.Date.day
      | MonthDay md -> return md.MonthDay.day
      | DateTime (d,_) -> return d.Date.day
      | _ -> fail
    let hours = function
      | Time t -> return t.Time.hours
      | DateTime (_,t) -> return t.Time.hours
      | _ -> fail
    let minutes = function
      | Time t -> return t.Time.minutes
      | DateTime (_,t) -> return t.Time.minutes
      | _ -> fail
    let seconds = function
      | Time t -> return t.Time.seconds
      | DateTime (_,t) -> return t.Time.seconds
      | _ -> fail
    let weekday : t -> string monad = function
      | Date d -> return (Date.weekday d)
      | DateTime (d,_) -> return (Date.weekday d)
      | _ -> fail
    let yearday : t -> int monad  = function
      | Date d -> return (Date.yearday d)
      | DateTime (d,_) -> return (Date.yearday d)
      | _ -> fail
    let during_daylight_savings : t -> bool = function
      | Date d -> Date.during_daylight_savings d
      | DateTime (d,_) -> Date.during_daylight_savings d
      | _ -> false

    let toTime = function
      | Time t -> return t
      | DateTime (d,t) -> return t
      | _ -> fail
    let toDate = function
      | Date d -> return d
      | DateTime (d,t) -> return d
      | _ -> fail
    let rec toYearMonth = function
      | Date d -> return {YearMonth.year = d.Date.year; YearMonth.month = d.Date.month}
      | YearMonth ym -> return ym
      | DateTime (d,t) -> toYearMonth (Date d)
      | _ -> fail
    let rec toMonthDay = function
      | Date d -> return {MonthDay.month = d.Date.month; MonthDay.day = d.Date.day}
      | MonthDay md -> return md
      | DateTime (d,t) -> toMonthDay (Date d)
      | _ -> fail

    let compare dt1 dt2 =
      match dt1, dt2 with
	| Time t1, Time t2 -> return (Time.compare t1 t2)
	| Date d1, Date d2 -> return (Date.compare d1 d2)
	| YearMonth ym1, YearMonth ym2 -> return (YearMonth.compare ym1 ym2)
	| MonthDay md1, MonthDay md2 -> return (MonthDay.compare md1 md2)
	| DateTime dt1, DateTime dt2 -> return (DateTime.compare dt1 dt2)
	| _ -> fail

    let rec add temp dur =
      if dur.Duration.negative
      then sub temp (Duration.abs dur)
      else
	match temp with
	  | Time t -> Time (snd (Time.add t dur))
	  | Date d -> Date (Date.add d dur)
	  | YearMonth ym -> YearMonth (YearMonth.add ym dur)
	  | MonthDay md -> MonthDay (MonthDay.add md dur)
	  | DateTime dt -> DateTime (DateTime.add dt dur)
    and sub temp dur =
      if dur.Duration.negative
      then add temp (Duration.abs dur)
      else
	match temp with
	  | Time t -> Time (snd (Time.sub t dur))
	  | Date d -> Date (Date.sub d dur)
	  | YearMonth ym -> YearMonth (YearMonth.sub ym dur)
	  | MonthDay md -> MonthDay (MonthDay.sub md dur)
	  | DateTime dt -> DateTime (DateTime.sub dt dur)

    let duration temp1 temp2 =
      match temp1, temp2 with
	| Time t1, Time t2 -> return (Time.duration t1 t2)
	| Date d1, Date d2 -> return (Date.duration d1 d2)
	| YearMonth ym1, YearMonth ym2 -> return (YearMonth.duration ym1 ym2)
	| MonthDay md1, MonthDay md2 -> return (MonthDay.duration md1 md2)
	| DateTime dt1, DateTime dt2 -> return (DateTime.duration dt1 dt2)
	| DateTime (d1,_), Date d2 -> return (Date.duration d1 d2)
	| Date d1, DateTime (d2,_) -> return (Date.duration d1 d2)
	| _ -> []
  end

(* Intervals *)

module Period =
  struct
    type t = DateTime.t * DateTime.t
    let make a b = (a,b)

    let regexp = Str.regexp "\\[\\([^,]+\\),\\([^,]+\\))$"
    let from_name = function
      | Rdf.Literal (s, Rdf.Typed dt) when dt = Strdf.uri_period ->
	if Str.string_match regexp s 0
	then
	  let s1 = Str.matched_group 1 s in
	  let s2 = Str.matched_group 2 s in
	  match
	    DateTime.from_name (Rdf.Literal (s1, Rdf.Typed Xsd.uri_dateTime)),
	    DateTime.from_name (Rdf.Literal (s2, Rdf.Typed Xsd.uri_dateTime))
	  with
	    | Some d1, Some d2 ->
	      if DateTime.compare d1 d2 <= 0
	      then Some (d1,d2)
	      else None
	    | _ -> (*None*) failwith "Period.from_name: invalid dates"
	else (*None*) failwith "Period.from_name: invalid period"
      | _ -> None
    let to_name (dt1,dt2) =
      match DateTime.to_name dt1, DateTime.to_name dt2 with
	| Rdf.Literal (s1, _), Rdf.Literal (s2, _) ->
	  let s = "[" ^ s1 ^ "," ^ s2 ^ ")" in
	  Rdf.Literal (s, Rdf.Typed Strdf.uri_period)
	| _ -> assert false

    let get_start (a,_) = return a
    let get_end (_,b) = return b

    let contains (a,b) dt =
      DateTime.compare a dt <= 0 && DateTime.compare dt b < 0 (* right-opened intervals *)

    let inter (a1,b1) (a2,b2) =
      let a = DateTime.max a1 a2 in
      let b = DateTime.min b1 b2 in
      if DateTime.compare a b < 0
      then return (a,b)
      else fail
    let union (a1,b1) (a2,b2) =
      let a = DateTime.min a1 a2 in
      let b = DateTime.max b1 b2 in
      return (a,b)
    let diff (a1,b1) (a2,b2) =
      match DateTime.compare a1 a2, DateTime.compare b1 b2 with
	| -1, -1 -> return (a1, DateTime.min b1 a2)
	| 1, 1 -> return (DateTime.max a1 b2, b1)
	| _ -> fail
    let left_diff (a1,b1) (a2,b2) =
      if DateTime.compare a1 a2 < 0
      then return (a1, min b1 a2)
      else fail
    let right_diff (a1,b1) (a2,b2) =
      if DateTime.compare b2 b1 < 0
      then return (max a1 b2, b1)
      else fail

    let duration (a,b) = return (DateTime.duration a b)
  end

module Interval =
  struct
    let types = [Xsd.uri_dateTime; Strdf.uri_period]

    type t = Instant of DateTime.t | Period of Period.t
    let from_name n =
      match n with
	| Rdf.Literal (_, Rdf.Typed dt) ->
	  if dt = Xsd.uri_dateTime then Option.map (fun dt -> Instant dt) (DateTime.from_name n)
	  else if dt = Strdf.uri_period then Option.map (fun p -> Period p) (Period.from_name n)
	  else None
	| _ -> None
    let to_name = function
      | Instant dt -> DateTime.to_name dt
      | Period p -> Period.to_name p

    let toPeriod : t -> Period.t monad = function
      | Instant x -> return (Period.make x x)
      | Period p -> return p

(*
    let inter i1 i2 =
      match i1, i2 with
	| Instant x1, Instant x2 ->
	  if DateTime.compare x1 x2 = 0
	  then return i1
	  else fail
	| Instant x1, Period p2 ->
	  if Period.contains p2 x1
	  then return i1
	  else fail
	| Period p1, Instant x2 ->
	  if Period.contains p1 x2
	  then return i2
	  else fail
	| Period p1, Period p2 ->
	  Period.inter p1 p2

    let union i1 i2 =
      match i1, i2 with
	| Instant x1, Instant x2 ->
	  if DateTime.compare x1 x2 = 0
	  then i1
	  else return (Period (DateTime.min x1 x2, DateTime.max x1 x2))
	| Instant x1, Period p2 -> union (Period (x1,x1)) i2
	| Period p1, Instant x2 -> union i1 (Period (x2,x2))
	| Period p1, Period p2 -> Period.union p1 p2
*)

(* Allen's relations without inverses *)

    let left = function
      | Instant x -> x
      | Period (a,b) -> a
    let right = function
      | Instant x -> x
      | Period (a,b) -> b

    let before i1 i2 = DateTime.compare (right i1) (left i2) < 0

    let meets i1 i2 = DateTime.compare (right i1) (left i2) = 0

    let overlaps i1 i2 =
      DateTime.compare (left i1) (left i2) < 0
      && DateTime.compare (left i2) (right i1) < 0
      && DateTime.compare (right i1) (right i2) < 0

    let starts i1 i2 =
      DateTime.compare (left i1) (left i2) = 0
      && DateTime.compare (right i1) (right i2) < 0

    let ends i1 i2 =
      DateTime.compare (left i2) (left i1) < 0
      && DateTime.compare (right i1) (right i2) = 0

    let during i1 i2 =
      DateTime.compare (left i2) (left i1) < 0
      && DateTime.compare (right i1) (right i2) < 0

    let equals i1 i2 =
      DateTime.compare (left i1) (left i2) = 0
      && DateTime.compare (right i1) (right i2) = 0

  end

(* Constructors *)

let utc_timezone = Time.utc_timezone

let local_timezone =
  let t = Unix.time () in
  let dt_gmt = DateTime.from_tm (Unix.gmtime t) Time.utc_timezone in
  let dt_local = DateTime.from_tm (Unix.localtime t) Time.utc_timezone in
  let delta = DateTime.duration dt_gmt dt_local in
  (delta.Duration.negative, delta.Duration.hours, delta.Duration.minutes)

let makeTime (hours : Basic.Int.t) (minutes : Basic.Int.t) (seconds : Basic.Float.t) : Time.t monad =
  return (Time.make ~hours ~minutes ~seconds ~timezone:local_timezone)
let makeTime_func = new func3
  [ [Xsd.uri_time; Xsd.uri_integer; Xsd.uri_integer; Xsd.uri_double] ] (* TODO: should be decimal *)
  Basic.Int.from_name Basic.Int.from_name Basic.Float.from_name makeTime Time.to_name

let makeDate (year : Basic.Int.t) (month : Basic.Int.t) (day : Basic.Int.t) : Date.t monad =
  return (Date.make ~year ~month ~day)
let makeDate_func = new func3
  [ [Xsd.uri_date; Xsd.uri_integer; Xsd.uri_integer; Xsd.uri_integer] ] (* TODO: should be decimal *)
  Basic.Int.from_name Basic.Int.from_name Basic.Int.from_name makeDate Date.to_name

let makeDateTime (d : Date.t) (t : Time.t) : DateTime.t monad =
  return (DateTime.make d t)
let makeDateTime_func = new func2
  [ [Xsd.uri_dateTime; Xsd.uri_date; Xsd.uri_time] ]
  Date.from_name Time.from_name makeDateTime DateTime.to_name

let today () : Date.t monad =
  let dt_local = DateTime.from_tm (Unix.localtime (Unix.time ())) local_timezone in
  return (DateTime.date dt_local)
let today_func = new func0
  [ [Xsd.uri_date] ]
  today Date.to_name

let now () : DateTime.t monad =
  let dt_local = DateTime.from_tm (Unix.localtime (Unix.time ())) local_timezone in
  return dt_local
let now_func = new func0
  [ [Xsd.uri_dateTime] ]
  now DateTime.to_name

let makePeriod (a : DateTime.t) (b : DateTime.t) : Period.t monad =
  return (Period.make a b)
let makePeriod_func = new func2
  [ [Strdf.uri_period; Xsd.uri_dateTime; Xsd.uri_dateTime] ]
  DateTime.from_name DateTime.from_name makePeriod Period.to_name

(* Field accessors *)

let year_func = new func1
  (List.map (fun uri -> [Xsd.uri_integer; uri]) [Xsd.uri_date; Xsd.uri_gYearMonth; Xsd.uri_dateTime])
  Temporal.from_name Temporal.year Basic.Int.to_name

let month_func = new func1
  (List.map (fun uri -> [Xsd.uri_integer; uri]) [Xsd.uri_date; Xsd.uri_gYearMonth; Xsd.uri_gMonthDay; Xsd.uri_dateTime])
  Temporal.from_name Temporal.month Basic.Int.to_name

let day_func = new func1
  (List.map (fun uri -> [Xsd.uri_integer; uri]) [Xsd.uri_date; Xsd.uri_gMonthDay; Xsd.uri_dateTime])
  Temporal.from_name Temporal.day Basic.Int.to_name

let weekday_func = new func1
  [ [Xsd.uri_string; Xsd.uri_date];
    [Xsd.uri_string; Xsd.uri_dateTime] ]
  Temporal.from_name Temporal.weekday Basic.String.to_name

let yearday_func = new func1
  [ [Xsd.uri_integer; Xsd.uri_date];
    [Xsd.uri_integer; Xsd.uri_dateTime] ]
  Temporal.from_name Temporal.yearday Basic.Int.to_name

let during_daylight_savings_pred = new pred1
  [ [Xsd.uri_date];
    [Xsd.uri_dateTime] ]
  Temporal.from_name Temporal.during_daylight_savings

let hours_func = new func1
  (List.map (fun uri -> [Xsd.uri_integer; uri]) [Xsd.uri_time; Xsd.uri_dateTime])
  Temporal.from_name Temporal.hours Basic.Int.to_name

let minutes_func = new func1
  (List.map (fun uri -> [Xsd.uri_integer; uri]) [Xsd.uri_time; Xsd.uri_dateTime])
  Temporal.from_name Temporal.minutes Basic.Int.to_name

let seconds_func = new func1
  (List.map (fun uri -> [Xsd.uri_double; uri]) [Xsd.uri_time; Xsd.uri_dateTime])
  Temporal.from_name Temporal.seconds Basic.Float.to_name

let start_func = new func1
  [ [Xsd.uri_dateTime; Strdf.uri_period] ]
  Period.from_name Period.get_start DateTime.to_name

let end_func = new func1
  [ [Xsd.uri_dateTime; Strdf.uri_period] ]
  Period.from_name Period.get_end DateTime.to_name

(* Convertors *)

let toTime_func = new func1
  [ [Xsd.uri_time; Xsd.uri_time];
    [Xsd.uri_time; Xsd.uri_dateTime] ]
  Temporal.from_name Temporal.toTime Time.to_name

let toDate_func = new func1
  [ [Xsd.uri_date; Xsd.uri_date];
    [Xsd.uri_date; Xsd.uri_dateTime] ]
  Temporal.from_name Temporal.toDate Date.to_name

let toYearMonth_func = new func1
  (List.map (fun uri -> [Xsd.uri_gYearMonth; uri])
     [Xsd.uri_date; Xsd.uri_gYearMonth; Xsd.uri_dateTime])
  Temporal.from_name Temporal.toYearMonth YearMonth.to_name

let toMonthDay_func = new func1
  (List.map (fun uri -> [Xsd.uri_gMonthDay; uri])
     [Xsd.uri_date; Xsd.uri_gMonthDay; Xsd.uri_dateTime])
  Temporal.from_name Temporal.toMonthDay MonthDay.to_name

let toPeriod_func = new func1
  [ [Strdf.uri_period; Xsd.uri_dateTime];
    [Strdf.uri_period; Strdf.uri_period] ]
  Interval.from_name Interval.toPeriod Period.to_name

(* Comparators *)

let duration_comp = new comp2
  [ [Xsd.uri_duration; Xsd.uri_duration] ]
  Duration.from_name Duration.from_name (fun d1 d2 -> return (Duration.compare d1 d2))

let temporal_comp = new comp2
  (List.map (fun uri -> [uri; uri]) Temporal.types)
  Temporal.from_name Temporal.from_name Temporal.compare

(* Operators *)

let duration_totalSeconds (dur : Duration.t) : Basic.Float.t monad =
  return (Duration.totalSeconds dur)
let duration_totalSeconds_func = new func1
  [ [Xsd.uri_double; Xsd.uri_duration] ]
  Duration.from_name duration_totalSeconds Basic.Float.to_name

let duration_totalHours (dur : Duration.t) : Basic.Float.t monad =
  return (Duration.totalSeconds dur /. 3600.)
let duration_totalHours_func = new func1
  [ [Xsd.uri_double; Xsd.uri_duration] ]
  Duration.from_name duration_totalHours Basic.Float.to_name

let duration_totalDays (dur : Duration.t) : Basic.Float.t monad =
  return (Duration.totalSeconds dur /. 86400.)
let duration_totalDays_func = new func1
  [ [Xsd.uri_double; Xsd.uri_duration] ]
  Duration.from_name duration_totalDays Basic.Float.to_name

let duration_plus (dur1 : Duration.t) (dur2 : Duration.t) : Duration.t monad =
  return (Duration.add dur1 dur2)
let duration_plus_func = new func2
  [ [Xsd.uri_duration; Xsd.uri_duration; Xsd.uri_duration] ]
  Duration.from_name Duration.from_name duration_plus Duration.to_name

let duration_minus (dur1 : Duration.t) (dur2 : Duration.t) : Duration.t monad =
  return (Duration.sub dur1 dur2)
let duration_minus_func = new func2
  [ [Xsd.uri_duration; Xsd.uri_duration; Xsd.uri_duration] ]
  Duration.from_name Duration.from_name duration_minus Duration.to_name

let temporal_plus_duration (temp : Temporal.t) (dur : Duration.t) : Temporal.t monad =
  return (Temporal.add temp dur)
let temporal_plus_duration_func = new func2
  (List.map (fun uri -> [uri; uri; Xsd.uri_duration]) Temporal.types)
  Temporal.from_name Duration.from_name temporal_plus_duration Temporal.to_name

let temporal_minus_duration (temp : Temporal.t) (dur : Duration.t) : Temporal.t monad =
  return (Temporal.sub temp dur)
let temporal_minus_duration_func = new func2
  (List.map (fun uri -> [uri; uri; Xsd.uri_duration]) Temporal.types)
  Temporal.from_name Duration.from_name temporal_minus_duration Temporal.to_name

let temporal_minus_temporal (temp1 : Temporal.t) (temp2 : Temporal.t) : Duration.t monad =
  Temporal.duration temp2 temp1
let temporal_minus_temporal_func = new func2
  (List.map (fun uri -> [Xsd.uri_duration; uri; uri]) Temporal.types)
  Temporal.from_name Temporal.from_name temporal_minus_temporal Duration.to_name

let period_duration_func = new func1
  [ [Xsd.uri_duration; Strdf.uri_period] ]
  Period.from_name Period.duration Duration.to_name

(* Relationships *)

let allen_pred2 (p2 : Interval.t -> Interval.t -> bool) = new pred2
  [ [Xsd.uri_dateTime; Xsd.uri_dateTime];
    [Xsd.uri_dateTime; Strdf.uri_period];
    [Strdf.uri_period; Xsd.uri_dateTime];
    [Strdf.uri_period; Strdf.uri_period] ]
  Interval.from_name Interval.from_name p2

let before_pred = allen_pred2 Interval.before
let meets_pred = allen_pred2 Interval.meets
let overlaps_pred = allen_pred2 Interval.overlaps
let starts_pred = allen_pred2 Interval.starts
let ends_pred = allen_pred2 Interval.ends
let during_pred = allen_pred2 Interval.during
let equals_pred = allen_pred2 Interval.equals
