NAME=sewelis

VPATH = ocaml-lib:http-daemon:src

OCAMLFIND = ocamlfind
OCAMLC = $(OCAMLFIND) ocamlc
OCAMLOPT = $(OCAMLFIND) ocamlopt
INCLUDES = -package str,unix,num,xmlm,xml-light,lablgtk2 -I ocaml-lib -I ocaml-lib/dcg -I ocaml-lib/ipp -I src
OCAMLFLAGS = -pp "camlp4o -I ocaml-lib/dcg -I ocaml-lib/ipp pa_dcg.cmo pa_ipp.cmo" -w usy -thread $(INCLUDES)
OCAMLFLAGS_NOCAMLP4 = -thread -w sy $(INCLUDES)

COMMON_SRC = common.ml unicode.ml bintree.ml lSet.ml cis.ml iterator.ml intmap.ml intset.ml intrel2.ml intreln.ml text_index.ml
SEWELIS_SRC = option.ml tarpit.ml uri.ml xsd.ml rdf.ml rdfs.ml owl.ml foaf.ml strdf.ml term.ml turtle.ml ntriples.ml name.ml builtins.ml builtins_basic.ml builtins_temporal.ml extension.ml code.ml store_order.ml store_match.ml store.ml fol.ml log.ml lisql_namespace.ml lisql_ast.ml lisql_syntax.ml lisql_display.ml lisql_semantics.ml lisql_transf.ml lisql_feature.ml lisql_index.ml lisql_concept.ml lisql.ml func_api.ml gui.ml


XOBJECTS_COMMON = $(COMMON_SRC:.ml=.cmx)
XOBJECTS_SEWELIS = $(SEWELIS_SRC:.ml=.cmx)
XOBJECTS = $(XOBJECTS_COMMON) $(XOBJECTS_SEWELIS)

ARCHIVE = $(NAME).cma
XARCHIVE = $(NAME).cmxa
XPLUGIN = $(NAME).cmxs

.PHONY: http-daemon http-daemon-run

# TODO: use lablgtk2.auto-init in INCLUDES and remove explicit inclusion of gtkInit.cmx here
sewelis: lib $(XPLUGIN)
	$(OCAMLOPT) $(INCLUDES) -o sewelis -linkpkg gtkInit.cmx $(XOBJECTS_COMMON) dcg.cmxa ipp.cmxa $(XOBJECTS_SEWELIS)

sewelis-java: lib-java $(XOBJECTS_COMMON) $(XOBJECTS_SEWELIS) jts.cmx builtins_spatial.cmx
	$(OCAMLOPT) $(INCLUDES) -I ocaml-lib/camljava/lib -o sewelis-java -linkpkg gtkInit.cmx jni.cmxa $(XOBJECTS_COMMON) jni_common.cmx dcg.cmxa ipp.cmxa $(XOBJECTS_SEWELIS) jts.cmx builtins_spatial.cmx

jts.cmx: jts.ml
	$(OCAMLOPT) $(OCAMLFLAGS) -I ocaml-lib/camljava/lib -c $<
builtins_spatial.cmx: builtins_spatial.ml
	$(OCAMLOPT) $(OCAMLFLAGS) -I ocaml-lib/camljava/lib -c $<

server:
	make -C http-daemon server

server-java:
	make -C http-daemon server-java

http-daemon-run:
	make -C http-daemon run

client:
	make -C js-client

gui.cmx: gui.ml
	$(OCAMLOPT) $(OCAMLFLAGS_NOCAMLP4) -package lablgtk2 -c $<

lib-java: lib
	make -C ocaml-lib/camljava
	make -C ocaml-lib all-java

lib:
	make -C ocaml-lib
	make -C ocaml-lib/dcg
	make -C ocaml-lib/ipp

$(ARCHIVE): $(OBJECTS)
	@echo "Byte-compiling Sewelis core library '$(ARCHIVE)'..."
	$(OCAMLC) -a $(OCAMLFLAGS) -o $@ $^

$(XARCHIVE): $(XOBJECTS)
	@echo "Compiling Sewelis core library '$(XARCHIVE)'..."
	$(OCAMLOPT) -a $(OCAMLFLAGS) -o $@ $^



# ----------------------------------------------------------------------
# Common rules

%.cmo: %.ml
	$(OCAMLC) $(OCAMLFLAGS) -c $<

%.cmi: %.mli
	$(OCAMLC) $(OCAMLFLAGS) -c $<

%.cmx: %.ml
	$(OCAMLOPT) $(OCAMLFLAGS) -c $<

%.cmxs: %.cmxa
	$(OCAMLOPT) -shared -linkall -o $@ $<



# ----------------------------------------------------------------------
# Clean up
clean:
	rm -f src/*.cm[ioax]
	rm -f src/*.cmx[sa]
	rm -f src/*.o

cleanlisql:
	rm -f src/lisql*.cm[ioax]
	rm -f src/func_api.cm[ioax]
	rm -f src/gui.cm[ioax]

clean-all: clean
	make -C ocaml-lib/dcg clean
	make -C ocaml-lib/ipp clean
	make -C ocaml-lib/camljava clean
	make -C ocaml-lib clean
	make -C http-daemon clean


# ----------------------------------------------------------------------
# Install

install: uninstall $(XPLUGIN)
	@echo "Installing sewelis libraries..."
	@ocamlfind install $(NAME) META ocaml-lib/*.cmi ocaml-lib/dcg/*.cmi ocaml-lib/ipp/*.cmi ocaml-lib/camljava/lib/*.cmi src/*.cmi $(XARCHIVE) $(XPLUGIN) -optional sewelis.a

uninstall:
	@echo "Uninstalling Sewelis..."
	@ocamlfind remove $(NAME)
